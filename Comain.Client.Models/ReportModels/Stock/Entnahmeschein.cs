﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class Entnahmeschein
    {

        public String   Nummer                { get; set; }
        public String   Datum                 { get; set; }
        public String   Artikelbezeichnung    { get; set; }
        public String   Artikelnummer         { get; set; }
        public String   Auftragsnummer        { get; set; }
        public String   Kostenstelle          { get; set; }
        public String   IHObjekt              { get; set; }
        public String   Maschine              { get; set; }
        public String   Besteller             { get; set; }
        public String   Lagerort              { get; set; }
        public decimal  Entnahmemenge         { get; set; }
        public decimal  Fachrestmenge         { get; set; }
        public String   Notizen               { get; set; }
    }
}
