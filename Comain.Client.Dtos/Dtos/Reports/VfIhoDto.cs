﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class VfIhoDto
    {

        public DateTime     Monat               { get; set; }
        public decimal?     SollBelegungszeit   { get; set; }
        public decimal?     SollAusfallzeit     { get; set; }
        public decimal?     Störzeit            { get; set; }
        public decimal?     Verfügbarkeit       { get; set; }
        public decimal?     Ausfallrate         { get; set; }
        public decimal?     Abweichungsgrad     { get; set; }
    }
}
