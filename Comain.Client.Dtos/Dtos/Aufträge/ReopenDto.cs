﻿using System;


namespace Comain.Client.Dtos.Aufträge
{

    ///<summary>
    ///DTO zum Öffnen geschlossener Aufträge, zum Anzeigen wiedergeöffneter Aufträge wird AuReopenDto verwendet
    ///</summary>
    public class ReopenDto
    {

        public int      Id      { get; set; }
        public String   Reason  { get; set; }
    }
}
