﻿using System.Windows.Controls;
using Comain.Client.Views.Reports;

namespace Comain.Client.Views.Stock.Inventuren
{
    /// <summary>
    /// Interaction logic for InventurReportView.xaml
    /// </summary>
    public partial class InventurReportView:UserControl    ,IInventurReportView
    {
        public InventurReportView()
        {
        InitializeComponent();
        }
    }
}
