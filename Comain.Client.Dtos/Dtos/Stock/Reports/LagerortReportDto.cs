﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class LagerortReportDto
    {

        public String   Nummer         { get; set; }
        public decimal? Menge          { get; set; }
        public decimal? Mischpreis     { get; set; }
        public decimal? Gesamtpreis    { get; set; }
    }
}
