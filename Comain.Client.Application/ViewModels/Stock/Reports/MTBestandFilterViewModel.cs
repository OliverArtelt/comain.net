﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class MTBestandFilterViewModel : ViewModel<IMTBestandFilterView>, IFilterViewModel
    {

        public MTBestandFilterViewModel(IMTBestandFilterView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            
            SortArtikelnummer = true;
            NurFehlende = false;
            KlasseA = false;
            KlasseB = false;
            KlasseC = false;
            deaktivierte = "bestand";
            RaiseRadios();
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            var bld = new StringBuilder();

            if (NurFehlende) bld.Append("nur Fehlende; ");
            if (KlasseA || KlasseB || KlasseC) {
            
                bld.Append("Klasse ");
                if (KlasseA) bld.Append("A,");
                if (KlasseB) bld.Append("B,");
                if (KlasseC) bld.Append("C,");
                bld.Length -= 1;
                bld.Append("; ");
            }

            if (deaktivierte == "aktiv") bld.Append("nur aktive Artikel; ");
            if (deaktivierte == "bestand") bld.Append("nur aktive Artikel oder mit Bestand; ");

            if (SortArtikelnummer) bld.Append("Sortierung Artikelnummer; ");
            else if (SortWarengruppe) bld.Append("Sortierung Warengruppe; ");
            else bld.Append("Sortierung Artikelbezeichnung; ");

            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString();
        }


        public void AddFilter(Suchkriterien filter)
        {
        
            if (SortArtikelnummer) filter.Add("Sortierung", "Artikelnummer");
            else if (SortWarengruppe) filter.Add("Sortierung", "Warengruppe");
            else filter.Add("Sortierung", "Artikelbezeichnung");

            if (NurFehlende) filter.Add("NurFehlende", true);
            if (KlasseA) filter.Add("KlasseA", true);
            if (KlasseB) filter.Add("KlasseB", true);
            if (KlasseC) filter.Add("KlasseC", true);

            filter.Add("Deaktivierte", deaktivierte);
        }


        public Task LoadAsync(CancellationToken ct = default(CancellationToken))
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }
 

        private void RaiseRadios()
        {

            RaisePropertyChanged(nameof(RadioAlle));
            RaisePropertyChanged(nameof(RadioNurAktive));
            RaisePropertyChanged(nameof(RadioMitBestand));
        }


#region P R O P E R T I E S


        private String deaktivierte = "bestand";
        public bool RadioNurAktive
        { 
            get { return deaktivierte == "aktiv"; }
            set { deaktivierte = "aktiv"; RaiseRadios(); }
        }

        public bool RadioMitBestand
        { 
            get { return deaktivierte == "bestand"; }
            set { deaktivierte = "bestand"; RaiseRadios(); }
        }

        public bool RadioAlle
        { 
            get { return deaktivierte == null; }
            set { deaktivierte = null; RaiseRadios(); }
        }


        private bool sortArtikelname;
        public bool SortArtikelname
        { 
            get { return sortArtikelname; }
            set { 
            
                if (SetProperty(ref sortArtikelname, value)) {
                
                    if (value) SortArtikelnummer = false;   
                    if (value) SortWarengruppe = false;   
                }
            }
        } 
   

        private bool sortArtikelnummer;
        public bool SortArtikelnummer
        { 
            get { return sortArtikelnummer; }
            set { 
            
                if (SetProperty(ref sortArtikelnummer, value)) {
                
                    if (value) SortArtikelname = false;   
                    if (value) SortWarengruppe = false;   
                }
            }
        } 
   

        private bool sortWarengruppe;
        public bool SortWarengruppe
        { 
            get { return sortWarengruppe; }
            set { 
            
                if (SetProperty(ref sortWarengruppe, value)) {
                
                    if (value) SortArtikelnummer = false;   
                    if (value) SortArtikelname = false;   
                }
            }
        } 
   

        private bool nurFehlende;
        public bool NurFehlende
        { 
            get { return nurFehlende; }
            set { SetProperty(ref nurFehlende, value); }
        } 
   

        private bool klasseA;
        public bool KlasseA
        { 
            get { return klasseA; }
            set { SetProperty(ref klasseA, value); }
        } 
   

        private bool klasseB;
        public bool KlasseB
        { 
            get { return klasseB; }
            set { SetProperty(ref klasseB, value); }
        } 
   

        private bool klasseC;
        public bool KlasseC
        { 
            get { return klasseC; }
            set { SetProperty(ref klasseC, value); }
        } 
     

#endregion

    }
}
