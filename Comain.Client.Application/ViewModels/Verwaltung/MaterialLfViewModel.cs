﻿using System.Collections.ObjectModel;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class MaterialLfViewModel : ViewModel<IMaterialLfView>
    {
        
        private readonly SynchronizationContext context;


        public MaterialLfViewModel(IMaterialLfView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }
 

        public void Unload()
        {
            Liste = null;
        }


#region P R O P E R T I E S


        private ObservableCollection<MtLfAssignItem> liste;
        public ObservableCollection<MtLfAssignItem> Liste
        { 
            get { return liste; }
            set { SetProperty(ref liste, value); }
        }

       
#endregion   

    }
}
