﻿using System;
using Comain.Client.Services;
using Comain.Client.ViewModels.Stock.Reklamationen;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Reklamationen
{

    public class ReklamCommandMediator:IReklamCommandMediator
    {

        private readonly ILoginService login;
        private readonly ReklamViewModel view;
        private readonly ReklamDetailViewModel detailView;
        private readonly ReklamDetailBaseViewModel basisView;
        private readonly ReklamListViewModel listView;
        private readonly ReklamBSPosViewModel addView;


        public ReklamCommandMediator(ILoginService login, ReklamViewModel view, ReklamDetailViewModel detailView, ReklamDetailBaseViewModel basisView,
                                     ReklamListViewModel listView, ReklamBSPosViewModel addView)
        {

            this.login = login;
            this.view = view;
            this.detailView = detailView;
            this.basisView = basisView;
            this.addView = addView;
            this.listView = listView;
        }


        private bool HasCurrent   { get { return detailView.Current != null; } }
        
        public EditMode CurrentMode { get; private set; }


        public void SetEditMode(EditMode newMode)
        {

            CurrentMode = newMode;
            listView.EditEnabled  = newMode == EditMode.ReadOnly; 
            basisView.EditEnabled = newMode != EditMode.ReadOnly; 

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            bool isAuthorized = login.AktuellerNutzer.IstAdministrator || login.AktuellerNutzer.IstLagerAdministrator || login.AktuellerNutzer.IstLagerEinlagerungen; 

            view.EditVisible        = CurrentMode == EditMode.ReadOnly && isAuthorized && detailView.Current != null;
            view.CancelVisible      = CurrentMode != EditMode.ReadOnly && isAuthorized && detailView.Current != null;
            view.SaveVisible        = CurrentMode != EditMode.ReadOnly && isAuthorized;
            view.AddVisible         = CurrentMode == EditMode.ReadOnly && isAuthorized && detailView.Tab == ReklamDetailViewModel.Tabs.Basis;
            view.DeleteVisible      = CurrentMode != EditMode.ReadOnly && isAuthorized && detailView.Tab == ReklamDetailViewModel.Tabs.Basis && detailView.Current != null;
            view.ReloadVisible      = CurrentMode == EditMode.ReadOnly && isAuthorized;
            view.AddPosVisible      = CurrentMode != EditMode.ReadOnly && isAuthorized && detailView.Tab == ReklamDetailViewModel.Tabs.Basis && detailView.Current != null;
            view.DeletePosVisible   = CurrentMode != EditMode.ReadOnly && isAuthorized && detailView.Tab == ReklamDetailViewModel.Tabs.Basis && detailView.Current != null;
        }
    }
}
