﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class LbAuPsItem
    {

        public String       PsNummer    { get; set; }
        public String       PsKlammer   { get; set; }
        public decimal?     Kosten      { get; set; }
        public DateTime?    Beginn      { get; set; }
        public DateTime?    Ende        { get; set; }
        public int?         Dauer       { get; set; }
    }
}
