﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Commands;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Controllers.Specs;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Specs;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.IHObjekte;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.ViewModels.IHObjekte;

namespace Comain.Client.Controllers.IHObjekte
{

    public class IHObjektController : ControllerBase, IIHObjektController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IFlyoutService flyouts;
        private readonly IModulService module;
        private readonly IIHObjektService store;
        private readonly MainViewModel mainView;
        private readonly IHObjektViewModel view;
        private readonly IHObjektTreeViewModel treeView;
        private readonly IHObjektFilterViewModel filterView;
        private readonly IHObjektDetailViewModel detailView;
        private readonly IHObjektDetailBaseViewModel baseView;
        private readonly IHObjektDetailPrintViewModel printView;
        private readonly IHObjektDetailMemoViewModel memoView;
        private readonly IHDeactivateViewModel deactivateView;
        private readonly IHObjektDetailEreignisViewModel ereignisView;
        private readonly IHMoveViewModel moveView;
        private readonly AssetSpecViewModel specView;
        private readonly AssetArtikelViewModel artikelView;
        private readonly AssetArtikelAddViewModel artikelAddView;
        private readonly AssetPersonalViewModel personalView;

        private IList<IHListDto> currentList;
        private Konfiguration cfg;

        private SerializableCommand editCommand;
        private SerializableCommand copyCommand;
        private SerializableCommand addSubCommand;
        private SerializableCommand deleteArtikelCommand;
        private SerializableCommand addArtikelCommand;
        private SerializableCommand moveCommand;

        private IDokumentAssignController docs;
        private IMassnahmePluginController massnahmen;


        public IHObjektController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts, IKonfigService configs,
                                  IIHObjektService store, IControllerBus bus, IModulService module,
                                  MainViewModel mainView, IHObjektViewModel view, IHObjektTreeViewModel treeView, IHObjektFilterViewModel filterView,
                                  IHObjektDetailViewModel detailView, IHObjektDetailBaseViewModel baseView, AssetSpecViewModel specView,
                                  AssetArtikelViewModel artikelView, AssetArtikelAddViewModel artikelAddView, AssetPersonalViewModel personalView,
                                  IHObjektDetailPrintViewModel printView, IHObjektDetailMemoViewModel memoView, IHDeactivateViewModel deactivateView,
                                  IHMoveViewModel moveView, IHObjektDetailEreignisViewModel ereignisView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.store = store;
            this.flyouts = flyouts;
            this.configs = configs;
            this.module = module;
            this.mainView = mainView;
            this.view = view;
            this.treeView = treeView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.baseView = baseView;
            this.printView = printView;
            this.memoView = memoView;
            this.deactivateView = deactivateView;
            this.moveView = moveView;
            this.ereignisView = ereignisView;
            this.specView = specView;
            this.artikelView = artikelView;
            this.artikelAddView = artikelAddView;
            this.personalView = personalView;
        }


        public void Initialize()
        {

            docs = bus.GetController<IIHDokumentController>();
            massnahmen = bus.GetController<IMassnahmePluginController>();

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.TreeView = treeView.View;

            detailView.BasisView = baseView.View;
            detailView.DruckView = printView.View;
            detailView.MemoView = memoView.View;
            detailView.EreignisView = ereignisView.View;
            detailView.DokumentView = docs.View;
            detailView.AuftragView = massnahmen.AuftragView;
            detailView.VorgabeView = massnahmen.MassnahmeView;
            detailView.SpecView = specView.View;
            detailView.ArtikelView = artikelView.View;
            detailView.PersonalView = personalView.View;
            view.OpenMassnahmeCommand = CreateCommand(_ => massnahmen.OpenMassnahmeAsync());
            view.AddMassnahmeCommand = CreateCommand(_ => massnahmen.AddMassnahmeAsync());
            view.OpenAuftragCommand = CreateCommand(_ => massnahmen.OpenAuftragAsync());
            view.SelfServiceCommand = CreateCommand(_ => SelfServiceAsync());
            view.CreateAufträgeCommand = CreateCommand(_ => massnahmen.CreateAufträgeAsync());
            view.DeleteArtikelCommand = deleteArtikelCommand = CreateCommand(_ => DeleteArtikelAsync(), _ => artikelView.CurrentArtikel != null);
            view.AddArtikelCommand = addArtikelCommand = CreateCommand(_ => AddArtikelAsync(), _ => detailView.Current != null);
            view.BackCommand = new AsyncDelegateCommand(_ => bus.BackAsync(detailView.Current == null || detailView.Current.IstAngefügt? null: detailView.Current));
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.MoveCommand = moveCommand = CreateCommand(_ => MoveRequestAsync(), _ => detailView.Current != null);
            view.EditCommand = editCommand = CreateCommand(_ => EditAsync(), _ => treeView.CurrentIHObjekt != null);
            view.CancelCommand = CreateCommand(_ => VerwerfenAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.AddCommand = CreateCommand(_ => AddAsync(CopyMode.Add));
            view.AddSubCommand = addSubCommand = CreateCommand(_ => AddAsync(CopyMode.AddSub), _ => treeView.CurrentIHObjekt != null);
            view.CopyCommand = copyCommand = CreateCommand(_ => AddAsync(CopyMode.Copy), _ => treeView.CurrentIHObjekt != null);
            view.DeleteCommand = CreateCommand(_ => DeleteAsync());
            view.DeactivateCommand = CreateCommand(_ => DeaktivierenAsync());
            treeView.DropCommand = CreateCommand(p => MoveRequestAsync(p));
            treeView.DetailCommand = CreateCommand(_ => DetailAsync());
            moveView.ExecuteCommand = CreateCommand(_ => MoveAsync());

            PropertyChangedEventManager.AddHandler(filterView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(ereignisView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(artikelView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(specView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            if (parameter is IHObjektCommand) detailView.ResetTabs(); //nur wenn vom Hauptmenü kommend
            crumbs.Add("Assets");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            view.MoveVisible = login.AktuellerNutzer.IstAdministrator || login.AktuellerNutzer.IstInstandhaltungLeitung;
            view.SelfServiceVisible = login.AktuellerNutzer.IstDevAdmin;

            await ExecuteWithLockAsync(_ => LoadAsync());
            if (module.Info.VerwendetModulWPlan) await massnahmen.RunAsync(parameter);

            if (parameter is ControllerMessage msg) {

                if (msg.Entity_Id > 0) await ExecuteWithLockAsync(_ => DetailAsync(msg.Entity_Id));
                else if (msg.EditMode == EditMode.Append) {

                    var ih = msg.Parameter as IHObjekt;
                    await ExecuteWithLockAsync(_ => AddAsync(CopyMode.Inject, ih));
                }
            }

            await docs.RunAsync(parameter);
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.AddHandler(specView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(artikelView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(ereignisView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(filterView, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (detailView.Current != null && (detailView.Current.IsChanged || detailView.Current.DocumentsChanged)) {

                detailView.ResetTabsIfPrint();
                if (!await messages.ConfirmExitAsync()) return false;
            }

            if (module.Info.VerwendetModulWPlan && !await massnahmen.UnloadAsync()) return false;
            await docs.UnloadAsync();

            currentList = null;

            baseView.Unload();
            treeView.Unload();
            filterView.Unload();
            memoView.Unload();
            ereignisView.Unload();
            moveView.Unload();
            artikelAddView.Unload();
            artikelView.Unload();
            personalView.Unload();

            return true;
        }


        private async Task LoadAsync()
        {

            SetEditMode(EditMode.ReadOnly);
            var oldlistmodel = treeView.CurrentIHObjekt;

            treeView.Unload();

            using (new VisualDelay(view)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var ogtask = store.ObjektgruppenAsync(bus.Token);
                var oatask = store.ObjektartenAsync(bus.Token);
                var lftask = store.LieferantenAsync(bus.Token);
                var pstask = store.PersonalAsync(bus.Token);
                var sptask = store.SpecsAsync(bus.Token);
                var mttask = store.MaterialstammAsync(bus.Token);
                var cftask = configs.GetAsync(bus.Token);

                await Task.WhenAll(sptask, ihtask, pztask, kstask, cftask, ogtask, oatask, pstask, lftask, mttask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();

                cfg = cftask.Result;
                currentList = ihtask.Result;
                filterView.SetIHListe(currentList);
                detailView.SetIHListe(currentList);
                personalView.Personalliste = pstask.Result ?? new List<PersonalFilterModel>();
                await OnUIThreadAsync(() => { artikelAddView.LoadMaterial(mttask.Result); });
                await moveView.SetzeListenAsync(kstask.Result, pztask.Result, ihtask.Result, bus.Token);
                await baseView.SetzeListenAsync(pztask.Result, kstask.Result, ogtask.Result, oatask.Result, lftask.Result, pstask.Result, sptask.Result);
                view.SetzeKonfig(cfg);

                await treeView.GenerateTreeAsync(currentList.Select(p => new IHFilterItem(p)).ToList(),
                                                 pztask.Result, kstask.Result, cfg, bus.Token).ConfigureAwait(false);

                if (module.Info.VerwendetModulWPlan) massnahmen.LoadAsync();
                await docs.LoadAsync();
            }

            if (oldlistmodel != null) {

                treeView.SetCurrent(oldlistmodel.Id);
            }
        }


        private async Task DetailAsync(int? id = null)
        {

            printView.Unload();
            baseView.Thumbnail = null;

            if (id.HasValue || treeView.CurrentIHObjekt != null) {

                IHObjekt model;

                using (new VisualDelay(detailView)) {

                    model = await store.DetailAsync(id ?? treeView.CurrentIHObjekt.Id, bus.Token);
                    if (module.Info.VerwendetModulWPlan) await massnahmen.DetailAsync(model);
                    bus.Token.ThrowIfCancellationRequested();

                    SetModel(model);
                }

                await docs.DetailAsync(model).ConfigureAwait(false);
                var titelbild = await docs.TitelbildAsync().ConfigureAwait(false);
                baseView.Thumbnail = titelbild?.Thumbnail;

            } else {

                SetModel(null);
                if (module.Info.VerwendetModulWPlan) await massnahmen.DetailAsync(null).ConfigureAwait(false);
                await docs.DetailAsync(null);
            }

            if (detailView.Tab == IHObjektDetailViewModel.Tabs.Historie) await UpdateEreignisAsync();

            UpdateCommands();
            await docs.FillThumbnailsAsync();
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => {

                editCommand.RaiseCanExecuteChanged();
                copyCommand.RaiseCanExecuteChanged();
                addSubCommand.RaiseCanExecuteChanged();
                deleteArtikelCommand.RaiseCanExecuteChanged();
                addArtikelCommand.RaiseCanExecuteChanged();
                moveCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private void SetModel(IHObjekt current)
        {

            if (current == detailView.Current) {

                NeuSetzen();
                return;
            }

            if (current != null) current.Konfiguration = cfg;

            detailView.Current = current;
            baseView.Current = current;
            memoView.Current = current;
            treeView.SetModel(current);
            specView.Current = current;
            artikelView.Current = current;
            personalView.Current = current;

            NeuSetzen();
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (sender == filterView && args.PropertyName == nameof(filterView.IHObjekt_Id)) {

                treeView.SetCurrent(filterView.IHObjekt_Id);
                await DetailAsync();
            }

            if (sender == detailView && args.PropertyName == nameof(detailView.Current) && detailView.Tab == IHObjektDetailViewModel.Tabs.Historie) await ExecuteAsync(_ => UpdateEreignisAsync());
            if (sender == detailView && args.PropertyName == nameof(detailView.Tab) && detailView.Tab == IHObjektDetailViewModel.Tabs.Historie) await ExecuteAsync(_ => UpdateEreignisAsync());
            if (sender == detailView && args.PropertyName == nameof(detailView.Tab)) NeuSetzen();
            if (sender == ereignisView && args.PropertyName == nameof(ereignisView.WithSub)) await ExecuteAsync(_ => UpdateEreignisAsync());
            if (sender == artikelView && args.PropertyName == nameof(artikelView.CurrentArtikel)) UpdateCommands();
        }


        private async Task SaveAsync()
        {

            if (detailView.Current == null) return;
            var oldmodel = detailView.Current;

            if (detailView.Current.IsChanged || docs.EntityIsChanged) {

                if (detailView.Current.ErrorList.Count > 0) {

                    await messages.ShowMessageAsync(new MessageToken { Header = "Das Asset kann nicht gespeichert werden.", Reasons = detailView.Current.ErrorList });
                    return;
                }

                using (new VisualDelay(view)) {

                    var model = await store.SaveAsync(detailView.Current, bus.Token);

                    //Bei Veränderungen die den Baum betreffen diesen neu aufbauen (NotiyPropertyChanged reicht nicht wenn IHO verschoben wird)
                    if (oldmodel == null || oldmodel.Nummer != model.Nummer || oldmodel.Kostenstelle.Id != model.Kostenstelle.Id) {

                        SetModel(null);
                        await LoadAsync();
                    }

                    SetModel(model);

                    docs.UpdateEntityId(model);
                    await docs.SaveAsync();
                    if (module.Info.VerwendetModulWPlan) await massnahmen.SaveAsync();

                    await docs.DetailAsync(model).ConfigureAwait(false);
                    var titelbild = await docs.TitelbildAsync().ConfigureAwait(false);
                    baseView.Thumbnail = titelbild?.Thumbnail;
                    if (detailView.Tab == IHObjektDetailViewModel.Tabs.Historie) await UpdateEreignisAsync();
                    UpdateCommands();
                    await docs.FillThumbnailsAsync();
                }
            }

            SetEditMode(EditMode.ReadOnly);
        }


        private Task EditAsync()
        {

            SetEditMode(EditMode.Edit);
            return Task.FromResult(true);
        }


        private async Task AddAsync(CopyMode mode, IHObjekt model = null)
        {

            switch (mode) {

            case CopyMode.Copy :

                if (detailView.Current == null) return;
                model = detailView.Current.ErstelleGeschwister();
                break;

            case CopyMode.AddSub :

                if (detailView.Current == null) return;
                model = detailView.Current.ErstelleTeilobjekt();
                break;

            case CopyMode.Add :

                model = new IHObjekt { Standort = treeView.CurrentStandort, Kostenstelle = treeView.CurrentKostenstelle };
                break;
            }

            detailView.ResetTabs();
            SetModel(model);
            await docs.DetailAsync(model);
            SetEditMode(EditMode.Append);
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current == null) return;

            using (new VisualDelay(view)) {

                var reasons = await store.DeleteKonsequenzenAsync(detailView.Current, bus.Token).ConfigureAwait(false);

                var header = reasons.IsDeleteable? String.Format("Möchten Sie das Asset {0} wirklich löschen?", detailView.Current.Nummer):
                                                   String.Format("Das Asset {0} kann nicht gelöscht werden.", detailView.Current.Nummer);
                if (!await messages.ConfirmDeleteAsync(new DeleteToken { Header = header, IsDeleteable = reasons.IsDeleteable, Reasons = reasons.Reasons })) return;

                await store.DeleteAsync(detailView.Current, bus.Token).ConfigureAwait(false);

                treeView.Unload();
                SetModel(null);
                SetEditMode(EditMode.ReadOnly);
                await LoadAsync();
            }
        }


        private async Task VerwerfenAsync()
        {

            await DetailAsync();
            SetEditMode(EditMode.ReadOnly);
        }


        private async Task DeaktivierenAsync()
        {

            if (detailView.Current == null || detailView.Current.IstAngefügt) return;
            deactivateView.IHObjekt = detailView.Current;
            deactivateView.DeactivateCommand = flyouts.YesCommand;
            deactivateView.Datum = DateTime.Today;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = deactivateView.View, Position = MahApps.Metro.Controls.Position.Bottom, IsPinned = true })) return;

            detailView.Current.DeaktiviertAm = deactivateView.Datum;
            detailView.Current.IstAktiv = false;

            using (new VisualDelay(view)) {

                await SaveAsync();
                await LoadAsync();
            }
        }


        private async Task AddArtikelAsync()
        {

            if (detailView.Current == null || CurrentMode == EditMode.ReadOnly) return;
            artikelAddView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = artikelAddView.View, Header = "Artikel anfügen", Position = MahApps.Metro.Controls.Position.Right }))
                return;

            var mtlist = artikelAddView.GetSelected();
            if (mtlist.IsNullOrEmpty()) return;

            var oldIds = artikelView.MaterialIHObjektMap.Select(p => p.ArtikelId).ToHashSet();
            await OnUIThreadAsync(() => {

                artikelView.MaterialIHObjektMap.AddRange(mtlist.Where(p => !oldIds.Contains(p.Id)).Select(p => new ArtikelSelectModel(p)));
            });
        }


        private async Task DeleteArtikelAsync()
        {

            if (CurrentMode == EditMode.ReadOnly || !HasCurrent || artikelView.CurrentArtikel == null) return;
            await OnUIThreadAsync(() => { artikelView.MaterialIHObjektMap.Remove(artikelView.CurrentArtikel); });
        }


        private bool HasCurrent { get { return detailView.Current != null; } }


        public EditMode CurrentMode { get; private set; }


        private void SetEditMode(EditMode newMode)
        {

            CurrentMode = newMode;
            docs.SetEditMode(newMode);
            if (module.Info.VerwendetModulWPlan) massnahmen.SetEditMode(newMode);

            UpdateCommands();
            NeuSetzen();
        }


        private void NeuSetzen()
        {

            view.SaveVisible                = CurrentMode != EditMode.ReadOnly;
            view.EditVisible                = CurrentMode == EditMode.ReadOnly;
            view.CancelVisible              = CurrentMode != EditMode.ReadOnly;
            view.ReloadVisible              = CurrentMode == EditMode.ReadOnly;
            view.AddVisible                 = CurrentMode == EditMode.ReadOnly && detailView.Tab == IHObjektDetailViewModel.Tabs.Basis;
            view.AddSubVisible              = CurrentMode == EditMode.ReadOnly && detailView.Tab == IHObjektDetailViewModel.Tabs.Basis;
            view.CopyVisible                = CurrentMode == EditMode.ReadOnly && detailView.Tab == IHObjektDetailViewModel.Tabs.Basis;
            view.DeleteVisible              = CurrentMode != EditMode.ReadOnly && detailView.Tab == IHObjektDetailViewModel.Tabs.Basis &&
                                              (login.AktuellerNutzer.IstAssetLöscher || login.AktuellerNutzer.IstAdministrator);
            view.DeactivateVisible          = CurrentMode == EditMode.Edit && detailView.Tab == IHObjektDetailViewModel.Tabs.Basis;
            view.OpenMassnahmeVisible       = CurrentMode == EditMode.ReadOnly && module.Info.VerwendetModulWPlan && view.OpenMassnahmeCommand != null && detailView.Tab == IHObjektDetailViewModel.Tabs.Massnahmen;
            view.AddMassnahmeVisible        = CurrentMode == EditMode.ReadOnly && module.Info.VerwendetModulWPlan && view.AddMassnahmeCommand != null && detailView.Tab == IHObjektDetailViewModel.Tabs.Massnahmen;
            view.OpenAuftragVisible         = CurrentMode == EditMode.ReadOnly && module.Info.VerwendetModulAuftrag && view.OpenAuftragCommand != null  && detailView.Tab == IHObjektDetailViewModel.Tabs.Wartung;
            view.CreateAufträgeVisible      = CurrentMode == EditMode.ReadOnly && module.Info.VerwendetModulWPlan && view.CreateAufträgeCommand != null && detailView.Tab == IHObjektDetailViewModel.Tabs.Wartung;
            view.DeleteArtikelVisible       = CurrentMode != EditMode.ReadOnly && module.Info.VerwendetModulStock && detailView.Tab == IHObjektDetailViewModel.Tabs.Artikel;
            view.AddArtikelVisible          = CurrentMode != EditMode.ReadOnly && module.Info.VerwendetModulStock && detailView.Tab == IHObjektDetailViewModel.Tabs.Artikel;


            filterView.EditEnabled          = CurrentMode == EditMode.ReadOnly;
            treeView.EditEnabled            = CurrentMode == EditMode.ReadOnly;
            detailView.EditEnabled          = CurrentMode != EditMode.ReadOnly;
            baseView.EditEnabled            = CurrentMode != EditMode.ReadOnly;
            memoView.EditEnabled            = CurrentMode != EditMode.ReadOnly;
            specView.InEditMode             = CurrentMode != EditMode.ReadOnly;
            artikelView.InEditMode          = CurrentMode != EditMode.ReadOnly;
            personalView.InEditMode         = CurrentMode != EditMode.ReadOnly;
        }


        private async Task UpdateEreignisAsync()
        {

            if (detailView.Current == null || detailView.Current.IstAngefügt) {

                ereignisView.ViewerVisible = false;
                return;
            }

            var address = "Comain.Client.Reports.IHEreignisReport.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = "Ereignisse " + detailView.Current.Nummer };
            var results = await store.EreignisseAsync(detailView.Current.Id, ereignisView.WithSub, bus.Token).ConfigureAwait(false);

            report.AddDataSource("MainData", results);
            report.AddParameter("parTitle", detailView.Current.Nummer);
            report.AddParameter("parMaschine", detailView.Current.Name);
            if (printView.DruckVollständig) report.AddParameter("parSub", ereignisView.WithSub? "mit untergeordneten Baugruppen / Bauteile": "");
            await ereignisView.BindViewerAsync(report);
        }


        private async Task MoveRequestAsync(object dropped = null)
        {

            if (detailView.Current == null) return;
            detailView.ResetTabsIfPrint();

            using (new VisualDelay(moveView)) {

                await flyouts.OpenAsync(new FlyoutConfig { ChildView = moveView.View, Header = $"Assetumzug",
                                                           Position = MahApps.Metro.Controls.Position.Right });
                switch (dropped) {

                    case TreeIHViewItem ih:
                        await moveView.SetRequestAsync(detailView.Current, MoveDestinationType.IHObjekt, ih.FilterModel.Id);
                        break;

                    case TreePZViewItem pz:
                        await moveView.SetRequestAsync(detailView.Current, MoveDestinationType.Standort, pz.Model.Id);
                        break;

                    case TreeKSTViewItem ks:
                        await moveView.SetRequestAsync(detailView.Current, MoveDestinationType.Kostenstelle, ks.Model.Id);
                        break;

                    default:
                        await moveView.SetRequestAsync(detailView.Current);
                        break;
                }
            }
        }


        private async Task MoveAsync()
        {

            using (new VisualDelay(moveView)) {

                var instruction = moveView.GetInstruction();
                await store.MoveAsync(instruction, bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();
            }

            await flyouts.CloseAsync();
            await LoadAsync();
        }


        private async Task SelfServiceAsync()
        {

            var mnlist = massnahmen.GebeMassnahmen();
            if (mnlist.IsNullOrEmpty()) return;


            using (new VisualDelay(view)) {

                await store.SelfServiceAsync(mnlist.Where(p => p.Id % 2 == 0).Select(p => p.Id)).ConfigureAwait(false);
            }
        }
    }
}
