﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CO.OpenXML
{
    
    /// <summary>
    /// Dateien des Archives halten (Support für Binärdateien)
    /// </summary>
    internal class FileList : IEnumerable<KeyValuePair<string, byte[]>> 
    {
    
        Dictionary<String, byte[]> myFiles;
        
        /// <summary>
        /// Verzeichnis einrichten
        /// </summary>
        public FileList()
        {
        
            myFiles = new Dictionary<string,byte[]>();
        }
    
        /// <summary>
        /// Datei als bytearray (Binärsupport) geben
        /// </summary>
        /// <param name="key">Dateiname im Verzeichnis</param>
        /// <returns>Dateiinhalt</returns>
        public byte[] GetBytes(String key)
        {
        
            return myFiles[key];
        }
    
        /// <summary>
        /// Datei als bytearray (Binärsupport) schreiben
        /// </summary>
        /// <param name="key">Dateiname im Verzeichnis</param>
        /// <param name="content">Dateiinhalt</param>
        public void SetBytes(String key, byte[] content)
        {
        
            myFiles[key] = content;
        }
    
        /// <summary>
        /// Datei als String geben
        /// </summary>
        /// <param name="key">Dateiname im Verzeichnis</param>
        /// <returns>Dateiinhalt</returns>
        public String GetString(String key)
        {
        
            UTF8Encoding enc = new UTF8Encoding();
            return enc.GetString(myFiles[key]);
        }
    
        /// <summary>
        /// Datei als String schreiben
        /// </summary>
        /// <param name="key">Dateiname im Verzeichnis</param>
        /// <param name="content">Dateiinhalt</param>
        public void SetString(String key, String content)
        {
        
            UTF8Encoding enc = new UTF8Encoding();
            myFiles[key] = enc.GetBytes(content);
        }
        
        /// <summary>
        /// vorhandene Dateien des Verzeichnisses listen
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> Keys()
        {
            return myFiles.Keys;
        }
        
        /// <summary>
        /// Prüfen ob Datei im Verzeichnis vorhanden ist 
        /// </summary>
        /// <param name="key">Name der Datei</param>
        /// <returns>true: vorhanden</returns>
        public bool ContainsKey(String key)
        {
            return myFiles.ContainsKey(key);
        }
    
        /// <summary>
        /// Verzeichniseinträge geben
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<string,byte[]>>  GetEnumerator()
        {
 	        return myFiles.GetEnumerator();
        }

        /// <summary>
        /// Verzeichniseinträge geben
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
 	        return myFiles.GetEnumerator();
        }
    }
}
