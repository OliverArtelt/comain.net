﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class FbDto : IComparable<FbDto>
    {

        public String    FbNummer        { get; set; }
        public int?      IntAuStelle     { get; set; }
        public String    Kostenstelle    { get; set; }
        public decimal   Eigenkosten     { get; set; }
        public decimal   Materialkosten  { get; set; }
        public decimal   Fremdkosten     { get; set; }

        public decimal Gesamtkosten { get { return Eigenkosten + Materialkosten + Fremdkosten; } }


        public int CompareTo(FbDto other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.FbNummer.CompareNatural(other.FbNummer);
        }
    }
}
