﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PreistabelleExport : IPlanExport
    {

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Preistabelle";
        public int      Sortierung              => 600;

        public bool     UseFilterAusgabe        => true;
        public bool     UseFilterLeistungstyp   => true;
        public bool     UseFilterErgebnis       => true;

        public IExportVisitor Exporter      => new PreistabelleWriter();


        public String   Text                => @"
Verwenden Sie die Preistabelle zur Anzeige geplanter und verbrauchter Kosten in einer kalenderartigen Übersicht. In der Preistabelle sehen Sie Kosten und Status sowohl für geplante als auch für offene Maßnahmen. Legen Sie vor dem Export nach Excel fest, was ausgegeben werden soll.
Wählen Sie die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein, Legen Sie die Berichtsparameter fest und exportieren Sie die Daten mit der Schaltfläche ""Preistabelle"" nach Excel. 
Bitte beachten Sie, es werden nur Maßnahmen ausgewertet, die im gewählten Zeitraum geplant oder erledigt wurden. Geben Sie die auszuwertenden Normzeiten und Preise in den Maßnahmedetails ein.";
    }
}
