﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class VfGesamtDto
    {

        public String       IHObjekt            { get; set; }
        public String       Maschine            { get; set; }
        public decimal?     Belegungszeit       { get; set; }
        public decimal?     Ausfallzeit         { get; set; }
        public decimal?     Störzeit            { get; set; }
        public int?         IHStatus            { get; set; }
        public decimal?     Verfügbarkeit       { get; set; }
        public decimal?     Ausfallrate         { get; set; }
        public decimal?     Abweichungsgrad     { get; set; }
    }
}
