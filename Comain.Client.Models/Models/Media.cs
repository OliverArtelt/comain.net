﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Comain.Client.Dtos;


namespace Comain.Client.Models
{

    public class Media : Entity
    {

        private const int    thumbnailWidth = 150;
        private const String defaultFileName = @"Bild.jpg";
        private const String conversionFailedMessage = "Das Bild kann nicht geladen werden da das Dateiformat nicht unterstützt wird oder das Bild beschädigt ist.";

        private String          dateiname;
        private BitmapSource    image;


        public String           Mimetype            { get => @"image/jpeg"; }
        public BitmapSource     Thumbnail           { get; set; }


        public String Dateiname
        {
            get { return dateiname; }
            set { SetProperty(ref dateiname, value); }
        }


        public BitmapSource Image
        {
            get { return image; }
            set { SetProperty(ref image, value); }
        }


        public bool ImageVisible { get => Image != null; }


        public String Name => Path.GetFileNameWithoutExtension(Dateiname);


        public String TempPath
        {
            get {

                if (String.IsNullOrEmpty(Dateiname)) return Path.Combine(Path.GetTempPath(), defaultFileName);
                return Path.Combine(Path.GetTempPath(), Dateiname);
            }
        }


        public Media()
        {}


        public Media(String path)
        {

            try {

                Dateiname = Path.GetFileName(path);

                var image = new BitmapImage();
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = new Uri(path);
                image.EndInit();
                image.Freeze();
                Image = image;
                RaisePropertyChanged(nameof(Image));

                var thumbnail = new BitmapImage();
                thumbnail.BeginInit();
                thumbnail.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                thumbnail.CacheOption = BitmapCacheOption.OnLoad;
                thumbnail.UriSource = new Uri(path);
                thumbnail.DecodePixelWidth = thumbnailWidth;
                thumbnail.EndInit();
                thumbnail.Freeze();
                Thumbnail = thumbnail;
                RaisePropertyChanged(nameof(Thumbnail));
            }
            catch (NotSupportedException x) {

                throw new ComainOperationException(conversionFailedMessage, x);
            }
        }


        public Media(Bitmap snapshot)
        {
            LoadFromBitmap(snapshot);
        }


        public Media(MediaDto dto, int? maxSize = null)
            : this(dto.Image, dto.Dateiname, dto.Id, maxSize)
        {}


        public Media(byte[] buffer, String filename, int id = 0, int? maxSize = null)
        {

            Id = id;
            Dateiname = filename;

            if (buffer.IsNullOrEmpty()) return;

            try {

                using (var stream = new MemoryStream(buffer)) {

                    Image image = System.Drawing.Image.FromStream(stream);
                    var bitmap = new Bitmap(image);
                    LoadFromBitmap(bitmap, maxSize);
                }
            }
            catch (NotSupportedException x) {

                throw new ComainOperationException(conversionFailedMessage, x);
            }
        }


        public void LoadFromBitmap(Bitmap snapshot, int? maxSize = null)
        {

            try {

                using (var stream = new MemoryStream()) {

                    snapshot.Save(stream, ImageFormat.Bmp);

                    stream.Position = 0;

                    var image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = stream;
                    image.EndInit();
                    image.Freeze();
                    Image = image;
                    RaisePropertyChanged(nameof(Image));

                    stream.Position = 0;

                    var thumbnail = new BitmapImage();
                    thumbnail.BeginInit();
                    thumbnail.CacheOption = BitmapCacheOption.OnLoad;
                    thumbnail.StreamSource = stream;
                    thumbnail.DecodePixelWidth = maxSize ?? thumbnailWidth;
                    thumbnail.EndInit();
                    thumbnail.Freeze();
                    Thumbnail = thumbnail;
                    RaisePropertyChanged(nameof(Thumbnail));
                }
            }
            catch (NotSupportedException x) {

                throw new ComainOperationException(conversionFailedMessage, x);
            }
        }


        public MediaDto AsDto()
        {

            var dto = new MediaDto();
            dto.Id = Id;
            dto.Dateiname = Dateiname;
            dto.Mimetype = Mimetype;
            dto.ChangedProps = ChangedProps?.ToHashSet();

            using (var stream = new MemoryStream()) {

                BitmapEncoder encoder = new JpegBitmapEncoder();
                if (Image != null) {

                    encoder.Frames.Add(BitmapFrame.Create(Image));
                    encoder.Save(stream);
                    dto.Image = stream.ToArray();
                }
            }

            return dto;
        }


        public static MediaDto AsDto(Media model)
        {

            var dto = new MediaDto();
            dto.Dateiname = model.Dateiname;
            dto.Mimetype = model.Mimetype;

            using (var stream = new MemoryStream()) {

                BitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(model.Image));
                encoder.Save(stream);
                dto.Image = stream.ToArray();
            }

            return dto;
        }


        public Task StoreFileAsync(String path, CancellationToken ct = default)
        {

            if (Image == null) throw new ComainOperationException("Das Bild wurde nicht gefunden.");

            return Task.Run(() => {

                using (var stream = new FileStream(path, FileMode.Create)) {

                    BitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(Image));
                    encoder.Save(stream);
                }
            });
        }


        public Task StoreTempFileAsync(CancellationToken ct = default)
            => StoreFileAsync(TempPath, ct);
    }
}

