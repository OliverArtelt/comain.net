﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class LagerortRow
    {
        
        public  String   Nummer         { get; set; }
        public  decimal? Restmenge      { get; set; }
    }
}
