﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class LbKstDto : IComparable<LbKstDto>
    {

        public String    Kostenstelle    { get; set; }
        public decimal   Eigenkosten     { get; set; }
        public decimal   Materialkosten  { get; set; }
        public decimal   Fremdkosten     { get; set; }
        public decimal   Mehrwertsteuer  { get; set; }
                       
        public decimal Gesamtkosten { get { return Eigenkosten + Materialkosten + Fremdkosten; } }


        public int CompareTo(LbKstDto other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Kostenstelle.CompareNatural(other.Kostenstelle);
        }
    }
}
