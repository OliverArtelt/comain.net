﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Commands;
using Comain.Client.Views;


namespace Comain.Client.ViewModels
{
   
    public class HomeViewModel : ViewModel<IHomeView>
    {

        public HomeViewModel(IHomeView view) : base(view)
        {
        }


        public void SetCommands(IEnumerable<IControllerCommand> actions)
        {

            Gruppen = new List<CommandGroup>();
            
            //zuerst benannte Standardmodule
            var lst = actions.Where(p => p.Gruppe == "Aufträge").OrderBy(p => p.Sortierung).ToList();
            if (!lst.IsNullOrEmpty()) Gruppen.Add(new CommandGroup { Name = "Aufträge", Commands = lst });
            lst = actions.Where(p => p.Gruppe == "Assets").OrderBy(p => p.Sortierung).ToList();
            if (!lst.IsNullOrEmpty()) Gruppen.Add(new CommandGroup { Name = "Assets", Commands = lst });

            //dann Zusatzmodule
            var stds = new String[] { "Aufträge", "Assets", "Verwaltung", "Auswertungen" }; 
            var zgrps = actions.Where(p => stds.All(q => q != p.Gruppe))
                               .OrderBy(p => p.Gruppe).ThenBy(p => p.Sortierung)
                               .GroupBy(p => p.Gruppe);
            Gruppen.AddRange(zgrps.Select(p => new CommandGroup { Name = p.Key, Commands = p.ToList() }));

            //zuletzt Verwaltung / Reporting 
            lst = actions.Where(p => p.Gruppe == "Verwaltung").OrderBy(p => p.Sortierung).ToList();
            if (!lst.IsNullOrEmpty()) Gruppen.Add(new CommandGroup { Name = "Verwaltung", Commands = lst });
            lst = actions.Where(p => p.Gruppe == "Auswertungen").OrderBy(p => p.Sortierung).ToList();
            if (!lst.IsNullOrEmpty()) Gruppen.Add(new CommandGroup { Name = "Auswertungen", Commands = lst });

            RaisePropertyChanged("Gruppen");
        }


#region P R O P E R T I E S


        public IList<CommandGroup> Gruppen { get; private set; }

 

        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }
 

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private IEnumerable actions;
        public IEnumerable Actions
        { 
            get { return actions; }
            set { SetProperty(ref actions, value); }
        }


        private ICommand actionCommand;
        public ICommand ActionCommand
        { 
            get { return actionCommand; }
            set { SetProperty(ref actionCommand, value); }
        }


#endregion

    }
}
