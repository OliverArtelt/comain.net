﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellEditPosViewModel : ViewModel<IBestellEditPosView>
    {
 
        private Bestellung current;
         

        public BestellEditPosViewModel(IBestellEditPosView view) : base(view)
        {
        }


        public void SetData(Bestellung bs, IList<Artikel> artikelliste)
        {

            current = bs;
            Artikelliste = new CollectionViewSource { Source = artikelliste.Select(at => new ArtikelBSEditPickItem(at, bs)).OrderBy(p => p.Nummer).ToList() }.View;
            Artikelliste.Filter = p => {

                var at = p as ArtikelBSEditPickItem;
                if (at == null) return false;
               // if (current.Positionen.Any(bspos => bspos.Material_Id == at.Model.Id)) return false;
                if (NurLieferant && !at.HatLieferant) return false;
                if (NurBestellwürdige && !at.IstBestellwürdig) return false;
                if (NurSelektierte && !at.IsSelected) return false;
                return true;
            };        
            Artikelliste.SortDescriptions.Add(new SortDescription { PropertyName = "Nummer", Direction = ListSortDirection.Ascending });
            Refresh();
        }


        private void Refresh()
        {

            if (Artikelliste == null) return;
            Artikelliste?.Refresh();        
            RaisePropertyChanged("Artikelliste");
        }


#region P R O P E R T I E S
        
        
        public ICollectionView Artikelliste   { get; private set; }
        

        private bool nurLieferant = true;
        public bool NurLieferant
        { 
            get { return nurLieferant; }
            set { if (SetProperty(ref nurLieferant, value)) Refresh(); }
        }
 
        
        private bool nurBestellwürdige;
        public bool NurBestellwürdige
        { 
            get { return nurBestellwürdige; }
            set { if (SetProperty(ref nurBestellwürdige, value)) Refresh(); }
        }
 
        
        private bool nurSelektierte;
        public bool NurSelektierte
        { 
            get { return nurSelektierte; }
            set { if (SetProperty(ref nurSelektierte, value)) Refresh(); }
        }


#endregion

    }
}
