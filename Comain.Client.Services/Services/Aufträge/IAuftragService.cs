﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Aufträge
{

    public interface IAuftragService
    {

        Task<IList<AuftragListDto>>                     AufträgeAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<Auftrag>                                   DetailAsync(int id, CancellationToken ct = default);
        Task<String>                                    IHPathAsync(String ihnummer, CancellationToken ct = default);

        Task<Auftrag>                                   CreateAsync(NeuerAuftragDto dto, CancellationToken ct = default);
        Task<Auftrag>                                   SaveAsync(Auftrag model, CancellationToken ct = default);
        Task                                            DeleteAsync(Auftrag model, CancellationToken ct = default);
        Task<Auftrag>                                   StornoAsync(Auftrag model, CancellationToken ct = default);
        Task<Auftrag>                                   CloseAsync(Auftrag model, CancellationToken ct = default);
        Task<Auftrag>                                   ReopenAsync(int auftrag, String reason, CancellationToken ct = default);
        Task<Auftrag>                                   NeuerUnterauftragAsync(int mainId, CancellationToken ct = default);

        Task<IList<PzFilterModel>>                      FilterStandorteAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>                  FilterGewerkeAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>                FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<IHObjektFilterModel>>                FilterIHObjekteAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>                     FilterKostenstellenAsync(CancellationToken ct = default);
        Task<IList<LeistungsartFilterModel>>            FilterLeistungsartenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>                  FilterSchadensbilderAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>                  FilterSchadensursachenAsync(CancellationToken ct = default);
        Task<IList<SubNummerFilterModel>>               FilterSubSchadensbilderAsync(CancellationToken ct = default);
        Task<IList<SubNummerFilterModel>>               FilterSubSchadensursachenAsync(CancellationToken ct = default);
        Task<IList<AuIHKSDto>>                          FilterAuIHKSAsync(CancellationToken ct = default);
        Task<IList<int>>                                FilterInterneAuStelleAsync(CancellationToken ct = default);
        Task<IDringlichkeitenProvider>                  FilterDringlichkeitAsync(CancellationToken ct = default);
    }
}
