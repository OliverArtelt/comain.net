﻿using System;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;

namespace Comain.Client.ViewModels.Stock.Wareneingänge
{

    public class BestellposSelektorItem : NotifyBase
    {

        private readonly EingangBestellposDto dto;


        public BestellposSelektorItem(EingangBestellposDto dto)
        {
            
            this.dto = dto;
            Nummer = new NummerUndPosition { Nummer = dto.Nummer, Position = dto.Position };
        }


        public Wareneingang AsModel()
        {

            var we = new Wareneingang();

            we.Material = new Artikel { Name = Artikelname, Nummer = Artikelnummer };
            we.Bestellposition = new Bestellposition { Bestellung = new Bestellung(), Menge = dto.Bestellmenge, Id = dto.Id };
            we.Liefermenge = MengeOffen;
            we.MengeGeprüft = false;
            we.QualitätGeprüft = false;

            return we;
        }


        private bool isSelected;
        public bool IsSelected
        { 
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }


        public NummerUndPosition    Nummer              { get; private set; }


        public int                  Id                  { get { return dto.Id; } }  
        public decimal              Bestellmenge        { get { return dto.Bestellmenge; } }  
        public decimal              MengeOffen          { get { return Math.Max(0.0m, dto.Bestellmenge - dto.MengeEingang); } }  
        public String               Auftrag             { get { return dto.Auftrag; } }  
        public String               Artikelnummer       { get { return dto.Artikelnummer; } }  
        public String               Artikelname         { get { return dto.Artikelname; } }
    }
}
