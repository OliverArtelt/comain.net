﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Administration
{

    public class AuftragViewModel : ValidatableViewModel<IAuftragView, Konfiguration>, IConfigViewModel
    {

        private readonly SynchronizationContext context;


        public AuftragViewModel(IAuftragView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }
  

        public override void CurrentChanged(Konfiguration oldSelected)
        {
            
            RaisePropertyChanged(nameof(AuftragsdruckMitPreisen));
            RaisePropertyChanged(nameof(AuftragsdruckMitMWS));
            RaisePropertyChanged(nameof(AuftraggeberImAuftragPflicht));
            RaisePropertyChanged(nameof(HandlingszuschlagMaterial));
            RaisePropertyChanged(nameof(InterneAuStelleImAuftragPflicht));
            RaisePropertyChanged(nameof(StandardInterneAuStelle));
            RaisePropertyChanged(nameof(StandardTageFertigstellung));
            RaisePropertyChanged(nameof(Abrechnungsschluß));
            RaisePropertyChanged(nameof(StandardLeistungsartPlanmäßig));
            RaisePropertyChanged(nameof(StandardLeistungsartUnplanmäßig));
            RaisePropertyChanged(nameof(LeistungsartMOC));
            RaisePropertyChanged(nameof(DuplikatprüfungPersonal));
        }


        public void Unload()
        {
        
            Leistungsarten = null;
        }


        public void SetzeLeistungsarten(IEnumerable<LeistungsartFilterModel> list)
        {

            context.Send(new SendOrPostCallback((o) => { 

                Leistungsarten = list.OrderBy(p => p.Nummer).ToList();

            }), null);
        }


#region P R O P E R T I E S
   

        public bool AuftragsdruckMitPreisen
        { 
            get { return Current == null? false: Current.AuftragsdruckMitPreisen; }
            set { 
                
                if (Current != null && Current.AuftragsdruckMitPreisen != value) {

                    Current.AuftragsdruckMitPreisen = value;
                    RaisePropertyChanged("AuftragsdruckMitPreisen");
                }
            }
        }


        public bool AuftragsdruckMitMWS
        { 
            get { return Current == null? false: Current.AuftragsdruckMitMWS; }
            set { 
                
                if (Current != null && Current.AuftragsdruckMitMWS != value) {

                    Current.AuftragsdruckMitMWS = value;
                    RaisePropertyChanged("AuftragsdruckMitMWS");
                }
            }
        }


        public bool AuftraggeberImAuftragPflicht
        { 
            get { return Current == null? false: Current.AuftraggeberImAuftragPflicht; }
            set { 
                
                if (Current != null && Current.AuftraggeberImAuftragPflicht != value) {

                    Current.AuftraggeberImAuftragPflicht = value;
                    RaisePropertyChanged("AuftraggeberImAuftragPflicht");
                }
            }
        }


        public bool InterneAuStelleImAuftragPflicht
        { 
            get { return Current == null? false: Current.InterneAuStelleImAuftragPflicht; }
            set { 
                
                if (Current != null && Current.InterneAuStelleImAuftragPflicht != value) {

                    Current.InterneAuStelleImAuftragPflicht = value;
                    RaisePropertyChanged("InterneAuStelleImAuftragPflicht");
                }
            }
        }


        public bool DuplikatprüfungPersonal
        { 
            get { return Current == null? false: Current.DuplikatprüfungPersonal; }
            set { 
                
                if (Current != null && Current.DuplikatprüfungPersonal != value) {

                    Current.DuplikatprüfungPersonal = value;
                    RaisePropertyChanged(nameof(DuplikatprüfungPersonal));
                }
            }
        }


        public decimal? HandlingszuschlagMaterial
        { 
            get { return Current == null? null: Current.HandlingszuschlagMaterial; }
            set { 
                
                if (Current != null && Current.HandlingszuschlagMaterial != value) {

                    Current.HandlingszuschlagMaterial = value;
                    RaisePropertyChanged("HandlingszuschlagMaterial");
                }
            }
        }


        public int? StandardInterneAuStelle
        { 
            get { return Current == null? null: Current.StandardInterneAuStelle; }
            set { 
                
                if (Current != null && Current.StandardInterneAuStelle != value) {

                    Current.StandardInterneAuStelle = value;
                    RaisePropertyChanged("StandardInterneAuStelle");
                }
            }
        }


        public int StandardTageFertigstellung
        { 
            get { return Current == null? 7: Current.StandardTageFertigstellung; }
            set { 
                
                if (Current != null && Current.StandardTageFertigstellung != value) {

                    Current.StandardTageFertigstellung = value;
                    RaisePropertyChanged("StandardTageFertigstellung");
                }
            }
        }


        public DateTime? Abrechnungsschluß
        { 
            get { return Current == null? null: Current.Abrechnungsschluß; }
            set { 
                
                if (Current != null && Current.Abrechnungsschluß != value) {

                    Current.Abrechnungsschluß = value;
                    RaisePropertyChanged("Abrechnungsschluß");
                }
            }
        }


        private IEnumerable<LeistungsartFilterModel> leistungsarten;
        public IEnumerable<LeistungsartFilterModel> Leistungsarten
        { 
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }


        public LeistungsartFilterModel StandardLeistungsartPlanmäßig
        { 
            get { return Current == null || Leistungsarten.IsNullOrEmpty()? null: Leistungsarten.FirstOrDefault(p => p.Nummer == Current.StandardLeistungsartPlanmäßig); }
            set { 
                
                if (Current != null && value != null && value.Nummer != Current.StandardLeistungsartPlanmäßig) {

                    Current.StandardLeistungsartPlanmäßig = value.Nummer;
                    RaisePropertyChanged("StandardLeistungsartPlanmäßig");
                }
            }
        }


        public LeistungsartFilterModel StandardLeistungsartUnplanmäßig
        { 
            get { return Current == null || Leistungsarten.IsNullOrEmpty()? null: Leistungsarten.FirstOrDefault(p => p.Nummer == Current.StandardLeistungsartUnplanmäßig); }
            set { 
                
                if (Current != null && value != null && value.Nummer != Current.StandardLeistungsartUnplanmäßig) {

                    Current.StandardLeistungsartUnplanmäßig = value.Nummer;
                    RaisePropertyChanged("StandardLeistungsartUnplanmäßig");
                }
            }
        }


        public LeistungsartFilterModel LeistungsartMOC
        { 
            get { return Current == null || Leistungsarten.IsNullOrEmpty()? null: Leistungsarten.FirstOrDefault(p => p.Nummer == Current.LeistungsartMOC); }
            set { 
                
                if (Current != null && value != null && value.Nummer != Current.LeistungsartMOC) {

                    Current.LeistungsartMOC = value.Nummer;
                    RaisePropertyChanged("LeistungsartMOC");
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion
    
    }
}
