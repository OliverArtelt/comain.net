﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan.Reports;

namespace Comain.Client.ViewData.WPlan
{
   
    public class BildReportItem
    {

        private const int maxLength = 500;

        public String       Positionsname   { get; set; }
        public String       Image           { get; set; }
        public String       Name            { get; set; }


        public BildReportItem(PositionDruckDto pos, BildDruckDto bild)
        {

            Positionsname = $"Position {pos.Name}";
            Name = bild.Dateiname;

           // debug output:
           // Image = System.Convert.ToBase64String(bild.Image, 0, bild.Image.Length);
            
            using (var istream = new MemoryStream(bild.Image))
            using (var ostream = new MemoryStream()) {

                Image image = Resize(System.Drawing.Image.FromStream(istream));
                var bitmap = new Bitmap(image);
                bitmap.Save(ostream, ImageFormat.Jpeg);

                ostream.Seek(0, SeekOrigin.Begin);
                Byte[] buffer = ostream.ToArray();
                Image = System.Convert.ToBase64String(buffer, 0, buffer.Length);
            }            
        }


        private Image Resize(Image original)
        {

            if (original.Width <= maxLength && original.Height <= maxLength) return original;
            
            float percentWidth = (float)maxLength / (float)original.Width;
            float percentHeight = (float)maxLength / (float)original.Height;
            float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
            int newWidth = (int)(original.Width * percent);
            int newHeight = (int)(original.Height * percent);

            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage)) {

                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(original, 0, 0, newWidth, newHeight);
            }

            return newImage;        
        }
    }
}
