﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Models
{

    public class Konfiguration : NotifyBase, IDataErrorInfo
    {

        private bool                    entnahmeschein;
        private bool                    lagerverwaltung;
        private int?                    auftragsnummerJahresziffern;
        private DateTime?               abrechnungsschluß;
        private bool                    parameterverwaltung;
        private bool                    automatischeFabriknummern;
        private bool                    fabriknummernPflicht;
        private bool                    inventarnummernPflicht;
        private int?                    iHObjektBarcodetyp;
        private bool                    iHObjektAuftragMitLöschen;
        private bool                    iHObjektTrotzAuftragLöschen;
        private bool                    iHObjektTrotzLeistungenLöschen;
        private bool                    iHObjektTrotzProjekteLöschen;
        private bool                    iHObjektObjektgruppeÄnderbar;
        private int?                    maximaleBauteilTiefe;
        private IHObjektFarbtyp?        iHObjektFarbtyp;
        private IHNummerAnzeigetyp?     iHNummerAnzeigetyp;
        private int?                    iHObjektBauteilZiffern;
        private int?                    iHObjektObjektartZiffern;
        private int?                    iHObjektObjektgruppeZiffern;
        private int?                    iHObjektMaschineZiffern;
        private int?                    iHObjektStandortZiffern;
        private String                  kunde;
        private int?                    rundungstyp;
        private String                  datenbankversion;
        private int?                    personalSortiertyp;
        private bool                    auftragsdruckMitPreisen;
        private bool                    auftragsdruckMitMWS;
        private bool                    auftraggeberImAuftragPflicht;
        private bool                    interneAuStelleImAuftragPflicht;
        private bool                    duplikatprüfungPersonal;
        private int?                    standardInterneAuStelle;
        private decimal?                handlingszuschlagMaterial;
        private int                     standardTageFertigstellung;
        private String                  standardLeistungsartPlanmäßig;
        private String                  standardLeistungsartUnplanmäßig;
        private String                  leistungsartMOC;
        private String                  bestellfuß;
        private String                  rechnungsanschrift;
        private String                  lieferanschrift;
        private int?                    ticketAuftragsleiter;
        private int?                    bevorAuftragsleiter;
        private int                     ticketSbPflichttyp;
        private int                     ticketSuPflichttyp;
        private int?                    wPlanAuftraggeber;
        private int?                    wPlanAuftragnehmer;
        private int?                    wPlanAuftragsleiter;
        private int?                    wPlanAbnahmeberechtigter;
        private int?                    wPlanDringlichkeit;
        private int?                    wPlanLeistungsart;
        private int?                    wPlanGewerk;
        private int?                    wPlanNokDringlichkeit;
        private int?                    wPlanNokLeistungsart;
        private int?                    wPlanNokGewerk;
        private int?                    wPlanNormzeit;
        private decimal?                wPlanStundensatz;
        private int?                    wPlanWochenRückmeldung;
        private bool                    wPlanProdDarfPlantafelSehen;
        private bool                    wPlanReloadAfterSave;
        private bool                    wPlanMaterialPrüfung;
        private bool                    wPlanEinlagerungNachfragen;
        private int?                    wPlanAnzahlWochenBestellen;
        private bool                    emmaIHObjektNummern;
        private bool                    emmaAuftragsNummern;
        private int?                    emmaLeistungsart;
        private bool                    dokumentReduce;
        private TimeSpan?               standardSchichtUhrzeitVon;
        private TimeSpan?               standardSchichtUhrzeitBis;
        private decimal?                standardSchichtPause;
        private bool                    standardSchichtUhrzeitFilter;


        public bool                     Entnahmeschein                          { get { return entnahmeschein                 ; } set { SetProperty(ref entnahmeschein                 , value); } }
        public bool                     Lagerverwaltung                         { get { return lagerverwaltung                ; } set { SetProperty(ref lagerverwaltung                , value); } }
        public int?                     AuftragsnummerJahresziffern             { get { return auftragsnummerJahresziffern    ; } set { SetProperty(ref auftragsnummerJahresziffern    , value); } }
        public DateTime?                Abrechnungsschluß                       { get { return abrechnungsschluß              ; } set { SetProperty(ref abrechnungsschluß              , value); } }
        public bool                     Parameterverwaltung                     { get { return parameterverwaltung            ; } set { SetProperty(ref parameterverwaltung            , value); } }
        public bool                     AutomatischeFabriknummern               { get { return automatischeFabriknummern      ; } set { SetProperty(ref automatischeFabriknummern      , value); } }
        public bool                     FabriknummernPflicht                    { get { return fabriknummernPflicht           ; } set { SetProperty(ref fabriknummernPflicht           , value); } }
        public bool                     InventarnummernPflicht                  { get { return inventarnummernPflicht         ; } set { SetProperty(ref inventarnummernPflicht         , value); } }
        public int?                     IHObjektBarcodetyp                      { get { return iHObjektBarcodetyp             ; } set { SetProperty(ref iHObjektBarcodetyp             , value); } }
        public IHNummerAnzeigetyp?      IHNummerAnzeigetyp                      { get { return iHNummerAnzeigetyp             ; } set { SetProperty(ref iHNummerAnzeigetyp             , value); } }
        public bool                     IHObjektAuftragMitLöschen               { get { return iHObjektAuftragMitLöschen      ; } set { SetProperty(ref iHObjektAuftragMitLöschen      , value); } }
        public bool                     IHObjektTrotzAuftragLöschen             { get { return iHObjektTrotzAuftragLöschen    ; } set { SetProperty(ref iHObjektTrotzAuftragLöschen    , value); } }
        public bool                     IHObjektTrotzLeistungenLöschen          { get { return iHObjektTrotzLeistungenLöschen ; } set { SetProperty(ref iHObjektTrotzLeistungenLöschen , value); } }
        public bool                     IHObjektTrotzProjekteLöschen            { get { return iHObjektTrotzProjekteLöschen   ; } set { SetProperty(ref iHObjektTrotzProjekteLöschen   , value); } }
        public bool                     IHObjektObjektgruppeÄnderbar            { get { return iHObjektObjektgruppeÄnderbar   ; } set { SetProperty(ref iHObjektObjektgruppeÄnderbar   , value); } }
        public int?                     MaximaleBauteilTiefe                    { get { return maximaleBauteilTiefe           ; } set { SetProperty(ref maximaleBauteilTiefe           , value); } }
        public IHObjektFarbtyp?         IHObjektFarbtyp                         { get { return iHObjektFarbtyp                ; } set { SetProperty(ref iHObjektFarbtyp                , value); } }
        public int?                     IHObjektBauteilZiffern                  { get { return iHObjektBauteilZiffern         ; } set { SetProperty(ref iHObjektBauteilZiffern         , value); } }
        public int?                     IHObjektObjektartZiffern                { get { return iHObjektObjektartZiffern       ; } set { SetProperty(ref iHObjektObjektartZiffern       , value); } }
        public int?                     IHObjektObjektgruppeZiffern             { get { return iHObjektObjektgruppeZiffern    ; } set { SetProperty(ref iHObjektObjektgruppeZiffern    , value); } }
        public int?                     IHObjektMaschineZiffern                 { get { return iHObjektMaschineZiffern        ; } set { SetProperty(ref iHObjektMaschineZiffern        , value); } }
        public int?                     IHObjektStandortZiffern                 { get { return iHObjektStandortZiffern        ; } set { SetProperty(ref iHObjektStandortZiffern        , value); } }
        public String                   Kunde                                   { get { return kunde                          ; } set { SetProperty(ref kunde                          , value); } }
        public int?                     Rundungstyp                             { get { return rundungstyp                    ; } set { SetProperty(ref rundungstyp                    , value); } }
        public String                   Datenbankversion                        { get { return datenbankversion               ; } set { SetProperty(ref datenbankversion               , value); } }
        public int?                     PersonalSortiertyp                      { get { return personalSortiertyp             ; } set { SetProperty(ref personalSortiertyp             , value); } }
        public bool                     AuftragsdruckMitPreisen                 { get { return auftragsdruckMitPreisen        ; } set { SetProperty(ref auftragsdruckMitPreisen        , value); } }
        public bool                     AuftragsdruckMitMWS                     { get { return auftragsdruckMitMWS            ; } set { SetProperty(ref auftragsdruckMitMWS            , value); } }
        public bool                     AuftraggeberImAuftragPflicht            { get { return auftraggeberImAuftragPflicht   ; } set { SetProperty(ref auftraggeberImAuftragPflicht   , value); } }
        public bool                     InterneAuStelleImAuftragPflicht         { get { return interneAuStelleImAuftragPflicht; } set { SetProperty(ref interneAuStelleImAuftragPflicht, value); } }
        public bool                     DuplikatprüfungPersonal                 { get { return duplikatprüfungPersonal;         } set { SetProperty(ref duplikatprüfungPersonal        , value); } }
        public int?                     StandardInterneAuStelle                 { get { return standardInterneAuStelle        ; } set { SetProperty(ref standardInterneAuStelle        , value); } }
        public decimal?                 HandlingszuschlagMaterial               { get { return handlingszuschlagMaterial      ; } set { SetProperty(ref handlingszuschlagMaterial      , value); } }
        public int                      StandardTageFertigstellung              { get { return standardTageFertigstellung     ; } set { SetProperty(ref standardTageFertigstellung     , value); } }
        public String                   StandardLeistungsartPlanmäßig           { get { return standardLeistungsartPlanmäßig  ; } set { SetProperty(ref standardLeistungsartPlanmäßig  , value); } }
        public String                   StandardLeistungsartUnplanmäßig         { get { return standardLeistungsartUnplanmäßig; } set { SetProperty(ref standardLeistungsartUnplanmäßig, value); } }
        public String                   LeistungsartMOC                         { get { return leistungsartMOC                ; } set { SetProperty(ref leistungsartMOC                , value); } }
        public String                   Bestellfuß                              { get { return bestellfuß                     ; } set { SetProperty(ref bestellfuß                     , value); } }
        public String                   Rechnungsanschrift                      { get { return rechnungsanschrift             ; } set { SetProperty(ref rechnungsanschrift             , value); } }
        public String                   Lieferanschrift                         { get { return lieferanschrift                ; } set { SetProperty(ref lieferanschrift                , value); } }
        public int?                     TicketAuftragsleiter                    { get { return ticketAuftragsleiter           ; } set { SetProperty(ref ticketAuftragsleiter           , value); } }
        public int?                     BevorAuftragsleiter                     { get { return bevorAuftragsleiter            ; } set { SetProperty(ref bevorAuftragsleiter            , value); } }
        public int                      TicketSbPflichttyp                      { get { return ticketSbPflichttyp             ; } set { SetProperty(ref ticketSbPflichttyp             , value); } }
        public int                      TicketSuPflichttyp                      { get { return ticketSuPflichttyp             ; } set { SetProperty(ref ticketSuPflichttyp             , value); } }

        public int?                     WPlanAuftraggeber                       { get { return wPlanAuftraggeber; }             set { SetProperty(ref wPlanAuftraggeber, value); } }
        public int?                     WPlanAuftragnehmer                      { get { return wPlanAuftragnehmer; }            set { SetProperty(ref wPlanAuftragnehmer, value); } }
        public int?                     WPlanAuftragsleiter                     { get { return wPlanAuftragsleiter; }           set { SetProperty(ref wPlanAuftragsleiter, value); } }
        public int?                     WPlanAbnahmeberechtigter                { get { return wPlanAbnahmeberechtigter; }      set { SetProperty(ref wPlanAbnahmeberechtigter, value); } }
        public int?                     WPlanDringlichkeit                      { get { return wPlanDringlichkeit; }            set { SetProperty(ref wPlanDringlichkeit, value); } }
        public int?                     WPlanLeistungsart                       { get { return wPlanLeistungsart; }             set { SetProperty(ref wPlanLeistungsart, value); } }
        public int?                     WPlanGewerk                             { get { return wPlanGewerk; }                   set { SetProperty(ref wPlanGewerk, value); } }
        public int?                     WPlanNokDringlichkeit                   { get { return wPlanNokDringlichkeit; }         set { SetProperty(ref wPlanNokDringlichkeit, value); } }
        public int?                     WPlanNokLeistungsart                    { get { return wPlanNokLeistungsart; }          set { SetProperty(ref wPlanNokLeistungsart, value); } }
        public int?                     WPlanNokGewerk                          { get { return wPlanNokGewerk; }                set { SetProperty(ref wPlanNokGewerk, value); } }
        public int?                     WPlanNormzeit                           { get { return wPlanNormzeit; }                 set { SetProperty(ref wPlanNormzeit, value); } }
        public decimal?                 WPlanStundensatz                        { get { return wPlanStundensatz; }              set { SetProperty(ref wPlanStundensatz, value); } }
        public int?                     WPlanWochenRückmeldung                  { get { return wPlanWochenRückmeldung;}         set { SetProperty(ref wPlanWochenRückmeldung, value); } }
        public bool                     WPlanProdDarfPlantafelSehen             { get { return wPlanProdDarfPlantafelSehen;}    set { SetProperty(ref wPlanProdDarfPlantafelSehen, value); } }
        public bool                     WPlanReloadAfterSave                    { get { return wPlanReloadAfterSave;}           set { SetProperty(ref wPlanReloadAfterSave, value); } }
        public bool                     WPlanMaterialPrüfung                    { get { return wPlanMaterialPrüfung;}           set { SetProperty(ref wPlanMaterialPrüfung, value); } }
        public bool                     WPlanEinlagerungNachfragen              { get { return wPlanEinlagerungNachfragen;}     set { SetProperty(ref wPlanEinlagerungNachfragen, value); } }
        public int?                     WPlanAnzahlWochenBestellen              { get { return wPlanAnzahlWochenBestellen;}     set { SetProperty(ref wPlanAnzahlWochenBestellen, value); } }

        public bool                     EmmaIHObjektNummern                     { get { return emmaIHObjektNummern; }           set { SetProperty(ref emmaIHObjektNummern, value); } }
        public bool                     EmmaAuftragsNummern                     { get { return emmaAuftragsNummern; }           set { SetProperty(ref emmaAuftragsNummern, value); } }
        public int?                     EmmaLeistungsart                        { get { return emmaLeistungsart; }              set { SetProperty(ref emmaLeistungsart, value); } }

        public bool                     DokumentReduce                          { get { return dokumentReduce; }                set { SetProperty(ref dokumentReduce, value); } }

        public TimeSpan?                StandardSchichtUhrzeitVon               { get => standardSchichtUhrzeitVon;             set => SetProperty(ref standardSchichtUhrzeitVon, value); }
        public TimeSpan?                StandardSchichtUhrzeitBis               { get => standardSchichtUhrzeitBis;             set => SetProperty(ref standardSchichtUhrzeitBis, value); }
        public decimal?                 StandardSchichtPause                    { get => standardSchichtPause;                  set => SetProperty(ref standardSchichtPause, value); }
        public bool                     StandardSchichtUhrzeitFilter            { get => standardSchichtUhrzeitFilter;          set => SetProperty(ref standardSchichtUhrzeitFilter, value); }

        public String StandardSchichtUhrzeitTextVon => !StandardSchichtUhrzeitVon.HasValue ? null: StandardSchichtUhrzeitVon.Value.ToString("hh\\:mm");
        public String StandardSchichtUhrzeitTextBis => !StandardSchichtUhrzeitBis.HasValue ? null: StandardSchichtUhrzeitBis.Value.ToString("hh\\:mm");


        public string Error => null;
        public string this[string columnName] => null;


        public IEnumerable<KeyValuePair<String, String>> Modifikationen()
        {

            if (!IsChanged) return null;
            var list = new List<KeyValuePair<String, String>>();

            if (ChangedProps.Contains(nameof(Entnahmeschein)))                  list.Add(new KeyValuePair<String, String>(nameof(Entnahmeschein)                  , Entnahmeschein? "1": "0"));
            if (ChangedProps.Contains(nameof(Lagerverwaltung)))                 list.Add(new KeyValuePair<String, String>(nameof(Lagerverwaltung)                 , Lagerverwaltung? "1": "0"));
            if (ChangedProps.Contains(nameof(AuftragsnummerJahresziffern)))     list.Add(new KeyValuePair<String, String>(nameof(AuftragsnummerJahresziffern)     , AuftragsnummerJahresziffern.ToString()));
            if (ChangedProps.Contains(nameof(Abrechnungsschluß)))               list.Add(new KeyValuePair<String, String>(nameof(Abrechnungsschluß)               , Abrechnungsschluß.HasValue? Abrechnungsschluß.Value.ToShortDateString(): null));
            if (ChangedProps.Contains(nameof(Parameterverwaltung)))             list.Add(new KeyValuePair<String, String>(nameof(Parameterverwaltung)             , Parameterverwaltung? "1": "0"));
            if (ChangedProps.Contains(nameof(AutomatischeFabriknummern)))       list.Add(new KeyValuePair<String, String>(nameof(AutomatischeFabriknummern)       , AutomatischeFabriknummern? "1": "0"));
            if (ChangedProps.Contains(nameof(FabriknummernPflicht)))            list.Add(new KeyValuePair<String, String>(nameof(FabriknummernPflicht)            , FabriknummernPflicht? "1": "0"));
            if (ChangedProps.Contains(nameof(InventarnummernPflicht)))          list.Add(new KeyValuePair<String, String>(nameof(InventarnummernPflicht)          , InventarnummernPflicht? "1": "0"));
            if (ChangedProps.Contains(nameof(IHNummerAnzeigetyp)))              list.Add(new KeyValuePair<String, String>(nameof(IHNummerAnzeigetyp)              , ((int?)IHNummerAnzeigetyp).ToString()));
            if (ChangedProps.Contains(nameof(IHObjektBarcodetyp)))              list.Add(new KeyValuePair<String, String>(nameof(IHObjektBarcodetyp)              , IHObjektBarcodetyp.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektAuftragMitLöschen)))       list.Add(new KeyValuePair<String, String>(nameof(IHObjektAuftragMitLöschen)       , IHObjektAuftragMitLöschen? "1": "0"));
            if (ChangedProps.Contains(nameof(IHObjektTrotzAuftragLöschen)))     list.Add(new KeyValuePair<String, String>(nameof(IHObjektTrotzAuftragLöschen)     , IHObjektTrotzAuftragLöschen? "1": "0"));
            if (ChangedProps.Contains(nameof(IHObjektTrotzLeistungenLöschen)))  list.Add(new KeyValuePair<String, String>(nameof(IHObjektTrotzLeistungenLöschen)  , IHObjektTrotzLeistungenLöschen? "1": "0"));
            if (ChangedProps.Contains(nameof(IHObjektTrotzProjekteLöschen)))    list.Add(new KeyValuePair<String, String>(nameof(IHObjektTrotzProjekteLöschen)    , IHObjektTrotzProjekteLöschen? "1": "0"));
            if (ChangedProps.Contains(nameof(IHObjektObjektgruppeÄnderbar)))    list.Add(new KeyValuePair<String, String>(nameof(IHObjektObjektgruppeÄnderbar)    , IHObjektObjektgruppeÄnderbar? "1": "0"));
            if (ChangedProps.Contains(nameof(MaximaleBauteilTiefe)))            list.Add(new KeyValuePair<String, String>(nameof(MaximaleBauteilTiefe)            , MaximaleBauteilTiefe.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektFarbtyp)))                 list.Add(new KeyValuePair<String, String>(nameof(IHObjektFarbtyp)                 , ((int?)IHObjektFarbtyp).ToString()));
            if (ChangedProps.Contains(nameof(IHObjektBauteilZiffern)))          list.Add(new KeyValuePair<String, String>(nameof(IHObjektBauteilZiffern)          , IHObjektBauteilZiffern.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektObjektartZiffern)))        list.Add(new KeyValuePair<String, String>(nameof(IHObjektObjektartZiffern)        , IHObjektObjektartZiffern.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektObjektgruppeZiffern)))     list.Add(new KeyValuePair<String, String>(nameof(IHObjektObjektgruppeZiffern)     , IHObjektObjektgruppeZiffern.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektMaschineZiffern)))         list.Add(new KeyValuePair<String, String>(nameof(IHObjektMaschineZiffern)         , IHObjektMaschineZiffern.ToString()));
            if (ChangedProps.Contains(nameof(IHObjektStandortZiffern)))         list.Add(new KeyValuePair<String, String>(nameof(IHObjektStandortZiffern)         , IHObjektStandortZiffern.ToString()));
            if (ChangedProps.Contains(nameof(Kunde)))                           list.Add(new KeyValuePair<String, String>(nameof(Kunde)                           , Kunde));
            if (ChangedProps.Contains(nameof(Rundungstyp)))                     list.Add(new KeyValuePair<String, String>(nameof(Rundungstyp)                     , Rundungstyp.ToString()));
            if (ChangedProps.Contains(nameof(Datenbankversion)))                list.Add(new KeyValuePair<String, String>(nameof(Datenbankversion)                , Datenbankversion));
            if (ChangedProps.Contains(nameof(PersonalSortiertyp)))              list.Add(new KeyValuePair<String, String>(nameof(PersonalSortiertyp)              , PersonalSortiertyp.ToString()));
            if (ChangedProps.Contains(nameof(AuftragsdruckMitPreisen)))         list.Add(new KeyValuePair<String, String>(nameof(AuftragsdruckMitPreisen)         , AuftragsdruckMitPreisen? "1": "0"));
            if (ChangedProps.Contains(nameof(AuftragsdruckMitMWS)))             list.Add(new KeyValuePair<String, String>(nameof(AuftragsdruckMitMWS)             , AuftragsdruckMitMWS? "1": "0"));
            if (ChangedProps.Contains(nameof(AuftraggeberImAuftragPflicht)))    list.Add(new KeyValuePair<String, String>(nameof(AuftraggeberImAuftragPflicht)    , AuftraggeberImAuftragPflicht? "1": "0"));
            if (ChangedProps.Contains(nameof(InterneAuStelleImAuftragPflicht))) list.Add(new KeyValuePair<String, String>(nameof(InterneAuStelleImAuftragPflicht) , InterneAuStelleImAuftragPflicht? "1": "0"));
            if (ChangedProps.Contains(nameof(StandardInterneAuStelle)))         list.Add(new KeyValuePair<String, String>(nameof(StandardInterneAuStelle)         , StandardInterneAuStelle.ToString()));
            if (ChangedProps.Contains(nameof(HandlingszuschlagMaterial)))       list.Add(new KeyValuePair<String, String>(nameof(HandlingszuschlagMaterial)       , HandlingszuschlagMaterial.ToString()));
            if (ChangedProps.Contains(nameof(StandardTageFertigstellung)))      list.Add(new KeyValuePair<String, String>(nameof(StandardTageFertigstellung)      , StandardTageFertigstellung.ToString()));
            if (ChangedProps.Contains(nameof(StandardLeistungsartPlanmäßig)))   list.Add(new KeyValuePair<String, String>(nameof(StandardLeistungsartPlanmäßig)   , StandardLeistungsartPlanmäßig));
            if (ChangedProps.Contains(nameof(StandardLeistungsartUnplanmäßig))) list.Add(new KeyValuePair<String, String>(nameof(StandardLeistungsartUnplanmäßig) , StandardLeistungsartUnplanmäßig));
            if (ChangedProps.Contains(nameof(LeistungsartMOC)))                 list.Add(new KeyValuePair<String, String>(nameof(LeistungsartMOC)                 , LeistungsartMOC));
            if (ChangedProps.Contains(nameof(Bestellfuß)))                      list.Add(new KeyValuePair<String, String>(nameof(Bestellfuß),                       Bestellfuß));
            if (ChangedProps.Contains(nameof(Rechnungsanschrift)))              list.Add(new KeyValuePair<String, String>(nameof(Rechnungsanschrift)              , Rechnungsanschrift));
            if (ChangedProps.Contains(nameof(Lieferanschrift)))                 list.Add(new KeyValuePair<String, String>(nameof(Lieferanschrift)                 , Lieferanschrift));
            if (ChangedProps.Contains(nameof(TicketAuftragsleiter)))            list.Add(new KeyValuePair<String, String>(nameof(TicketAuftragsleiter),             TicketAuftragsleiter.ToString()));
            if (ChangedProps.Contains(nameof(BevorAuftragsleiter)))             list.Add(new KeyValuePair<String, String>(nameof(BevorAuftragsleiter),              BevorAuftragsleiter.ToString()));
            if (ChangedProps.Contains(nameof(TicketSbPflichttyp)))              list.Add(new KeyValuePair<String, String>(nameof(TicketSbPflichttyp),               TicketSbPflichttyp.ToString()));
            if (ChangedProps.Contains(nameof(TicketSuPflichttyp)))              list.Add(new KeyValuePair<String, String>(nameof(TicketSuPflichttyp),               TicketSuPflichttyp.ToString()));
            if (ChangedProps.Contains(nameof(WPlanAuftraggeber)))               list.Add(new KeyValuePair<String, String>(nameof(WPlanAuftraggeber),                WPlanAuftraggeber.ToString()));
            if (ChangedProps.Contains(nameof(WPlanAuftragnehmer)))              list.Add(new KeyValuePair<String, String>(nameof(WPlanAuftragnehmer),               WPlanAuftragnehmer.ToString()));
            if (ChangedProps.Contains(nameof(WPlanAuftragsleiter)))             list.Add(new KeyValuePair<String, String>(nameof(WPlanAuftragsleiter),              WPlanAuftragsleiter.ToString()));
            if (ChangedProps.Contains(nameof(WPlanAbnahmeberechtigter)))        list.Add(new KeyValuePair<String, String>(nameof(WPlanAbnahmeberechtigter),         WPlanAbnahmeberechtigter.ToString()));
            if (ChangedProps.Contains(nameof(WPlanDringlichkeit)))              list.Add(new KeyValuePair<String, String>(nameof(WPlanDringlichkeit),               WPlanDringlichkeit.ToString()));
            if (ChangedProps.Contains(nameof(WPlanLeistungsart)))               list.Add(new KeyValuePair<String, String>(nameof(WPlanLeistungsart),                WPlanLeistungsart.ToString()));
            if (ChangedProps.Contains(nameof(WPlanGewerk)))                     list.Add(new KeyValuePair<String, String>(nameof(WPlanGewerk),                      WPlanGewerk.ToString()));
            if (ChangedProps.Contains(nameof(WPlanNokDringlichkeit)))           list.Add(new KeyValuePair<String, String>(nameof(WPlanNokDringlichkeit),            WPlanNokDringlichkeit.ToString()));
            if (ChangedProps.Contains(nameof(WPlanNokLeistungsart)))            list.Add(new KeyValuePair<String, String>(nameof(WPlanNokLeistungsart),             WPlanNokLeistungsart.ToString()));
            if (ChangedProps.Contains(nameof(WPlanNokGewerk)))                  list.Add(new KeyValuePair<String, String>(nameof(WPlanNokGewerk),                   WPlanNokGewerk.ToString()));
            if (ChangedProps.Contains(nameof(WPlanNormzeit)))                   list.Add(new KeyValuePair<String, String>(nameof(WPlanNormzeit),                    WPlanNormzeit.ToString()));
            if (ChangedProps.Contains(nameof(WPlanStundensatz)))                list.Add(new KeyValuePair<String, String>(nameof(WPlanStundensatz),                 WPlanStundensatz.ToString()));
            if (ChangedProps.Contains(nameof(WPlanWochenRückmeldung)))          list.Add(new KeyValuePair<String, String>(nameof(WPlanWochenRückmeldung),           WPlanWochenRückmeldung.ToString()));
            if (ChangedProps.Contains(nameof(WPlanProdDarfPlantafelSehen)))     list.Add(new KeyValuePair<String, String>(nameof(WPlanProdDarfPlantafelSehen),      WPlanProdDarfPlantafelSehen? "1": "0"));
            if (ChangedProps.Contains(nameof(WPlanReloadAfterSave)))            list.Add(new KeyValuePair<String, String>(nameof(WPlanReloadAfterSave),             WPlanReloadAfterSave? "1": "0"));
            if (ChangedProps.Contains(nameof(WPlanMaterialPrüfung)))            list.Add(new KeyValuePair<String, String>(nameof(WPlanMaterialPrüfung),             WPlanMaterialPrüfung? "1": "0"));
            if (ChangedProps.Contains(nameof(WPlanEinlagerungNachfragen)))      list.Add(new KeyValuePair<String, String>(nameof(WPlanEinlagerungNachfragen),       WPlanEinlagerungNachfragen? "1": "0"));
            if (ChangedProps.Contains(nameof(WPlanAnzahlWochenBestellen)))      list.Add(new KeyValuePair<String, String>(nameof(WPlanAnzahlWochenBestellen),       WPlanAnzahlWochenBestellen.ToString()));
            if (ChangedProps.Contains(nameof(DuplikatprüfungPersonal)))         list.Add(new KeyValuePair<String, String>(nameof(DuplikatprüfungPersonal),          DuplikatprüfungPersonal? "1": "0"));
            if (ChangedProps.Contains(nameof(EmmaIHObjektNummern)))             list.Add(new KeyValuePair<String, String>(nameof(EmmaIHObjektNummern),              EmmaIHObjektNummern? "1": "0"));
            if (ChangedProps.Contains(nameof(EmmaAuftragsNummern)))             list.Add(new KeyValuePair<String, String>(nameof(EmmaAuftragsNummern),              EmmaAuftragsNummern.ToString()));
            if (ChangedProps.Contains(nameof(EmmaLeistungsart)))                list.Add(new KeyValuePair<String, String>(nameof(EmmaLeistungsart),                 EmmaLeistungsart.ToString()));
            if (ChangedProps.Contains(nameof(DokumentReduce)))                  list.Add(new KeyValuePair<String, String>(nameof(DokumentReduce),                   DokumentReduce? "1": "0"));
            if (ChangedProps.Contains(nameof(StandardSchichtUhrzeitVon)))       list.Add(new KeyValuePair<String, String>(nameof(StandardSchichtUhrzeitVon),        StandardSchichtUhrzeitVon.ToString()));
            if (ChangedProps.Contains(nameof(StandardSchichtUhrzeitBis)))       list.Add(new KeyValuePair<String, String>(nameof(StandardSchichtUhrzeitBis),        StandardSchichtUhrzeitBis.ToString()));
            if (ChangedProps.Contains(nameof(StandardSchichtPause)))            list.Add(new KeyValuePair<String, String>(nameof(StandardSchichtPause),             StandardSchichtPause.ToString()));
            if (ChangedProps.Contains(nameof(StandardSchichtUhrzeitFilter)))    list.Add(new KeyValuePair<String, String>(nameof(StandardSchichtUhrzeitFilter),     StandardSchichtUhrzeitFilter? "1": "0"));


            return list;
        }
    }
}
