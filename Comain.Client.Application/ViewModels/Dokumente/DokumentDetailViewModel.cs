﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Views.Dokumente;


namespace Comain.Client.ViewModels.Dokumente
{

    public class DokumentDetailViewModel : ValidatableViewModel<IDokumentDetailView, Dokument>, IDelayableViewModel
    {

        private IEnumerable<Kategorie> klist;


        public DokumentDetailViewModel(IDokumentDetailView view) : base(view)
        {}


        public void Load(IList<Kategorie> katlist, IList<String> taglist)
        {

            klist = katlist;
            AlleSchlagworte = taglist;
        }


        public void Unload()
        {

            Kategorien = null;
            AlleSchlagworte = null;
            ZugeordneteSchlagworte = null;
            Entities = null;
        }


        public override void CurrentChanged(Dokument oldSelected)
        {

            RaisePropertyChanged(nameof(Filename));
            RaisePropertyChanged(nameof(Filesize));
            RaisePropertyChanged(nameof(Typ));
            RaisePropertyChanged(nameof(Url));
            RaisePropertyChanged(nameof(PreviewUrl));
            RaisePropertyChanged(nameof(Version));
            RaisePropertyChanged(nameof(ErstelltAm));
            RaisePropertyChanged(nameof(ErstelltVon));
            RaisePropertyChanged(nameof(GeändertAm));
            RaisePropertyChanged(nameof(GeändertVon));
            RaisePropertyChanged(nameof(GeöffnetAm));
            RaisePropertyChanged(nameof(Bezeichnung));
            RaisePropertyChanged(nameof(Beschreibung));

            Kategorien = null;
            ZugeordneteSchlagworte = null;
            Entities = null;
            if (Current == null) return;

            Kategorien = klist.Select(p => new KategorieAssignViewItem(Current, p)).ToList();
            ZugeordneteSchlagworte = Current.Tags.OrderBy(p => p).ToList();
        }


        public void AddSchlagwort(String text)
        {

            Current.Tags.Add(text);
            ZugeordneteSchlagworte = Current.Tags.OrderBy(p => p).ToList();
            Current.RaisePropertyChanged(nameof(Current.Tags));
        }


        public void RemoveSchlagwort(String text)
        {

            Current.Tags.Remove(text);
            ZugeordneteSchlagworte = Current.Tags.OrderBy(p => p).ToList();
            Current.RaisePropertyChanged(nameof(Current.Tags));
        }


#region P R O P E R T I E S


        public  String       Filename    => Current?.Filename;
        public  String       Filesize    => Current != null && Current.Filesize.HasValue? $"{Current.Filesize} bytes": null;
        public  String       Typ         => Current?.Typ;
        public  String       Url         => Current?.Url;
        public  String       PreviewUrl  => Current?.PreviewUrl;
        public  int?         Version     => Current?.Version;
        public  DateTime?    ErstelltAm  => Current?.ErstelltAm;
        public  String       ErstelltVon => Current?.ErstelltVon;
        public  DateTime?    GeändertAm  => Current?.GeändertAm;
        public  String       GeändertVon => Current?.GeändertVon;
        public  DateTime?    GeöffnetAm  => Current?.GeöffnetAm;


        public String Bezeichnung
        {
            get => Current?.Bezeichnung;
            set {

                if (value != null && Current != null && value != Current.Bezeichnung) Current.Bezeichnung = value;
                RaisePropertyChanged(nameof(Bezeichnung));
            }
        }


        public String Beschreibung
        {
            get => Current?.Beschreibung;
            set {

                if (value != null && Current != null && value != Current.Beschreibung) Current.Beschreibung = value;
                RaisePropertyChanged(nameof(Beschreibung));
            }
        }


        private IList<AssignedEntityItem> entities;
        public IList<AssignedEntityItem> Entities
        {
            get => entities;
            set => SetProperty(ref entities, value);
        }


        private IList<KategorieAssignViewItem> kategorien;
        public IList<KategorieAssignViewItem> Kategorien
        {
            get => kategorien;
            set => SetProperty(ref kategorien, value);
        }


        private IList<String> alleSchlagworte;
        public IList<String> AlleSchlagworte
        {
            get => alleSchlagworte;
            set => SetProperty(ref alleSchlagworte, value);
        }


        private IList<String> zugeordneteSchlagworte;
        public IList<String> ZugeordneteSchlagworte
        {
            get => zugeordneteSchlagworte;
            set => SetProperty(ref zugeordneteSchlagworte, value);
        }


        private ICommand addTagCommand;
        public ICommand AddTagCommand
        {
            get => addTagCommand;
            set => SetProperty(ref addTagCommand, value);
        }


        private ICommand removeTagCommand;
        public ICommand RemoveTagCommand
        {
            get => removeTagCommand;
            set => SetProperty(ref removeTagCommand, value);
        }


        private ICommand openEntityCommand;
        public ICommand OpenEntityCommand
        {
            get => openEntityCommand;
            set => SetProperty(ref openEntityCommand, value);
        }


        private String addTagText;
        public String AddTagText
        {
            get => addTagText;
            set => SetProperty(ref addTagText, value);
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }


#endregion

    }
}
