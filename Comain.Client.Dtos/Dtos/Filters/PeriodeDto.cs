﻿namespace Comain.Client.Dtos.Filters
{

    public class PeriodeDto
    {

        public int Von  { get; set; }
        public int Bis  { get; set; }
    }
}
