﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.Tickets
{

    public class TicketUpdateDto
    {

        public int          Id                      { get; set; }

        public int?         AuIHKS_Id               { get; set; }
        public int          Dringlichkeit           { get; set; }
        public DateTime?    Fertigstellungstermin   { get; set; }

        public String       Kurzbeschreibung        { get; set; }
        public String       Aktivitätenbeschreibung { get; set; }
        public int?         Schadensbild            { get; set; }
        public int?         Schadensursache         { get; set; }

        public int?         Gewerk                  { get; set; }
        public int?         Auftragnehmer           { get; set; }
        public int?         Auftraggeber            { get; set; }
        public int?         Auftragsleiter          { get; set; }
        public int?         Abnahmeberechtigter     { get; set; }

        public DateTime?    LeDatum                 { get; set; }
        public TimeSpan?    LeUhrzeitVon            { get; set; }
        public TimeSpan?    LeUhrzeitBis            { get; set; }
        public String       LeAnmerkung             { get; set; }
        public String       LeMaterial              { get; set; }
        public bool         LeIstFertig             { get; set; }
        public bool         LeIstNichtFertig        { get; set; }
        public bool         LeIstSauber             { get; set; }

        public bool         AbnahmeAbgenommen       { get; set; }
        public bool         AbnahmeAbgelehnt        { get; set; }
        public String       AbnahmeBegründung       { get; set; }
    }
}
