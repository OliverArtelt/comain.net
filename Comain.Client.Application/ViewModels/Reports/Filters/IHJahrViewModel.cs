﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Reports.Filters;


namespace Comain.Client.ViewModels.Reports.Filters
{

    public class IHJahrViewModel : ViewModel<IIHJahrView>, IFilterViewModel
    {

        private readonly IFilterService store;
        public IEnumerable<PeriodToken> Jahre    { get; private set; }
        private readonly IHSelektorViewModel ihFilter;


        public IHJahrViewModel(IHSelektorViewModel ihFilter, IFilterService store, IIHJahrView view) : base(view)
        {

            this.ihFilter = ihFilter;
            this.store = store;
        }


        public void Reset()
        {

            ihFilter.Reset();
            Jahr = null;
        }


        public String Validiere()
        {
            
            if (IHObjekt == null) return "Wählen Sie ein Asset.";
            if (Jahr == null) return "Wählen Sie ein Jahr.";
            return null;
        }


        public String FilterAlsString()
        {

            String s = String.Empty;

            if (IHObjekt != null) s += String.Format("Asset {0} - {1}, ", IHObjekt.Nummer, IHObjekt.Name);
            if (Jahr != null) s += String.Format("Jahr {0}, ", Jahr.Caption);
            return s.Trim(' ', ',');
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (IHObjekt != null) filter.Add("IH", IHObjekt.Id);
            if (Jahr != null) filter.Add("Jahr", Jahr.Caption);
        }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            Jahre = Enumerable.Range(store.Periode.Von, store.Periode.Bis - store.Periode.Von + 1)
                              .Select(p => new PeriodToken { Token = "Y" + p.ToString(), Caption = p.ToString() })
                              .ToList();
            var current = Jahre.FirstOrDefault(p => p.Caption == DateTime.Now.Year.ToString());
            if (current != null) current.Highlighted = true;
            RaisePropertyChanged("Jahre");

            await ihFilter.LoadAsync(store.IHObjekte.Select(p => new IHFilterItem(p)).ToList(), store.Standorte, store.Kostenstellen, ct).ConfigureAwait(false);
        }


        public void Unload()
        {
            ihFilter.Unload();
        }


#region P R O P E R T I E S
    

        private PeriodToken jahr;
        public PeriodToken Jahr
        { 
            get { return jahr; }
            set { SetProperty(ref jahr, value); }
        } 


        public object       IHSelektor  { get { return ihFilter.View; } }
        public IHFilterItem IHObjekt    { get { return ihFilter.CurrentIHObjekt; } }


#endregion

    }
}
