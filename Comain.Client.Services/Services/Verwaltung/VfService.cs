﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{

    public class VfService : IVfService
    {

        protected readonly IRepository store;
        private readonly IMapper mapper;
        public TrackableCollection<IHObjektBelegung> CurrentList { get; private set; }


        public VfService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<IHSelektorItem>> GetIHListAsync(CancellationToken ct = default)
        {

            var result = await store.GetAsync<IList<IHObjektFilterModel>>("vw/vfih", ct).ConfigureAwait(false);
            var list = result.OrderBy(p => p.Nummer).Select(p => new IHSelektorItem(p)).ToList();
            return list;
        }


        public async Task<IList<DateTime>> GetVormonateAsync(CancellationToken ct = default)
        {

            var result = await store.GetAsync<IList<DateTime>>("vw/vfvm", ct).ConfigureAwait(false);
            return result;
        }


        public async Task<TrackableCollection<IHObjektBelegung>> GetBelegungAsync(int year, int month, CancellationToken ct = default)
        {

            var list = await GetCopyAsync(year, month, ct);
            CurrentList = list.AsTrackable();
            return CurrentList;
        }


        public async Task<IList<IHObjektBelegung>> GetCopyAsync(int year, int month, CancellationToken ct = default)
        {

            String uri = String.Format(@"vw/vf/{0}/{1}", year, month);

            var qry = await store.GetAsync<IList<IHBelegungDto>>(uri, ct).ConfigureAwait(false);
            return mapper.Map<List<IHObjektBelegung>>(qry);
        }

     
        public bool HasChanges
        {
            get { return CurrentList != null && CurrentList.IsChanged; }
        }


        public async Task SaveAsync(CancellationToken ct = default)
        {
            
            var result = mapper.AsDtoList<IHObjektBelegung, IHBelegungDto>(CurrentList);
            if (result == null) return;
           
            await store.PostAsync<List<IHBelegungDto>>(@"vw/vf", result, ct).ConfigureAwait(false);
            CurrentList.AcceptChanges();
        }


        public void Reset()
        {
            CurrentList = null;
        }
    }
}
