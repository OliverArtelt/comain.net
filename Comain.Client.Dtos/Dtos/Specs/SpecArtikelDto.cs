﻿using System;
using System.Collections.Generic;
using System.Text;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Specs
{

    public class SpecArtikelDto : ChangeTrackingBase
    {

        public  decimal?            Menge               { get; set; }
        public  bool                Erforderlich        { get; set; }
        public  String              Anweisung           { get; set; }

        public  int                 Artikel_Id          { get; set; }
        public  int                 Spezifikation_Id    { get; set; }

        public  String              Name                { get; set; }
        public  String              Nummer              { get; set; }
        public  String              Beschreibung        { get; set; }
        public  decimal?            Mischpreis          { get; set; }
        public  NummerFilterModel   Einheit             { get; set; }
        public  NummerFilterModel   Warengruppe         { get; set; }
    }
}
