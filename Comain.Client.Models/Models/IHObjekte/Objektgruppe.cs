﻿using System;
using System.ComponentModel;


namespace Comain.Client.Models.IHObjekte
{

    public class Objektgruppe : Entity, IDataErrorInfo, IComparable<Objektgruppe>
    {

        private short nummer;
        private String name;
        private bool istGebäude;


#region P R O P E R T I E S


        public short Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        
        public bool IstGebäude
        {
            get { return istGebäude; }
            set { SetProperty(ref istGebäude, value); }
        }


#endregion

        
        public int CompareTo(Objektgruppe other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareTo(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung der Objektgruppe an.";
                    if (Name.Length > 50) return "Die Bezeichnung der Objektgruppe darf aus maximal 50 Zeichen bestehen."; 
                    break;

                case "Nummer":

                    if (Nummer < 1 || Nummer > 99) return "Die Nummer der Objektgruppe muss zwischen 1 und 99 liegen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
