﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models.Stock;

namespace Comain.Client.Services.Stock.Warenausgänge
{

    public class AusgangService : IAusgangService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public AusgangService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public Task<IList<LagerortDto>> LagerorteAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<LagerortDto>>("/VW/LO", ct);
        }


        public Task<IList<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct); 
        }
 

        public Task<IList<AuSelektorDto>> AufträgeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<AuSelektorDto>>("/WA/AU", ct); 
        }
 

        public Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);   
        }


        public Task<IList<EinlagerungDto>> VerfügbareArtikelAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<EinlagerungDto>>("/WA/VfWl", ct);
        }


        public Task<IHInfoDto> IHInfoAsync(int auid, CancellationToken ct = default)
        {
            return store.GetAsync<IHInfoDto>(String.Format("/IH/AU/{0}", auid), ct);
        }


        public Task<decimal> FachRestmengeAsync(int lagerortid, int artikelid, CancellationToken ct = default)
        {
            return store.GetAsync<decimal>(String.Format("/WA/LagerRest/{0}/{1}", lagerortid, artikelid), ct);
        }


        public async Task<TrackableCollection<Warenausgang>> FindAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var models = await store.FindAsync<IList<Warenausgang>>("/WA/Search", filter, ct).ConfigureAwait(false);
            return new TrackableCollection<Warenausgang>(models);
        }

 
        public Task<IList<WAReportDto>> ReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<WAReportDto>>("/WA/Report", filter, ct);
        }


        public async Task SaveAsync(TrackableCollection<Warenausgang> list, CancellationToken ct = default)
        {
            
            var qry = mapper.AsDtoList<Warenausgang, AusgangDto>(list);
            await store.PostAsync<IList<AusgangDto>>("/WA", qry, ct);
        }
    }
}
