﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.ReportModels;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Reports.Types
{
    
    public class VFGesamtReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public VFGesamtReport(IReportService store, MonatViewModel monatView)
        {

            this.store = store;
            filterViews = new List<IFilterViewModel> { monatView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {
            
            var result = Create();
            var data = await store.GesamtverfügbarkeitAsync(GebeFilter(), ct);
            result.AddDataSource("MainData", data.Select(p => new VfGesamtRow(p)));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            return false;
            //return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Verfügbarkeit Gesamtprojekt"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 430; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.VFGesamtReport.rdlc"; }
        }
    }
}
