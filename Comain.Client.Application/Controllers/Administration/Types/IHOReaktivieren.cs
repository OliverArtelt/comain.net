﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class IHOReaktivieren : IAdminTyp
    {
        
        public string       Title           => "Assets reaktivieren";
        public int          Sortierung      => 20000;
        public Type         Controller      => typeof(IIHReactivateController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
