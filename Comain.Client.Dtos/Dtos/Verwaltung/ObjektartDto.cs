﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class ObjektartDto : ChangeTrackingBase
    {

        public short     Nummer           { get; set; }
        public String    Name             { get; set; }
        public int       Haupt_Id         { get; set; }
    }
}
