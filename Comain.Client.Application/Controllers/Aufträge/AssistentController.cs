﻿using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Aufträge;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Controllers.Aufträge
{

    public class AssistentController : ControllerBase, IAssistentController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IAuftragService store;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService config;
        private readonly MainViewModel mainView;
        private readonly AssistentViewModel view;
        private readonly AssistentResultViewModel resultView;


        public AssistentController(IBreadcrumbService crumbs, IAuftragService store, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                   IKonfigService config, IControllerBus bus,
                                   MainViewModel mainView, AssistentViewModel view, AssistentResultViewModel resultView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.store = store;
            this.login = login;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.resultView = resultView;
            this.config = config;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            resultView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.CreateCommand = CreateCommand(_ => SaveAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Auftrag erstellen");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            view.ResetSearch();
            resultView.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {}


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var ihtask  = store.FilterAuIHKSAsync(bus.Token);
                var pztask  = store.FilterStandorteAsync(bus.Token);
                var sbtask  = store.FilterSchadensbilderAsync(bus.Token);
                var ssbtask = store.FilterSubSchadensbilderAsync(bus.Token);
                var pstask  = store.FilterPersonalAsync(bus.Token);
                var drtask  = store.FilterDringlichkeitAsync(bus.Token);
                var cftask  = config.GetAsync(bus.Token);

                await Task.WhenAll(ihtask, pztask, sbtask, ssbtask, pstask, drtask, cftask).ConfigureAwait(false);
                await view.SetFilterAsync(ihtask.Result, pztask.Result, sbtask.Result, pstask.Result, cftask.Result, drtask.Result.Liste(Auftragstyp.Standardauftrag), ssbtask.Result, bus.Token);
            }
        }


        protected override Task<bool> UnloadImplAsync()
        {

            view.Unload();
            resultView.Unload();
            return Task.FromResult(true);
        }


        private async Task BackAsync()
        {

            crumbs.Remove();
            await bus.BackAsync();
        }


        private async Task SaveAsync()
        {

            ComainValidationException ex = null;

            try {

                var auftrag = view.GebeAuftrag();

                using (new VisualDelay(view)) {

                    var result = await store.CreateAsync(auftrag, bus.Token);
                    result.Dringlichkeit = view.Dringlichkeit;
                    var address = "Comain.Client.Reports.AuftragLeerReport.rdlc";
                    var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = result.Nummer, HasLogo = true };
                    var ihpath = await store.IHPathAsync(result.AuIHKS.IHObjekt, bus.Token);
                    report.AddDataSource("MainData", new List<AuftragRow> { new AuftragRow(result, ihpath, false) });
                    report.AddParameter("parPrice", "0");
                    await resultView.BindViewerAsync(report);
                }

                mainView.PageView = resultView.View;
                return;
            }
            catch (ComainValidationException x)
            {
                ex = x;
            }

            if (ex != null) await messages.ShowMessageAsync(new ViewData.MessageToken(ex));
        }
    }
}
