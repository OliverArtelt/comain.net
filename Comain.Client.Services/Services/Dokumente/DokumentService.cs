﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Dokumente;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Services.Dokumente
{

    public class DokumentService : IDokumentService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public DokumentService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        /// <summary>
        /// Dokumente in der Dokumentverwaltung suchen
        /// </summary>
        public async Task<IList<Dokument>> SearchAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var uri = "/Dokument/Search";
            var result = await store.FindAsync<IList<Dokument>>(uri, filter, ct).ConfigureAwait(false);
            return result?.OrderBy(p => p.Bezeichnung).ToList();
        }


        /// <summary>
        /// Datei eines Dokumentes nachladen
        /// </summary>
        public async Task<byte[]> GetContentAsync(int id, CancellationToken ct = default)
        {

            var uri = $"/Dokument/Raw/{id}";
            var result = await store.GetAsync<byte[]>(uri, ct);
            return result;
        }


        public async Task<Media> GetThumbnailAsync(Dokument model, CancellationToken ct = default)
        {

            var uri = $"/Dokument/Thumbnail/{model.Id}";

            try {

                var result = await store.GetAsync<byte[]>(uri, ct);
                return new Media(result, model.Filename, model.Id, 300);
            }
            catch {

                return null;
            }
        }


        /// <summary>
        /// bearbeitete Dokumente in der Dokumentverwaltung speichern
        /// </summary>
        public async Task<IList<Dokument>> SaveAsync(TrackableCollection<Dokument> list, CancellationToken ct = default)
        {

            var uri = "/Dokument";
            var originals = list.ToDictionary(p => p.Url);
            var dtolist = mapper.AsDtoList<Dokument, DokumentDto>(list);
            if (dtolist == null) return list;
            dtolist.ForEach(dto => { if (!dto.ChangedProps.Contains(nameof(DokumentDto.Content))) dto.Content = null; });

            var result = await store.PutAsync<List<DokumentDto>, IList<Dokument>>(uri, dtolist, ct).ConfigureAwait(false);
            result.ForEach(p => originals[p.Url] = p);
            return originals.Values.ToList();
        }


        /// <summary>
        /// Dokumente eines Entities lesen
        /// </summary>
        public async Task<IList<Dokument>> GetAssignedAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var uri = $"/Dokument/Assigned/";
            var models = await store.FindAsync<IList<Dokument>>(uri, filter, ct).ConfigureAwait(false);
            models = models?.OrderBy(p => p.Bezeichnung).ToList();
            models?.ForEach(p => p.AcceptChanges());
            return models;
        }


        /// <summary>
        /// einem Entity Dokumente zuweisen
        /// </summary>
        public Task SaveAssignedAsync(DocEntity model, CancellationToken ct = default)
            => SaveAssignedAsync(new List<DocEntity> { model }, ct);


        public async Task SaveAssignedAsync(IEnumerable<DocEntity> models, CancellationToken ct = default)
        {

            var uri = @"/Dokument/Assign/";

            var dtos = models.Select(p => new DokumentLinkDto { EntityType = p.EntityType,
                                                                Entity_Id = p.Id,
                                                                Link_Ids = p.Dokumente.Where(p => !p.IstAngefügt).Select(p => p.Id).ToList(),
                                                                Uploads = mapper.Map<List<DokumentDto>>(p.Dokumente.Where(p => p.IstAngefügt)),
                                                                Presenters = p.Dokumente.Where(p => p.PräsentiertModell).Select(p => p.Id).ToList()})
                             .ToList();

            await store.PutAsync<IEnumerable<DokumentLinkDto>>(uri, dtos, ct);
        }


        /// <summary>
        /// zugeordnete Entities eines Dokumentes in der Dokumentverwaltung zeigen
        /// </summary>
        public Task<IList<AssignedEntity>> AssignedEntitiesAsync(int documentId, CancellationToken ct = default)
            => store.GetAsync<IList<AssignedEntity>>($"/Dokument/Entities/{documentId}", ct);


        public Task<IList<Kategorie>> KategorienAsync(CancellationToken ct = default)
            => store.GetAsync<IList<Kategorie>>("/Dokument/Kategorie", ct);


        public Task<IList<String>> SchlagworteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<String>>("/Dokument/Schlagwort", ct);


        public Task<IHObjekt> GetIHObjektByMassnahmeIdAsync(int massnahmeId, CancellationToken ct = default)
            => store.GetAsync<IHObjekt>($"/Dokument/IHPerMassnahme/{massnahmeId}", ct);
    }
}
