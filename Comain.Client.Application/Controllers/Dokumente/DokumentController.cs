﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Dokumente;
using Comain.Client.Controllers.Stock.Artikelstamm;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Dokumente;
using Comain.Client.Controllers.WPlan;
using Microsoft.Win32;

namespace Comain.Client.Controllers.Dokumente
{

    public class DokumentController : ControllerBase, IDokumentController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IDokumentService store;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService config;
        private readonly IModulService modules;
        private readonly MainViewModel mainView;
        private readonly DokumentViewModel view;
        private readonly DokumentDetailViewModel detailView;
        private readonly DokumentListViewModel listView;
        private readonly DokumentFilterViewModel filterView;
        private SerializableCommand editCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand openCommand;
        private SerializableCommand downloadCommand;
        private SerializableCommand addTagCommand;
        private int currentId;


        public DokumentController(IBreadcrumbService crumbs, IDokumentService store, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                  IKonfigService config, IControllerBus bus, IModulService modules,
                                  MainViewModel mainView, DokumentViewModel view, DokumentDetailViewModel detailView,
                                  DokumentListViewModel listView, DokumentFilterViewModel filterView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.store = store;
            this.login = login;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.detailView = detailView;
            this.listView = listView;
            this.filterView = filterView;
            this.config = config;
            this.modules = modules;
        }


        public void Initialize()
        {

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => CancelAsync());
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.EditCommand = editCommand = CreateCommand(_ => ChangeFileAsync(), _ => detailView.Current != null);
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => detailView.Current != null);
            view.OpenCommand = openCommand = CreateCommand(_ => OpenFileAsync(), _ => detailView.Current != null);
            view.DownloadCommand = downloadCommand = CreateCommand(_ => DownloadAsync(), _ => detailView.Current != null);

            listView.DetailCommand = CreateCommand(_ => DetailAsync());
            listView.OpenCommand = CreateCommand(_ => DetailAndOpenAsync());
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());
            detailView.AddTagCommand = addTagCommand = CreateCommand(_ => AddTagAsync(), _ => detailView.Current != null);
            detailView.RemoveTagCommand = CreateCommand(text => RemoveTagAsync(text?.ToString()));
            detailView.OpenEntityCommand = CreateCommand(p => CallEntityAsync(p as AssignedEntityItem));
        }


        public void Shutdown()
        {}


        public async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            mainView.PageView = view.View;
            if (parameter is ControllerMessage) return;

            view.Mandant = login.AktuelleVerbindung;
            currentId = 0;
            crumbs.Add("Dokumente");
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            var kattask = store.KategorienAsync(bus.Token);
            var tagtask = store.SchlagworteAsync(bus.Token);
            await Task.WhenAll(kattask, tagtask).ConfigureAwait(false);

            await OnUIThreadAsync(() => {

                filterView.Load(kattask.Result, tagtask.Result);
                detailView.Load(kattask.Result, tagtask.Result);
            });
            bus.Token.ThrowIfCancellationRequested();

            await SearchAsync();
            UpdateCommands();
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (listView.HasChanges && !await messages.ConfirmExitAsync()) return false;

            view.Unload();
            detailView.Unload();
            listView.Unload();
            filterView.Unload();

            return true;
        }


        private void UpdateCommands()
        {

            OnUIThreadFireAndForget(() => {

                editCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                openCommand.RaiseCanExecuteChanged();
                downloadCommand.RaiseCanExecuteChanged();
                addTagCommand.RaiseCanExecuteChanged();

            });
        }


        private async Task BackAsync()
        {

            crumbs.Remove();
            await bus.BackAsync();
        }


        private async Task CancelAsync()
        {

            await LoadAsync();
            await DetailAsync();
        }


        private Task SaveAsync()
            => ServerCallAsync(store.SaveAsync(listView.Foundlist, bus.Token));


        private Task SearchAsync()
            => ServerCallAsync(store.SearchAsync(filterView.GebeFilter(), bus.Token));


        private async Task ServerCallAsync(Task<IList<Dokument>> call)
        {

            using (new VisualDelay(view)) {

                var current = detailView.Current;
                var result = await call.ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                await listView.LoadAsync(result);
                filterView.ShowResult(result);

                if (current != null && !listView.Foundlist.IsNullOrEmpty()) {

                    listView.Current = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                    await DetailAsync();

                } else {

                    detailView.Current = null;
                }
            }

            UpdateCommands();
        }


        private async Task DetailAsync()
        {

            detailView.Current = listView.Current;

            if (detailView.Current != null && !detailView.Current.IstAngefügt) {

                bus.CancelToken();
                using (new VisualDelay(detailView)) {

                    var assigned = await store.AssignedEntitiesAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
                    await OnUIThreadAsync(() => { detailView.Entities = assigned.Select(p => new AssignedEntityItem(p)).ToList(); });
                }
            }

            UpdateCommands();
        }


        private async Task CallEntityAsync(AssignedEntityItem entity)
        {

            if (entity == null) return;

            switch (entity.EntityType) {

                case "Auftrag":

                    if (!modules.Info.VerwendetModulAuftrag) return;
                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IAuftragController), Entity_Id = entity.EntityId, Source = this });
                    break;

                case "Maßnahme":

                    if (!modules.Info.VerwendetModulWPlan) return;
                    var ih = await store.GetIHObjektByMassnahmeIdAsync(entity.EntityId, bus.Token).ConfigureAwait(false);
                    await bus.JumpAsync(new IHObjektControllerMessage { Destination = typeof(IVorgabeController), Source = this, Entity_Id = entity.EntityId, 
                                                                        IHObjekt = ih, Massnahme_Id = entity.EntityId });
                    break;

                case "Asset":

                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IIHObjektController), Entity_Id = entity.EntityId, Source = this });
                    break;

                case "Personal":

                    var ps = new Personal { Id = entity.EntityId };
                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IVerwaltungController), Parameter = ps, Entity_Id = entity.EntityId, Source = this });
                    break;

                case "Artikel":

                    if (!modules.Info.VerwendetModulStock) return;
                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IArtikelController), Entity_Id = entity.EntityId, Source = this });
                    break;
            }
        }


        private async Task DetailAndOpenAsync()
        {

            await DetailAsync();
            await OpenFileAsync();
        }


        private async Task AddAsync()
        {

            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Dokumente wählen";

			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            var newlist = dialog.FileNames.Select(p => new Dokument(p, --currentId));
            await OnUIThreadAsync(() => { listView.Add(newlist); });

            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current == null) return;
            await OnUIThreadAsync(() => { listView.Remove(detailView.Current); });
            UpdateCommands();
        }


        private Task ChangeFileAsync()
        {

            if (detailView.Current == null) return Task.FromResult(true);

            var dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Ersetzendes Dokument wählen";

			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return Task.FromResult(true);
            detailView.Current.ChangeDocumentFile(dialog.FileName);

            UpdateCommands();
            return Task.FromResult(true);
        }


        private async Task OpenFileAsync()
        {

            if (detailView.Current == null) return;

            using (new VisualDelay(view)) {

                if (detailView.Current.Content == null) detailView.Current.SetDocumentFile(await store.GetContentAsync(detailView.Current.Id));
                await detailView.Current.StoreTempFileAsync();
                if (File.Exists(detailView.Current.TempPath)) System.Diagnostics.Process.Start(detailView.Current.TempPath);
                UpdateCommands();
            }
        }


        private async Task DownloadAsync()
        {

            if (detailView.Current == null) return;

            var dialog = new SaveFileDialog();
            dialog.Title = "Dokument speichern";
            dialog.FileName = detailView.Current.Filename;
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                if (detailView.Current.Content == null) {
                
                    var content = await store.GetContentAsync(detailView.Current.Id, bus.Token);
                    detailView.Current.SetDocumentFile(content);
                }
                await detailView.Current.StoreFileAsync(dialog.FileName);
            }
        }


        private async Task AddTagAsync()
        {

            var text = detailView.AddTagText?.Trim();

            if (detailView.Current == null) return;
            if (String.IsNullOrWhiteSpace(text)) return;
            if (detailView.ZugeordneteSchlagworte.Contains(text)) return;

            await OnUIThreadAsync(() => { detailView.AddSchlagwort(text); });
            detailView.AddTagText = null;
        }


        private async Task RemoveTagAsync(String text)
        {

            if (detailView.Current == null) return;
            if (String.IsNullOrWhiteSpace(text)) return;

            await OnUIThreadAsync(() => { detailView.RemoveSchlagwort(text); });
        }
    }
}
