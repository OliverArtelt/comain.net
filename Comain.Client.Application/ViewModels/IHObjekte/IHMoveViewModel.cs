﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.Services.IHObjekte;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHMoveViewModel : ViewModel<IIHMoveView>, IDelayableViewModel
    {

        protected readonly SynchronizationContext context = SynchronizationContext.Current;
        private readonly IIHObjektService store;
        private readonly IControllerBus bus;
        private readonly IMessageService messages;
        private readonly IHSelektorViewModel ihFilter;
        private MoveDestinationType moveType;
        private IHObjekt source;
        private IHMoveInfo info;


        public IHMoveViewModel(IHSelektorViewModel ihFilter, IIHMoveView view, IIHObjektService store,
                               IControllerBus bus, IMessageService messages)
          : base(view)
        {

            this.store = store;
            this.bus = bus;
            this.messages = messages;
            this.ihFilter = ihFilter;
            ihFilter.ClearButtonVisible = true;
            PropertyChangedEventManager.AddHandler(ihFilter, ViewPropertyChanged, "CurrentIHObjekt");
        }


        public async Task SetzeListenAsync(IEnumerable<KstFilterModel> kstlist, IEnumerable<PzFilterModel> pzlist,
                                           IEnumerable<IHListDto> ihlist, CancellationToken ct = default)
        {

            Standorte = pzlist.OrderBy(p => p.Nummer).ToList();
            Kostenstellen = kstlist.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).ToList();
            await ihFilter.LoadAsync(ihlist.OrderBy(p => p.Nummer).Select(p => new IHFilterItem(p)).ToList(), Standorte, Kostenstellen, ct);
        }


        public async Task SetRequestAsync(IHObjekt source, MoveDestinationType type = MoveDestinationType.Undefiniert, int destId = 0)
        {

            Kostenstelle = null;
            Standort = null;

            this.source = source;
            this.moveType = type;

            if (type == MoveDestinationType.IHObjekt)     ihFilter.SetModel(destId);
            if (type == MoveDestinationType.Standort)     Standort = Standorte.FirstOrDefault(p => p.Id == destId);
            if (type == MoveDestinationType.Kostenstelle) Kostenstelle = Kostenstellen.FirstOrDefault(p => p.Id == destId);

            RaisePropertyChanged(nameof(Titel));
            RaiseModeChanged();
            await UpdateStatusAsync();
        }


        private async Task UpdateStatusAsync()
        {

            try {

                info = null;
                if (ZielWurdeAngegeben) {

                    using (new VisualDelay(this)) {

                        info = await store.MoveInfoAsync(new IHMoveRequestDto { SourceId = source.Id, DestinationType = (int)moveType, DestinationId = ZielId.Value }, bus.Token)
                                          .ConfigureAwait(false);
                    }
                }

                KstMode = IHMoveKstMode.Undefiniert;
                if (info != null) KstMode = info.Umzugtyp == MoveDestinationType.Kostenstelle? IHMoveKstMode.Parent: IHMoveKstMode.Stays;
                AuMode = IHMoveAuftragMode.Offene;
                KostenstellenDesStandorts = null;
                if (info?.KstAmStandort != null) KostenstellenDesStandorts = Kostenstellen.Where(p => info.KstAmStandort.Any(q => q == p.Id)).ToList();

                RaisePropertyChanged(nameof(KostenstellenSichtbar));
                RaisePropertyChanged(nameof(AufträgeSichtbar));
                RaisePropertyChanged(nameof(KstParentSichtbar));
                RaisePropertyChanged(nameof(KstStandortSichtbar));
                RaisePropertyChanged(nameof(OffeneAufträge));
                RaisePropertyChanged(nameof(KostenstellenDesStandorts));
                RaisePropertyChanged(nameof(KstParentInfo));
                RaisePropertyChanged(nameof(KstStaysInfo));
                SetState();
            }
            catch (Exception x) { messages.Show(x); }
        }


        private bool ZielWurdeAngegeben => ZielId.HasValue;


        private int? ZielId
        {
            get {

                if (moveType == MoveDestinationType.IHObjekt) return ihFilter.CurrentIHObjekt?.Id;
                if (moveType == MoveDestinationType.Standort) return Standort?.Id;
                if (moveType == MoveDestinationType.Kostenstelle) return Kostenstelle?.Id;
                return null;
            }
        }


        private void SetState()
        {

            Statusinfo = null;
            RequestValid = false;

            if (!ZielWurdeAngegeben) {

                Statusinfo = new String[] { "Geben Sie das Ziel an." };
                return;
            }

            if (moveType == MoveDestinationType.IHObjekt && source.Nummer.StartsWith(ihFilter.CurrentIHObjekt?.Nummer)) {

                Statusinfo = new String[] { "Sie dürfen die Quelle nicht in sich selbst verschieben." };
                return;
            }

            RequestValid = true;
            Statusinfo = new String[] { InfoZiel, InfoNummer, InfoTeilobjekte, InfoMassnahmen, InfoOffeneAufträge, InfoÜbergebeneAufträge }
                                     .Where(p => p != null)
                                     .ToList();
        }


        public void Unload()
        {

            info = null;
            Kostenstellen = null;
            KostenstellenDesStandorts = null;
            Standorte = null;
            ihFilter.Unload();
        }


        private void RaiseModeChanged()
        {

            RaisePropertyChanged(nameof(ModeStandort));
            RaisePropertyChanged(nameof(ModeAsset));
            RaisePropertyChanged(nameof(ModeKostenstelle));
        }


        public IHMoveInstruction GetInstruction()
        {

            if (info == null) return null;

            var result = new IHMoveInstruction {

                SourceId        = info.SourceId,
                DestinationId   = info.DestinationId,
                DestinationType = (int)info.Umzugtyp,
                KstMode         = KstMode,
                AuftragMode     = AuMode
            };

            switch (KstMode) {

                case IHMoveKstMode.Stays:

                    result.KostenstellenId = info.KostenstelleAlt.Id;
                    break;

                case IHMoveKstMode.Parent:

                    if (info.KostenstelleNeu == null) throw new ComainOperationException("Die neue Kostenstelle konnte nicht zugeordnet werden.");
                    result.KostenstellenId = info.KostenstelleNeu.Id;
                    break;

                case IHMoveKstMode.Standort:

                    if (KostenstelleDesStandorts == null) throw new ComainOperationException("Die neue Kostenstelle wurde nicht angegeben.");
                    result.KostenstellenId = KostenstelleDesStandorts.Id;
                    break;

                case IHMoveKstMode.Frei:

                    if (Kostenstelle == null) throw new ComainOperationException("Die neue Kostenstelle wurde nicht angegeben.");
                    result.KostenstellenId = Kostenstelle.Id;
                    break;
            }

            if (KstMode != IHMoveKstMode.Stays && !OffeneAufträge.IsNullOrEmpty())
                result.UmziehendeAufträge = OffeneAufträge.Where(p => p.IsSelected).Select(p => p.Id).ToArray();

            return result;
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            UpdateStatusAsync();
        }


#region P R O P E R T I E S


        private IEnumerable<String> statusinfo;
        public IEnumerable<String> Statusinfo
        {
            get => statusinfo;
            set => SetProperty(ref statusinfo, value);
        }


        private bool requestValid;
        public bool RequestValid
        {
            get => requestValid;
            set { if (SetProperty(ref requestValid, value)) RaisePropertyChanged(nameof(StatusColor)); }
        }


        public String StatusColor => requestValid? "Black": "Coral";


        public bool ModeStandort
        {
            get => moveType == MoveDestinationType.Standort;
            set {

                if (value && moveType != MoveDestinationType.Standort) {

                    moveType = MoveDestinationType.Standort;
                    RaiseModeChanged();
                    UpdateStatusAsync();
                }
            }
        }


        public bool ModeAsset
        {
            get => moveType == MoveDestinationType.IHObjekt;
            set {

                if (value && moveType != MoveDestinationType.IHObjekt) {

                    moveType = MoveDestinationType.IHObjekt;
                    RaiseModeChanged();
                    UpdateStatusAsync();
                }
            }
        }


        public bool ModeKostenstelle
        {
            get => moveType == MoveDestinationType.Kostenstelle;
            set {

                if (value && moveType != MoveDestinationType.Kostenstelle) {

                    moveType = MoveDestinationType.Kostenstelle;
                    RaiseModeChanged();
                    UpdateStatusAsync();
                }
            }
        }


        public String Titel => source?.NummerUndName;


        private String InfoTeilobjekte
        {
            get {

                if (info == null) return null;
                return info.AnzahlTeilobjekte == 0? "• es wird kein Teilobjekt mit verschoben":
                       info.AnzahlTeilobjekte == 1? "• es wird ein Teilobjekt mit verschoben":
                       $"• es werden {info.AnzahlTeilobjekte} Teilobjekte mit verschoben";
            }
        }


        private String InfoNummer
        {
            get {

                if (info == null) return null;
                return info.AlteNummer == info.NeueNummer? $"• behält die Nummer {info.AlteNummer}":
                                                           $"• bekommt die neue Nummer {info.NeueNummer}";
            }
        }


        private String InfoZiel
        {
            get {

                if (info == null) return null;
                if (info.Umzugtyp == MoveDestinationType.Kostenstelle)  return $"• zieht zur Kostenstelle {info.NeuePosition}";
                if (info.Umzugtyp == MoveDestinationType.Standort)      return $"• zieht zum Standort {info.NeuePosition}";
                if (IHObjekt.IstMaschinennummer(info.NeueNummer))       return $"• wird neues Hauptobjekt";
                return $"• wird Teilobjekt von {info.NeuePosition}";
            }
        }


        private String InfoMassnahmen
        {
            get {

                if (info == null) return null;

                switch (info.AnzahlMassnahen) {
                    case 0: return "• das Objekt besitzt keine Maßnahmen";
                    case 1: return "• das Objekt besitzt eine Maßnahme";
                    default: return $"• das Objekt besitzt {info.AnzahlMassnahen} Maßnahmen";
                }
            }
        }


        private String InfoOffeneAufträge
        {
            get {

                if (info == null) return null;

                switch (AnzahlOffeneAufträge) {
                    case 0: return "• das Objekt besitzt keine offene Aufträge";
                    case 1: return "• das Objekt besitzt einen offenen Auftrag";
                    default: return $"• das Objekt besitzt {AnzahlOffeneAufträge} offene Aufträge";
                }
            }
        }


        private String InfoÜbergebeneAufträge
        {
            get {

                if (info == null) return null;

                switch (AnzahlÜbergebeneAufträge) {
                    case 0: return null;
                    case 1: return "• ein Auftrag wurde bereits übergeben";
                    default: return $"• {AnzahlÜbergebeneAufträge} Aufträge wurden bereits übergeben";
                }
            }
        }


        private String KstAltInfo            => info?.KostenstelleAlt == null? String.Empty: info.KostenstelleAlt.Nummer + " " + info.KostenstelleAlt.Name;
        private String KstNeuInfo            => info?.KostenstelleNeu == null? String.Empty: info.KostenstelleNeu.Nummer + " " + info.KostenstelleNeu.Name;
        public String KstStaysInfo           => $"Kostenstelle {KstAltInfo} beibehalten und diese ggfs. dem Standort zuordnen";
        public String KstParentInfo          => $"Kostenstelle {KstNeuInfo} des übergeordneten Objektes verwenden";
        public bool KostenstellenSichtbar    => requestValid && info != null && info.Umzugtyp != MoveDestinationType.Kostenstelle;
        public bool KstParentSichtbar        => KostenstellenSichtbar && info.KostenstelleNeu != null;
        public bool KstStandortSichtbar      => KostenstellenSichtbar && !info.KstAmStandort.IsNullOrEmpty();
        public bool AufträgeSichtbar         => requestValid && !KstStays && AnzahlOffeneAufträge > 0;
        private int AnzahlOffeneAufträge     => info?.OffeneAufträge == null? 0: info.OffeneAufträge.Count;
        private int AnzahlÜbergebeneAufträge => info?.OffeneAufträge == null? 0: info.OffeneAufträge.Count(p => p.ÜbergebenAm.HasValue);


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }


        private List<PzFilterModel> standorte;
        public List<PzFilterModel> Standorte
        {
            get => standorte;
            set => SetProperty(ref standorte, value);
        }


        private PzFilterModel standort;
        public PzFilterModel Standort
        {
            get => standort;
            set {

                if (SetProperty(ref standort, value)) UpdateStatusAsync();
            }
        }


        private List<KstFilterModel> kostenstellen;
        public List<KstFilterModel> Kostenstellen
        {
            get => kostenstellen;
            set => SetProperty(ref kostenstellen, value);
        }


        private List<KstFilterModel> kostenstellenDesStandorts;
        public List<KstFilterModel> KostenstellenDesStandorts
        {
            get => kostenstellenDesStandorts;
            set => SetProperty(ref kostenstellenDesStandorts, value);
        }


        private KstFilterModel kostenstelle;
        public KstFilterModel Kostenstelle
        {
            get => kostenstelle;
            set {

                if (SetProperty(ref kostenstelle, value)) UpdateStatusAsync();
            }
        }


        private KstFilterModel kostenstelleDesStandorts;
        public KstFilterModel KostenstelleDesStandorts
        {
            get => kostenstelleDesStandorts;
            set => SetProperty(ref kostenstelleDesStandorts, value);
        }


        public List<AuftragSelektor> OffeneAufträge => info?.OffeneAufträge;


        public ICommand SelectAllCommand        => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.Select(true)));
        public ICommand DeSelectAllCommand      => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.Select(false)));
        public ICommand SelectStandardCommand   => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectStandard(true)));
        public ICommand DeSelectStandardCommand => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectStandard(false)));
        public ICommand SelectPlanungCommand    => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectPlanung(true)));
        public ICommand DeSelectPlanungCommand  => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectPlanung(false)));
        public ICommand SelectTicketCommand     => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectTicket(true)));
        public ICommand DeSelectTicketCommand   => new DelegateCommand(_ => OffeneAufträge?.ForEach(p => p.SelectTicket(false)));


        private ICommand executeCommand;
        public ICommand ExecuteCommand
        {
            get => executeCommand;
            set => SetProperty(ref executeCommand, value);
        }


        private IHMoveKstMode kstMode;
        public IHMoveKstMode KstMode
        {
            get => kstMode;
            set {

                if (SetProperty(ref kstMode, value)) {

                    RaisePropertyChanged(nameof(KstStays));
                    RaisePropertyChanged(nameof(KstParent));
                    RaisePropertyChanged(nameof(KstStandort));
                    RaisePropertyChanged(nameof(KstFrei));
                    RaisePropertyChanged(nameof(KstParentSichtbar));
                    RaisePropertyChanged(nameof(KstStandortSichtbar));
                    RaisePropertyChanged(nameof(AufträgeSichtbar));
                }
            }
        }


        public bool KstStays
        {
            get => KstMode == IHMoveKstMode.Stays;
            set => KstMode = IHMoveKstMode.Stays;
        }


        public bool KstParent
        {
            get => KstMode == IHMoveKstMode.Parent;
            set => KstMode = IHMoveKstMode.Parent;
        }


        public bool KstStandort
        {
            get => KstMode == IHMoveKstMode.Standort;
            set => KstMode = IHMoveKstMode.Standort;
        }


        public bool KstFrei
        {
            get => KstMode == IHMoveKstMode.Frei;
            set => KstMode = IHMoveKstMode.Frei;
        }


        private IHMoveAuftragMode auMode;
        public IHMoveAuftragMode AuMode
        {
            get => auMode;
            set {

                if (SetProperty(ref auMode, value)) {

                    RaisePropertyChanged(nameof(AuStays));
                    RaisePropertyChanged(nameof(AuOffene));
                    RaisePropertyChanged(nameof(AuNichtÜbergebene));
                    RaisePropertyChanged(nameof(AuFrei));
                }
            }
        }


        public bool AuStays
        {
            get => AuMode == IHMoveAuftragMode.Stays;
            set => AuMode = IHMoveAuftragMode.Stays;
        }


        public bool AuOffene
        {
            get => AuMode == IHMoveAuftragMode.Offene;
            set => AuMode = IHMoveAuftragMode.Offene;
        }


        public bool AuNichtÜbergebene
        {
            get => AuMode == IHMoveAuftragMode.NichtÜbergebene;
            set => AuMode = IHMoveAuftragMode.NichtÜbergebene;
        }


        public bool AuFrei
        {
            get => AuMode == IHMoveAuftragMode.Frei;
            set => AuMode = IHMoveAuftragMode.Frei;
        }


        public object IHSelektor => ihFilter.View;


#endregion


    }
}
