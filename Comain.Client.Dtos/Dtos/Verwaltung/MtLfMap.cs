﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class MtLfMap : ChangeTrackingBase
    {
       
        public String   Name                    { get; set; }
        public String   Nummer                  { get; set; }
        public decimal? Preis                   { get; set; }
        public short?   Rank                    { get; set; }
        public decimal? Mindestbestellmenge     { get; set; }
        public int?     Bestellzeit             { get; set; }

        public int      Lieferant_Id            { get; set; }
        public int      Materialstamm_Id        { get; set; }
    }
}
