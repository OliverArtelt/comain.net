﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class ObjektgruppeDto : ChangeTrackingBase
    {

        public short     Nummer                  { get; set; }
        public String    Name                    { get; set; }
        public bool      IstGebäude              { get; set; }
    }
}
