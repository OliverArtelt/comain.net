﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.WPlan.Planen
{

    public class PositionStatistik
    {

        public int?     Leistungsart_Id     { get; set; }
        public int?     Gewerk_Id           { get; set; }
        public int?     Personal_Id         { get; set; }
        public int?     IHObjekt_Id         { get; set; }
        public int?     Massname_Id         { get; set; }

        public int?     Stunden             { get; set; }
        public decimal? Kosten              { get; set; }
        public bool     IstErledigt         { get; set; }
    }
}
