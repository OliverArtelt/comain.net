﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Dtos.Stock.Reports
{

    public class ArtikelReportDto
    {

        public int          ArtikelId           { get; set; }
        public String       Artikelnummer       { get; set; }
        public String       Artikelname         { get; set; }
        public String       Warengruppe         { get; set; }
        public String       Herstellernummer    { get; set; }
        
        public decimal?     Mindestbestand      { get; set; }
        public decimal?     Mindestbestellmenge { get; set; }
        public decimal?     Bestellt            { get; set; }
        public DateTime?    DeaktiviertAm       { get; set; }

        public IEnumerable<NummerFilterModel>   IHObjekte   { get; set; }
        public IEnumerable<LagerortReportDto>   Lager       { get; set; }
    }
}
