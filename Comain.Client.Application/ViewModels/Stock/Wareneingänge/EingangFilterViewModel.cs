﻿using System;
using System.Collections;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Wareneingänge;

namespace Comain.Client.ViewModels.Stock.Wareneingänge
{

    public class EingangFilterViewModel : ViewModel<IEingangFilterView>
    {

        private readonly SynchronizationContext context;


        public EingangFilterViewModel(IEingangFilterView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
          
            if (!String.IsNullOrEmpty(Auftrag))      filter.Add("Auftrag",      Auftrag);
            if (!String.IsNullOrEmpty(Bestellung))   filter.Add("Bestellung",   Bestellung);
            if (!String.IsNullOrEmpty(Lieferschein)) filter.Add("Lieferschein", Lieferschein);
            if (!String.IsNullOrEmpty(Artikel))      filter.Add("Artikel",      Artikel);
            
            if (Ungeprüft)  filter.Add("Ungeprüft", true);
            if (NurOffene)  filter.Add("NurOffene", true);
            if (Reklamiert) filter.Add("Reklamiert", true);
            
            if (DatumVon.HasValue)  filter.Add("BestelltVon", DatumVon.Value);
            if (DatumBis.HasValue)  filter.Add("BestelltBis", DatumBis.Value);
            if (TerminVon.HasValue) filter.Add("TerminVon", TerminVon.Value);
            if (TerminBis.HasValue) filter.Add("TerminBis", TerminBis.Value);
            
            return filter;
        }


        public void Unload()
        {

            Aufträge = null;
            Lieferscheine = null;
            Bestellungen = null;
        }


        public void Reset()
        {

            Auftrag = null;
            Bestellung = null;
            Lieferschein = null;
            Artikel = null;
            DatumVon = null;
            DatumBis = null;
            TerminBis = null;
            TerminVon = null;
            Ungeprüft = false;
            NurOffene = true;
            Reklamiert = false;
        }


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private String searchText;
        public String SearchText
        { 
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private DateTime? datumVon;
        public DateTime? DatumVon
        { 
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }


        private DateTime? datumBis;
        public DateTime? DatumBis
        { 
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }


        private DateTime? terminVon;
        public DateTime? TerminVon
        { 
            get { return terminVon; }
            set { SetProperty(ref terminVon, value); }
        }


        private DateTime? terminBis;
        public DateTime? TerminBis
        { 
            get { return terminBis; }
            set { SetProperty(ref terminBis, value); }
        }


        private IEnumerable aufträge;
        public IEnumerable Aufträge
        { 
            get { return aufträge; }
            set { SetProperty(ref aufträge, value); }
        }


        private IEnumerable bestellungen;
        public IEnumerable Bestellungen
        { 
            get { return bestellungen; }
            set { SetProperty(ref bestellungen, value); }
        }


        private IEnumerable lieferscheine;
        public IEnumerable Lieferscheine
        { 
            get { return lieferscheine; }
            set { SetProperty(ref lieferscheine, value); }
        }


        private String auftrag;
        public String Auftrag
        { 
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }


        private String bestellung;
        public String Bestellung
        { 
            get { return bestellung; }
            set { SetProperty(ref bestellung, value); }
        }


        private String lieferschein;
        public String Lieferschein
        { 
            get { return lieferschein; }
            set { SetProperty(ref lieferschein, value); }
        }


        private String artikel;
        public String Artikel
        { 
            get { return artikel; }
            set { SetProperty(ref artikel, value); }
        }


        private bool ungeprüft;
        public bool Ungeprüft
        { 
            get { return ungeprüft; }
            set { SetProperty(ref ungeprüft, value); }
        }


        private bool reklamiert;
        public bool Reklamiert
        { 
            get { return reklamiert; }
            set { SetProperty(ref reklamiert, value); }
        }


        private bool nurOffene = true;
        public bool NurOffene
        { 
            get { return nurOffene; }
            set { SetProperty(ref nurOffene, value); }
        }


#endregion

    }
}
