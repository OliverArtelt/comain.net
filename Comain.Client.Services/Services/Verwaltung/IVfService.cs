﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{

    public interface IVfService
    {
        
        bool HasChanges { get; }
        void Reset();
        Task SaveAsync(CancellationToken ct = default);

        Task<IList<IHSelektorItem>>                 GetIHListAsync(CancellationToken ct = default);
        Task<IList<DateTime>>                       GetVormonateAsync(CancellationToken ct = default);
        Task<IList<IHObjektBelegung>>               GetCopyAsync(int year, int month, CancellationToken ct = default);
        Task<TrackableCollection<IHObjektBelegung>> GetBelegungAsync(int year, int month, CancellationToken ct = default);
    }
}
