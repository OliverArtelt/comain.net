﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan
{

    public class EreignisDto
    {

        public int          PositionId                  { get; set; }
        public int          AuftragId                   { get; set; }
        public String       Auftragsnummer              { get; set; }
        public bool         AuftragIstStornierbar       { get; set; }

        public String       Auftragnehmer               { get; set; }
        public DateTime?    FälligAm                    { get; set; }
        public DateTime?    DurchgeführtAm              { get; set; }

        public int          Ergebnis                    { get; set; }
        public int?         MinutenGearbeitet           { get; set; }
        public String       FortgeführtAus              { get; set; }
    }
}
