﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class IHListDetailDto
    {

        public DateTime?    Datum       { get; set; }
        public String       Nummer      { get; set; }
        public String       Name        { get; set; }
        public String       Typ         { get; set; }
    }
}
