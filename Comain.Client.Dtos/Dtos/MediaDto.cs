﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos
{

    public class MediaDto : ChangeTrackingBase 
    {

        public String           Dateiname           { get; set; }
        public String           Mimetype            { get; set; }
        public byte[]           Image               { get; set; }
    }
}

