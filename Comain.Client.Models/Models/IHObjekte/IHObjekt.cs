﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.Specs;
using Comain.Client.Models.Stock;

namespace Comain.Client.Models.IHObjekte
{

    [DebuggerDisplay("{Nummer}")]
    public class IHObjekt : DocEntity, IDataErrorInfo
    {

        private String                  nummer                ;
        private String                  name                  ;
        private String                  inventarnummer        ;
        private String                  bauteil               ;
        private String                  baugruppe             ;
        private String                  fabriknummer          ;
        private String                  scannummer;
        private String                  typ                   ;
        private String                  anlage                ;
        private short?                  baujahr               ;
        private short?                  anschaffungsjahr      ;
        private decimal?                anschaffungswert      ;
        private decimal?                wiederbeschaffungswert;
        private Erhaltklasse?           erhaltklasse          ;
        private IHObjektStatus?         status                ;
        private String                  technischeDaten       ;
        private String                  bemerkungen           ;
        private String                  ihwnummer             ;
        private bool?                   istAktiv              ;
        private DateTime?               deaktiviertAm         ;
        private double?                 gpsLongitude         ;
        private double?                 gpsLatitude         ;

        private KstFilterModel          kostenstelle          ;
        private PzFilterModel           standort              ;
        private NummerFilterModel       objektgruppe          ;
        private SubNummerFilterModel    objektart             ;
        private LieferantFilterModel    instandhalter         ;
        private LieferantFilterModel    lieferant             ;
        private LieferantFilterModel    hersteller            ;
        private PersonalFilterModel     verantwortlicherInstandhaltung;
        private PersonalFilterModel     verantwortlicherProduktion;
        private SpecFilterModel         spezifikation;

        private Dictionary<String, HashSet<TechnischerWert>> werteTabGruppen;
        private static readonly HashSet<String> tabGruppen = new HashSet<String> { "Base data", "TechSpecs", "Measures", "Material", "Administration", "Consumption/Equipment", "Remarks", "Wartungsplan" };


        public override String EntityType => "IH";
        public String   NummerUndName     => $"{Nummer} - {Name}";


        public override bool IsChanged => base.IsChanged || TechnischeWerte.Any(p => p.IsChanged) || MaterialIHObjektMap.IsChanged;


        public override void AcceptChanges()
        {

            base.AcceptChanges();
            TechnischeWerte.ForEach(p => p.AcceptChanges());
            MaterialIHObjektMap.AcceptChanges();
            Personalliste.AcceptChanges();
        }


        private List<TechnischerWert> technischeWerte;
        public List<TechnischerWert> TechnischeWerte
        {
            get => technischeWerte ?? (technischeWerte = new List<TechnischerWert>());
            set {

                if (SetProperty(ref technischeWerte, value)) {

                    werteTabGruppen = technischeWerte?.Select(p => (key: tabGruppen.Contains(p.AssetTab)? p.AssetTab: "TechSpecs", value:p))
                                                      .ToMultiDictionary(p => p.key, p => p.value);
                }
            }
        }


        public IEnumerable<TechnischerWert> BaseSpecs        => werteTabGruppen?.GetValueOrDefault("Base data")?.ToList();
        public IEnumerable<TechnischerWert> MeasureSpecs     => werteTabGruppen?.GetValueOrDefault("Measures")?.ToList();
        public IEnumerable<TechnischerWert> MaterialSpecs    => werteTabGruppen?.GetValueOrDefault("Material")?.ToList();
        public IEnumerable<TechnischerWert> AdminSpecs       => werteTabGruppen?.GetValueOrDefault("Administration")?.ToList();
        public IEnumerable<TechnischerWert> ConsumptionSpecs => werteTabGruppen?.GetValueOrDefault("Consumption/Equipment")?.ToList();
        public IEnumerable<TechnischerWert> RemarkSpecs      => werteTabGruppen?.GetValueOrDefault("Remarks")?.ToList();
        public IEnumerable<TechnischerWert> PlanSpecs        => werteTabGruppen?.GetValueOrDefault("Wartungsplan")?.ToList();
        public IEnumerable<TechnischerWert> RemainingSpecs   => werteTabGruppen?.GetValueOrDefault("TechSpecs")?.ToList();


        private TrackableCollection<ArtikelSelectModel> materialIHObjektMap;
        public TrackableCollection<ArtikelSelectModel> MaterialIHObjektMap
        {
            get => materialIHObjektMap ?? (materialIHObjektMap = new TrackableCollection<ArtikelSelectModel>());
            set => SetProperty(ref materialIHObjektMap, value);
        }


        private TrackableCollection<IHObjektPersonal> personalliste;
        public TrackableCollection<IHObjektPersonal> Personalliste
        {
            get => personalliste ?? (personalliste = new TrackableCollection<IHObjektPersonal>());
            set => SetProperty(ref personalliste, value);
        }


#region P R O P E R T I E S


        public IEnumerable<IHPzDto>     IHObjektPzMap                   { get; set; }
        public IEnumerable<IHKstDto>    IHObjektKstMap                  { get; set; }


        public String                   Nummer                          { get { return nummer                   ; } set { SetProperty(ref nummer                , value); } }
        public String                   Name                            { get { return name                     ; } set { SetProperty(ref name                  , value); } }
        public String                   Inventarnummer                  { get { return inventarnummer           ; } set { SetProperty(ref inventarnummer        , value); } }
        public String                   Bauteil                         { get { return bauteil                  ; } set { SetProperty(ref bauteil               , value); } }
        public String                   Baugruppe                       { get { return baugruppe                ; } set { SetProperty(ref baugruppe             , value); } }
        public String                   Fabriknummer                    { get { return fabriknummer             ; } set { SetProperty(ref fabriknummer          , value); } }
        public String                   Scannummer                      { get { return scannummer               ; } set { SetProperty(ref scannummer            , value); } }
        public String                   Typ                             { get { return typ                      ; } set { SetProperty(ref typ                   , value); } }
        public String                   Anlage                          { get { return anlage                   ; } set { SetProperty(ref anlage                , value); } }
        public short?                   Baujahr                         { get { return baujahr                  ; } set { SetProperty(ref baujahr               , value); } }
        public short?                   Anschaffungsjahr                { get { return anschaffungsjahr         ; } set { SetProperty(ref anschaffungsjahr      , value); } }
        public decimal?                 Anschaffungswert                { get { return anschaffungswert         ; } set { SetProperty(ref anschaffungswert      , value); } }
        public decimal?                 Wiederbeschaffungswert          { get { return wiederbeschaffungswert   ; } set { SetProperty(ref wiederbeschaffungswert, value); } }
        public Erhaltklasse?            Erhaltklasse                    { get { return erhaltklasse             ; } set { SetProperty(ref erhaltklasse          , value); } }
        public IHObjektStatus?          Status                          { get { return status                   ; } set { SetProperty(ref status                , value); } }
        public String                   WNummer                         { get { return ihwnummer                ; } set { SetProperty(ref ihwnummer             , value); } }
        public String                   TechnischeDaten                 { get { return technischeDaten          ; } set { SetProperty(ref technischeDaten       , value); } }
        public String                   Bemerkungen                     { get { return bemerkungen              ; } set { SetProperty(ref bemerkungen           , value); } }
        public bool?                    IstAktiv                        { get { return istAktiv                 ; } set { SetProperty(ref istAktiv              , value); } }
        public DateTime?                DeaktiviertAm                   { get { return deaktiviertAm            ; } set { SetProperty(ref deaktiviertAm         , value); } }
        public double?                  GpsLongitude                    { get { return gpsLongitude             ; } set { SetProperty(ref gpsLongitude         , value); } }
        public double?                  GpsLatitude                     { get { return gpsLatitude              ; } set { SetProperty(ref gpsLatitude         , value); } }

        public KstFilterModel           Kostenstelle                    { get { return kostenstelle             ; } set { SetProperty(ref kostenstelle          , value); } }
        public PzFilterModel            Standort                        { get { return standort                 ; } set { SetProperty(ref standort              , value); } }
        public NummerFilterModel        Objektgruppe                    { get { return objektgruppe             ; } set { SetProperty(ref objektgruppe          , value); } }
        public SubNummerFilterModel     Objektart                       { get { return objektart                ; } set { SetProperty(ref objektart             , value); } }
        public LieferantFilterModel     Instandhalter                   { get { return instandhalter            ; } set { SetProperty(ref instandhalter         , value); } }
        public LieferantFilterModel     Lieferant                       { get { return lieferant                ; } set { SetProperty(ref lieferant             , value); } }
        public LieferantFilterModel     Hersteller                      { get { return hersteller               ; } set { SetProperty(ref hersteller            , value); } }
        public PersonalFilterModel      VerantwortlicherInstandhaltung  { get { return verantwortlicherInstandhaltung; } set { SetProperty(ref verantwortlicherInstandhaltung, value); } }
        public PersonalFilterModel      VerantwortlicherProduktion      { get { return verantwortlicherProduktion; }     set { SetProperty(ref verantwortlicherProduktion, value); } }
        public SpecFilterModel          Spezifikation                   { get { return spezifikation; } set { SetProperty(ref spezifikation, value); } }

        public Konfiguration            Konfiguration                   { get; set; }


#endregion


        public IHObjekt ErstelleTeilobjekt()
        {

            var ih = new IHObjekt {

                Nummer                 = this.Nummer + "-xx",
                Baujahr                = this.Baujahr,
                Anschaffungsjahr       = this.Anschaffungsjahr,
                Erhaltklasse           = this.Erhaltklasse,
                Status                 = this.Status,
                IstAktiv               = this.IstAktiv,
                DeaktiviertAm          = this.DeaktiviertAm,
                GpsLongitude           = this.GpsLongitude,
                GpsLatitude            = this.GpsLatitude,
                Kostenstelle           = this.Kostenstelle,
                Standort               = this.Standort,
                Objektgruppe           = this.Objektgruppe,
                Objektart              = this.Objektart,
                Instandhalter          = this.Instandhalter,
                Lieferant              = this.Lieferant,
                Hersteller             = this.Hersteller
            };

            if (Konfiguration.MaximaleBauteilTiefe.HasValue) {

                int btcnt = ih.Nummer.Count(p => p == '-');
                if (btcnt - 3 > Konfiguration.MaximaleBauteilTiefe.Value) throw new ComainOperationException("Das Asset kann nicht erstellt werden da die Assetnummer zu stark verschachtelt wird.");
            }

            return ih;
        }


        public IHObjekt ErstelleGeschwister()
        {

            if (String.IsNullOrEmpty(Nummer)) throw new ComainOperationException("Das Asset kann nicht kopiert werden da die Assetnummer unvollständig ist.");
            var ihnrlist = Nummer.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (ihnrlist.Length < 4) throw new ComainOperationException("Das Asset kann nicht kopiert werden da die Assetnummer unvollständig ist.");
            ihnrlist[ihnrlist.Length - 1] = "xx";

            var ih = new IHObjekt {

                Nummer                          = String.Join("-", ihnrlist),
                Name                            = this.Name,
                Inventarnummer                  = this.Inventarnummer,
                Bauteil                         = this.Bauteil,
                Baugruppe                       = this.Baugruppe,
                Fabriknummer                    = this.Fabriknummer,
                Scannummer                      = this.Scannummer,
                Typ                             = this.Typ,
                Anlage                          = this.Anlage,
                Baujahr                         = this.Baujahr,
                Anschaffungsjahr                = this.Anschaffungsjahr,
                Anschaffungswert                = this.Anschaffungswert,
                Wiederbeschaffungswert          = this.Wiederbeschaffungswert,
                Erhaltklasse                    = this.Erhaltklasse,
                Status                          = this.Status,
                WNummer                         = this.WNummer,
                TechnischeDaten                 = this.TechnischeDaten,
                Bemerkungen                     = this.Bemerkungen,
                IstAktiv                        = this.IstAktiv,
                DeaktiviertAm                   = this.DeaktiviertAm,
                Kostenstelle                    = this.Kostenstelle,
                Standort                        = this.Standort,
                Objektgruppe                    = this.Objektgruppe,
                Objektart                       = this.Objektart,
                Instandhalter                   = this.Instandhalter,
                Lieferant                       = this.Lieferant,
                Hersteller                      = this.Hersteller,
                Konfiguration                   = this.Konfiguration,
                Spezifikation                   = this.Spezifikation,
                VerantwortlicherInstandhaltung  = this.VerantwortlicherInstandhaltung,
                VerantwortlicherProduktion      = this.VerantwortlicherProduktion,
                GpsLongitude                    = this.GpsLongitude,
                GpsLatitude                     = this.GpsLatitude,
                Dokumente                       = new TrackableCollection<Dokument>(this.Dokumente)
            };

            return ih;
        }


        public bool IstMaschine => !String.IsNullOrEmpty(Nummer) && Nummer.Count(p => p == '-') == 3;


        public static int Baugruppenlevel(String nummer) => String.IsNullOrEmpty(nummer)? 0: nummer.Split('-').Length - 4;


        public static bool IstMaschinennummer(String nummer)
            => !String.IsNullOrEmpty(nummer) && nummer.Count(p => p == '-') == 3;


        public static String GebeMaschinennummer(String baugruppe)
        {

            if (String.IsNullOrEmpty(baugruppe)) return null;
            if (baugruppe.Count(p => p == '-') < 3) return null;
            return String.Join("-", baugruppe.Split('-').Take(4).ToArray());
        }


        public static String GebeParentnummer(String baugruppe)
        {

            if (String.IsNullOrEmpty(baugruppe)) return null;
            if (baugruppe.Count(p => p == '-') < 4) return null;
            return baugruppe.Substring(0, baugruppe.LastIndexOf('-'));
        }


        public IHListDto AsListDto()
            => new IHListDto { Id              = Id,
                               Nummer          = Nummer,
                               Name            = Name,
                               Inventarnummer  = Inventarnummer,
                               Bauteil         = Bauteil,
                               Baugruppe       = Baugruppe,
                               Fabriknummer    = Fabriknummer,
                               Scannummer      = Scannummer,
                               Typ             = Typ,
                               Anlage          = Anlage,
                               TechnischeDaten = TechnischeDaten,
                               Bemerkungen     = Bemerkungen,
                               Kostenstelle_Id = Kostenstelle.Id,
                               Standort_Id     = Standort.Id,
                               Erhaltklasse    = (int?)Erhaltklasse,
                               Status          = (int?)Status  };


#region V A L I D A T I O N


        public virtual IList<String> ErrorList
        {

            get {

                var result = new List<string>();

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Inventarnummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Bauteil"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Baugruppe"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Fabriknummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Scannummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Typ"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Anlage"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["WNummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["DatumStandort"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["DatumKostenstelle"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Standort"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kostenstelle"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Objektgruppe"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Objektart"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string Error
        {

            get {

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Inventarnummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Bauteil"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Baugruppe"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Fabriknummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Scannummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Typ"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Anlage"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["WNummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["DatumStandort"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["DatumKostenstelle"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Standort"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kostenstelle"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Objektgruppe"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Objektart"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung des Assets an.";
                    if (Name.Length > 500) return "Die Bezeichnung darf aus maximal 500 Zeichen bestehen.";
                    break;

                case "Inventarnummer":

                    if (Konfiguration.InventarnummernPflicht && String.IsNullOrWhiteSpace(Inventarnummer)) return "Die Inventarnummer muß angegeben werden.";
                    if (String.IsNullOrWhiteSpace(Inventarnummer)) return null;
                    if (Inventarnummer.Length > 254) return "Die Inventarnummer darf aus maximal 254 Zeichen bestehen.";
                    break;

                case "Bauteil":

                    if (String.IsNullOrWhiteSpace(Bauteil)) return null;
                    if (Bauteil.Length > 150) return "Das Bauteil darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Baugruppe":

                    if (String.IsNullOrWhiteSpace(Baugruppe)) return null;
                    if (Baugruppe.Length > 150) return "Die Baugruppe darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Fabriknummer":

                    if (Konfiguration.FabriknummernPflicht && String.IsNullOrWhiteSpace(Fabriknummer)) return "Die Fabriknummer muß angegeben werden.";
                    if (String.IsNullOrWhiteSpace(Fabriknummer)) return null;
                    if (Fabriknummer.Length > 150) return "Die Fabriknummer darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Scannummer":

                    if (String.IsNullOrWhiteSpace(Scannummer)) return null;
                    if (Scannummer.Length > 150) return "Die Scannummer darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Typ":

                    if (String.IsNullOrWhiteSpace(Typ)) return null;
                    if (Typ.Length > 254) return "Der Typ darf aus maximal 254 Zeichen bestehen.";
                    break;

                case "Anlage":

                    if (String.IsNullOrWhiteSpace(Anlage)) return null;
                    if (Anlage.Length > 150) return "Die Anlage darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "WNummer":

                    if (String.IsNullOrWhiteSpace(WNummer)) return null;
                    if (WNummer.Length > 24) return "Die W-Auftragsnummer darf aus maximal 24 Zeichen bestehen.";
                    break;

                case "Kostenstelle":

                    if (Kostenstelle == null) return "Geben Sie die Kostenstelle an.";
                    break;

                case "Standort":

                    if (Standort == null) return "Geben Sie den Standort an.";
                    break;

                case "Objektgruppe":

                    if (Objektgruppe == null) return "Geben Sie die Objektgruppe an.";
                    break;

                case "Objektart":

                    if (Objektart == null) return "Geben Sie die Objektart an.";
                    break;
                }

                return null;
            }
        }


#endregion

    }
}
