﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Comain.Client.Views.Internal
{

    public class DateTextInfoPicker : DateTextPicker
    {

        static DateTextInfoPicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DateTextInfoPicker), new FrameworkPropertyMetadata(typeof(DateTextInfoPicker)));
        }


        private TextBlock weekText;
        private TextBlock distanceText;
        private DatePicker picker;


        public override void OnApplyTemplate()
        {

            picker = this.GetTemplateChild("PART_Picker") as DatePicker;
            weekText = this.GetTemplateChild("PART_Week") as TextBlock;
            distanceText = this.GetTemplateChild("PART_Distance") as TextBlock;

            picker.SelectedDateChanged += (s, e) => SelectedDate = picker.SelectedDate;
            SelectedDateChanged += (s, e) => SynchronizeControls();
            picker.CalendarClosed += (s, e) => SynchronizeControls();
            picker.CalendarOpened += (s, e) => base.OnCalendarOpened(e);
            picker.DateValidationError += (s, e) => base.OnDateValidationError(e);
            DataContextChanged += (s, e) => SynchronizeControls();
            SynchronizeControls();

            base.OnApplyTemplate();
        }


        protected virtual void SynchronizeControls()
        {

            if (weekText != null) weekText.Text = CalculateWeek();
            if (distanceText != null) distanceText.Text = CalculateDistance();
            if (picker != null && picker.SelectedDate != SelectedDate) picker.SelectedDate = SelectedDate;
        }


        protected virtual String CalculateDistance()
        {

            if (!SelectedDate.HasValue) return null;

            var period = SelectedDate.Value.Date.TotalsBetween(DateTime.Today);
            if (period.years  >  1) return $"vor {period.years} Jahren";
            if (period.years  < -1) return $"in {Math.Abs(period.years)} Jahren";

            if (period.months >  2) return $"vor {period.months} Monaten";
            if (period.months < -2) return $"in {Math.Abs(period.months)} Monaten";

            if (period.weeks  >  2) return $"vor {period.weeks} Wochen";
            if (period.weeks  < -2) return $"in {Math.Abs(period.weeks)} Wochen";

            if (period.days ==  0) return "heute";
            if (period.days ==  1) return "gestern";
            if (period.days == -1) return "morgen";
            if (period.days ==  2) return "vorgestern";
            if (period.days == -2) return "übermorgen";
            if (period.days >   2) return $"vor {period.days} Tagen";
            if (period.days <  -2) return $"in {Math.Abs(period.days)} Tagen";

            return null;
        }


        protected virtual String CalculateWeek()
        {

            if (!SelectedDate.HasValue) return null;

            String result = String.Format("KW {0:D2}", SelectedDate.Value.WeekOfYear());
            return result;
        }
    }
}
