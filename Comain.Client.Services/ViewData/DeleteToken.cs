﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData
{
    
    public class DeleteToken : MessageToken
    {

        public bool    IsDeleteable    { get; set; }
    }
}
