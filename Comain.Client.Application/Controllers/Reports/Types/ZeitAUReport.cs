﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class ZeitAUReport : ReportBase, IReport
    {

        public ZeitAUReport(ZeitAUViewModel zeitView, PersonalViewModel personView)
        {
            filterViews = new List<IFilterViewModel> { zeitView, personView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            await Task.Yield();
            return Create();
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Zeit in geplanten und ungeplanten Aufträgen"; }
        }


        public string Gruppe
        {
            get { return null; }
        }


        public int Sortierung
        {
            get { return 600; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.ZeitAUReport.rdlc"; }
        }
    }
}
