﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{
    
    public class Bestellposition : Entity, IDataErrorInfo
    {
      
        private int?         position;       
        private decimal      menge;          
        private decimal?     preis;          
        private int          material_Id;    
        private String       lfartikelnummer;  
        private String       artikelnummer;  
        private String       artikelname;    
        private String       einheit;    


        public decimal? GPreis
        {
            get { 
            
                if (!Preis.HasValue) return null;
                return Menge * Preis.Value; 
            }
        } 


        public String PositionUndNummer
        { 
            get { 
            
                if (!position.HasValue) return null;
                if (Bestellung == null) return null;
                return String.Format("{0} - {1}", Bestellung.Nummer, position.Value); 
            }
        }


#region P R O P E R T I E S

        
        public Bestellung Bestellung    { get; set; }


        public int? Position       
        {
            get { return position; }
            set { SetProperty(ref position, value); }
        }


        public decimal Menge          
        {
            get { return menge; }
            set { if (SetProperty(ref menge, value)) RaisePropertyChanged("GPreis"); }
        }


        public decimal? Preis          
        {
            get { return preis; }
            set { if (SetProperty(ref preis, value)) RaisePropertyChanged("GPreis"); }
        }


        public int Material_Id    
        {
            get { return material_Id; }
            set { SetProperty(ref material_Id, value); }
        }


        public String LfArtikelnummer  
        {
            get { return lfartikelnummer; }
            set { SetProperty(ref lfartikelnummer, value); }
        }


        public String Artikelnummer  
        {
            get { return artikelnummer; }
            set { SetProperty(ref artikelnummer, value); }
        }


        public String Artikelname    
        {
            get { return artikelname; }
            set { SetProperty(ref artikelname, value); }
        }


        public String Einheit    
        {
            get { return einheit; }
            set { SetProperty(ref einheit, value); }
        }  


#endregion
 

#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Material_Id"];
                return test;
            }
        }

       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Material_Id"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Menge":

                    if (Menge <= 0m) return "Jede Position muss eine Menge größer 0 aufweisen."; 
                    break;

                case "Material_Id":

                    if (Material_Id == 0) return "Jeder Position muss ein Artikel zugewiesen worden sein."; 
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
