﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class RKReportDto
    {

        public int          ReklamId            { get; set; }  
        public String       Bestellnummer       { get; set; }  
        public String       Lieferant           { get; set; }  
        public String       Lieferschein        { get; set; }  
        public DateTime?    Bestelldatum        { get; set; }  
        public DateTime     Lieferdatum         { get; set; }  
        public DateTime?    ReklamiertAm        { get; set; }  
        public int?         Bestellposition     { get; set; }  
        public int          Reklamposition      { get; set; }  
        public String       Artikelnummer       { get; set; }  
        public String       LfArtikelnummer     { get; set; }  
        public String       Artikelname         { get; set; }  
        public decimal      Bestellmenge        { get; set; }  
        public decimal      Liefermenge         { get; set; }  
        public decimal      Reklammenge         { get; set; }  
    }
}
