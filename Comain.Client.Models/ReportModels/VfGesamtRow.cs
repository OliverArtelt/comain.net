﻿using System;
using Comain.Client.Dtos.Reports;

namespace Comain.Client.ReportModels
{

    public class VfGesamtRow
    {

        public String       IHObjekt            { get; set; }
        public String       Maschine            { get; set; }
        public decimal?     Belegungszeit       { get; set; }
        public decimal?     Ausfallzeit         { get; set; }
        public decimal?     Störzeit            { get; set; }
        public String       IHStatusString      { get; set; }
        public int          SortierungStatus    { get; set; }
        public decimal?     Verfügbarkeit       { get; set; }
        public decimal?     Ausfallrate         { get; set; }
        public decimal?     Abweichungsgrad     { get; set; }

        
        public VfGesamtRow()
        {}

        
        public VfGesamtRow(VfGesamtDto dto)
        {
        
            IHObjekt         = dto.IHObjekt;         
            Maschine         = dto.Maschine;         
            Belegungszeit    = dto.Belegungszeit;    
            Ausfallzeit      = dto.Ausfallzeit;      
            Störzeit         = dto.Störzeit;         
            Verfügbarkeit    = dto.Verfügbarkeit;    
            Ausfallrate      = dto.Ausfallrate;      
            Abweichungsgrad  = dto.Abweichungsgrad;
            
            switch (dto.IHStatus.GetValueOrDefault()) {
            
            case 1:

                IHStatusString = "sehr hoch";
                SortierungStatus = 1;   
                break;
            
            case 2:

                IHStatusString = "hoch";
                SortierungStatus = 2;   
                break;
            
            case 3:

                IHStatusString = "gering";
                SortierungStatus = 3;   
                break;
            
            case 4:

                IHStatusString = "keine";
                SortierungStatus = 4;   
                break;
            
            default:

                IHStatusString = "nicht angegeben";
                SortierungStatus = 5;   
                break;
            }
        }
    }
}
