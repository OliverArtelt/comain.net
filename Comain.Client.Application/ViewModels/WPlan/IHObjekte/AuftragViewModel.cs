﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Views.WPlan.IHObjekte;
using Comain.Client.ViewData.WPlan;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.WPlan;

namespace Comain.Client.ViewModels.WPlan.IHObjekte
{

    public class AuftragViewModel : ViewModel<IAuftragView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public AuftragViewModel(IAuftragView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            Von = DateTime.Today;
            Bis = Von.Value.AddYears(1);

            SelectAllCommand   = new DelegateCommand(_ => { WählbareListe.Where(p => p.AuswahlSichtbar)
                                                                         .ForEach(p => p.IstGewählt = true); });
            SelectFreeCommand  = new DelegateCommand(_ => { WählbareListe.Where(p => p.AuswahlSichtbar)
                                                                         .Where(p => String.IsNullOrEmpty(p.PersonalAsString))
                                                                         .ForEach(p => p.IstGewählt = true); });
            DeselectAllCommand = new DelegateCommand(_ => { WählbareListe.Where(p => p.AuswahlSichtbar)
                                                                         .ForEach(p => p.IstGewählt = false); });

            PsSearchProvider = searcher;
        }


        public void Unload()
        {

            Foundlist = null;
            PsSearchProvider.ClearItems();
        }


        public WPlanCreateInfo GetFilter() => new WPlanCreateInfo { StartDate = Von, EndDate = Bis };


        public List<String> ValidateFilter()
        {

            var result = new List<String>();

            if (!Von.HasValue) result.Add("Geben Sie das Startdatum der Planung an.");
            if (!Bis.HasValue) result.Add("Geben Sie das Enddatum der Planung an.");
            if (Von > Bis) result.Add("Das Startdatum darf nicht jünger als das Enddatum sein.");
            if (Von < DateTime.Today.MondayOf()) result.Add("Das Startdatum darf nicht älter als der Montag der aktuellen Woche sein.");

            return result;
        }


        public bool HatPlanung => !Foundlist.IsNullOrEmpty();


        public async Task SetAuftraglistAsync(IEnumerable<AuftragViewItem> aulist)
        {

            var tracklist = new TrackableCollection<AuftragViewItem>(aulist);

            await context.SendAsync(new SendOrPostCallback((o) => {

                Foundlist = tracklist;
            }), null);

            RaisePropertyChanged(nameof(Auftragnehmer));
        }


        public void AuftragnehmerZuordnen() => WählbareListe.Where(p => p.IstGewählt).ForEach(p => p.Auftragnehmer = Auftragnehmer);


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private PersonalFilterModel auftragnehmer;
        public PersonalFilterModel Auftragnehmer
        {
            get { return auftragnehmer; }
            set { SetProperty(ref auftragnehmer, value); }
        }


        private IEnumerable<AuftragViewItem> WählbareListe => Foundlist.IsNullOrEmpty() ? new List<AuftragViewItem>() :
                                                                                          Foundlist.Where(p => p.AuswahlSichtbar).ToList();

        private TrackableCollection<AuftragViewItem> foundlist;
        public TrackableCollection<AuftragViewItem> Foundlist
        {
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private AuftragViewItem currentAuftrag;
        public AuftragViewItem CurrentAuftrag
        {
            get { return currentAuftrag; }
            set { SetProperty(ref currentAuftrag, value); }
        }


        public ICommand SelectAllCommand    { get; }
        public ICommand SelectFreeCommand   { get; }
        public ICommand DeselectAllCommand  { get; }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private DateTime? von;
        public DateTime? Von
        {
            get { return von; }
            set { SetProperty(ref von, value); }
        }


        private DateTime? bis;
        public DateTime? Bis
        {
            get { return bis; }
            set { SetProperty(ref bis, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private object progressView;
        public object ProgressView
        {
            get { return progressView; }
            set { SetProperty(ref progressView, value); }
        }


        private bool wplanStatusVisible;
        public bool WPlanStatusVisible
        {
            get { return wplanStatusVisible; }
            set { SetProperty(ref wplanStatusVisible, value); }
        }


#endregion

    }
}
