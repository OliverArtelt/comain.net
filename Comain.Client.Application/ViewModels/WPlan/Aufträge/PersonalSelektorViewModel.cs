﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.WPlan.Aufträge;


namespace Comain.Client.ViewModels.WPlan.Aufträge
{

    public class PersonalSelektorViewModel : ViewModel<IPersonalSelektorView>
    {

        private readonly SynchronizationContext context;

        public ChecklistViewItem Current   { get; private set; }


        public PersonalSelektorViewModel(IPersonalSelektorView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ResetFilterCommand = new DelegateCommand(_ => { Suchtext = null;
                                                            AktuellesGewerk = null; });
            PersonAddCommand = new DelegateCommand(Add);
            PersonRemoveCommand = new DelegateCommand(Remove);
            PersonClearCommand = new DelegateCommand(Clear);
        }


        public void Unload()
        {

            Gewerke = null;
            AllePersonen = null;
            Current = null;
        }


        public async Task SetListAsync(IList<PersonalFilterModel> pslist, IList<NummerFilterModel> gwlist)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                AllePersonen = pslist;
                Gewerke = gwlist;

            }), null);

            RaisePropertyChanged(nameof(AllePersonen));
            RaisePropertyChanged(nameof(Gewerke));
        }


        private void Add()
        {

            if (PersonForAdding == null) return;
            GewähltePersonen.Add(PersonForAdding);
            PersonForAdding = null;
            Refresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        private void Remove()
        {

            if (PersonForRemoving == null) return;
            GewähltePersonen.Remove(PersonForRemoving);
            PersonForRemoving = null;
            Refresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        private void Clear()
        {

            GewähltePersonen.Clear();
            Refresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        public async Task SetzeModelAsync(ChecklistViewItem model)
        {

            if (AllePersonen == null || model == null) return;
            Current = model;

            await context.SendAsync(new SendOrPostCallback((o) => {

                GefiltertePersonen = new CollectionViewSource { Source = AllePersonen }.View;
                GefiltertePersonen.Filter = PersonalPredicate;
                GefiltertePersonen.SortDescriptions.Clear();
                GefiltertePersonen.SortDescriptions.Add(new SortDescription { PropertyName="Name" });
                Refresh();

            }), null);

            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        public void Refresh()
        {

            if (AllePersonen == null || GefiltertePersonen == null) return;
            context.Post(new SendOrPostCallback((o) => {

                GefiltertePersonen?.Refresh();
                RaisePropertyChanged(nameof(GefiltertePersonen));
            }), null);

            RaisePropertyChanged(nameof(GefiltertePersonen));
        }


        private bool PersonalPredicate(object arg)
        {

            var ps = arg as PersonalFilterModel;
            if (ps == null) return false;

            if (AktuellesGewerk != null && (!ps.Gewerk_Id.HasValue || ps.Gewerk_Id.Value != AktuellesGewerk.Id)) return false;
            if (!String.IsNullOrWhiteSpace(suchtext)) {

                if (!ps.Nummer.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase) &&
                !ps.Nachname.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase) &&
                !ps.Vorname.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase)) return false;
            }
            if (GewähltePersonen.IsNullOrEmpty()) return true;

            return GewähltePersonen.All(p => p.Id != ps.Id);
        }


        public ICollectionView GefiltertePersonen       { get; private set; }
        public IList<PersonalFilterModel> AllePersonen  { get; private set; }


        public ObservableCollection<PersonalFilterModel> GewähltePersonen  => Current?.Model?.Personalliste;


        private String suchtext;
        public String Suchtext
        {
            get { return suchtext; }
            set { if (SetProperty(ref suchtext, value)) Refresh(); }
        }


        public IList<NummerFilterModel> Gewerke  { get; private set; }


        private NummerFilterModel aktuellesGewerk;
        public NummerFilterModel AktuellesGewerk
        {
            get { return aktuellesGewerk; }
            set { if (SetProperty(ref aktuellesGewerk, value)) Refresh(); }
        }


        private PersonalFilterModel personForRemoving;
        public PersonalFilterModel PersonForRemoving
        {
            get { return personForRemoving; }
            set { SetProperty(ref personForRemoving, value); }
        }


        private PersonalFilterModel personForAdding;
        public PersonalFilterModel PersonForAdding
        {
            get { return personForAdding; }
            set { SetProperty(ref personForAdding, value); }
        }


        public ICommand PersonClearCommand  { get; }
        public ICommand ResetFilterCommand  { get; }
        public ICommand PersonAddCommand    { get; }
        public ICommand PersonRemoveCommand { get; }
    }
}
