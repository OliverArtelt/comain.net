﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;
using GongSolutions.Wpf.DragDrop;
using DragDrop = GongSolutions.Wpf.DragDrop.DragDrop;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektTreeViewModel : ViewModel<IIHObjektTreeView>, IDelayableViewModel, IDragSource, IDropTarget
    {

        private Dictionary<int, UpdateableTreeIHViewItem> pzTreeItems;
        private Dictionary<int, UpdateableTreeIHViewItem> kstTreeItems;
        private IEnumerable<IHFilterItem> currentIHList;
        private readonly ILoginService logins;


        public IHObjektTreeViewModel(IIHObjektTreeView view, ILoginService logins) : base(view)
        {
            this.logins = logins;
        }


        public Task GenerateTreeAsync(IEnumerable<IHFilterItem> ihlist, IEnumerable<PzFilterModel> pzlist, IEnumerable<KstFilterModel> kstlist, Konfiguration cfg,
                                      CancellationToken ct = default)
        {

            var kstask = GenerateKstTreeAsync(ihlist, kstlist, cfg, ct);
            var pztask = GeneratePzTreeAsync(ihlist, pzlist, cfg, ct);
            return Task.WhenAll(kstask, pztask);
        }


        private async Task GeneratePzTreeAsync(IEnumerable<IHFilterItem> ihlist, IEnumerable<PzFilterModel> pzs, Konfiguration cfg, CancellationToken ct)
        {

            currentIHList = ihlist;

            await Task.Run(() => {

                Standorte = pzs.OrderBy(p => p.Nummer).Select(p => new TreePZViewItem(p)).ToList();

                var list = ihlist.OrderBy(p => p.Nummer).Select(p => new UpdateableTreeIHViewItem(p, cfg)).ToList();
                pzTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihdic = list.ToDictionary(p => p.IHNummer);

                list.ForEach(p => {

                    if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                        ihdic[p.NummerParent].Add(p);
                        p.Parent = ihdic[p.NummerParent];
                    }
                });

                ct.ThrowIfCancellationRequested();

                var pzdic = Standorte.ToDictionary(p => p.Model.Id);
                list.Where(p => p.IstMaschine && pzdic.ContainsKey(p.FilterModel.Standort_Id))
                    .ForEach(p => {

                        pzdic[p.FilterModel.Standort_Id].Add(p);
                        p.Parent = pzdic[p.FilterModel.Standort_Id];
                    });

                Standorte = Standorte.Where(p => !p.SubItems.IsNullOrEmpty()).OrderBy(p => p.Nummer).ToList();
                RaisePropertyChanged("Standorte");
            });
        }


        private Task GenerateKstTreeAsync(IEnumerable<IHFilterItem> ihlist, IEnumerable<KstFilterModel> ksts, Konfiguration cfg, CancellationToken ct)
        {

            return Task.Run(() => {

                Kostenstellen = ksts.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).Select(p => new TreeKSTViewItem(p)).ToList();

                var list = ihlist.OrderBy(p => p.Nummer).Select(p => new UpdateableTreeIHViewItem(p, cfg)).ToList();
                kstTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihkstdict = list.ToMultiDictionary(p => p.FilterModel.Kostenstelle_Id);

                Kostenstellen.ForEach(kst => {

                    ct.ThrowIfCancellationRequested();
                    FillKst(kst, ihkstdict.GetValueOrDefault(kst.Model.Id));
                });

                Kostenstellen = Kostenstellen.Where(p => !p.SubItems.IsNullOrEmpty()).OrderBy(p => p.Nummer, new NaturalComparer()).ToList();
                RaisePropertyChanged("Kostenstellen");
            });
        }


        private void FillKst(TreeKSTViewItem kst, HashSet<UpdateableTreeIHViewItem> ihlist)
        {

            if (ihlist == null) return;

            var ihdic = ihlist.ToDictionary(p => p.IHNummer);
            ihlist.ToList().ForEach(p => {

                if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                    ihdic[p.NummerParent].Add(p);
                    p.Parent = ihdic[p.NummerParent];
                    ihlist.Remove(p);
                }
            });

            ihlist.OrderBy(p => p.IHNummer)
                  .ForEach(p => {

                        kst.Add(p);
                        p.Parent = kst;
                    });
        }


        public void SetModel(IHObjekt current)
        {

            CurrentIHObjekt = null;
            CurrentStandort = null;
            CurrentKostenstelle = null;

            if (current == null) return;
            var listvm = currentIHList.FirstOrDefault(p => p.Id == current.Id);
            CurrentIHObjekt = listvm;
            ExpandCurrentNode();

            var pzih = pzTreeItems.GetValueOrDefault(current.Id);
            if (pzih != null) pzih.Model = current;
            var kstih = kstTreeItems.GetValueOrDefault(current.Id);
            if (kstih != null) kstih.Model = current;
        }


        /// <summary>
        /// Benutzer hat auf einen Knoten geklickt -> IH laden
        /// </summary>
        public void SelectItem(object node)
        {

            if (!EditEnabled) return;

            CurrentIHObjekt = null;
            CurrentStandort = null;
            CurrentKostenstelle = null;

            var ihnode = node as UpdateableTreeIHViewItem;
            if (ihnode != null) CurrentIHObjekt = ihnode.FilterModel;

            var pznode = node as TreePZViewItem;
            if (pznode != null) CurrentStandort = pznode.Model;

            var ksnode = node as TreeKSTViewItem;
            if (ksnode != null) CurrentKostenstelle = ksnode.Model;

            RaisePropertyChanged("CurrentIHObjekt");
            RaisePropertyChanged("CurrentStandort");
            RaisePropertyChanged("CurrentKostenstelle");

            if (DetailCommand != null && DetailCommand.CanExecute(CurrentIHObjekt)) DetailCommand.Execute(CurrentIHObjekt);
        }


        public void Unload()
        {

            CurrentIHObjekt = null;
            CurrentStandort = null;
            CurrentKostenstelle = null;
            Standorte = null;
            Kostenstellen = null;
            pzTreeItems = null;
            kstTreeItems = null;

            RaisePropertyChanged("Standorte");
            RaisePropertyChanged("Kostenstellen");
        }


        public void SetCurrent(int? ihid)
        {

            if (!ihid.HasValue) {

                CurrentIHObjekt = null;
                return;
            }

            if (CurrentIHObjekt != null && CurrentIHObjekt.Id == ihid.Value) return;
            CurrentIHObjekt = currentIHList.FirstOrDefault(p => p.Id == ihid);
            ExpandCurrentNode();
        }


        private void ExpandCurrentNode()
        {

            if (CurrentIHObjekt == null) return;
            var dic = currentTab == 0? pzTreeItems: kstTreeItems;
            if (dic == null) return;
            UpdateableTreeIHViewItem current;
            if (!dic.TryGetValue(CurrentIHObjekt.Id, out current)) return;

            ExpandCurrentNode(current);
            current.IsSelected = true;
        }


        private void ExpandCurrentNode(TreeViewItem node)
        {

            node.IsExpanded = true;
            if (node.Parent != null) ExpandCurrentNode(node.Parent);
        }


        public IHFilterItem     CurrentIHObjekt     { get; private set; }
        public PzFilterModel    CurrentStandort     { get; private set; }
        public KstFilterModel   CurrentKostenstelle { get; private set; }


#region D R A G   A N D   D R O P


        public bool CanStartDrag(IDragInfo dragInfo)
            => Keyboard.IsKeyDown(Key.LeftShift) && dragInfo.SourceItem is UpdateableTreeIHViewItem &&
               (logins.AktuellerNutzer.IstAdministrator || logins.AktuellerNutzer.IstInstandhaltungLeitung);


        public void StartDrag(IDragInfo dragInfo)
        {
            DragDrop.DefaultDragHandler.StartDrag(dragInfo);
        }


        public void DragOver(IDropInfo dropInfo)
        {

            var src = dropInfo.DragInfo?.SourceItem as UpdateableTreeIHViewItem;
            var dst = dropInfo.TargetItem as TreeViewItem;

            //Akzeptoren ermitteln:
            //  muß Knoten im Baum sein
            //  nicht in sich selbst verschieben
            //  nicht zum Parent schieben (bereits da)
            if (src == null || dst == null || src == dst || dst.IsParentOf(src) || src.IsAnchestorOf(dst as UpdateableTreeIHViewItem)) {

                dropInfo.Effects = DragDropEffects.None;
                return;
            }

            //bei Drag auf Baumknoten-Expander diesen öffnen
            var hit = VisualTreeHelper.HitTest(dropInfo.VisualTarget, dropInfo.DropPosition);
            var hitElement = hit.VisualHit as Path;
            if (hitElement != null && hitElement.Name == "ExpandPath") dst.IsExpanded = true;

            dropInfo.Effects = DragDropEffects.Move;
        }


        public void Drop(IDropInfo dropInfo)
        {

            if (dropInfo.DragInfo.SourceItem is TreeIHViewItem && dropInfo.TargetItem != null)
                DropCommand.Execute(dropInfo.TargetItem);
        }


        public void Dropped(IDropInfo dropInfo) {}
        public void DragCancelled() {}
        public void DragDropOperationFinished(DragDropEffects operationResult, IDragInfo dragInfo) {}
        public bool TryCatchOccurredException(Exception exception) => false;


#endregion


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private ICommand dropCommand;
        public ICommand DropCommand
        {
            get { return dropCommand; }
            set { SetProperty(ref dropCommand, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) ExpandCurrentNode(); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public  List<TreePZViewItem> Standorte { get; private set; }


        public  List<TreeKSTViewItem> Kostenstellen { get; private set; }


#endregion

    }
}
