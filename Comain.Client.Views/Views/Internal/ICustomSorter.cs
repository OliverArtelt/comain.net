﻿using System;
using System.Collections;
using System.ComponentModel;


namespace Comain.Client.Views.Internal
{

    public interface ICustomSorter : IComparer
    {

        ListSortDirection   SortDirection   { get; set; }
        String              SortMemberPath  { get; set; }
    }
}
