﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.ViewModels.Stock.Reports
{

    public class ArtikelFilterViewModel : ViewModel<IArtikelFilterView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public ArtikelFilterViewModel(IFilterService store, IArtikelFilterView view, ILieferantenSearchProvider lfSearcher) : base(view)
        {

            this.store = store;
            LfSearchProvider = lfSearcher;
        }


        public void Reset()
        {

            Warengruppe = null;
            Lieferant = null;
            Artikel = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {

            var bld = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(Artikel)) bld.Append("Artikel ").Append(Artikel).Append("; ");
            if (Lieferant != null) bld.Append("Lieferant ").Append(Lieferant.Name1).Append("; ");
            if (Warengruppe != null) bld.Append("Warengruppe ").Append(Warengruppe.Nummer).Append("; ");

            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString();
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Warengruppe != null) filter.Add("Warengruppe", Warengruppe.Id);
            if (Lieferant != null) filter.Add("Lieferant", Lieferant.Id);
            if (!String.IsNullOrWhiteSpace(Artikel)) filter.Add("Artikel", Artikel);
        }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            RaisePropertyChanged("Warengruppen");
            RaisePropertyChanged("Lieferanten");
            LfSearchProvider.SetItems(store.Lieferanten);
        }


        public void Unload()
        {
            LfSearchProvider.ClearItems();
        }


#region P R O P E R T I E S


        public ILieferantenSearchProvider LfSearchProvider  { get; }


        public IList<NummerFilterModel> Warengruppen
        {
            get { return store == null? null: store.Warengruppen; }
        }


        private NummerFilterModel warengruppe;
        public NummerFilterModel Warengruppe
        {
            get { return warengruppe; }
            set { SetProperty(ref warengruppe, value); }
        }


        private LieferantFilterModel lieferant;
        public LieferantFilterModel Lieferant
        {
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }


        private String artikel;
        public String Artikel
        {
            get { return artikel; }
            set { SetProperty(ref artikel, value); }
        }


#endregion

    }
}
