﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan
{

    public class MaterialInfoDto
    {

        public int          Id              { get; set; }
        public int?         MaterialId      { get; set; }
        public String       Name            { get; set; }
        public String       Nummer          { get; set; }
        public String       Einheit         { get; set; }
        public decimal?     Menge           { get; set; }
        public decimal?     EPreis          { get; set; }
        public bool         Erforderlich    { get; set; }
        public String       Anweisung       { get; set; }
    }
}
