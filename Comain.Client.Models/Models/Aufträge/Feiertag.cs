﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{
  
    public class Feiertag : Entity, IDataErrorInfo, IComparable<Feiertag>
    {

        private String      name;
        private DateTime?   datum;


#region P R O P E R T I E S

        
        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        
        public DateTime? Datum
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }


#endregion

        
        public int CompareTo(Feiertag other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            if (!other.Datum.HasValue) return 1;
            if (!this.Datum.HasValue) return -1;
            return this.Datum.Value.CompareTo(other.Datum.Value);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 48) return "Die Bezeichnung darf aus maximal 48 Zeichen bestehen."; 
                    break;

                case "Datum":

                    if (!Datum.HasValue) return "Geben Sie das Datum an.";
                    if (Datum.Value.Year < 1900 || Datum.Value.Year > 2100) return "Geben Sie ein plausibles Datum an."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
