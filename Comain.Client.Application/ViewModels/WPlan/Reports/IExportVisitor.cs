﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{
    public interface IExportVisitor : IVisitor
    {

        String Exportname   { get; }

        void SetStyles(IStyleFactory s);
        void SetOptions(PlanOptions opts);
        void SetWriter(WorksheetWriter w);
    }
}
