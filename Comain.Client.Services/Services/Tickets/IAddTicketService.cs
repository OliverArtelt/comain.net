﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Tickets
{
    public interface IAddTicketService
    {

        Task<IList<AuIHKSDto>>                      IHObjekteAsync(CancellationToken ct = default);
        Task<IList<PzFilterModel>>                  StandorteAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>              GewerkeAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>            PersonalAsync(CancellationToken ct = default);
        Task<IDringlichkeitenProvider>              DringlichkeitAsync(CancellationToken ct = default);

        Task<(Auftrag model, String sendefehler)>   CreateAsync(NeuesTicketDto dto, CancellationToken ct = default);
        Task<String>                                IHPathAsync(String ihnummer, CancellationToken ct = default);
    }
}