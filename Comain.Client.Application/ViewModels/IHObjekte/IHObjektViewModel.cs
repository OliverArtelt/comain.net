﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektViewModel : ViewModel<IIHObjektView>, IDelayableViewModel
    {

        private Konfiguration cfg;


        public IHObjektViewModel(IIHObjektView view) : base(view)
        {}


        public void SetzeKonfig(Konfiguration cfg)
        {

            this.cfg = cfg;

            RaisePropertyChanged(nameof(HatLegende));
            RaisePropertyChanged(nameof(LegendeText));
            RaisePropertyChanged(nameof(LegendeText1));
            RaisePropertyChanged(nameof(LegendeText2));
            RaisePropertyChanged(nameof(LegendeText3));
            RaisePropertyChanged(nameof(LegendeText4));
        }


#region P R O P E R T I E S


        public bool HatLegende
        {
            get { return cfg != null && cfg.IHObjektFarbtyp.HasValue; }
        }


        public String LegendeText
        {
            get {

                if (cfg == null || !cfg.IHObjektFarbtyp.HasValue) return null;
                return cfg.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse? "Erhaltklasse": "IH-Status";
            }
        }


        public String LegendeText1
        {
            get {

                if (cfg == null || !cfg.IHObjektFarbtyp.HasValue) return null;
                return cfg.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse? "IV": "sehr hoch";
            }
        }


        public String LegendeText2
        {
            get {

                if (cfg == null || !cfg.IHObjektFarbtyp.HasValue) return null;
                return cfg.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse? "III": "hoch";
            }
        }


        public String LegendeText3
        {
            get {

                if (cfg == null || !cfg.IHObjektFarbtyp.HasValue) return null;
                return cfg.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse? "II": "gering";
            }
        }


        public String LegendeText4
        {
            get {

                if (cfg == null || !cfg.IHObjektFarbtyp.HasValue) return null;
                return cfg.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse? "I": "keine";
            }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object treeView;
        public object TreeView
        {
            get { return treeView; }
            set { SetProperty(ref treeView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand selfServiceCommand;
        public ICommand SelfServiceCommand
        {
            get { return selfServiceCommand; }
            set { SetProperty(ref selfServiceCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private ICommand copyCommand;
        public ICommand CopyCommand
        {
            get { return copyCommand; }
            set { SetProperty(ref copyCommand, value); }
        }


        private ICommand addSubCommand;
        public ICommand AddSubCommand
        {
            get { return addSubCommand; }
            set { SetProperty(ref addSubCommand, value); }
        }


        private ICommand addArtikelCommand;
        public ICommand AddArtikelCommand
        {
            get { return addArtikelCommand; }
            set { SetProperty(ref addArtikelCommand, value); }
        }


        private ICommand deactivateCommand;
        public ICommand DeactivateCommand
        {
            get { return deactivateCommand; }
            set { SetProperty(ref deactivateCommand, value); }
        }


        private ICommand openMassnahmeCommand;
        public ICommand OpenMassnahmeCommand
        {
            get { return openMassnahmeCommand; }
            set { SetProperty(ref openMassnahmeCommand, value); }
        }


        private ICommand addMassnahmeCommand;
        public ICommand AddMassnahmeCommand
        {
            get { return addMassnahmeCommand; }
            set { SetProperty(ref addMassnahmeCommand, value); }
        }


        private ICommand openAuftragCommand;
        public ICommand OpenAuftragCommand
        {
            get { return openAuftragCommand; }
            set { SetProperty(ref openAuftragCommand, value); }
        }


        private ICommand createAufträgeCommand;
        public ICommand CreateAufträgeCommand
        {
            get { return createAufträgeCommand; }
            set { SetProperty(ref createAufträgeCommand, value); }
        }


        private ICommand deleteArtikelCommand;
        public ICommand DeleteArtikelCommand
        {
            get { return deleteArtikelCommand; }
            set { SetProperty(ref deleteArtikelCommand, value); }
        }


        private ICommand moveCommand;
        public ICommand MoveCommand
        {
            get => moveCommand;
            set => SetProperty(ref moveCommand, value);
        }


        private bool deleteArtikelVisible;
        public bool DeleteArtikelVisible
        {
            get { return deleteArtikelVisible; }
            set { SetProperty(ref deleteArtikelVisible, value); }
        }


        private bool addArtikelVisible;
        public bool AddArtikelVisible
        {
            get { return addArtikelVisible; }
            set { SetProperty(ref addArtikelVisible, value); }
        }


        private bool editVisible;
        public bool EditVisible
        {
            get { return editVisible; }
            set { SetProperty(ref editVisible, value); }
        }


        private bool addVisible;
        public bool AddVisible
        {
            get { return addVisible; }
            set { SetProperty(ref addVisible, value); }
        }


        private bool moveVisible;
        public bool MoveVisible
        {
            get => moveVisible;
            set => SetProperty(ref moveVisible, value);
        }


        private bool addSubVisible;
        public bool AddSubVisible
        {
            get { return addSubVisible; }
            set { SetProperty(ref addSubVisible, value); }
        }


        private bool cancelVisible;
        public bool CancelVisible
        {
            get { return cancelVisible; }
            set { SetProperty(ref cancelVisible, value); }
        }


        private bool reloadVisible;
        public bool ReloadVisible
        {
            get { return reloadVisible; }
            set { SetProperty(ref reloadVisible, value); }
        }


        private bool saveVisible;
        public bool SaveVisible
        {
            get { return saveVisible; }
            set { SetProperty(ref saveVisible, value); }
        }


        private bool deleteVisible;
        public bool DeleteVisible
        {
            get { return deleteVisible; }
            set { SetProperty(ref deleteVisible, value); }
        }


        private bool closeVisible;
        public bool CloseVisible
        {
            get { return closeVisible; }
            set { SetProperty(ref closeVisible, value); }
        }


        private bool copyVisible;
        public bool CopyVisible
        {
            get { return copyVisible; }
            set { SetProperty(ref copyVisible, value); }
        }


        private bool deactivateVisible;
        public bool DeactivateVisible
        {
            get { return deactivateVisible; }
            set { SetProperty(ref deactivateVisible, value); }
        }


        private bool openMassnahmeVisible;
        public bool OpenMassnahmeVisible
        {
            get { return openMassnahmeVisible; }
            set { SetProperty(ref openMassnahmeVisible, value); }
        }


        private bool addMassnahmeVisible;
        public bool AddMassnahmeVisible
        {
            get { return addMassnahmeVisible; }
            set { SetProperty(ref addMassnahmeVisible, value); }
        }


        private bool openAuftragVisible;
        public bool OpenAuftragVisible
        {
            get { return openAuftragVisible; }
            set { SetProperty(ref openAuftragVisible, value); }
        }


        private bool createAufträgeVisible;
        public bool CreateAufträgeVisible
        {
            get { return createAufträgeVisible; }
            set { SetProperty(ref createAufträgeVisible, value); }
        }


        private bool selfServiceVisible;
        public bool SelfServiceVisible
        {
            get { return selfServiceVisible; }
            set { SetProperty(ref selfServiceVisible, value); }
        }


#endregion

    }
}
