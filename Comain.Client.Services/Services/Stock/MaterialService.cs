﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Stock;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Aufträge
{

    public class MaterialService : IMaterialService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public MaterialService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<Materialleistung> SaveAsync(Materialleistung model, CancellationToken ct = default)
        {

            var dto = mapper.Map<MaterialleistungDto>(model);
            var savedDto = await store.PostAsync<MaterialleistungDto, MaterialleistungDto>("/MT", dto, ct).ConfigureAwait(false);
            var savedModel = mapper.Map<Materialleistung>(savedDto);
            return savedModel;
        }


        public async Task<Materialleistung> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format(@"/MT/{0}", id);
            var dto = await store.GetAsync<MaterialleistungDto>(url, ct).ConfigureAwait(false);
            var model = mapper.Map<Materialleistung>(dto);
            return model;
        }


        public async Task DeleteAsync(Materialleistung model, CancellationToken ct = default)
        {

            var url = String.Format(@"/MT/{0}", model.Id);
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }


        public Task<IHInfoDto> IHInfoAsync(int auid, CancellationToken ct = default)
        {
            return store.GetAsync<IHInfoDto>(String.Format("/IH/AU/{0}", auid), ct);
        }


        public Task<decimal> FachRestmengeAsync(int lagerortid, int artikelid, CancellationToken ct = default)
        {
            return store.GetAsync<decimal>(String.Format("/WA/LagerRest/{0}/{1}", lagerortid, artikelid), ct);
        }


        public Task<IList<Dtos.Filters.NummerFilterModel>> FilterEinheitenAsync(CancellationToken ct = default)
        {
            return FilterAsync<Dtos.Filters.NummerFilterModel>("/Filter/EH", ct);
        }


        public Task<IList<Dtos.Filters.MaterialstammFilterModel>> FilterMaterialstammAsync(CancellationToken ct = default)
        {
            return FilterAsync<Dtos.Filters.MaterialstammFilterModel>("/Filter/MT", ct);
        }


        public Task<IList<LieferantFilterModel>> FilterLieferantenAsync(CancellationToken ct = default)
        {
            return FilterAsync<LieferantFilterModel>("/Filter/LF", ct);
        }


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
        {
            return FilterAsync<PersonalFilterModel>("/Filter/PS", ct);
        }


        public Task<IList<WLFilterDto>> FilterEinlagerungenAsync(CancellationToken ct = default)
        {
            return FilterAsync<WLFilterDto>("/WL/WLFilter", ct);
        }


        private Task<IList<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<IList<TFilterModel>>(url, ct);
        }
    }
}
