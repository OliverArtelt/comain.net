﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;


namespace Comain.Client.Controllers.Aufträge
{
    
    public class AuftragCommandMediator : IAuftragCommandMediator 
    {

        private readonly ILoginService login;
        private readonly AuftragViewModel view;
        private readonly AuftragDetailViewModel detailView;
        private readonly AuftragDetailBasisViewModel basisView;
        private readonly AuftragDetailPersonalViewModel personalView;
        private readonly AuftragDetailLeistungViewModel leistungView;
        private readonly AuftragFilterViewModel filterView;
        private readonly AuftragListViewModel listView;
        private Konfiguration config;


        public AuftragCommandMediator(ILoginService login, AuftragViewModel view, AuftragDetailViewModel detailView, AuftragDetailBasisViewModel basisView,
                                      AuftragDetailPersonalViewModel personalView, AuftragDetailLeistungViewModel leistungView, 
                                      AuftragListViewModel listView, AuftragFilterViewModel filterView)
        {

            this.login = login;
            this.view = view;
            this.detailView = detailView;
            this.basisView = basisView;
            this.personalView = personalView;
            this.leistungView = leistungView;
            this.filterView = filterView;
            this.listView = listView;
        }


        private bool    HasCurrent              { get { return detailView.Current != null; } }
        private bool    KostenstelleVerlegt     { get { return detailView.Current != null && detailView.Current.AuIHKS != null && !detailView.Current.AuIHKS.IstAktiv; } }
        private bool    AuftragBearbeitbar      { get { return detailView.Current != null && detailView.Current.StatusBearbeitbar; } }
        

        public EditMode CurrentMode { get; private set; }


        public void Initialize(Konfiguration config)
        {

            this.config = config;
            detailView.IstKunde = login.AktuellerNutzer.IstProduktion;
            basisView.IstKunde = login.AktuellerNutzer.IstProduktion;
            leistungView.IstKunde = login.AktuellerNutzer.IstProduktion;
            personalView.IstKunde = login.AktuellerNutzer.IstProduktion;
        }


        public void SetEditMode(EditMode newMode)
        {
        
            if (newMode == EditMode.Edit && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diesen Auftrag nicht bearbeiten.");
            if (newMode == EditMode.Append && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diesen Auftrag nicht bearbeiten.");

            CurrentMode = newMode;
            basisView.EditEnabled = newMode == EditMode.Append || newMode == EditMode.Edit && !login.AktuellerNutzer.IstProduktion; 
            personalView.EditEnabled = newMode == EditMode.Edit; 
            personalView.AddEnabled = newMode == EditMode.Append || newMode == EditMode.Edit; 
            leistungView.EditEnabled = newMode == EditMode.Edit; 
            listView.EditEnabled = !basisView.EditEnabled;
            filterView.EditEnabled = !basisView.EditEnabled;

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            //nicht im Editmodus
            //kein Kundenprofil
            //kein Storno
            //kein Abgeschlossen
            view.EditVisible   = !login.AktuellerNutzer.IstProduktion && CurrentMode == EditMode.ReadOnly && AuftragBearbeitbar;

            //im Editmodus
            view.SaveVisible   = CurrentMode != EditMode.ReadOnly;
            
            //nicht im Editmodus
            view.AddVisible    = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion;
            
            //im Editmodus
            view.CancelVisible = CurrentMode != EditMode.ReadOnly;
            
            //nicht im Editmodus
            view.ReloadVisible = CurrentMode == EditMode.ReadOnly;
                        
            //kein Kundenprofil
            //kein Storno
            //kein Abgeschlossen
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_10")
            //im Editmodus
            //"Für den Auftrag existieren Personalleistungen vor dem Grenzwert des Abrechnungsdatums.\nDer Auftrag kann nicht gelöscht werden."
            //"Für den Auftrag existieren Materialleistungen vor dem Grenzwert des Abrechnungsdatums.\nDer Auftrag kann nicht gelöscht werden."
            //"Dieser WI-Auftrag ist bereits n mal realisiert worden.\n Der Auftrag kann nicht gelöscht werden";
            //"Dieser WI-Auftrag besitzt n Termin(e) Der Auftrag kann nicht gelöscht werden";
            //"Es sind Unteraufträge vorhanden.\nDer Auftrag kann nicht gelöscht werden.");
            view.DeleteVisible = !login.AktuellerNutzer.IstProduktion && AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Basis && CurrentMode == EditMode.Edit;
            
            //kein Kundenprofil
            //kein Storno
            //kein Abgeschlossen
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_10")
            //im Editmodus
            //"Im Auftrag wurden keine Leistungen erfaßt.\nGeben Sie bitte Leistungen ein oder stornieren Sie den Auftrag.");
            //"Tragen Sie bitte das Übergabedatum ein.
            view.CloseVisible  = !login.AktuellerNutzer.IstProduktion && AuftragBearbeitbar && !login.AktuellerNutzer.IstInstandhaltungInstandhalter && 
                                 detailView.Tab == AuftragDetailViewModel.Tabs.Basis && CurrentMode == EditMode.Edit;
            
            //kein Kundenprofil
            //kein Storno
            //kein Abgeschlossen
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_10")
            //im Editmodus
            //"Im Auftrag wurden Personalleistungen erfaßt.\nEnthält ein Auftrag Leistungen, kann dieser nicht storniert werden.");
            //"Im Auftrag wurden Materialleistungen erfaßt.\nEnthält ein Auftrag Leistungen, kann dieser nicht storniert werden.");
            view.StornoVisible = !login.AktuellerNutzer.IstProduktion && !login.AktuellerNutzer.IstInstandhaltungInstandhalter && 
                                 AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Basis && CurrentMode == EditMode.Edit;
            
            //kein Kundenprofil
            //nur Abgeschlossene
            //IsMember("admins");
            view.ReopenVisible = login.AktuellerNutzer.IstAdministrator && detailView.Current != null && detailView.Current.Status == Auftragstatus.Abgeschlossen && detailView.Tab == AuftragDetailViewModel.Tabs.Basis;
            
            //kein Kundenprofil
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_6 profil_10")
            //im Editmodus
            //bIHKSAktiv: "Die Kostenstelle dieses Auftrages wurde verlegt.\nDieser Auftrag darf nicht weiter belastet werden.
            //DB_ReadGlobalValue("FreezeLeistung");
            view.LENeuVisible  = !login.AktuellerNutzer.IstProduktion && !KostenstelleVerlegt && CurrentMode == EditMode.Edit && AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Leistung;
            
            //kein Kundenprofil
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_6 profil_10")
            //DB_ReadGlobalValue("FreezeLeistung");
            view.LEEditVisible = !login.AktuellerNutzer.IstProduktion && HasCurrent && detailView.Tab == AuftragDetailViewModel.Tabs.Leistung;
            
            //kein Kundenprofil
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_6 profil_10")
            //im Editmodus
            //bIHKSAktiv: "Die Kostenstelle dieses Auftrages wurde verlegt.\nDieser Auftrag darf nicht weiter belastet werden.
            view.MTNeuVisible  = !login.AktuellerNutzer.IstProduktion && !KostenstelleVerlegt && CurrentMode == EditMode.Edit && AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Leistung;
            
            //kein Kundenprofil
            //IsMember("admins profil_1 profil_2 profil_3 profil_4 profil_6 profil_10")
            view.MTEditVisible = !login.AktuellerNutzer.IstProduktion && HasCurrent && detailView.Tab == AuftragDetailViewModel.Tabs.Leistung;
            
            //kein Kundenprofil
            //im Editmodus
            view.FbNummerVisible = !login.AktuellerNutzer.IstProduktion && AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Basis && CurrentMode == EditMode.Edit;

            //Checkliste Wartung, Detail wegen Detailinfos auch bei ReadOnly anzeigen
            view.PositionDetailVisible = AuftragBearbeitbar && detailView.Tab == AuftragDetailViewModel.Tabs.Checkliste ;
        }
    } 
}
