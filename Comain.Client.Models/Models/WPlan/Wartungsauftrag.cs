﻿using System.Collections.Generic;
using System.Linq;
using Comain;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.WPlan;


namespace Comain.Client.Models.WPlan
{

    public class Wartungsauftrag : Auftrag
    {

        public ICollection<Checklistposition> checklist;


        public bool IstErledigt  => !checklist.IsNullOrEmpty() && checklist.All(p => p.IstErledigt); 
        public bool HatErledigte => !checklist.IsNullOrEmpty() && checklist.Any(p => p.IstErledigt); 


        public ICollection<Checklistposition> Checklist      
        {
            get { return checklist ?? (checklist = new HashSet<Checklistposition>()); }
            set { SetProperty(ref checklist, value); }
        }
    }
}
