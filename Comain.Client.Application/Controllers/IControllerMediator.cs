﻿using System;
using System.Threading;
using System.Threading.Tasks;


namespace Comain.Client.Controllers
{

    public interface IControllerMediator : IControllerBus
    {

        IController         Current                     { get; }
        event EventHandler  CurrentControllerChanged;

        void                Reset();
        void                Shutdown();
    }
}
