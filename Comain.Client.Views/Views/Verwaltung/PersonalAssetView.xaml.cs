﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Comain.Client.Views.Verwaltung
{
    /// <summary>
    /// Interaction logic for PersonalAssetView.xaml
    /// </summary>
    public partial class PersonalAssetView : UserControl,IPersonalAssetView
    {
        public PersonalAssetView()
        {
            InitializeComponent();
        }
    }
}
