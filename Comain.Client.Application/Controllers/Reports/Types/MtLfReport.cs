﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Artikelstamm;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class MtLfReport : ReportBase, IReport
    {

        private readonly DisplayFilterViewModel displayView;
        private readonly IArtikelService store;
       
        
        public MtLfReport(IArtikelService store, DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, IHTreeViewModel treeView)
        {

            this.displayView = displayView;
            this.store = store;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, treeView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }
 

        public override string Title
        {
            get { return "Artikel mit Asset"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 8000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.MtLfExport.rdlc": "Comain.Client.Reports.Stock.MtLfReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            if (displayView.DisplayExport) result.AddDataSource("MainData", await store.MtLfExportAsync(GebeFilter(), ct));
            else                           result.AddDataSource("MainData", await store.MtLfReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
