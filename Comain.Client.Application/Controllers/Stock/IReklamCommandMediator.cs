﻿using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Reklamationen
{

    public interface IReklamCommandMediator
    {
        
        EditMode CurrentMode { get; }

        void NeuSetzen();
        void SetEditMode(EditMode newMode);
    }
}