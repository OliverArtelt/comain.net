﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class KstDto : ChangeTrackingBase
    {

        public String       Nummer                  { get; set; }
        public String       Name                    { get; set; }
        public decimal      Sortierung              { get; set; }
        public bool         IstKundenKostenstelle   { get; set; }
    }
}
