﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.Converters.Stock
{

    public class LieferscheinModelConverter : ITypeConverter<Lieferschein, LieferscheinDto>
    {

        public LieferscheinDto Convert(Lieferschein src, LieferscheinDto dst, ResolutionContext context)
        {

            if (dst == null) dst = new LieferscheinDto();
            if (src == null) return dst;

            dst.ChangedProps = src.ChangedProps;
            
            dst.Id       = src.Id;         
            dst.Nummer   = src.Nummer;         
            dst.Notizen  = src.Notizen;          
            dst.Datum    = src.Datum;        
                        
            dst.Eingänge = new List<EingangDto>();
            if (src.Eingänge.IsNullOrEmpty()) return dst;

            foreach (var srcWe in src.Eingänge) {

                var dstWe = new EingangDto();

                dstWe.ChangedProps       = srcWe.ChangedProps;
                dstWe.Id                 = srcWe.Id;          
                dstWe.Position           = srcWe.Position;          
                dstWe.Liefermenge        = srcWe.Liefermenge;       
                dstWe.MengeOK            = srcWe.MengeOK;           
                dstWe.Notizen            = srcWe.Notizen;           
                dstWe.MengeGeprüft       = srcWe.MengeGeprüft;      
                dstWe.QualitätGeprüft    = srcWe.QualitätGeprüft;   
                dstWe.Bestellposition_Id = srcWe.Bestellposition.Id;
                dstWe.Material_Id        = srcWe.Material?.Id;       
                dstWe.Lieferschein_Id    = srcWe.Lieferschein.Id;   
             
                if (!srcWe.Einlagerungen.IsNullOrEmpty()) {
            
                    dstWe.Einlagerungen = new List<WLInfoDto>();

                    foreach (var srcWl in srcWe.Einlagerungen) {

                        var dstWl = new WLInfoDto();
                        dstWe.Einlagerungen.Add(dstWl);

                        dstWl.Id              = srcWl.Id;
                        dstWl.Menge           = srcWl.Menge;
                        dstWl.Restmenge       = srcWl.Restmenge;
                        dstWl.Preis           = srcWl.Preis;
                        dstWl.Datum           = srcWl.Datum;
                        dstWl.Wareneingang_Id = srcWe.Id;
                        if (srcWl.Lagerort != null) 
                            dstWl.Lagerort    = new LagerortDto { Id = srcWl.Lagerort.Id };
                    }
                }

                dst.Eingänge.Add(dstWe);
            }

            return dst;   
        }
    }
}
