﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{
    
    public class MonatViewModel : ViewModel<IMonatView>, IFilterViewModel
    {

        public MonatViewModel(IMonatView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            monat = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }


        public String Validiere()
        {

            if (!monat.HasValue) return "Geben Sie ein gültiges Datum an.";
            return null;
        }


        public DateTime? MonatOf
        {
            
            get {

                if (!monat.HasValue) return null;
                return new DateTime(monat.Value.Year, monat.Value.Month, 1);
            }
        }


        public string FilterAlsString()
        {
            
            if (monat.HasValue) return String.Format("{0}, {1}", MonatOf.Value.ToShortDateString(), KwString);
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (monat.HasValue) filter.Add("Monat", MonatOf);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);            
        }


        public void Unload()
        {}


        public String KwString
        {

            get {

                if (!monat.HasValue) return null;
                return "KW " + MonatOf.KWString();
            }
        }


#region P R O P E R T I E S
   

        private DateTime? monat;
        public DateTime? Monat
        { 
            get { return monat; }
            set { if (SetProperty(ref monat, value)) RaisePropertyChanged("KwString"); }
        } 
   

#endregion


    }
}
