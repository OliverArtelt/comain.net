﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Comain.Client.Views.Internal
{
	public static class PasswordBoxHelper
	{
		public static readonly DependencyProperty BoundPassword = DependencyProperty.RegisterAttached("BoundPassword", typeof(string), typeof(PasswordBoxHelper), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(PasswordBoxHelper.OnBoundPasswordChanged)));
		public static readonly DependencyProperty BindPassword = DependencyProperty.RegisterAttached("BindPassword", typeof(bool), typeof(PasswordBoxHelper), new PropertyMetadata(false, new PropertyChangedCallback(PasswordBoxHelper.OnBindPasswordChanged)));
		private static readonly DependencyProperty UpdatingPassword = DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxHelper));
		private static void OnBoundPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			PasswordBox passwordBox = d as PasswordBox;
			if (d != null && PasswordBoxHelper.GetBindPassword(d))
			{
				passwordBox.PasswordChanged -= new RoutedEventHandler(PasswordBoxHelper.HandlePasswordChanged);
				string password = (string)e.NewValue;
				if (!PasswordBoxHelper.GetUpdatingPassword(passwordBox))
				{
					passwordBox.Password = password;
				}
				passwordBox.PasswordChanged += new RoutedEventHandler(PasswordBoxHelper.HandlePasswordChanged);
			}
		}
		private static void OnBindPasswordChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
		{
			PasswordBox passwordBox = dp as PasswordBox;
			if (passwordBox != null)
			{
				bool flag = (bool)e.OldValue;
				bool flag2 = (bool)e.NewValue;
				if (flag)
				{
					passwordBox.PasswordChanged -= new RoutedEventHandler(PasswordBoxHelper.HandlePasswordChanged);
				}
				if (flag2)
				{
					passwordBox.PasswordChanged += new RoutedEventHandler(PasswordBoxHelper.HandlePasswordChanged);
				}
			}
		}
		private static void HandlePasswordChanged(object sender, RoutedEventArgs e)
		{
			PasswordBox passwordBox = sender as PasswordBox;
			PasswordBoxHelper.SetUpdatingPassword(passwordBox, true);
			PasswordBoxHelper.SetBoundPassword(passwordBox, passwordBox.Password);
			PasswordBoxHelper.SetUpdatingPassword(passwordBox, false);
		}
		public static void SetBindPassword(DependencyObject dp, bool value)
		{
			dp.SetValue(PasswordBoxHelper.BindPassword, value);
		}
		public static bool GetBindPassword(DependencyObject dp)
		{
			return (bool)dp.GetValue(PasswordBoxHelper.BindPassword);
		}
		public static string GetBoundPassword(DependencyObject dp)
		{
			return (string)dp.GetValue(PasswordBoxHelper.BoundPassword);
		}
		public static void SetBoundPassword(DependencyObject dp, string value)
		{
			dp.SetValue(PasswordBoxHelper.BoundPassword, value);
		}
		private static bool GetUpdatingPassword(DependencyObject dp)
		{
			return (bool)dp.GetValue(PasswordBoxHelper.UpdatingPassword);
		}
		private static void SetUpdatingPassword(DependencyObject dp, bool value)
		{
			dp.SetValue(PasswordBoxHelper.UpdatingPassword, value);
		}
	}
}
