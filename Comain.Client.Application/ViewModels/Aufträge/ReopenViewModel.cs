﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class ReopenViewModel : ViewModel<IReopenView> 
    {
 
        public ReopenViewModel(IReopenView view) : base(view)
        {}


#region P R O P E R T I E S
 
    
        private String reason;
        public String Reason
        {
            get { return reason; }   
            set { SetProperty(ref reason, value); }   
        }
 
    
        private ICommand reopenCommand;
        public ICommand ReopenCommand
        {
            get { return reopenCommand; }   
            set { SetProperty(ref reopenCommand, value); }   
        }
 
    
        private Auftrag auftrag;
        public Auftrag Auftrag
        {
            get { return auftrag; }   
            set { if (SetProperty(ref auftrag, value)) RaisePropertyChanged("Header"); }   
        }
 

        public String Header
        {
            get { 
            
                if (auftrag == null) return null; 
                return String.Format("Der Auftrag {0} wird wieder geöffnet.", auftrag.Nummer);
            }   
        }


#endregion

    }
}
