﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanDetailViewModel : ViewModel<IPlanDetailView>
    {

        public enum Tabs { Dashboard = 0, Report, Create }


        private readonly SynchronizationContext context;


        public PlanDetailViewModel(IPlanDetailView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {}


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


#region P R O P E R T I E S


        private bool createIsVisible;
        public bool CreateIsVisible
        {
            get { return createIsVisible; }
            set { SetProperty(ref createIsVisible, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }


        private object dashboardView;
        public object DashboardView
        {
            get { return dashboardView; }
            set { SetProperty(ref dashboardView, value); }
        }


        private object reportView;
        public object ReportView
        {
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


        private object createView;
        public object CreateView
        {
            get { return createView; }
            set { SetProperty(ref createView, value); }
        }


#endregion

    }
}
