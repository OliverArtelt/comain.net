﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class IHSelektorViewModel : ViewModel<IIHSelektorView>
    {

        private readonly SynchronizationContext context;


        public IHSelektorViewModel(IIHSelektorView view) : base(view)
        {

            context = SynchronizationContext.Current;
            SearchCommand = new DelegateCommand(Refresh);
            ClearCommand = new DelegateCommand(ResetSearch);
            SelectAllCommand = new DelegateCommand(p =>   { if (Foundlist != null) foreach (var ih in Foundlist) { ((IHSelektorItem)ih).IsSelected = true; }});
            DeselectAllCommand = new DelegateCommand(p => { if (Foundlist != null) foreach (var ih in Foundlist) { ((IHSelektorItem)ih).IsSelected = false; }});
        }


        public async Task SetListAsync(IEnumerable<IHSelektorItem> list)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                if (list == null) {

                    Foundlist = null;
                    return;
                }

                Foundlist = new CollectionViewSource { Source = list }.View;
                Foundlist.Filter = Predicate;
                Foundlist.SortDescriptions.Clear();
                Foundlist.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });

                Refresh();

            }), null);
        }


        private bool Predicate(object arg)
        {

            var ih = arg as IHSelektorItem;
            if (ih == null) return false;

            if (!String.IsNullOrEmpty(searchText)) {

                if (!ih.Nummer.StartsWith(searchText) && !ih.Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase) &&
                    (ih.Inventarnummer == null || !ih.Inventarnummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))) return false;
            }

            if (ih.Status.GetValueOrDefault() == 0 && !StatusUndefiniert) return false;
            if (ih.Status.GetValueOrDefault() == 1 && !StatusSehrHoch) return false;
            if (ih.Status.GetValueOrDefault() == 2 && !StatusHoch) return false;
            if (ih.Status.GetValueOrDefault() == 3 && !StatusGering) return false;
            if (ih.Status.GetValueOrDefault() == 4 && !StatusKeine) return false;

            return true;
        }


        private void Refresh()
        {

            if (Foundlist == null) return;

            context.Post(new SendOrPostCallback((o) => {

                Foundlist?.Refresh();
                RaisePropertyChanged("Foundlist");

                var viewcnt = ((CollectionView)Foundlist).Count;
                if (viewcnt == 0) ResultText = "Keine Assets gefunden.";
                else if (viewcnt == 1) ResultText = "Ein Asset gefunden.";
                else ResultText = String.Format("{0} Assets gefunden.", viewcnt);

            }), null);
        }


        private void ResetSearch()
        {

            SearchText = null;

            StatusUndefiniert = true;
            StatusSehrHoch = true;
            StatusHoch = true;
            StatusGering = true;
            StatusKeine = true;

            Refresh();
        }


        public void Unload()
        {
            Foundlist = null;
        }


        public IEnumerable<IHSelektorItem> GetSelected()
        {

            if (Foundlist == null) yield break;

            foreach (IHSelektorItem ih in (CollectionView)Foundlist) {

                if (ih.IsSelected) yield return ih;
            }
        }


#region P R O P E R T I E S


        public ICollectionView Foundlist     { get; private set; }


        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


        public ICommand SelectAllCommand    { get; private set; }
        public ICommand DeselectAllCommand  { get; private set; }
        public ICommand SearchCommand       { get; private set; }
        public ICommand ClearCommand        { get; private set; }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private bool statusUndefiniert = true;
        public bool StatusUndefiniert
        {
            get { return statusUndefiniert; }
            set { SetProperty(ref statusUndefiniert, value); }
        }


        private bool statusSehrHoch = true;
        public bool StatusSehrHoch
        {
            get { return statusSehrHoch; }
            set { SetProperty(ref statusSehrHoch, value); }
        }


        private bool statusHoch = true;
        public bool StatusHoch
        {
            get { return statusHoch; }
            set { SetProperty(ref statusHoch, value); }
        }


        private bool statusGering = true;
        public bool StatusGering
        {
            get { return statusGering; }
            set { SetProperty(ref statusGering, value); }
        }


        private bool statusKeine = true;
        public bool StatusKeine
        {
            get { return statusKeine; }
            set { SetProperty(ref statusKeine, value); }
        }


#endregion

    }
}
