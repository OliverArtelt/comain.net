﻿using System;
using System.Linq;
using AutoMapper;
using Comain.Client.Converters.Stock;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Dokumente;
using Comain.Client.Dtos.Emma;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.Emma;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Specs;
using Comain.Client.Models.Stock;
using Comain.Client.Models.Tickets;
using Comain.Client.Models.Users;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Trackers;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Converters
{

    public class MappingProfile : Profile
    {

        public MappingProfile()
        {

            CreateMap<AuftragDetailDto,          Auftrag>().ConvertUsing(new AuftragConverter());

            CreateMap<Auftrag,                   AuftragDetailDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                                    .ForMember(p => p.IHObjektKst_Id, opt => opt.Ignore())
                                                                    .ForMember(p => p.GemeldetAm, opt => opt.Ignore())
                                                                    .ForMember(p => p.ÜbergabeAm, opt => opt.Ignore())
                                                                    .ForMember(p => p.Parent_Id, opt => opt.Ignore())
                                                                    .AfterMap((src, dst) => {

                                                                        DateTime? test = null;
                                                                        if (src.GemeldetDatum.HasValue && src.GemeldetZeit.HasValue) test = src.GemeldetDatum.Value.Add(src.GemeldetZeit.Value);
                                                                        if (dst.GemeldetAm != test) {

                                                                            dst.GemeldetAm = test;
                                                                            dst.ChangedProps.Add(nameof(dst.GemeldetAm));
                                                                        }

                                                                        test = null;
                                                                        if (src.ÜbergabeDatum.HasValue && src.ÜbergabeZeit.HasValue) test = src.ÜbergabeDatum.Value.Add(src.ÜbergabeZeit.Value);
                                                                        if (dst.ÜbergabeAm != test) {

                                                                            dst.ÜbergabeAm = test;
                                                                            dst.ChangedProps.Add(nameof(dst.ÜbergabeAm));
                                                                        }
                                                                    });


            CreateMap<LeistungsartFilterModel,   Leistungsart>().ForMember(p => p.ChangedProps, opt => opt.Ignore());
            CreateMap<Leistungsart,              LeistungsartFilterModel>();
            CreateMap<PersonalleistungDto,       Personalleistung>().ForMember(p => p.Beginn, opt => opt.Ignore())
                                                                    .ForMember(p => p.Ende, opt => opt.Ignore())
                                                                    .ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<MaterialleistungDto,       Materialleistung>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<Personalleistung,          PersonalleistungDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<NutzerDto,                 Nutzer>().ForMember(p => p.ChangedProps, opt => opt.Ignore())
                                                          .ForMember(p => p.Errorlist, opt => opt.Ignore())
                                                          .ForMember(p => p.Password1, opt => opt.Ignore())
                                                          .ForMember(p => p.Password2, opt => opt.Ignore());
            CreateMap<Nutzer,                    NutzerDto>().ForMember(p => p.NeuesPasswort, opt => opt.Ignore())
                                                             .AfterMap((model, dto) => { dto.NeuesPasswort = model.Password1; });
            CreateMap<SollwertDto,               Sollwert>();
            CreateMap<Sollwert,                  SollwertDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Einheit, EinheitDto>().ForMember(p => p.ChangeStatus, (IMemberConfigurationExpression<Einheit, EinheitDto, ChangeStatus> opt) => opt.Ignore());
            CreateMap<EinheitDto, Einheit>();
            CreateMap<Ferien,                    FerienDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<FerienDto,                 Ferien>();
            CreateMap<Feiertag,                  FeiertagDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<FeiertagDto,               Feiertag>();
            CreateMap<Dringlichkeit,             DringlichkeitDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<DringlichkeitDto,          Dringlichkeit>();
            CreateMap<Leistungsart,              LeistungsartDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<LeistungsartDto,           Leistungsart>();
            CreateMap<Gewerk,                    KeyNameDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<KeyNameDto,                Gewerk>();
            CreateMap<Leistungsart,              LeistungsartDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<LeistungsartDto,           Leistungsart>();
            CreateMap<Lieferant,                 Dtos.Verwaltung.LieferantDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<LieferantAP,               LieferantAPDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Dtos.Verwaltung.LieferantDto, Lieferant>();
            CreateMap<LieferantAPDto,            LieferantAP>();
            CreateMap<Aufschlagtyp,              AufschlagtypDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<AufschlagtypDto,           Aufschlagtyp>();
            CreateMap<Aufschlag,                 AufschlagDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<AufschlagDto,              Aufschlag>();
            CreateMap<AufschlagtypFilterModel,   Aufschlagtyp>().ForMember(p => p.ChangedProps, opt => opt.Ignore());

            CreateMap<NummerDto,                 NummerItem>();
            CreateMap<NummerItem,                NummerDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<SubNummerDto,              SubNummerItem>();
            CreateMap<SubNummerItem,             SubNummerDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<PzDto,                     Standort>().ForMember(p => p.ZugeordneteKostenstellen, opt => opt.Ignore());
            CreateMap<Standort,                  PzDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<KstDto,                    Kostenstelle>().ForMember(p => p.ZugeordneteStandorte, opt => opt.Ignore());
            CreateMap<Kostenstelle,              KstDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<Personal,                  PersonalDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<PersonalDto,               Personal>().ForMember(p => p.Dokumente, opt => opt.Ignore());

            CreateMap<Materialstamm,             Dtos.Verwaltung.MaterialDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<MtLfMap,                   MaterialLieferant>().ForMember(p => p.Lieferant, opt => opt.Ignore())
                                                                     .ForMember(p => p.NoRank, opt => opt.Ignore())
                                                                     .ForMember(p => p.Rank1, opt => opt.Ignore())
                                                                     .ForMember(p => p.Rank2, opt => opt.Ignore())
                                                                     .ForMember(p => p.Rank3, opt => opt.Ignore());

            CreateMap<MaterialLieferant,         MtLfMap>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<IHBelegungDto,             IHObjektBelegung>();
            CreateMap<IHObjektBelegung,          IHBelegungDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<IHObjekt,                  IHDetailDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<IHDetailDto,               IHObjekt>().ForMember(p => p.Dokumente, opt => opt.Ignore())
                                                            .ForMember(p => p.Konfiguration, opt => opt.Ignore());

            CreateMap<Dtos.Verwaltung.MaterialDto, Materialstamm>();
            CreateMap<Materialleistung,          MaterialleistungDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<KeyNameDto,                Models.Materialien.Warengruppe>();
            CreateMap<Models.Materialien.Warengruppe, KeyNameDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<ObjektgruppeDto,           Objektgruppe>();
            CreateMap<Objektgruppe,              ObjektgruppeDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<ObjektartDto,              Objektart>();
            CreateMap<Objektart,                 ObjektartDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<IHObjekt,                  IHListDto>();

            CreateMap<Dokument,                  DokumentDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<DokumentDto,               Dokument>();


            CreateMap<SpezifikationDto,    Spezifikation>().ForMember(p => p.Errorlist, opt => opt.Ignore())
                                                           .ForMember(p => p.Wertschablonen, opt => opt.Ignore())
                                                           .ForMember(p => p.Dokumente, opt => opt.Ignore())
                                                           .AfterMap((dto, model) =>
                                                              { if (dto.Wertschablonen != null) model.Wertschablonen =
                                                                   new TrackableCollection<SpecWert>(dto.Wertschablonen.Select(p => new SpecWert(p)).ToList());
                                                                   model.AcceptChanges();
                                                                   model.VorgeseheneArtikelliste?.AcceptChanges();
                                                           });
            CreateMap<Spezifikation,       SpezifikationDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                              .ForMember(p => p.VorgeseheneArtikelliste, opt => opt.Ignore())
                                                              .ForMember(p => p.VorgeseheneMassnahmen, opt => opt.Ignore())
                                                              .ForMember(p => p.Wertschablonen, opt => opt.Ignore());
            CreateMap<SpecWert,            SpecWertDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<SpecArtikelDto,      SpecArtikel>().ForMember(p => p.ChangedProps, opt => opt.Ignore());
            CreateMap<SpecArtikel,         SpecArtikelDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<SpecMassnahmeDto,    SpecMassnahme>().ForMember(p => p.ChangedProps, opt => opt.Ignore())
                                                           .ForMember(p => p.Montags, opt => opt.Ignore())
                                                           .ForMember(p => p.Dienstags, opt => opt.Ignore())
                                                           .ForMember(p => p.Mittwochs, opt => opt.Ignore())
                                                           .ForMember(p => p.Donnerstags, opt => opt.Ignore())
                                                           .ForMember(p => p.Freitags, opt => opt.Ignore())
                                                           .ForMember(p => p.Samstags, opt => opt.Ignore())
                                                           .ForMember(p => p.Sonntags, opt => opt.Ignore());
            CreateMap<SpecMassnahme,       SpecMassnahmeDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());


            CreateMap<Models.Stock.Warengruppe, KeyNameDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<KeyNameDto,               Models.Stock.Warengruppe>();
            CreateMap<Lagerort,                 LagerortDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<LagerortDto,              Lagerort>().ForMember(p => p.Einlagerungen, opt => opt.Ignore());
            CreateMap<ArtikelLfMap,             MaterialLieferant>().ForMember(p => p.Lieferant, opt => opt.Ignore())
                                                                    .ForMember(p => p.NoRank, opt => opt.Ignore())
                                                                    .ForMember(p => p.Rank1, opt => opt.Ignore())
                                                                    .ForMember(p => p.Rank2, opt => opt.Ignore())
                                                                    .ForMember(p => p.Rank3, opt => opt.Ignore());
            CreateMap<BestellDto,               Bestellung>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<BestellposDto,            Bestellposition>().ForMember(p => p.ErrorList, opt => opt.Ignore())
                                                                  .ForMember(p => p.Bestellung, opt => opt.Ignore());

            CreateMap<Artikel,                  ArtikelDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                             .ForMember(p => p.Einlagerungen, opt => opt.Ignore());
            CreateMap<ArtikelDto,               Artikel>().ForMember(p => p.ErrorList, opt => opt.Ignore())
                                                          .ForMember(p => p.Einlagerungen, opt => opt.Ignore())
                                                          .ForMember(p => p.Dokumente, opt => opt.Ignore());

            CreateMap<ArtikelLfMap,             ArtikelLieferant>().ForMember(p => p.Rank0, opt => opt.Ignore())
                                                                   .ForMember(p => p.Rank1, opt => opt.Ignore())
                                                                   .ForMember(p => p.Rank2, opt => opt.Ignore())
                                                                   .ForMember(p => p.Rank3, opt => opt.Ignore());
            CreateMap<ArtikelIhoMap,            ArtikelIHObjekt>();
            CreateMap<ArtikelLieferant,         ArtikelLfMap>().ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                               .ForMember(p => p.Materialstamm_Id, opt => opt.Ignore());
            CreateMap<ArtikelIHObjekt,          ArtikelIhoMap>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Dtos.Stock.LieferantDto,  Lieferant>().ForMember(p => p.ChangedProps, opt => opt.Ignore())
                                                                             .ForMember(p => p.LieferantFürIHObjekte, opt => opt.Ignore())
                                                                             .ForMember(p => p.LieferantFürAufträge, opt => opt.Ignore());
            CreateMap<LieferantApDto,           LieferantAP>().ForMember(p => p.ChangedProps, opt => opt.Ignore());

            CreateMap<Bestellung,               BestellDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Bestellposition,          BestellposDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Lieferant,                Dtos.Stock.LieferantDto>();
            CreateMap<LieferantAP,              LieferantApDto>();
            CreateMap<LieferscheinDto,          Lieferschein>().ConvertUsing(new LieferscheinDtoConverter());
            CreateMap<BestellInfoDto,           Bestellposition>().ConvertUsing(new BestellInfoConverter());
            CreateMap<Lieferschein,             LieferscheinDto>().ConvertUsing(new LieferscheinModelConverter());
            CreateMap<Wareneingang,             EingangDto>().ForMember(p => p.Material, opt => opt.Ignore())
                                                             .ForMember(p => p.Bestellposition, opt => opt.Ignore())
                                                             .ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                             .AfterMap((model, dto) => {

                                                                dto.Bestellposition_Id = model.Bestellposition.Id;
                                                                if (model.Material != null)     dto.Material_Id = model.Material.Id;
                                                                if (model.Lieferschein != null) dto.Lieferschein_Id = model.Lieferschein.Id;
                                                             });
            CreateMap<EingangDto,               Wareneingang>().ConvertUsing(new EingangDtoConverter());

            CreateMap<Wareneinlagerung,         WLInfoDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<WLInfoDto,                Wareneinlagerung>().ForMember(p => p.ErrorList, opt => opt.Ignore())
                                                                   .ForMember(p => p.Material, opt => opt.Ignore())
                                                                   .ForMember(p => p.Wareneingang, opt => opt.Ignore());

            CreateMap<EinlagerungDto,           Wareneinlagerung>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<Wareneinlagerung,         EinlagerungDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<AusgangDto,               Warenausgang>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<Warenausgang,             AusgangDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<ReklamDto,                Reklamation>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<ReklamposDto,             Reklamposition>().ForMember(p => p.ErrorList, opt => opt.Ignore());
            CreateMap<Reklamposition,           ReklamposDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<Reklamation,              ReklamDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());


            CreateMap<EmmaDto,       Leistung>().ForMember(p => p.Statuslist, opt => opt.Ignore());
            CreateMap<Leistung,      EmmaDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());


            CreateMap<Ticketprotokoll,          TicketprotokollDto>();
            CreateMap<TicketprotokollDto,       Ticketprotokoll>().ForMember(p => p.ChangedProps, opt => opt.Ignore());

            CreateMap<TicketDetailDto,          Ticket>().ForMember(p => p.LeDatum, opt => opt.Ignore())
                                                         .ForMember(p => p.LeUhrzeitVon, opt => opt.Ignore())
                                                         .ForMember(p => p.LeUhrzeitBis, opt => opt.Ignore())
                                                         .ForMember(p => p.LeAnmerkung, opt => opt.Ignore())
                                                         .ForMember(p => p.LeMaterial, opt => opt.Ignore())
                                                         .ForMember(p => p.LeIstFertig, opt => opt.Ignore())
                                                         .ForMember(p => p.LeIstNichtFertig, opt => opt.Ignore())
                                                         .ForMember(p => p.AbnahmeAbgenommen, opt => opt.Ignore())
                                                         .ForMember(p => p.AbnahmeAbgelehnt, opt => opt.Ignore())
                                                         .ForMember(p => p.AbnahmeBegründung, opt => opt.Ignore())
                                                         .ForMember(p => p.Errors, opt => opt.Ignore())
                                                         .ForMember(p => p.IstSauber, opt => opt.Ignore())
                                                         .ForMember(p => p.Dokumente, opt => opt.Ignore());
            CreateMap<TicketDetailDto,          Auftrag>().ForMember(p => p.Projektnummer, opt => opt.Ignore())
                                                          .ForMember(p => p.ErstelltAm, opt => opt.Ignore())
                                                          .ForMember(p => p.GeändertAm, opt => opt.Ignore())
                                                          .ForMember(p => p.ExterneNummer, opt => opt.Ignore())
                                                          .ForMember(p => p.FBNummer, opt => opt.Ignore())
                                                          .ForMember(p => p.ImFremdbudget, opt => opt.Ignore())
                                                          .ForMember(p => p.InterneAuStelle, opt => opt.Ignore())
                                                          .ForMember(p => p.Titel, opt => opt.Ignore())
                                                          .ForMember(p => p.GemeldetDatum, opt => opt.Ignore())
                                                          .ForMember(p => p.ÜbergabeDatum, opt => opt.Ignore())
                                                          .ForMember(p => p.GemeldetZeit, opt => opt.Ignore())
                                                          .ForMember(p => p.ÜbergabeZeit, opt => opt.Ignore())
                                                          .ForMember(p => p.Störzeit, opt => opt.Ignore())
                                                          .ForMember(p => p.Auftragsleiterdatum, opt => opt.Ignore())
                                                          .ForMember(p => p.Abnahmedatum, opt => opt.Ignore())
                                                          .ForMember(p => p.Menge, opt => opt.Ignore())
                                                          .ForMember(p => p.Wert, opt => opt.Ignore())
                                                          .ForMember(p => p.Stunden, opt => opt.Ignore())
                                                          .ForMember(p => p.SubSchadensbild, opt => opt.Ignore())
                                                          .ForMember(p => p.SubSchadensursache, opt => opt.Ignore())
                                                          .ForMember(p => p.Konfiguration, opt => opt.Ignore())
                                                          .ForMember(p => p.Beauftragungsdatum, opt => opt.Ignore())
                                                          .ForMember(p => p.Materialleistungen, opt => opt.Ignore())
                                                          .ForMember(p => p.Dokumente, opt => opt.Ignore())
                                                          .ForMember(p => p.UnveränderlicherTermin, opt => opt.Ignore())
                                                          .AfterMap((dto, model) => {
                                                                    model.GemeldetDatum = dto.GemeldetAm.Date;
                                                                    model.GemeldetZeit = dto.GemeldetAm.TimeOfDay;
                                                                   });


            CreateMap<WiMassnahmeDto,           WiMassnahme>().ForMember(p => p.Medien, opt => opt.Ignore())
                                                              .ForMember(p => p.Montags, opt => opt.Ignore())
                                                              .ForMember(p => p.Dienstags, opt => opt.Ignore())
                                                              .ForMember(p => p.Mittwochs, opt => opt.Ignore())
                                                              .ForMember(p => p.Donnerstags, opt => opt.Ignore())
                                                              .ForMember(p => p.Freitags, opt => opt.Ignore())
                                                              .ForMember(p => p.Samstags, opt => opt.Ignore())
                                                              .ForMember(p => p.Sonntags, opt => opt.Ignore())
                                                              .ForMember(p => p.Dokumente, opt => opt.Ignore());
            CreateMap<WiMassnahme,              WiMassnahmeDto>().ForMember(p => p.Medien, opt => opt.Ignore())
                                                                 .ForMember(p => p.Materialien, opt => opt.Ignore())
                                                                 .ForMember(p => p.ChangeStatus, opt => opt.Ignore());
                                                                // .AfterMap((model, dto) => dto.Medien = mmapper.AsDtoList(p => p.AsDto(model.Medien)))
                                                                // .AfterMap((model, dto) => dto.Materialien = mapper.AsDtoList(p => p.AsDto(model.Materialien)));

            CreateMap<WiMaterialVorgabeDto,     WiMaterialVorgabe>().ForMember(p => p.Massnahme, opt => opt.Ignore());
            CreateMap<WiMaterialVorgabe,        WiMaterialVorgabeDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<ChecklistPositionDto,     Checklistposition>().ForMember(p => p.ChangedProps, opt => opt.Ignore())
                                                                    .ForMember(p => p.InVorigerSitzungErledigt, opt => opt.Ignore());
            CreateMap<Checklistposition,        ChecklistPositionDto>();

            CreateMap<Wartungsergebnis,         WartungsergebnisDto>().ForMember(p => p.Erledigte, opt => opt.Ignore())
                                                                      .ForMember(p => p.Folgeauftrag, opt => opt.Ignore());

            CreateMap<MassnahmeViewItem,        MassnahmeListDto>().ForMember(p => p.AnzahlPositionen, opt => opt.Ignore())
                                                                   .ForMember(p => p.ChangeStatus, opt => opt.Ignore())
                                                                   .ForMember(p => p.Menge, opt => opt.Ignore());
            CreateMap<MassnahmeListDto,         MassnahmeViewItem>();

            CreateMap<Planaufgabe,              PositionSubmitDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<TechWertDto,              TechnischerWert>().ForMember(p => p.BoolWert, opt => opt.Ignore());
            CreateMap<TechnischerWert,          TechWertDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<AssetArtikelDto,          ArtikelSelectModel>().ForMember(p => p.ChangedProps, opt => opt.Ignore());
            CreateMap<ArtikelSelectModel,       AssetArtikelDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<IHObjektPersonal,         IHObjektPersonalDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<IHObjektPersonalDto,      IHObjektPersonal>();

            CreateMap<AnwesenheitDto,           Anwesenheit>();
            CreateMap<Anwesenheit,              AnwesenheitDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());

            CreateMap<AnwesenheitMassnahme,     AnwesenheitMassnahmeDto>().ForMember(p => p.ChangeStatus, opt => opt.Ignore());
            CreateMap<AnwesenheitMassnahmeDto,  AnwesenheitMassnahme>();
        }
    }
}