﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IMaterialController : IVwTypController
    {}


    public class MaterialController : ControllerBase, IMaterialController
    {

        private readonly IMaterialService store;
        private readonly IFlyoutService flyouts;
        private readonly VerwaltungViewModel mainView;
        private readonly MaterialViewModel view;
        private readonly MaterialLfViewModel lfView;
        private SerializableCommand deleteCommand;
        private SerializableCommand assignLfCommand;
        private List<LieferantDto> lfliste;
        private int idstamp;


        public MaterialController(IMessageService messages, IMaterialService store, IFlyoutService flyouts, IControllerBus bus,
                                  VerwaltungViewModel mainView, MaterialViewModel view, MaterialLfViewModel lfView)
          : base(messages, bus)
        {

            this.store = store;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.lfView = lfView;
        }


        public void Initialize()
        {

            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => view.Current != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());
            view.AssignLfCommand = assignLfCommand = CreateCommand(_ => AssignLfAsync(), _ => view.Current != null);

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;

            store.Reset();
            view.Unload();
            lfView.Unload();
            lfliste = null;
            return true;
        }


        private async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                await store.SaveAsync(bus.Token);
                await LoadAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var oldmodel = view.Current;

                var mtask = store.LoadAsync(bus.Token);
                var etask = store.EinheitenAsync(bus.Token);
                var wtask = store.WarengruppenAsync(bus.Token);
                var ltask = store.LieferantenAsync(bus.Token);

                await Task.WhenAll(mtask, ltask, etask, wtask).ConfigureAwait(false);


                await OnUIThreadAsync(() => {

                    view.Liste = store.CurrentList;
                    view.Einheiten = etask.Result;
                    view.Warengruppen = wtask.Result;

                    lfliste = ltask.Result.OrderBy(p => p.Name1).ThenBy(p => p.Name2).ToList();
                    var lfdic = lfliste.ToDictionary(p => p.Id);
                    view.Liste.SelectMany(p => p.MaterialLieferantMap).ForEach(p => { p.Lieferant = (lfdic[p.Lieferant_Id].Name1 + " " + lfdic[p.Lieferant_Id].Name2).Trim(); });
                    view.Liste.ForEach(p => p.MaterialLieferantMap.AcceptChanges());

                    if (oldmodel != null) view.Current = mtask.Result.FirstOrDefault(p => p.Id == oldmodel.Id);

                });


                idstamp = 0;
            }
        }


        private async Task AddAsync()
        {

            if (view.Liste == null) return;

            await OnUIThreadAsync(() => { 

                var model = new Materialstamm { Id = --idstamp, MaterialLieferantMap = new TrackableCollection<MaterialLieferant>() };
                view.Liste.Add(model);
                view.Current = model;

            });
        }


        private async Task DeleteAsync()
        {

            if (view.Liste == null) return;

            await OnUIThreadAsync(() => { 

                if (view.Current == null) return;
                view.Liste.Remove(view.Current);
                view.Current = null;

            });
        }


        private async Task AssignLfAsync()
        {

            if (view.Current == null) return;

            lfView.Liste = new ObservableCollection<MtLfAssignItem>(lfliste.Select(lf => new MtLfAssignItem(view.Current, lf)));
            await flyouts.OpenAsync(new FlyoutConfig { ChildView = lfView.View, Header = "Lieferanten zuordnen", Position = MahApps.Metro.Controls.Position.Right });
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == "Current") UpdateCommands();
        }


        private void UpdateCommands()
        {
            context.Post(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
                assignLfCommand.RaiseCanExecuteChanged();
            }), null);
        }
    }
}
