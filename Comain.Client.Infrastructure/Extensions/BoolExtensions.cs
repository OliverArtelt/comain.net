﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain
{

    public static class BoolExtensions
    {

        /// <summary>
        /// True: alle Booleans haben den gleichen Wert
        /// </summary>
        static public bool AllEqual(this bool firstValue, params bool[] bools)
        {
            return bools.All(thisBool => thisBool == firstValue);
        }    
    }
}
