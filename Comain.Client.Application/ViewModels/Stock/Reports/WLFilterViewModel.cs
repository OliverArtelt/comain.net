﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class WLFilterViewModel : ViewModel<IWLFilterView>, IFilterViewModel
    {

        public WLFilterViewModel(IWLFilterView view) : base(view)
        {}


        public void Reset()
        {
            RestmengeVorhanden = false;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {

            if (RestmengeVorhanden) return "Restmenge vorhanden";
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (RestmengeVorhanden) filter.Add("RestmengeVorhanden", true);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private bool restmengeVorhanden;
        public bool RestmengeVorhanden
        { 
            get { return restmengeVorhanden; }
            set { SetProperty(ref restmengeVorhanden, value); }
        } 
   

#endregion

    }
}
