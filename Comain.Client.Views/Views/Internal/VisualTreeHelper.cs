﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;


namespace Comain
{

    /// <remarks>
    /// https://rachel53461.wordpress.com/2011/10/09/navigating-wpfs-visual-tree/
    /// https://stackoverflow.com/questions/25502150/wpf-datagrid-get-row-number-which-mouse-cursor-is-on
    /// http://www.hardcodet.net/2009/03/moving-data-grid-rows-using-drag-and-drop
    /// </remarks>
    public static class VisualTreeHelperExtensions
    {

        public static T TryFindParent<T>(this DependencyObject child) where T : DependencyObject
        {

            DependencyObject parentObject = GetParentObject(child);
            if(parentObject == null) return null;

            T parent = parentObject as T;
            if (parent != null) return parent;
            else return TryFindParent<T>(parentObject);
        }


        public static DependencyObject GetParentObject(this DependencyObject child)
        {

            if(child == null) return null;
            ContentElement contentElement = child as ContentElement;

            if(contentElement != null) {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if(parent != null) return parent;

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            return VisualTreeHelper.GetParent(child);
        }


        public static T FindChild<T>(this DependencyObject parent, string childName) where T : DependencyObject
        {

            if (parent == null) return null;
            T foundChild = null;
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++) {

                var child = VisualTreeHelper.GetChild(parent, i);
                T childType = child as T;

                if (childType == null) {

                    foundChild = FindChild<T>(child, childName);
                    if (foundChild != null) break;

                } else if (!string.IsNullOrEmpty(childName)) {

                    var frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == childName) {

                        foundChild = (T)child;
                        break;

                    } else {

                        foundChild = FindChild<T>(child, childName);
                        if (foundChild != null) break;
                    }

                } else {

                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }


        public static T FindChild<T>(this DependencyObject parent) where T : DependencyObject
        {

            if (parent == null) return null;
            T foundChild = null;
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++) {

                var child = VisualTreeHelper.GetChild(parent, i);
                T childType = child as T;

                if (childType == null) {

                    foundChild = FindChild<T>(child);
                    if (foundChild != null) break;

                } else {

                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }


        public static T TryFindFromPoint<T>(UIElement reference, Point point) where T : DependencyObject
        {

            DependencyObject element = reference.InputHitTest(point) as DependencyObject;
            if (element == null) return null;
            else if(element is T) return (T)element;
            else return TryFindParent<T>(element);
        }
    }
}
