﻿using System;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.ViewModels.Reports.Filters
{

    public class LAListViewItem : NotifyBase
    {

        private readonly LeistungsartFilterModel vm;


        public LAListViewItem(LeistungsartFilterModel vm)
        {
            this.vm = vm;
        }


        public int    Id     { get { return vm.Id; } }
        public String Nummer { get { return vm.Nummer; } }
        public String Name   { get { return vm.Name; } }


        public bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }


        public void SelektZyklische()    { IsSelected = vm.IstWI; }
        public void SelektPlanmäßige()   { IsSelected = vm.IstPlankosten; }
        public void SelektUnplanmäßige() { IsSelected = vm.IstUnPlankosten; }
        public void SelektPkz()          { IsSelected = vm.IstProdKennziffer; }
        public void SelektPräventive()   { IsSelected = vm.IstPräventation; }
    }
}
