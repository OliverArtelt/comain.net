﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views.Aufträge;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Materialien;

namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragDetailLeistungViewModel : ValidatableViewModel<IAuftragDetailLeistungView, Auftrag>
    {

        private readonly SynchronizationContext context;


        public AuftragDetailLeistungViewModel(IAuftragDetailLeistungView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(Auftrag oldSelected)
        {

            Refresh();
            RaisePropertyChanged(nameof(Aktivitätenbeschreibung));
            RaisePropertyChanged(nameof(InterneAuStelle));
            RaisePropertyChanged("PreiseAnzeigen");
            RaisePropertyChanged(nameof(Abrechnungsschluss));
        }


        public void Refresh()
        {

            int? le = Personalleistung?.Id;
            int? mt = Materialleistung?.Id;

            Unload();
            if (Current == null) return;

            Personalleistungen = Current.Personalleistungen.OrderByDescending(p => p.Beginn).ToList();
            Materialleistungen = Current.Materialleistungen.OrderByDescending(p => p.Datum).ToList();
            if (le.HasValue) Personalleistung = Personalleistungen.FirstOrDefault(p => p.Id == le.Value);
            if (mt.HasValue) Materialleistung = Materialleistungen.FirstOrDefault(p => p.Id == mt.Value);

            RaisePropertyChanged(nameof(Personalleistungen));
            RaisePropertyChanged(nameof(Materialleistungen));
            RaisePropertyChanged(nameof(KostenPersonalleistungen));
            RaisePropertyChanged(nameof(KostenMaterialleistungen));
        }


        public void Unload()
        {

            Personalleistungen = null;
            Materialleistungen = null;
            Personalleistung = null;
            Materialleistung = null;
        }


#region P R O P E R T I E S


        public DateTime? Abrechnungsschluss { get { return current == null || current.Konfiguration == null? null: current.Konfiguration.Abrechnungsschluß ; } }


        private ICommand lEEditCommand;
        public ICommand LEEditCommand
        {
            get { return lEEditCommand; }
            set { SetProperty(ref lEEditCommand, value); }
        }


        private ICommand mTEditCommand;
        public ICommand MTEditCommand
        {
            get { return mTEditCommand; }
            set { SetProperty(ref mTEditCommand, value); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool istKunde;
        public bool IstKunde
        {
            get { return istKunde; }
            set { SetProperty(ref istKunde, value); }
        }


        public decimal? KostenPersonalleistungen
        {
            get { return current == null? (decimal?)null: current.KostenPersonalleistungen; }
        }


        public decimal? KostenMaterialleistungen
        {
            get { return current == null? (decimal?)null: current.KostenMaterialleistungen; }
        }


        public String Aktivitätenbeschreibung
        {
            get { return current == null? null: current.Aktivitätenbeschreibung; }
            set {

                if (current != null && current.Aktivitätenbeschreibung != value) {

                    current.Aktivitätenbeschreibung = value;
                    RaisePropertyChanged("Aktivitätenbeschreibung");
                }
            }
        }


        public int? InterneAuStelle
        {
            get { return current == null? (int?)null: current.InterneAuStelle; }
            set {

                if (current != null && current.InterneAuStelle != value) {

                    current.InterneAuStelle = value;
                    RaisePropertyChanged("InterneAuStelle");
                }
            }
        }


        public List<Personalleistung> Personalleistungen { get; private set; }
        public List<Materialleistung> Materialleistungen { get; private set; }


        private Personalleistung personalleistung;
        public Personalleistung Personalleistung
        {
            get { return personalleistung; }
            set { SetProperty(ref personalleistung, value); }
        }


        private Materialleistung materialleistung;
        public Materialleistung Materialleistung
        {
            get { return materialleistung; }
            set { SetProperty(ref materialleistung, value); }
        }


#endregion

    }
}
