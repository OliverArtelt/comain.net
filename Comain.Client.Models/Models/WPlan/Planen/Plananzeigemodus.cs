﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.WPlan.Planen
{

    public enum Plananzeigemodus
    {
        Zeiten, Kosten, Personal, Auftrag, Material
    }


    public static class PlananzeigemodusExtended
    {

        public static int Width(this Plananzeigemodus value)
        {

            if (value == Plananzeigemodus.Auftrag) return 120;
            if (value == Plananzeigemodus.Personal) return 200;
            return 70;
        }
    }
}
