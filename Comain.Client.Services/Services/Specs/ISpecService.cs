﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.Specs;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;

namespace Comain.Client.Services.Specs
{
    public interface ISpecService
    {

        Task<List<SpecListItem>>                SearchAsync(Suchkriterien search, CancellationToken ct = default);
        Task<Spezifikation>                     DetailAsync(int id, CancellationToken ct = default);
        Task<Spezifikation>                     UpdateAsync(Spezifikation model, CancellationToken ct = default);
        Task                                    DeleteAsync(Spezifikation model, CancellationToken ct = default);
                                                
        Task<IList<NummerFilterModel>>          EinheitenAsync(CancellationToken ct = default);
        Task<List<MaterialstammFilterModel>>    MaterialstammAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>          GewerkeAsync(CancellationToken ct = default);
        Task<IList<LeistungsartFilterModel>>    LeistungsartenAsync(CancellationToken ct = default);
        Task<IList<AssignedEntity>>             AssignedEntitiesAsync(int specId, CancellationToken ct = default);
    }
}