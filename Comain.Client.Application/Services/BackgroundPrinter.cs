﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Comain.Client.ViewModels;


namespace Comain.Client.Services
{

    public class BackgroundPrinter : IBackgroundPrinter, IDisposable
    {
       
        private const String deviceInfo =
              @"<DeviceInfo>
                    <OutputFormat>EMF</OutputFormat>
                    <PageWidth>8.5in</PageWidth>
                    <PageHeight>11in</PageHeight>
                    <MarginTop>0.5in</MarginTop>
                    <MarginLeft>1in</MarginLeft>
                    <MarginRight>0.25in</MarginRight>
                    <MarginBottom>0.25in</MarginBottom>
                </DeviceInfo>";

        private IList<Stream> streams;
    


        public void Print(ReportResult reportData)
        {

          //  using (var report = new LocalReport()) {
          //
          //
          //      if (reportData.DataSources != null) 
          //          reportData.DataSources.ForEach(p => report.DataSources.Add(new ReportDataSource(p.Item1, p.Item2)));
          //
          //      report.DisplayName = reportData.Title;
          //
          //      Assembly assembly = Assembly.Load(reportData.AssemblyAddress);
          //      using (var stream = assembly.GetManifestResourceStream(reportData.ReportAddress)) {
          //
          //          report.LoadReportDefinition(stream);
          //          if (reportData.Parameters != null) {
          //
          //              var pars = reportData.Parameters.Select(p => new ReportParameter(p.Item1, p.Item2)).ToList();
          //              report.SetParameters(pars);
          //          }
          //      }
          //
          //      Export(report);
          //      PrintImpl();
          //  }
        }


#region D I S P O S I N G


        private bool disposed = false;


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        private void Dispose(bool disposing)
        {

            if (!this.disposed) {

                if (disposing) {

                    if (streams != null) {

                        streams.ForEach(p => p.Dispose());
                        streams = null;
                    }
                }

                disposed = true;
            }
        }


        ~BackgroundPrinter()
        {
            Dispose(false);
        }

#endregion

    }
}
