﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class LeistungKSTReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly LbKstViewModel lbView;


        public LeistungKSTReport(IReportService store, LbKstViewModel lbView, ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, IntAuStelleViewModel intView)
        {
            
            this.store = store;
            this.lbView = lbView;
            filterViews = new List<IFilterViewModel> { lbView, zeitView, kstView, pzView, intView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.LeistungKstAsync(GebeFilter(), ct));
            result.AddParameter("parShowMwst", lbView.Mwst? "TRUE": "FALSE");
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Leistungsbericht - Kostenstellen"; }
        }


        public string Gruppe
        {
            get { return "Personal"; }
        }


        public int Sortierung
        {
            get { return 500; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.LeistungKSTReport.rdlc"; }
        }
    }
}
