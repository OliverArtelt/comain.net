﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Microsoft.Extensions.DependencyInjection;

namespace Comain.Client.Controllers
{

    public class ControllerBus : IControllerBus, IControllerMediator
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IMessageService messages;
        private readonly IServiceProvider services;
        private readonly MainViewModel view;
        private CancellationTokenSource cts = new CancellationTokenSource();
        private Dictionary<Type, IController> registeredControllers;
        private Stack<StackedControllerMessage> busMessages;
        private SynchronizationContext context = SynchronizationContext.Current;


        public ControllerBus(IBreadcrumbService crumbs, IMessageService messages, MainViewModel view,
                             IServiceProvider services)
        {

            this.crumbs = crumbs;
            this.messages = messages;
            this.services = services;
            this.view = view;
            busMessages = new Stack<StackedControllerMessage>();
            registeredControllers = new Dictionary<Type, IController>();
        }


        public CancellationToken Token => cts.Token;


        public void CancelToken()
        {

            cts.Cancel();
            cts = new CancellationTokenSource();
        }


        public T GetController<T>() where T: IController
        {

            IController con = registeredControllers.GetValueOrDefault(typeof(T));

            if (con == null) {

                context.Send(new SendOrPostCallback((o) => {

                    try {

                        con = services.GetService<T>();
                        con.Initialize();
                    }
                    catch (Exception x) {

                        messages.Show(x);
                    }

                }), null);

                if (con != null) registeredControllers.Add(typeof(T), con);
            }

            return (T)con;
        }


        public IController GetController(Type type)
        {

            IController con = registeredControllers.GetValueOrDefault(type);

            if (con == null) {

                context.Send(new SendOrPostCallback((o) => {

                    try {

                        con = (IController)services.GetService(type);
                        con.Initialize();
                    }
                    catch (Exception x) {

                        messages.Show(x);
                    }

                }), null);

                if (con != null) registeredControllers.Add(type, con);
            }

            return con;
        }


        public void Reset()
        {

            CancelToken();
            busMessages.Clear();
            crumbs.Clear();
            Current = null;
        }


        public void Shutdown()
        {

            Reset();
            registeredControllers.Values.ForEach(p => p.Shutdown());
            registeredControllers.Clear();
        }


        public IController Current { get; private set; }


        public async Task JumpAsync(Type type, object parameter = null)
        {

            var ctrl = GetController(type);
            if (ctrl == null) return;

            Current = ctrl;
            await Current.RunAsync(parameter);
            OnCurrentControllerChanged();
        }


        public async Task JumpAsync(ControllerMessage message)
        {

            var ctrl = GetController(message.Destination);
            if (ctrl == null) return;

            Current = ctrl;
            busMessages.Push(new StackedControllerMessage { Message = message, BreadcrumbDepth = crumbs.Depth });
            await Current.RunAsync(message);

            OnCurrentControllerChanged();
        }


        public async Task<ControllerMessage> CallAsync(ControllerMessage message)
        {

            var ctrl = GetController(message.Destination);
            if (ctrl == null) return null;

            Current = ctrl;
            var tcs = new TaskCompletionSource<ControllerMessage>();
            busMessages.Push(new StackedControllerMessage { Message = message, BreadcrumbDepth = crumbs.Depth, WaitableResult = tcs, View = view.PageView });
            await Current.RunAsync(message);

            OnCurrentControllerChanged();
            return await tcs.Task;
        }


        public async Task BackAsync(object result = null)
        {

            if (Current != null && !await Current.UnloadAsync()) return;
            Current = null;

            try {

                if (busMessages.Count > 0) {

                    var stackedMessage = busMessages.Pop();
                    Current = stackedMessage.Message.Source;
                    crumbs.Trim(stackedMessage.BreadcrumbDepth);
                    stackedMessage.Message.Result = result;

                    if (stackedMessage.WaitableResult != null) {

                        if (stackedMessage.View != null) view.PageView = stackedMessage.View;
                        stackedMessage.WaitableResult.TrySetResult(stackedMessage.Message);

                    } else {

                        await Current.RunAsync(stackedMessage.Message);
                    }
                }

                OnCurrentControllerChanged();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        public event EventHandler CurrentControllerChanged;


        protected virtual void OnCurrentControllerChanged()
        {
            if (CurrentControllerChanged != null) { CurrentControllerChanged(this, new CurrentControllerChangedEventArgs(Current)); }
        }
    }
}
