﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class IHObjektCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IIHObjektController);
        public String       Name        => "Assets";
        public String       Parameter   => "Asset";
        public String       Gruppe      => "Assets";
        public int          Sortierung  => 1000;
        public String       Color       => "Goldenrod";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.cogs.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
