﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragDetailPrintViewModel : PrintViewModel<IAuftragDetailPrintView>
    {

        private enum PrintMode { Vollständig, Unvollständig, Wartungsplan }
        private PrintMode mode = PrintMode.Unvollständig;


        public AuftragDetailPrintViewModel(IMessageService messages, IAuftragDetailPrintView view) : base(messages, view)
        {}


        private void RaiseModeChanged()
        {

            RaisePropertyChanged(nameof(DruckVollständig));
            RaisePropertyChanged(nameof(DruckUnvollständig));
            RaisePropertyChanged(nameof(DruckWartungsplan));
        }


#region P R O P E R T I E S


        private bool hatWartungsplan;
        public bool HatWartungsplan
        {
            get { return hatWartungsplan; }
            set {

                if (SetProperty(ref hatWartungsplan, value)) {

                    if (DruckWartungsplan && !value) DruckUnvollständig = true;
                }
            }
        }


        public bool DruckVollständig
        {
            get { return mode == PrintMode.Vollständig; }
            set {

                if (value && mode != PrintMode.Vollständig) {

                    mode = PrintMode.Vollständig;
                    RaiseModeChanged();
                }
            }
        }


        public bool DruckUnvollständig
        {
            get { return mode == PrintMode.Unvollständig; }
            set {

                if (value && mode != PrintMode.Unvollständig) {

                    mode = PrintMode.Unvollständig;
                    RaiseModeChanged();
                }
            }
        }


        public bool DruckWartungsplan
        {
            get { return mode == PrintMode.Wartungsplan; }
            set {

                if (value && mode != PrintMode.Wartungsplan) {

                    mode = PrintMode.Wartungsplan;
                    RaiseModeChanged();
                }
            }
        }


#endregion

    }
}
