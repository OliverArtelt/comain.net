﻿using System;


namespace Comain.Client.Dtos.Stock
{

    /// <summary>
    /// Listobjekt Wareneingänge (Kuddelmuddel aus Lieferschein und Wareneingang) 
    /// </summary>
    public class EingangListDto
    {

        public int          LieferscheinId          { get; set; }
        public int          EingangId               { get; set; }
        public String       Bestellnummer           { get; set; }
        public String       Lieferschein            { get; set; }
        public String       Artikelnummer           { get; set; }
        public String       Artikelbezeichnung      { get; set; }
        public decimal      Menge                   { get; set; }
        public String       Auftrag                 { get; set; }
        public String       Kostenstelle            { get; set; }
        public DateTime?    BestelltAm              { get; set; }
        public DateTime?    GeliefertAm             { get; set; }
    }
}
