﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Summen eines Standortes einer Periode
    /// </summary>
    public class PZSpaltensumme : NotifyBase, IAcceptor
    {

        private Planspalte period;


        public PZSpaltensumme(PZPlanzeile parent, Planspalte period)
        {

            Parent = parent;
            this.period = period;
        }


        public Plan                 Plan    => Parent.Parent;
        public PZPlanzeile          Parent  { get; }
        public List<IHSpaltensumme> Zellen  { get; } = new List<IHSpaltensumme>();
        public int                  ColumnIndex   => period.Index;


        public decimal? Stunden     => Zellen?.Sum(p => p.Stunden.GetValueOrDefault());
        public decimal? Kosten      => Zellen?.Sum(p => p.Kosten.GetValueOrDefault());

        public double   Height      => Parent.Height;
        public double   Width       => Plan.Spaltenbreite;
        public double   Left        => ColumnIndex * Width;
        public double   Top         => Parent.YStartposition;
        public Brush    TypAsFarbe  => Parent.TypAsFarbe;


        public String Wert 
        {
            get {

                if (Anzeigemodus == Plananzeigemodus.Zeiten && Stunden.HasValue) return String.Format("{0:N1} h", Stunden.Value);
                if (Anzeigemodus == Plananzeigemodus.Kosten && Kosten.HasValue) return Kosten.Value.ToString("c");
                return null;
            }
        }


        public decimal ExportierteGeplanteStunden(PlanOptions opts)
            => Zellen.Sum(p => p.ExportierteGeplanteStunden(opts));

        public decimal ExportierteGeplanteKosten(PlanOptions opts)
            => Zellen.Sum(p => p.ExportierteGeplanteKosten(opts));


        private Plananzeigemodus anzeigemodus;
        public Plananzeigemodus Anzeigemodus
        {
            get { return anzeigemodus; }
            set { 
            
                if (SetProperty(ref anzeigemodus, value)) {

                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }

        
        public void NotifyPositionChanged()
        {

            RaisePropertyChanged(nameof(Height));
            RaisePropertyChanged(nameof(Width));
            RaisePropertyChanged(nameof(Left));
            RaisePropertyChanged(nameof(Top));
            RaisePropertyChanged(nameof(TypAsFarbe));
        }


        public void Accept(IVisitor v)
        {
            v.Visit(this);
        }
    }
}
