﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;

namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{

    public class EinlagerFilterViewModel : ViewModel<IEinlagerFilterView>
    {

        private readonly SynchronizationContext context;


        public EinlagerFilterViewModel(IEinlagerFilterView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();

            if (!String.IsNullOrEmpty(Artikel)) filter.Add("Artikel", Artikel);
            if (Eingang != null) filter.Add("Eingang", Eingang.Id);
            if (Lagerort != null) filter.Add("Lagerort", Lagerort.Id);
            if (DatumVon.HasValue) filter.Add("DatumVon", DatumVon.Value);
            if (DatumBis.HasValue) filter.Add("DatumBis", DatumBis.Value);

            return filter;
        }


        public void SetzeAnzahlGefunden(int count)
        {

            if (count == 0) ResultText = "Keine Einlagerungen gefunden.";
            else if (count == 1) ResultText = "Eine Einlagerung gefunden.";
            else ResultText = String.Format("{0} Einlagerungen gefunden.", count);
        }


        public void Unload()
        {

            Eingänge = null;
            Lagerorte = null;
        }


        public void Reset()
        {

            Artikel  = null;
            Eingang  = null;
            Lagerort = null;
            DatumVon = DateTime.Today.AddMonths(-6);
            DatumBis = DateTime.Today;
        }


#region P R O P E R T I E S


        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private String artikel;
        public String Artikel
        { 
            get { return artikel; }
            set { SetProperty(ref artikel, value); }
        }


        private IList<EingangFilterDto> eingänge;
        public IList<EingangFilterDto> Eingänge
        { 
            get { return eingänge; }
            set { SetProperty(ref eingänge, value); }
        }


        private IList<Lagerort> lagerorte;
        public IList<Lagerort> Lagerorte
        { 
            get { return lagerorte; }
            set { SetProperty(ref lagerorte, value); }
        }


        private EingangFilterDto eingang;
        public EingangFilterDto Eingang
        { 
            get { return eingang; }
            set { SetProperty(ref eingang, value); }
        }
        

        private Lagerort lagerort;
        public Lagerort Lagerort
        { 
            get { return lagerort; }
            set { SetProperty(ref lagerort, value); }
        }
        

        private DateTime? datumVon;
        public DateTime? DatumVon
        { 
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }


        private DateTime? datumBis;
        public DateTime? DatumBis
        { 
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }


#endregion

    }
}
