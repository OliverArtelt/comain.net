﻿using System;
using Comain.Client.Services;
using Comain.Client.Services.Dokumente;
using Comain.Client.ViewModels.Dokumente;


namespace Comain.Client.Controllers.Dokumente
{

    public class PSDokumentController : DokumentAssignController, IPSDokumentController
    {


        public PSDokumentController(IDokumentService store, IMessageService messages, IControllerBus bus,
                                    DokumentAssignViewModel view)
          : base(store, messages, bus, view)
        {}


        protected override String EntityType => "PS";

        protected override bool   ShowDisplayAsset              => false;
        protected override bool   ShowDisplayAssetWithParents   => false;
        protected override bool   ShowDisplayAssetWithChildren  => false;
        protected override bool   ShowDisplayAuftrag            => false;
        protected override bool   ShowDisplayMassnahme          => false;

        protected override bool   ShowSearchAllDocs             => false;
        protected override bool   ShowSearchAssets              => false;
        protected override bool   ShowSearchMassnahmen          => false;
    }
}
