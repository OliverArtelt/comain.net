﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan
{
    public enum Planungstyp
    {
        Wöchentlich, Täglich, JahrKw, Jahr
    }


    public static class PlanungstypExtended
    {

        public static String AsText(this Planungstyp value)
        {

            switch (value) {

                case Planungstyp.Jahr: return "Jahresplanung 01.01. - 31.12.";
                case Planungstyp.JahrKw: return "Jahresplanung KW 01 - 52/53";
                case Planungstyp.Täglich: return "Tagesplanung";
                default: return "Wochenplanung";
            }
        }
    }
}
