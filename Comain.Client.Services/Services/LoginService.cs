﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Auth;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ViewData;
using Microsoft.Extensions.Configuration;

namespace Comain.Client.Services
{

    public class LoginService : ILoginService
    {

        private readonly List<ConnectionEndpoint> connections = new ();
        private readonly IRepository store;
        private readonly IMapper mapper;
        private ConnectionEndpoint currentStore;
        private Nutzer currentUser;


        public List<ConnectionViewItem> Endpoints  { get; private set; }


        public LoginService(IRepository store, IMapper mapper, IConfiguration config)
        {

            this.store = store;
            this.mapper = mapper;
            config.GetSection("ConnectionEndpoints").Bind(connections);
            Endpoints = connections.Select(p => new ConnectionViewItem { Key = p.Key, Name = p.Name, Color = p.Color }).ToList();
        }


        public async Task<Nutzer> LoginAsync(String endpoint, String username, String password, CancellationToken ct = default)
        {

            currentStore = connections.First(p => p.Key == endpoint);
            currentStore.Username = username;
            currentStore.Password = password;
            await store.LoginAsync(currentStore, ct).ConfigureAwait(false);
            var user = await store.GetAsync<NutzerDto>("/Login/Current", ct).ConfigureAwait(false);

            currentUser = mapper.Map<Nutzer>(user);
            currentUser.AcceptChanges();

            return currentUser;
        }


        public async Task LogoutAsync(CancellationToken ct = default)
        {

            currentStore = null;
            currentUser = null;
            await store.LogoutAsync(ct).ConfigureAwait(false);
        }


        public Task SetPasswordAsync(PasswordDto dto, CancellationToken ct = default)
        {
            return store.PostAsync<PasswordDto>("/Login/SetPassword", dto, ct);
        }


        public String AktuelleVerbindung
        {
            get { return currentStore == null? String.Empty: currentStore.Name; }
        }


        public String AktuelleAdresse
        {
            get { return currentStore == null? String.Empty: currentStore.BaseUrl; }
        }


        public Nutzer AktuellerNutzer
        {
            get { return currentUser; }
        }


        public String AktuellesProfil
        {
            get {

                if (currentUser == null) return AktuelleVerbindung;
                return String.Format("{0} @ {1}", currentUser.Fullname, AktuelleVerbindung);
            }
        }
    }
}
