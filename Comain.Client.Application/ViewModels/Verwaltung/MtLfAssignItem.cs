﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class MtLfAssignItem
    {

        private readonly Materialstamm mt;
        private readonly LieferantDto lf;


        public MtLfAssignItem(Materialstamm mt, LieferantDto lf)
        {
        
            Contract.Assert(mt != null);
            Contract.Assert(lf != null);
            
            this.mt = mt;
            this.lf = lf;
        }
        

        public String Lieferant 
        {
            
            get { 
            
                if (String.IsNullOrWhiteSpace(lf.Name2)) return lf.Name1.Trim();
                return String.Format("{0} {1}", lf.Name1, lf.Name2).Trim();            
            }
        }
        

        public bool IstZugeordnet 
        {
            get { return mt.MaterialLieferantMap.Any(p => p.Lieferant_Id == lf.Id); }
            set {

                if (value) {

                    if (mt.MaterialLieferantMap.Any(p => p.Lieferant_Id == lf.Id)) return;
                    mt.MaterialLieferantMap.Add(new MaterialLieferant { Lieferant_Id = lf.Id, Materialstamm_Id = mt.Id, Lieferant = Lieferant });  

                } else {

                    mt.MaterialLieferantMap.Remove(mt.MaterialLieferantMap.First(p => p.Lieferant_Id == lf.Id));       
                }
            }
        }
    }
}
