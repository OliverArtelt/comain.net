﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Services;
using Comain.Client.ViewData.WPlan;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanTreeViewModel : ViewModel<IPlanTreeView>
    {

        private readonly SynchronizationContext context;
        private readonly ILoginService login;
        private bool ihTreeIsSelected;
        private bool ihTreeIsExpanded;


        public PlanTreeViewModel(IPlanTreeView view, ILoginService login) : base(view)
        {

            context = SynchronizationContext.Current;
            this.login = login;
            SelectIHTreeCommand = new DelegateCommand(_ => { ihTreeIsSelected = !ihTreeIsSelected; PZList?.ForEach(p => { p.IsChecked = ihTreeIsSelected; }); });
            ExpandIHTreeCommand = new DelegateCommand(_ => { ihTreeIsExpanded = !ihTreeIsExpanded; view.ExpandTree(ihTreeIsExpanded); });
            SelectUserCommand   = new DelegateCommand(_ => { PZList?.SelectMany(p => p.IHObjekte)
                                                                    .ForEach(p => p.IsChecked = login.AktuellerNutzer.Personal_Id.HasValue && (
                                                                                                p.VerantwIH == login.AktuellerNutzer.Personal_Id ||
                                                                                                p.VerantwProd == login.AktuellerNutzer.Personal_Id)); });             
        }


        public IEnumerable<IHFilterItem> GewählteMaschinen()
            => PZList?.SelectMany(p => p.IHObjekte.Where(q => q.IsChecked)).ToList();


        public String GewählteNummern()
        {

            var ihlist = GewählteMaschinen();
            if (ihlist == null) return null;
            return String.Join(",", ihlist.Select(p => p.Nummer).ToArray());
        }


        public async Task LoadAsync(List<PZFilterItem> pzlist)
        {
            PZList = pzlist;
        }


        public void Unload()
        {

            PZList = null;
            ihTreeIsSelected = false;
            ihTreeIsExpanded = false;
        }


#region P R O P E R T I E S


        public ICommand SelectIHTreeCommand     { get; }
        public ICommand ExpandIHTreeCommand     { get; }
        public ICommand SelectUserCommand       { get; }


        private List<PZFilterItem> pzlist;
        public List<PZFilterItem> PZList
        {
            get { return pzlist; }
            set { SetProperty(ref pzlist, value); }
        }


#endregion

    }
}
