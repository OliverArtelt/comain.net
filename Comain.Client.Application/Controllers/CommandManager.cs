﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Comain.Client.Controllers
{

    public class CommandManager : ICommandManager
    {

        private readonly Stack<IUndoRedoCommand> undolist = new Stack<IUndoRedoCommand>();
        private readonly Stack<IUndoRedoCommand> redolist = new Stack<IUndoRedoCommand>();
        private SynchronizationContext context = SynchronizationContext.Current;


        public void Add(IUndoRedoCommand cmd)
        {

            if (cmd == null) return;

            redolist.Clear();
            undolist.Push(cmd);
        }


        public async Task AddAndExecuteAsync(IUndoRedoCommand cmd)
        {

            if (cmd == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => { cmd.Redo(); }), null);
            Add(cmd);
        }


        public void Clear()
        {

            undolist.Clear();
            redolist.Clear();
        }


        public async Task ExecuteUndoAsync()
        {

            if (!HasUndo) return;

            var cmd = undolist.Pop();
            await context.SendAsync(new SendOrPostCallback((o) => { cmd.Undo(); }), null);
            redolist.Push(cmd);
        }


        public async Task ExecuteRedoAsync()
        {

            if (!HasRedo) return;

            var cmd = redolist.Pop();
            await context.SendAsync(new SendOrPostCallback((o) => { cmd.Redo(); }), null);
            undolist.Push(cmd);
        }


        public IEnumerable<IUndoRedoCommand> GetModifications() => undolist.Where(p => p.ModelHasChanged).Reverse().ToList();


        public bool HasUndo => undolist.Count > 0;
        public bool HasRedo => redolist.Count > 0;
        public bool HasModifications => HasUndo && undolist.Any(p => p.ModelHasChanged);
    }
}
