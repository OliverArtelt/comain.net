﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.Stock
{

    public class AusgangDto : ChangeTrackingBase
    {

        public decimal              Menge                   { get; set; }
        public decimal?             Preis                   { get; set; }
        public DateTime             Datum                   { get; set; }
        public String               Entnahmeschein          { get; set; }
        public String               Notizen                 { get; set; }
       
        public AuSelektorDto        Auftrag                 { get; set; }
        public EinlagerungDto       Wareneinlagerung        { get; set; }
        public LagerortDto          Lagerort                { get; set; }
        public ArtikelDto           Material                { get; set; }
        public PersonalFilterModel  Lagerist                { get; set; }
        public PersonalFilterModel  Nutzer                  { get; set; }
        public KstFilterModel       Kostenstelle            { get; set; }
    }
}
