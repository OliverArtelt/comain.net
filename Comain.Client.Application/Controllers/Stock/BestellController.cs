﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels.Stock;
using Comain.Client.Services.Stock.Bestellungen;
using Comain.Client.ViewModels.Stock.Bestellungen;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Bestellungen
{

    public class BestellController : ControllerBase, IBestellController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IBestellService store;
        private readonly IBestellCommandMediator commands;
        private readonly MainViewModel mainView;
        private readonly BestellViewModel view;
        private readonly BestellListViewModel listView;
        private readonly BestellFilterViewModel filterView;
        private readonly BestellDetailViewModel detailView;
        private readonly BestellDetailBaseViewModel baseView;
        private readonly BestellDetailReportViewModel reportView;
        private readonly BestellDetailMemoViewModel memoView;
        private readonly BestellEditPosViewModel posView;
        private IAddBestellController addController;
        private bool loadCompleted;
        private IList<Lieferant> lieferanten;
        private IList<Artikel> atlist;
        private bool addMode;
        private IList<BestellListViewItem> currentList;
        private Konfiguration config;
        private SerializableCommand deleteCommand;
        private SerializableCommand editCommand;
        private SerializableCommand addPosCommand;
        private SerializableCommand deletePosCommand;
        private SerializableCommand setOrderedCommand;
        private SerializableCommand setClosedCommand;
        private SerializableCommand setStornoCommand;
        private PersonalFilterModel currentUser;


        public BestellController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                 IBestellService store, IBestellCommandMediator commands, IControllerBus bus,
                                 MainViewModel mainView, BestellViewModel view, BestellListViewModel listView, BestellFilterViewModel filterView,
                                 BestellDetailViewModel detailView, BestellDetailBaseViewModel baseView, BestellDetailReportViewModel reportView,
                                 BestellEditPosViewModel posView, BestellDetailMemoViewModel memoView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.commands = commands;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.baseView = baseView;
            this.reportView = reportView;
            this.posView = posView;
            this.memoView = memoView;
        }


        public void Initialize()
        {

            addController = bus.GetController<IAddBestellController>();

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;
            detailView.BasisView = baseView.View;
            detailView.ReportView = reportView.View;
            detailView.MemoView = memoView.View;

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.CancelCommand = CreateCommand(_ => CancelAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => listView.Current != null);
            view.EditCommand = editCommand = CreateCommand(_ => EditAsync(), _ => detailView.Current != null);
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.AddPosCommand = addPosCommand = CreateCommand(_ => AddPosAsync(), _ => detailView.Current != null);
            view.DeletePosCommand = deletePosCommand = CreateCommand(_ => DeletePosAsync(), _ => detailView.Current != null && baseView.Position != null);
            view.SetOrderedCommand = setOrderedCommand = CreateCommand(_ => SetOrderedAsync(), _ => detailView.Current != null);
            view.SetClosedCommand = setClosedCommand = CreateCommand(_ => SetClosedAsync(), _ => detailView.Current != null);
            view.SetStornoCommand = setStornoCommand = CreateCommand(_ => SetStornoAsync(), _ => detailView.Current != null);

            listView.DetailCommand = CreateCommand(_ => DetailAsync());
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(baseView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Bestellungen");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            addMode = false;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(baseView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (addMode) return await addController.UnloadAsync().ConfigureAwait(false);

            if (detailView.Current != null && commands.CurrentMode != EditMode.ReadOnly &&
                (detailView.Current.IsChanged || detailView.Current.Positionen.IsChanged)) {

                detailView.ResetTabsIfPrint();
                if (!await messages.ConfirmExitAsync()) return false;
            }

            baseView.Unload();
            reportView.Unload();
            filterView.Unload();
            listView.Unload();
            memoView.Unload();
            currentList = null;
            SetModel(null);

            return true;
        }


        private async Task LoadAsync()
        {

            SetModel(null);
            commands.SetEditMode(EditMode.ReadOnly);
            loadCompleted = false;

            listView.Unload();
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                var lftask = store.LieferantenAsync(bus.Token);
                var pstask = store.BearbeiterAsync(bus.Token);
                var cftask = store.KonfigAsync(bus.Token);
                var bstask = LoadListAsync();

                await Task.WhenAll(bstask, lftask, pstask, cftask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();
                config = cftask.Result;
                lieferanten = lftask.Result;

                filterView.SetzeListen(lieferanten);
                baseView.SetzeListen(pstask.Result);

                currentUser = null;
                if (login.AktuellerNutzer.Personal_Id.HasValue) currentUser = pstask.Result.FirstOrDefault(p => p.Id == login.AktuellerNutzer.Personal_Id.Value);
            }

            UpdateCommands();

            atlist = await store.ArtikelAsync(bus.Token);
            loadCompleted = true;
        }


        private async Task SearchAsync()
        {

            if (loadCompleted) await LoadListAsync();
            else               await LoadAsync();
        }


        private async Task LoadListAsync()
        {

            detailView.ResetTabs();
            var oldlistmodel = listView.Current;

            using (new VisualDelay(view)) {

                listView.Current = null;
                SetModel(null);

                var qry = await store.ListeAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                currentList = qry.Select(p => new BestellListViewItem(p)).ToList();
                await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist = new ObservableCollection<BestellListViewItem>(currentList); }), null);

                if (currentList.Count == 0) filterView.ResultText = "Filter: Keine Bestellung gefunden";
                else if (currentList.Count == 1) filterView.ResultText = "Filter: Eine Bestellung gefunden";
                else filterView.ResultText = String.Format("Filter: {0} Bestellungen gefunden", currentList.Count);
            }

            if (oldlistmodel != null && !currentList.IsNullOrEmpty()) {

                listView.Current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                await DetailAsync();

            } else {

                SetModel(null);
            }
        }


        private async Task DetailAsync()
        {

            if (listView.Current != null) {

                using (new VisualDelay(detailView)) {

                    var model = await store.DetailAsync(listView.Current.Id, bus.Token).ConfigureAwait(false);
                    //reference equality beachten
                    if (model != null && model.Lieferant != null) model.Lieferant = lieferanten.FirstOrDefault(p => p.Id == model.Lieferant.Id);
                    model.AcceptChanges();

                    SetModel(model);
                    if (detailView.Tab == BestellDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
                }

            } else {

                SetModel(null);
            }
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == "CurrentTab") {

                UpdateCommands();
                if (detailView.Tab == BestellDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
            }

            if (args.PropertyName == "Current") {

                UpdateCommands();
                if (detailView.Tab == BestellDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
            }

            if (sender ==  baseView && args.PropertyName == "Position") UpdateCommands();
        }


        private async Task UpdatePrintViewAsync()
        {

            reportView.ViewerVisible = false;
            if (detailView.Current == null) return;

            var address = "Comain.Client.Reports.Stock.Bestellung.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = ("Bestellung " + detailView.Current.Nummer).Trim() };
            report.HasLogo = true;
            var reportmodel = new BestellReportModel(detailView.Current, config);

            report.AddDataSource("MainData", new List<BestellReportModel> { reportmodel });
            report.AddDataSource("PosData", reportmodel.Positionen);

            await reportView.BindViewerAsync(report);
        }


        private void SetModel(Bestellung current)
        {

            detailView.Current = current;
            baseView.Current = current;
            memoView.Current = current;

            if (current != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null) listviewitem.Model = current;
            }

            UpdateCommands();
        }


        private void UpdateCommands()
        {

            commands.NeuSetzen();

            context.Post(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
                editCommand.RaiseCanExecuteChanged();
                addPosCommand.RaiseCanExecuteChanged();
                deletePosCommand.RaiseCanExecuteChanged();
                setOrderedCommand.RaiseCanExecuteChanged();
                setClosedCommand.RaiseCanExecuteChanged();
                setStornoCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private Task EditAsync()
        {

            if (detailView.Current != null && detailView.Current.IsEditable) commands.SetEditMode(EditMode.Edit);
            return Task.FromResult(true);
        }


        private async Task CancelAsync()
        {

            if (commands.CurrentMode == EditMode.Append) {

                SetModel(null);

            } else {

                await DetailAsync();
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task SaveAsync()
        {

            if (detailView.Current == null) return;

            var validation = detailView.Current.ErrorListWithLieferant(atlist);
            if (!validation.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Bestellung kann nicht gespeichert werden.", Reasons = validation });
                return;
            }

            using (new VisualDelay(detailView)) {

                var model = await store.UpdateAsync(detailView.Current, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                if (detailView.Current.IstAngefügt) await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist.Add(new BestellListViewItem(model)); }), null);
                SetModel(model);
            }
        }


        private async Task AddAsync()
        {

            addMode = true;
            var data = new BestellAddData { Artikelliste = atlist, Lieferanten = lieferanten, Callback = AddBestellung };
            await addController.RunAsync(data);
        }

        /// <summary>
        /// mit dem AddController erstellte Bestellung übernehmen
        /// </summary>
        /// <param name="model">neue Bestellung</param>
        protected void AddBestellung(Bestellung model)
        {

            crumbs.Remove();
            addMode = false;
            mainView.PageView = view.View;
            if (model == null) return;

            if (currentUser != null)
                model.UnsereZeichen = currentUser.Nachname + ", " + currentUser.Vorname + " [" + currentUser.Nummer + "]";

            SetModel(model);
            model.Bearbeiter = currentUser;


            commands.SetEditMode(EditMode.Append);
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current == null) return;

            if (!await messages.ConfirmDeleteAsync(new DeleteToken { Header = "Wollen Sie die Bestellung wirklich löschen?", IsDeleteable = true })) return;

            using (new VisualDelay(detailView)) {

                await store.DeleteAsync(detailView.Current.Id, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                await context.SendAsync(new SendOrPostCallback((o) => { if (listView.Current != null) listView.Foundlist.Remove(listView.Current); }), null);
                SetModel(null);
            }
        }


        private async Task AddPosAsync()
        {

            if (detailView.Current == null) return;

            posView.SetData(detailView.Current, atlist);
            await flyouts.OpenAsync(new FlyoutConfig { ChildView = posView.View, Header = "Bestellpositionen bearbeiten", Position = MahApps.Metro.Controls.Position.Right });
        }


        private async Task DeletePosAsync()
        {

            if (detailView.Current == null || baseView.Position == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => { baseView.Positionen.Remove(baseView.Position); }), null);
            UpdateCommands();
        }


        private async Task SetOrderedAsync()
        {

            if (detailView.Current == null) return;

            var validation = detailView.Current.ErrorListWithLieferant(atlist);
            if (!validation.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Bestellung kann nicht ausgelöst werden.", Reasons = validation });
                return;
            }

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll die Bestellung tatsächlich ausgelöst werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.OrderAsync(detailView.Current, bus.Token);
                bus.Token.ThrowIfCancellationRequested();

                //Liste bestellwürdiger Artikel anpassen
                atlist = await store.ArtikelAsync(bus.Token);

                commands.SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }


        private async Task SetClosedAsync()
        {

            if (detailView.Current == null) return;

            var reasons = new List<String>();
            if (detailView.Current.Status != Bestellstatus.Abschlussfähig) reasons.Add("Es wurden noch nicht alle Artikel geliefert bzw. eingelagert.");
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll die Bestellung tatsächlich abgeschlossen werden?", Reasons = reasons })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.CloseAsync(detailView.Current.Id, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }


        private async Task SetStornoAsync()
        {

            if (detailView.Current == null) return;

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll die Bestellung tatsächlich storniert werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.StornoAsync(detailView.Current.Id, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }
    }
}
