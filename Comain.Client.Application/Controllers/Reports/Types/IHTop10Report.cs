﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class IHTop10Report : ReportBase, IReport
    {

        private readonly Top10ViewModel topView;
        private readonly IReportService store;
        private IEnumerable<Top10Dto> currentData;


        public IHTop10Report(IReportService store, Top10ViewModel topView, ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, LeistungsartViewModel laView)
        {
            
            this.store = store;
            this.topView = topView;
            filterViews = new List<IFilterViewModel> { topView, zeitView, kstView, pzView, laView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = new ReportResult { Title = TypeTitle, ReportAddress = ReportAddress, AssemblyAddress = ReportAssembly };
            result.AddParameter("parTitle", TypeTitle);
            result.AddParameter("parType",  TypeParameter);
            result.AddParameter("parFilter", String.Join("; ", filterViews.Select(p => p.FilterAlsString()).Where(p => !String.IsNullOrWhiteSpace(p)).ToArray()));
           
            currentData = await store.Top10IHAsync(GebeFilter(), ct);
            var groupedData = currentData.GroupBy(p => p.HauptIHNummer).Select(p => new Top10Dto { 
            
                    HauptIHNummer   = p.Key,
                    HauptIHName     = p.First().HauptIHName,
                    Minuten         = p.Sum(q => q.Minuten),  
                    Personalkosten  = p.Sum(q => q.Personalkosten),
                    Materialkosten  = p.Sum(q => q.Materialkosten),
                    Fremdkosten     = p.Sum(q => q.Fremdkosten),
                    Anzahl          = p.Count() });

            if (TypeParameter == "Kosten") groupedData = groupedData.OrderByDescending(p => p.Gesamtkosten);
            if (TypeParameter == "Zeit")   groupedData = groupedData.OrderByDescending(p => p.Minuten);
            if (TypeParameter == "Anzahl") groupedData = groupedData.OrderByDescending(p => p.Anzahl);
           
            result.AddDataSource("MainData", groupedData);
            
            return result;
        }


        public override Task<ReportResult> CreateDetailAsync(String report, List<Tuple<String, String>> parameters, CancellationToken ct = default)
        {

            String iho = parameters.FirstOrDefault(p => p.Item1 == "parIHNummer").Item2;
            var result = new ReportResult { Title = DetailTypeTitle, ReportAddress = DetailReportAddress, AssemblyAddress = ReportAssembly };
            result.AddParameter("parTitle", DetailTypeTitle);
            result.AddParameter("parType",  TypeParameter);
            result.AddParameter("parFilter", String.Format("Asset: " + iho));
           
            var detailData = currentData.Where(p => p.HauptIHNummer == iho);
            if (TypeParameter == "Kosten") detailData = detailData.OrderByDescending(p => p.Gesamtkosten);
            if (TypeParameter == "Zeit")   detailData = detailData.OrderByDescending(p => p.Minuten);
            if (TypeParameter == "Anzahl") detailData = detailData.OrderByDescending(p => p.Anzahl);
           
            result.AddDataSource("MainData", detailData.ToList());
            return Task.FromResult<ReportResult>(result);
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Assets - Top 10"; }
        }


        private string TypeTitle
        {
            
            get { 
            
                if (topView.TypKosten) return "Assets - Top 10 Störkosten"; 
                if (topView.TypZeit) return "Assets - Top 10 Störzeit"; 
                return "Assets - Top 10 Störanzahl"; 
            }
        }


        private string DetailTypeTitle
        {
            
            get { 
            
                if (topView.TypKosten) return "Assets - Top 10 Störkosten Auftragsliste"; 
                if (topView.TypZeit) return "Assets - Top 10 Störzeit Auftragsliste"; 
                return "Assets - Top 10 Störanzahl Auftragsliste"; 
            }
        }


        private string TypeParameter
        {
            
            get { 
            
                if (topView.TypKosten) return "Kosten"; 
                if (topView.TypZeit) return "Zeit"; 
                return "Anzahl"; 
            }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 300; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.IHTop10Report.rdlc"; }
        }


        private string DetailReportAddress
        {
            get { return "Comain.Client.Reports.IHTop10AuReport.rdlc"; }
        }
    }
}
