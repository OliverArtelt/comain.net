﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.Filters
{

    [DebuggerDisplay("{Name}")]
    public class PersonalFilterModel : EntityModelBase
    {

        public int?       Gewerk_Id     { get; set; }
        public String     Klammer       { get; set; }
        public String     Nummer        { get; set; }
        public String     Nachname      { get; set; }
        public String     Vorname       { get; set; }

        public String     Name          => String.Format("{0}, {1}", Nachname, Vorname); 
        public String     NameNummer    => String.Format("[{2}] {0}, {1}", Nachname, Vorname, Nummer); 
    }
}
