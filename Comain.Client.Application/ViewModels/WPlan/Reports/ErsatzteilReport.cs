﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.Services.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class ErsatzteilReport : IPlanReport
    {

        private readonly IPlanService store;

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Ersatzteilbedarf";
        public int      Sortierung              => 450;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;


        public ErsatzteilReport(IPlanService store)
            => this.store = store;


        public Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo info, PlanOptions options, CancellationToken ct = default)
        {

            var report = new ReportResult { ReportAddress = "Comain.Client.Reports.ErsatzteilReport.rdlc",
                                            AssemblyAddress = "comain.Client.Reports",
                                            Title = "Ersatzteilbedarf" };
            var maindata = model.OffenePositionen.Where(p => !p.IstVergangenheit)
                                                 .SelectMany(p => p.Materialien)
                                                 .Where(p => p.Erforderlich)
                                                 .Select(p => new ErsatzteilbedarfRow(p))
                                                 .ToList();
            report.AddDataSource("MainData", maindata);
            return Task.FromResult(report);
        }


        public String   Text                => @"
Ersatzteilbedarf offener Checklistpositionen mit erforderlichem Einbau.
";
    }
}
