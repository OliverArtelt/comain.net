﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class RKListDto
    {

        public int          Id              { get; set; }
        public String       Lieferschein    { get; set; }
        public String       Bestellung      { get; set; }
        public DateTime     Lieferdatum     { get; set; }
        public DateTime?    ReklamiertAm    { get; set; }
    }
}
