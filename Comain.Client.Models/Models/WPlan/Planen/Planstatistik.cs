﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Zusammenfassende Informationen über Vollständigkeit und Genauigkeit des Planes
    /// </summary>
    public class Planstatistik
    {

        public DateTime         ErstelltAm                          { get; set; }
        public DateTime         Von                                 { get; set; }
        public DateTime         Bis                                 { get; set; }
        public String           Anlagen                             { get; set; }
        public String           LeistungsartAsString                { get; set; }
        public String           GewerkAsString                      { get; set; }
        public String           PersonalAsString                    { get; set; }
        public String           Zeitraum                            { get; set; }
        public Planungstyp      Planungstyp                         { get; set; }
        public Planungsstatus   Planungsstatus                      { get; set; }

        public String PlanungstypAsString     => Planungstyp.AsText();
        public String PlanungsstatusAsString  => Planungsstatus.AsText();


        public String GebeFilterText()
        {

            var bld = new StringBuilder();
            bld.Append(PlanungstypAsString).Append("; ").Append(PlanungsstatusAsString).Append("; ").Append(Zeitraum);

            var ihlist = Anlagen?.Split(';').ToList();
            if (ihlist.IsNullOrEmpty()) bld.Append("; alle Assets");
            else if (ihlist.Count > 5) bld.Append("; Assets: ").Append(String.Join(", ", ihlist.Take(5).ToArray())).Append(", ...");
            else if (ihlist.Count == 1) bld.Append("; Asset: ").Append(ihlist.First());
            else bld.Append("; Assets: ").Append(String.Join(", ", ihlist.ToArray()));

            if (!String.IsNullOrWhiteSpace(LeistungsartAsString)) bld.Append("; Leistungsart: ").Append(LeistungsartAsString);
            if (!String.IsNullOrWhiteSpace(GewerkAsString))       bld.Append("; Gewerk: ").Append(GewerkAsString);
            if (!String.IsNullOrWhiteSpace(PersonalAsString))     bld.Append("; Personal: ").Append(PersonalAsString);

            return bld.ToString();
        }


        /// <summary>
        /// Anzahl vorhandene Positionen im Zeitraum
        /// </summary>
        public int  AnzahlPositionen                            { get; set; }
        /// <summary>
        /// Wie viele von den vorhandenen Positionen im Zeitraum sind offen?
        /// </summary>
        public int  AnzahlOffenePositionen                      { get; set; }
        /// <summary>
        /// Wie viele von den vorhandenen Positionen im Zeitraum sind erledigt?
        /// </summary>
        public int  AnzahlErledigtePositionen                   { get; set; }
        /// <summary>
        /// Wie viele von den vorhandenen Positionen im Zeitraum sind überfällig?
        /// </summary>
        public int  AnzahlÜberfälligePositionen                 { get; set; }
        /// <summary>
        /// Wie viele von den vorhandenen Positionen im Zeitraum besitzen keinen Instandhalter?
        /// </summary>
        public int  AnzahlPositionenOhneAuftragnehmer           { get; set; }
        /// <summary>
        /// Wie viele Positionen im Zeitraum wären notwendig? (ungeplante Maßnahmen erkennen)
        /// </summary>
        public int  AnzahlNotwendigePositionen                  { get; set; }
        /// <summary>
        /// Positionen die im Zeitraum realisiert aber nicht fällig waren
        /// </summary>
        public int  AnzahlZusätzlicherPositionen                { get; set; }


        public decimal  KostenErledigt                          { get; set; }
        public decimal  KostenOffen                             { get; set; }
        public decimal  StundenErledigt                         { get; set; }
        public decimal  StundenOffen                            { get; set; }
    }
}
