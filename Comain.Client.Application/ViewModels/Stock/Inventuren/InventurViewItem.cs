﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Stock;


namespace Comain.Client.ViewModels.Stock.Inventuren
{

    public class InventurViewItem : NotifyBase, IDataErrorInfo
    {

        private readonly InventurDto dto;


        public InventurViewItem(InventurDto dto)
        {
            this.dto = dto;
        }


        public InventurDto  Model               { get { return dto; } }
        public int          ArtikelId           { get { return dto.ArtikelId; } }
        public int          LagerortId          { get { return dto.LagerortId; } }
        public String       Lagerort            { get { return dto.Lagerort; } }
        public String       Artikelnummer       { get { return dto.Artikelnummer; } }
        public String       Artikelname         { get { return dto.Artikelname; } }
        public String       Herstellernummer    { get { return dto.Herstellernummer; } }
        public String       Einheit             { get { return dto.Einheit; } }
        public decimal      Lagerbestand        { get { return dto.Lagerbestand; } }
        public String       BestandMitEinheit   { get { return String.Format("{0:N1} {1}", Lagerbestand, Einheit); } }
       

        public decimal? Inventurbestand 
        {
            get { return dto.Inventurbestand; }
            set { 
            
                if (dto.Inventurbestand != value) {

                    dto.Inventurbestand = value;
                    RaisePropertyChanged("Inventurbestand");
                }
            }
        }
       

        public String Bemerkungen 
        {
            get { return dto.Bemerkungen; }
            set { 
            
                if (dto.Bemerkungen != value) {

                    dto.Bemerkungen = value;
                    RaisePropertyChanged("Bemerkungen");
                }
            }
        }


        public string Error
        {
            get { return this["Inventurbestand"]; }
        }


        public string this[string columnName]
        {
            get { 
            
                if (columnName == "Inventurbestand" && Inventurbestand.GetValueOrDefault() < 0) 
                    return "Der Inventurbestand darf nicht negativ sein.";
                return null;
            }
        }
    }
}
