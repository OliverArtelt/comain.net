﻿using System;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Stock.Verwaltung.Types
{

    public class Lagerorte : IVwTyp
    {
        
        public string       Title           => "Lagerorte";
        public int          Sortierung      => 5000;
        public Type         Controller      => typeof(ILagerortController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator;    
        }
    }
}
