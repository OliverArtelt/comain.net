﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.IHObjekte
{
    public class IHObjektPersonal: Entity, IDataErrorInfo
    {

        public int    IHObjekt_Id     { get; set; }
        public int    Personal_Id     { get; set; }


        private Personalrolle rolle;
        public Personalrolle Rolle
        {
            get => rolle;
            set => SetProperty(ref rolle, value);
        }


        public string Error => null;
        public string this[string columnName] => null;
    }
}
