﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{

    public class Warenausgang : Entity, IDataErrorInfo
    {
       
        private String               entnahmeschein;  
        private String               notizen;  
        private decimal              menge;  
        private decimal?             preis;  
        private DateTime             datum;  
        private AuSelektorDto        auftrag;  
        private EinlagerungDto       wareneinlagerung;  
        private LagerortDto          lagerort;  
        private ArtikelDto           material;  
        private PersonalFilterModel  lagerist;  
        private PersonalFilterModel  nutzer;  
        private KstFilterModel       kostenstelle;  

        /// <summary>
        /// Ausgabescheinnummer, aus ID ermitteln
        /// </summary>
        public String Nummer            
        {
            get { return String.Format("AS{0:D8}", Id); }
        }


        public Warenausgang()
        {}


        public Warenausgang(EinlagerungDto lager, PersonalFilterModel lagerist)
        {
        
           // menge            = lager.Restmenge;
            preis            = lager.Preis;
            datum            = DateTime.Now.RoundToSeconds();
            wareneinlagerung = lager;
            lagerort         = lager.Lagerort;
            material         = lager.Material;
            this.lagerist    = lagerist;
        }


        public String Artikelnummer  { get { return material == null? null: material.Nummer; } }
        public String Lagerortnummer { get { return lagerort == null? null: lagerort.Nummer; } }
        public String Auftragsnummer { get { return auftrag == null? null: auftrag.Nummer; } }
        public String Kstnummer      { get { return kostenstelle == null? null: kostenstelle.Nummer; } }


#region P R O P E R T I E S


        public String Entnahmeschein    
        {
            get { return entnahmeschein; }
            set { SetProperty(ref entnahmeschein, value); }
        }
        

        public String Notizen           
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }
        

        public decimal Menge             
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }
        

        public decimal? Preis             
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }
        

        public DateTime Datum             
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }
        

       
        public AuSelektorDto Auftrag           
        {
            get { return auftrag; }
            set { if (SetProperty(ref auftrag, value)) RaisePropertyChanged("Auftragsnummer"); }
        }
        

        public EinlagerungDto Wareneinlagerung  
        {
            get { return wareneinlagerung; }
            set { SetProperty(ref wareneinlagerung, value); }
        }
        

        public LagerortDto Lagerort          
        {
            get { return lagerort; }
            set { if (SetProperty(ref lagerort, value)) RaisePropertyChanged("Lagerortnummer"); }
        }
        

        public ArtikelDto Material          
        {
            get { return material; }
            set { if (SetProperty(ref material, value)) RaisePropertyChanged("Artikelnummer"); }
        }
        

        public PersonalFilterModel Lagerist          
        {
            get { return lagerist; }
            set { SetProperty(ref lagerist, value); }
        }
        

        public PersonalFilterModel Nutzer            
        {
            get { return nutzer; }
            set { SetProperty(ref nutzer, value); }
        }
        

        public KstFilterModel Kostenstelle        
        {
            get { return kostenstelle; }
            set { if (SetProperty(ref kostenstelle, value)) RaisePropertyChanged("Kstnummer"); }
        }


#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Auftrag"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Material"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kostenstelle"];
                return test;
            }
        }
 
       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Auftrag"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Material"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kostenstelle"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Auftrag":

                    if (Auftrag == null) return "Der Warenausgang muss einem Auftrag zugeordnet sein."; 
                    break;

                case "Datum":

                    if (Datum.Year < 2000 || Datum.Year > 2100) return "Geben Sie ein plausibles Ausgangsdatum an."; 
                    break;

                case "Menge":

                    if (Menge <= 0) return "Die Menge des Warenausganges muss grösser 0 sein."; 
                    break;

                case "Kostenstelle":

                    if (Kostenstelle == null) return "Der Warenausgang muss einer Kostenstelle zugeordnet sein."; 
                    break;
                }

                return null;
            }
        }

#endregion    
        
    }
}
