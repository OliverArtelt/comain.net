﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PreisblattReport : IPlanReport
    {

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Preisblatt";
        public int      Sortierung              => 400;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;


        public Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo info, PlanOptions options, CancellationToken ct = default)
        {

            var report = new ReportResult { ReportAddress = "Comain.Client.Reports.PreisblattReport.rdlc",
                                            AssemblyAddress = "comain.Client.Reports",
                                            Title = "Preisblatt" };
            var maindata = model.AllePositionen
                                .Where(p => p.IstErledigt)
                                .Select(p => new PreisblattRow(p))
                                .ToList();

            report.AddDataSource("MainData", maindata);
            report.AddParameter("parFilter", model.FilterInfo);
            return Task.FromResult(report);
        }


        public String   Text                => @"
Verwenden Sie das Preisblatt, für eine Übersicht der für zyklische Maßnahmen kalkulierten Kosten. Im Preisblatt werden die in den Maßnahmen angegebenen Preise pro Maßnahme, Asset und Gewerk ausgewertet.
Wählen Sie dazu die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein, Gehen Sie auf Suchen und betätigen Sie danach die Schaltfläche ""Preisblatt"".
Bitte beachten Sie, es werden nur Maßnahmen ausgewertet, die im gewählten Zeitraum zurück gemeldet wurden. Geben Sie die auszuwertenden Preise bitte mit einer entsprechenden Menge in den Maßnahmedetails ein.";
    }
}
