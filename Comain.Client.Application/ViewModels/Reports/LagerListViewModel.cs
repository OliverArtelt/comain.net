﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewData.Reports;
using Comain.Client.Views.Reports;


namespace Comain.Client.ViewModels.Reports
{
    
    public class LagerListViewModel : ViewModel<ILagerListView>
    {

        public LagerListViewModel(ILagerListView view) : base(view)
        {
        }


        public void Unload()
        {
            Foundlist = null;
        }


#region P R O P E R T I E S
 

        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private ObservableCollection<ArtikelListItem> foundlist;
        public ObservableCollection<ArtikelListItem> Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private ArtikelListItem current;
        public ArtikelListItem Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
