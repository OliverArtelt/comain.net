﻿using System;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Services;
using Comain.Client.Services.Dokumente;
using Comain.Client.ViewModels.Dokumente;


namespace Comain.Client.Controllers.Specs
{

    public class SpecDokumentController : DokumentAssignController, ISpecDokumentController
    {


        public SpecDokumentController(IDokumentService store, IMessageService messages, IControllerBus bus,
                                      DokumentAssignViewModel view)
          : base(store, messages, bus, view)
        {}


        protected override String EntityType => "Spec";

        protected override bool   ShowDisplayAsset              => true;
        protected override bool   ShowDisplayAssetWithParents   => false;
        protected override bool   ShowDisplayAssetWithChildren  => false;
        protected override bool   ShowDisplayAuftrag            => false;
        protected override bool   ShowDisplayMassnahme          => true;

        protected override bool   ShowSearchAllDocs             => true;
        protected override bool   ShowSearchAssets              => true;
        protected override bool   ShowSearchMassnahmen          => true;
    }
}
