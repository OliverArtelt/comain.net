﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Services.Verwaltung
{
    public interface IObjektgruppenService
    {

        bool HasChanges { get; }
        void Reset();

        Task<Tuple<TrackableCollection<Objektgruppe>, TrackableCollection<Objektart>>> GetAsync(CancellationToken ct = default);
        Task SaveAsync(CancellationToken ct = default);
    }
}