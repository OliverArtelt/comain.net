﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Services.Verwaltung
{

    public interface IPersonalService : ISimpleService<PersonalDto, Personal>
    {
        Task<IList<NummerFilterModel>>      GewerkeAsync(CancellationToken ct = default);
        Task<IList<IHObjektFilterModel>>    MaschinenAsync(CancellationToken ct = default);
    }
}
