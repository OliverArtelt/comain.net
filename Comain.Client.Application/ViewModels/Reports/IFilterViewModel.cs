﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Services.Reports;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports
{

    public interface IFilterViewModel
    {

        object          View            { get; }

        String          FilterAlsString();
        String          Validiere();
        void            AddFilter(Suchkriterien filter);
        void            Reset();

        Task            LoadAsync(CancellationToken ct = default);
        void            Unload();
    }
}
