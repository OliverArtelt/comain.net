﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.IHObjekte
{
    public class IHObjektPersonalDto : ChangeTrackingBase
    {

        public int    IHObjekt_Id     { get; set; }
        public int    Personal_Id     { get; set; }
        public int    Rolle           { get; set; }
    }
}
