﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.WPlan
{

    public class PositionInfo
    {

        public int                  Id                      { get; set; }
        public String               Name                    { get; set; }
        public Checklistergebnis    Ergebnis                { get; set; }
        public int?                 Normzeit                { get; set; }
        public DateTime?            FälligAm                { get; set; }
        public DateTime?            DurchgeführtAm          { get; set; }

        public String ErgebnisAsText => Ergebnis.AsText();
    }
}
