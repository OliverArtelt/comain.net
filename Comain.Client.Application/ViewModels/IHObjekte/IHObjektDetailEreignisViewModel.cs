﻿using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektDetailEreignisViewModel : PrintViewModel<IIHObjektDetailEreignisView>
    {

        public IHObjektDetailEreignisViewModel(IMessageService messages, IIHObjektDetailEreignisView view) : base(messages, view)
        {}


#region P R O P E R T I E S
 

        private bool withSub;
        public bool WithSub
        {
            get { return withSub; }
            set { SetProperty(ref withSub, value); }
        }

#endregion

    }
}
