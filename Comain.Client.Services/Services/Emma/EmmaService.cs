﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Emma;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models.Emma;

namespace Comain.Client.Services.Emma
{

    public class EmmaService : IEmmaService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public EmmaService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<EmmaSession> CreateSessionAsync(CancellationToken ct)
        {

            var pstask = PersonalAsync(ct);
            var autask = AufträgeAsync(ct);
            var pztask = StandorteAsync(ct);
            var kstask = KostenstellenAsync(ct);
            var ihtask = IHObjekteAsync(ct);

            await Task.WhenAll(pstask, autask, pztask, kstask, ihtask).ConfigureAwait(false);

            return new EmmaSession(pslist:pstask.Result, aulist:autask.Result, pzlist:pztask.Result,
                                   kslist:kstask.Result, ihlist:ihtask.Result);
        }


        public Task<IList<Leistung>> SearchAsync(Suchkriterien search, CancellationToken ct = default)
            => store.FindAsync<IList<Leistung>>("/Emma/Search", search, ct);

        /// <summary>
        /// Datei importieren Woche oder Tag
        /// </summary>
        public Task<IList<Leistung>> ImportAsync(String filepath, CancellationToken ct = default)
            => store.PutAsync<String, IList<Leistung>>("/Emma/Import", File.ReadAllText(filepath, Encoding.GetEncoding("ISO-8859-1")), ct);

        /// <summary>
        /// manuelle Zuordnungen speichern
        /// </summary>
        public Task<IList<Leistung>> SaveAsync(IEnumerable<Leistung> models, CancellationToken ct = default)
            => SaveImplAsync("/Emma", models, ct);

        /// <summary>
        /// Automatisch zuordnen
        /// </summary>
        public Task<IList<Leistung>> AutoSaveAsync(IEnumerable<Leistung> models, CancellationToken ct = default)
            => SaveImplAsync("/Emma/Auto", models, ct);


        private async Task<IList<Leistung>> SaveImplAsync(String url, IEnumerable<Leistung> models, CancellationToken ct)
        {

            if (models.IsNullOrEmpty()) return null;
            var dtos = mapper.Map<IEnumerable<Leistung>, List<EmmaDto>>(models);
            var result = await store.PutAsync<List<EmmaDto>, IList<Leistung>>(url, dtos, ct);
            result.ForEach(p => p.AcceptChanges());
            return result;
        }

        /// <summary>
        /// Woche abschließen
        /// </summary>
        public async Task<IList<Leistung>> SaveAndCloseAsync(IEnumerable<Leistung> models, CancellationToken ct = default)
        {

            if (models.IsNullOrEmpty()) return null;
            var dtos = mapper.Map<IEnumerable<Leistung>, List<EmmaDto>>(models);
            var result = await store.PostAsync<List<EmmaDto>, IList<Leistung>>("/Emma/Close", dtos, ct);
            result.ForEach(p => p.AcceptChanges());
            return result;
        }


        public Task<EmmaCloseInfo> CloseInfoAsync(DateTime week, CancellationToken ct = default)
            => store.GetAsync<EmmaCloseInfo>($"/Emma/CloseInfo/{week.ToString("yyyy-MM-dd")}", ct);


        private Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);


        private Task<IList<AuftragListDto>> AufträgeAsync(CancellationToken ct = default)
        {

            var filter = new Suchkriterien();
            return store.FindAsync<IList<AuftragListDto>>("/Auftrag/Search", filter, ct);
        }


        private Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);


        private Task<IList<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct);


        private Task<IList<IHListDto>> IHObjekteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<IHListDto>>("/Filter/IH", ct);
    }
}
