﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Filters
{

    public class Suchkriterien
    {

        public List<String> Keys      { get; private set; }  = new List<String>();
        public List<String> Values    { get; private set; }  = new List<String>();


        public void Add(String key, object val)
        {

            if (val == null) return;
            Keys.Add(key);
            Values.Add(val.ToString());
        }
    }
}
