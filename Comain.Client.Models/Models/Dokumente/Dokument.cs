﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Comain.Client.Models.Dokumente
{

    [DebuggerDisplay("{Bezeichnung} [{Url}]")]
    public class Dokument : Entity, IDataErrorInfo
    {

        private static String[] picFileTypes = new String[] { "png", "jpg", "jpeg", "gif" };


        private String                  bezeichnung;
        private String                  beschreibung;
        private String                  filename;
        private long?                   filesize;
        private String                  typ;
        private String                  url;
        private String                  previewUrl;
        private bool                    präsentiertModell;

        private int                     version;
        private DateTime                erstelltAm;
        private String                  erstelltVon;
        private DateTime                geändertAm;
        private String                  geändertVon;
        private DateTime?               geöffnetAm;

        private byte[]                  preview;
        private byte[]                  content;

        private ICollection<String>     tags;
        private ICollection<int>        kategorien;


        public bool AlsBildDarstellbar => typ != null && picFileTypes.Contains(typ.ToLower());


        public Dokument()
        {}


        public Dokument(String path, int id)
        {

            if (!File.Exists(path)) throw new ComainOperationException("Dokument wurde nicht gefunden.");

            Id = id;
            var buffer = File.ReadAllBytes(path);
            Content = buffer;
            Bezeichnung = Path.GetFileNameWithoutExtension(path);
            Filename = Path.GetFileName(path);
            Typ = Path.GetExtension(path).Trim('.');
            Filesize = buffer.Length;
            Version = 1;
            ErstelltAm = DateTime.Now;
            GeändertAm = ErstelltAm;
            Url = Guid.NewGuid().ToString();
        }


        public void ChangeDocumentFile(String path)
        {

            if (!File.Exists(path)) throw new ComainOperationException("Dokument wurde nicht gefunden.");

            var buffer = File.ReadAllBytes(path);
            Content = buffer;
            if (Bezeichnung == Path.GetFileNameWithoutExtension(Filename)) Bezeichnung = Path.GetFileNameWithoutExtension(path);
            Filename = Path.GetFileName(path);
            Typ = Path.GetExtension(path).Trim('.');
            Filesize = buffer.Length;
            Version++;
            GeändertAm = DateTime.Now;
        }


        public void SetDocumentFile(byte[] buffer)
        {

            Content = buffer;
            AcceptChange(nameof(Content));
        }


        public String TempPath
        {
            get {

                if (String.IsNullOrEmpty(Filename)) return Path.Combine(Path.GetTempPath(), "Standard.dat");
                return Path.Combine(Path.GetTempPath(), Filename);
            }
        }


        public async Task StoreFileAsync(String path)
        {

            if (Content.IsNullOrEmpty()) return;
            await File.WriteAllBytesAsync(path, Content).ConfigureAwait(false);
        }


        public Task StoreTempFileAsync() => StoreFileAsync(TempPath);


#region P R O P E R T I E S


        // keine Persistenz, nur Angabe in welcher Beziehung Entity zum Dokument steht
        public DokumentQuelltyp Quelltyp            { get; set; }
        public String           Quelle              { get; set; }
        public bool             IstDirektBezug      { get; set; }


        public bool PräsentiertModell
        {
            get { return präsentiertModell; }
            set { SetProperty(ref präsentiertModell, value); }
        }


        public String Bezeichnung
        {
            get { return bezeichnung; }
            set { SetProperty(ref bezeichnung, value); }
        }

        public String Beschreibung
        {
            get { return beschreibung; }
            set { SetProperty(ref beschreibung, value); }
        }

        public String Filename
        {
            get { return filename; }
            set { SetProperty(ref filename, value); }
        }

        public long? Filesize
        {
            get { return filesize; }
            set { SetProperty(ref filesize, value); }
        }

        public String Typ
        {
            get { return typ; }
            set { SetProperty(ref typ, value); }
        }

        public String Url
        {
            get { return url; }
            set { SetProperty(ref url, value); }
        }

        public String PreviewUrl
        {
            get { return previewUrl; }
            set { SetProperty(ref previewUrl, value); }
        }

        public String ErstelltVon
        {
            get { return erstelltVon; }
            set { SetProperty(ref erstelltVon, value); }
        }

        public String GeändertVon
        {
            get { return geändertVon; }
            set { SetProperty(ref geändertVon, value); }
        }

        public int Version
        {
            get { return version; }
            set { SetProperty(ref version, value); }
        }

        public DateTime ErstelltAm
        {
            get { return erstelltAm; }
            set { SetProperty(ref erstelltAm, value); }
        }

        public DateTime GeändertAm
        {
            get { return geändertAm; }
            set { SetProperty(ref geändertAm, value); }
        }

        public DateTime? GeöffnetAm
        {
            get { return geöffnetAm; }
            set { SetProperty(ref geöffnetAm, value); }
        }

        public byte[] Preview
        {
            get { return preview; }
            set { SetProperty(ref preview, value); }
        }

        public byte[] Content
        {
            get { return content; }
            set { SetProperty(ref content, value); }
        }

        public ICollection<String> Tags
        {
            get { return tags ?? (tags = new List<String>()); }
            set { SetProperty(ref tags, value); }
        }

        public ICollection<int> Kategorien
        {
            get { return kategorien ?? (kategorien = new HashSet<int>()); }
            set { SetProperty(ref kategorien, value); }
        }

#endregion


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Bezeichnung"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Bezeichnung":

                    if (String.IsNullOrWhiteSpace(Bezeichnung)) return "Geben Sie die Bezeichnung an.";
                    if (Bezeichnung.Length > 250) return "Die Bezeichnung darf aus maximal 250 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
