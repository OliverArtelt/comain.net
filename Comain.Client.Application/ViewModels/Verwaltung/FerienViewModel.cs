﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class FerienViewModel : SimpleViewModel<Ferien, IFerienView>
    {

        public FerienViewModel(IFerienView view) : base(view)
        {}


        private ICommand weekCommand;
        public ICommand WeekCommand
        {
            get { return weekCommand; }
            set { SetProperty(ref weekCommand, value); }
        }
    }
}
