﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.ViewData.WPlan;
using Comain.Client.Views.WPlan.IHObjekte;

namespace Comain.Client.ViewModels.WPlan.IHObjekte
{

    public class MassnahmeViewModel : ViewModel<IMassnahmeView>
    {

        private readonly SynchronizationContext context;


        public MassnahmeViewModel(IMassnahmeView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public async Task SetDetailAsync(IList<MassnahmeViewItem> mnlist)
        {

            Massnahmen = null;
            if (!mnlist.IsNullOrEmpty()) {

                await context.SendAsync(new SendOrPostCallback((o) => {

                    Massnahmen = new TrackableCollection<MassnahmeViewItem>(mnlist);
                }), null);
            }
        }


        public void Unload()
        {
            Massnahmen = null;
        }


#region P R O P E R T I E S


        private TrackableCollection<MassnahmeViewItem> massnahmen;
        public TrackableCollection<MassnahmeViewItem> Massnahmen
        {
            get { return massnahmen; }
            set { SetProperty(ref massnahmen, value); }
        }


        private MassnahmeViewItem currentMassnahme;
        public MassnahmeViewItem CurrentMassnahme
        {
            get { return currentMassnahme; }
            set { SetProperty(ref currentMassnahme, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


#endregion

    }
}
