﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewData.Verwaltung
{

    public class KstSelektorItem : NotifyBase
    {

        private readonly Standort pz;
        private readonly Kostenstelle kst;


        public KstSelektorItem(Standort pz, Kostenstelle kst)
        {
        
            Contract.Assert(pz != null);
            Contract.Assert(kst != null);

            this.pz = pz;
            this.kst = kst;
        } 
        
        
        public String Nummer { get { return kst.Nummer; } }   
        
        public String Name   { get { return kst.Name; } }   
        
        
        public bool IstZugeordnet
        { 
            get { return pz.ZugeordneteKostenstellen.Contains(kst); } 
            set { 
                
                if (value && !pz.ZugeordneteKostenstellen.Contains(kst)) pz.ZugeordneteKostenstellen.Add(kst);
                else if (!value && pz.ZugeordneteKostenstellen.Contains(kst)) pz.ZugeordneteKostenstellen.Remove(kst);
                else return;

                RaisePropertyChanged("IstZugeordnet");
            } 
        }   
    }
}
