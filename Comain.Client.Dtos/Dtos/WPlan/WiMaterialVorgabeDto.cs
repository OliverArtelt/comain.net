﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.WPlan
{

    public class WiMaterialVorgabeDto : ChangeTrackingBase
    {

        public MaterialstammFilterModel Material        { get; set; }
        public String                   Name            { get; set; }
        public String                   Nummer          { get; set; }
        public NummerFilterModel        Einheit         { get; set; }
        public decimal?                 Menge           { get; set; }
        public decimal?                 EPreis          { get; set; }
        public bool                     Erforderlich    { get; set; }
        public String                   Anweisung       { get; set; }
    }
}
