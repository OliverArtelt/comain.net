﻿using System;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Stock;

namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class IHPickItem : NotifyBase
    {

        private readonly Artikel at;
        private readonly IHObjektFilterModel ih;
        private ArtikelIHObjekt ai;


        public IHPickItem(Artikel at, IHObjektFilterModel ih)
        {
        
            this.at = at;
            this.ih = ih;
            if (at != null && !at.MaterialIHObjektMap.IsNullOrEmpty())
                ai = at.MaterialIHObjektMap.FirstOrDefault((Func<ArtikelIHObjekt, bool>)(p => (bool)(p.IHObjekt != null && p.IHObjekt.Id == ih.Id)));
        } 


        public String Nummer            { get { return ih == null? null: ih.Nummer; } }

        public String Name              { get { return ih == null? null: ih.Name; } }

        public String Inventarnummer    { get { return ih == null? null: ih.Inventarnummer; } }


        public bool IsSelected
        {
            get { return ai != null; }
            set {

                if (value) {

                    ai = new ArtikelIHObjekt { IHObjekt = ih };
                    if (at.MaterialIHObjektMap != null) at.MaterialIHObjektMap.Add(ai);

                } else {

                    if (at.MaterialIHObjektMap != null) at.MaterialIHObjektMap.Remove(ai);
                    ai = null;     
                }

                RaisePropertyChanged("IsSelected");
            }
        }
    }
}
