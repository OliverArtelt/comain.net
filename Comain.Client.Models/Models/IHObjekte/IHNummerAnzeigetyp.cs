﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.IHObjekte
{

    public enum IHNummerAnzeigetyp
    {

        Nummer = 0,
        Inventarnummer = 1,
        Keine = 2
    }
}
