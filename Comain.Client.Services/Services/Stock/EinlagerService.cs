﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Wareneinlagerungen
{

    public class EinlagerService : IEinlagerService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public EinlagerService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public Task<IList<EingangFilterDto>> EingängeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<EingangFilterDto>>("/WL/WEFilter", ct);
        }


        public async Task<IList<Wareneingang>> LagerfähigeEingängeAsync(CancellationToken ct = default)
        {

            var dtos = await store.GetAsync<IList<EingangDto>>("/WL/WELager", ct);
            var models = mapper.Map<List<Wareneingang>>(dtos);
            return models;
        }


        public Task<Lagerort> LetzerLagerortAsync(int artikelId, CancellationToken ct = default)
        {
            return store.GetAsync<Lagerort>(String.Format("WL/LastLO/{0}", artikelId), ct);
        }


        public Task<Inventur> LetzeInventurAsync(int wlId, CancellationToken ct = default)
        {
            return store.GetAsync<Inventur>(String.Format("WL/LastInventur/{0}", wlId), ct);
        }


        public async Task<IList<Lagerort>> LagerorteAsync(CancellationToken ct = default)
        {

            var dtos = await store.GetAsync<IList<LagerortDto>>("/VW/LO", ct).ConfigureAwait(false);
            var models = mapper.Map<List<Lagerort>>(dtos);
            return models;
        }


        public async Task<IList<Artikel>> ArtikelAsync(CancellationToken ct = default)
        {

            var models = await store.GetAsync<IList<Artikel>>("/WL/Artikel", ct).ConfigureAwait(false);
            return models;
        }


        public async Task<TrackableCollection<Wareneinlagerung>> SearchAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var models = await store.FindAsync<IList<Wareneinlagerung>>("/WL/Search", filter, ct).ConfigureAwait(false);
            return new TrackableCollection<Wareneinlagerung>(models);
        }


        public Task<IList<WLReportDto>> ReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<WLReportDto>>("/WL/Report", filter, ct);
        }


        public Task SaveAsync(TrackableCollection<Wareneinlagerung> list, CancellationToken ct = default)
        {
            var dtos = mapper.AsDtoList<Wareneinlagerung, EinlagerungDto>(list);
            return store.PostAsync<IList<EinlagerungDto>>("/WL", dtos, ct);
        }


        public Task<IEnumerable<BestellListDto>> AbschlussfähigeBestellungenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IEnumerable<BestellListDto>>("/Bestell/Closeable", ct);
        }


        public Task BestellungenAbschliessenAsync(List<int> idlist,CancellationToken ct = default)
        {
            return store.PostAsync<List<int>>("/Bestell/Closeable", idlist, ct);
        }
    }
}
