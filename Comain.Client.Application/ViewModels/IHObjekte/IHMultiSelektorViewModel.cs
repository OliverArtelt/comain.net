﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;
using Comain.Client.Dtos.Filters;
using Comain.Client.Controllers;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHMultiSelektorViewModel : ViewModel<IIHMultiSelektorView>, IDelayableViewModel
    {

        private readonly IKonfigService configs;
        private readonly SynchronizationContext context;
        private const int maxFilterCount = 100;

        /// <summary>
        /// IH-Objekte im PZ-Baum
        /// </summary>
        private Dictionary<int, TreeIHViewItem> pzTreeItems;
        /// <summary>
        /// IH-Objekte im KST-Baum
        /// </summary>
        private Dictionary<int, TreeIHViewItem> kstTreeItems;
        /// <summary>
        /// alle IH-Objekte
        /// </summary>
        private IReadOnlyList<IHFilterItem> currentIHList;
        private Konfiguration cfg;
        private int filterCount;


        public IHMultiSelektorViewModel(IKonfigService configs, IIHMultiSelektorView view) : base(view)
        {

            context = SynchronizationContext.Current;
            this.configs = configs;
            ClearCommand = new DelegateCommand(Reset);
            SearchCommand = new Comain.Client.Controllers.AsyncDelegateCommand(_ => SearchAsync());
        }


        public async Task LoadAsync(List<IHFilterItem> ihlist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                    CancellationToken ct = default)
        {

            ct.ThrowIfCancellationRequested();

            using (new VisualDelay(this)) {

                cfg = await configs.GetAsync(ct);
                currentIHList = ihlist.ToReadOnlyList();
                var kstreetask = GenerateKstTreeAsync(kstlist, ct);
                var pztreetask = GeneratePzTreeAsync(pzlist, ct);
                await Task.WhenAll(kstreetask, pztreetask).ConfigureAwait(false);
            }
        }


        private async Task GeneratePzTreeAsync(IEnumerable<PzFilterModel> pzs, CancellationToken ct)
        {

            await Task.Run(() => {

                Standorte = pzs.OrderBy(p => p.Nummer).Select(p => new TreePZViewItem(p)).ToList();

                var list = currentIHList.OrderBy(p => p.Nummer).Select(p => new TreeIHViewItem(p, cfg)).ToList();
                pzTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihdic = list.ToDictionary(p => p.IHNummer);

                list.ForEach(p => {

                    if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                        ihdic[p.NummerParent].Add(p);
                        p.Parent = ihdic[p.NummerParent];
                    }
                });

                ct.ThrowIfCancellationRequested();

                var pzdic = Standorte.ToDictionary(p => p.Model.Id);
                list.Where(p => p.IstMaschine && pzdic.ContainsKey(p.FilterModel.Standort_Id))
                    .ForEach(p => {

                        pzdic[p.FilterModel.Standort_Id].Add(p);
                        p.Parent = pzdic[p.FilterModel.Standort_Id];
                    });

                RaisePropertyChanged("Standorte");
            });
        }


        private Task GenerateKstTreeAsync(IEnumerable<KstFilterModel> ksts, CancellationToken ct)
        {

            return Task.Run(() => {

                Kostenstellen = ksts.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).Select(p => new TreeKSTViewItem(p)).ToList();

                var list = currentIHList.OrderBy(p => p.Nummer).Select(p => new TreeIHViewItem(p, cfg)).ToList();
                kstTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihkstdict = list.ToMultiDictionary(p => p.FilterModel.Kostenstelle_Id);

                Kostenstellen.ForEach(kst => {

                    ct.ThrowIfCancellationRequested();
                    FillKst(kst, ihkstdict.GetValueOrDefault(kst.Model.Id));
                });

                RaisePropertyChanged("Kostenstellen");
            });
        }


        private void FillKst(TreeKSTViewItem kst, HashSet<TreeIHViewItem> ihlist)
        {

            if (ihlist == null) return;

            var ihdic = ihlist.ToDictionary(p => p.IHNummer);
            ihlist.ToList().ForEach(p => {

                if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                    ihdic[p.NummerParent].Add(p);
                    p.Parent = ihdic[p.NummerParent];
                    ihlist.Remove(p);
                }
            });

            ihlist.OrderBy(p => p.IHNummer)
                  .ForEach(p => {

                        kst.Add(p);
                        p.Parent = kst;
                    });
        }


        public void Reset()
        {

            pzTreeItems.Values.ForEach(p => p.IsSelected = false);
            kstTreeItems.Values.ForEach(p => p.IsSelected = false);
            Foundlist = null;
            SearchText = null;
        }


        public void Unload()
        {

            Standorte = null;
            Kostenstellen = null;
            Foundlist = null;
            pzTreeItems = null;
            kstTreeItems = null;

            RaisePropertyChanged("Standorte");
            RaisePropertyChanged("Kostenstellen");
        }


        private Dictionary<int, TreeIHViewItem> CurrentDictionary
        {
            get {

                if (currentTab == 0) return pzTreeItems;
                if (currentTab == 1) return kstTreeItems;
                return null;
            }
        }


        private async Task SearchAsync()
        {

            List<IHFilterItem> list = null;
            if (!String.IsNullOrWhiteSpace(searchText)) {

                var terms = searchText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                list = currentIHList.Where(p => p.Matches(terms)).ToList();
                filterCount = list.Count;
            }

            await context.SendAsync(new SendOrPostCallback((o) => { Foundlist = list.Take(maxFilterCount).ToList(); }), null);
        }


#region P R O P E R T I E S


        public ICommand OpenCommand     { get; private set; }

        public ICommand ClearCommand    { get; private set; }

        public ICommand SearchCommand   { get; private set; }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        public String ResultText
        {
            get {

                if (String.IsNullOrEmpty(searchText)) return "Geben Sie Suchbegriffe ein und bestätigen Sie mit Enter.";
                if (foundlist.IsNullOrEmpty()) return "Kein Asset gefunden.";
                if (filterCount == 1) return "Ein Asset gefunden.";
                if (filterCount <= maxFilterCount) return String.Format("{0} Assets gefunden.", filterCount);
                return String.Format("{0} Assets gefunden, die ersten {1} werden angezeigt.", filterCount, maxFilterCount);
            }
        }

        /// <summary>
        /// Ergebnis der Freisuche
        /// </summary>
        private List<IHFilterItem> foundlist;
        public List<IHFilterItem> Foundlist
        {
            get { return foundlist; }
            set {

                if (SetProperty(ref foundlist, value)) {

                    RaisePropertyChanged("ResultText");
                }
            }
        }


        public List<IHFilterItem> CurrentIHObjekte
        {
            get {

                return null;
            }
        }


        private bool clearButtonVisible = true;
        public bool ClearButtonVisible
        {
            get { return clearButtonVisible; }
            set { SetProperty(ref clearButtonVisible, value); }
        }


        private bool editEnabled = true;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public  List<TreePZViewItem> Standorte { get; private set; }


        public  List<TreeKSTViewItem> Kostenstellen { get; private set; }


#endregion


    }
}
