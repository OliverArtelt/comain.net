﻿using System;
using System.Drawing;


namespace CO.OpenXML
{
    
    public static class ColorExtended
    {
    
        public static String ToRgbHexString(this Color color)
        {
        
            return String.Format("FF{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
        }
    }
}
