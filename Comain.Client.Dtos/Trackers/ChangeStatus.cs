﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Trackers
{

    public enum ChangeStatus
    {
        Unchanged,
        Added,
        Changed,
        Deleted
    }
}
