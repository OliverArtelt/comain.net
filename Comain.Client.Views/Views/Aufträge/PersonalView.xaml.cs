﻿using System.Windows.Controls;


namespace Comain.Client.Views.Aufträge
{

    public partial class PersonalView:UserControl  ,IPersonalView
    {


        public PersonalView()
        {
            InitializeComponent();
        }


        public void SetStartFocus()
        {
            PersonalSelektor.Focus();            
        }
    }
}
