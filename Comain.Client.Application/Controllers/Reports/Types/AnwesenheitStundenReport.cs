﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels.Anwesenheiten;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class AnwesenheitStundenReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly WocheViewModel wocheView;
        private readonly StandortViewModel pzView;


        public AnwesenheitStundenReport(IReportService store, WocheViewModel wocheView, StandortViewModel pzView)
        {

            this.store = store;
            this.wocheView = wocheView;
            this.pzView = pzView;
            filterViews = new List<IFilterViewModel> { wocheView, pzView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            var dtos = await store.AnwesenheitenAsync(GebeFilter(), ct);
            result.AddDataSource("MainData", dtos.Select(p => new AnwesenheitReportModel(p)).ToList());
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;


        public override string Title => "Anwesenheiten - Stundenzettel";
        public string Gruppe => "Personal";
        public int Sortierung => 800;
        public override string ReportAddress => "Comain.Client.Reports.AnwesenheitStundenReport.rdlc";
    }
}
