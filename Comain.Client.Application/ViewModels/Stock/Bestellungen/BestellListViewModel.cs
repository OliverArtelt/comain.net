﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Bestellungen;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellListViewModel : ViewModel<IBestellListView>
    {
 

        public BestellListViewModel(IBestellListView view) : base(view)
        {
        }


        public void Unload()
        {
            Foundlist = null;
        }


#region P R O P E R T I E S
 
        
        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private ObservableCollection<BestellListViewItem> foundlist;
        public ObservableCollection<BestellListViewItem> Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private BestellListViewItem current;
        public BestellListViewItem Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
