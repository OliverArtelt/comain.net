﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.IHObjekte
{

    public class IHDetailDto : ChangeTrackingBase
    {

        public String                           Nummer                          { get; set; }
        public String                           Name                            { get; set; }
        public String                           Inventarnummer                  { get; set; }
        public String                           Bauteil                         { get; set; }
        public String                           Baugruppe                       { get; set; }
        public String                           Fabriknummer                    { get; set; }
        public String                           Typ                             { get; set; }
        public String                           Anlage                          { get; set; }
        public String                           Scannummer                      { get; set; }
        public short?                           Baujahr                         { get; set; }
        public short?                           Anschaffungsjahr                { get; set; }
        public decimal?                         Anschaffungswert                { get; set; }
        public decimal?                         Wiederbeschaffungswert          { get; set; }
        public int?                             Erhaltklasse                    { get; set; }
        public int?                             Status                          { get; set; }
        public String                           TechnischeDaten                 { get; set; }
        public String                           Bemerkungen                     { get; set; }
        public bool?                            IstAktiv                        { get; set; }
        public DateTime?                        DeaktiviertAm                   { get; set; }
        public String                           WNummer                         { get; set; }
        public double?                          GpsLongitude                    { get; set; }
        public double?                          GpsLatitude                     { get; set; }


        public KstFilterModel                   Kostenstelle                    { get; set; }
        public PzFilterModel                    Standort                        { get; set; }
        public NummerFilterModel                Objektgruppe                    { get; set; }
        public SubNummerFilterModel             Objektart                       { get; set; }
        public LieferantFilterModel             Instandhalter                   { get; set; }
        public LieferantFilterModel             Lieferant                       { get; set; }
        public LieferantFilterModel             Hersteller                      { get; set; }
        public PersonalFilterModel              VerantwortlicherInstandhaltung  { get; set; }
        public PersonalFilterModel              VerantwortlicherProduktion      { get; set; }
        public SpecFilterModel                  Spezifikation                   { get; set; }


        public IEnumerable<IHPzDto>             IHObjektPzMap                   { get; set; }
        public IEnumerable<IHKstDto>            IHObjektKstMap                  { get; set; }
        public IEnumerable<TechWertDto>         TechnischeWerte                 { get; set; }
        public IEnumerable<AssetArtikelDto>     MaterialIHObjektMap             { get; set; }
        public IEnumerable<IHObjektPersonalDto> Personalliste                   { get; set; }
    }
}
