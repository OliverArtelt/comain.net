﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using CO.OpenXML.XLSX.ExcelStyles;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Excel-Stilverzeichnis als verschachtelte Fliegengewichte
    /// </summary>
    public class StylePool
    {    

        /// <summary>
        /// Unterverzeichnis Schriftattribute
        /// </summary>
        StyleFlyweight<FontStyle> myFonts;
        /// <summary>
        /// Unterverzeichnis Rahmen
        /// </summary>
        StyleFlyweight<Border> myBorders;
        /// <summary>
        /// Unterverzeichnis Füllfarben
        /// </summary>
        StyleFlyweight<BackColor> myBackcolors;
        /// <summary>
        /// Unterverzeichnis Ausrichtungen
        /// </summary>
        StyleFlyweight<Alignment> myAlignments;
        /// <summary>
        /// Unterverzeichnis Stilkombinationen
        /// </summary>
        StyleFlyweight<CellXf> myCombos; 
        /// <summary>
        /// Stilverzeichnis als XML-Dokument
        /// </summary>
        XDocument myDocument;
        
        
        public StylePool(String xmldoc) 
        {

            using (TextReader rdr = new StringReader(xmldoc)) {
            
                myDocument = XDocument.Load(rdr);
                XNamespace ns = myDocument.Root.Name.Namespace;
                int count;
                
                count = (from c in myDocument.Descendants(ns + "fonts") select Int32.Parse(c.Attribute("count").Value)).SingleOrDefault();
                myFonts = new StyleFlyweight<FontStyle>(count);

                count = (from c in myDocument.Descendants(ns + "borders") select Int32.Parse(c.Attribute("count").Value)).SingleOrDefault();
                myBorders = new StyleFlyweight<Border>(count);

                count = (from c in myDocument.Descendants(ns + "fills") select Int32.Parse(c.Attribute("count").Value)).SingleOrDefault();
                myBackcolors = new StyleFlyweight<BackColor>(count);

                myAlignments = new StyleFlyweight<Alignment>(1);

                count = (from c in myDocument.Descendants(ns + "cellXfs") select Int32.Parse(c.Attribute("count").Value)).SingleOrDefault();
                myCombos = new StyleFlyweight<CellXf>(count);
            }
        }

        /// <summary>
        /// Fliegengewichtsfabrik: Stil eintragen und dessen Stilindex übergeben
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public int this[Style style]
        {
        
            get {
            
                int fontidx   = myFonts[style.Font];
                int formatidx = (style.Format != null)? (int)style.Format.format: 0;       
                int borderidx = myBorders[style.Border];  
                int coloridx  = myBackcolors[style.Color];
                int alignidx  = myAlignments[style.Align];    

                //Stilkombination bilden
                CellXf xfs = new CellXf(fontidx, formatidx, borderidx, coloridx, alignidx);
                return myCombos[xfs];
            }
        }
        
        /// <summary>
        /// Stildefinitionen schreiben
        /// </summary>
        /// <returns></returns>
        public String WriteXML()
        {

            //Schreiben der Teillisten an Unterverzeichnisse delegieren
            XNamespace ns = myDocument.Root.Name.Namespace;
            
            myFonts.WriteXML(myDocument.Descendants(ns + "fonts").First());
            myBackcolors.WriteXML(myDocument.Descendants(ns + "fills").First());
            myBorders.WriteXML(myDocument.Descendants(ns + "borders").First());
            myCombos.WriteXML(myDocument.Descendants(ns + "cellXfs").First());

            UpdateAlignment();

            //Anzahl vorhandener Stilknoten anpassen
            XElement p;
            p = myDocument.Descendants(ns + "fonts").First();
            p.Attribute("count").Value = p.Descendants(ns + "font").Count().ToString();

            p = myDocument.Descendants(ns + "fills").First();
            p.Attribute("count").Value = p.Descendants(ns + "fill").Count().ToString();

            p = myDocument.Descendants(ns + "borders").First();
            p.Attribute("count").Value = p.Descendants(ns + "border").Count().ToString();

            p = myDocument.Descendants(ns + "cellXfs").First();
            p.Attribute("count").Value = p.Descendants(ns + "cellXf").Count().ToString();
        
    
            String content = String.Format("{0}{1}{2}", myDocument.Declaration, Environment.NewLine, 
                                           myDocument.ToString(SaveOptions.DisableFormatting));
            return content;
        }
        
        /// <summary>
        /// neu eingefügten Stilkombinationsknoten das Alignment verpassen
        /// </summary>
        private void UpdateAlignment()
        {
        
            XNamespace ns = myDocument.Root.Name.Namespace;
        
            //Problem: Alignments besitzen keinen eingenen Elternknoten sondern werden individuell den Stilkombinationsknoten untergeordnet
            XElement cellXfs = myDocument.Descendants(ns + "cellXfs").First();
            
            //Anzahl Kombinationen vor dem Schreiben des Dokumentes; Originalanzahl befindet sich noch in <cellXfs count="?">
            int oldCount = Int32.Parse(cellXfs.Attribute("count").Value);
            List<XElement> newXf = cellXfs.Descendants(ns + "xf").Skip(oldCount).ToList();
            int cndx = -1;
            
            foreach (var entry in newXf) {

                cndx++;

                //Alignment-Index des Stilkombinationsknoten ermitteln
                int val = myCombos[cndx].AlignIndex;
                if (val == 0) continue;  //kein Alignment definiert

                //Alignment dieses Stilkombinationsknotens dem Knoten im XML-Dokument als Unterknoten nachtragen
                myAlignments[val - 1].AddAlignmentToXFNode(entry);
            }
        }
    }
}
