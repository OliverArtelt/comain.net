﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;
using System.Windows.Input;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecDetailReportViewModel : ValidatableViewModel<ISpecDetailReportView, Spezifikation>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecDetailReportViewModel(ISpecDetailReportView view)
          : base(view)
        {
        }


        public void Unload()
        {
            Entities = null;
        }


#region P R O P E R T I E S


        private IList<AssignedEntityItem> entities;
        public IList<AssignedEntityItem> Entities
        {
            get => entities;
            set => SetProperty(ref entities, value);
        }


        private ICommand openEntityCommand;
        public ICommand OpenEntityCommand
        {
            get => openEntityCommand;
            set => SetProperty(ref openEntityCommand, value);
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }


#endregion

    }
}
