﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IEinheitController : IVwTypController 
    {}
    
    
    public class EinheitController : SimpleController<EinheitDto, Einheit, IEinheitView>, IEinheitController
    {
        
        public EinheitController(IMessageService messages, IControllerBus bus,
                                 ISimpleService<EinheitDto, Einheit> store, VerwaltungViewModel mainView, SimpleViewModel<Einheit, IEinheitView> view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/EH"; }
        }
    }
}
