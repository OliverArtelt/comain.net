﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Controllers;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Services.WPlan;
using Comain.Client.ViewData.WPlan;
using Comain.Client.ViewModels.WPlan.IHObjekte;
using AuftragViewModel = Comain.Client.ViewModels.WPlan.IHObjekte.AuftragViewModel;
using MassnahmeViewModel = Comain.Client.ViewModels.WPlan.IHObjekte.MassnahmeViewModel;
using System.ComponentModel;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Controllers.WPlan
{

    public interface IMassnahmeController : IController
    {}

    /// <summary>
    /// Massnahmen und Wartungsaufträge in IHObjektdetail einblenden
    /// </summary>
    public class MassnahmeController : ControllerBase, IMassnahmeController, IMassnahmePluginController
    {

        private readonly IFlyoutService flyouts;
        private readonly IMassnahmeService store;
        private readonly IModulService modules;
        private readonly MainViewModel mainView;
        private readonly IHObjektViewModel view;
        private readonly AuftragViewModel auftragView;
        private readonly MassnahmeViewModel massnahmeView;
        private readonly ProgressViewModel progressView;
        private IVorgabeController vorgaben;
        private IHObjekt currentIH;
        private EditMode currentMode;

        private SerializableCommand searchMassnahmeCommand;
        private SerializableCommand openMassnahmeCommand;
        private SerializableCommand addMassnahmeCommand;
        private SerializableCommand openAuftragCommand;
        private SerializableCommand createAufträgeCommand;
        private SerializableCommand saveAufträgeCommand;
        private CancellationTokenSource pollTokenSource = new CancellationTokenSource();


        public MassnahmeController(IMessageService messages, IFlyoutService flyouts, IControllerBus bus, IMassnahmeService store,
                                   IModulService modules, MainViewModel mainView, IHObjektViewModel view,
                                   AuftragViewModel auftragView, MassnahmeViewModel massnahmeView, ProgressViewModel progressView)
            : base(messages, bus)
        {

            this.store = store;
            this.flyouts = flyouts;
            this.view = view;
            this.mainView = mainView;
            this.auftragView = auftragView;
            this.massnahmeView = massnahmeView;
            this.modules = modules;
            this.progressView = progressView;
        }


        public object   AuftragView         => auftragView?.View;
        public object   MassnahmeView       => massnahmeView?.View;
        public ICommand SaveAufträgeCommand => saveAufträgeCommand;


        public List<MassnahmeViewItem>   GebeMassnahmen() => massnahmeView.Massnahmen.ToList();


        public void Initialize()
        {

            vorgaben = bus.GetController<IVorgabeController>();

            auftragView.ProgressView = progressView.View;

            auftragView.EditCommand = CreateCommand(_ => OpenAuftragAsync());
            massnahmeView.EditCommand = CreateCommand(_ => OpenMassnahmeAsync());
            auftragView.SearchCommand = searchMassnahmeCommand = CreateCommand(_ => ReadAufträgeAsync(), _ => currentIH != null);
            openAuftragCommand = CreateCommand(_ => OpenAuftragAsync(), _ => auftragView.CurrentAuftrag != null);
            openMassnahmeCommand = CreateCommand(_ => OpenMassnahmeAsync(), _ => massnahmeView.CurrentMassnahme != null);
            addMassnahmeCommand = CreateCommand(_ => AddMassnahmeAsync(), _ => currentIH != null);
            createAufträgeCommand = CreateCommand(_ => CreateAufträgeAsync(), _ => currentIH != null);
            auftragView.SaveCommand = saveAufträgeCommand = CreateCommand(_ => SaveAufträgeAsync(), _ => auftragView.HatPlanung);
            progressView.CloseCommand = new AsyncDelegateCommand(_ => CloseCreateAufträgeAsync());

            PropertyChangedEventManager.AddHandler(massnahmeView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(auftragView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.PageView = view.View;

            var mr = parameter as IHObjektControllerMessage;   //Rückgabe
            if (mr != null) await DetailAsync(mr.IHObjekt).ConfigureAwait(false);
            else {    //Direktaufruf Kachel

                var pslist = await store.FilterPersonalAsync(bus.Token).ConfigureAwait(false);
                auftragView.PsSearchProvider.SetItems(pslist);
            }

            UpdateCommands();
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(auftragView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(massnahmeView, ViewPropertyChanged, "");
        }


        public async Task<bool> UnloadAsync()
        {

            if (AufträgeWurdenBearbeitet && !await messages.ConfirmExitAsync()) return false;
            Unload();
            return true;
        }


        private bool AufträgeWurdenBearbeitet   => auftragView.HatPlanung && auftragView.Foundlist.IsChanged;
        private bool MassnahmenWurdenBearbeitet => massnahmeView.Massnahmen != null && massnahmeView.Massnahmen.IsChanged;


        private void Unload()
        {

            auftragView.Unload();
            massnahmeView.Unload();
            UpdateCommands();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (sender == massnahmeView && args.PropertyName == nameof(massnahmeView.CurrentMassnahme)) UpdateCommands();
            if (sender == auftragView && args.PropertyName == nameof(auftragView.CurrentAuftrag)) UpdateCommands();
        }


        /// <summary>
        /// Zusatzdaten laden
        /// </summary>
        public async Task LoadAsync()
            => Task.FromResult(true);


        /// <summary>
        /// Massnahmen / Aufträge eines IH laden
        /// </summary>
        public async Task DetailAsync(IHObjekt ih)
        {

            currentIH = ih;

            if (ih == null) {

                auftragView.Foundlist = null;
                massnahmeView.Massnahmen = null;
                UpdateCommands();
                return;
            }

            var autask = ReadAufträgeAsync();
            var mntask = ReadMassnahmenAsync();
            await Task.WhenAll(autask, mntask).ConfigureAwait(false);
            UpdateCommands();
        }


        /// <summary>
        /// Schnellbearbeitung Massnahmen speichern
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {

            if (currentMode != EditMode.Edit) return;

            using (new VisualDelay(view)) {

                if (!massnahmeView.Massnahmen.IsNullOrEmpty() && massnahmeView.Massnahmen.IsChanged) {

                    await store.MassnahmenSaveAsync(massnahmeView.Massnahmen, bus.Token).ConfigureAwait(false);
                    bus.Token.ThrowIfCancellationRequested();
                }

                await ReadMassnahmenAsync();
            }
        }


        public void SetEditMode(EditMode mode)
        {

            currentMode = mode;
            auftragView.EditEnabled = mode != EditMode.ReadOnly;
            massnahmeView.EditEnabled = auftragView.EditEnabled;
        }


        private async Task ReadMassnahmenAsync()
        {

            var list = await store.MassnahmenReadAsync(currentIH.Id, bus.Token).ConfigureAwait(false);
            await massnahmeView.SetDetailAsync(list.OrderBy(p => p.Name).ToList());

            UpdateCommands();
        }


        private async Task ReadAufträgeAsync()
        {

            if (currentIH == null) return;
            var args = auftragView.GetFilter();
            args.IHList = currentIH.Nummer;

            using (new VisualDelay(auftragView)) {

                var list = await store.WartungReadAsync(args, bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();
                await auftragView.SetAuftraglistAsync(list);
            }

            UpdateCommands();
        }


        public async Task CreateAufträgeAsync()
        {

           if (currentIH == null) return;

            var vallist = auftragView.ValidateFilter();
            if (!vallist.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Wartungsplan kann nicht erzeugt werden.", Reasons = vallist });
                return;
            }

            var args = auftragView.GetFilter();
            args.IHList = currentIH.Nummer;

            var sessionId = await store.WartungCreateAsync(args, bus.Token).ConfigureAwait(false);
            bus.Token.ThrowIfCancellationRequested();

            pollTokenSource = CancellationTokenSource.CreateLinkedTokenSource(bus.Token);

            auftragView.WPlanStatusVisible = true;
            await progressView.PollAsync(sessionId, pollTokenSource);
            UpdateCommands();
        }


        private async Task CloseCreateAufträgeAsync()
        {

            pollTokenSource.Cancel();
            auftragView.WPlanStatusVisible = false;
            await ReadAufträgeAsync();
        }


        private async Task SaveAufträgeAsync()
        {

            auftragView.AuftragnehmerZuordnen();
            if (!auftragView.Foundlist.IsChanged) return;

            using (new VisualDelay(auftragView)) {

                await store.WartungSaveAsync(auftragView.Foundlist, bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();
                await ReadAufträgeAsync();
            }
        }


        public async Task OpenAuftragAsync()
        {

            if (currentMode != EditMode.ReadOnly) return;
            if (!modules.Info.VerwendetModulAuftrag) return;
            if (auftragView.CurrentAuftrag == null) return;
            await bus.JumpAsync(new ControllerMessage { Destination = typeof(IAuftragController), Source = this, Entity_Id = auftragView.CurrentAuftrag.Id }).ConfigureAwait(false);
        }


        public async Task OpenMassnahmeAsync()
        {

            if (currentMode != EditMode.ReadOnly) return;
            if (massnahmeView.CurrentMassnahme == null) return;

            var msg = new IHObjektControllerMessage { Destination = typeof(IVorgabeController), Source = this, Entity_Id = massnahmeView.CurrentMassnahme.Id, IHObjekt = currentIH,
                                                      Massnahme_Id = massnahmeView.CurrentMassnahme.Id };
            await bus.JumpAsync(msg).ConfigureAwait(false);
        }


        public async Task AddMassnahmeAsync()
        {

            var msg = new IHObjektControllerMessage { Destination = typeof(IVorgabeController), Source = this, IHObjekt = currentIH };
            await bus.JumpAsync(msg).ConfigureAwait(false);
        }


        private void UpdateCommands()
        {

            context.Send(new SendOrPostCallback((o) => {

                openMassnahmeCommand.RaiseCanExecuteChanged();
                addMassnahmeCommand.RaiseCanExecuteChanged();
                openAuftragCommand.RaiseCanExecuteChanged();
                createAufträgeCommand.RaiseCanExecuteChanged();
                saveAufträgeCommand.RaiseCanExecuteChanged();
                searchMassnahmeCommand.RaiseCanExecuteChanged();

            }), null);
        }
    }
}
