﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;


namespace Comain.Client.Views
{

    public partial class MainView : MetroWindow, IMainView
    {

        private readonly SynchronizationContext context;
        private readonly ITabletInfo tabletInfo;
        private TaskCompletionSource<bool> flyoutTcs;
        private bool dialogOpened;


        public MainView(ITabletInfo tabletInfo)
        {

            InitializeComponent();

            this.tabletInfo = tabletInfo;
            context = SynchronizationContext.Current;
            FlyoutControl.IsOpenChanged += async (s, e) => { if (flyoutTcs != null && !await FlyoutIsOpenAsync()) flyoutTcs.TrySetResult(false); };
            DialogManager.DialogOpened += (o, e) => { dialogOpened = true; };
            DialogManager.DialogClosed += (o, e) => { dialogOpened = false; };
        }


        public Task<bool> OpenFlyoutAsync(FlyoutConfig view)
        {

            flyoutTcs = new TaskCompletionSource<bool>();

            context.Send(new SendOrPostCallback((o) => {

                    this.FlyoutView.Content = view.ChildView;
                    FlyoutControl.Header    = view.Header;
                    FlyoutControl.Position  = view.Position;
                    FlyoutControl.IsModal   = view.IsModal;
                    FlyoutControl.IsPinned  = view.IsPinned;
                    FlyoutControl.Theme     = view.Theme;
                    FlyoutControl.IsOpen    = true;
            }), null);

            return flyoutTcs.Task;
        }


        public void OpenFlyout(FlyoutConfig view)
        {

            context.Send(new SendOrPostCallback((o) => {

                    this.FlyoutView.Content  = view.ChildView;
                    FlyoutControl.Header     = view.Header;
                    FlyoutControl.Position   = view.Position;
                    FlyoutControl.IsModal    = view.IsModal;
                    FlyoutControl.IsPinned   = view.IsPinned;
                    FlyoutControl.Theme      = view.Theme;
                    FlyoutControl.IsOpen     = true;
            }), null);
        }


        public async Task CloseFlyoutAsync()
        {
            await context.SendAsync(new SendOrPostCallback((o) => { FlyoutControl.IsOpen = false; }), null);
        }


        public async Task<bool> FlyoutIsOpenAsync()
        {

            bool b = false;
            await context.SendAsync(new SendOrPostCallback((o) => { b = FlyoutControl.IsOpen; }), null);
            return b;
        }


        public bool DialogIsOpen { get { return dialogOpened; } }


        public void ShowMessage(String header, String message)
        {

            context.Post(new SendOrPostCallback((o) => {

                MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
                this.ShowMessageAsync(header, message);
            }), null);
        }


        public Task<bool> ConfirmExitAsync(String head = null, String body = null)
        {

            var st = new MetroDialogSettings();
            st.AffirmativeButtonText = "Ja";
            st.NegativeButtonText = "Nein";
            st.AnimateShow = true;
            st.AnimateHide = false;
            String headmsg = head == null? "Ihre Bearbeitung wurde noch nicht gespeichert": head;
            String bodymsg = body == null? "Möchten Sie fortfahren?": body;

            var tcs = new TaskCompletionSource<bool>();

            context.Send(new SendOrPostCallback(async (o) => {

                var mdr = await this.ShowMessageAsync(headmsg, bodymsg, MessageDialogStyle.AffirmativeAndNegative, st);
                tcs.TrySetResult(mdr == MessageDialogResult.Affirmative);
            }), null);

            return tcs.Task;
        }


        private void WindowClosing(object sender, CancelEventArgs e)
        {

            var ctx = DataContext as MainViewModel;
            if (ctx == null || ctx.ExitCommand == null) return;

            e.Cancel = true;
            ctx.ExitCommand.Execute(null);
        }


        private void Window_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            if (tabletInfo != null) tabletInfo.Update();
        }
    }
}
