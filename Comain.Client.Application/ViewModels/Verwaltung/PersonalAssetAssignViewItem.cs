﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Users;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class PersonalAssetAssignViewItem : NotifyBase
    {

        private Personal psmodel;
        private IHObjektFilterModel ihmodel;
        private IHObjektPersonal model;


        public PersonalAssetAssignViewItem(Personal psmodel, IHObjektFilterModel ihmodel, IHObjektPersonal model)
        {

            this.psmodel = psmodel;
            this.ihmodel = ihmodel;
            this.model = model;
        }


        public bool IsSelected
        {
            get => model != null;
            set => Select(value);
        }


        /// <remarks>
        /// Methode kann auch fremd aufgerufen werden, also wird selektiert obwohl schon ein Objekt dranhängt 
        /// </remarks>
        public void Select(bool select)
        {

            if (!select && model != null) {

                psmodel.Assets.Remove(model);
                model = null;
                RaisePropertyChanged(nameof(IsSelected));

            } else if (select && model == null) {

                model = new IHObjektPersonal { IHObjekt_Id = ihmodel.Id, Personal_Id = psmodel.Id, Rolle = Rolle };
                psmodel.Assets.Add(model);
                RaisePropertyChanged(nameof(IsSelected));
            }
        }


        private Personalrolle Rolle { get; set; }


        public bool IstTeamleiter
        {
            get => model != null && model.Rolle == Personalrolle.Teamleiter;
            set {
                if (model != null) {

                    model.Rolle = value? Personalrolle.Teamleiter: Personalrolle.Nutzer;
                    RaisePropertyChanged(nameof(IstTeamleiter));
                }
            }
        }


        public String Assetnummer       => ihmodel.Nummer;
        public String Asset             => ihmodel.Name;

        public String Personalnummer    => psmodel.Nummer;
        public String Nachname          => psmodel.Nachname;
        public String Vorname           => psmodel.Vorname;
    }
}
