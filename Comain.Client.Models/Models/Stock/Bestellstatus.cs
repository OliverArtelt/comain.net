﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Stock
{

    public enum Bestellstatus
    {

        Undefiniert = 0,
        /// <summary>
        /// entspricht hier "Bestellt"
        /// </summary>
        Offen = 1,
        Abgeschlossen = 2,
        Storniert = 3,
        Angelegt = 4,  
        Überfällig = 5,  
        Abschlussfähig = 6  
    }
}
