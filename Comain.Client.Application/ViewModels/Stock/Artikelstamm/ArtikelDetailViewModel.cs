﻿using System;
using Comain.Client.Views.Stock.Artikelstamm;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.Stock;
using System.ComponentModel;
using Comain.Client.Services;

namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class ArtikelDetailViewModel : ValidatableViewModel<IArtikelDetailView, Artikel>, IDelayableViewModel
    {


        public enum Tabs { Basis = 0, Specs, Lieferant, Druck, Docs }
        private readonly IModulService module;


        public ArtikelDetailViewModel(IArtikelDetailView view, IModulService module) : base(view)
        {
            this.module = module;
        }


        public override void CurrentChanged(Artikel oldSelected)
        {

            SetzeHeader();
            RaisePropertyChanged(nameof(SpecsVisible));
        }


        protected override void ModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == nameof(Artikel.Nummer)) RaisePropertyChanged(nameof(ArtikelHeader1));
            if (args.PropertyName == nameof(Artikel.Name))   RaisePropertyChanged(nameof(ArtikelHeader2));
        }


        private void SetzeHeader()
        {

            RaisePropertyChanged(nameof(ArtikelHeader1));
            RaisePropertyChanged(nameof(ArtikelHeader2));
            RaisePropertyChanged(nameof(DeaktiviertHint));
            RaisePropertyChanged(nameof(HintVisible));
        }


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public void ResetTabsIfPrint()
        {
            if (Tab == Tabs.Druck) CurrentTab = 0;
        }


#region P R O P E R T I E S


        public String ArtikelHeader1 => Current?.Nummer;


        public String ArtikelHeader2 => Current?.Name;


        public String DeaktiviertHint
            => current == null || !current.DeaktiviertAm.HasValue? null: $"Der Artikel wurde am {current.DeaktiviertAm.Value.ToShortDateString()} deaktiviert.";


        public bool HintVisible => current != null && current.DeaktiviertAm.HasValue;


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }


        private object specView;
        public object SpecView
        { 
            get { return specView; }
            set { if (SetProperty(ref specView, value)) RaisePropertyChanged(nameof(SpecsVisible)); }
        }


        public bool SpecsVisible => module.Info.VerwendetModulSpecs && SpecView != null;


        private object basisView;
        public object BasisView
        {
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object lfView;
        public object LfView
        {
            get { return lfView; }
            set { SetProperty(ref lfView, value); }
        }


        private object reportView;
        public object ReportView
        {
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


        private object dokumentView;
        public object DokumentView
        {
            get { return dokumentView; }
            set { SetProperty(ref dokumentView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
