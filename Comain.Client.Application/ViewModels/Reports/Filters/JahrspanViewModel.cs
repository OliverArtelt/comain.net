﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class JahrspanViewModel : ViewModel<IJahrspanView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public JahrspanViewModel(IFilterService store, IJahrspanView view) : base(view)
        {
            this.store = store;    
        }


        public void Reset()
        {
            
            JahrVon = Jahre.FirstOrDefault();
            JahrBis = Jahre.LastOrDefault();
        }


        public String Validiere()
        {
            
            if (!JahrVon.HasValue || !JahrBis.HasValue) return "Geben Sie beide Jahreszahlen an.";
            if (JahrVon.Value > JahrBis.Value) return "Das Startjahr darf nicht größer als des Endjahr sein.";

            return null;
        }


        public string FilterAlsString()
        {
            return String.Format("Jahre {0} - {1}", JahrVon, JahrBis);
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (JahrVon.HasValue) filter.Add("JahrVon", JahrVon.Value);
            if (JahrVon.HasValue) filter.Add("JahrBis", JahrBis.Value);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            
            Jahre = Enumerable.Range(store.Periode.Von, store.Periode.Bis - store.Periode.Von + 1).ToList();
            Reset();
            return Task.FromResult(true);     
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private IEnumerable<int> jahre;
        public IEnumerable<int> Jahre
        { 
            get { return jahre; }
            set { SetProperty(ref jahre, value); }
        } 
   

        private int? von;
        public int? JahrVon
        { 
            get { return von; }
            set { SetProperty(ref von, value); }
        } 
   

        private int? bis;
        public int? JahrBis
        { 
            get { return bis; }
            set { SetProperty(ref bis, value); }
        } 
   

#endregion


    }
}
