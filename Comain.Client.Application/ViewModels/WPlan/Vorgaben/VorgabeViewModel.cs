﻿using Comain.Client.ViewModels;
using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.WPlan.Vorgaben;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeViewModel : ViewModel<IVorgabeView>, IDelayableViewModel
    {
 
        public VorgabeViewModel(IVorgabeView view) 
          : base(view)
        {
        }


        public void Unload()
        {
        }
         

#region P R O P E R T I E S


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool saveVisible;
        public bool SaveVisible
        { 
            get { return saveVisible; }
            set { SetProperty(ref saveVisible, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        { 
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private bool editVisible;
        public bool EditVisible
        { 
            get { return editVisible; }
            set { SetProperty(ref editVisible, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private bool cancelVisible;
        public bool CancelVisible
        { 
            get { return cancelVisible; }
            set { SetProperty(ref cancelVisible, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private bool reloadVisible;
        public bool ReloadVisible
        { 
            get { return reloadVisible; }
            set { SetProperty(ref reloadVisible, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private bool addVisible;
        public bool AddVisible
        { 
            get { return addVisible; }
            set { SetProperty(ref addVisible, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private bool deleteVisible;
        public bool DeleteVisible
        { 
            get { return deleteVisible; }
            set { SetProperty(ref deleteVisible, value); }
        }


        private ICommand addMaterialCommand;
        public ICommand AddMaterialCommand
        { 
            get { return addMaterialCommand; }
            set { SetProperty(ref addMaterialCommand, value); }
        }


        private ICommand deleteMaterialCommand;
        public ICommand DeleteMaterialCommand
        { 
            get { return deleteMaterialCommand; }
            set { SetProperty(ref deleteMaterialCommand, value); }
        }


        private bool materialSelected;
        public bool MaterialSelected
        { 
            get { return materialSelected; }
            set { SetProperty(ref materialSelected, value); }
        }


        private ICommand imageFromCameraCommand;
        public ICommand ImageFromCameraCommand
        { 
            get { return imageFromCameraCommand; }
            set { SetProperty(ref imageFromCameraCommand, value); }
        }


        private ICommand imageFromFileCommand;
        public ICommand ImageFromFileCommand
        { 
            get { return imageFromFileCommand; }
            set { SetProperty(ref imageFromFileCommand, value); }
        }


        private ICommand editImageCommand;
        public ICommand EditImageCommand
        { 
            get { return editImageCommand; }
            set { SetProperty(ref editImageCommand, value); }
        }


        private ICommand saveImageCommand;
        public ICommand SaveImageCommand
        { 
            get { return saveImageCommand; }
            set { SetProperty(ref saveImageCommand, value); }
        }


        private ICommand deleteImageCommand;
        public ICommand DeleteImageCommand
        { 
            get { return deleteImageCommand; }
            set { SetProperty(ref deleteImageCommand, value); }
        }


        private ICommand deleteImagesCommand;
        public ICommand DeleteImagesCommand
        { 
            get { return deleteImagesCommand; }
            set { SetProperty(ref deleteImagesCommand, value); }
        }


        private ICommand openAuftragCommand;
        public ICommand OpenAuftragCommand
        { 
            get { return openAuftragCommand; }
            set { SetProperty(ref openAuftragCommand, value); }
        }


        private bool openAuftragVisible;
        public bool OpenAuftragVisible
        { 
            get { return openAuftragVisible; }
            set { SetProperty(ref openAuftragVisible, value); }
        }


        private bool mediaSelected;
        public bool MediaSelected
        { 
            get { return mediaSelected; }
            set { SetProperty(ref mediaSelected, value); }
        }


        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
    

#endregion

    }
}
