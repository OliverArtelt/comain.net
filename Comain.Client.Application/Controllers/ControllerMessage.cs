﻿using System;
using System.Collections.Generic;
using Comain.Client.ViewModels;

namespace Comain.Client.Controllers
{

    public class ControllerMessage
    {

        public Type         Destination   { get; set; }
        public IController  Source        { get; set; }
        public int          Entity_Id     { get; set; }
        public object       Result        { get; set; }
        public object       Parameter     { get; set; }
        public EditMode     EditMode      { get; set; }
    }
}
