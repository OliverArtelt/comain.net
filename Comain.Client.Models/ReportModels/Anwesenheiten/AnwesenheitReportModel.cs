﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Anwesenheiten;

namespace Comain.Client.ReportModels.Anwesenheiten
{

    public class AnwesenheitReportModel
    {

        private readonly AnwesenheitReportDto dto;


        public AnwesenheitReportModel(AnwesenheitReportDto dto)
            => this.dto = dto;


        public int          Id                  => dto.Id;
        public String       Personalname        => dto.Personalname;
        public String       Personalnummer      => dto.Personalnummer;
        public String       Personalklammer     => dto.Personalklammer;

        public String       Standortname        => dto.Standortname;
        public int          Standortnummer      => dto.Standortnummer;
        public String       Assetname           => dto.Assetname;
        public String       Assetnummer         => dto.Assetnummer;

        public DateTime     Datum               => dto.Datum;
        public TimeSpan     UhrzeitVon          => dto.UhrzeitVon;
        public TimeSpan     UhrzeitBis          => dto.UhrzeitBis;
        public decimal      Pause               => dto.Pause.GetValueOrDefault();
        public String       Tätigkeit           => dto.Tätigkeit;

        public DateTime     Start               => Datum + UhrzeitVon;
        public DateTime     Ende                => Datum + UhrzeitBis;
        public TimeSpan     Anwesenheit         => Ende - Start;
        public TimeSpan     Arbeitszeit         => Anwesenheit - Pausenzeit;
        public TimeSpan     Pausenzeit          => new TimeSpan((int)Pause, (int)((Pause - (int)Pause) * 60), 0);
        public double       GearbeiteteStunden  => Arbeitszeit.TotalHours;
    }
}
