﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Emma
{
    public class EmmaDto : ChangeTrackingBase
    {

        public String               EmmaAuftragsnummer      { get; set; }
        public String               EmmaPersonalnummer      { get; set; }
        public String               EmmaNachname            { get; set; }
        public String               EmmaVorname             { get; set; }
        public String               EmmaInventarnummer      { get; set; }
        public String               EmmaMaschine            { get; set; }
        public String               EmmaAktion              { get; set; }
        public DateTime             EmmaVon                 { get; set; }
        public DateTime             EmmaBis                 { get; set; }

        public int                  Typ                     { get; set; }
        public DateTime             ImportiertAm            { get; set; }
        public DateTime?            GelöschtAm              { get; set; }
        public String               Fehlerstatus            { get; set; }

        public DateTime             Datum                   { get; set; }
        public TimeSpan             UhrzeitVon              { get; set; }
        public TimeSpan             UhrzeitBis              { get; set; }
        public int                  Minuten                 { get; set; }
        public bool                 IstBestätigt            { get; set; }

        public IHListDto            IHObjekt                { get; set; }
        public AuftragListDto       Auftrag                 { get; set; }
        public PersonalFilterModel  Personal                { get; set; }
        public virtual int?         Personalleistung_Id     { get; set; }
    }
}
