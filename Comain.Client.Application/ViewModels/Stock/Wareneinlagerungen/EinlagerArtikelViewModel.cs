﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{
    
    public class EinlagerArtikelViewModel : ViewModel<IEinlagerArtikelView>
    {
        
        private readonly SynchronizationContext context;
        private IEnumerable<String> searchterms;


        public EinlagerArtikelViewModel(IEinlagerArtikelView view) : base(view)
        {
            
            context = SynchronizationContext.Current;
            SearchCommand = new DelegateCommand(Refresh);
        }


        public void Unload()
        {
            FoundList = null;
        }


        public void SetzeArtikelliste(IEnumerable<Artikel> liste)
        {

            FoundList = new CollectionViewSource { Source = liste }.View;
            FoundList.Filter = Predicate;
            Refresh();
        }


        private bool Predicate(object arg)
        {

            if (String.IsNullOrEmpty(filter)) return true;
            
            var at = arg as Artikel;
            if (at == null) return false;
            return at.Matches(searchterms);
        }

       
        private void Refresh()
        {

            if (FoundList == null) return;
            searchterms = null;
            if (!String.IsNullOrWhiteSpace(Filter)) searchterms = Filter.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            context.Post(new SendOrPostCallback((o) => { 
            
                FoundList?.Refresh();        
                RaisePropertyChanged("FoundList");
                Current = null;
                var cnt = ((CollectionView)FoundList).Count;

                switch (cnt) {

                case 0: 

                    Result = "Es wurde kein Artikel gefunden.";
                    break;    

                case 1: 

                    Result = "Es wurde ein Artikel gefunden.";
                    break;    

                default: 

                    Result = String.Format("Es wurden {0} Artikel gefunden.", cnt);
                    break;    
                }

            }), null);
        } 


#region P R O P E R T I E S
 
        
        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }   
            set { SetProperty(ref takeCommand, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }   
            set { SetProperty(ref searchCommand, value); }
        }


        private String filter;
        public String Filter
        {
            get { return filter; }   
            set { SetProperty(ref filter, value); }
        }


        private String result;
        public String Result
        {
            get { return result; }   
            set { SetProperty(ref result, value); }
        }


        private Artikel current;
        public Artikel Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


        public ICollectionView FoundList { get; private set; }


#endregion

    }
}
