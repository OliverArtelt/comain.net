﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class SubNummerFilterModel : EntityModelBase
    {

        public String   Nummer      { get; set; }
        public String   Name        { get; set; }
        public int      Haupt_Id    { get; set; }
    }
}
