﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneingänge;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{
    
    public class EingangListViewModel : ViewModel<IEingangListView>
    {
 
        private readonly SynchronizationContext context;


        public EingangListViewModel(IEingangListView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
            Foundlist = null;
        }


        /// <summary>
        /// Liste aktualisieren beim Speichern falls LS neu war, WE eingefügt oder gelöscht wurden
        /// </summary>
        /// <param name="ls">Lieferschein Zustand nach Speichern</param>
        public async Task SynchronizeAsync(Lieferschein ls)
        {

            if (ls == null || ls.Eingänge.IsNullOrEmpty() || ls.Id == 0) return;

            var items = Foundlist.Where(p => p.LieferscheinId == ls.Id).ToList();
            var winners = ls.Eingänge.Where(we => we.IstAngefügt && !items.Any(q => q.EingangId == we.Id)).Select(p => new EingangListViewItem(p)).ToList();
            var losers = items.Where(p => !ls.Eingänge.Any(we => we.Id == p.EingangId)).ToList();

            await context.SendAsync(new SendOrPostCallback((o) => {  
            
                if (!losers.IsNullOrEmpty()) losers.ForEach(p => Foundlist.Remove(p));
                if (!winners.IsNullOrEmpty()) Foundlist.AddRange(winners);
                if (Current != null && !Foundlist.Contains(Current)) Current = null;

            }), null);
        }


        /// <summary>
        /// Lieferschein wurde gelöscht
        /// </summary>
        public async Task RemoveItemsAsync(IEnumerable<Wareneingang> welist)
        {

            if (welist.IsNullOrEmpty()) return;
            var items = Foundlist.Where(p => welist.Any(q => q.Id == p.EingangId)).ToList();
            if (items.IsNullOrEmpty()) return;

            await context.SendAsync(new SendOrPostCallback((o) => {  
            
                items.ForEach(p => Foundlist.Remove(p));
                if (items.Any(p => p == Current)) Current = null;

            }), null);
        }


#region P R O P E R T I E S
 
        
        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private ObservableCollection<EingangListViewItem> foundlist;
        public ObservableCollection<EingangListViewItem> Foundlist
        {
            get { return foundlist; }   
            set { SetProperty(ref foundlist, value); }
        }


        private EingangListViewItem current;
        public EingangListViewItem Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
