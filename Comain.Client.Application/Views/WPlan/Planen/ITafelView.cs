﻿using System;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Views.WPlan.Planen
{

    public interface ITafelView : IView
    {

        void SpaltenbreitenAnpassen();
        void Build(Plan model);
        Task UnloadAsync();
        void ScrollToLeftTop();
        void ScrollToYPosition(double value);
    }
}
