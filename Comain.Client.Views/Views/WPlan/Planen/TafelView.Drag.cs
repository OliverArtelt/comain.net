﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Comain.Client.Views.Internal;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Views.WPlan.Planen
{

    partial class TafelView
    {

        private enum DragMode { None, Object, SelectionBox }
        private Point mouseDownPos;
        private DragMode MouseOperation { get; set; }


        private void SelectionBoxMouseDown(object sender, MouseEventArgs e)
        {

            if (e.LeftButton == MouseButtonState.Pressed) {

                MouseOperation = DragMode.SelectionBox;
                mouseDownPos = e.GetPosition(Plan);

                Plan.CaptureMouse();
                Canvas.SetLeft(SelectionBox, mouseDownPos.X);
                Canvas.SetTop(SelectionBox, mouseDownPos.Y);
                SelectionBox.Width = 0;
                SelectionBox.Height = 0;
                SelectionBox.Visibility = Visibility.Visible;

            } else if (e.RightButton == MouseButtonState.Pressed) {

                ViewModel.PopupCommand.Execute(e.GetPosition(Plan));
            }
        }


        private void SelectionBoxMouseMove(object sender, MouseEventArgs e)
        {

            if (MouseOperation == DragMode.SelectionBox) {

                Point mousePos = e.GetPosition(Plan);
                Canvas.SetLeft(SelectionBox, Math.Min(mouseDownPos.X, mousePos.X));
                SelectionBox.Width = Math.Abs(mousePos.X - mouseDownPos.X);
                Canvas.SetTop(SelectionBox, Math.Min(mouseDownPos.Y, mousePos.Y));
                SelectionBox.Height = Math.Abs(mousePos.Y - mouseDownPos.Y);
            }
        }


        private void SelectionBoxMouseUp(object sender, MouseButtonEventArgs e)
        {

            if (MouseOperation != DragMode.SelectionBox) return;
            MouseOperation = DragMode.None;
            Plan.ReleaseMouseCapture();
            SelectionBox.Visibility = Visibility.Collapsed;

            Point mouseUpPos = e.GetPosition(Plan);
            Rect rect = new Rect(mouseDownPos, mouseUpPos);
            ViewModel.SelectCommand.Execute(rect);
        }


        /// <summary>
        /// Scrollen wenn SelectionBox / Drag operation am Rand angelangt ist
        /// </summary>
        private void ScrollPlanIntoView(Point mousePos)
        {

            if (mousePos.X > VisibleMaxX) PlanViewPort.ScrollToHorizontalOffset(PlanViewPort.HorizontalOffset + (mousePos.X - VisibleMaxX) / 5);
            if (mousePos.X < VisibleMinX) PlanViewPort.ScrollToHorizontalOffset(PlanViewPort.HorizontalOffset - (VisibleMinX - mousePos.X) / 5);
            if (mousePos.Y > VisibleMaxY) PlanViewPort.ScrollToVerticalOffset(PlanViewPort.VerticalOffset + (mousePos.Y - VisibleMaxY) / 5);
            if (mousePos.Y < VisibleMinY) PlanViewPort.ScrollToVerticalOffset(PlanViewPort.VerticalOffset - (VisibleMinY - mousePos.Y) / 5);
        }


        /// <remarks>
        /// Objekte direkt verschieben und Drag-Autoscroll
        /// </remarks>
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {

            if (e.LeftButton != MouseButtonState.Pressed) return;

            Point mousePos = e.GetPosition(Plan);
            if (MouseOperation != DragMode.None) ScrollPlanIntoView(mousePos);

            if (MouseOperation == DragMode.SelectionBox) return;
            var sender = (e.OriginalSource as Border)?.Tag as Planaufgabe;
            if (sender == null || !sender.IstBearbeitbar || sender.IstFixiert) return;

            DragDrop.DoDragDrop(this, sender, DragDropEffects.Move | DragDropEffects.Copy);
            MouseOperation = DragMode.Object;
        }


        private void ObjectDrop(object sender, DragEventArgs e)
        {

            if (e.Handled) return;
            if (MouseOperation != DragMode.Object) return;
            MouseOperation = DragMode.None;

            var pos = e.GetPosition(Plan);
            ViewModel.DragCommand?.Execute(pos);
        }
    }
}
