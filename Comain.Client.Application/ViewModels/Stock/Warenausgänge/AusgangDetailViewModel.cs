﻿using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Warenausgänge;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{
    
    public class AusgangDetailViewModel : ValidatableViewModel<IAusgangDetailView, Warenausgang>, IDelayableViewModel
    {
 
        public enum Tabs { Basis = 0, Druck } 


        public AusgangDetailViewModel(IAusgangDetailView view) : base(view)
        {}


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public Tabs Tab { get { return (Tabs)currentTab; } }


#region P R O P E R T I E S
                 
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }


        private object basisView;
        public object BasisView
        { 
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object reportView;
        public object ReportView
        { 
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
 

#endregion

    }
}
