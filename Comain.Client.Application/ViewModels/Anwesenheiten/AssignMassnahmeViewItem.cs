﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.WPlan;
using Comain.Client.Trackers;


namespace Comain.Client.ViewModels.Anwesenheiten
{

    public class AssignMassnahmeViewItem : NotifyBase
    {

        private readonly Anwesenheit anwesenheit;
        private readonly MassnahmeListDto massnahme;
        private AnwesenheitMassnahme assignment;


        public AssignMassnahmeViewItem(Anwesenheit anwesenheit, MassnahmeListDto massnahme)
        {

            this.anwesenheit = anwesenheit;
            this.massnahme = massnahme;
            assignment = anwesenheit.Massnahmen?.FirstOrDefault(p => p.Massnahme_Id == massnahme.Id);
            Menge = assignment?.Menge ?? massnahme.Menge ?? 1;
        }


        public bool IstSelektiert
        {
            get => assignment != null;
            set {

                if (!value) {

                    anwesenheit.Massnahmen.Remove(assignment);
                    assignment = null;

                } else if (value && assignment == null) {

                    assignment = new AnwesenheitMassnahme { Massnahme_Id = massnahme.Id, Anwesenheit_Id = anwesenheit.Id, Menge = massnahme.Menge ?? 1 };
                    anwesenheit.Massnahmen.Add(assignment);
                }

                RaisePropertyChanged(nameof(IstSelektiert));
            }
        }


        public int Menge
        {
            get => assignment?.Menge ?? massnahme.Menge ?? 1;
            set {

                if (assignment != null && value != Menge) {

                    assignment.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public String Name                  => massnahme.Name;
        public String Nummer                => massnahme.Nummer;
        public String Auftrag               => assignment?.Auftrag;
        public int?   Auftrag_Id            => assignment?.Auftrag_Id;
        public int?   Normzeit              => massnahme.Normzeit;
        public String TermintypAsString     => ((Termintyp)massnahme.Termintyp).AsText();
        public String Leistungsart          => massnahme.Leistungsart;
        public String Gewerk                => massnahme.Gewerk;
        public String ExterneAuftragsnummer => massnahme.ExterneAuftragsnummer;
        public String ZyklusAsString        => WiMassnahme.ZyklusAsString((Termintyp)massnahme.Termintyp, massnahme.Zyklus, (Weekday)massnahme.Tagesmuster);

        public int?   Leistungsart_Id       => massnahme.Leistungsart_Id;
        public int?   Gewerk_Id             => massnahme.Gewerk_Id;
    }
}
