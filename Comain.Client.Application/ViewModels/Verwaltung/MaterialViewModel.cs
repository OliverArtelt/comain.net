﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class MaterialViewModel : ValidatableViewModel<IMaterialView, Materialstamm>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public MaterialViewModel(IMaterialView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }
 

        public void Unload()
        {

            Liste = null;
            Warengruppen = null;
            Einheiten = null;
            Current = null;
        }


        public override void CurrentChanged(Materialstamm oldSelected)
        {
            
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Herstellernummer");
            RaisePropertyChanged("Name");
            RaisePropertyChanged("KlasseUndefiniert");
            RaisePropertyChanged("KlasseA");
            RaisePropertyChanged("KlasseB");
            RaisePropertyChanged("KlasseC");
            RaisePropertyChanged("Mischpreis");
            RaisePropertyChanged("Einheit_Id");
            RaisePropertyChanged("Warengruppe_Id");
            RaisePropertyChanged("Beschreibung");
            RaisePropertyChanged("MaterialLieferantMap");
        }


#region P R O P E R T I E S


        public String Nummer                  
        {
            get { return current == null? null: current.Nummer; }
            set { 
            
                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }


        public String Herstellernummer                  
        {
            get { return current == null? null: current.Herstellernummer; }
            set { 
            
                if (current != null && current.Herstellernummer != value) {

                    current.Herstellernummer = value;
                    RaisePropertyChanged("Herstellernummer");
                }
            }
        }


        public String Name                  
        {
            get { return current == null? null: current.Name; }
            set { 
            
                if (current != null && current.Name != value) {

                    current.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }


        public String Beschreibung                  
        {
            get { return current == null? null: current.Beschreibung; }
            set { 
            
                if (current != null && current.Beschreibung != value) {

                    current.Beschreibung = value;
                    RaisePropertyChanged("Beschreibung");
                }
            }
        }


        public decimal? Mischpreis                  
        {
            get { return current == null? null: current.Mischpreis; }
            set { 
            
                if (current != null && current.Mischpreis != value) {

                    current.Mischpreis = value;
                    RaisePropertyChanged("Mischpreis");
                }
            }
        }


        public bool KlasseUndefiniert                
        {
            get { return current == null? false: !current.Klasse.HasValue; }
            set { 
            
                if (current != null && value && current.Klasse.HasValue) {

                    current.Klasse = null;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseA                  
        {
            get { return current == null? false: current.Klasse == Materialklasse.A; }
            set { 
            
                if (current != null && value && current.Klasse != Materialklasse.A) {

                    current.Klasse = Materialklasse.A;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseB                  
        {
            get { return current == null? false: current.Klasse == Materialklasse.B; }
            set { 
            
                if (current != null && value && current.Klasse != Materialklasse.B) {

                    current.Klasse = Materialklasse.B;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseC                  
        {
            get { return current == null? false: current.Klasse == Materialklasse.C; }
            set { 
            
                if (current != null && value && current.Klasse != Materialklasse.C) {

                    current.Klasse = Materialklasse.C;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public int? Einheit_Id                  
        {
            get { return current == null? null: current.Einheit_Id; }
            set { 
            
                if (current != null && current.Einheit_Id != value) {

                    current.Einheit_Id = value;
                    RaisePropertyChanged("Einheit_Id");
                }
            }
        }


        public int? Warengruppe_Id                  
        {
            get { return current == null? null: current.Warengruppe_Id; }
            set { 
            
                if (current != null && current.Warengruppe_Id != value) {

                    current.Warengruppe_Id = value;
                    RaisePropertyChanged("Warengruppe_Id");
                }
            }
        }


        public TrackableCollection<MaterialLieferant> MaterialLieferantMap
        { 
            get { return current == null? null: current.MaterialLieferantMap; }
            set { 
            
                if (current != null && current.MaterialLieferantMap != value) {

                    current.MaterialLieferantMap = value;
                    RaisePropertyChanged("MaterialLieferantMap");
                }
            }
        }


        private IEnumerable einheiten;
        public IEnumerable Einheiten
        { 
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


        private IEnumerable warengruppen;
        public IEnumerable Warengruppen
        { 
            get { return warengruppen; }
            set { SetProperty(ref warengruppen, value); }
        }


        private ObservableCollection<Materialstamm> liste;
        public ObservableCollection<Materialstamm> Liste
        { 
            get { return liste; }
            set { SetProperty(ref liste, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
  

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
   

        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
   

        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }
   

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }
   

        private ICommand assignLfCommand;
        public ICommand AssignLfCommand
        { 
            get { return assignLfCommand; }
            set { SetProperty(ref assignLfCommand, value); }
        }

       
#endregion   

    }
}
