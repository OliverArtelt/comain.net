﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Models.Aufträge
{

    public class Personalleistung : Entity, IDataErrorInfo
    {

        public int                  Auftrag_Id              { get; set; }
        public int                  Personal_Id             { get; set; }
        public String               Kurzbeschreibung        { get; set; }
        public DateTime?            BeginnDatum             { get; set; }
        public DateTime?            EndeDatum               { get; set; }
        public TimeSpan?            BeginnZeit              { get; set; }
        public TimeSpan?            EndeZeit                { get; set; }
        public int?                 Minuten                 { get; set; }
        public int                  Menge                   { get; set; }
        public decimal?             Kosten                  { get; set; }
        public decimal?             Stundensatz             { get; set; }

        public String               AufschlägeAsString      { get; set; }
        public PersonalFilterModel  Personal                { get; set; }


        public String Personalnummer => Personal?.Nummer;


        private ICollection<Aufschlag> aufschläge;
        public ICollection<Aufschlag> Aufschläge
        {
            get {

                if (aufschläge == null) aufschläge = new HashSet<Aufschlag>();
                return aufschläge;
            }
            set { aufschläge = value; }
        }


        public decimal? Stunden { get { return Minuten / 60m; } }


        public decimal? KostenOhneAufschlag
        {

            get {

                if (!Stunden.HasValue || !Stundensatz.HasValue) return null;
                return Stunden.Value * Stundensatz.Value;
            }
        }


        public String ZeitAsString
        {
            get {

                if (!BeginnDatum.HasValue || !BeginnZeit.HasValue || !EndeZeit.HasValue) return null;
                return String.Format("{0:d}: {1:t} - {2:t}", Beginn, Beginn, Ende);
            }
        }


        public DateTime? Beginn
        {
            get { return BeginnDatum + BeginnZeit; }
            set {

                if (!value.HasValue) {

                    BeginnDatum = null;
                    BeginnZeit = null;

                } else {

                    BeginnDatum = value.Value.Date;
                    BeginnZeit = value.Value.TimeOfDay;
                }
            }
        }


        public DateTime? Ende
        {
            get { return EndeDatum + EndeZeit; }
            set {

                if (!value.HasValue) {

                    EndeDatum = null;
                    EndeZeit = null;

                } else {

                    EndeDatum = value.Value.Date;
                    EndeZeit = value.Value.TimeOfDay;
                }
            }
        }


        public int? BerechneteMinuten
        {
            get {

                if (!Beginn.HasValue || !Ende.HasValue) return null;
                if (Ende <= Beginn) return null;

                TimeSpan ts = Ende.Value - Beginn.Value;
                return (int)ts.TotalMinutes;
            }
        }


#region V A L I D A T I O N


        public List<String> ErrorList
        {

            get {

                var errors = new List<String>();
                if (!String.IsNullOrEmpty(this["Personal_Id"])) errors.Add(this["Personal_Id"]);
                if (!String.IsNullOrEmpty(this["BeginnDatum"])) errors.Add(this["BeginnDatum"]);
                if (!String.IsNullOrEmpty(this["BeginnZeit"]))  errors.Add(this["BeginnZeit"]);
                if (!String.IsNullOrEmpty(this["EndeDatum"]))   errors.Add(this["EndeDatum"]);
                if (!String.IsNullOrEmpty(this["EndeZeit"]))    errors.Add(this["EndeZeit"]);
                if (!String.IsNullOrEmpty(this["Minuten"]))     errors.Add(this["Minuten"]);

                return errors;
            }
        }


        public string Error
        {

            get {

                String test = this["Personal_Id"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["BeginnDatum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["BeginnZeit"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["EndeDatum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["EndeZeit"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Minuten"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Personal_Id":

                    if (Personal_Id == 0) return "Geben Sie das Personal an.";
                    break;

                case "BeginnDatum":

                    if (!BeginnDatum.HasValue) return "Geben Sie das Datum des Arbeitsbeginns an.";
                    break;

                case "BeginnZeit":

                    if (!BeginnZeit.HasValue) return "Geben Sie die Zeit des Arbeitsbeginns an.";
                    break;

                case "EndeDatum":

                    if (!EndeDatum.HasValue) return "Geben Sie das Datum des Arbeitsendes an.";
                    if (Beginn.HasValue && Ende.HasValue && Ende <= Beginn) return "Das Arbeitsende muss nach dem Beginn liegen. Es muss mindestens eine Minute gearbeitet worden sein.";
                    break;

                case "EndeZeit":

                    if (!EndeZeit.HasValue) return "Geben Sie die Zeit des Arbeitsendes an.";
                    break;

                case "Minuten":

                    if (!Minuten.HasValue) return "Geben Sie die gearbeiteten Minuten an.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
