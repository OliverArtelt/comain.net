﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// einzelnes Stilattribut
    /// </summary>
    /// <typeparam name="T">Stiltyp</typeparam>
    public class StyleFlyweight<T> where T: ICellStyleType, IEquatable<T>
    {

        /// <summary>
        /// Stilliste mit neuen Stilen
        /// </summary>
        List<T> myStyles; 
        /// <summary>
        /// virtueller Offset im Dokument
        /// </summary>
        int startIndex;


        /// <summary>
        /// virtueller Offset übergeben (Anzahl im Dokument bereits vorhandener Stilknoten)
        /// </summary>
        public StyleFlyweight() : this(0) {}
        
        /// <summary>
        /// virtueller Offset übergeben (Anzahl im Dokument bereits vorhandener Stilknoten)
        /// </summary>
        /// <param name="si">startoffset</param>
        public StyleFlyweight(int si)
        {
            
            startIndex = si;
            myStyles = new List<T>();
        }

        /// <summary>
        /// Fliegengewichtsfabrik
        /// </summary>
        /// <param name="style">zu indizierender Stilknoten</param>
        /// <returns>Index des Stiles in der Liste (bereits existierende Knoten im Dokument berücksichtigen)</returns>
        public int this[T style]
        {
       
            get {
             
                // Index 0 == keine Angabe eines Stiles (== Standardstil)
                if (style == null || style.IsDefault()) return 0;

                int ndx = myStyles.FindIndex(p => p.Equals(style));

                if (ndx == -1) {

                    myStyles.Add(style);
                    return myStyles.Count - 1 + startIndex;

                } else {

                    return ndx + startIndex;
                }
            }
        }

        /// <summary>
        /// einzelnen Knoten am Index ausgeben, wird für Nachtragen des Alignment-Knotens am Stilkombinationsknoten benötigt
        /// </summary>
        /// <param name="index">index im Vektor</param>
        /// <returns>Stil</returns>
        public T this[int index]
        {
        
            get {
            
                return myStyles[index];            
            }
        }
        
        /// <summary>
        /// neue Stilknoten in das Dokument schreiben
        /// </summary>
        /// <param name="w">zu aktualisierendes Dokument</param>
        public void WriteXML(XElement parent) {

            foreach (var entry in myStyles) entry.WriteXML(parent);
        }
    }
}
