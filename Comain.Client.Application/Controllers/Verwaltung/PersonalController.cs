﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IPersonalController : IVwTypController
    {}


    public class PersonalController : SimpleController<PersonalDto, Personal, IPersonalView>, IPersonalController
    {

        private readonly new IPersonalService store;
        private readonly new PersonalViewModel view;
        private readonly PersonalDetailViewModel detailView;
        private readonly PersonalAssetViewModel assetView;
        private TrackableCollection<Personal> pslist;
        private IDokumentAssignController docs;
        private DelegateCommand selectAllAssetsCommand;
        private DelegateCommand deselectAllAssetsCommand;


        public PersonalController(IMessageService messages, IPersonalService store, IControllerBus bus, VerwaltungViewModel mainView,
                                  PersonalViewModel view, PersonalDetailViewModel detailView, PersonalAssetViewModel assetView)
          : base(messages, store, bus, mainView, view)
        {

            this.store = store;
            this.view = view;
            this.detailView = detailView;
            this.assetView = assetView;
        }


        public override void Initialize()
        {

            docs = bus.GetController<IPSDokumentController>();
            base.Initialize();
            view.DetailView = detailView.View;
            view.AssetView = assetView.View;
            view.DokumentView = docs?.View;

            view.SelectAllAssetsCommand = selectAllAssetsCommand = new DelegateCommand(_ => SelectAllAssets(true), _ => view.Current != null && view.Tab == PersonalViewModel.Tabs.Assets);
            view.DeselectAllAssetsCommand = deselectAllAssetsCommand = new DelegateCommand(_ => SelectAllAssets(false), _ => view.Current != null && view.Tab == PersonalViewModel.Tabs.Assets);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            var par = parameter as ControllerMessage;
            if (par != null) {

                if (par.EditMode == EditMode.Append) {

                    var ps = par.Parameter as Personal;
                    if (ps == null) return;
                    await AddAsync(ps);

                } else {

                    if (par.Entity_Id > 0) {

                        view.Current = pslist?.FirstOrDefault(p => p.Id == par.Entity_Id);
                    }
                }
            }

            await docs.RunAsync(parameter);
            UpdateCommands();
        }


        public bool HasChanges => store.HasChanges || !view.Liste.IsNullOrEmpty() && view.Liste.Any(p => p.DocumentsChanged);


        protected override async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var pstask = store.GetAsync(Url, bus.Token);
                var gwtask = store.GewerkeAsync(bus.Token);
                var ihtask = store.MaschinenAsync(bus.Token);

                await Task.WhenAll(pstask, gwtask, ihtask).ConfigureAwait(false);

                pslist = pstask.Result;
                pslist.AcceptChanges();


                await OnUIThreadAsync(() => {

                    view.SetList(pslist);
                    view.Gewerke = gwtask.Result;
                    detailView.Gewerke = gwtask.Result;
                    assetView.Maschinen = ihtask.Result ?? new List<IHObjektFilterModel>();
                });

                await docs.LoadAsync();
            }
        }


        protected override async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                var doclist = view.Liste.Where(p => p.DocumentsChanged).ToList();
                await store.SaveAsync(Url, bus.Token);
                await docs.SaveAsync(doclist);
            }
            await LoadAsync();
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (HasChanges && !await messages.ConfirmExitAsync()) return false;

            store.Reset();
            view.Unload();
            detailView.Unload();
            assetView.Unload();
            await docs.UnloadAsync();

            return true;
        }


        private async Task DetailAsync()
        {

            detailView.Current = view.Current;
            assetView.Current = view.Current;
            if (view.Current != null) await docs.DetailAsync(view.Current);
            docs.SetEditMode(view.Current != null ? EditMode.Edit: EditMode.ReadOnly);
            UpdateCommands();
        }


        protected override async Task AddAsync(Personal model = null)
        {

             await base.AddAsync(model);
             view.ResetTabs();
        }


        protected override async Task DeleteAsync()
        {

             await base.DeleteAsync();
             view.ResetTabs();
        }


        private void SelectAllAssets(bool select) => assetView.Select(select);


        protected override async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                base.ViewPropertyChanged(sender, args);
                if (args.PropertyName == "Current") await DetailAsync();
                if (args.PropertyName == "CurrentTab") UpdateCommands();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private void UpdateCommands()
        {

            OnUIThreadFireAndForget(() => {

                selectAllAssetsCommand.RaiseCanExecuteChanged();
                deselectAllAssetsCommand.RaiseCanExecuteChanged();

            });
        }


        protected override string Url => "/VW/PS";
    }
}
