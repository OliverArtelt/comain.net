﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;
using Comain.Client.Gateways;

namespace Comain.Client.Services.Reports
{

    public class ReportService : IReportService
    {

        private readonly IRepository store;


        public ReportService(IRepository store)
            => this.store = store;


        public Task<IEnumerable<MTListRow>> MTListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<MTListRow>>("/Report/MTList", filter, ct);


        public Task<IHBudgetDto> IHBudgetAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IHBudgetDto>("/Report/IHBudget", filter, ct);


        public Task<IEnumerable<EntwIHBudgetDto>> EntwIHBudgetAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<EntwIHBudgetDto>>("/Report/EntwIHBudget", filter, ct);


        public Task<IEnumerable<EntwPkzDto>> EntwPkzAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<EntwPkzDto>>("/Report/EntwPkz", filter, ct);


        public Task<IHObjektDto> IHObjektAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IHObjektDto>("/Report/IHO", filter, ct);


        public Task<IEnumerable<LAListDto>> LAListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<LAListDto>>("/Report/LA", filter, ct);


        public Task<IEnumerable<SbSuListDto>> SBListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<SbSuListDto>>("/Report/SB", filter, ct);


        public Task<IEnumerable<SbSuListDto>> SUListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<SbSuListDto>>("/Report/SU", filter, ct);


        public Task<StörDto> StörAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<StörDto>("/Report/Stoer", filter, ct);


        public Task<PlanungsgradDto> PGAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<PlanungsgradDto>("/Report/PG", filter, ct);


        public Task<IEnumerable<LieferantDto>> LieferantenAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<LieferantDto>>("/Report/LF", filter, ct);


        public Task<IEnumerable<LbKstDto>> LeistungKstAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<LbKstDto>>("/Report/LbKst", filter, ct);


        public Task<IEnumerable<LbAuDto>> LeistungAuAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<LbAuDto>>("/Report/LbAu", filter, ct);


        public Task<IEnumerable<LbAuDto>> FbListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<LbAuDto>>("/Report/FB", filter, ct);


        public Task<IEnumerable<PSListDto>> PSListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<PSListDto>>("/Report/PSList", filter, ct);


        public Task<IEnumerable<PsDto>> PsAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<PsDto>>("/Report/PS", filter, ct);


        public Task<IEnumerable<PsSollIstDto>> PsSollIstAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<PsSollIstDto>>("/Report/PSSollIst", filter, ct);


        public Task<IEnumerable<StundenDto>> StundenAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<StundenDto>>("/Report/Stunden", filter, ct);


        public Task<IEnumerable<AuListDto>> SimpleAUListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<AuListDto>>("/Report/AUListe", filter, ct);


        public Task<IEnumerable<AuFullListDto>> FullAUListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<AuFullListDto>>("/Report/AUListeFull", filter, ct);


        public Task<IEnumerable<InaktiveIhoDto>> InaktiveIHAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<InaktiveIhoDto>>("/Report/InactiveIH", filter, ct);


        public Task<IEnumerable<Top10Dto>> Top10IHAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<Top10Dto>>("/Report/Top10", filter, ct);


        public Task<IEnumerable<IHListDto>> IHListAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<IHListDto>>("/Report/IHList", filter, ct);


        public Task<IEnumerable<VfGesamtDto>> GesamtverfügbarkeitAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<VfGesamtDto>>("/Report/VfGp", filter, ct);


        public Task<IEnumerable<VfIhoDto>> IhVerfügbarkeitAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<VfIhoDto>>("/Report/VfIh", filter, ct);


        public Task<IEnumerable<AnwesenheitReportDto>> AnwesenheitenAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IEnumerable<AnwesenheitReportDto>>("/RC/Report", filter, ct);
    }
}
