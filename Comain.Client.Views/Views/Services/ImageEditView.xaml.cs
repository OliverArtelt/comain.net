﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace Comain.Client.Views.Services
{

    public partial class ImageEditView : UserControl, IImageEditView
    {

        public ImageEditView()
        {
            InitializeComponent();
        }


        public InkCanvas Canvas => CanvasImpl;
    }
}
