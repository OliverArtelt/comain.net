﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Views.Tickets.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.ViewModels.Tickets.Administration
{

    public class TicketAdminViewModel : ValidatableViewModel<ITicketAdminView, Konfiguration>, IConfigViewModel
    {

        private readonly SynchronizationContext context;
        private Dictionary<int, PersonalFilterModel> psdic;

        public List<Tuple<int, String>> Pflichtpunkte    { get; }


        public TicketAdminViewModel(ITicketAdminView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            Pflichtpunkte = new List<Tuple<int, string>> { new Tuple<int, string>(0, "keine Pflichtangabe"),
                                                           new Tuple<int, string>(1, "bei Auftragsabschluss durch den Auftragsleiter") };
            PsSearchProvider = searcher;
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(TicketSbPflichttyp));
            RaisePropertyChanged(nameof(TicketSuPflichttyp));

            TicketAuftragsleiter = psdic != null && Current != null && Current.TicketAuftragsleiter.HasValue?
                psdic.GetValueOrDefault(Current.TicketAuftragsleiter.Value): null;
        }


        public void Unload()
        {

            PsSearchProvider.ClearItems();
            psdic = null;
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist)
        {

            psdic = pslist.ToDictionary(p => p.Id);
            context.Send(new SendOrPostCallback((o) => {

                PsSearchProvider.SetItems(pslist?.OrderBy(p => p.Name));

            }), null);
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private PersonalFilterModel personal;
        public PersonalFilterModel TicketAuftragsleiter
        {
            get { return personal; }
            set {

                personal = value;
                Current.TicketAuftragsleiter = value?.Id;
                RaisePropertyChanged(nameof(TicketAuftragsleiter));
            }
        }


        public int TicketSbPflichttyp
        {
            get { return Current == null? 0: Current.TicketSbPflichttyp; }
            set {

                if (Current != null && Current.TicketSbPflichttyp != value) {

                    Current.TicketSbPflichttyp = value;
                    RaisePropertyChanged(nameof(TicketSbPflichttyp));
                }
            }
        }


        public int TicketSuPflichttyp
        {
            get { return Current == null? 0: Current.TicketSuPflichttyp; }
            set {

                if (Current != null && Current.TicketSuPflichttyp != value) {

                    Current.TicketSuPflichttyp = value;
                    RaisePropertyChanged(nameof(TicketSuPflichttyp));
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
