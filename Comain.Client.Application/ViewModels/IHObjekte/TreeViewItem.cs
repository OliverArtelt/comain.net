﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.IHObjekte
{

    public abstract class TreeViewItem : NotifyBase
    {

        public TreeViewItem Parent  { get; set; }


        private bool isSelected;
        private bool isExpanded;


        public virtual bool NummerVisible => true;


        public virtual bool IsParentOf(TreeIHViewItem dest)
        {
            return false;
        }


        public bool IsSelected
        {
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }


        public bool IsExpanded
        {
            get { return isExpanded; }
            set { SetProperty(ref isExpanded, value); }
        }
    }
}
