﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Anwesenheiten
{
    public class AnwesenheitMassnahmeDto : ChangeTrackingBase
    {

        public int      Anwesenheit_Id          { get; set; }
        public int      Massnahme_Id            { get; set; }
        public int?     Personalleistung_Id     { get; set; }
        public int?     Auftrag_Id              { get; set; }
        public String   Auftrag                 { get; set; }

        public int      Menge                   { get; set; }
   }
}
