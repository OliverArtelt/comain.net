﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Comain.Client.Models.Dokumente
{

    [DebuggerDisplay("{Typ}")]
    public class Kategorie
    {

        public int          Id               { get; set; }
        public String       Bezeichnung      { get; set; }
        public String       Typ              { get; set; }
    }
}
