﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Views.Dokumente;

namespace Comain.Client.ViewModels.Dokumente
{

    public class DokumentFilterViewModel : ViewModel<IDokumentFilterView>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public DokumentFilterViewModel(IDokumentFilterView view) : base(view)
        {

            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public void Reset()
        {

            SearchText    = null;
            Assigned      = false;
            NotAssigned   = false;
            Kategorien?.ForEach(p => p.IsSelected = false);
        }


        public void Load(IList<Kategorie> katlist, IList<String> taglist)
        {

            Kategorien = katlist.Select(p => new KategorieFilterItem(p)).ToList();
            Schlagworte = taglist;
        }


        public void Unload()
        {

            Kategorien = null;
            Schlagworte = null;
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();

            if (!String.IsNullOrEmpty(SearchText)) filter.Add(nameof(SearchText), SearchText);
            if (Assigned) filter.Add(nameof(Assigned), true);
            if (NotAssigned) filter.Add(nameof(NotAssigned), true);
            filter.Add(nameof(Kategorien), String.Join(",", Kategorien.Where(p => p.IsSelected).Select(p => p.Id)));
            return filter;
        }


        public void ShowResult(IList<Dokument> result)
        {

            if (result.IsNullOrEmpty()) ResultText = "Keine Dokumente gefunden.";
            else if (result.Count == 1) ResultText = "Ein Dokument gefunden.";
            else ResultText = $"{result.Count} Dokumente gefunden.";
        }


#region P R O P E R T I E S


        private IList<KategorieFilterItem> kategorien;
        public IList<KategorieFilterItem> Kategorien
        {
            get { return kategorien; }
            set { SetProperty(ref kategorien, value); }
        }


        private IList<String> schlagworte;
        public IList<String> Schlagworte
        {
            get { return schlagworte; }
            set { SetProperty(ref schlagworte, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        public ICommand ResetCommand    { get; }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool assigned;
        public bool Assigned
        {
            get { return assigned; }
            set { SetProperty(ref assigned, value); }
        }


        private bool notAssigned;
        public bool NotAssigned
        {
            get { return notAssigned; }
            set { SetProperty(ref notAssigned, value); }
        }

#endregion

    }
}
