﻿using System;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan;


namespace Comain.Client.ViewModels.WPlan.Aufträge
{

    public class ChecklistViewItem : ValidatableViewItem<Checklistposition>
    {

        public ChecklistViewItem(Checklistposition model) : base(model)
        {
            if (model == null) throw new ArgumentException("Interner Fehler: es wurde keine Checklistposition verwendet.");
        }


        public bool     BereitsErledigt     => Model.InVorigerSitzungErledigt;
        public bool     IstBearbeitbar      => ChecklisteBearbeitbar;
        public String   BearbeitbarAsColor  => IstBearbeitbar ? "Green": "Coral";
        public bool     PersonalSichtbar    => ErgebnisOk || ErgebnisNotOK;


        private void RaisepositionErgebnis()
        {

            if (ErgebnisOk || ErgebnisNotOK) Model.PrüfePersonal();

            RaisePropertyChanged(nameof(NichtDurchgeführt));
            RaisePropertyChanged(nameof(ErgebnisOk));
            RaisePropertyChanged(nameof(ErgebnisNotOK));
            RaisePropertyChanged(nameof(ErgebnisWirdNichtErledigt));
            RaisePropertyChanged(nameof(ErgebnisFortgeführt));
            RaisePropertyChanged(nameof(Status));
            RaisePropertyChanged(nameof(LeistungBuchbar));
            RaisePropertyChanged(nameof(IstBearbeitbar));
            RaisePropertyChanged(nameof(PersonalSichtbar));
            RaisePropertyChanged(nameof(Personal));
            RaisePropertyChanged(nameof(ZeitGearbeitet));
            RaisePropertyChanged(nameof(NormzeitAlsText));
            RaisePropertyChanged(nameof(MengeGeplant));
            RaisePropertyChanged(nameof(MengeErledigt));
            RaisePropertyChanged(nameof(Normzeit));
            RaisePropertyChanged(nameof(Wert));
        }


        private bool checklisteBearbeitbar;
        public bool ChecklisteBearbeitbar
        {
            get { return checklisteBearbeitbar; }
            set {

                if (SetProperty(ref checklisteBearbeitbar, value)) {

                    RaisePropertyChanged(nameof(IstBearbeitbar));
                    RaisePropertyChanged(nameof(BearbeitbarAsColor));
                    RaisePropertyChanged(nameof(LeistungBuchbar));
                }
            }
        }


        public bool LeistungBuchbar => IstBearbeitbar && (ErgebnisOk || ErgebnisNotOK);


        public String ZeitGearbeitet =>
            (BereitsErledigt && (Model.Ergebnis == Checklistergebnis.OK || Model.Ergebnis == Checklistergebnis.NotOK)) ? ErledigtAm : null;


        public String NormzeitAlsText
        {
            get {

                if (BereitsErledigt) return null;
                if (Model.Ergebnis != Checklistergebnis.OK && Model.Ergebnis != Checklistergebnis.NotOK) return null;
                if (!Model.Normzeit.HasValue) return null;
                if (Model.Normzeit.Value == 1) return "Normzeit: eine Minute";
                return $"Normzeit: {Model.Normzeit.Value} Minuten";
            }
        }


        public void RefreshPersonal()
        {
            RaisePropertyChanged(nameof(Personal));
        }


        public String       Name                   => Model.Name;
        public String       Beschreibung           => Model.Kurzbeschreibung;
        public String       Massnahmenummer        => Model.Massnahmenummer;
        public String       ExterneAuftragsnummer  => Model.ExterneAuftragsnummer;
        public DateTime?    ZuletztAm              => Model.ZuletztAm;
        public DateTime?    NächsterAm             => Model.NächsterAm;
        public int?         Normzeit               => Model.Normzeit;
        public int?         MengeGeplant           => Model.MengeGeplant;
        public decimal?     Wert                   => Model.Wert;


        public String Personal
        {
            get {

                if (Model.Personalliste.IsNullOrEmpty()) return null;

                var pslist = Model.Personalliste.OrderBy(p => p.Name).Select(p => p.Name).Distinct().ToList();
                var psstring = String.Join(System.Environment.NewLine, pslist.Take(5).ToArray());
                if (pslist.Count == 6) psstring += " (und ein weiterer)";
                else if (pslist.Count > 6) psstring += $" (und {pslist.Count - 5} weitere)";

                return psstring;
            }
        }


        public String Status
        {
            get {

                if (ErgebnisOk) return "OK";
                if (ErgebnisNotOK) return $"nicht OK, Auftrag {Model.Folgeauftrag} angelegt";
                if (ErgebnisWirdNichtErledigt) return "wird nicht erledigt";
                if (ErgebnisFortgeführt) return $"wird im Auftrag {Model.Folgeauftrag} fortgeführt";
                return "noch nicht erledigt";
            }
        }


        public String ErledigtAm
        {
            get {

                if (!Model.ErledigtZeitVon.HasValue || !Model.ErledigtZeitBis.HasValue) return null;
                return Model.ErledigtZeitVon.Value.ToString("dd.MM.yy HH:mm") + " - " + Model.ErledigtZeitBis.Value.ToString("HH:mm");
            }
        }


        public DateTime? DurchgeführtAm => Model.DurchgeführtAm;


        public bool NichtDurchgeführt
        {
            get => Model.Ergebnis == Checklistergebnis.Undefiniert;
            set {

                if (value && Model.Ergebnis != Checklistergebnis.Undefiniert) {

                    Model.Ergebnis = Checklistergebnis.Undefiniert;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisOk
        {
            get => Model.Ergebnis == Checklistergebnis.OK;
            set {

                if (value && Model.Ergebnis != Checklistergebnis.OK) {

                    Model.Ergebnis = Checklistergebnis.OK;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisNotOK
        {
            get => Model.Ergebnis == Checklistergebnis.NotOK;
            set {

                if (value && Model.Ergebnis != Checklistergebnis.NotOK) {

                    Model.Ergebnis = Checklistergebnis.NotOK;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisWirdNichtErledigt
        {
            get => Model.Ergebnis == Checklistergebnis.WirdNichtErledigt;
            set {

                if (value && Model.Ergebnis != Checklistergebnis.WirdNichtErledigt) {

                    Model.Ergebnis = Checklistergebnis.WirdNichtErledigt;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisFortgeführt
        {
            get => Model.Ergebnis == Checklistergebnis.Fortgeführt;
            set {

                if (value && Model.Ergebnis != Checklistergebnis.Fortgeführt) {

                    Model.Ergebnis = Checklistergebnis.Fortgeführt;
                    RaisepositionErgebnis();
                }
            }
        }


        public int? MinutenGearbeitet
        {
            get => Model.MinutenGearbeitet;
            set {

                if (value != Model.MinutenGearbeitet) {

                    Model.MinutenGearbeitet = value;
                    RaisePropertyChanged(nameof(MinutenGearbeitet));
                }
            }
        }


        public int? MengeErledigt
        {
            get => Model.MengeErledigt;
            set {

                if (value != Model.MengeErledigt) {

                    Model.MengeErledigt = value;
                    RaisePropertyChanged(nameof(MengeErledigt));
                }
            }
        }
    }
}
