﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{

    public class Lieferschein : Entity, IDataErrorInfo
    {
        
        private String     nummer;         
        private String     notizen;        
        private DateTime   datum;          
        private TrackableCollection<Wareneingang> eingänge;


        public bool HatEinlagerungen { get { return eingänge.Any(p => p.HatEinlagerungen); } }


#region P R O P E R T I E S

        
        public String Nummer          
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public String Notizen         
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }
        

        public DateTime Datum         
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }
        

        public TrackableCollection<Wareneingang> Eingänge  
        {
            get { 
            
                if (eingänge == null) eingänge = new TrackableCollection<Wareneingang>();
                return eingänge;
            }
            set { SetProperty(ref eingänge, value); }
        }
        

#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Eingänge"];
                return test;
            }
        }

       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Eingänge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrEmpty(Nummer)) return "Der Lieferschein muss eine Nummer aufweisen."; 
                    if (Nummer.Length > 32) return "Die Lieferscheinnummer darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "Datum":

                    if (Datum.Year > 2100 || Datum.Year < 1990) return "Geben Sie ein plausibles Eingangsdatum an."; 
                    break;

                case "Eingänge":

                    if (Eingänge.IsNullOrEmpty()) return "Der Lieferschein besitzt keine Wareneingänge."; 
                    break;
                }

                return null;
            }
        }

#endregion    
        
    }
}
