﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Aufträge;

namespace Comain.Client.Views.Aufträge
{

    public partial class AuftragListView:UserControl   ,IAuftragListView
    {

        public AuftragListView()
        {
            InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as AuftragListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
