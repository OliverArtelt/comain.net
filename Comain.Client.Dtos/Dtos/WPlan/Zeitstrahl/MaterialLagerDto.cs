﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class MaterialLagerDto
    {

        public int            Id                { get; set; }

        public String         Name              { get; set; }
        public String         Nummer            { get; set; }
        public String         Einheit           { get; set; }
        public decimal?       MengeVorhanden    { get; set; }
    }
}
