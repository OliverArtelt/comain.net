﻿using System;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Dtos.Tickets
{

    public class TicketprotokollDto 
    {
 
        public int                  Id              { get; set; }
        public DateTime             ErstelltAm      { get; set; }
        public int                  Status          { get; set; }
        public String               Bemerkungen     { get; set; }
        public bool                 IstSauber       { get; set; }
        public PersonalFilterModel  Ersteller       { get; set; }
        public PersonalFilterModel  Zielperson      { get; set; }
    }
}
