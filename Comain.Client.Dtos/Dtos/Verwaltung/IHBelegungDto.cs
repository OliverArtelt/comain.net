﻿using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class IHBelegungDto : ChangeTrackingBase
    {

        public int              IHObjekt_Id             { get; set; }
        public int              Jahr                    { get; set; }
        public short            Monat                   { get; set; }
        public int?             Belegungszeit           { get; set; }
        public int?             Ausfallzeit             { get; set; }
    }
}
