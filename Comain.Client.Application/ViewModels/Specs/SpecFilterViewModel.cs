﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecFilterViewModel : ViewModel<ISpecFilterView>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecFilterViewModel(ISpecFilterView view)
          : base(view)
        {

            ResetCommand = new DelegateCommand(ResetSearch);
            ResetSearch();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();

            filter.Add(nameof(FilterAssets), FilterAssets);
            filter.Add(nameof(FilterArtikel), FilterArtikel);
            filter.Add(nameof(SearchText), SearchText);

            return filter;
        }


        public void ResetSearch()
        {

            FilterAssets = true;
            FilterArtikel = true;
            SearchText = null;
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool filterAssets;
        public bool FilterAssets
        {
            get { return filterAssets; }
            set { SetProperty(ref filterAssets, value); }
        }


        private bool filterArtikel;
        public bool FilterArtikel
        {
            get { return filterArtikel; }
            set { SetProperty(ref filterArtikel, value); }
        }


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        public ICommand ResetCommand { get; private set; }


#endregion

    }
}
