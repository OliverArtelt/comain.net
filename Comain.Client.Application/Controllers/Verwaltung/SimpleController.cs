﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.Trackers;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.Controllers.Verwaltung
{

    public abstract class SimpleController<TDto, TModel, TView> : ControllerBase where TView  : IView
                                                                                 where TDto   : ChangeTrackingBase
                                                                                 where TModel : Entity, IDataErrorInfo, new()
    {

        protected readonly VerwaltungViewModel mainView;
        protected readonly SimpleViewModel<TModel, TView> view;
        protected readonly ISimpleService<TDto, TModel> store;
        protected SerializableCommand deleteCommand;
        protected int insertedId = 0;

        protected abstract String Url { get; }


        public SimpleController(IMessageService messages, ISimpleService<TDto, TModel> store, IControllerBus bus,
                                VerwaltungViewModel mainView, SimpleViewModel<TModel, TView> view)
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
            this.mainView = mainView;
        }


        public virtual void Initialize()
        {

            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => view.Current != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public virtual void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;

            store.Reset();
            view.Unload();
            return true;
        }


        protected virtual async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                await store.SaveAsync(Url, bus.Token);
                await LoadAsync();
            }
        }


        protected virtual async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var oldmodel = view.Current;
                var list = await store.GetAsync(Url, bus.Token);
                insertedId = 0;

                context.Send(new SendOrPostCallback((o) => {

                    view.SetList(list);
                    if (oldmodel != null) view.Current = list.FirstOrDefault(p => p.Id == oldmodel.Id);

                }), null);
            }
        }


        protected virtual async Task AddAsync(TModel model = default)
        {

            if (view.Liste == null) return;

            if (model == default) model = new TModel();
            model.Id = --insertedId;

            await OnUIThreadAsync(() => { 

                view.Liste.Add(model);
                view.Current = model;

            });
        }


        protected virtual async Task DeleteAsync()
        {

            if (view.Liste == null) return;

            await OnUIThreadAsync(() => { 

                var model = view.Current;

                if (model == null) return;
                view.Liste.Remove(model);
                view.Current = null;

            });
        }


        protected virtual void UpdateCommands()
        {
            context.Post(new SendOrPostCallback((o) => { deleteCommand.RaiseCanExecuteChanged(); }), null);
        }


        protected virtual void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current") UpdateCommands();
        }
    }
}
