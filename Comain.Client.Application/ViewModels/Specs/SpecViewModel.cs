﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecViewModel : ViewModel<ISpecView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecViewModel(ISpecView view)
          : base(view)
        {
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private bool showMassnahmeButtons;
        public bool ShowMassnahmeButtons
        {
            get { return showMassnahmeButtons; }
            set { SetProperty(ref showMassnahmeButtons, value); }
        }


        private bool showArtikelButtons;
        public bool ShowArtikelButtons
        {
            get { return showArtikelButtons; }
            set { SetProperty(ref showArtikelButtons, value); }
        }


        private bool showDocButtons;
        public bool ShowDocButtons
        {
            get { return showDocButtons; }
            set { SetProperty(ref showDocButtons, value); }
        }


        private bool showBaseButtons;
        public bool ShowBaseButtons
        {
            get { return showBaseButtons; }
            set { SetProperty(ref showBaseButtons, value); }
        }


        private bool showEditButtons;
        public bool ShowInEditModeButtons
        {
            get { return showEditButtons; }
            set { SetProperty(ref showEditButtons, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand copyCommand;
        public ICommand CopyCommand
        {
            get { return copyCommand; }
            set { SetProperty(ref copyCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand addWertCommand;
        public ICommand AddWertCommand
        {
            get { return addWertCommand; }
            set { SetProperty(ref addWertCommand, value); }
        }


        private ICommand deleteWertCommand;
        public ICommand DeleteWertCommand
        {
            get { return deleteWertCommand; }
            set { SetProperty(ref deleteWertCommand, value); }
        }


        private ICommand addMassnahmeCommand;
        public ICommand AddMassnahmeCommand
        {
            get { return addMassnahmeCommand; }
            set { SetProperty(ref addMassnahmeCommand, value); }
        }


        private ICommand deleteMassnahmeCommand;
        public ICommand DeleteMassnahmeCommand
        {
            get { return deleteMassnahmeCommand; }
            set { SetProperty(ref deleteMassnahmeCommand, value); }
        }


        private ICommand addArtikelCommand;
        public ICommand AddArtikelCommand
        {
            get { return addArtikelCommand; }
            set { SetProperty(ref addArtikelCommand, value); }
        }


        private ICommand removeArtikelCommand;
        public ICommand RemoveArtikelCommand
        {
            get { return removeArtikelCommand; }
            set { SetProperty(ref removeArtikelCommand, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private object listView;
        public object ListView
        {
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
