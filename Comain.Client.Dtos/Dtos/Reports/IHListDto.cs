﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Reports
{

    public class IHListDto
    {

        public int      IHObjekt_Id             { get; set; }
        public String   IHObjekt                { get; set; }
        public String   Maschine                { get; set; }
        public String   Standort                { get; set; }
        public String   Kostenstelle            { get; set; }
        public String   Inventarnummer          { get; set; }
        public String   Bemerkung               { get; set; }
        public short?   Baujahr                 { get; set; }
        public short?   Anschaffjahr            { get; set; }
        public String   Fabriknummer            { get; set; }
        public String   Hersteller              { get; set; }
        public String   Lieferant               { get; set; }
        public String   Instandhalter           { get; set; }
        public String   Typ                     { get; set; }
        public String   TechnischeDaten         { get; set; }
        public String   IHStatus                { get; set; }
        public String   Erhaltklasse            { get; set; }
        public String   VerantwIH               { get; set; }
        public String   VerantwProd             { get; set; }

        public List<IHListDetailDto> Details    { get; set; }
    }
}
