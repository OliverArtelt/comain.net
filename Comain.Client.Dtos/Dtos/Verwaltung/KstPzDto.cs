﻿using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class KstPzDto
    {

        public List<KstDto>     Kostenstellen   { get; set; }
        public List<PzDto>      Standorte       { get; set; }
        public List<KstPzMap>   Map             { get; set; }
    }
}
