﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.ReportModels.WPlan
{
    public class ErsatzteilbedarfRow
    {

        public ErsatzteilbedarfRow()
        {}


        public ErsatzteilbedarfRow(PlanMaterial model)
        {

            Id            = model.Id;
            MaterialId    = model.MaterialId.Value;
            Name          = model.Name;
            Nummer        = model.Nummer;
            MengeBenötigt = model.MengeBenötigt;
            IstFehlend    = model.IstFehlend;
            FälligAm      = model.Parent.FälligAm.Value;
            Auftrag       = model.Parent.Nummer;
            Massnahme     = model.Parent.Massnahme;
            IHObjekt      = model.Parent.IHObjekt;
            Maschine      = model.Parent.Maschine;  
        }


        public int          Id             { get; set; }
        public int          MaterialId     { get; set; }
        public String       Name           { get; set; }
        public String       Nummer         { get; set; }

        public decimal      MengeBenötigt  { get; set; }
        public bool         IstFehlend     { get; set; }
        public String       FehlendText    => IstFehlend? "fehlt": "vorhanden";
        public DateTime     FälligAm       { get; set; }

        public String       Auftrag        { get; set; }
        public String       Massnahme      { get; set; }
        public String       IHObjekt       { get; set; }
        public String       Maschine       { get; set; }
    }
}
