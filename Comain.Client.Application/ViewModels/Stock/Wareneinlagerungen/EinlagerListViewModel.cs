﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{
    
    public class EinlagerListViewModel : ViewModel<IEinlagerListView>
    {
 

        public EinlagerListViewModel(IEinlagerListView view) : base(view)
        {
        }


        public void Unload()
        {
            FoundList = null;
        }


#region P R O P E R T I E S
 

        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }
 
        
        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private Wareneinlagerung current;
        public Wareneinlagerung Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


        private ObservableCollection<Wareneinlagerung> foundlist;
        public ObservableCollection<Wareneinlagerung> FoundList
        {
            get { return foundlist; }   
            set { SetProperty(ref foundlist, value); }
        }


#endregion

    }
}
