﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using FeserWard.Controls;


namespace Comain.Client.ViewModels.Specs
{

    public interface ISpecSearchProvider : IIntelliboxResultsProvider
    {

        void SetItems(IEnumerable<SpecFilterModel> speclist);
        void ClearItems();
        void AddModel(SpecFilterModel model);
    }


    public class SpecSearchProvider : ISpecSearchProvider
    {

        private ICollection<SpecFilterModel> speclist;


        public void SetItems(IEnumerable<SpecFilterModel> speclist) => this.speclist = speclist?.ToList();


        public void AddModel(SpecFilterModel model) => speclist.Add(model);


        public void ClearItems() => speclist = null;


        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (speclist == null) yield break;

            int count = 0;
            foreach (var sp in speclist) {

                if (String.IsNullOrWhiteSpace(searchTerm) ||
                    sp.Baugruppenname.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    sp.Bauteilname.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) {

                    count++;
                    if (count > maxResults) yield break;

                    yield return sp;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (speclist.IsNullOrEmpty()) yield break;
            foreach (var sp in speclist) yield return sp;
        }
    }
}
