﻿using System;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Models.WPlan;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Views.WPlan.Vorgaben;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailViewModel : ValidatableViewModel<IVorgabeDetailView, WiMassnahme>, IDelayableViewModel
    {

        public enum Tabs { Basis, Auftrag, Media, Memo, Docs, Material } 


        private readonly SynchronizationContext context;
 

        public VorgabeDetailViewModel(IVorgabeDetailView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
        }
  

#region P R O P E R T I E S


        private IHObjekt iHObjekt;
        public IHObjekt IHObjekt
        { 
            get { return iHObjekt; }
            set { if (SetProperty(ref iHObjekt, value)) RaisePropertyChanged(nameof(Header1)); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public Tabs Tab
        { 
            get { return (Tabs)currentTab; }
            set { CurrentTab = (int)value; }
        }


        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }


        public String Header1 => IHObjekt == null? null: $"Maßnahmekatalog für {IHObjekt.Nummer}: {IHObjekt.Name}";


        private object baseView;
        public object BaseView
        { 
            get { return baseView; }
            set { SetProperty(ref baseView, value); }
        }


        private object memoView;
        public object MemoView
        { 
            get { return memoView; }
            set { SetProperty(ref memoView, value); }
        }


        private object materialView;
        public object MaterialView
        { 
            get { return materialView; }
            set { SetProperty(ref materialView, value); }
        }


        private object mediaView;
        public object MediaView
        { 
            get { return mediaView; }
            set { SetProperty(ref mediaView, value); }
        }


        private object messungView;
        public object MessungView
        { 
            get { return messungView; }
            set { SetProperty(ref messungView, value); }
        }


        private object auftragView;
        public object AuftragView
        { 
            get { return auftragView; }
            set { SetProperty(ref auftragView, value); }
        }


        private object dokumentView;
        public object DokumentView
        { 
            get { return dokumentView; }
            set { if (SetProperty(ref dokumentView, value)) RaisePropertyChanged(nameof(DokumentVisible)); }
        }


        public bool DokumentVisible => DokumentView != null;


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
