﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels.Stock;
using Comain.Client.Services.Stock.Aufträge;
using Comain.Client.ViewModels.Stock.Aufträge;
using Comain.Client.ViewModels;
using Comain.Factories;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Controllers.Stock.Aufträge
{

    public class MaterialController : ControllerBase, IMaterialController
    {

        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IMaterialService store;
        private readonly IContainerFactory<IBackgroundPrinter> printer;
        private readonly MaterialViewModel view;
        private Auftrag  currentAu;
        private Materialleistung currentMt;
        private EditMode currentAuMode;
        private EditMode currentMode;
        private LeMtParameter currentContext;
        private IModulService modules;


        public MaterialController(IFlyoutService flyouts, IMessageService messages, ILoginService login, IMaterialService store, MaterialViewModel view,
                                  IModulService modules, IContainerFactory<IBackgroundPrinter> printer, IControllerBus bus)
          : base(messages, bus)
        {

            this.flyouts = flyouts;
            this.store = store;
            this.view = view;
            this.login = login;
            this.printer = printer;
            this.modules = modules;
        }


        public void Initialize()
        {

            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = CreateCommand(_ => DeleteAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync(false, false));
            view.SperrenCommand = CreateCommand(_ => SaveAsync(true, false));
            view.EntsperrenCommand = CreateCommand(_ => SaveAsync(false, true));
            view.PrintCommand = CreateCommand(_ => PrintAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            if (login.AktuellerNutzer.IstProduktion) throw new ComainOperationException("Sie haben hierfür nicht die notwendige Berechtigung.");
            currentContext = (LeMtParameter)parameter;
            if (currentContext.EditMode == EditMode.ReadOnly && currentContext.Material == null) return;
            currentAu = currentContext.Auftrag;
            if (currentContext.Material == null) currentMt = NeueMT();
            else currentMt = await store.DetailAsync(currentContext.Material.Id, bus.Token).ConfigureAwait(false);
            currentAuMode = currentContext.EditMode;

            await modules.LoadAsync(bus.Token).ConfigureAwait(false);

            view.SetzeModel(currentAu, currentMt);
            view.UseLager = modules.Info.VerwendetModulStock;

            currentMode = EditMode.ReadOnly;
            String header = "Material / Subleistung einsehen";
            if (currentMt != null && currentMt.Versiegelt) header = "Versiegelte Material / Subleistung";

            if (currentAu.StatusBearbeitbar) {

                if (currentMt != null && !currentMt.IstAngefügt && !currentMt.Versiegelt && currentAuMode == EditMode.Edit &&
                    (!currentMt.DatumIstPlausibel || !currentAu.Konfiguration.Abrechnungsschluß.HasValue || currentAu.Konfiguration.Abrechnungsschluß < currentMt.Datum)) {

                    currentMode = EditMode.Edit;
                    header = "Material / Subleistung bearbeiten";
                }

                if (currentMt == null || currentMt.IstAngefügt && !currentAu.KostenstelleVerlegt && currentAuMode == EditMode.Edit) {

                    currentMode = EditMode.Append;
                    header = "Material / Subleistung anfügen";
                }
            }

            view.EditEnabled = currentMode != EditMode.ReadOnly && !currentMt.Versiegelt;
            view.Header = header;

            view.ClearMessage();

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = view.View, Header = "Material / Subleistung einsehen", Position = MahApps.Metro.Controls.Position.Right });

            view.ChecklistPositionen = null;
            var wtController = bus.GetController<IChecklistPluginController>();
            if (wtController != null) {

                view.ChecklistPositionen = await wtController.MTPositionAsync(currentMt.Id).ConfigureAwait(false);
            }
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (currentMode != EditMode.ReadOnly && currentMt != null && currentMt.IsChanged && !await messages.ConfirmExitAsync()) return false;
            view.Unload();
            return true;
        }


        public void Shutdown()
        {
        }


        public async Task LoadAsync()
        {

            var ehTask = store.FilterEinheitenAsync(bus.Token);
            var mtTask = store.FilterMaterialstammAsync(bus.Token);
            var lfTask = store.FilterLieferantenAsync(bus.Token);
            var psTask = store.FilterPersonalAsync(bus.Token);
            var wlTask = store.FilterEinlagerungenAsync(bus.Token);

            await Task.WhenAll(ehTask, mtTask, lfTask, psTask, wlTask).ConfigureAwait(false);
            view.SetzeListen(ehTask.Result, mtTask.Result, lfTask.Result, psTask.Result, wlTask.Result);
        }


        private async Task SaveAsync(bool sperren, bool entsperren)
        {

            if (currentMode == EditMode.ReadOnly && !entsperren) return;

            if (currentMt.ErrorList.Count > 0) {

                view.SetMessage(currentMt.ErrorList);
                return;

            } else {

                view.ClearMessage();
            }

            try {

                using (new VisualDelay(view)) {

                    if (sperren) currentMt.Versiegelt = true;
                    if (entsperren) currentMt.Versiegelt = false;

                    currentMt = await store.SaveAsync(currentMt, bus.Token);
                    view.SetzeModel(currentAu, currentMt);
                    view.EditEnabled = currentMode != EditMode.ReadOnly && !currentMt.Versiegelt;

                    var old = currentAu.Materialleistungen.FirstOrDefault(p => p.Id == currentMt.Id);
                    if (old != null) currentAu.Materialleistungen.Remove(old);
                    currentAu.Materialleistungen.Add(currentMt);
                    if (currentContext.Update != null) currentContext.Update();
                }
            }
            catch (Exception x) {

                view.SetMessage(x);
            }
        }


        private async Task AddAsync()
        {

            currentMt = NeueMT();
            view.SetzeModel(currentAu, currentMt);
            view.Header = "Materialleistung anfügen";
            currentMode = EditMode.Append;
            view.EditEnabled = true;
            view.ClearMessage();
        }


        private async Task DeleteAsync()
        {

            if (currentMode == EditMode.ReadOnly) return;

            using (new VisualDelay(view)) {

                await store.DeleteAsync(currentMt, bus.Token);
                if (!currentMt.IstAngefügt) currentAu.Materialleistungen.Remove(currentMt);
                currentMt = null;
                currentAu = null;
            }

            await flyouts.CloseAsync();
            if (currentContext.Update != null) currentContext.Update();
        }


        private async Task PrintAsync()
        {

            view.ClearMessage();

            if (!currentMt.Lagerung_Id.HasValue || !currentMt.Nutzer_Id.HasValue) {

                var errors = new List<String>();
                if (!currentMt.Lagerung_Id.HasValue) errors.Add("Es wurde kein Lagerort angegeben");
                if (!currentMt.Nutzer_Id.HasValue) errors.Add("Es wurde kein Abholer angegeben");
                view.SetMessage(errors, "Der Entnahmeschein kann nicht gedruckt werden.");
                return;
            }

            var address = "Comain.Client.Reports.Stock.Entnahmeschein.rdlc";
            var reportmodel = await CreateReportModelAsync();
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = "Entnahmeschein " + currentMt.Entnahmeschein };
            report.AddDataSource("MainData", new List<Entnahmeschein> { reportmodel });

            using (var print = printer.Create()) {

                print.Value.Print(report);
            }

            currentMt.Entnahmeschein = reportmodel.Nummer;
            view.SetMessage(null, "Das Dokument wurde gedruckt.");
        }


        private Materialleistung NeueMT()
        {

            if (currentAu == null) return null;
            var mt = new Materialleistung { Auftrag_Id = currentAu.Id, Datum = DateTime.Today, Menge = 1, Handlingszuschlag = currentAu.Konfiguration.HandlingszuschlagMaterial };
            mt.Nutzer_Id = login.AktuellerNutzer.Personal_Id;

            return mt;
        }


        private async Task<Entnahmeschein> CreateReportModelAsync()
        {

            var es = String.Format("{0}_{1}", currentAu.Nummer, currentMt.Nummer);
            if (es.Length > 32) es = es.Substring(0, 32);
            currentMt.Entnahmeschein = es;
            var wl = view.Lagerungen.First(p => p.Wareneinlagerung_Id == currentMt.Lagerung_Id.Value);
            var ps = view.Abholer;

            var result = new Entnahmeschein();
            result.Nummer             = currentMt.Entnahmeschein;
            result.Datum              = currentMt.Datum.ToShortDateString();
            result.Artikelbezeichnung = currentMt.Name;
            result.Artikelnummer      = currentMt.Nummer;
            result.Auftragsnummer     = currentAu.Nummer;
            result.Kostenstelle       = currentAu.AuIHKS.Kostenstelle;
            result.IHObjekt           = currentAu.AuIHKS.IHObjekt;
            result.Maschine           = currentAu.AuIHKS.Maschine;
            result.Besteller          = ps.Nachname + ", " + ps.Vorname + " [" + ps.Nummer + "]";
            result.Lagerort           = wl.Lagerort;
            result.Entnahmemenge      = currentMt.Menge;
            result.Fachrestmenge      = await store.FachRestmengeAsync(wl.Lagerort_Id, currentMt.Materialstamm_Id.Value, bus.Token);
            result.Notizen            = currentMt.Beschreibung;

            return result;
        }
    }
}
