﻿using System;


namespace Comain.Client.Dtos.Auth
{

    public class PasswordDto
    {

        public String   UserId          { get; set; }
        public String   OldPassword     { get; set; }
        public String   NewPassword     { get; set; }
    }
}
