﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.Anwesenheiten
{

    public class AssignedCopyRequest : AssignedPSRequest
    {

        public DateTime    Von                      { get; set; }
        public DateTime    Bis                      { get; set; }
        public int         Weekdays                 { get; set; }
        public bool        SkipWhenAlreadyPlanned   { get; set; }
    }
}
