﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Reports
{

    public class AuftragDruckDto
    {

        public int          AuftragId           { get; set; }
        public String       Nummer              { get; set; }
        public DateTime?    Termin              { get; set; }
        public String       IHObjektNummer      { get; set; }
        public String       IHObjektPfad        { get; set; }
        public String       Leistungsart        { get; set; }
        public String       Gewerk              { get; set; }
        public String       Abnahmeberechtigter { get; set; }
        public String       Auftragsleiter      { get; set; }


        public IEnumerable<PositionDruckDto>    Positionen      { get; set; }
    }
}
