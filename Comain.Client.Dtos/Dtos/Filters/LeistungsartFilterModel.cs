﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class LeistungsartFilterModel : EntityModelBase, IComparable<LeistungsartFilterModel>
    {

        public String          Nummer                   { get; set; }
        public String          Name                     { get; set; }
        public bool            IstProdKennziffer        { get; set; }
        public bool            IstWI                    { get; set; }
        public bool            IstPlankosten            { get; set; }
        public bool            IstUnPlankosten          { get; set; }
        public bool            IstPräventation          { get; set; }
        public bool            IstSachkundigenprüfung   { get; set; }


        public int CompareTo(LeistungsartFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            if (this.Nummer == null && other.Nummer == null) return 0;
            if (this.Nummer == null) return -1;
            
            return this.Nummer.CompareNatural(other.Nummer);
        }
    }
}
