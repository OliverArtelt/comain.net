﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Waf.Applications;
using System.Windows.Input;


namespace Comain.Client.ViewModels
{

    public class ValidatableViewModel<TView, TModel> : ViewModel<TView>, IDataErrorInfo where TView : IView
                                                                                        where TModel : INotifyPropertyChanged, IDataErrorInfo
    {

        public ValidatableViewModel(TView view) : base(view)
        {
        }


        protected virtual void ModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(args.PropertyName);
        }


        protected TModel current;
        public virtual TModel Current
        {
            get { return current; }
            set {

                var oldModel = current;

                if (SetProperty(ref current, value)) {

                    if (oldModel != null) PropertyChangedEventManager.RemoveHandler(oldModel, ModelPropertyChanged, "");
                    if (current != null)  PropertyChangedEventManager.AddHandler(current, ModelPropertyChanged, "");

                    RaisePropertyChanged("EditEnabled");
                    CurrentChanged(oldModel);
                }
            }
        }


        public virtual bool EditEnabled { get { return Current != null; } }


        public virtual void CurrentChanged(TModel oldSelected)
        {}


        public virtual string Error
        {

            get {

                if (Current == null) return String.Empty;
                return Current.Error;
            }
        }


        public virtual String this[string columnName]
        {

            get {

                if (Current == null) return String.Empty;
                return Current[columnName];
            }
        }
    }
}
