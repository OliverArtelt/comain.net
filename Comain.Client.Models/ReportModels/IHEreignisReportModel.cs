﻿using System.Diagnostics;
using System;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.ReportModels
{

    [DebuggerDisplay("{ZeitpunktAsString} {Ereignis}")]
    public class IHEreignisReportModel
    {

        /// <summary>
        /// zum Gruppieren von Einträgen desselben Ereignisses
        /// </summary>
        public int EreignisId               { get; set; }
        /// <summary>
        /// falls sich Eregnis auf eine Baugruppen bezieht, diese hier angeben
        /// </summary>
        public int IHId                     { get; set; }
        /// <summary>
        /// falls sich Eregnis auf eine Baugruppen bezieht, diese hier angeben
        /// </summary>
        public String IHObjekt              { get; set; }
        /// <remarks>
        /// falls sich Eregnis auf eine Baugruppen bezieht, diese hier angeben
        /// </remarks>
        public String Maschine              { get; set; }
        /// <remarks>
        /// Zeitangabe für das Sortieren der Ereignisse
        /// </remarks>
        public DateTime Zeitpunkt           { get; set; }
        /// <remarks>
        /// wie der Zeitpunkt angezeigt werden soll (mit / ohne Uhrzeit)
        /// </remarks>
        public String ZeitpunktAsString     { get; set; }
        public String Ereignis              { get; set; }
        public String Ereignistext          { get; set; }
        public String Detailname            { get; set; }
        public String Detailwert            { get; set; }


        public IHEreignisReportModel(IHEreignisDto dto, int ereignisId)
        {

            EreignisId        = ereignisId;
            IHId              = dto.IHId;             
            IHObjekt          = dto.IHObjekt;         
            Maschine          = dto.Maschine;         
            Zeitpunkt         = dto.Zeitpunkt;        
            ZeitpunktAsString = dto.ZeitpunktAsString;
            Ereignis          = dto.Ereignis;         
            Ereignistext      = dto.Ereignistext;     
        }


        public IHEreignisReportModel(IHEreignisDto dto, int ereignisId, String detailname, String detailwert) : this(dto, ereignisId)
        {

            Detailname        = detailname;             
            Detailwert        = detailwert;         
        }
    }
}
