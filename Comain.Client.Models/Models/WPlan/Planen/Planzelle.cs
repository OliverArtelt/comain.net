﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Erledigung einer Maßnahme zu einem Kalenderzeitpunkt (Woche o.ä.)
    /// </summary>
    /// <remarks>
    /// Warum eine Aufzählung der Aufträge? Durch Drag'n'Drop (= dynamische Terminplanung) können mehrere Positionen in derselben Woche landen
    /// </remarks>
    [DebuggerDisplay("{Begin}: {Ergebniskurztext}")]
    public class Planzelle : NotifyBase, IAcceptor
    {

        private Planspalte period;


        public Planzelle(MNPlanzeile parent, Planspalte period)
        {

            Parent = parent;
            this.period = period;
            Aufgaben = new ObservableCollection<Planaufgabe>();
        }


        public Plan                 Plan                    => Parent.Plan;
        public MNPlanzeile          Parent                  { get; }
        public DateTime             Begin                   => period.Begin;
        public DateTime             End                     => period.BeginNext;

        public bool                 IstBearbeitbar          => !Parent.IstDeaktiviert && period.IstBearbeitbar && (!Parent.IstBedarfsposition || period.IstAktuell || period.IstVergangenheit); //Bedarfspositionen lassen sich nicht planen
        public bool                 IstZukunft              => period.IstZukunft;
        public bool                 IstVergangenheit        => period.IstVergangenheit;
        public bool                 IstAktuell              => period.IstAktuell;
        public bool                 IstSichtbar             => Aufgaben.Any(p => p.IstSichtbar);
        public bool                 IstUnveränderlicheKette => Parent.Termintyp == Aufträge.Termintyp.UnveränderlicheKette;
        public int                  AnzahlAufgaben          => Aufgaben == null? 0: Aufgaben.Count(p => p.IstSichtbar);

        public bool                 ErgebnisOffenErlaubt    => IstBearbeitbar && !Parent.IstBedarfsposition && (period.IstZukunft || period.IstAktuell);
        public bool                 ErgebnisOkNokErlaubt    => IstBearbeitbar && (IstVergangenheit || IstAktuell);
        public bool                 ErgebnisWneErlaubt      => IstBearbeitbar;
        public String               Ergebniskurztext        => AnzahlAufgaben == 0? String.Empty: AnzahlAufgaben < 1? $"{AnzahlAufgaben} Positionen": Aufgaben.First().Ergebniskurztext;
        public Checklistergebnis    Ergebnis                => AnzahlAufgaben != 1? Checklistergebnis.Undefiniert: Aufgaben.First().Ergebnis;
        public decimal?             Stunden                 => Aufgaben?.Where(p => p.IstSichtbar).Sum(p => p.MinutenAngezeigt.GetValueOrDefault()) / 60m;
        public decimal?             Kosten                  => Aufgaben?.Where(p => p.IstSichtbar).Sum(p => p.KostenAngezeigt);
        public int?                 Menge                   => Aufgaben?.Where(p => p.IstSichtbar).Sum(p => p.Menge ?? 1);

        public double               Height                  => Parent.Height;
        public double               Width                   => Plan.Spaltenbreite;
        public double               Left                    => period.Index * Width;
        public double               Top                     => Parent.YStartposition;
        public Brush                TypAsFarbe              => Parent.TypAsFarbe;


        public Plananzeigemodus     Anzeigemodus    { get; set; }


        public ObservableCollection<Planaufgabe> Aufgaben   { get; }


        public bool ImExportSichtbar(PlanOptions opts) => Aufgaben.Any(p => p.ImExportSichtbar(opts));


        public void NotifyPositionChanged()
        {

            RaisePropertyChanged(nameof(Height));
            RaisePropertyChanged(nameof(Width));
            RaisePropertyChanged(nameof(Left));
            RaisePropertyChanged(nameof(Top));
            RaisePropertyChanged(nameof(TypAsFarbe));
        }


        public void AddPosition(Planaufgabe aufgabe)
        {

            aufgabe.Anzeigemodus = Anzeigemodus;
            Aufgaben.Add(aufgabe);
        }


        public void RemovePosition(Planaufgabe aufgabe)
        {
            Aufgaben.Remove(aufgabe);
        }


        public Planaufgabe CreatePosition(EditAction action)
        {

            if (action.Ergebnis == Checklistergebnis.WirdNichtErledigt)
                throw new ComainOperationException("Sonderleistungen und Bedarfspositionen dürfen nicht den Status 'Wird nicht erledigt' erhalten.");
            if (Parent.IstBedarfsposition && period.IstZukunft)
                throw new ComainOperationException("Bedarfspositionen können nicht in der Zukunft geplant werden. Sie dürfen nur Erledigungen in der Vergangenheit oder in der aktuellen Zeit eintragen.");
            if (Parent.IstBedarfsposition && (action.Ergebnis == Checklistergebnis.Undefiniert))
                throw new ComainOperationException("Bedarfspositionen dürfen nur mit den Status OK, Nicht OK oder Fortgeführt rückgemeldet werden.");
            if (period.IstVergangenheit && (action.Ergebnis == Checklistergebnis.Undefiniert))
                throw new ComainOperationException("Sie dürfen nicht in der Vergangenheit planen. Es können nur Erledigungen rückgemeldet werden.");
            if (period.IstZukunft && action.Ergebnis != Checklistergebnis.Undefiniert)
                throw new ComainOperationException("Sie dürfen nicht in der Zukunft rückmelden. Es darf nur geplant werden.");

            var dto = new PositionDto {

                Position_Id    = --Plan.NeueAufgabeId,
                Massnahme_Id   = Parent.Id,
                Auftragsnummer = action.Ergebnis == Checklistergebnis.Undefiniert? "neu": Parent.IstBedarfsposition? "Bedarfsposition": "Sonderleistung",
                Wert           = Parent.Wert,
                Ergebnis       = (int)action.Ergebnis
            };


            if (action.Ergebnis == Checklistergebnis.Undefiniert) {

                dto.Normzeit   = Parent.Normzeit;
                dto.Menge      = action.Menge ?? Parent.Menge ?? 1;
                dto.FälligAm   = period.Begin;

            } else {

                dto.MinutenGearbeitet = Parent.Normzeit;
                dto.MengeGearbeitet   = action.Menge ?? Parent.Menge ?? 1;
                dto.DurchgeführtAm    = period.Begin;
            }


            var aufgabe = new Planaufgabe(this, dto);
            aufgabe.IstSichtbar = true;
            return aufgabe;
        }


        public void Accept(IVisitor v)
        {
            v.Visit(this);
            Aufgaben.ForEach(p => p.Accept(v));
        }
    }
}
