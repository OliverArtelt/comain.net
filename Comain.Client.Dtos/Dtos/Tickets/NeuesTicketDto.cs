﻿using System;


namespace Comain.Client.Dtos.Tickets
{

    public class NeuesTicketDto
    {
        
        public int       AuIHKS              { get; set; }
        public DateTime? GemeldetAm          { get; set; }
        public String    Kurzbeschreibung    { get; set; }
        public int       Dringlichkeit_Id    { get; set; }
        public int?      Gewerk_Id           { get; set; }
        public int?      Personal_Id         { get; set; }
        public String    Anmerkung           { get; set; }
    }
}
