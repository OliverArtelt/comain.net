﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHDeactivateViewModel : ViewModel<IIHDeactivateView>
    {
        
        public IHDeactivateViewModel(IIHDeactivateView view) : base(view)
        {}


        private IHObjekt ihObjekt;
        public IHObjekt IHObjekt
        { 
            get { return ihObjekt; }
            set { 
            
                if (SetProperty(ref ihObjekt, value)) {

                    RaisePropertyChanged("Nummer");
                    RaisePropertyChanged("Name");
                }
            }
        }  


#region P R O P E R T I E S
  
        
        public String Nummer
        { 
            get { return IHObjekt == null? null: IHObjekt.Nummer; }
        }  
  
        
        public String Name
        { 
            get { return IHObjekt == null? null: IHObjekt.Name; }
        }  
  
        
        private DateTime datum;
        public DateTime Datum
        { 
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }  
  
        
        private ICommand deactivateCommand;
        public ICommand DeactivateCommand
        { 
            get { return deactivateCommand; }
            set { SetProperty(ref deactivateCommand, value); }
        }  


#endregion


    }
}
