﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class IHObjektDto
    {

        public int            Id                    { get; set; }
        public String         Nummer                { get; set; }
        public String         Inventarnummer        { get; set; }
        public String         Pfad                  { get; set; }
        public bool           IstInaktiv            { get; set; }

        public List<MassnahmeDto>   Massnahmen      { get; set; }
    }
}
