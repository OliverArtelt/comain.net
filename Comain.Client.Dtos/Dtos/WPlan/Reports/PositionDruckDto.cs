﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Reports
{
   
    public class PositionDruckDto
    {

        public int          PositionId          { get; set; }
        public String       Name                { get; set; }
        public String       Kurzbeschreibung    { get; set; }
        public int          Ergebnis            { get; set; }
        public DateTime?    ErledigtDatum       { get; set; }
        public TimeSpan?    ErledigtZeitVon     { get; set; }
        public TimeSpan?    ErledigtZeitBis     { get; set; }

        public String       Werkzeuge           { get; set; }
        public String       Durchführung        { get; set; }
        public String       Sicherheit          { get; set; }


        public IEnumerable<MaterialDruckDto>    Materialien     { get; set; }
        public IEnumerable<BildDruckDto>        Bilder          { get; set; }
    }
}
