﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class TafelViewModel : ViewModel<ITafelView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        const int scrollbarHeight = 14;


        public TafelViewModel(ITafelView view, IMessageService messages) : base(view)
        {

            context = SynchronizationContext.Current;
            ClosePopupCommand = new DelegateCommand(_ => { PopupIsOpen = false; });
            FillMengeCommand = new DelegateCommand(p => { EditMenge = Int32.Parse(p.ToString()); });
            messages.OverlayRaisedEvent += (o, e) => { if (PopupIsOpen) ClosePopupCommand.Execute(null); };
            ScrollHomeCommand = new DelegateCommand(_ => view.ScrollToLeftTop());
        }


        public async Task LoadAsync(Plan plan)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                using (new VisualDelay(this)) {

                    PopupIsOpen = false;
                    Spalten = plan.Spalten;
                    Zeilen = plan.Zeilen;
                    ViewCore.Build(plan);
                    StatusInfo = plan?.StatusInfo;
                    Spaltenbreite = plan.Spaltenbreite;
                    DarfPersonalZuordnen = plan.DarfPersonalZuordnen;
                    DarfRückmelden = plan.DarfRückmelden;
                    PlanHeight = plan.TotalHeight;
                    PlanWidth = plan.TotalWidth;
                }
            }), null);
        }


        public void Unload()
        {
            Zeilen = null;
            Spalten = null;
        }


        public async Task UnloadPlan()
        {

            DetailsOpen = false;
            PopupIsOpen = false;
            await ViewCore.UnloadAsync();
            Zeilen = null;
        }


        public void OpenPopup(Planzelle zelle, Planaufgabe pos)
        {

            // neu positionieren
            if (PopupIsOpen) PopupIsOpen = false;
            if (zelle == null) return;

            if (pos == null) {

                if (zelle.Parent.IstBedarfsposition) PopupTitle = "Bedarfsposition";
                else if (zelle.IstZukunft) PopupTitle = "Neue Position";
                else PopupTitle = "Sonderleistung";

            } else {

                var sp = zelle.Parent.Plan.SelektiertePositionen.Count();
                if (sp == 0) return;
                if (sp == 1) PopupTitle = "Eine Position";
                else PopupTitle = $"{sp} Positionen";
            }

            PopupIsOpen = true;
        }


        public void ScrollToYPosition(double value)
            => ViewCore.ScrollToYPosition(value);


#region P R O P E R T I E S


        public ICommand ScrollHomeCommand { get; }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private bool darfRückmelden;
        public bool DarfRückmelden
        {
            get { return darfRückmelden; }
            set { SetProperty(ref darfRückmelden, value); }
        }


        private bool darfPersonalZuordnen;
        public bool DarfPersonalZuordnen
        {
            get { return darfPersonalZuordnen; }
            set { SetProperty(ref darfPersonalZuordnen, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        public ICommand ClosePopupCommand   { get; }
        public ICommand FillMengeCommand    { get; }


        private int? editMenge;
        public int? EditMenge
        {
            get { return editMenge; }
            set { SetProperty(ref editMenge, value); }
        }


        private int spaltenbreite;
        public int Spaltenbreite
        {
            get { return spaltenbreite; }
            set { if (SetProperty(ref spaltenbreite, value)) ViewCore.SpaltenbreitenAnpassen(); }
        }


        private double planHeight;
        public double PlanHeight
        {
            get { return planHeight; }
            set { if (SetProperty(ref planHeight, value)) RaisePropertyChanged(nameof(PlanHeightWithOffset)); }
        }


        public double PlanHeightWithOffset => PlanHeight + scrollbarHeight;


        private double planWidth;
        public double PlanWidth
        {
            get { return planWidth; }
            set { SetProperty(ref planWidth, value); }
        }


        private bool popupIsOpen;
        public bool PopupIsOpen
        {
            get { return popupIsOpen; }
            set { SetProperty(ref popupIsOpen, value); }
        }


        private bool ergebnisOffenErlaubt;
        public bool ErgebnisOffenErlaubt
        {
            get { return ergebnisOffenErlaubt; }
            set { if (SetProperty(ref ergebnisOffenErlaubt, value)) RaisePropertyChanged(nameof(ErgebnismengeErlaubt)); }
        }


        private bool ergebnisOkNokErlaubt;
        public bool ErgebnisOkNokErlaubt
        {
            get { return ergebnisOkNokErlaubt; }
            set { if (SetProperty(ref ergebnisOkNokErlaubt, value)) RaisePropertyChanged(nameof(ErgebnismengeErlaubt)); }
        }


        private bool ergebnisWneErlaubt;
        public bool ErgebnisWneErlaubt
        {
            get { return ergebnisWneErlaubt; }
            set { SetProperty(ref ergebnisWneErlaubt, value); }
        }


        public bool ErgebnismengeErlaubt => ErgebnisOkNokErlaubt || ErgebnisOffenErlaubt;


        private String statusInfo;
        public String StatusInfo
        {
            get { return statusInfo; }
            set { SetProperty(ref statusInfo, value); }
        }


        private String popupTitle;
        public String PopupTitle
        {
            get { return popupTitle; }
            set { SetProperty(ref popupTitle, value); }
        }


        private List<Planspalte> spalten;
        public List<Planspalte> Spalten
        {
            get { return spalten; }
            set { SetProperty(ref spalten, value); }
        }


        private List<IPlanzeile> zeilen;
        public List<IPlanzeile> Zeilen
        {
            get { return zeilen; }
            set { SetProperty(ref zeilen, value); }
        }


        private double zoom = 1d;
        public double Zoom
        {
            get { return zoom; }
            set { SetProperty(ref zoom, value); }
        }


        private ICommand selectCommand;
        public ICommand SelectCommand
        {
            get { return selectCommand; }
            set { SetProperty(ref selectCommand, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand dragCommand;
        public ICommand DragCommand
        {
            get { return dragCommand; }
            set { SetProperty(ref dragCommand, value); }
        }


        private ICommand undoCommand;
        public ICommand UndoCommand
        {
            get { return undoCommand; }
            set { SetProperty(ref undoCommand, value); }
        }


        private ICommand redoCommand;
        public ICommand RedoCommand
        {
            get { return redoCommand; }
            set { SetProperty(ref redoCommand, value); }
        }


        private ICommand popupCommand;
        public ICommand PopupCommand
        {
            get { return popupCommand; }
            set { SetProperty(ref popupCommand, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private bool detailsOpen;
        public bool DetailsOpen
        {
            get { return detailsOpen; }
            set { SetProperty(ref detailsOpen, value); }
        }


#endregion

    }
}
