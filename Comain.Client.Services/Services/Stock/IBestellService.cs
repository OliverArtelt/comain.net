﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Bestellungen
{

    public interface IBestellService
    {

        Task<IList<Lieferant>>              LieferantenAsync(CancellationToken ct = default);
        Task<IList<Artikel>>                ArtikelAsync(CancellationToken ct = default);
        Task<IList<BestellListDto>>         ListeAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<BestellInfoReportDto>>   BestellInfoAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<BestellReportDto>>       BestellReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>    BearbeiterAsync(CancellationToken ct = default);
        Task<Konfiguration>                 KonfigAsync(CancellationToken ct = default);
        /// <summary>
        /// Livetile-Support
        /// </summary>
        Task<int>                           AnzahlBSArtikelAsync(CancellationToken ct = default);

        Task<Bestellung>                    DetailAsync(int id, CancellationToken ct = default);

        Task<Bestellung>                    UpdateAsync(Bestellung model, CancellationToken ct = default);
        Task                                DeleteAsync(int id, CancellationToken ct = default);
        Task<Bestellung>                    OrderAsync(Bestellung model, CancellationToken ct = default);
        Task<Bestellung>                    StornoAsync(int id, CancellationToken ct = default);
        Task<Bestellung>                    CloseAsync(int id, CancellationToken ct = default);
    }
}
