﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Administration
{

    public interface IIHReactivateService
    {

        Task<IList<IHReactivateItem>> GetAsync(CancellationToken ct = default);
        Task                          SetAsync(IEnumerable<int> list, CancellationToken ct = default);
    }
}
