﻿using System;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHPzDto
    {

        public int          Id                  { get; set; } 
        public DateTime?    Datum               { get; set; } 
        public String       Nummer              { get; set; }
        public String       Inventarnummer      { get; set; }
        public String       Standortnummer      { get; set; }
    }
}
