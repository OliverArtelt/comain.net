﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public abstract class SubListController : ControllerBase
    {

        private readonly VerwaltungViewModel mainView;
        private readonly SubListViewModel view;
        private readonly ISubListService store;
        private int idstamp;
        private ObservableCollection<NummerItem> mainlist;
        private ObservableCollection<SubNummerItem> sublist;
        private SerializableCommand deleteMainCommand;
        private SerializableCommand deleteSubCommand;
        private SerializableCommand addSubCommand;


        public abstract String Url           { get; }
        public abstract String Itemname      { get; }
        public abstract String SubItemname   { get; }


        public SubListController(IMessageService messages, ISubListService store, IControllerBus bus,
                                 VerwaltungViewModel mainView, SubListViewModel view)
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
            this.mainView = mainView;
        }


        public void Initialize()
        {

            view.AddMainCommand = CreateCommand(_ => AddMainAsync());
            view.DeleteMainCommand = deleteMainCommand = CreateCommand(_ => DeleteMainAsync(), _ => view.Mainitem != null);
            view.AddSubCommand = addSubCommand = CreateCommand(_ => AddSubAsync(), _ => view.Mainitem != null);
            view.DeleteSubCommand = deleteSubCommand = CreateCommand(_ => DeleteSubAsync(), _ => view.Subitem != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            view.Itemname = Itemname;
            view.SubItemname = SubItemname;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;

            store.Reset();
            view.Unload();
            return true;
        }


        private async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                await store.SaveAsync(Url, bus.Token);
                await LoadAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var oldmodel = view.Mainitem;

                var data = await store.GetAsync(Url, bus.Token);
                mainlist = data.Item1;
                sublist = data.Item2;

                context.Send(new SendOrPostCallback((o) => {

                    view.SetLists(mainlist, sublist);
                    if (oldmodel != null) view.Mainitem = mainlist.FirstOrDefault(p => p.Id == oldmodel.Id);

                }), null);

                idstamp = 0;
            }
        }


        private async Task AddMainAsync()
        {

            if (mainlist == null) return;
            idstamp -= 1;

            await OnUIThreadAsync(() => {

                var model = new NummerItem { Id = idstamp };
                mainlist.Add(model);
                view.Mainitem = model;
            });
        }


        private async Task DeleteMainAsync()
        {

            if (mainlist == null) return;

            await OnUIThreadAsync(() => {

                if (view.Mainitem == null) return;
                mainlist.Remove(view.Mainitem);
                view.Mainitem = null;
            });
        }


        private async Task AddSubAsync()
        {

            if (sublist == null) return;
            idstamp -= 1;

            await OnUIThreadAsync(() => {

                if (view.Mainitem == null) return;
                var model = new SubNummerItem { Id = idstamp };
                model.Haupt_Id = view.Mainitem.Id;
                sublist.Add(model);
                view.Subitem = model;
            });
        }


        private async Task DeleteSubAsync()
        {

            if (sublist == null) return;

            await OnUIThreadAsync(() => {

                if (view.Subitem == null) return;
                sublist.Remove(view.Subitem);
                view.Subitem = null;
            });
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => {

                deleteMainCommand.RaiseCanExecuteChanged();
                deleteSubCommand.RaiseCanExecuteChanged();
                addSubCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current" || args.PropertyName == "Mainitem" || args.PropertyName == "Subitem") UpdateCommands();
        }
    }
}
