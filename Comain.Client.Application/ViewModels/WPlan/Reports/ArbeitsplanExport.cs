﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class ArbeitsplanExport : IPlanExport
    {

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Arbeitsplan";
        public int      Sortierung              => 700;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => true;
        public bool     UseFilterErgebnis       => true;

        public IExportVisitor Exporter      => new ArbeitsplanWriter();


        public String   Text                => @"
Verwenden Sie den Arbeitsplan für eine Übersicht der zu erledigenden Maßnahmen in einem Kalenderlayout. Der Arbeitsplan zeigt die Maßnahmen pro Asset mit der jeweiligen Menge pro Tag oder pro Woche, je nachdem welchen Plan Sie ausgeben möchten.
Wählen Sie dazu die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein und legen Sie die Berichtsparameter fest.
Exportieren Sie den Arbeitsplan als Wochen- oder als Tagesplan nach Excel.";
    }
}
