﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class VFStep1ViewModel : ViewModel<IVFStep1View>
    {

        public VFStep1ViewModel(IVFStep1View view) : base(view)
        {
            ResetSearch();
        }


        private void ResetSearch()
        {

            Jahr = DateTime.Now.Year;
            Monat = DateTime.Now.Month - 1;
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S

        
        public List<int> Jahre
        {
            get { return Enumerable.Range(DateTime.Now.Year - 2, 5).ToList(); }
        }

        
        public List<String> Monate
        {
            get { return new List<String> { "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" }; }
        }


        private int jahr;
        public int Jahr
        { 
            get { return jahr; }
            set { SetProperty(ref jahr, value); }
        }


        private int monat;
        public int Monat
        { 
            get { return monat; }
            set { SetProperty(ref monat, value); }
        }


        private ICommand stepCommand;
        public ICommand StepCommand
        { 
            get { return stepCommand; }
            set { SetProperty(ref stepCommand, value); }
        }

 
#endregion

    }
}
