﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;


namespace Comain.Client.Gateways
{

    public class TokenResponse
    {

        public class AccessToken
        {

            public String       access_token        { get; set; }
            public String       id_token            { get; set; }
        }


        public String       token_type  => "Bearer";

        public String       contentType         { get; set; }
        public String       serializerSettings  { get; set; }
        public String       statusCode          { get; set; }
        public AccessToken  value               { get; set; }


        private AuthenticationHeaderValue header;
        public AuthenticationHeaderValue Header
        {
            get {

                if (header == null) header = new AuthenticationHeaderValue(token_type, value?.access_token);
                return header;
            }
        }
    }
}
