﻿using System;
using System.Collections;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneingänge;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{
    
    public class EingangDetailViewModel : ValidatableViewModel<IEingangDetailView, Lieferschein>, IDelayableViewModel
    {

        public EingangDetailViewModel(IEingangDetailView view) : base(view)
        {}


        public override void CurrentChanged(Lieferschein oldSelected)
        {
           
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Datum");
            RaisePropertyChanged("Notizen");
            RaisePropertyChanged("Eingänge");
            RaisePropertyChanged("CurrentWE");
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
                 

        private ICommand addPosCommand;
        public ICommand AddPosCommand
        { 
            get { return addPosCommand; }
            set { SetProperty(ref addPosCommand, value); }
        }
                 
        
        public DateTime? Datum
        { 
            get { return Current == null? (DateTime?)null: Current.Datum; }
            set { 
            
                if (current != null && current.Datum != value && value.HasValue) {

                    current.Datum = value.Value;
                    RaisePropertyChanged("Datum");
                }
            }
        }
                 
        
        public String Nummer
        { 
            get { return Current == null? null: Current.Nummer; }
            set { 
            
                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }
                 
        
        public String Notizen
        { 
            get { return Current == null? null: Current.Notizen; }
            set { 
            
                if (current != null && current.Notizen != value) {

                    current.Notizen = value;
                    RaisePropertyChanged("Notizen");
                }
            }
        }
                 
        
        public IEnumerable Eingänge
        { 
            get { return Current == null? null: Current.Eingänge; }
        }
                 
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private Wareneingang currentWE;
        public Wareneingang CurrentWE
        { 
            get { return currentWE; }
            set { SetProperty(ref currentWE, value); }
        }
 

#endregion

    }
}
