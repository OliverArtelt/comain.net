﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecListViewModel : ViewModel<ISpecListView>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecListViewModel(ISpecListView view)
          : base(view)
        {
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ObservableCollection<SpecListItem> foundlist;
        public ObservableCollection<SpecListItem> Foundlist
        {
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private SpecListItem current;
        public SpecListItem Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
