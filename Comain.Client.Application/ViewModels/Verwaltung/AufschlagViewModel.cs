﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{
    
    public class AufschlagViewModel : SimpleViewModel<Aufschlagtyp, IAufschlagView>, IDelayableViewModel
    {

        public AufschlagViewModel(IAufschlagView view) : base(view)
        {}


        public override void CurrentChanged(Aufschlagtyp oldSelected)
        {
            
            RaisePropertyChanged("NameKurz");                  
            RaisePropertyChanged("NameLang");               
            RaisePropertyChanged("Bemerkung");              
            RaisePropertyChanged("IstStandard");                  
            RaisePropertyChanged("Sonntags");                   
            RaisePropertyChanged("Samstags");                   
            RaisePropertyChanged("Feiertags");           
            RaisePropertyChanged("UhrzeitVon");                    
            RaisePropertyChanged("UhrzeitBis");               
            RaisePropertyChanged("Prozent");    
        }


#region P R O P E R T I E S


        public String NameKurz                 
        {
            get { return current == null? null: current.NameKurz; }
            set { 
            
                if (current != null && current.NameKurz != value) {

                    current.NameKurz = value;
                    RaisePropertyChanged("NameKurz");
                }
            }
        }
        

        public String NameLang              
        {
            get { return current == null? null: current.NameLang; }
            set { 
            
                if (current != null && current.NameLang != value) {

                    current.NameLang = value;
                    RaisePropertyChanged("NameLang");
                }
            }
        }
        

        public String Bemerkung              
        {
            get { return current == null? null: current.Bemerkung; }
            set { 
            
                if (current != null && current.Bemerkung != value) {

                    current.Bemerkung = value;
                    RaisePropertyChanged("Bemerkung");
                }
            }
        }
        

        public bool? IstStandard  
        {
            get { return current == null? null: current.IstStandard; }
            set { 
            
                if (current != null && current.IstStandard != value) {

                    current.IstStandard = value;
                    RaisePropertyChanged("IstStandard");
                }
            }
        }
        

        public bool? Sonntags  
        {
            get { return current == null? null: current.Sonntags; }
            set { 
            
                if (current != null && current.Sonntags != value) {

                    current.Sonntags = value;
                    RaisePropertyChanged("Sonntags");
                }
            }
        }
        

        public bool? Samstags  
        {
            get { return current == null? null: current.Samstags; }
            set { 
            
                if (current != null && current.Samstags != value) {

                    current.Samstags = value;
                    RaisePropertyChanged("Samstags");
                }
            }
        }
        

        public bool? Feiertags  
        {
            get { return current == null? null: current.Feiertags; }
            set { 
            
                if (current != null && current.Feiertags != value) {

                    current.Feiertags = value;
                    RaisePropertyChanged("Feiertags");
                }
            }
        }

        
        public TimeSpan? UhrzeitVon  
        {
            get { return current == null? null: current.UhrzeitVon; }
            set { 
            
                if (current != null && current.UhrzeitVon != value) {

                    current.UhrzeitVon = value;
                    RaisePropertyChanged("UhrzeitVon");
                }
            }
        }

        
        public TimeSpan? UhrzeitBis  
        {
            get { return current == null? null: current.UhrzeitBis; }
            set { 
            
                if (current != null && current.UhrzeitBis != value) {

                    current.UhrzeitBis = value;
                    RaisePropertyChanged("UhrzeitBis");
                }
            }
        }

        
        public decimal? Prozent  
        {
            get { return current == null? null: current.Prozent; }
            set { 
            
                if (current != null && current.Prozent != value) {

                    current.Prozent = value;
                    RaisePropertyChanged("Prozent");
                }
            }
        }
        

#endregion

    }
}
