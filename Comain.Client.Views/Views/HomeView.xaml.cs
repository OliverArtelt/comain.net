﻿using System.Windows.Controls;
using System.Windows.Input;

namespace Comain.Client.Views
{

    public partial class HomeView : UserControl, IHomeView
    {

        public HomeView()
        {
            InitializeComponent();
        }


        private void PanoramaScroller(object sender, MouseWheelEventArgs e)
        {
          
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToHorizontalOffset(scv.HorizontalOffset - e.Delta);
            e.Handled = true;
        }
    }
}
