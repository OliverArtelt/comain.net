﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{
    
    public class Top10ViewModel : ViewModel<ITop10View>, IFilterViewModel
    {

        public Top10ViewModel(ITop10View view) : base(view)
        {}


        public void Reset()
        {
            
            TypAnzahl = true;
            FBAusserhalb = false;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            String st = "nach " + typ;
            if (FBAusserhalb) st += ", FB-Aufträge außerhalb des Budgets berücksichtigen";
            return st;
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("Top10Typ", typ);
            if (FBAusserhalb) filter.Add("FBAusserhalb", true);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);            
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private bool fBAusserhalb;
        public bool FBAusserhalb
        { 
            get { return fBAusserhalb; }
            set { SetProperty(ref fBAusserhalb, value); }
        } 
   

        private String typ = "Störanzahl";
        
        public bool TypAnzahl
        { 
            get { return typ == "Störanzahl"; }
            set { 
            
                if (value && typ != "Störanzahl") {
                    
                    typ = "Störanzahl";
                    RaisePropertyChanged("TypAnzahl");
                    RaisePropertyChanged("TypKosten");
                    RaisePropertyChanged("TypZeit");
                }
            }
        } 
        
        public bool TypKosten
        { 
            get { return typ == "Störfallkosten"; }
            set { 
            
                if (value && typ != "Störfallkosten") {
                    
                    typ = "Störfallkosten";
                    RaisePropertyChanged("TypAnzahl");
                    RaisePropertyChanged("TypKosten");
                    RaisePropertyChanged("TypZeit");
                }
            }
        } 
        
        public bool TypZeit
        { 
            get { return typ == "Störzeiten"; }
            set { 
            
                if (value && typ != "Störzeiten") {
                    
                    typ = "Störzeiten";
                    RaisePropertyChanged("TypAnzahl");
                    RaisePropertyChanged("TypKosten");
                    RaisePropertyChanged("TypZeit");
                }
            }
        } 
   

#endregion
    }
}
