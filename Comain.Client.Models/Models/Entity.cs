﻿namespace Comain.Client.Models
{

    public abstract class Entity : NotifyBase
    {

        public int  Id          { get; set; }
        public bool IstAngefügt => Id <= 0;


        public static bool Equals<T>(T lhs, T rhs) where T: Entity
        {

            if (lhs == null && rhs == null) return true;
            else if (lhs == null | rhs == null) return false;
            else return lhs.Id == rhs.Id;
        }
    }
}
