﻿using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;

namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{

    public class BestellabschlussViewModel : ViewModel<IBestellabschlussView>
    {
 
        public BestellabschlussViewModel(IBestellabschlussView view) : base(view)
        {
        }


        public void Unload()
        {
            Bestellungen = null;
        }


#region P R O P E R T I E S
 

        private IEnumerable<BestellListDto> bestellungen;
        public IEnumerable<BestellListDto> Bestellungen
        { 
            get { return bestellungen; }
            set { SetProperty(ref bestellungen, value); }
        }
 

        private ICommand executeCommand;
        public ICommand ExecuteCommand
        { 
            get { return executeCommand; }
            set { SetProperty(ref executeCommand, value); }
        }


#endregion

    }
}
