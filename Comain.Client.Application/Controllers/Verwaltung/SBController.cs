﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.Controllers.Verwaltung
{

    public interface ISBController : IVwTypController 
    {}


    public class SBController : SubListController, ISBController
    {

        public SBController(IMessageService messages, ISubListService store, IControllerBus bus,
                            VerwaltungViewModel mainView, SubListViewModel view) 
          : base(messages, store, bus, mainView, view)
        {}


        public override string Url          => @"/VW/SB";
        public override string Itemname     => @"Schadensbild";
        public override string SubItemname  => @"Subschadensbild";
    }
}
