﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ReportModels
{
    
    public class GraphValue
    {

        public String   Name    { get; set; }
        public decimal? Value   { get; set; }
    }
}
