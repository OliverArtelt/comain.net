﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using FeserWard.Controls;


namespace Comain.Client.ViewModels.Aufträge
{

    public interface IAuftragSearchProvider : IIntelliboxResultsProvider
    {

        IHListDto     IHObjekt        { get; set; }

        void SetItems(IEnumerable<AuftragListDto> aulist);
        void ClearItems();
        void AddModel(AuftragListDto model);
    }


    public class AuftragSearchProvider : IAuftragSearchProvider
    {

        private ICollection<AuftragListDto> aufträge;

        public IHListDto     IHObjekt        { get; set; }


        public void SetItems(IEnumerable<AuftragListDto> aulist) => aufträge = aulist?.ToList();


        public void AddModel(AuftragListDto model) => aufträge.Add(model);


        public void ClearItems() => aufträge = null;


        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (aufträge == null) yield break;

            int count = 0;
            foreach (var au in aufträge) {

                //stornierte, umgezogene Aufträge beachten
                if (IHObjekt != null && !String.IsNullOrEmpty(au.IHObjektNummer) && !au.IHObjektNummer.StartsWith(IHObjekt.Nummer)) continue;

                if (String.IsNullOrWhiteSpace(searchTerm) ||
                    au.Nummer != null && au.Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    au.Kurzbeschreibung != null && au.Kurzbeschreibung.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    au.ExterneNummer != null && au.ExterneNummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) {

                    count++;
                    if (count > maxResults) yield break;

                    yield return au;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (aufträge.IsNullOrEmpty()) yield break;
            foreach (var ih in aufträge) yield return ih;
        }
    }
}
