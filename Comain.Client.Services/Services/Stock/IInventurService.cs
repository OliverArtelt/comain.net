﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Stock;


namespace Comain.Client.Services.Stock.Inventuren
{

    public interface IInventurService
    {

        Task<IList<InventurDto>> ReadAsync(CancellationToken ct = default);
        Task SaveAsync(IList<InventurDto> changes, CancellationToken ct = default);
    }
}
