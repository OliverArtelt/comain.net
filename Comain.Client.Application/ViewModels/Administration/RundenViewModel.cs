﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views.Administration;


namespace Comain.Client.ViewModels.Administration
{

    public class RundenViewModel : ValidatableViewModel<IRundenView, Konfiguration>, IConfigViewModel
    {

        public RundenViewModel(IRundenView view) : base(view)
        {
        }
 

        public override void CurrentChanged(Konfiguration oldSelected)
        {
            
            RaisePropertyChanged("Runden0");
            RaisePropertyChanged("Runden1");
            RaisePropertyChanged("Runden2");
            RaisePropertyChanged("Runden3");
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public bool Runden0
        { 
            get { return Current == null? false: Current.Rundungstyp.GetValueOrDefault() == 0; }
            set { 
                
                if (Current != null && value && Current.Rundungstyp.GetValueOrDefault() != 0) {

                    Current.Rundungstyp = 0;
                    RaisePropertyChanged("Runden0");
                    RaisePropertyChanged("Runden1");
                    RaisePropertyChanged("Runden2");
                    RaisePropertyChanged("Runden3");
                }
            }
        }


        public bool Runden3
        { 
            get { return Current == null? false: Current.Rundungstyp.GetValueOrDefault() == 3; }
            set { 
                
                if (Current != null && value && Current.Rundungstyp.GetValueOrDefault() != 3) {

                    Current.Rundungstyp = 3;
                    RaisePropertyChanged("Runden0");
                    RaisePropertyChanged("Runden1");
                    RaisePropertyChanged("Runden2");
                    RaisePropertyChanged("Runden3");
                }
            }
        }


        public bool Runden1
        { 
            get { return Current == null? false: Current.Rundungstyp.GetValueOrDefault() == 1; }
            set { 
                
                if (Current != null && value && Current.Rundungstyp.GetValueOrDefault() != 1) {

                    Current.Rundungstyp = 1;
                    RaisePropertyChanged("Runden0");
                    RaisePropertyChanged("Runden1");
                    RaisePropertyChanged("Runden2");
                    RaisePropertyChanged("Runden3");
                }
            }
        }


        public bool Runden2
        { 
            get { return Current == null? false: Current.Rundungstyp.GetValueOrDefault() == 2; }
            set { 
                
                if (Current != null && value && Current.Rundungstyp.GetValueOrDefault() != 2) {

                    Current.Rundungstyp = 2;
                    RaisePropertyChanged("Runden0");
                    RaisePropertyChanged("Runden1");
                    RaisePropertyChanged("Runden2");
                    RaisePropertyChanged("Runden3");
                }
            }
        }
 

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion
    
    }
}
