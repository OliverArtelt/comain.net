﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class LSFilterViewModel : ViewModel<ILSFilterView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public LSFilterViewModel(IFilterService store, ILSFilterView view) : base(view)
        {
            this.store = store;
        }


        public void Reset()
        {
            Lieferschein = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (Lieferschein != null) return "Lieferschein " + Lieferschein.Nummer;
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (Lieferschein != null) filter.Add("Lieferschein", Lieferschein.Id);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            RaisePropertyChanged("Lieferscheine");
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   
    
        public IList<NummerFilterModel> Lieferscheine
        { 
            get { return store == null? null: store.Lieferscheine; }
        } 
   

        private NummerFilterModel lieferschein;
        public NummerFilterModel Lieferschein
        { 
            get { return lieferschein; }
            set { SetProperty(ref lieferschein, value); }
        } 
     

#endregion

    }
}
