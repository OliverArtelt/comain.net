﻿using System;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Schadensursachen : IVwTyp
    {

        public string       Title           => "Schadensursachen";
        public int          Sortierung      => 1400;
        public Type         Controller      => typeof(ISUController);


        public bool HatZugriff(Nutzer nutzer)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }
    }
}
