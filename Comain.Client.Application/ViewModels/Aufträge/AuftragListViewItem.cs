﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragListViewItem : NotifyBase
    {

        private String      nummer;
        private String      typ;
        private String      kurzbeschreibung;
        private DateTime    auftragsdatum;
        private String      iHObjektNummer;
        private String      maschine;
        private String      auftraggeber;
        private String      status;
        private String      dringlichkeit;
        private DateTime?   termin;
        private String      externeNummer;
        private String      kostenstelle;
        private String      gewerk;
        private decimal?    stunden;


        public int Id       { get; set; }
        public int Projekt  { get; set; }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        public String Typ
        {
            get { return typ; }
            set { SetProperty(ref typ, value); }
        }

        public String Kurzbeschreibung
        {
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }

        public DateTime Auftragsdatum
        {
            get { return auftragsdatum; }
            set { SetProperty(ref auftragsdatum, value); }
        }

        public String IHObjektNummer
        {
            get { return iHObjektNummer; }
            set { SetProperty(ref iHObjektNummer, value); }
        }

        public String Maschine
        {
            get { return maschine; }
            set { SetProperty(ref maschine, value); }
        }

        public String Auftraggeber
        {
            get { return auftraggeber; }
            set { SetProperty(ref auftraggeber, value); }
        }

        public String Status
        {
            get { return status; }
            set { if (SetProperty(ref status, value)) RaisePropertyChanged("Color"); }
        }

        public String Dringlichkeit
        {
            get { return dringlichkeit; }
            set { SetProperty(ref dringlichkeit, value); }
        }

        public DateTime? Termin
        {
            get { return termin; }
            set { SetProperty(ref termin, value); }
        }

        public String ExterneNummer
        {
            get { return externeNummer; }
            set { SetProperty(ref externeNummer, value); }
        }

        public String Kostenstelle
        {
            get { return kostenstelle; }
            set { SetProperty(ref kostenstelle, value); }
        }

        public String Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }

        public decimal? Stunden
        {
            get { return stunden; }
            set { SetProperty(ref stunden, value); }
        }


        public String Color
        {
            get {

                if (Status == "Offen")          return "DodgerBlue";
                if (Status == "Periodisch")     return "MediumPurple";
                if (Status == "Überfällig")     return "Coral";
                if (Status == "Übergeben")      return "Gold";
                if (Status == "Abgeschlossen")  return "LimeGreen";
                if (Status == "Storniert")      return "LightSlateGray";
                return "White";
            }
        }


        private Auftrag model;
        public Auftrag Model
        {
            get { return model; }
            set {

                if (value != model) {

                    if (model != null) PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    model = value;
                    if (model != null) PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                    Emboss();
                }
            }
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case "Nummer":

                Nummer = model.Nummer;
                RaisePropertyChanged("Nummer");
                break;

            case "Kurzbeschreibung":

                Kurzbeschreibung = model.Kurzbeschreibung.RemoveNewlines();
                RaisePropertyChanged("Kurzbeschreibung");
                break;

            case "StartTermin":
            case "ErstelltAm":

                Auftragsdatum = model.ErstelltAm;
                RaisePropertyChanged("Auftragsdatum");
                break;

            case "AuIHKS":

                IHObjektNummer = model.AuIHKS.IHObjekt;
                RaisePropertyChanged("IHObjektNummer");
                Maschine = model.AuIHKS.Maschine;
                RaisePropertyChanged("Maschine");
                Kostenstelle = model.AuIHKS.Kostenstelle;
                RaisePropertyChanged("Kostenstelle");
                break;

            case "Dringlichkeit":

                Dringlichkeit = model.Dringlichkeit?.Nummer;
                RaisePropertyChanged("Dringlichkeit");
                break;

            case "Status":

                Status = model.StatusAsString;
                RaisePropertyChanged("Status");
                RaisePropertyChanged("Color");
                break;

            case "ExterneNummer":

                ExterneNummer = model.ExterneNummer;
                RaisePropertyChanged("ExterneNummer");
                break;

            case "Stunden":

                Stunden = model.Stunden;
                RaisePropertyChanged("Stunden");
                break;

            case "Auftraggeber":

                Auftraggeber = model.Auftraggeber == null? null: model.Auftraggeber.Nachname + ", " + model.Auftraggeber.Vorname + " [" + model.Auftraggeber.Nummer + "]";
                RaisePropertyChanged("Auftraggeber");
                break;

            case "Gewerk":

                Gewerk = model.Gewerk == null? null: model.Gewerk.Name;
                RaisePropertyChanged("Gewerk");
                break;
            }
        }


        public AuftragListViewItem()
        {}


        public AuftragListViewItem(Auftrag model)
        {
            Model = model;
            Emboss();
        }


        public AuftragListViewItem(AuftragListDto dto)
        {

            Id               = dto.Id;
            Nummer           = dto.Nummer;
            Projekt          = dto.Projekt;
            Typ              = ((Auftragstyp)dto.Typ).AsText();
            Kurzbeschreibung = dto.Kurzbeschreibung.RemoveNewlines();
            Auftragsdatum    = dto.Auftragsdatum;
            IHObjektNummer   = dto.IHObjektNummer;
            Maschine         = dto.Maschine;
            Auftraggeber     = dto.Auftraggeber;
            Dringlichkeit    = dto.Dringlichkeit;
            Termin           = dto.Termin;
            ExterneNummer    = dto.ExterneNummer;
            Kostenstelle     = dto.Kostenstelle;
            Status           = dto.Status;
            Stunden          = dto.Stunden;
            Gewerk           = dto.Gewerk;
        }


        public void Emboss()
        {

            Id                  = model.Id;
            Nummer              = model.Nummer;
            Kurzbeschreibung    = model.Kurzbeschreibung.RemoveNewlines();
            Auftragsdatum       = model.ErstelltAm;
            Dringlichkeit       = model.Dringlichkeit?.Nummer;
            Status              = model.StatusAsString;
            ExterneNummer       = model.ExterneNummer;
            Stunden             = model.Stunden;

            Auftraggeber        = model.Auftraggeber != null? model.Auftraggeber.Nachname + ", " + model.Auftraggeber.Vorname + " [" + model.Auftraggeber.Nummer + "]": null;
            Gewerk              = model.Gewerk != null? model.Gewerk.Name: null;

            if (model.AuIHKS != null && IHObjektNummer != model.AuIHKS.IHObjekt) {

                IHObjektNummer = model.AuIHKS.IHObjekt;
                Maschine       = model.AuIHKS.Maschine;
                Kostenstelle   = model.AuIHKS.Kostenstelle;
            }
        }
    }
}
