﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class ReportCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IReportController);
        public String       Name        => "Berichte";
        public String       Parameter   => "Report";
        public String       Gruppe      => "Auswertungen";
        public int          Sortierung  => 2000;
        public String       Color       => "#CC3399";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.page.powerpoint.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstProduktionLeitung || nutzer.IstInstandhaltungInstandhalter ||
                   nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator || nutzer.IstLagerReporting;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
