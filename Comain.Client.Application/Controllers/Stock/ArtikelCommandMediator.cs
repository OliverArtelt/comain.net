﻿using System;
using Comain.Client.Services;
using Comain.Client.ViewModels.Stock.Artikelstamm;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Artikelstamm
{

    public class ArtikelCommandMediator : IArtikelCommandMediator
    {

        private readonly ILoginService login;
        private readonly ArtikelViewModel view;
        private readonly ArtikelDetailViewModel detailView;
        private readonly ArtikelDetailBaseViewModel basisView;
        private readonly ArtikelDetailLfViewModel lfView;
        private readonly ArtikelListViewModel listView;
        private readonly ArtikelFilterViewModel filterView;
        private readonly ArtikelDetailSpecViewModel specView;


        public ArtikelCommandMediator(ILoginService login, ArtikelViewModel view, ArtikelDetailViewModel detailView, ArtikelDetailBaseViewModel basisView,
                                      ArtikelDetailLfViewModel lfView, ArtikelListViewModel listView, ArtikelFilterViewModel filterView,
                                      ArtikelDetailSpecViewModel specView)
        {

            this.login = login;
            this.view = view;
            this.detailView = detailView;
            this.basisView = basisView;
            this.lfView = lfView;
            this.listView = listView;
            this.filterView = filterView;
            this.specView = specView;
        }


        private bool HasCurrent   { get { return detailView.Current != null; } }
        
        public EditMode CurrentMode { get; private set; }


        public void SetEditMode(EditMode newMode)
        {
        
            if (newMode == EditMode.Edit && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diesen Artikel nicht bearbeiten.");
            if (newMode == EditMode.Append && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diesen Artikel nicht bearbeiten.");

            CurrentMode = newMode;
            basisView.EditEnabled   = newMode != EditMode.ReadOnly; 
            specView.EditEnabled    = newMode != EditMode.ReadOnly; 
            lfView.EditEnabled      = newMode != EditMode.ReadOnly; 
            listView.EditEnabled    = newMode == EditMode.ReadOnly; 
            filterView.EditEnabled  = newMode == EditMode.ReadOnly; 

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            view.EditVisible       = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Current != null;
            view.CancelVisible     = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Current != null;
            view.SaveVisible       = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion;
            view.AddVisible        = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis;
            view.DeleteVisible     = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis && detailView.Current != null;
            view.ReloadVisible     = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion;
            view.CopyVisible       = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis && detailView.Current != null;
            view.LieferantVisible  = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Lieferant && detailView.Current != null;
            view.IhObjektVisible   = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis && detailView.Current != null;
            view.DeactivateVisible = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis && detailView.Current != null &&
                                     !detailView.Current.DeaktiviertAm.HasValue;
            view.ReactivateVisible = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == ArtikelDetailViewModel.Tabs.Basis && detailView.Current != null &&
                                     detailView.Current.DeaktiviertAm.HasValue;
        }
    }
}
