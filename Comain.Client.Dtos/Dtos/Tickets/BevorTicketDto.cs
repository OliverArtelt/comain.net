﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos;


namespace Comain.Client.Dtos.Tickets
{

    public class BevorTicketDto 
    {

        public int              IHObjekt                { get; set; }
        public DateTime?        Datum                   { get; set; }
        public String           Kurzbeschreibung        { get; set; }
        public int              Dringlichkeit_Id        { get; set; }
        public int?             Personal_Id             { get; set; }

        public IEnumerable<MediaDto> Medien        { get; set; }
    }
}

