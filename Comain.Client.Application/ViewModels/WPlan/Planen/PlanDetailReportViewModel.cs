﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.WPlan.Reports;
using Comain.Client.Views.WPlan.Planen;

namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanDetailReportViewModel : PrintViewModel<IPlanDetailReportView>
    {

        public PlanDetailReportViewModel(IMessageService messages, IPlanDetailReportView view, IEnumerable<IPlanOutput> reports) 
          : base(messages, view)
        {

            Reports = reports.OrderBy(p => p.Sortierung).ToList();
            SelectReportCommand = new DelegateCommand(report => { CurrentReport = report as IPlanOutput; GoToFilter(); });
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        private void Reset()
        {

            Stunden           = false;
            Preise            = false;
            Gruppensummen     = false;
            Planleistungen    = true;
            Sonderleistungen  = true;
            Bedarfspositionen = true;
            Erledigt          = true;
            Offen             = true;
            NurRelevante      = false;
        }


        public PlanOptions GebeOptions()
            => new PlanOptions {

                    Stunden           = Stunden,
                    Preise            = Preise,
                    Gruppensummen     = Gruppensummen,
                                      
                    Planleistungen    = Planleistungen,
                    Sonderleistungen  = Sonderleistungen,
                    Bedarfspositionen = Bedarfspositionen,
                    
                    Erledigt         = Erledigt,
                    Offen            = Offen,
                    NurRelevante     = NurRelevante
                };


        private void NotifyPropertyChanged()
        {

            RaisePropertyChanged(nameof(UseFilterLeistungstyp));
            RaisePropertyChanged(nameof(UseFilterAusgabe));
            RaisePropertyChanged(nameof(UseFilterErgebnis));
            RaisePropertyChanged(nameof(NameReport));
            RaisePropertyChanged(nameof(TextReport));
            RaisePropertyChanged(nameof(FilterVisible));
            RaisePropertyChanged(nameof(UseFilterPanel));
        }


        public void GoToReport() => GoToImpl(true);


        public void GoToFilter() => GoToImpl(false);


        private void GoToImpl(bool viewerVisible)
        {

            ViewerVisible = viewerVisible;
            RaisePropertyChanged(nameof(FilterVisible));
        }


#region P R O P E R T I E S


        public List<IPlanOutput> Reports    { get; }


        public bool     UseFilterAusgabe        => CurrentReport == null? false: CurrentReport.UseFilterAusgabe;
        public bool     UseFilterLeistungstyp   => CurrentReport == null? false: CurrentReport.UseFilterLeistungstyp;
        public bool     UseFilterErgebnis       => CurrentReport == null? false: CurrentReport.UseFilterErgebnis;
        public String   NameReport              => CurrentReport == null? null:  CurrentReport is IPlanExport? $"Export {CurrentReport.Name}": $"Report {CurrentReport.Name}";
        public String   TextReport              => CurrentReport?.Text;
        public bool     UseFilterPanel          => UseFilterAusgabe || UseFilterLeistungstyp || UseFilterErgebnis;


        private IPlanOutput currentReport;
        public IPlanOutput CurrentReport
        {
            get { return currentReport; }
            set { if (SetProperty(ref currentReport, value)) NotifyPropertyChanged(); }
        }


        public ICommand SelectReportCommand     { get; }


        public ICommand ResetCommand            { get; }


        private ICommand executeCommand;
        public ICommand ExecuteCommand
        {
            get { return executeCommand; }
            set { SetProperty(ref executeCommand, value); }
        }


        private ICommand planCommand;
        public ICommand PlanCommand
        {
            get { return planCommand; }
            set { SetProperty(ref planCommand, value); }
        }


        public bool FilterVisible => !ViewerVisible && CurrentReport != null;


        private bool stunden;
        public bool Stunden
        {
            get { return stunden; }
            set { SetProperty(ref stunden, value); }
        }


        private bool preise;
        public bool Preise
        {
            get { return preise; }
            set { SetProperty(ref preise, value); }
        }


        private bool gruppensummen;
        public bool Gruppensummen
        {
            get { return gruppensummen; }
            set { SetProperty(ref gruppensummen, value); }
        }


        private bool planleistungen;
        public bool Planleistungen
        {
            get { return planleistungen; }
            set { SetProperty(ref planleistungen, value); }
        }


        private bool sonderleistungen;
        public bool Sonderleistungen
        {
            get { return sonderleistungen; }
            set { SetProperty(ref sonderleistungen, value); }
        }


        private bool bedarfspositionen;
        public bool Bedarfspositionen
        {
            get { return bedarfspositionen; }
            set { SetProperty(ref bedarfspositionen, value); }
        }


        private bool erledigt;
        public bool Erledigt
        {
            get { return erledigt; }
            set { SetProperty(ref erledigt, value); }
        }


        private bool offen;
        public bool Offen
        {
            get { return offen; }
            set { SetProperty(ref offen, value); }
        }


        private bool nurRelevante;
        public bool NurRelevante
        {
            get { return nurRelevante; }
            set { SetProperty(ref nurRelevante, value); }
        }


#endregion

    }
}
