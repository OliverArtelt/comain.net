﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Stock.Inventuren;


namespace Comain.Client.Commands
{
    
    public class InventurCommand : IControllerCommand 
    {

        public Type         Controller  => typeof(IInventurController);
        public String       Name        => "Inventur";
        public String       Parameter   => "Stock.IV"; 
        public String       Gruppe      => "Ersatzteile";
        public int          Sortierung  => 7000;
        public String       Color       => "Coral";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.layer.delete.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null || !module.VerwendetModulStock) return false;
            return nutzer.IstAdministrator || nutzer.IstLagerAdministrator;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
