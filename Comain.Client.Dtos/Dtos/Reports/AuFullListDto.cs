﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.Reports
{

    [DebuggerDisplay("{Auftragsnummer}")]
    public class AuFullListDto : AuListDto
    {

        public String               Inventarnummer              { get; set; }
        public String               Baugruppe                   { get; set; }
        public String               Bauteil                     { get; set; }
        public String               Bemerkungen                 { get; set; }
        public int?                 Baujahr                     { get; set; }
        public short?               Anschaffungsjahr            { get; set; }
        public String               Fabriknummer                { get; set; }
        public String               Typ                         { get; set; }
        public String               Anlage                      { get; set; }
        public decimal?             Anschaffungswert            { get; set; }
        public decimal?             Wiederbeschaffungswert      { get; set; }
        public String               TechnischeDetails           { get; set; }
        public DateTime?            DatumStandort               { get; set; }
        public DateTime?            DatumKostenstelle           { get; set; }
        public DateTime?            IHODeaktivierungsdatum      { get; set; }
        public bool?                IHOAktivstatus              { get; set; }

        public DateTime?            ZuletztGeändert             { get; set; }
        public String               ExterneAuftragsnummer       { get; set; }
        public String               Auftragsanmerkung           { get; set; }
        public String               Aktivbeschreibung           { get; set; }
        public String               FBNummer                    { get; set; }
        public bool?                FBAuftragImBudget           { get; set; }
        public DateTime?            Beauftragungsdatum          { get; set; }
        public DateTime?            Fertigstellungstermin       { get; set; }
        public DateTime?            Meldungsdatum               { get; set; }
        public DateTime?            RealisiertAm                { get; set; }
        public DateTime?            Übergabedatum               { get; set; }
        public int?                 Störzeitdauer               { get; set; }
        public DateTime?            Abnahmedatum                { get; set; }
        public DateTime?            DatumAuftragleiter          { get; set; }
        public DateTime?            StartTermin                 { get; set; }
        public String               Schadensbild                { get; set; }
        public String               Subschadensbild             { get; set; }
        public String               Schadensursache             { get; set; }
        public String               Subschadensursache          { get; set; }
        public String               Auftraggeber                { get; set; }
        public String               Auftragsleiter              { get; set; }
        public String               Auftragnehmer               { get; set; }
        public String               Abnahmeberechtigter         { get; set; }
        public decimal?             Arbeitszeit                 { get; set; }
        public decimal?             Fremdkosten                 { get; set; }
        public decimal?             Stunden                     { get; set; }
       
        public override decimal?    Kosten          => LeKosten.GetValueOrDefault() + MtKosten.GetValueOrDefault() + Fremdkosten.GetValueOrDefault();
        public TimeSpan?            Meldungszeit    => Meldungsdatum?.TimeOfDay;
        public TimeSpan?            Übergabezeit    => Übergabedatum?.TimeOfDay;
    }
}
