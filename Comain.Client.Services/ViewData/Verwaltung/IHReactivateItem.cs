﻿using System;
using System.Diagnostics;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.ViewData.Verwaltung
{

    [DebuggerDisplay("{Nummer}")]
    public class IHReactivateItem : NotifyBase
    {

        private readonly IHReactivateDto dto;


        public IHReactivateItem(IHReactivateDto dto)
        {
            this.dto = dto;
        }


        private bool isSelected;
        public bool IsSelected 
        { 
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); } 
        }


        public int          Id                  { get { return dto.Id; } }
        public String       Nummer              { get { return dto.Nummer; } }
        public String       Name                { get { return dto.Name; } }
        public DateTime?    DeaktiviertAm       { get { return dto.DeaktiviertAm; } }
    }
}
