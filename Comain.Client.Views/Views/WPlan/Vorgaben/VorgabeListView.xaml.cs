﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.WPlan.Vorgaben;

namespace Comain.Client.Views.WPlan.Vorgaben
{

    public partial class VorgabeListView : UserControl ,IVorgabeListView
    {
       
        public VorgabeListView()
        {
            InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as VorgabeListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
