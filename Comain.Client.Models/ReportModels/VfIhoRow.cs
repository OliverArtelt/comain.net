﻿using System;
using Comain.Client.Dtos.Reports;

namespace Comain.Client.ReportModels
{

    public class VfIhoRow
    {

        public DateTime     Monat               { get; set; }
        public decimal?     Sollzeit            { get; set; }
        public decimal?     Istzeit             { get; set; }
        public decimal?     Rate                { get; set; }
    

        public String MonatAsString
        {
            get { return Monat.ToString(@"yyyy\/MM"); }
        }
    

        public int MonatAsZahl
        {
            get { return Monat.Month; }
        }


        public VfIhoRow()
        {}


        public VfIhoRow(VfIhoDto dto, String typ)
        {
        
            Monat   = dto.Monat;
            Istzeit = dto.Störzeit;

            switch (typ) {

            case "Verfügbarkeit":

                Sollzeit = dto.SollBelegungszeit;
                Rate     = dto.Verfügbarkeit;
                break;

            case "Ausfallrate":

                Sollzeit = dto.SollBelegungszeit;
                Rate     = dto.Ausfallrate;
                break;

            case "Abweichungsgrad":

                Sollzeit = dto.SollAusfallzeit;
                Rate     = dto.Abweichungsgrad;
                break;
            }
        }
    }
}
