﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class PsSollIstDto
    {
      
        public String   Klammer         { get; set; }
        public String   Nummer          { get; set; }
        public String   Name            { get; set; }
        public decimal? Soll            { get; set; }
        public decimal? Ist             { get; set; }
        
        public decimal? Abweichung      { get { return Ist - Soll; } }
    }
}
