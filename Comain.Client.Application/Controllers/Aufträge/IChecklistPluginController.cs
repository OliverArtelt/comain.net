﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.ViewModels;
using Comain.Client.Controllers.WPlan;

namespace Comain.Client.Controllers.Aufträge
{

    public interface IChecklistPluginController : IChecklistController
    {

        object                  ChecklistView           { get; }

        bool                    HasChanges              { get; }
        List<String>            ErrorList               { get; }

        void                    SetEditMode(EditMode newMode);
        void                    Unload();

        ICommand                OpenDetailCommand       { get; }

        Task                    LoadAsync();
        Task                    LoadAuftragAsync(Auftrag au);
        Task<bool>              ConfirmSaveAsync();
        Task                    SaveAsync();
        Task<IEnumerable>       LEPositionAsync(int leid);
        Task<IEnumerable>       MTPositionAsync(int mtid);

        Task<ReportResult>      ReportAsync(Suchkriterien criteria);
    }
}
