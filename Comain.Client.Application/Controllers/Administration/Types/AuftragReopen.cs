﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class AuftragReopen : IAdminTyp
    {
        
        public string       Title           => "Wiedergeöffnete Aufträge";
        public int          Sortierung      => 21000;
        public Type         Controller      => typeof(IAuReopenController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
