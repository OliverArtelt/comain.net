﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Administration
{

    public class BevorAdminViewModel : ValidatableViewModel<IBevorAdminView, Konfiguration>, IConfigViewModel
    {

        private readonly SynchronizationContext context;
        private Dictionary<int, PersonalFilterModel> psdic;


        public BevorAdminViewModel(IBevorAdminView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            BevorAuftragsleiter = psdic != null && Current != null && Current.BevorAuftragsleiter.HasValue?
                psdic.GetValueOrDefault(Current.BevorAuftragsleiter.Value): null;
        }


        public void Unload()
        {
            PsSearchProvider.ClearItems();
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist)
        {

            psdic = pslist.ToDictionary(p => p.Id);
            context.Send(new SendOrPostCallback((o) => {

                PsSearchProvider.SetItems(pslist?.OrderBy(p => p.Name));

            }), null);
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private PersonalFilterModel personal;
        public PersonalFilterModel BevorAuftragsleiter
        {
            get { return personal; }
            set {

                personal = value;
                Current.BevorAuftragsleiter = value?.Id;
                RaisePropertyChanged(nameof(BevorAuftragsleiter));
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
