﻿using System;
using System.Collections;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Administration;
using Comain.Client.Views.Administration;


namespace Comain.Client.ViewModels.Administration
{

    public class AdminViewModel : ViewModel<IAdminView>
    {

        public AdminViewModel(IAdminView view) : base(view)
        {
        }


#region P R O P E R T I E S


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private IEnumerable einheiten;
        public IEnumerable Einheiten
        { 
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


        private IAdminTyp current;
        public IAdminTyp Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }
         

        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


#endregion
    
    }
}
