﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Services.Verwaltung
{

    public interface IMaterialService
    {
        
        TrackableCollection<Materialstamm> CurrentList { get; }

        bool HasChanges { get; }
        void Reset();

        Task<TrackableCollection<Materialstamm>> LoadAsync(CancellationToken ct = default);
        Task                                     SaveAsync(CancellationToken ct = default);
        Task<IEnumerable<KeyNameDto>>            EinheitenAsync(CancellationToken ct = default);
        Task<IEnumerable<KeyNameDto>>            WarengruppenAsync(CancellationToken ct = default);
        Task<IEnumerable<LieferantDto>>          LieferantenAsync(CancellationToken ct = default);
    }
}
