﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;

namespace Comain.Client.Models.Emma
{

    public class Leistung : Entity, IDataErrorInfo
    {

        private static String[] changeableProps = new String[] { nameof(Datum), nameof(UhrzeitVon), nameof(UhrzeitBis), nameof(IstBestätigt),
                                                                 nameof(IHObjekt), nameof(Auftrag), nameof(Personal) };


        private void RaiseStatusChanged()
        {

            RaisePropertyChanged(nameof(StatusColor));
            RaisePropertyChanged(nameof(Statuslist));
            RaisePropertyChanged(nameof(KannBestätigtWerden));
        }


        /// <summary>
        /// Bearbeiter hat IHObjekt zugeordnet => alle Leistungen mit derselben Inventarnummer gehören ebenfalls zu diesem IH-Objekt
        /// </summary>
        public void TesteIHObjektZuordnung(Leistung sibling)
        {

            if (sibling == null || String.IsNullOrWhiteSpace(EmmaInventarnummer)) return;
            if (sibling.EmmaInventarnummer != EmmaInventarnummer) return;
            if (IHObjekt != null || sibling.IHObjekt == null) return;
            if (this == sibling) return;
            if (IHObjekt == sibling.IHObjekt) return;

            if (Auftrag == null) IHObjekt = sibling.IHObjekt;
            if (Auftrag != null && Auftrag.IHObjektNummer != sibling.IHObjekt.Nummer) Auftrag = null;
        }


        /// <summary>
        /// Bearbeiter hat Auftrag zugeordnet => alle Leistungen mit derselben externen Auftragsnummer gehören ebenfalls zu diesem Auftrag
        /// </summary>
        public void TesteAuftragZuordnung(Leistung sibling)
        {

            if (sibling == null || String.IsNullOrWhiteSpace(EmmaAuftragsnummer)) return;
            if (sibling.EmmaAuftragsnummer != EmmaAuftragsnummer) return;
            if (Auftrag != null || sibling.Auftrag == null) return;
            if (Auftrag == sibling.Auftrag) return;

            Auftrag = sibling.Auftrag;
            IHObjekt = sibling.IHObjekt;
        }


        /// <summary>
        /// Bearbeiter hat Personal zugeordnet => alle Leistungen mit derselben Personalnummer gehören ebenfalls zu diesem Personal
        /// </summary>
        /// <remarks>
        /// dürfte eigentlich nicht passieren, da Personalnummer angegeben werden muß und eindeutig sein muß -> sollte immer stimmen
        /// </remarks>
        public void TestePersonalZuordnung(Leistung sibling)
        {

            if (sibling == null || String.IsNullOrWhiteSpace(EmmaPersonalnummer)) return;
            if (sibling.EmmaPersonalnummer != EmmaPersonalnummer) return;
            if (Personal != null || sibling.Personal == null) return;
            if (Personal == sibling.Personal) return;

            Personal = sibling.Personal;
        }


        public override bool IsChanged => ChangedProps != null && changeableProps.Any(q => ChangedProps.Contains(q));


        //originale Importdaten zum Vergleich / Duplikatprüfung / im Client unveränderliche Properties
        public String        EmmaAuftragsnummer     { get; set; }
        public String        EmmaPersonalnummer     { get; set; }
        public String        EmmaNachname           { get; set; }
        public String        EmmaVorname            { get; set; }
        public String        EmmaInventarnummer     { get; set; }
        public String        EmmaMaschine           { get; set; }
        public String        EmmaAktion             { get; set; }
        public DateTime      EmmaVon                { get; set; }
        public DateTime      EmmaBis                { get; set; }

        public DateTime?     GelöschtAm             { get; set; }
        public DateTime      ImportiertAm           { get; set; }
        public EmmaImportTyp Typ                    { get; set; }
        public String        Fehlerstatus           { get; set; }


        public bool LeistungGespeichert     => Personalleistung_Id.GetValueOrDefault() > 0;
        public bool AuftragIstAbgeschlossen => Auftrag != null && (Auftrag.Status == "Storno" || Auftrag.Status == "Abgeschlossen");
        public bool KannBestätigtWerden     => !AuftragIstAbgeschlossen && !GelöschtAm.HasValue &&
                                               Auftrag != null && Personal != null && String.IsNullOrEmpty(Fehlerstatus) &&
                                               !Errors.Any() && Typ != EmmaImportTyp.Abgeschlossen;
        public bool IstBearbeitbar          => !AuftragIstAbgeschlossen && !GelöschtAm.HasValue && Typ != EmmaImportTyp.Abgeschlossen;


        public String EmmaPersonalname
        {
            get {

                if (String.IsNullOrWhiteSpace(EmmaNachname)) return null;
                if (String.IsNullOrWhiteSpace(EmmaVorname)) return EmmaNachname;
                return EmmaNachname + ", " + EmmaVorname;
            }
        }


        private DateTime datum;
        public DateTime Datum
        {
            get { return datum; }
            set { if (SetProperty(ref datum, value)) RaiseStatusChanged(); }
        }


        private TimeSpan uhrzeitVon;
        public TimeSpan UhrzeitVon
        {
            get { return uhrzeitVon; }
            set { if (SetProperty(ref uhrzeitVon, value)) RaiseStatusChanged(); }
        }


        private TimeSpan uhrzeitBis;
        public TimeSpan UhrzeitBis
        {
            get { return uhrzeitBis; }
            set { if (SetProperty(ref uhrzeitBis, value)) RaiseStatusChanged(); }
        }


        private int? minuten;
        public  int? Minuten
        {
            get { return minuten; }
            set { if (SetProperty(ref minuten, value)) RaiseStatusChanged(); }
        }


        private bool istBestätigt;
        public  bool IstBestätigt
        {
            get { return istBestätigt; }
            set { if (SetProperty(ref istBestätigt, value)) RaiseStatusChanged(); }
        }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get { return personal; }
            set { if (SetProperty(ref personal, value)) RaiseStatusChanged(); }
        }


        private AuftragListDto auftrag;
        public AuftragListDto Auftrag
        {
            get { return auftrag; }
            set {

                if (SetProperty(ref auftrag, value)) {

                    //passt Auftrag  noch zum IHO?
                    if (value != null && IHObjekt != null && !String.IsNullOrEmpty(value.IHObjektNummer) && 
                        !value.IHObjektNummer.StartsWith(Auftrag.IHObjektNummer)) IHObjekt = null;
                    RaiseStatusChanged();
                }
            }
        }


        private IHListDto ihObjekt;
        public IHListDto IHObjekt
        {
            get { return ihObjekt; }
            set {

                if (SetProperty(ref ihObjekt, value)) {

                    //passt Auftrag  noch zum IHO?
                    if (value != null && Auftrag != null && !String.IsNullOrEmpty(value.Nummer) && 
                        (String.IsNullOrEmpty(Auftrag.IHObjektNummer) || !value.Nummer.StartsWith(Auftrag.IHObjektNummer))) Auftrag = null;
                    RaiseStatusChanged();
                }
            }
        }


        private int? personal_Id;
        public int? Personal_Id
        {
            get { return personal_Id; }
            set { if (SetProperty(ref personal_Id, value)) RaiseStatusChanged(); }
        }


        private int? auftrag_Id;
        public int? Auftrag_Id
        {
            get { return auftrag_Id; }
            set { if (SetProperty(ref auftrag_Id, value)) RaiseStatusChanged(); }
        }


        private int? ihObjekt_Id;
        public int? IHObjekt_Id
        {
            get { return ihObjekt_Id; }
            set { if (SetProperty(ref ihObjekt_Id, value)) RaiseStatusChanged(); }
        }


        private int? personalleistung_Id;
        public int? Personalleistung_Id
        {
            get { return personalleistung_Id; }
            set { if (SetProperty(ref personalleistung_Id, value)) RaiseStatusChanged(); }
        }


        public String StatusColor
        {

            get {

                if (GelöschtAm.HasValue) return "Gray";
                if (!String.IsNullOrWhiteSpace(Fehlerstatus)) return "Coral";
                if (AuftragIstAbgeschlossen) return "Purple";
                if (IHObjekt == null && Auftrag == null && Personal == null) return "White";
                if (Auftrag == null || Personal == null || Errors.Any()) return "Yellow";
                return Typ == EmmaImportTyp.Abgeschlossen? "RoyalBlue": "LimeGreen";
            }
        }


        public List<String> Statuslist
        {

            get {

                var result = new List<String>();
                result.Add($"• Importiert am {ImportiertAm.ToShortDateString()}");

                if (!IstBearbeitbar) result.Add("• Die Leistung darf nicht weiter bearbeitet werden.");

                if (GelöschtAm.HasValue) {

                    result.Add("• Die Leistung wurde nicht im Wochenbericht bestätigt und daher gelöscht.");
                    result.Add($"• Gelöscht am {GelöschtAm.Value.ToShortDateString()}");
                    return result;
                }

                if (Auftrag != null && Auftrag.Status == "Abgeschlossen") {

                    result.Add("• Der zugeordnete Auftrag ist abgeschlossen.");
                    return result;
                }

                if (Auftrag != null && Auftrag.Status == "Storno") {

                    result.Add("• Der zugeordnete Auftrag ist storniert.");
                    return result;
                }

                if (Typ == EmmaImportTyp.Abgeschlossen) {

                    result.Add("• Die Leistung wurde im Wochenbericht bestätigt.");
                    return result;
                }

                if (IHObjekt == null && Auftrag == null && Personal == null) result.Add("• Es wurden noch keine Zuordnungen vorgenommen.");
                if (Auftrag == null || Personal == null) result.Add("• Es fehlen Zuordnungen.");
                if (Errors.Any()) result.AddRange(Errors);
                if (!String.IsNullOrWhiteSpace(Fehlerstatus)) result.Add($"• {Fehlerstatus}");

                return result;
            }
        }


        public int? BerechneteMinuten()
        {

            var von = Datum + UhrzeitVon;
            var bis = Datum + UhrzeitBis;
            if (bis < von) bis = bis.AddDays(1);

            int minuten = (int)((bis - von).TotalMinutes);
            return minuten;
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Datum)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Minuten)];
                return test;
            }
        }


        public IEnumerable<String> Errors
        {

            get {

                var tests = new List<String>();
                tests.Add(this[nameof(Datum)]);
                tests.Add(this[nameof(Minuten)]);
                return tests.Where(p => !String.IsNullOrEmpty(p)).ToList();
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Datum):

                    if (Datum == default) return "• Geben Sie das Datum an.";
                    if (Datum.Year < 2000 || Datum.Year > DateTime.Today.Year + 1) return "• Geben Sie ein sinnvolles Datum an.";
                    break;

                case nameof(Minuten):

                    if (!Minuten.HasValue) return "• Geben Sie die gearbeiteten Minuten an.";
                    if (Minuten.Value > BerechneteMinuten())
                        return $"• Die gearbeiteten Minuten dürfen nicht größer als die Differenz zwischen Start- und Endzeit sein.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
