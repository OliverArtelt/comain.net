﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Comain
{

    /// <summary>
    /// wirft NotifyPropertyChanged wenn sich eine Eigenschaft eines Elementes ändert.
    /// </summary>
    /// <remarks>
    /// z.B. Gesamtpreis einer Rechnung neu berechnen/anzeigen wenn sich Menge oder Einzelpreis einer Rechnungsposition geändert hat.
    /// </remarks>
    public class NotifyTrackableCollection<T> : TrackableCollection<T>, INotifyPropertyChanged where T : NotifyBase
    {

        public NotifyTrackableCollection() : base()
        {}


        public NotifyTrackableCollection(IEnumerable<T> collection) : base(collection)
        {
            Items.ForEach(p => PropertyChangedEventManager.AddHandler(p, ItemPropertyChanged, String.Empty));
        }


        public NotifyTrackableCollection(List<T> list) : base(list)
        {
            Items.ForEach(p => PropertyChangedEventManager.AddHandler(p, ItemPropertyChanged, String.Empty));
        }


        public override void Add(T item)
        {
            
            PropertyChangedEventManager.AddHandler(item, ItemPropertyChanged, String.Empty);
            base.Add(item);
        }


        public override void Insert(int index, T item)
        {
            
            PropertyChangedEventManager.AddHandler(item, ItemPropertyChanged, String.Empty);
            base.Insert(index, item);
        }


        protected override void InsertItem(int index, T item)
        {
            
            PropertyChangedEventManager.AddHandler(item, ItemPropertyChanged, String.Empty);
            base.InsertItem(index, item);
        }


        public override void Remove(T item)
        {
            
            PropertyChangedEventManager.RemoveHandler(item, ItemPropertyChanged, String.Empty);
            base.Remove(item);
        }


        public void Remove(IEnumerable<T> items)
        {

            if (items.IsNullOrEmpty()) return;            
            items.ToList().ForEach(item => Remove(item));
        }

                                      
        protected virtual new void RemoveAt(int index)
        {
            
            var item = Items[index]; 
            PropertyChangedEventManager.RemoveHandler(item, ItemPropertyChanged, String.Empty);
            base.RemoveItem(index);
        }


        protected override void RemoveItem(int index)
        {
            
            var item = Items[index]; 
            PropertyChangedEventManager.RemoveHandler(item, ItemPropertyChanged, String.Empty);
            base.RemoveItem(index);
        }
         

        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e);
        }
    } 
}

