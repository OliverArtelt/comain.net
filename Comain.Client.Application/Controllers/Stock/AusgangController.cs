﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels.Stock;
using Comain.Client.Services.Stock.Warenausgänge;
using Comain.Client.ViewModels.Stock.Warenausgänge;
using Comain.Client.ViewModels;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Controllers.Stock.Warenausgänge
{

    public class AusgangController : ControllerBase, IAusgangController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IAusgangService store;
        private readonly MainViewModel mainView;
        private readonly AusgangViewModel view;
        private readonly AusgangListViewModel listView;
        private readonly AusgangFilterViewModel filterView;
        private readonly AusgangDetailViewModel detailView;
        private readonly AusgangDetailBaseViewModel baseView;
        private readonly AusgangDetailReportViewModel reportView;
        private readonly ArtikelSelektorViewModel addView;
        private TrackableCollection<Warenausgang> currentList;
        private IList<PersonalFilterModel> psList;
        private SerializableCommand deleteCommand;


        public AusgangController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                 IAusgangService store, IControllerBus bus,
                                 MainViewModel mainView, AusgangViewModel view, AusgangListViewModel listView, AusgangFilterViewModel filterView,
                                 AusgangDetailViewModel detailView, AusgangDetailBaseViewModel baseView, AusgangDetailReportViewModel reportView,
                                 ArtikelSelektorViewModel addView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.baseView = baseView;
            this.reportView = reportView;
            this.addView = addView;
        }


        public void Initialize()
        {

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;
            detailView.BasisView = baseView.View;
            detailView.ReportView = reportView.View;

            filterView.SearchCommand = CreateCommand(_ => SearchAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => detailView.Current != null);
            listView.DetailCommand = CreateCommand(_ => DetailAsync());

            PropertyChangedEventManager.AddHandler(listView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Warenausgänge");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.RemoveHandler(listView, ViewPropertyChanged, String.Empty);
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await ConfirmAsync()) return false;

            baseView.Unload();
            reportView.Unload();
            filterView.Unload();
            listView.Unload();
            currentList = null;
            psList = null;
            SetModel(null);

            return true;
        }


        private async Task<bool> ConfirmAsync()
        {

            if (currentList == null || !currentList.IsChanged) return true;
            return await messages.ConfirmExitAsync();
        }


        private async Task LoadAsync()
        {

            detailView.Current = null;
            currentList = null;
            psList = null;
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                var lotask = store.LagerorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var autask = store.AufträgeAsync(bus.Token);
                var pstask = store.PersonalAsync(bus.Token);
                var vftask = store.VerfügbareArtikelAsync(bus.Token);
                var watask = SearchAsync();

                await Task.WhenAll(watask, autask, lotask, kstask, pstask, vftask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();
                psList = pstask.Result;

                await context.SendAsync(new SendOrPostCallback((o) => {

                    filterView.Lagerorte     = lotask.Result.OrderBy(p => p.Nummer).ToList();
                    filterView.Kostenstellen = kstask.Result;
                    addView.Aufträge         = autask.Result.Where(p => p.IstOffen).ToList();
                    addView.SetzeArtikelliste(vftask.Result.Select(p => new WLItem(p)).ToList());
                    baseView.SetzeListen(kstask.Result, psList, autask.Result);

                }), null);
            }
        }


        private async Task SearchAsync()
        {

            if (!await ConfirmAsync()) return;
            var oldlistmodel = listView.Current;

            currentList = await store.FindAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
            filterView.SetzeAnzahlGefunden(currentList.Count);

            await context.SendAsync(new SendOrPostCallback((o) => {

                listView.FoundList = currentList;

            }), null);

            if (!currentList.IsNullOrEmpty()) {

                Warenausgang current = null;
                if (oldlistmodel != null) current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                if (current == null) current = currentList.FirstOrDefault();
                SetModel(current);
            }

            UpdateCommands();
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
            }), null);
        }


        private void SetModel(Warenausgang current)
        {

            detailView.Current = current;
            baseView.Current = current;

            if (current != null && currentList != null) {

                var listviewitem = currentList.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null && listviewitem != listView.Current) listView.Current = listviewitem;
            }

            UpdateCommands();
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == "CurrentTab") {

                UpdateCommands();
                if (detailView.Tab == AusgangDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
            }
        }


        private async Task DetailAsync()
        {

            SetModel(listView.Current);
            UpdateCommands();
            if (detailView.Tab == AusgangDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
        }


        private async Task UpdatePrintViewAsync()
        {

            reportView.ViewerVisible = false;
            if (detailView.Current == null) return;

            var ihtask = store.IHInfoAsync(detailView.Current.Auftrag.Id, bus.Token);
            var rmtask = store.FachRestmengeAsync(detailView.Current.Lagerort.Id, detailView.Current.Material.Id, bus.Token);
            await Task.WhenAll(ihtask, rmtask).ConfigureAwait(false);

            var address = "Comain.Client.Reports.Stock.Ausgabeschein.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = ("Ausgabeschein " + detailView.Current.Nummer).Trim() };
            var reportmodel = new Ausgabeschein(detailView.Current, rmtask.Result, ihtask.Result.Name, ihtask.Result.Nummer);

            report.AddDataSource("MainData", new List<Ausgabeschein> { reportmodel });

            await reportView.BindViewerAsync(report);
        }


        private async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                if (currentList == null || !currentList.IsChanged) return;
                await store.SaveAsync(currentList, bus.Token).ConfigureAwait(false);
                await LoadAsync();
            }
        }


        private async Task AddAsync()
        {

            addView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = addView.View, Header = "Waren auslagern", Position = MahApps.Metro.Controls.Position.Right })) return;
            if (addView.Current == null) return;

            var wa = new Warenausgang(addView.Current.Model, CurrentUser());
            if (addView.Auftrag != null) {

                wa.Auftrag = addView.Auftrag;
                wa.Kostenstelle = baseView.Kostenstellen.FirstOrDefault(p => p.Id == addView.Auftrag.Kostenstelle_Id);
            }

            await context.SendAsync(new SendOrPostCallback((o) => {

                currentList.Add(wa);
                SetModel(wa);

            }), null);
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current != null) {

                await context.SendAsync(new SendOrPostCallback((o) => {

                    currentList.Remove(detailView.Current);
                }), null);
            }
        }


        private PersonalFilterModel CurrentUser()
        {

            if (psList == null) return null;
            if (!login.AktuellerNutzer.Personal_Id.HasValue) return null;
            return psList.FirstOrDefault(p => p.Id == login.AktuellerNutzer.Personal_Id.Value);
        }
    }
}
