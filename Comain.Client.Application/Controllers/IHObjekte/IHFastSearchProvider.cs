﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.ViewModels.IHObjekte;
using FeserWard.Controls;


namespace Comain.Client.Controllers.IHObjekte
{

    /// <summary>
    /// IH-Combobox als Autocomplete (z.B. Auftragerstellungsassistent)
    /// </summary>
    public interface IIHFastSearchProvider : IIntelliboxResultsProvider
    {

        void SetItems(IEnumerable<IHListDto> ihlist);
        void SetItems(IEnumerable<AuIHKSDto> ihlist);
        void SetItems(IEnumerable<IHFilterItem> ihlist);
        void ClearItems();
        void AddModel(IHFilterItem model);
    }


    public class IHFastSearchProvider : IIHFastSearchProvider
    {

        private Dictionary<String, IHFilterItem> currentIHDic;


        public void AddModel(IHFilterItem model) => currentIHDic.Add(model.Nummer, model);


        public void ClearItems() => currentIHDic = null;


        public void SetItems(IEnumerable<AuIHKSDto> ihlist)
        {

            currentIHDic = ihlist.DistinctBy(p => p.IHObjekt).Select(p => new IHFilterItem(p)).ToDictionary(p => p.Nummer);
            SetPaths();
        }


        public void SetItems(IEnumerable<IHListDto> ihlist)
        {

            currentIHDic = ihlist.DistinctBy(p => p.Nummer).Select(p => new IHFilterItem(p)).ToDictionary(p => p.Nummer);
            SetPaths();
        }


        public void SetItems(IEnumerable<IHFilterItem> ihlist)
        {

            currentIHDic = ihlist.DistinctBy(p => p.Nummer).ToDictionary(p => p.Nummer);
            SetPaths();
        }


        private void SetPaths()
        {

            foreach (var ih in currentIHDic.Values) {

                ih.Baugruppen.Push(ih.Name);
                var parent = currentIHDic.GetValueOrDefault(ih.ParentNummer);
                while (parent != null) {

                    ih.Baugruppen.Push(parent.Name);
                    parent = currentIHDic.GetValueOrDefault(parent.ParentNummer);
                }
            }
        }


        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (currentIHDic.IsNullOrEmpty()) yield break;
            if (String.IsNullOrWhiteSpace(searchTerm)) yield break;

            int count = 0;
            foreach (var ih in currentIHDic.Values) {

                if (ih.Nummer.StartsWith(searchTerm) ||
                    !String.IsNullOrEmpty(ih.Pfad) && ih.Pfad.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    !String.IsNullOrEmpty(ih.Inventarnummer) && ih.Inventarnummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    !String.IsNullOrEmpty(ih.Fabriknummer) && ih.Fabriknummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) {

                    count++;
                    if (count > maxResults) yield break;

                    yield return ih;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (currentIHDic.IsNullOrEmpty()) yield break;
            foreach (var ih in currentIHDic.Values) yield return ih;
        }
    }
}
