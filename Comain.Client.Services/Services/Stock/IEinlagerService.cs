﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;

namespace Comain.Client.Services.Stock.Wareneinlagerungen
{

    public interface IEinlagerService
    {

        Task<IList<EingangFilterDto>>               EingängeAsync(CancellationToken ct = default);
        Task<IList<Wareneingang>>                   LagerfähigeEingängeAsync(CancellationToken ct = default);
        Task<Lagerort>                              LetzerLagerortAsync(int artikelId, CancellationToken ct = default);
        Task<Inventur>                              LetzeInventurAsync(int wlId, CancellationToken ct = default);
        Task<IList<Lagerort>>                       LagerorteAsync(CancellationToken ct = default);
        Task<IList<Artikel>>                        ArtikelAsync(CancellationToken ct = default);
        
        Task<TrackableCollection<Wareneinlagerung>> SearchAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<WLReportDto>>                    ReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task                                        SaveAsync(TrackableCollection<Wareneinlagerung> list, CancellationToken ct = default);

        Task<IEnumerable<BestellListDto>>           AbschlussfähigeBestellungenAsync(CancellationToken ct = default);
        Task                                        BestellungenAbschliessenAsync(List<int> idlist, CancellationToken ct = default);
    }
}
