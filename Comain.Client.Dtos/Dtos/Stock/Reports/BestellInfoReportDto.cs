﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class BestellInfoReportDto
    {

        public int       ArtikelId	            { get; set; }
        public String    Artikelnummer	        { get; set; }
        public String    Artikelbezeichnung	    { get; set; }
        public String    Einheit                { get; set; }
        public decimal?  Mindestbestand	        { get; set; }
        public decimal?  Mindestbestellmenge    { get; set; }
        public decimal?  Lagerbestand	        { get; set; }
        public decimal?  FehlendeMenge	        { get; set; }
        public decimal?  BestellteMenge         { get; set; }
        public decimal?  Mischpreis             { get; set; }

        public String    Lieferant		        { get; set; }
        public String    Bestellnummer	        { get; set; }
        public short?    Rank                   { get; set; }
        public decimal?  Einzelpreis	        { get; set; }
        public decimal?  Gesamtpreis            { get; set; }
        public decimal?  MindestbestellmengeLf  { get; set; }
        public int?      BestellzeitLf          { get; set; }
    }
}
