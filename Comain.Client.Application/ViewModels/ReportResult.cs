﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Comain.Client.ViewModels
{

    public class ReportResult
    {

        private List<Tuple<String, IEnumerable>>  dataSources;
        private List<Tuple<String, String>> parameters;


        public IList<Tuple<String, IEnumerable>>  DataSources  
        { 
            get => dataSources ?? (dataSources = new List<Tuple<String, IEnumerable>>()); 
        }


        public IList<Tuple<String, String>> Parameters  
        { 
            get => parameters ?? (parameters = new List<Tuple<String, String>>()); 
        }

        
        /// <remarks>
        /// Adresse des Berichtes als Ressource z.B. "Comain.Client.Reports.AuReopenReport.rdlc"
        /// </remarks>
        public String ReportAddress   { get; set; }
        /// <remarks>
        /// Name des Assemblies z.B. "comain.Client.Reports"
        /// </remarks>
        public String AssemblyAddress { get; set; }
        /// <remarks>
        /// Name der Exportdatei, auf ungültige Dateinamen achten
        /// </remarks>
        public String Title           { get; set; }
        /// <remarks>
        /// Bericht hat Kundenlogo, entsprechenden Parameter und Bild in Bericht einfügen
        /// </remarks>
        public bool HasLogo           { get; set; }


        public void AddDataSource(String key, IEnumerable data)
        {
            DataSources.Add(new Tuple<String, IEnumerable>(key, data));
        }


        public void AddParameter(String key, String value)
        {
            Parameters.Add(new Tuple<String, String>(key, value));
        }
    }
}
