﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{
    
    public interface ILieferantController : IVwTypController 
    {}
    
    
    public class LieferantController : ControllerBase, ILieferantController
    {
        
        private readonly IFlyoutService flyouts;
        private readonly ILieferantService store;
        private readonly VerwaltungViewModel mainView;
        private readonly LieferantenViewModel view;
        private readonly LieferantCombineViewModel combineView;
        private ObservableCollection<Lieferant> currentList;
        private SerializableCommand deleteCommand;
        private SerializableCommand combineCommand;
        private SerializableCommand combine2Command;
        private SerializableCommand addAPCommand;
        private SerializableCommand deleteAPCommand;


        public LieferantController(IMessageService messages, ILieferantService store, IFlyoutService flyouts, IControllerBus bus, 
                                   VerwaltungViewModel mainView, LieferantenViewModel view, LieferantCombineViewModel combineView)
          : base(messages, bus)
        {
        
            this.store = store;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.combineView = combineView;
        }

        
        protected string Url
        {
            get { return "/VW/LF"; }
        }


        public void Initialize()
        {
            
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => view.Current != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());
            view.AddAPCommand = addAPCommand = CreateCommand(_ => AddAPAsync(), _ => view.Current != null);
            view.DeleteAPCommand = deleteAPCommand = CreateCommand(_ => DeleteAPAsync(), _ => view.CurrentAP != null);
            view.CombineCommand = combineCommand = CreateCommand(_ => CombineAsync(), _ => view.Current != null);
            combineView.ExecuteCommand = combine2Command = CreateCommand(_ => Combine2Async(), _ => combineView.Quelle != null);
            
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(combineView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public virtual void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(combineView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {
            
            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;
            
            store.Reset();
            view.Unload();
            currentList = null;
            return true;
        }


        private async Task SaveAsync()
        {
            
            using (new VisualDelay(view)) {
            
                await store.SaveAsync(Url, bus.Token);
                await LoadAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var oldmodel = view.Current;
                
                currentList = await store.GetAsync(Url, bus.Token);
                context.Send(new SendOrPostCallback((o) => { 
                
                    view.SetList(currentList); 
                    if (oldmodel != null) view.Current = currentList.FirstOrDefault(p => p.Id == oldmodel.Id);

                }), null);
            }
        }


        private async Task AddAsync()
        {

            if (view.Liste == null) return;
        
            await OnUIThreadAsync(() => { 
            
                var model = new Lieferant(); 
                view.Liste.Add(model);
                view.Current = model;
                
            });
        }


        private async Task DeleteAsync()
        {

            if (view.Liste == null) return;
        
            await OnUIThreadAsync(() => { 
            
                var model = view.Current;

                if (model == null) return;
                view.Liste.Remove(model);
                view.Current = null;
                
            });
        }

           
        private async Task CombineAsync()
        {
        
            if (view.Current == null) return;

            await OnUIThreadAsync(() => { 
            
                combineView.Lieferanten = currentList.Where(p => p != view.Current).ToList();
                combineView.Ziel = view.Current.VollerName;
                       
            });

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = combineView.View, Position = MahApps.Metro.Controls.Position.Bottom });
        }

           
        private async Task Combine2Async()
        {
        
            if (view.Current == null) return;
            if (combineView.Quelle == null) return;

            await store.CombineAsync(combineView.Quelle.Id, view.Current.Id, bus.Token);
            await flyouts.CloseAsync();
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => { 
            
                deleteCommand.RaiseCanExecuteChanged(); 
                combineCommand.RaiseCanExecuteChanged(); 
                combine2Command.RaiseCanExecuteChanged(); 
                addAPCommand.RaiseCanExecuteChanged();
                deleteAPCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
           
            if (args.PropertyName == "Current")   UpdateCommands();
            if (args.PropertyName == "CurrentAP") UpdateCommands();
            if (args.PropertyName == "Quelle")    UpdateCommands();
        }


        private Task AddAPAsync()
        {

            return OnUIThreadAsync(() => { 
            
                if (view.Current == null) return;
                var newAp = new LieferantAP { Lieferant_Id = view.Current.Id }; 
                view.Current.Ansprechpartnerliste.Add(newAp);
                view.CurrentAP = newAp;
            });
        }


        private Task DeleteAPAsync()
        {

            return OnUIThreadAsync(() => { 
            
                if (view.Current == null || view.CurrentAP == null) return;
                view.Current.Ansprechpartnerliste.Remove(view.CurrentAP);
                view.CurrentAP = null;
            });
        }
    }
}
