﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Materialien;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Aufträge
{

    public class LeMtParameter
    {

        public Auftrag           Auftrag        { get; set; }
        public Personalleistung  Leistung       { get; set; }
        public Materialleistung  Material       { get; set; }
        ///<summary>
        ///Bearbeitungsmodus Auftrag
        ///</summary
        public EditMode          EditMode       { get; set; }
        /// <summary>
        /// aufrufende Einheit über Änderungen informieren
        /// </summary>
        public Action            Update         { get; set; }
    }
}
