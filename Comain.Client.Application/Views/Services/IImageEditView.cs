﻿using System.Security.Cryptography.X509Certificates;
using System.Waf.Applications;
using System.Windows.Controls;

namespace Comain.Client.Views.Services
{
    public interface IImageEditView : IView
    {
        InkCanvas Canvas { get; }
    }
}
