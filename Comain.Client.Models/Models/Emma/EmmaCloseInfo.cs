﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Emma
{
    public class EmmaCloseInfo
    {

        public DateTime  Woche          { get; set; }
        public int       Gesamt         { get; set; }
        public int       Abgeschlossen  { get; set; }
        public int       Verworfen      { get; set; }


        public String  WocheAsText  => $"KW {Woche.WeekOfYear()} {Woche.ToShortDateString()} - {Woche.AddDays(6).ToShortDateString()}";


        public List<String> Explanations(IEnumerable<Leistung> editlist)
        {

            var result = new List<String>();

            result.Add($"Abschluß Woche {WocheAsText}");
            result.Add(Gesamt != 1? $"Es wurden {Gesamt} Leistungen in dieser Woche importiert.": $"Es wurde eine Leistung in dieser Woche importiert.");
            result.Add(editlist.Count() != 1? $"Es werden {editlist.Count()} Leistungen angezeigt.": $"Es wird eine Leistung angezeigt.");
            result.Add(Abgeschlossen != 1? $"Es wurden bereits {Abgeschlossen} Leistungen abgeschlossen.": $"Es wurde bereits eine Leistung abgeschlossen.");
            result.Add(Abgeschlossen != 1? $"Es wurden bereits {Verworfen} Leistungen verworfen.": $"Es wurde bereits eine Leistung verworfen.");

            return result;
        }
    }
}
