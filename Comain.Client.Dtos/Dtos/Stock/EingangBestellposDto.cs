﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class EingangBestellposDto
    {
       
        public int        Id              { get; set; }  
        public String     Nummer          { get; set; }  
        public int?       Position        { get; set; }  
        public String     Auftrag         { get; set; }  
        public decimal    Bestellmenge    { get; set; } 
        /// <summary>
        /// Menge die bereits eingegangen ist
        /// </summary>
        public decimal    MengeEingang    { get; set; } 
        public decimal?   Preis           { get; set; }  

        public int        Material_Id     { get; set; }  
        public String     Artikelnummer   { get; set; }  
        public String     Artikelname     { get; set; }
    }
}
