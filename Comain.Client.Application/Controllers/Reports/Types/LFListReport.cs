﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public class LFListReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public LFListReport(IReportService store)
        {
            this.store = store;            
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.LieferantenAsync(null, ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Lieferantenliste"; }
        }


        public string Gruppe
        {
            get { return "Stammdaten"; }
        }


        public int Sortierung
        {
            get { return 200; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.LFListReport.rdlc"; }
        }
    }
}
