﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Artikelstamm;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public class MtLfRankReport : ReportBase, IReport
    {

        private readonly DisplayFilterViewModel displayView;
        private readonly IArtikelService store;
       

        public MtLfRankReport(IArtikelService store, DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, RankingFilterViewModel rankView)
        {

            this.store = store;
            this.displayView = displayView;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, rankView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }
 

        public override string Title
        {
            get { return "Lieferanten-Ranking"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 9000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.MtLfRankExport.rdlc": "Comain.Client.Reports.Stock.MtLfRankReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.MtLfRankReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
