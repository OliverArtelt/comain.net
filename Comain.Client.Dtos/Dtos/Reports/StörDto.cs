﻿namespace Comain.Client.Dtos.Reports
{

    public class StörDto
    {

        public decimal?     SollZeit        { get; set; }
        public decimal?     SollAnzahl      { get; set; }
        public decimal?     SollKosten      { get; set; }
        
        public decimal?     IstZeit         { get; set; }
        public decimal?     IstAnzahl       { get; set; }
        public decimal?     IstKosten       { get; set; }

        public decimal?     DivZeit         { get; set; }
        public decimal?     DivAnzahl       { get; set; }
        public decimal?     DivKosten       { get; set; }

        public decimal?     ProzZeit        { get; set; }
        public decimal?     ProzAnzahl      { get; set; }
        public decimal?     ProzKosten      { get; set; }

        public decimal      JahrAnteil      { get; set; }
    }
}
