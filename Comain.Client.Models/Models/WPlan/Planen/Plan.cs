﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels.WPlan;
using Microsoft.Extensions.Logging;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Die gesamte Planung eines Zeitraumes, möglicherweise gefiltert nach GW, IH, Kst, PS
    /// </summary>
    public class Plan : IAcceptor, IChangeTracking
    {

        private readonly ILogger logger;
        public const int Aufgabenhöhe = 28;
        public const int Aufgabenrand = 2;


        public Plan(WPlanCreateInfo pars, List<Planspalte> spalten, List<IPlanzeile> zeilen, Nutzer aktuellerNutzer, Konfiguration cfg)
        {

            Bearbeiter = aktuellerNutzer;
            Parameter = pars;
            Konfiguration = cfg;
            Spalten = spalten;
            Zeilen = zeilen;
            Spaltensummen = spalten.Select(p => new Planspaltensumme(p)).ToList();
        }


        public Plan(WPlanCreateInfo pars, List<Planspalte> spalten, List<IPlanzeile> zeilen, Nutzer aktuellerNutzer, Konfiguration cfg, ILogger logger)
            : this(pars, spalten, zeilen, aktuellerNutzer, cfg)
            => this.logger = logger;


        public Nutzer                           Bearbeiter              { get; private set; }
        public WPlanCreateInfo                  Parameter               { get; private set; }
        public Konfiguration                    Konfiguration           { get; private set; }
        public int                              NeueAufgabeId           { get; set; }
        public List<Planspalte>                 Spalten                 { get; private set; }
        public List<IPlanzeile>                 Zeilen                  { get; private set; }
        public List<Planspaltensumme>           Spaltensummen           { get; private set; }
        public int                              Spaltenbreite           { get; set; }
        public String                           Client                  { get; set; }
        public String                           Benutzer                { get; set; }
        public Planstatistik                    Statistik               { get; set; }

        public IQueryable<PZPlanzeile>          AlleStandorte           => Zeilen.AsQueryable().Where(p => p is PZPlanzeile).Cast<PZPlanzeile>();
        public IQueryable<IHPlanzeile>          AlleIHObjekte           => Zeilen.AsQueryable().Where(p => p is IHPlanzeile).Cast<IHPlanzeile>();
        public IQueryable<MNPlanzeile>          AlleMassnahmen          => Zeilen.AsQueryable().Where(p => p is MNPlanzeile).Cast<MNPlanzeile>();
        public IQueryable<Planzelle>            AlleZellen              => AlleMassnahmen.SelectMany(p => p.Perioden);
        public IQueryable<Planaufgabe>          AllePositionen          => AlleZellen.SelectMany(q => q.Aufgaben);
        public IQueryable<Planaufgabe>          OffenePositionen        => AllePositionen.Where(p => p.IstOffen);
        public IQueryable<Planaufgabe>          SelektiertePositionen   => AllePositionen.Where(p => p.IstSelektiert);
        public IQueryable<Planaufgabe>          ModifiziertePositionen  => AllePositionen.Where(p => p.IsChanged);
        public IQueryable<Planauftrag>          AlleAufträge            => AlleIHObjekte.Where(p => !p.Aufträge.IsNullOrEmpty()).SelectMany(q => q.Aufträge);
        public IQueryable<Planauftrag>          OffeneAufträge          => AlleIHObjekte.Where(p => !p.OffeneAufträge.IsNullOrEmpty()).SelectMany(q => q.OffeneAufträge);
        public bool                             HatSelektion            => AllePositionen.Any(p => p.IstSelektiert);
        public bool                             HatPlanung              => Zeilen.Any();
        public bool                             DarfRückmelden          => HatPlanung && (Bearbeiter.IstAdministrator || Bearbeiter.IstInstandhaltungLeitung ||
                                                                                          Bearbeiter.IstInstandhaltungInstandhalter);
        public bool                             DarfPersonalZuordnen    => HatPlanung && (Bearbeiter.IstAdministrator || Bearbeiter.IstInstandhaltungLeitung);


        public int AnzahlMaterialVorhanden => OffenePositionen.Count(p => !p.IstVergangenheit && p.HatMaterial && p.Materialien.All(q => !q.IstFehlend || !q.Erforderlich));
        public int AnzahlMaterialFehlend   => OffenePositionen.Count(p => !p.IstVergangenheit && p.Materialien.Any(q => q.IstFehlend && q.Erforderlich));


        public int AnzahlMassnahmen  => Zeilen.AsQueryable().Count(p => p is MNPlanzeile);
        public int AnzahlPositionen  => AllePositionen.Count();


        public decimal ExportierteGeplanteStunden(PlanOptions opts)
            => AlleStandorte.Sum(p => p.ExportierteGeplanteStunden(opts));

        public decimal ExportierteVerbrauchteStunden(PlanOptions opts)
            => AlleStandorte.Sum(p => p.ExportierteVerbrauchteStunden(opts));

        public decimal ExportierteGeplanteKosten(PlanOptions opts)
            => AlleStandorte.Sum(p => p.ExportierteGeplanteKosten(opts));

        public decimal ExportierteVerbrauchteKosten(PlanOptions opts)
            => AlleStandorte.Sum(p => p.ExportierteVerbrauchteKosten(opts));


        public Dictionary<int, PersonalFilterModel>  Personalverzeichnis  { get; set; }


        public String PersonalFromId(int? psid) => psid.HasValue && Personalverzeichnis != null?
                                                   Personalverzeichnis.GetValueOrDefault(psid.Value)?.Name: null;

        public String PeriodAsString
        {
            get {

                switch (Parameter.Planungstyp) {

                    case Planungstyp.Jahr: return $"Jahr {Parameter.StartDate.Value.ToShortDateString()} - {Parameter.EndDate.Value.ToShortDateString()}";
                    case Planungstyp.JahrKw: return $"Jahr {Parameter.StartDate.Value.ToShortDateString()} - {Parameter.EndDate.Value.ToShortDateString()}";
                    case Planungstyp.Wöchentlich: return $"Wochen {Parameter.StartDate.Value.ToShortDateString()} - {Parameter.EndDate.Value.ToShortDateString()}";
                    case Planungstyp.Täglich: return $"Tage {Parameter.StartDate.Value.ToShortDateString()} - {Parameter.EndDate.Value.ToShortDateString()}";
                }

                return null;
            }
        }


        public String StatusAsString
        {
            get {

                switch (Parameter.Planungsstatus) {

                    case Planungsstatus.NurBedarfspositionen: return "Bedarfspositionen";
                    case Planungsstatus.AlleMassnahmen: return "Alles";
                    case Planungsstatus.NurPositionen: return "Positionen";
                }

                return null;
            }
        }


        public String Name => StatusAsString + " " + PeriodAsString;


        public String StatusInfo
        {
            get {

                var mn = AlleMassnahmen.Count(p => p.IstSichtbar);
                if (mn == 0) return "keine Maßnahmen gefunden";
                String result = mn == 1? "eine Maßnahme, ": $"{mn} Maßnahmen, ";

                var au = AllePositionen.Count(p => p.IstSichtbar);
                if (au == 0) return result + "keine Positionen";
                result += au == 1? "eine Position, ": $"{au} Positionen, ";

                var sp = SelektiertePositionen.Count();
                if (sp == 0) return result + "keine ausgewählt";
                result += sp == 1? "eine ausgewählt": $"{sp} ausgewählt";

                return result;
            }
        }


        public String FilterInfo => Statistik.GebeFilterText();


        public RingCursor<IPlanzeile> FindAll(String searchTerm)
            => new RingCursor<IPlanzeile>(Zeilen.Where(p => p.Matches(searchTerm)));


        public IEnumerable<Planaufgabe> GebePositionen(object sender)
        {

            var aufgabe = sender as Planaufgabe;
            if (aufgabe != null) return new List<Planaufgabe> { aufgabe };

            var zeile = sender as IPlanzeile;
            if (zeile != null) return zeile.GebePositionen();

            var spalte = sender as Planspalte;
            if (spalte != null) return AlleMassnahmen.SelectMany(p => p.GebePositionen(spalte.Index, spalte.Index));

            if (sender is Rect) {

                var rect = CoordToIndex((Rect)sender);
                return Zeilen.Skip((int)rect.Top).Take((int)rect.Height + 1).SelectMany(p => p.GebePositionen((int)rect.Left, (int)rect.Right));
            }

            if (sender is Point) {

                var cell = CoordToIndex((Point)sender);
                return Zeilen[(int)cell.Y]?.GebePositionen((int)cell.X, (int)cell.X) ?? new List<Planaufgabe>();
            }

            return new List<Planaufgabe>();
        }


        public double GebeYPosition(IPlanzeile zeile)
            => Zeilen.TakeWhile(p => p != zeile).Sum(p => p.Height);


        public double TotalHeight  { get; private set; }


        public double TotalWidth => Spalten.Count * Spaltenbreite;


        public void CalculateHeights()
        {

            var calculator = new UpdateHeightsVisitor();
            Accept(calculator);
            TotalHeight = calculator.CurrentHeight;
        }


        public Planzelle GebeZelle(Point position)
        {

            var cell = CoordToIndex(position);
            var row = Zeilen[(int)cell.Y] as MNPlanzeile;
            if (row == null) return null;
            return row.Perioden[(int)cell.X];
        }


        /// <summary>
        /// Wenn Zelle mehrere Aufgaben hat: welche wurde geklickt?
        /// </summary>
        public Planaufgabe GebeAufgabe(Point position)
        {

            var cell = GebeZelle(position);

            if (cell == null) return null;
            if (cell.Aufgaben.IsNullOrEmpty()) return null;
            if (cell.Aufgaben.Count == 1) return cell.Aufgaben.First();

            int ypos = OffsetInZeile(position.Y);
            int zeilenhöhe = cell.Parent.MaxAmount * Aufgabenhöhe;
            int zellenhöhe = cell.Aufgaben.Count * Aufgabenhöhe;
            int zellstart = (zeilenhöhe - zellenhöhe) / 2;
            if (ypos < zellstart || ypos > zellstart + zellenhöhe) return null;
            int ndx = (ypos - zellstart) / Aufgabenhöhe;

            if (ndx < 0 || ndx >= cell.Aufgaben.Count) return null;
            return cell.Aufgaben[ndx];
        }


        /// <summary>
        /// Period-Spaltenindex in der Planung ab 0
        /// </summary>
        public int? DatumToSpaltenIndex(DateTime? date)
        {

            if (Spalten.IsNullOrEmpty() || !date.HasValue) return null;
            if (date.Value < Spalten.First().Begin) return null;
            if (date.Value >= Spalten.Last().BeginNext) return null;

            return Spalten.Where(p => p.Begin <= date.Value).Count() - 1;
        }


        public int SpaltenIndex(double xpos) => Math.Max(0, Math.Min(Spalten.Count - 1, (int)(xpos / Spaltenbreite)));


        public int ZeilenIndex(double ypos)
        {

            if (ypos > TotalHeight) return -1;
            for (int i = 0; i < Zeilen.Count; i++)
                if (Zeilen[i].YStartposition <= ypos &&
                    Zeilen[i].YStartposition + Zeilen[i].Height > ypos) return i;
            return -1;
        }


        public int ZeilenIndexOrMax(double ypos)
        {

            if (ypos > TotalHeight) return Zeilen.Count - 1;
            for (int i = 0; i < Zeilen.Count; i++)
                if (Zeilen[i].YStartposition <= ypos &&
                    Zeilen[i].YStartposition + Zeilen[i].Height > ypos) return i;
            return -1;
        }


        /// <summary>
        /// Herausfinden wieviele Pixel in Y-Richtung sich der Klick vom Rand befindet (welche Aufgabe geklickt)
        /// </summary>
        public int OffsetInZeile(double ypos)
        {

            if (ypos > TotalHeight) return -1;
            return (int)(ypos - Zeilen[ZeilenIndex(ypos)].YStartposition);
        }


        /// <summary>
        /// Pixelkoordinaten eines Punktes (mit Maus angeklickte Stelle in Spalten- und Zeilenindex umrechnen)
        /// </summary>
        public Point CoordToIndex(Point coords)
            => new Point(SpaltenIndex(coords.X), ZeilenIndex(coords.Y));


        /// <summary>
        /// Pixelkoordinaten eines Rechteckes (mit Maus aufgezogene Selection box in Spalten- und Zeilenindizies umrechnen)
        /// </summary>
        public Rect CoordToIndex(Rect coords)
            => new Rect(new Point(SpaltenIndex(coords.Left), ZeilenIndex(coords.Top)),
                        new Point(SpaltenIndex(coords.Right), ZeilenIndex(coords.Bottom)));


        public void Accept(IVisitor v)
        {

            v.Visit(this);
            AlleStandorte.ForEach(p => p.Accept(v));
            Spaltensummen.ForEach(p => p.Accept(v));
            v.VisitAfter(this);
        }


        public bool IsChanged => ModifiziertePositionen.Any();


        public void AcceptChanges() => ModifiziertePositionen.ForEach(p => p.AcceptChanges());


        public void VerteileMaterial(List<(int id, decimal menge)> lager)
        {

            var dic = lager.ToDictionary(p => p.id, p => p.menge);
            var mtlist = OffenePositionen.Where(p => !p.Materialien.IsNullOrEmpty())
                                         .Where(p => p.FälligAm.HasValue)
                                         .Where(p => !p.IstVergangenheit)
                                         .OrderBy(p => p.FälligAm.Value)
                                         .SelectMany(p => p.Materialien)
                                         .Where(p => p.Erforderlich)
                                         .Where(p => p.MaterialId.HasValue)
                                         .Where(p => p.MengeBenötigt > 0);

            foreach (var mt in mtlist) {


                mt.MengeZugewiesen = 0;
                var vorhanden = dic.GetValueOrDefault(mt.MaterialId.Value);

                if (vorhanden <= 0) continue;
                if (vorhanden <= mt.MengeBenötigt) {

                    dic[mt.MaterialId.Value] = 0;
                    mt.MengeZugewiesen = vorhanden;

                } else {

                    dic[mt.MaterialId.Value] -= mt.MengeBenötigt;
                    mt.MengeZugewiesen = mt.MengeBenötigt;
                }
            }
        }
    }
}
