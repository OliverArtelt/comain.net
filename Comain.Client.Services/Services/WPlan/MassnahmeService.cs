﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;
using Comain.Client.Gateways;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.ViewData.WPlan;


namespace Comain.Client.Services.WPlan
{

    public class MassnahmeService : IMassnahmeService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public MassnahmeService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public Task<List<MassnahmeRow>> MassnahmenReportAsync(WPlanCreateInfo filter, CancellationToken ct = default)
        {
            return store.PostAsync<WPlanCreateInfo, List<MassnahmeRow>>("/WPlan/MNReport", filter, ct);
        }


        public Task<List<AuftragReportItem>> WPlanReportAsync(IEnumerable<int> auids, CancellationToken ct = default)
        {
            return store.PostAsync<IEnumerable<int>, List<AuftragReportItem>>("/WPlan/Report", auids, ct);
        }


        public async Task<IList<MassnahmeViewItem>> MassnahmenReadAsync(int ihid, CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<MassnahmeListDto>>($"/Massn/List/{ihid}", ct);
            return mapper.Map<List<MassnahmeViewItem>>(qry.OrderBy(p => p.Status).ThenBy(p => p.Name));
        }


        public async Task MassnahmenSaveAsync(TrackableCollection<MassnahmeViewItem> mnlist, CancellationToken ct = default)
        {

            var modified = mapper.AsDtoList<MassnahmeViewItem, MassnahmeListDto>(mnlist);
            await store.PutAsync<IList<MassnahmeListDto>>($"/Massn/List", modified, ct);
        }


        public async Task<IList<AuftragViewItem>> WartungReadAsync(WPlanCreateInfo args, CancellationToken ct = default)
        {

            var dtos = await store.PostAsync<WPlanCreateInfo, IList<WartungListDto>>("/ChkLst", args, ct).ConfigureAwait(false);
            return dtos.Select(p => new AuftragViewItem(p)).ToList();
        }


        public Task<String> WartungCreateAsync(WPlanCreateInfo args, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, String>("/ChkLst/Create", args, ct);


        public Task<OrphanedCount> WartungOrphanedAsync(WPlanCreateInfo args, CancellationToken ct = default)
        {
            return store.PostAsync<WPlanCreateInfo, OrphanedCount>("/ChkLst/Orphaned", args, ct);
        }


        public async Task WartungSaveAsync(IList<AuftragViewItem> models, CancellationToken ct = default)
        {

            if (models.IsNullOrEmpty()) return;
            var dtos = models.Where(p => p.IsChanged).Select(p => p.Dto).ToList();
            if (dtos.IsNullOrEmpty()) return;
            await store.PutAsync<IList<WartungListDto>>("/ChkLst", dtos, ct).ConfigureAwait(false);
        }


        public async Task WartungCloseAsync(IList<AuftragViewItem> models, CancellationToken ct = default)
        {

            if (models.IsNullOrEmpty()) return;
            var ids = models.Select(p => p.Id).ToList();

            await store.PostAsync<IList<int>>("/WPlan/Close", ids, ct).ConfigureAwait(false);
        }


        public Task<List<PZFilterItem>> IHTreeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<PZFilterItem>>("/WPlan/IHTree", ct);
        }


        public Task<IList<NummerFilterModel>> FilterGewerkeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct);
        }


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);
        }


        public Task<IList<LeistungsartFilterModel>> FilterLeistungsartenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<LeistungsartFilterModel>>("/Filter/LA", ct);
        }
    }
}
