﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{

    public interface IAuftragConfigController : IController
    {}


    public class AuftragConfigController : ConfigController<AuftragViewModel>, IAuftragConfigController
    {

        private readonly IFilterService filterStore;
        private readonly AuftragViewModel configView;


        public AuftragConfigController(IKonfigService store, IMessageService messages, IControllerBus bus, IFilterService filterStore,
                                       AdminViewModel view, AuftragViewModel configView)

            : base(store, messages, bus, view, configView)

        {

            this.filterStore = filterStore;
            this.configView = configView;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                var cftask = store.GetAsync(bus.Token);
                var latask = filterStore.LeistungsartenAsync(bus.Token);
                await Task.WhenAll(cftask, latask).ConfigureAwait(false);

                var lalist = latask.Result;
                current = cftask.Result;
                configView.SetzeLeistungsarten(lalist);
                configView.Current = current;
            }
        }
    }
}
