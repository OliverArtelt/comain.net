﻿using System;
using System.ComponentModel;


namespace Comain.Client.Models.IHObjekte
{

    public class Kostenstelle: Entity, IDataErrorInfo, IComparable<Kostenstelle>
    {

        private decimal sortierung;
        private String name;
        private String nummer;
        private bool istKundenKostenstelle;
        private TrackableCollection<Standort> zugeordneteStandorte;


        public void UpdateSort()
        {
            if (Decimal.TryParse(nummer, out decimal test)) Sortierung = test;
        }


#region P R O P E R T I E S


        private bool SortEqual
        {
            get => Decimal.TryParse(nummer, out decimal test) && test == sortierung;
        }


        public decimal Sortierung
        {
            get { return sortierung; }
            set { SetProperty(ref sortierung, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set {

                bool sortWasEqual = SortEqual;
                if (SetProperty(ref nummer, value) && sortWasEqual) UpdateSort();
            }
        }


        public bool IstKundenKostenstelle
        {
            get { return istKundenKostenstelle; }
            set { SetProperty(ref istKundenKostenstelle, value); }
        }


        public TrackableCollection<Standort> ZugeordneteStandorte
        {
            get { return zugeordneteStandorte ?? (zugeordneteStandorte = new TrackableCollection<Standort>()); }
            set { SetProperty(ref zugeordneteStandorte, value); }
        }


#endregion


        public int CompareTo(Kostenstelle other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 150) return "Die Bezeichnung darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 16) return "Die Nummer darf aus maximal 16 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
