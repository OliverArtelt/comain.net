﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.Anwesenheiten;

namespace Comain.Client.Services.Anwesenheiten
{

    public interface IAnwesenheitService
    {

        Task<IList<Anwesenheit>>                AnwesenheitenAsync(AssignedPSRequest args, CancellationToken ct = default);
        Task<IEnumerable<AnwesenheitReportDto>> ReportAsync(AssignedPSRequest args, CancellationToken ct = default);
        Task<IEnumerable<AnwesenheitReportDto>> ReportAsync(AssignedCopyRequest args, CancellationToken ct = default);
        Task<IList<MassnahmeListDto>>           MassnahmenAsync(int ihid, CancellationToken ct = default);

        Task                                    SaveZeitplanAsync(AssignedPSResponse args, CancellationToken ct = default);
        Task                                    SaveMassnahmenAsync(IEnumerable<Anwesenheit> list, CancellationToken ct = default);

        Task<IList<Anwesenheit>>                CopyFromAsync(AssignedPSRequest args, CancellationToken ct = default);
        Task                                    CopyToAsync(AssignedCopyRequest args, CancellationToken ct = default);

        Task<IList<IHListDto>>                  IHObjekteAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>        AssetPersonalAsync(int ihid, CancellationToken ct = default);
        Task<IList<PzFilterModel>>              StandorteAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>             KostenstellenAsync(CancellationToken ct = default);
        Task<IList<LeistungsartFilterModel>>    LeistungsartenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>          GewerkeAsync(CancellationToken ct = default);
        Task<IList<String>>                     TätigkeitenAsync(CancellationToken ct = default);
    }
}