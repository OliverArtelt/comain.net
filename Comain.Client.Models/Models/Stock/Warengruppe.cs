﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain;


namespace Comain.Client.Models.Stock
{

    public class Warengruppe : Entity, IDataErrorInfo, IComparable<Warengruppe>
    {
        
        private String   nummer;
        private String   name;


#region P R O P E R T I E S


        public String Nummer 
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public String Name 
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
      

#endregion

        
        public int CompareTo(Warengruppe other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Name"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Die Warengruppe muss eine Nummer besitzen";
                    if (Nummer.Length > 4) return "Die Nummer der Warengruppe darf aus maximal 4 Zeichen bestehen."; 
                    break;

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Die Warengruppe muss einen Namen besitzen";
                    if (Name.Length > 32) return "Der Name der Warengruppe darf aus maximal 32 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
