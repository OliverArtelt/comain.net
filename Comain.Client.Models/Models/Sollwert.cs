﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models
{
   
    public class Sollwert : Entity, IDataErrorInfo, IComparable<Sollwert>
    {

        private String      jahr;
        private decimal?    eigenleistungen;
        private decimal?    materialkosten;
        private decimal?    fremdleistungen;
        private int?        störanzahl;
        private decimal?    ausfallzeit;
        private decimal?    störkosten;
        private decimal?    produktionsmenge;
        private String      einheit;


#region P R O P E R T I E S

        
        public String Jahr
        {
            get { return jahr; }
            set { SetProperty(ref jahr, value); }
        }
        
        public decimal? Eigenleistungen
        {
            get { return eigenleistungen; }
            set { SetProperty(ref eigenleistungen, value); }
        }
        
        public decimal? Materialkosten
        {
            get { return materialkosten; }
            set { SetProperty(ref materialkosten, value); }
        }
        
        public decimal? Fremdleistungen
        {
            get { return fremdleistungen; }
            set { SetProperty(ref fremdleistungen, value); }
        }
        
        public int? Störanzahl
        {
            get { return störanzahl; }
            set { SetProperty(ref störanzahl, value); }
        }
        
        public decimal? Ausfallzeit
        {
            get { return ausfallzeit; }
            set { SetProperty(ref ausfallzeit, value); }
        }
        
        public decimal? Störkosten
        {
            get { return störkosten; }
            set { SetProperty(ref störkosten, value); }
        }
        
        public decimal? Produktionsmenge
        {
            get { return produktionsmenge; }
            set { SetProperty(ref produktionsmenge, value); }
        }
        
        public String Einheit
        {
            get { return einheit; }
            set { SetProperty(ref einheit, value); }
        }


#endregion

        
        public int CompareTo(Sollwert other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Jahr.CompareNatural(other.Jahr);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Jahr"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Jahr":

                    int jahr;
                    if (!Int32.TryParse(Jahr, out jahr)) return "Geben Sie eine gültige Jahreszahl an."; 
                    if (jahr < 1900 || jahr > 2100) return "Geben Sie eine gültige Jahreszahl an."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
