﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class SbSuListDto
    {

        public int      Id              { get; set; }
        public String   Nummer          { get; set; }
        public String   Name            { get; set; }
        public int      Minuten         { get; set; }
        public decimal  Kosten          { get; set; }
        public decimal  MinutenProzent  { get; set; }
        public decimal  KostenProzent   { get; set; }
        
        public String  NameUndMinutenProzent  { get { return String.Format("{1}  ({0:P})", MinutenProzent, Name); } }
        public String  NameUndKostenProzent   { get { return String.Format("{1}  ({0:P})", KostenProzent, Name); } }
    }
}
