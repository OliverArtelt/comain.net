﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{

    public class Wareneinlagerung : Entity, IDataErrorInfo
    {

        private decimal         menge;
        private decimal         restmenge;
        private decimal?        preis;
        private DateTime        datum;
        private Wareneingang    wareneingang;
        private Lagerort        lagerort;
        private Artikel         material;


        public Wareneinlagerung()
        {}


        public Wareneinlagerung(Wareneingang copyFrom)
        {

            Menge        = copyFrom.EinlagerbareMenge;
            Restmenge    = this.Menge;
            Preis        = copyFrom.Bestellposition.Preis;
            Datum        = DateTime.Today;
            Wareneingang = copyFrom;
            Material     = copyFrom.Material;
        }


        public Wareneinlagerung(Artikel copyFrom)
        {

            Menge        = 1;
            Restmenge    = this.Menge;
            Datum        = DateTime.Today;
            Material     = copyFrom;
        }


        public String Artikelnummer  { get { return material == null? null: material.Nummer; } }
        public String Eingangsnummer { get { return wareneingang == null? null: wareneingang.Nummer; } }
        public String Lagerortnummer { get { return lagerort == null? null: lagerort.Nummer; } }


#region P R O P E R T I E S


        public decimal Menge
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }

        public decimal Restmenge
        {
            get { return restmenge; }
            set { SetProperty(ref restmenge, value); }
        }

        public decimal? Preis
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }

        public DateTime Datum
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }

        public Lagerort Lagerort
        {
            get { return lagerort; }
            set { if (SetProperty(ref lagerort, value)) RaisePropertyChanged("Lagerortnummer"); }
        }

        public Wareneingang Wareneingang
        {
            get { return wareneingang; }
            set { if (SetProperty(ref wareneingang, value)) RaisePropertyChanged("Eingangsnummer"); }
        }

        public Artikel Material
        {
            get { return material; }
            set { if (SetProperty(ref material, value)) RaisePropertyChanged("Artikelnummer"); }
        }


#endregion


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Lagerort"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Restmenge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                return test;
            }
        }


        public List<String> ErrorList
        {

            get {

                var result = new List<String>();

                String test = this["Lagerort"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Restmenge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public List<String> ErrorListWithMaxMenge(decimal? einlagerbareMenge)
        {

            var result = ErrorList;
            if (Menge > einlagerbareMenge.GetValueOrDefault())
                result.Add("Sie können nicht mehr einlagern als aus dem Wareneingang noch verfügbar ist.");
            return result;
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Menge":

                    if (Menge <= 0) return "Die Menge der Einlagerung muss größer 0 sein.";
                    break;

                case "Restmenge":

                    if (Restmenge < 0) return "Die Restmenge der Einlagerung darf nicht kleiner 0 sein.";
                    if (Restmenge > Menge) return "Die Restmenge der Einlagerung kann nicht größer als deren Menge sein.";
                    break;

                case "Datum":

                    if (Datum.Year < 2000 || Datum.Year > 2100) return "Geben Sie ein plausibles Einlagerungsdatum an.";
                    break;

                case "Lagerort":

                    if (Lagerort == null) return "Geben Sie den Lagerort an.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
