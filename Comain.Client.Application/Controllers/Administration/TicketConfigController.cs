﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{

    public interface ITicketConfigController : IController
    {}


    public class TicketConfigController : ConfigController<TicketAdminViewModel>, ITicketConfigController
    {

        private readonly IFilterService filterStore;
        private readonly TicketAdminViewModel configView;


        public TicketConfigController(IKonfigService store, IMessageService messages, IControllerBus bus, IFilterService filterStore,
                                      AdminViewModel view, TicketAdminViewModel configView)

            : base(store, messages, bus, view, configView)

        {

            this.filterStore = filterStore;
            this.configView = configView;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                var cftask = store.GetAsync(bus.Token);
                var pstask = filterStore.PersonalAsync(bus.Token);
                await Task.WhenAll(cftask, pstask).ConfigureAwait(false);

                current = cftask.Result;
                configView.SetzeListen(pstask.Result);
                configView.Current = current;
            }
        }
    }
}
