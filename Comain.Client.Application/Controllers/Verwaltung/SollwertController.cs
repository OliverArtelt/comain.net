﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface ISollwertController : IVwTypController 
    {}
    
    
    public class SollwertController : SimpleController<SollwertDto, Sollwert, ISollwertView>, ISollwertController
    {
        
        public SollwertController(IMessageService messages, ISimpleService<SollwertDto, Sollwert> store, IControllerBus bus,
                                  VerwaltungViewModel mainView, SimpleViewModel<Sollwert, ISollwertView> view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/SW"; }
        }
    }
}
