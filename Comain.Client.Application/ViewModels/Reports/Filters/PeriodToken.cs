﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewModels.Reports.Filters
{

    public class PeriodToken : NotifyBase
    {

        public String Token         { get; set; }
        public String Caption       { get; set; }


        private bool highlighted;
        public bool Highlighted
        {
            get => highlighted;
            set => SetProperty(ref highlighted, value);
        }


        public override string ToString()
        {
            return Caption;
        }
    }
}
