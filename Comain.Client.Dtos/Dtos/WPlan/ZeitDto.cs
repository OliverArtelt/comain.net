﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan
{

    public class ZeitDto
    {

        public DateTime   Begin         { get; set; }
        public DateTime   End           { get; set; }
        public String     Name          { get; set; }
        public bool       IstFeiertag   { get; set; }
    }
}
