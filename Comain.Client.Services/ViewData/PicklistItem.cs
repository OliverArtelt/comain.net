﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData
{
    
    public class PicklistItem
    {

        public int      Id      { get; set; }
        public String   Name    { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }
    

    public class PicklistItem<T>
    {

        public T        Id      { get; set; }
        public String   Name    { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }
}
