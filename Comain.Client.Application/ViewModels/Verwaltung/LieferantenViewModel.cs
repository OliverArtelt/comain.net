﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{
    
    public class LieferantenViewModel : SimpleViewModel<Lieferant, ILieferantView>, IDelayableViewModel
    {

        public LieferantenViewModel(ILieferantView view) : base(view)
        {}


        public override void CurrentChanged(Lieferant oldSelected)
        {
            
            RaisePropertyChanged("Name1");                  
            RaisePropertyChanged("Name2");               
            RaisePropertyChanged("Strasse");              
            RaisePropertyChanged("Plz");                  
            RaisePropertyChanged("Ort");                   
            RaisePropertyChanged("Telefon");           
            RaisePropertyChanged("Fax");                    
            RaisePropertyChanged("EMail");               
            RaisePropertyChanged("Rechnungsanschrift");    
            RaisePropertyChanged("LieferantFürIHObjekte");  
            RaisePropertyChanged("LieferantFürAufträge");  
            RaisePropertyChanged("Ansprechpartnerliste");  
            RaisePropertyChanged("CurrentAP");  
        }


        private bool Predicate(object arg)
        {

            var lf = arg as Lieferant;
            if (lf == null) return false;

            if (filterMT == filterIH) return true;
            if (filterMT && !lf.LieferantFürAufträge) return false;
            if (filterIH && !lf.LieferantFürIHObjekte) return false;
            return true;
        }


        private void Refresh()
        {
            
            context.Post(new SendOrPostCallback((o) => { 
            
                if (Lieferanten != null) Lieferanten?.Refresh();        
                RaisePropertyChanged("Lieferanten");
            }), null);
        }   


        public override void Unload()
        {
            
            Lieferanten = null;
            RaisePropertyChanged("Lieferanten");
            base.Unload();
        }


        public override void SetList(ObservableCollection<Lieferant> liste)
        {

            base.SetList(liste);
            Lieferanten = new CollectionViewSource { Source = liste }.View;
            Lieferanten.Filter = Predicate;
            Refresh();
        }


#region P R O P E R T I E S


        private ICommand combineCommand;
        public ICommand CombineCommand
        { 
            get { return combineCommand; }
            set { SetProperty(ref combineCommand, value); }
        }


        private ICommand addAPCommand;
        public ICommand AddAPCommand
        { 
            get { return addAPCommand; }
            set { SetProperty(ref addAPCommand, value); }
        }


        private ICommand deleteAPCommand;
        public ICommand DeleteAPCommand
        { 
            get { return deleteAPCommand; }
            set { SetProperty(ref deleteAPCommand, value); }
        }


        public TrackableCollection<LieferantAP> Ansprechpartnerliste
        { 
            get { return current == null? null: current.Ansprechpartnerliste; }
            set { 
            
                if (current != null && current.Ansprechpartnerliste != value) {

                    current.Ansprechpartnerliste = value;
                    RaisePropertyChanged("Ansprechpartnerliste");
                }
            }
        }


        private LieferantAP currentAP;
        public LieferantAP CurrentAP
        { 
            get { return currentAP; }
            set { SetProperty(ref currentAP, value); }
        }


        public ICollectionView Lieferanten { get; private set; }


        private bool filterIH = true;  
        public bool FilterIH  
        {
            get { return filterIH; }
            set { if (SetProperty(ref filterIH, value)) Refresh(); }
        }


        private bool filterMT = true;  
        public bool FilterMT  
        {
            get { return filterMT; }
            set { if (SetProperty(ref filterMT, value)) Refresh(); }
        }


        public String Name1                  
        {
            get { return current == null? null: current.Name1; }
            set { 
            
                if (current != null && current.Name1 != value) {

                    current.Name1 = value;
                    RaisePropertyChanged("Name1");
                }
            }
        }
        

        public String Name2               
        {
            get { return current == null? null: current.Name2; }
            set { 
            
                if (current != null && current.Name2 != value) {

                    current.Name2 = value;
                    RaisePropertyChanged("Name2");
                }
            }
        }
        

        public String Strasse              
        {
            get { return current == null? null: current.Strasse; }
            set { 
            
                if (current != null && current.Strasse != value) {

                    current.Strasse = value;
                    RaisePropertyChanged("Strasse");
                }
            }
        }
        

        public String Plz                  
        {
            get { return current == null? null: current.Plz; }
            set { 
            
                if (current != null && current.Plz != value) {

                    current.Plz = value;
                    RaisePropertyChanged("Plz");
                }
            }
        }

        
        public String Ort                   
        {
            get { return current == null? null: current.Ort; }
            set { 
            
                if (current != null && current.Ort != value) {

                    current.Ort = value;
                    RaisePropertyChanged("Ort");
                }
            }
        }
        

        public String Telefon            
        {
            get { return current == null? null: current.Telefon; }
            set { 
            
                if (current != null && current.Telefon != value) {

                    current.Telefon = value;
                    RaisePropertyChanged("Telefon");
                }
            }
        }
        

        public String Fax                    
        {
            get { return current == null? null: current.Fax; }
            set { 
            
                if (current != null && current.Fax != value) {

                    current.Fax = value;
                    RaisePropertyChanged("Fax");
                }
            }
        }
        

        public String EMail               
        {
            get { return current == null? null: current.EMail; }
            set { 
            
                if (current != null && current.EMail != value) {

                    current.EMail = value;
                    RaisePropertyChanged("EMail");
                }
            }
        }
        

        public String Rechnungsanschrift    
        {
            get { return current == null? null: current.Rechnungsanschrift; }
            set { 
            
                if (current != null && current.Rechnungsanschrift != value) {

                    current.Rechnungsanschrift = value;
                    RaisePropertyChanged("Rechnungsanschrift");
                }
            }
        }
        

        public bool LieferantFürIHObjekte  
        {
            get { return current == null? false: current.LieferantFürIHObjekte; }
            set { 
            
                if (current != null && current.LieferantFürIHObjekte != value) {

                    current.LieferantFürIHObjekte = value;
                    RaisePropertyChanged("LieferantFürIHObjekte");
                }
            }
        }
        

        public bool LieferantFürAufträge  
        {
            get { return current == null? false: current.LieferantFürAufträge; }
            set { 
            
                if (current != null && current.LieferantFürAufträge != value) {

                    current.LieferantFürAufträge = value;
                    RaisePropertyChanged("LieferantFürAufträge");
                }
            }
        }


#endregion

    }
}
