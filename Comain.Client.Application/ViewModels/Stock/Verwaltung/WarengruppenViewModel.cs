﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.ViewModels.Stock.Verwaltung
{
    
    public class WarengruppenViewModel : SimpleViewModel<Warengruppe, IWarengruppenView>, IDelayableViewModel
    {

        public WarengruppenViewModel(IWarengruppenView view) : base(view)
        {}


        public override void CurrentChanged(Warengruppe oldSelected)
        {
            
            RaisePropertyChanged("Nummer");                  
            RaisePropertyChanged("Name");                   
        }


#region P R O P E R T I E S


        public String Nummer  
        {
            get { return current == null? null: current.Nummer; }
            set { 
            
                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }
        

        public String Name  
        {
            get { return current == null? null: current.Name; }
            set { 
            
                if (current != null && current.Name != value) {

                    current.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }
        

#endregion

    }
}
