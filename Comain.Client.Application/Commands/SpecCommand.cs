﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers.Specs;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class SpecCommand : IControllerCommand
    {

        public Type         Controller  => typeof(ISpecController);
        public String       Name        => "Technische Spezifikation";
        public String       Parameter   => "Spec";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 600;
        public String       Color       => "Chocolate";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.book.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null || !module.VerwendetModulSpecs) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
