﻿using AutoMapper;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;


namespace Comain.Client.Converters.Stock
{

    public class BestellInfoConverter : ITypeConverter<BestellInfoDto, Bestellposition>
    {

        public Bestellposition Convert(BestellInfoDto src, Bestellposition dst, ResolutionContext context)
        {

            if (dst == null) dst = new Bestellposition();
            if (src == null) return dst;

            dst.Bestellung = new Bestellung();
            dst.Bestellung.Id           = src.BestellungId;
            dst.Bestellung.Nummer       = src.Nummer;      
            dst.Bestellung.Auftrag      = src.Auftrag;     
            dst.Bestellung.Kostenstelle = src.Kostenstelle;
            dst.Bestellung.DatumAusgang = src.DatumAusgang;
            dst.Bestellung.Termin       = src.Termin;      
            dst.Id                      = src.PositionId;  
            dst.Position                = src.Position;    
            dst.Menge                   = src.Menge;       
            dst.Preis                   = src.Preis;        
            dst.LfArtikelnummer         = src.LfArtikelnummer;        
  
            return dst;   
        }
    }
}
