﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class FerienDto : ChangeTrackingBase
    {

        public String     Name                    { get; set; }
        public DateTime?  DatumVon                { get; set; }
        public DateTime?  DatumBis                { get; set; }
    }
}
