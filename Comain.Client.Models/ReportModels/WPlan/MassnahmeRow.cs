﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.WPlan;

namespace Comain.Client.ReportModels.WPlan
{

    public class MassnahmeRow
    {

        public String               Standort            { get; set; }
        public String               Kostenstelle        { get; set; }
        public String               Maschinennummer     { get; set; }
        public String               Maschine            { get; set; }
        public String               IHObjekt            { get; set; }
        public String               IHPfad              { get; set; }
        public String               Name                { get; set; }
        public String               Kurzbeschreibung    { get; set; }
        public int                  Zyklus              { get; set; }
        public Weekday              Tagesmuster         { get; set; }
        public String               Leistungsart        { get; set; }
        public String               Gewerk              { get; set; }
        public int?                 Normzeit            { get; set; }
        public Termintyp            Termintyp           { get; set; }
        public Checklistergebnis?   LetztesErgebnis     { get; set; }
        public DateTime?            ZuletztAm           { get; set; }
        public decimal?             Budget48            { get; set; }
        public decimal?             Budget52            { get; set; }


        public String Status  
            => !ZuletztAm.HasValue && !LetztesErgebnis.HasValue? null:
               !ZuletztAm.HasValue? LetztesErgebnis.Value.AsShortText():
               !LetztesErgebnis.HasValue? ZuletztAm.Value.ToShortDateString():
               $"{ZuletztAm.Value.ToShortDateString()}, {LetztesErgebnis.Value.AsText()}";


        public String ZyklusAsString
            => WiMassnahme.ZyklusAsString(Termintyp, Zyklus, Tagesmuster);
    }
}
