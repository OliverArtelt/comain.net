﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Views.Verwaltung;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Users;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class PersonalDetailViewModel : ValidatableViewModel<IPersonalDetailView, Personal>
    {

        public PersonalDetailViewModel(IPersonalDetailView view) : base(view)
        {
        }


        public override void CurrentChanged(Personal oldSelected)
        {

            RaisePropertyChanged("Klammer");
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Nachname");
            RaisePropertyChanged("Vorname");
            RaisePropertyChanged("Strasse");
            RaisePropertyChanged("Plz");
            RaisePropertyChanged("Ort");
            RaisePropertyChanged("Telefon");
            RaisePropertyChanged("Qualifikation");
            RaisePropertyChanged("EMail");
            RaisePropertyChanged("Eintritt");
            RaisePropertyChanged("Austritt");
            RaisePropertyChanged("Gewerk_Id");
            RaisePropertyChanged("Monatssoll");
            RaisePropertyChanged("Stundensatz");
        }


        public virtual void Unload()
        {
            Gewerke = null;
        }


#region P R O P E R T I E S



        public String Klammer
        {
            get { return current == null? null: current.Klammer; }
            set {

                if (current != null && current.Klammer != value) {

                    current.Klammer = value;
                    RaisePropertyChanged("Klammer");
                }
            }
        }


        public String Nummer
        {
            get { return current == null? null: current.Nummer; }
            set {

                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }


        public String Nachname
        {
            get { return current == null? null: current.Nachname; }
            set {

                if (current != null && current.Nachname != value) {

                    current.Nachname = value;
                    RaisePropertyChanged("Nachname");
                }
            }
        }


        public String Vorname
        {
            get { return current == null? null: current.Vorname; }
            set {

                if (current != null && current.Vorname != value) {

                    current.Vorname = value;
                    RaisePropertyChanged("Vorname");
                }
            }
        }


        public String Strasse
        {
            get { return current == null? null: current.Strasse; }
            set {

                if (current != null && current.Strasse != value) {

                    current.Strasse = value;
                    RaisePropertyChanged("Strasse");
                }
            }
        }


        public String Plz
        {
            get { return current == null? null: current.Plz; }
            set {

                if (current != null && current.Plz != value) {

                    current.Plz = value;
                    RaisePropertyChanged("Plz");
                }
            }
        }


        public String Ort
        {
            get { return current == null? null: current.Ort; }
            set {

                if (current != null && current.Ort != value) {

                    current.Ort = value;
                    RaisePropertyChanged("Ort");
                }
            }
        }


        public String Telefon
        {
            get { return current == null? null: current.Telefon; }
            set {

                if (current != null && current.Telefon != value) {

                    current.Telefon = value;
                    RaisePropertyChanged("Telefon");
                }
            }
        }


        public String Qualifikation
        {
            get { return current == null? null: current.Qualifikation; }
            set {

                if (current != null && current.Qualifikation != value) {

                    current.Qualifikation = value;
                    RaisePropertyChanged("Qualifikation");
                }
            }
        }


        public String EMail
        {
            get { return current == null? null: current.EMail; }
            set {

                if (current != null && current.EMail != value) {

                    current.EMail = value;
                    RaisePropertyChanged("EMail");
                }
            }
        }


        public DateTime? Eintritt
        {
            get { return current == null? null: current.Eintritt; }
            set {

                if (current != null && current.Eintritt != value) {

                    current.Eintritt = value;
                    RaisePropertyChanged("Eintritt");
                }
            }
        }


        public DateTime? Austritt
        {
            get { return current == null? null: current.Austritt; }
            set {

                if (current != null && current.Austritt != value) {

                    current.Austritt = value;
                    RaisePropertyChanged("Austritt");
                }
            }
        }


        public int? Gewerk_Id
        {
            get { return current == null? null: current.Gewerk_Id; }
            set {

                if (current != null && current.Gewerk_Id != value) {

                    current.Gewerk_Id = value;
                    RaisePropertyChanged("Gewerk_Id");
                }
            }
        }


        public int? Monatssoll
        {
            get { return current == null? null: current.Monatssoll; }
            set {

                if (current != null && current.Monatssoll != value) {

                    current.Monatssoll = value;
                    RaisePropertyChanged("Monatssoll");
                }
            }
        }


        public decimal? Stundensatz
        {
            get { return current == null? null: current.Stundensatz; }
            set {

                if (current != null && current.Stundensatz != value) {

                    current.Stundensatz = value;
                    RaisePropertyChanged("Stundensatz");
                }
            }
        }


        private IList<NummerFilterModel> gewerke;
        public IList<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


#endregion

    }
}
