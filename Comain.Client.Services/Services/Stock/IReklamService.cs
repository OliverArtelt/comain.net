﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Reklamationen
{

    public interface IReklamService
    {

        Task<IList<RKEingangDto>>   EingängeAsync(CancellationToken ct = default);
        Task<Bestellung>            BestellungAsync(int id, CancellationToken ct = default);
        Task<IList<RKListDto>>      ListAsync(CancellationToken ct = default);
        Task<IList<RKReportDto>>    ReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<Konfiguration>         KonfigAsync(CancellationToken ct = default);

        Task<Reklamation>           DetailAsync(int id, CancellationToken ct = default);
        Task<Reklamation>           UpdateAsync(Reklamation model, CancellationToken ct = default);
        Task                        DeleteAsync(int id, CancellationToken ct = default);
    }
}
