﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Anwesenheiten;

namespace Comain.Client.ViewModels.Anwesenheiten
{

    public class ZeitplanViewModel: PrintViewModel<IZeitplanView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly IHSelektorViewModel ihFilter;
        private IList<PersonalFilterModel> allePersonen;


        public ZeitplanViewModel(IZeitplanView view, IHSelektorViewModel ihFilter, IMessageService messages) : base(messages, view)
        {

            context = SynchronizationContext.Current;
            this.ihFilter = ihFilter;
            PropertyChangedEventManager.AddHandler(ihFilter, ViewPropertyChanged, String.Empty);

            PersonAddCommand = new DelegateCommand(Add);
            PersonRemoveCommand = new DelegateCommand(Remove);
        }


        public async Task SetzeListenAsync(IList<IHListDto> ihlist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                           IList<String> taetlist, CancellationToken ct = default)
        {

            await ihFilter.LoadAsync(ihlist.Where(p => Comain.Client.Models.IHObjekte.IHObjekt.IstMaschinennummer(p.Nummer))
                                           .Select(p => new IHFilterItem(p)).ToList(),
                                     pzlist, kstlist, ct);
            Tätigkeiten = new ObservableCollection<String>(taetlist);
        }


        public void SetzeAllePersonen(IList<PersonalFilterModel> allePersonen)
        {

            this.allePersonen = allePersonen;
            CurrentPerson = null;
            Personen = new CollectionViewSource { Source = allePersonen }.View;
            Personen.Filter = PersonPredicate;
            Refresh();
        }


        private bool PersonPredicate(object o)
        {

            var ps = o as PersonalFilterModel;
            if (ps == null) return false;
            if (Anwesenheiten.IsNullOrEmpty()) return true;
            return Anwesenheiten.All(p => p.Personal == null || p.Personal.Id != ps.Id);
        }


        private void Refresh()
        {

            if (Personen == null) return;
            context.Send(new SendOrPostCallback((o) => { Personen?.Refresh(); }), null);
        }


        public void AddList(IEnumerable<Anwesenheit> list)
        {

            if (list.IsNullOrEmpty() || !Datum.HasValue) return;

            var psids = Anwesenheiten?.Select(p => p.Personal.Id)?.ToArray() ?? new int[] {};
            var newList = list.Where(p => !psids.Any(q => q == p.Personal.Id));
            newList.ForEach(p => { p.Datum = Datum.Value; p.Id = 0; });
            Anwesenheiten.AddRange(newList);
            Refresh();
        }


        private void Add()
        {

            if (CurrentPerson == null || Anwesenheiten.Any(p => p.Personal.Id == CurrentPerson.Id)) return;

            var model = new Anwesenheit { Asset_Id = IHObjekt?.Id ?? 0, Personal = CurrentPerson, Datum = Datum,
                                          UhrzeitVon = UhrzeitVon, UhrzeitBis = UhrzeitBis, Pause = Pause };
            Anwesenheiten.Add(model);
            CurrentAnwesenheit = model;
            CurrentPerson = null;
            Refresh();
            RaisePropertyChanged(nameof(Anwesenheiten));
        }


        private void Remove()
        {

            if (CurrentAnwesenheit == null) return;

            Anwesenheiten.Remove(CurrentAnwesenheit);
            CurrentAnwesenheit = null;
            Refresh();
            RaisePropertyChanged(nameof(Anwesenheiten));
        }


        public void UnloadReport()
        {
            base.Unload();
        }


        public override void Unload()
        {

            Anwesenheiten = null;
            Personen = null;
            allePersonen = null;
            ihFilter.Unload();
            UnloadReport();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (sender == ihFilter && args.PropertyName == nameof(ihFilter.CurrentIHObjekt))
                SelectAssetCommand?.Execute(sender);
        }


        private void UpdateTätigkeitenliste(String entry)
        {

            if (String.IsNullOrWhiteSpace(entry)) return;
            if (Tätigkeiten == null) return;
            if (Tätigkeiten.Contains(entry)) return;
            Tätigkeiten.Add(entry);
        }


        public AssignedPSRequest RequestFilter(Konfiguration cfg)
        {

            if (!Datum.HasValue) return null;
            if (IHObjekt == null) return null;
            if (cfg.StandardSchichtUhrzeitFilter && !UhrzeitVon.HasValue) return null;
            return new AssignedPSRequest { Asset_Id = IHObjekt.Id, Datum = Datum.Value, UhrzeitVon = cfg.StandardSchichtUhrzeitFilter? UhrzeitVon: (TimeSpan?)null };
        }


        public AssignedPSResponse ResponseFilter(Konfiguration cfg)
        {

            if (!Datum.HasValue) return null;
            if (IHObjekt == null) return null;
            if (cfg.StandardSchichtUhrzeitFilter && !UhrzeitVon.HasValue) return null;
            return new AssignedPSResponse { Asset_Id = IHObjekt.Id, Datum = Datum.Value, UhrzeitVon = cfg.StandardSchichtUhrzeitFilter? UhrzeitVon: (TimeSpan?)null };
        }


#region P R O P E R T I E S


        private DateTime? datum = DateTime.Today;
        public DateTime? Datum
        {
            get => datum;
            set { if (SetProperty(ref datum, value)) SelectDateTimeCommand?.Execute(this); }
        }


        public TimeSpan? UhrzeitVon   { get; private set; }
        public TimeSpan? UhrzeitBis   { get; private set; }


        public String UhrzeitVonInput
        {
            get => !UhrzeitVon.HasValue ? null: UhrzeitVon.Value.ToString("hh\\:mm");
            set {

                var time = value.ParseTime();
                if (UhrzeitVon != time) {

                    UhrzeitVon = time;
                    RaisePropertyChanged(nameof(UhrzeitVon));
                    RaisePropertyChanged(nameof(UhrzeitVonInput));
                    SelectDateTimeCommand?.Execute(this);
                }
            }
        }


        public String UhrzeitBisInput
        {
            get => !UhrzeitBis.HasValue ? null: UhrzeitBis.Value.ToString("hh\\:mm");
            set {

                var time = value.ParseTime();
                if (UhrzeitBis != time) {

                    UhrzeitBis = time;
                    RaisePropertyChanged(nameof(UhrzeitBis));
                    RaisePropertyChanged(nameof(UhrzeitBisInput));
                }
            }
        }


        private decimal? pause;
        public decimal? Pause
        {
            get => pause;
            set => SetProperty(ref pause, value);
        }


        public object           IHSelektor   => ihFilter.View;
        public IHFilterItem     IHObjekt     => ihFilter.CurrentIHObjekt;


        private TrackableCollection<Anwesenheit> anwesenheiten;
        public TrackableCollection<Anwesenheit> Anwesenheiten
        {
            get => anwesenheiten ?? (anwesenheiten = new TrackableCollection<Anwesenheit>());
            set { if (SetProperty(ref anwesenheiten, value)) Refresh(); }
        }


        public ICommand PersonAddCommand     { get; }
        public ICommand PersonRemoveCommand  { get; }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get => saveCommand;
            set => SetProperty(ref saveCommand, value);
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get => cancelCommand;
            set => SetProperty(ref cancelCommand, value);
        }


        private ICommand selectAssetCommand;
        public ICommand SelectAssetCommand
        {
            get => selectAssetCommand;
            set => SetProperty(ref selectAssetCommand, value);
        }


        private ICommand selectDateTimeCommand;
        public ICommand SelectDateTimeCommand
        {
            get => selectDateTimeCommand;
            set => SetProperty(ref selectDateTimeCommand, value);
        }


        private ICommand copyFromCommand;
        public ICommand CopyFromCommand
        {
            get => copyFromCommand;
            set => SetProperty(ref copyFromCommand, value);
        }


        private ICommand copyToCommand;
        public ICommand CopyToCommand
        {
            get => copyToCommand;
            set => SetProperty(ref copyToCommand, value);
        }


        private ICommand printCommand;
        public ICommand PrintCommand
        {
            get => printCommand;
            set => SetProperty(ref printCommand, value);
        }


        private Anwesenheit currentAnwesenheit;
        public Anwesenheit CurrentAnwesenheit
        {
            get { return currentAnwesenheit; }
            set {

                if (value != currentAnwesenheit) {

                    UpdateTätigkeitenliste(currentAnwesenheit?.Tätigkeit);
                    currentAnwesenheit = value;
                    RaisePropertyChanged(nameof(CurrentAnwesenheit));
                }
            }
        }


        private ICollectionView personen;
        public ICollectionView Personen
        {
            get { return personen; }
            set { SetProperty(ref personen, value); }
        }


        private PersonalFilterModel currentPerson;
        public PersonalFilterModel CurrentPerson
        {
            get { return currentPerson; }
            set { SetProperty(ref currentPerson, value); }
        }


        private ObservableCollection<String> tätigkeiten;
        public ObservableCollection<String> Tätigkeiten
        {
            get { return tätigkeiten; }
            set { SetProperty(ref tätigkeiten, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get => backCommand;
            set => SetProperty(ref backCommand, value);
        }


        private String mandant;
        public String Mandant
        {
            get => mandant;
            set => SetProperty(ref mandant, value);
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }

#endregion

    }
}
