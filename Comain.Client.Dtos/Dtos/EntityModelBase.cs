﻿using System;

namespace Comain.Client.Dtos
{

    public class EntityModelBase : IEquatable<EntityModelBase>
    {
        
        public int  Id   { get; set; }


        public bool Equals(EntityModelBase other)
        {
            
            if (other == null) return false;
            return this.Id == other.Id;
        }


        public override bool Equals(object obj)
        {
            return Equals(obj as EntityModelBase);
        }


        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }


        public static bool Equals<T>(T lhs, T rhs) where T: EntityModelBase
        {
            
            if (lhs == null && rhs == null) return true;
            else if (lhs == null | rhs == null) return false;
            else return lhs.Id == rhs.Id;
        }
    }
}
