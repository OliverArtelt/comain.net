﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class MtLfRankReportDto
    {

        public int        LieferantId               { get; set; }
        public String     LieferantName1            { get; set; }
        public String     LieferantName2            { get; set; }
        public String     Strasse                   { get; set; }
        public String     Plz                       { get; set; }
        public String     Ort                       { get; set; }
        public String     Telefon                   { get; set; }
        public String     Fax                       { get; set; }
        public String     EMail                     { get; set; }
        public String     Rechnungsanschrift        { get; set; }

        public String     MtLfName                  { get; set; }
        public String     MtLfNummer                { get; set; }
        public decimal?   MtLfPreis                 { get; set; }
        public short?     MtLfRank                  { get; set; }
        public decimal?   MtLfMindestbestellmenge   { get; set; }
        public int?       MtLfBestellzeit           { get; set; }

        public String     MtName                    { get; set; }
        public String     MtNummer                  { get; set; }
        public String     MtHerstellernummer        { get; set; }
        public decimal?   Mischpreis                { get; set; }
    }
}
