﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class ZeitViewModel : ViewModel<IZeitView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public IEnumerable<PeriodToken> Jahre           { get; private set; }
        public IEnumerable<PeriodToken> Quartale        { get; private set; }
        public IEnumerable<PeriodToken> Monate          { get; private set; }
        public ICommand                 SelectCommmand  { get; private set; }


        public ZeitViewModel(IFilterService store, IZeitView view) : base(view)
        {

            this.store = store;

            SelectCommmand = new DelegateCommand(p => SetPeriod(p));
            Quartale = new List<PeriodToken> { new PeriodToken { Caption = "Quartal I", Token = "Q1" }, new PeriodToken { Caption = "Quartal II", Token = "Q2" },
                                               new PeriodToken { Caption = "Quartal III", Token = "Q3" }, new PeriodToken { Caption = "Quartal IV", Token = "Q4" },
                                               new PeriodToken { Caption = "Startdatum als Woche", Token = "K0" },
                                               new PeriodToken { Caption = "diese Woche", Token = "W0", Highlighted = true },
                                               new PeriodToken { Caption = "nächste Woche", Token = "W1" },
                                               new PeriodToken { Caption = "vorige Woche", Token = "W2" },
                                               new PeriodToken { Caption = "ab heute", Token = "T0" }, new PeriodToken { Caption = "bis heute", Token = "T1" }};
            Monate = new List<PeriodToken> { new PeriodToken { Caption = "Jan", Token = "M1" }, new PeriodToken { Caption = "Feb", Token = "M2" },
                                             new PeriodToken { Caption = "Mrz", Token = "M3" }, new PeriodToken { Caption = "Apr", Token = "M4" },
                                             new PeriodToken { Caption = "Mai", Token = "M5" }, new PeriodToken { Caption = "Jun", Token = "M6" },
                                             new PeriodToken { Caption = "Jul", Token = "M7" }, new PeriodToken { Caption = "Aug", Token = "M8" },
                                             new PeriodToken { Caption = "Sep", Token = "M9" }, new PeriodToken { Caption = "Okt", Token = "M10" },
                                             new PeriodToken { Caption = "Nov", Token = "M11" }, new PeriodToken { Caption = "Dez", Token = "M12" }};
            Reset();
        }


        public void Reset()
        {

            Von = new DateTime(DateTime.Now.Year, 1, 1);
            Bis = Von.Value.AddYears(1).AddDays(-1);
        }


        public String Validiere()
        {

            if (!Von.HasValue || !Bis.HasValue) return "Geben Sie einen gültigen Zeitraum an.";
            if (Von.Value > Bis.Value) return "Geben Sie einen gültigen Zeitraum an.";
            return null;
        }


        public String ValidiereEndtermin()
        {

            if (!Bis.HasValue) return "Geben Sie das Endedatum an.";
            return null;
        }


        public string FilterAlsString()
        {

            if (Von.HasValue && Bis.HasValue) return String.Format("{0} - {1}", Von.Value.ToShortDateString(), Bis.Value.ToShortDateString());
            if (Bis.HasValue) return Bis.Value.ToShortDateString();
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Von.HasValue) filter.Add("ZeitVon", Von);
            if (Bis.HasValue) filter.Add("ZeitBis", Bis);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            Jahre = Enumerable.Range(store.Periode.Von, store.Periode.Bis - store.Periode.Von + 1)
                              .Select(p => new PeriodToken { Token = "Y" + p.ToString(), Caption = p.ToString() })
                              .ToList();
            var period = Jahre.FirstOrDefault(p => p.Caption == DateTime.Now.Year.ToString());
            if (period != null) period.Highlighted = true;

            String currtag = $"M{DateTime.Now.Month}";
            period = Monate.FirstOrDefault(p => p.Token == currtag);
            if (period != null) period.Highlighted = true;

            currtag = $"Q{1 + (DateTime.Now.Month - 1) / 3}";
            period = Quartale.FirstOrDefault(p => p.Token == currtag);
            if (period != null) period.Highlighted = true;

            RaisePropertyChanged("Jahre");
            return Task.FromResult(true);
        }


        public void Unload()
        {
            Jahre = null;
        }


        private void SetPeriod(object context)
        {

            if (context == null) return;
            String token = context.ToString();
            if (String.IsNullOrEmpty(token) || token.Length < 2) return;

            int currentYear = Von.HasValue ? Von.Value.Year: DateTime.Now.Year;
            if (!Int32.TryParse(token.Substring(1), out int iv)) return;

            switch (token[0]) {

                case 'Y':

                    Von = new DateTime(iv, 1, 1);
                    Bis = new DateTime(iv, 12, 31);
                    break;

                case 'Q':

                    Von = new DateTime(currentYear, iv * 3 - 2, 1);
                    Bis = Von.Value.AddMonths(3).AddDays(-1);
                    break;

                case 'M':

                    Von = new DateTime(currentYear, iv, 1);
                    Bis = Von.Value.AddMonths(1).AddDays(-1);
                    break;

                case 'K':

                    if (!von.HasValue) von = DateTime.Now;
                    Von = von.Value.MondayOf();
                    Bis = Von.Value.AddDays(6);
                    break;

                case 'W':

                    Von = DateTime.Today.MondayOf();
                    if (iv == 1) Von = Von.Value.AddDays(7);
                    if (iv == 2) Von = Von.Value.AddDays(-7);
                    Bis = Von.Value.AddDays(6);
                    break;

                case 'T':

                    if (iv == 0) Von = DateTime.Today;
                    else Bis = DateTime.Today;
                    break;
            }
        }


        protected void ReadFilterAsString(StringBuilder bld)
        {

            if (!Von.HasValue && !Bis.HasValue) return;
            if (Von.HasValue && Bis.HasValue) bld.Append(String.Format("Zeitraum {0} - {1}", Von.Value.ToShortDateString(), Bis.Value.ToShortDateString()));
            if (Von.HasValue) bld.Append(String.Format("Zeitraum ab {0}", Von.Value.ToShortDateString()));
            bld.Append(String.Format("Zeitraum bis {0}", Bis.Value.ToShortDateString()));
        }


        public String KwString
        {

            get {

                if (!Von.HasValue || !Bis.HasValue) return null;
                var v = Von.Value.KWString();
                var b = Bis.Value.KWString();
                if (v == b) return String.Format("KW {0}", v);
                return String.Format("KW {0} - {1}", v, b);
            }
        }


#region P R O P E R T I E S


        private DateTime? von;
        public DateTime? Von
        {
            get { return von; }
            set { if (SetProperty(ref von, value)) RaisePropertyChanged("KwString"); }
        }


        private DateTime? bis;
        public DateTime? Bis
        {
            get { return bis; }
            set { if (SetProperty(ref bis, value)) RaisePropertyChanged("KwString"); }
        }


#endregion


    }
}
