﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Reports
{

    public class PlanungsgradDto
    {

        public IList<PgItem>    Jahre       { get; set; }
        public String           PlanLA      { get; set; }
        public String           UnplanLA    { get; set; }
    }
}
