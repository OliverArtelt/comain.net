﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Stock.Warenausgänge;

namespace Comain.Client.Views.Stock.Warenausgänge
{
    /// <summary>
    /// Interaction logic for AusgangListView.xaml
    /// </summary>
    public partial class AusgangListView:UserControl      ,IAusgangListView
    {
        public AusgangListView()
        {
        InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as AusgangListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
