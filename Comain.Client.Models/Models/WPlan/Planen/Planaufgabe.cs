﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Eine einzelne Checklistposition
    /// </summary>
    [DebuggerDisplay("{Info}")]
    public class Planaufgabe : NotifyBase, IAcceptor
    {

        public  Planzelle    Parent        { get; set; }


        public Planaufgabe(Planzelle parent, PositionDto dto)
        {

            Parent = parent;

            Bemerkungen             = dto.Bemerkungen;
            Ergebnis                = (Checklistergebnis)dto.Ergebnis;
            Normzeit                = dto.Normzeit;
            MinutenGearbeitet       = dto.MinutenGearbeitet;
            Wert                    = dto.Wert;
            Menge                   = dto.Menge;
            MengeGearbeitet         = dto.MengeGearbeitet;
            PersonalIds             = dto.PersonalIds;
            FälligAm                = dto.FälligAm;
            DurchgeführtAm          = dto.DurchgeführtAm;

            AuftragId               = dto.Auftrag_Id;
            PositionId              = dto.Position_Id;
            Nummer                  = dto.Auftragsnummer;
            ÜbergabeAm              = dto.ÜbergabeAm;
            Folgeauftrag            = dto.Folgeauftrag;
            Auftragstatus           = (Auftragstatus)dto.Auftragsstatus;

            IstSichtbar             = true;
            IstBearbeitbar          = dto.Position_Id <= 0 || Parent.IstBearbeitbar && Ergebnis == Checklistergebnis.Undefiniert && Auftragstatus == Auftragstatus.Offen;

            Auftragnehmer_Id        = dto.Auftragnehmer_Id;
            Auftraggeber_Id         = dto.Auftraggeber_Id;
            Auftragsleiter_Id       = dto.Auftragsleiter_Id;
            Abnahmeberechtigter_Id  = dto.Abnahmeberechtigter_Id;
            PersonalIds             = dto.PersonalIds ?? new List<int>();

            Materialien             = parent.Parent.MaterialInfo?
                                            .Where(p => p.Erforderlich)
                                            .Select(p => new PlanMaterial(this, p))
                                            .ToList();
        }


        public Plan                 Plan                    => Parent.Plan;
        public Konfiguration        Konfiguration           => Plan.Konfiguration;
        public bool                 IstGeplant              => FälligAm.HasValue;
        public bool                 IstDeaktiviert          => Parent.Parent.IstDeaktiviert;
        public String               DeaktiviertText         => Parent.Parent.DeaktiviertText;
        public bool                 IstTäglich              => Parent.Parent.Termintyp == Termintyp.Tagesplanung;
        public bool                 IstVergangenheit        => ((IstTäglich && FälligAm.HasValue && FälligAm.Value < DateTime.Today) ||
                                                               (!IstTäglich && FälligAm.HasValue && FälligAm.Value.MondayOf() < DateTime.Today.MondayOf()));
        public bool                 IstÜberfällig           => Ergebnis == Checklistergebnis.Undefiniert && IstVergangenheit;
        public bool                 IstErledigt             => Ergebnis == Checklistergebnis.OK || Ergebnis == Checklistergebnis.NotOK || Ergebnis == Checklistergebnis.Fortgeführt;
        public bool                 IstBedarfsposition      => Parent.Parent.IstBedarfsposition;
        public bool                 IstSonderleistung       => !IstBedarfsposition && !FälligAm.HasValue;
        public bool                 IstPlanleistung         => !IstBedarfsposition && !IstSonderleistung;
        public bool                 IstFixiert              => Parent.IstUnveränderlicheKette;
        public bool                 IstOffen                => Parent.IstBearbeitbar && Ergebnis == Checklistergebnis.Undefiniert && Auftragstatus == Auftragstatus.Offen;
        public bool                 HatKosten               => IstErledigt;
        public bool                 HatMaterial             => !Materialien.IsNullOrEmpty();
        public bool                 ErgebnisOffenErlaubt    => Auftragstatus == Auftragstatus.Offen && Parent.IstBearbeitbar;
        public bool                 ErgebnisOkNokErlaubt    => Auftragstatus == Auftragstatus.Offen && Parent.IstBearbeitbar && (Parent.IstAktuell || Parent.IstVergangenheit);
        public bool                 ErgebnisWneErlaubt      => Auftragstatus == Auftragstatus.Offen && Parent.IstBearbeitbar;
        public DateTime?            DisplayDate             => FälligAm ?? DurchgeführtAm;
        public int                  MassnahmeId             => Parent.Parent.Id;
        public String               Massnahme               => Parent.Parent.NameNummer;
        public String               IHObjekt                => Parent.Parent.Parent.Nummer;
        public String               Maschine                => Parent.Parent.Parent.Name;
        public String               Name                    => Parent.Parent.Name;
        public String               Massnahmenummer         => Parent.Parent.Nummer;
        public String               Kurzbeschreibung        => Parent.Parent.Kurzbeschreibung;
        public String               Gewerk                  => Parent.Parent.Gewerk;
        public String               Leistungsart            => Parent.Parent.Leistungsart;
        public String               Kostenstelle            => Parent.Parent.Kostenstelle;
        public String               Ergebniskurztext        => Ergebnis.AsShortText();
        public String               Info                    => $"Position aus {Nummer}, {ErgebnisAlsText}, fällig am {FälligAm}";
        public String               NormzeitText            => Normzeit.HasValue? $"{Normzeit} min": null;
        public String               MinutenGearbeitetText   => HatKosten? $"{MinutenGearbeitet ?? Normzeit} min": null;
        public String               TermGeplanteKosten      => IstGeplant && Wert.HasValue && Menge > 0? $"{Menge} x {Wert.Value.ToString("c")}": null;
        public String               TermVerbrauchteKosten   => Wert.HasValue && HatKosten? $"{MengeGearbeitet ?? Menge} x {Wert.Value.ToString("c")}": null;
        public String               FormelGeplanteKosten    => TermGeplanteKosten != null? $"{TermGeplanteKosten} = {GeplanteKosten.Value.ToString("c")}": null;
        public String               FormelVerbrauchteKosten => TermVerbrauchteKosten != null? $"{TermVerbrauchteKosten} = {VerbrauchteKosten.Value.ToString("c")}": null;
        public String               AnzeigeTerm             => HatKosten? TermVerbrauchteKosten: TermGeplanteKosten;
        public String               AnzeigeFormel           => HatKosten? FormelVerbrauchteKosten: FormelGeplanteKosten;

        public decimal?             GeplanteKosten          => Wert * (Menge ?? 1);
        public decimal?             VerbrauchteKosten       => HatKosten? Wert * (MengeGearbeitet ?? Menge ?? 1): (decimal?)null;
        public decimal?             KostenAngezeigt         => HatKosten? VerbrauchteKosten: Ergebnis == Checklistergebnis.Undefiniert? GeplanteKosten: (decimal?)null;
        public int?                 MinutenAngezeigt        => HatKosten? MinutenGearbeitet ?? Normzeit: Ergebnis == Checklistergebnis.Undefiniert? Normzeit: (int?)null;

        public bool                 IstBearbeitbar          { get; }
        public int                  AuftragId               { get; }
        public int                  PositionId              { get; }
        /// <summary>
        /// Auftragsnummer
        /// </summary>
        public String               Nummer                  { get; }
        public String               Folgeauftrag            { get; }
        public DateTime?            ÜbergabeAm              { get; }
        public Auftragstatus        Auftragstatus           { get; }

        public int?                 Auftraggeber_Id         { get; }
        public int?                 Auftragsleiter_Id       { get; }
        public int?                 Abnahmeberechtigter_Id  { get; }
        /// <summary>
        /// Personal bereits erfolgter Personalleistungen für 'Meine Aufträge anzeigen'
        /// </summary>
        public List<int>            PersonalIds             { get; }

        public bool                 IstSelektiert           { get; private set; }
        public String               Bemerkungen             { get; private set; }
        public Checklistergebnis    Ergebnis                { get; private set; }
        public int?                 Normzeit                { get; private set; }
        public int?                 MinutenGearbeitet       { get; private set; }
        public decimal?             Wert                    { get; private set; }
        public int?                 Menge                   { get; private set; }
        public int?                 MengeGearbeitet         { get; private set; }
        public DateTime?            FälligAm                { get; private set; }
        public DateTime?            DurchgeführtAm          { get; private set; }

        public String               Auftragnehmer           => Plan.PersonalFromId(Auftragnehmer_Id);
        public String               Auftraggeber            => Plan.PersonalFromId(Auftraggeber_Id);
        public String               Auftragsleiter          => Plan.PersonalFromId(Auftragsleiter_Id);
        public String               Abnahmeberechtigter     => Plan.PersonalFromId(Abnahmeberechtigter_Id);
        public String               Personalliste           => PersonalIds.IsNullOrEmpty()? null:
                                                               String.Join(", ", PersonalIds.Select(p => Plan.PersonalFromId(p))
                                                                                            .Where(p => !String.IsNullOrEmpty(p))
                                                                                            .Distinct()
                                                                                            .ToArray());

        private List<PlanMaterial> materialien;
        public List<PlanMaterial> Materialien 
        {
            get => materialien ?? (materialien = new List<PlanMaterial>());
            set => SetProperty(ref materialien, value);
        }


        private int? auftragnehmer_Id;
        public int? Auftragnehmer_Id
        {
            get => auftragnehmer_Id;
            set {

                if (SetProperty(ref auftragnehmer_Id, value)) RaisePropertyChanged(nameof(Auftragnehmer));
            }
        }


        private Plananzeigemodus anzeigemodus;
        public Plananzeigemodus Anzeigemodus
        {
            get { return anzeigemodus; }
            set {

                if (SetProperty(ref anzeigemodus, value)) {

                    RaisePropertyChanged(nameof(AnzeigeWert));
                    RaisePropertyChanged(nameof(WertAlignment));
                    RaisePropertyChanged(nameof(SelectColor));
                    RaisePropertyChanged(nameof(FontColor));
                    RaisePropertyChanged(nameof(BackColor));
                }
            }
        }


        private bool istSichtbar;
        public bool IstSichtbar
        {
            get => istSichtbar;
            set {

                if (istSichtbar != value) {

                    istSichtbar = value;
                    RaisePropertyChanged(nameof(IstSichtbar));
                }
                if (!value && IstSelektiert) IstSelektiert = false;
            }
        }


        public bool ImExportSichtbar(PlanOptions opts)
        {

            if (!IstSichtbar) return false;
            if (IstOffen && !opts.Offen) return false;
            if (!IstOffen && !opts.Erledigt) return false;

            if (IstPlanleistung && !opts.Planleistungen) return false;
            if (IstSonderleistung && !opts.Sonderleistungen) return false;
            if (IstBedarfsposition && !opts.Bedarfspositionen) return false;

            return true;
        }


        public void SetzeAuswahl(bool selectState)
        {

            if (IstSelektiert == selectState) return;
            if (!IstBearbeitbar) return;

            IstSelektiert = selectState;
            RaisePropertyChanged(nameof(IstSelektiert));
            RaisePropertyChanged(nameof(SelectColor));
            RaisePropertyChanged(nameof(BackColor));
            RaisePropertyChanged(nameof(FontColor));
        }


        public void Accept(IVisitor v)
        {
            v.Visit(this);
        }


        /// <remarks>
        /// Fehlerhafte Anweisungen einfach ignorieren da Massenbearbeitung durchgeführt werden könnte
        /// (alles auf WirdNicht Erledigt setzen: nicht meckern wenn es in dieser einen Position nicht geht)
        /// </remarks>
        public void Edit(EditAction action)
        {

            if (!IstBearbeitbar) return;


            if (action.Ergebnis == Checklistergebnis.Undefiniert) {

                Menge             = action.Menge ?? Parent.Parent.Menge ?? 1;
                MengeGearbeitet   = null;
                MinutenGearbeitet = null;
                DurchgeführtAm    = null;
                Ergebnis          = Checklistergebnis.Undefiniert;

            } else if (action.Ergebnis == Checklistergebnis.WirdNichtErledigt) {

                MengeGearbeitet   = null;
                MinutenGearbeitet = null;
                DurchgeführtAm    = null;
                Ergebnis          = Checklistergebnis.WirdNichtErledigt;

            } else if (!Parent.IstZukunft) {

                MinutenGearbeitet = Normzeit ?? Parent.Parent.Normzeit;
                MengeGearbeitet   = action.Menge ?? Menge;
                Ergebnis          = action.Ergebnis;
                DurchgeführtAm    = action.HeuteErledigt? DateTime.Today: FälligAm ?? DateTime.Today;
            }

            RaisePropertyChanged(nameof(IstÜberfällig));
            RaisePropertyChanged(nameof(ErgebnisAlsText));
            RaisePropertyChanged(nameof(SelectColor));
            RaisePropertyChanged(nameof(BackColor));
            RaisePropertyChanged(nameof(FontColor));
            RaisePropertyChanged(nameof(StatusColor));
            RaisePropertyChanged(nameof(AnzeigeWert));
            RaisePropertyChanged(nameof(WertAlignment));
            RaisePropertyChanged(nameof(AnzeigeFormel));
            RaisePropertyChanged(nameof(DurchgeführtAm));
        }


        public void SetDate(DateTime date)
        {
            FälligAm = date;
        }


        public bool HatPersonal(int psid)
            => Auftragnehmer_Id == psid || Auftraggeber_Id == psid || Abnahmeberechtigter_Id == psid || Auftragsleiter_Id == psid ||
               PersonalIds.Any(p => p == psid);


        public void SetPersonal(int? psid)
        {

            Auftragnehmer_Id = psid;
            RaisePropertyChanged(nameof(AnzeigeWert));
        }


        public String WertAlignment => Anzeigemodus == Plananzeigemodus.Personal? "Left": "Center";


        public String AnzeigeWert
        {
            get {

                if (Anzeigemodus == Plananzeigemodus.Zeiten) return MinutenAngezeigt.HasValue? $"{MinutenAngezeigt} min": null;
                if (Anzeigemodus == Plananzeigemodus.Kosten) return KostenAngezeigt.HasValue? KostenAngezeigt.Value.ToString("c"): null;
                if (Anzeigemodus == Plananzeigemodus.Personal) return Auftragnehmer;
                if (Anzeigemodus == Plananzeigemodus.Auftrag) return Nummer;
                if (Anzeigemodus == Plananzeigemodus.Material) {

                    if (Ergebnis != Checklistergebnis.Undefiniert) return null;
                    if (Materialien.IsNullOrEmpty()) return null;
                    if (!IstBearbeitbar) return null;
                    if (IstVergangenheit) return null;
                    if (Materialien.All(p => !p.IstFehlend)) return "vorhanden";
                    return "fehlt";
                }
                return null;
            }
        }


        public String ErgebnisAlsText
        {
            get {

                if (IstÜberfällig) return "überfällig";
                if (Ergebnis == Checklistergebnis.Undefiniert) return "offen";
                if (Ergebnis == Checklistergebnis.OK) return "OK";
                if (Ergebnis == Checklistergebnis.NotOK) return "nicht OK";
                if (Ergebnis == Checklistergebnis.WirdNichtErledigt) return "wird nicht erledigt";
                if (Ergebnis == Checklistergebnis.Fortgeführt) return String.IsNullOrEmpty(Folgeauftrag)? "fortgeführt": $"fortgeführt in {Folgeauftrag}";
                return null;
            }
        }


        public String FontColor
        {
            get {

                if (Anzeigemodus == Plananzeigemodus.Material) {

                    if (Ergebnis != Checklistergebnis.Undefiniert || Materialien.IsNullOrEmpty()) return IstBearbeitbar && IstSelektiert? "White": "Black";
                    if (Materialien.All(p => p.IstFehlend)) return IstBearbeitbar && IstSelektiert? "LightSalmon": "Crimson";
                    if (Materialien.All(p => !p.IstFehlend)) return IstBearbeitbar && IstSelektiert? "LimeGreen": "Green";
                    return IstBearbeitbar && IstSelektiert? "Cornsilk": "GoldenRod";
                }

                return IstBearbeitbar && IstSelektiert? "White": "Black";
            }
        }


        public String BackColor => IstBearbeitbar && IstSelektiert? SelectColor: StatusColor;


        public String SelectColor
        {
            get {


                if (IstÜberfällig) return "GoldenRod";
                if (Ergebnis == Checklistergebnis.Undefiniert) return "Gray";
                if (Ergebnis == Checklistergebnis.OK) return "LimeGreen";
                if (Ergebnis == Checklistergebnis.NotOK) return "Tomato";
                if (Ergebnis == Checklistergebnis.WirdNichtErledigt) return "DarkOrchid";
                if (Ergebnis == Checklistergebnis.Fortgeführt) return "DodgerBlue";
                return "Gray";
            }
        }


        public String StatusColor
        {
            get {

                if (IstÜberfällig) return "#ffef99";
                if (Ergebnis == Checklistergebnis.Undefiniert) return "WhiteSmoke";
                if (Ergebnis == Checklistergebnis.OK) return "#c2f0c2";
                if (Ergebnis == Checklistergebnis.NotOK) return "#ffc8bf";
                if (Ergebnis == Checklistergebnis.WirdNichtErledigt) return "#e0c2f0";
                if (Ergebnis == Checklistergebnis.Fortgeführt) return "#bcdeff";
                return "White";
            }
        }
    }
}
