﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{

    public class AssistentViewModel : ViewModel<IAssistentView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private IList<AuIHKSDto> ihliste;


        public AssistentViewModel(IAssistentView view, IIHFastSearchProvider ihSearcher, IPersonalSearchProvider psSearcher) : base(view)
        {

            context = SynchronizationContext.Current;
            IHSearchProvider = ihSearcher;
            PsSearchProvider = psSearcher;
        }


        public async Task SetFilterAsync(IList<AuIHKSDto> ihliste, IList<PzFilterModel> pzliste, IList<NummerFilterModel> sbliste, IList<PersonalFilterModel> psliste,
                                         Konfiguration config, IList<Dringlichkeit> drliste, IList<SubNummerFilterModel> ssbliste, CancellationToken ct = default)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Pzliste = pzliste;
                Schadensbilder = sbliste;
                Konfiguration = config;
                Dringlichkeiten = drliste;

                this.ihliste = ihliste;
                IHSearchProvider.SetItems(ihliste);
                PsSearchProvider.SetItems(psliste);

                ct.ThrowIfCancellationRequested();

                Maschinen = new CollectionViewSource { Source = ihliste }.View;
                Maschinen.Filter = MaschinenPredicate;
                Maschinen.SortDescriptions.Clear();
                Maschinen.SortDescriptions.Add(new SortDescription { PropertyName="IHObjekt" });
                RefreshMaschinen();

                ct.ThrowIfCancellationRequested();

                Bauteile = new CollectionViewSource { Source = ihliste }.View;
                Bauteile.Filter = BauteilPredicate;
                Bauteile.SortDescriptions.Clear();
                Bauteile.SortDescriptions.Add(new SortDescription { PropertyName="IHObjekt" });
                RefreshBauteile();

                ct.ThrowIfCancellationRequested();

                SubSchadensbilder = new CollectionViewSource { Source = ssbliste }.View;
                SubSchadensbilder.Filter = SubSchadensbildPredicate;
                SubSchadensbilder.SortDescriptions.Clear();
                SubSchadensbilder.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                RefreshSubSchadensbilder();

            }), null);
        }


        private bool MaschinenPredicate(object arg)
        {

            var ih = arg as AuIHKSDto;
            if (ih == null) return false;

            if (Standort != null && Standort.Id != ih.Standort_Id) return false;
            return !String.IsNullOrEmpty(ih.IHObjekt) && ih.IHObjekt.Count(p => p == '-') == 3;
        }


        private bool BauteilPredicate(object arg)
        {

            var ih = arg as AuIHKSDto;
            if (ih == null || ih.IHObjekt == null) return false;

            if (Maschine == null || Maschine.IHObjekt == null) return false;
            return ih.IHObjekt.StartsWith(Maschine.IHObjekt);
        }


        private bool SubSchadensbildPredicate(object arg)
        {

            var ssb = arg as SubNummerFilterModel;
            if (ssb == null) return false;

            if (Schadensbild == null) return false;
            return ssb.Haupt_Id == Schadensbild.Id;
        }


        private void RefreshMaschinen()
        {

            if (ihliste == null || Maschinen == null) return;
            context.Post(new SendOrPostCallback((o) => {

                Maschinen?.Refresh();
                RaisePropertyChanged(nameof(Maschinen));
                Maschine = null;
            }), null);
        }


        private void RefreshBauteile()
        {

            if (ihliste == null || Bauteile == null) return;
            context.Post(new SendOrPostCallback((o) => {

                Bauteile?.Refresh();
                RaisePropertyChanged(nameof(Bauteile));
            }), null);
        }


        private void RefreshSubSchadensbilder()
        {

            if (Schadensbilder == null || SubSchadensbilder == null) return;
            context.Post(new SendOrPostCallback((o) => {

                SubSchadensbilder?.Refresh();
                RaisePropertyChanged(nameof(SubSchadensbilder));
            }), null);
        }


        public int? IHObjekt_Id  =>  FreiesIHObjekt?.Id ?? Bauteil?.Id ?? Maschine?.Id;


        public NeuerAuftragDto GebeAuftrag()
        {

            var validatorMsgs = new List<string>();
            if (!IHObjekt_Id.HasValue)
                validatorMsgs.Add("• Geben Sie die betreffende Maschine / Anlage bzw. das Bauteil an.");
            if (Dringlichkeit == null)
                validatorMsgs.Add("• Geben Sie die Dringlichkeit an.");
            if (String.IsNullOrWhiteSpace(Kurzbeschreibung))
                validatorMsgs.Add("• Geben Sie die Kurzbeschreibung an.");
            if (Dringlichkeit == null)
                validatorMsgs.Add("• Geben Sie die Dringlichkeit an.");
            if (Termin.HasValue && Termin.Value < DateTime.Now.Date)
                validatorMsgs.Add("• Der Fertigstellungstermin darf nicht in der Vergangenheit liegen.");

            if (validatorMsgs.Count > 0) throw new ComainValidationException("Der Auftrag kann nicht angelegt werden", validatorMsgs);

            var au = new NeuerAuftragDto { AuIHKS = IHObjekt_Id.Value, Dringlichkeit = Dringlichkeit.Id, Kurzbeschreibung = Kurzbeschreibung, Termin = Termin };
            if (Abnahmeberechtigter != null) au.Abnahmeberechtigter = Abnahmeberechtigter.Id;
            if (Schadensbild != null) au.Schadensbild = Schadensbild.Id;
            if (SubSchadensbild != null) au.SubSchadensbild = SubSchadensbild.Id;
            au.ExterneNummer = ExterneNummer;

            return au;
        }


        public void Unload()
        {

            ihliste = null;
            Schadensbilder = null;
            Pzliste = null;
            Maschinen = null;
            Bauteile = null;
            SubSchadensbilder = null;
            Dringlichkeiten = null;
            IHSearchProvider.ClearItems();
            PsSearchProvider.ClearItems();
        }


        public void ResetSearch()
        {

            Schadensbild        = null;
            SubSchadensbild     = null;
            Abnahmeberechtigter = null;
            Kurzbeschreibung    = null;
            Termin              = null;
            Standort            = null;
            Maschine            = null;
            Bauteil             = null;
            FreiesIHObjekt      = null;
            Dringlichkeit       = null;
            ExterneNummer       = null;
        }


        public Konfiguration Konfiguration { get; set; }


#region P R O P E R T I E S


        private NummerFilterModel schadensbild;
        public NummerFilterModel Schadensbild
        {
            get { return schadensbild; }
            set { if (SetProperty(ref schadensbild, value)) RefreshSubSchadensbilder(); }
        }


        private IList<NummerFilterModel> schadensbilder;
        public IList<NummerFilterModel> Schadensbilder
        {
            get { return schadensbilder; }
            set { SetProperty(ref schadensbilder, value); }
        }


        private SubNummerFilterModel subSchadensbild;
        public SubNummerFilterModel SubSchadensbild
        {
            get { return subSchadensbild; }
            set { SetProperty(ref subSchadensbild, value); }
        }


        private PersonalFilterModel abnahmeberechtigter;
        public PersonalFilterModel Abnahmeberechtigter
        {
            get { return abnahmeberechtigter; }
            set { SetProperty(ref abnahmeberechtigter, value); }
        }


        private String externeNummer;
        public String ExterneNummer
        {
            get { return externeNummer; }
            set { SetProperty(ref externeNummer, value); }
        }


        private String kurzbeschreibung;
        public String Kurzbeschreibung
        {
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }


        private DateTime? termin;
        public DateTime? Termin
        {
            get { return termin; }
            set { SetProperty(ref termin, value); }
        }


        public IIHFastSearchProvider    IHSearchProvider   { get; }


        public IPersonalSearchProvider  PsSearchProvider   { get; }


        private IList<PzFilterModel> pzliste;
        public IList<PzFilterModel> Pzliste
        {
            get { return pzliste; }
            set { SetProperty(ref pzliste, value); }
        }


        public ICollectionView Maschinen            { get; private set; }
        public ICollectionView Bauteile             { get; private set; }
        public ICollectionView SubSchadensbilder    { get; private set; }


        private PzFilterModel standort;
        public PzFilterModel Standort
        {
            get { return standort; }
            set {

                if (SetProperty(ref standort, value)) {

                    RefreshMaschinen();
                    Maschine = null;
                    Bauteil = null;
                    if (value != null) FreiesIHObjekt = null;
                }
            }
        }


        private AuIHKSDto maschine;
        public AuIHKSDto Maschine
        {
            get { return maschine; }
            set {

                if (SetProperty(ref maschine, value)) {

                    RefreshBauteile();
                    Bauteil = null;
                    if (value != null) FreiesIHObjekt = null;
                }
            }
        }


        private AuIHKSDto bauteil;
        public AuIHKSDto Bauteil
        {
            get { return bauteil; }
            set {

                if (SetProperty(ref bauteil, value)) {

                    if (value != null) FreiesIHObjekt = null;
                }
            }
        }


        private IHFilterItem freiesIHObjekt;
        public IHFilterItem FreiesIHObjekt
        {
            get { return freiesIHObjekt; }
            set {

                if (SetProperty(ref freiesIHObjekt, value)) {

                    RaisePropertyChanged(nameof(FreieMaschine));

                    if (value != null) {

                        Standort = null;
                        Maschine = null;
                        Bauteil = null;
                    }
                }
            }
        }

        public String FreieMaschine { get { return freiesIHObjekt == null? null: freiesIHObjekt.Pfad; } }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand createCommand;
        public ICommand CreateCommand
        {
            get { return createCommand; }
            set { SetProperty(ref createCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private IList<Dringlichkeit> dringlichkeiten;
        public IList<Dringlichkeit> Dringlichkeiten
        {
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        private Dringlichkeit dringlichkeit;
        public Dringlichkeit Dringlichkeit
        {
            get { return dringlichkeit; }
            set {

                if (Dringlichkeit != value) {

                    dringlichkeit = value;
                    RaisePropertyChanged(nameof(Dringlichkeit));
                    if (value != null && value.Tagesfrist.HasValue && value.Tagesfrist.Value >= 0)
                        Termin = DateTime.Today.AddDays(value.Tagesfrist.Value);
                }
            }
        }


#endregion

    }
}
