﻿using System;
using MahApps.Metro.Controls;

namespace Comain.Client.Services
{

    public class FlyoutConfig
    {

        public FlyoutConfig()
        {

            IsModal = false;
            Position = Position.Bottom;
            Theme = FlyoutTheme.Adapt;
            IsPinned = false;   
        }


        public String       Header      { get; set; }
        public Position     Position    { get; set; }
        public FlyoutTheme  Theme       { get; set; }
        public object       ChildView   { get; set; }
        public bool         IsModal     { get; set; }
        public bool         IsPinned    { get; set; }
    }
}
