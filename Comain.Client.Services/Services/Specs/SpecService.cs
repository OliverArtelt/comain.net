﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models.Specs;
using Comain;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Trackers;
using Comain.Client.Models;

namespace Comain.Client.Services.Specs
{
    public class SpecService : ISpecService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public SpecService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<List<SpecListItem>> SearchAsync(Suchkriterien search, CancellationToken ct = default)
        {

            var uri = $"/Spc/Search";
            var dtolist = await store.FindAsync<List<SpecListItem>>(uri, search, ct).ConfigureAwait(false);
            return dtolist;
        }


        public async Task<Spezifikation> DetailAsync(int id, CancellationToken ct = default)
        {

            var uri = $"/Spc/{id}";
            var dto = await store.GetAsync<SpezifikationDto>(uri, ct).ConfigureAwait(false);
            var model = mapper.Map<SpezifikationDto, Spezifikation>(dto);
            model.AcceptChanges();
            return model;
        }


        public async Task<Spezifikation> UpdateAsync(Spezifikation model, CancellationToken ct = default)
        {

            if (model == null || !model.IsChanged) return model;
            var uri = $"/Spc/";
            var dto = mapper.Map<Spezifikation, SpezifikationDto>(model);
            dto.Wertschablonen = mapper.AsDtoList<SpecWert, SpecWertDto>(model.Wertschablonen);
            dto.VorgeseheneArtikelliste = mapper.AsDtoList<SpecArtikel, SpecArtikelDto>(model.VorgeseheneArtikelliste);
            dto.VorgeseheneMassnahmen = mapper.AsDtoList<SpecMassnahme, SpecMassnahmeDto>(model.VorgeseheneMassnahmen);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<SpezifikationDto, SpezifikationDto>(uri, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<SpezifikationDto, Spezifikation>(result);
            return newmodel;
        }


        public async Task DeleteAsync(Spezifikation model, CancellationToken ct = default)
        {

            if (model == null) return;
            await store.DeleteAsync($"/Spc/{model.Id}", ct).ConfigureAwait(false);
        }


        /// <summary>
        /// zugeordnete Entities einer Spezifikation in der Spec-Verwaltung zeigen
        /// </summary>
        public Task<IList<AssignedEntity>> AssignedEntitiesAsync(int specId, CancellationToken ct = default)
            => store.GetAsync<IList<AssignedEntity>>($"/Spc/Entities/{specId}", ct);


        public Task<IList<NummerFilterModel>> EinheitenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/EH", ct);


        public Task<List<MaterialstammFilterModel>> MaterialstammAsync(CancellationToken ct = default)
            => store.GetAsync<List<MaterialstammFilterModel>>("/Filter/MT", ct); 


        public Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct); 


        public Task<IList<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<LeistungsartFilterModel>>("/Filter/LA", ct); 
    }
}
