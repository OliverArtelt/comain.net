﻿using System.Threading.Tasks;


namespace Comain.Client.Controllers
{

    public interface IController
    {

        /// <summary>
        /// Handler aufsetzen und Ressourcen anfordern
        /// </summary>
        void Initialize();
        /// <summary>
        /// Daten laden
        /// </summary>
        Task RunAsync(object parameter);
        /// <summary>
        /// Modul wechseln
        /// </summary>
        /// <remarks>
        /// Bei ungespeicherter Bearbeitung nachfragen
        /// </remarks>
        /// <returns>true: kann fortfahren, false: abbrechen (Bearbeitung noch nicht gespeichert)</returns>
        Task<bool> UnloadAsync();
        /// <summary>
        /// initialisierte Ressourcen freigeben
        /// </summary>
        void Shutdown();
    }
}
