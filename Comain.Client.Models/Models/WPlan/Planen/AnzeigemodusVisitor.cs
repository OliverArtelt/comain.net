﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{
    public class AnzeigemodusVisitor : IVisitor
    {

        private Plananzeigemodus modus;


        public AnzeigemodusVisitor(Plananzeigemodus modus)
        {
            this.modus = modus;
        }


        public void Visit(Plan model)
        {
            model.Spaltenbreite = modus.Width();
        }


        public void Visit(Planspaltensumme model)
        {
            model.Anzeigemodus = modus;
        }


        public void Visit(PZPlanzeile model)
        {}


        public void Visit(PZSpaltensumme model)
        {
            model.Anzeigemodus = modus;
        }


        public void Visit(IHPlanzeile model)
        {}


        public void Visit(IHSpaltensumme model)
        {
            model.Anzeigemodus = modus;
        }


        public void Visit(MNPlanzeile model)
        {}


        public void Visit(Planzelle model)
        {
            model.Anzeigemodus = modus;
        }


        public void Visit(Planaufgabe model)
        {
            model.Anzeigemodus = modus;
        }


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
