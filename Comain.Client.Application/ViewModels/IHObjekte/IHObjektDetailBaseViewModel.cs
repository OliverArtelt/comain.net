﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Specs;
using Comain.Client.ViewModels.Specs;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektDetailBaseViewModel : ValidatableViewModel<IIHObjektDetailBaseView, IHObjekt>
    {

        private readonly SynchronizationContext context;
        private IEnumerable<SubNummerFilterModel> oalist;


        public IHObjektDetailBaseViewModel(IIHObjektDetailBaseView view, IPersonalSearchProvider psSearcher, ISpecSearchProvider specSearcher,
                                           ILieferantenSearchProvider lfSearcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = psSearcher;
            SpecSearchProvider = specSearcher;
            LfSearchProvider = lfSearcher;
        }


        public override void CurrentChanged(IHObjekt oldSelected)
        {

            RaisePropertyChanged(nameof(Objektgruppe));
            RaisePropertyChanged(nameof(Kostenstelle));
            RaisePropertyChanged(nameof(Standort));
            RaisePropertyChanged(nameof(Hersteller));
            RaisePropertyChanged(nameof(Lieferant));
            RaisePropertyChanged(nameof(Instandhalter));
            RaisePropertyChanged(nameof(DatumStandort));
            RaisePropertyChanged(nameof(DatumKostenstelle));
            RaisePropertyChanged(nameof(Inventarnummer));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(WNummer));
            RaisePropertyChanged(nameof(Baugruppe));
            RaisePropertyChanged(nameof(Bauteil));
            RaisePropertyChanged(nameof(Typ));
            RaisePropertyChanged(nameof(Fabriknummer));
            RaisePropertyChanged(nameof(Anlage));
            RaisePropertyChanged(nameof(Baujahr));
            RaisePropertyChanged(nameof(Anschaffungsjahr));
            RaisePropertyChanged(nameof(Wiederbeschaffungswert));
            RaisePropertyChanged(nameof(Anschaffungswert));
            RaisePropertyChanged(nameof(Scannummer));

            RaisePropertyChanged(nameof(StatusSehrHoch));
            RaisePropertyChanged(nameof(StatusHoch));
            RaisePropertyChanged(nameof(StatusGering));
            RaisePropertyChanged(nameof(StatusKeine));
            RaisePropertyChanged(nameof(Klasse1));
            RaisePropertyChanged(nameof(Klasse2));
            RaisePropertyChanged(nameof(Klasse3));
            RaisePropertyChanged(nameof(Klasse4));

            RaisePropertyChanged(nameof(VerantwortlicherInstandhaltung));
            RaisePropertyChanged(nameof(VerantwortlicherProduktion));
            RaisePropertyChanged(nameof(Spezifikation));
            RaisePropertyChanged(nameof(OgrEditEnabled));
            RaisePropertyChanged(nameof(TechnischeWerte));

            RefreshObjektarten();
        }


        public Task SetzeListenAsync(IEnumerable<PzFilterModel> pzlist, IEnumerable<KstFilterModel> kstlist, IEnumerable<NummerFilterModel> ogrlist,
                                     IEnumerable<SubNummerFilterModel> oalist, IEnumerable<LieferantFilterModel> lflist, IEnumerable<PersonalFilterModel> pslist,
                                     IEnumerable<SpecFilterModel> splist)
        {

            return context.SendAsync(new SendOrPostCallback((o) => {

                Kostenstellen = kstlist.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).ToList();
                Standorte     = pzlist;
                Objektgruppen = ogrlist;
                this.oalist   = oalist;

                PsSearchProvider.SetItems(pslist);
                SpecSearchProvider.SetItems(splist);
                LfSearchProvider.SetItems(lflist);

                RaisePropertyChanged(nameof(Kostenstellen));
                RaisePropertyChanged(nameof(Standorte));
                RaisePropertyChanged(nameof(Objektgruppen));

                RefreshObjektarten();
            }), null);
        }


        private void RefreshObjektarten()
        {

            context.Send(new SendOrPostCallback((o) => {

                if (oalist == null) return;
                Objektarten = null;
                if (Objektgruppe != null) Objektarten = oalist.Where(p => p.Haupt_Id == Objektgruppe.Id).OrderBy(p => p.Nummer).ToList();
                RaisePropertyChanged(nameof(Objektart));
                RaisePropertyChanged(nameof(Objektarten));
            }), null);
        }


        public void Unload()
        {

            Objektgruppen = null;
            Objektarten = null;
            Standorte = null;
            Kostenstellen = null;
            oalist = null;
            PsSearchProvider.ClearItems();
            SpecSearchProvider.ClearItems();
            LfSearchProvider.ClearItems();
            Thumbnail = null;
        }


        public bool OgrEditEnabled
        {

            get {

                if (Current == null) return false;
                if (!EditEnabled) return false;
                if (Current.IstAngefügt) return true;
                if (Current.Konfiguration == null) return false;
                return Current.Konfiguration.IHObjektObjektgruppeÄnderbar;
            }
        }


#region P R O P E R T I E S


        public ILieferantenSearchProvider   LfSearchProvider    { get; }
        public IPersonalSearchProvider      PsSearchProvider    { get; }
        public ISpecSearchProvider          SpecSearchProvider  { get; }


        private BitmapSource thumbnail;
        public BitmapSource Thumbnail
        {
            get { return thumbnail; }
            set { SetProperty(ref thumbnail, value); }
        }


        public IEnumerable<TechnischerWert> TechnischeWerte
            => Current?.BaseSpecs;


        public NummerFilterModel Objektgruppe
        {
            get { return current == null? null: current.Objektgruppe; }
            set {

                if (current != null && current.Objektgruppe != value) {

                    current.Objektgruppe = value;
                    RaisePropertyChanged("Objektgruppe");
                    current.Objektart = null;
                    RefreshObjektarten();
                }
            }
        }


        public SubNummerFilterModel Objektart
        {
            get { return current == null? null: current.Objektart; }
            set {

                if (current != null && current.Objektart != value) {

                    current.Objektart = value;
                    RaisePropertyChanged("Objektart");
                }
            }
        }


        public KstFilterModel Kostenstelle
        {
            get { return current == null? null: current.Kostenstelle; }
            set {

                if (current != null && current.Kostenstelle != value) {

                    current.Kostenstelle = value;
                    RaisePropertyChanged("Kostenstelle");
                }
            }
        }


        public PzFilterModel Standort
        {
            get { return current == null? null: current.Standort; }
            set {

                if (current != null && current.Standort != value) {

                    current.Standort = value;
                    RaisePropertyChanged("Standort");
                }
            }
        }


        public SpecFilterModel Spezifikation
        {
            get { return current == null? null: current.Spezifikation; }
            set {

                if (current != null && current.Spezifikation != value) {

                    current.Spezifikation = value;
                    RaisePropertyChanged(nameof(Spezifikation));
                }
            }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { if (SetProperty(ref editEnabled, value)) RaisePropertyChanged("OgrEditEnabled"); }
        }


        public decimal? Wiederbeschaffungswert
        {
            get { return current == null? null: current.Wiederbeschaffungswert; }
            set {

                if (current != null && current.Wiederbeschaffungswert != value) {

                    current.Wiederbeschaffungswert = value;
                    RaisePropertyChanged("Wiederbeschaffungswert");
                }
            }
        }


        public decimal? Anschaffungswert
        {
            get { return current == null? null: current.Anschaffungswert; }
            set {

                if (current != null && current.Anschaffungswert != value) {

                    current.Anschaffungswert = value;
                    RaisePropertyChanged("Anschaffungswert");
                }
            }
        }


        public short? Baujahr
        {
            get { return current == null? null: current.Baujahr; }
            set {

                if (current != null && current.Baujahr != value) {

                    current.Baujahr = value;
                    RaisePropertyChanged("Baujahr");
                }
            }
        }


        public short? Anschaffungsjahr
        {
            get { return current == null? null: current.Anschaffungsjahr; }
            set {

                if (current != null && current.Anschaffungsjahr != value) {

                    current.Anschaffungsjahr = value;
                    RaisePropertyChanged("Anschaffungsjahr");
                }
            }
        }


        public String WNummer
        {
            get { return current == null? null: current.WNummer; }
            set {

                if (current != null && current.WNummer != value) {

                    current.WNummer = value;
                    RaisePropertyChanged("WNummer");
                }
            }
        }


        public String Scannummer
        {
            get { return current == null? null: current.Scannummer; }
            set {

                if (current != null && current.Scannummer != value) {

                    current.Scannummer = value;
                    RaisePropertyChanged("Scannummer");
                }
            }
        }


        public String Anlage
        {
            get { return current == null? null: current.Anlage; }
            set {

                if (current != null && current.Anlage != value) {

                    current.Anlage = value;
                    RaisePropertyChanged("Anlage");
                }
            }
        }


        public String Fabriknummer
        {
            get { return current == null? null: current.Fabriknummer; }
            set {

                if (current != null && current.Fabriknummer != value) {

                    current.Fabriknummer = value;
                    RaisePropertyChanged("Fabriknummer");
                }
            }
        }


        public String Inventarnummer
        {
            get { return current == null? null: current.Inventarnummer; }
            set {

                if (current != null && current.Inventarnummer != value) {

                    current.Inventarnummer = value;
                    RaisePropertyChanged("Inventarnummer");
                }
            }
        }


        public String Nummer
        {
            get { return current == null? null: current.Nummer; }
            set {

                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }


        public String Name
        {
            get { return current == null? null: current.Name; }
            set {

                if (current != null && current.Name != value) {

                    current.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }


        public String Baugruppe
        {
            get { return current == null? null: current.Baugruppe; }
            set {

                if (current != null && current.Baugruppe != value) {

                    current.Baugruppe = value;
                    RaisePropertyChanged("Baugruppe");
                }
            }
        }


        public String Bauteil
        {
            get { return current == null? null: current.Bauteil; }
            set {

                if (current != null && current.Bauteil != value) {

                    current.Bauteil = value;
                    RaisePropertyChanged("Bauteil");
                }
            }
        }


        public String Typ
        {
            get { return current == null? null: current.Typ; }
            set {

                if (current != null && current.Typ != value) {

                    current.Typ = value;
                    RaisePropertyChanged("Typ");
                }
            }
        }


        public DateTime? DatumStandort
        {
            get => current?.IHObjektPzMap?.Max(p => p.Datum);
        }


        public DateTime? DatumKostenstelle
        {
            get => current?.IHObjektKstMap?.Max(p => p.Datum);
        }


        public LieferantFilterModel Hersteller
        {
            get { return current == null? null: current.Hersteller; }
            set {

                if (current != null && current.Hersteller != value) {

                    current.Hersteller = value;
                    RaisePropertyChanged("Hersteller");
                }
            }
        }


        public LieferantFilterModel Lieferant
        {
            get { return current == null? null: current.Lieferant; }
            set {

                if (current != null && current.Lieferant != value) {

                    current.Lieferant = value;
                    RaisePropertyChanged("Lieferant");
                }
            }
        }


        public LieferantFilterModel Instandhalter
        {
            get { return current == null? null: current.Instandhalter; }
            set {

                if (current != null && current.Instandhalter != value) {

                    current.Instandhalter = value;
                    RaisePropertyChanged("Instandhalter");
                }
            }
        }


        public PersonalFilterModel VerantwortlicherInstandhaltung
        {
            get { return current == null? null: current.VerantwortlicherInstandhaltung; }
            set {

                if (current != null && current.VerantwortlicherInstandhaltung != value) {

                    current.VerantwortlicherInstandhaltung = value;
                    RaisePropertyChanged(nameof(VerantwortlicherInstandhaltung));
                }
            }
        }


        public PersonalFilterModel VerantwortlicherProduktion
        {
            get { return current == null? null: current.VerantwortlicherProduktion; }
            set {

                if (current != null && current.VerantwortlicherProduktion != value) {

                    current.VerantwortlicherProduktion = value;
                    RaisePropertyChanged(nameof(VerantwortlicherProduktion));
                }
            }
        }


        public IEnumerable<KstFilterModel>       Kostenstellen  { get; private set; }
        public IEnumerable<PzFilterModel>        Standorte      { get; private set; }
        public IEnumerable<NummerFilterModel>    Objektgruppen  { get; private set; }
        public IEnumerable<SubNummerFilterModel> Objektarten    { get; private set; }


        public bool StatusSehrHoch
        {
            get { return current == null? false: current.Status == IHObjektStatus.SehrHoch; }
            set {

                if (current != null && value && current.Status != IHObjektStatus.SehrHoch) {

                    current.Status = IHObjektStatus.SehrHoch;
                    RaisePropertyChanged("StatusSehrHoch");
                    RaisePropertyChanged("StatusHoch");
                    RaisePropertyChanged("StatusGering");
                    RaisePropertyChanged("StatusKeine");
                }
            }
        }


        public bool StatusHoch
        {
            get { return current == null? false: current.Status == IHObjektStatus.Hoch; }
            set {

                if (current != null && value && current.Status != IHObjektStatus.Hoch) {

                    current.Status = IHObjektStatus.Hoch;
                    RaisePropertyChanged("StatusSehrHoch");
                    RaisePropertyChanged("StatusHoch");
                    RaisePropertyChanged("StatusGering");
                    RaisePropertyChanged("StatusKeine");
                }
            }
        }


        public bool StatusGering
        {
            get { return current == null? false: current.Status == IHObjektStatus.Gering; }
            set {

                if (current != null && value && current.Status != IHObjektStatus.Gering) {

                    current.Status = IHObjektStatus.Gering;
                    RaisePropertyChanged("StatusSehrHoch");
                    RaisePropertyChanged("StatusHoch");
                    RaisePropertyChanged("StatusGering");
                    RaisePropertyChanged("StatusKeine");
                }
            }
        }


        public bool StatusKeine
        {
            get { return current == null? false: current.Status == IHObjektStatus.Keine; }
            set {

                if (current != null && value && current.Status != IHObjektStatus.Keine) {

                    current.Status = IHObjektStatus.Keine;
                    RaisePropertyChanged("StatusSehrHoch");
                    RaisePropertyChanged("StatusHoch");
                    RaisePropertyChanged("StatusGering");
                    RaisePropertyChanged("StatusKeine");
                }
            }
        }


        public bool Klasse4
        {
            get { return current == null? false: current.Erhaltklasse == Erhaltklasse.Klasse4; }
            set {

                if (current != null && value && current.Erhaltklasse != Erhaltklasse.Klasse4) {

                    current.Erhaltklasse = Erhaltklasse.Klasse4;
                    RaisePropertyChanged("Klasse1");
                    RaisePropertyChanged("Klasse2");
                    RaisePropertyChanged("Klasse3");
                    RaisePropertyChanged("Klasse4");
                }
            }
        }


        public bool Klasse1
        {
            get { return current == null? false: current.Erhaltklasse == Erhaltklasse.Klasse1; }
            set {

                if (current != null && value && current.Erhaltklasse != Erhaltklasse.Klasse1) {

                    current.Erhaltklasse = Erhaltklasse.Klasse1;
                    RaisePropertyChanged("Klasse1");
                    RaisePropertyChanged("Klasse2");
                    RaisePropertyChanged("Klasse3");
                    RaisePropertyChanged("Klasse4");
                }
            }
        }


        public bool Klasse2
        {
            get { return current == null? false: current.Erhaltklasse == Erhaltklasse.Klasse2; }
            set {

                if (current != null && value && current.Erhaltklasse != Erhaltklasse.Klasse2) {

                    current.Erhaltklasse = Erhaltklasse.Klasse2;
                    RaisePropertyChanged("Klasse1");
                    RaisePropertyChanged("Klasse2");
                    RaisePropertyChanged("Klasse3");
                    RaisePropertyChanged("Klasse4");
                }
            }
        }


        public bool Klasse3
        {
            get { return current == null? false: current.Erhaltklasse == Erhaltklasse.Klasse3; }
            set {

                if (current != null && value && current.Erhaltklasse != Erhaltklasse.Klasse3) {

                    current.Erhaltklasse = Erhaltklasse.Klasse3;
                    RaisePropertyChanged("Klasse1");
                    RaisePropertyChanged("Klasse2");
                    RaisePropertyChanged("Klasse3");
                    RaisePropertyChanged("Klasse4");
                }
            }
        }


#endregion

    }
}
