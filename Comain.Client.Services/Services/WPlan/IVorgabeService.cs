﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.WPlan
{

    public interface IVorgabeService
    {

        Task<List<MassnahmeListDto>>            ListAsync(int ihid, CancellationToken ct = default);
        Task<WiMassnahme>                       DetailAsync(int idMassnahme, CancellationToken ct = default);
        Task<WiMassnahme>                       UpdateAsync(WiMassnahme model, CancellationToken ct = default);
        Task                                    DeleteAsync(int idMassnahme, CancellationToken ct = default);

        Task<List<LeistungsartFilterModel>>     LeistungsartenAsync(CancellationToken ct = default);
        Task<List<NummerFilterModel>>           EinheitenAsync(CancellationToken ct = default);
        Task<List<NummerFilterModel>>           GewerkeAsync(CancellationToken ct = default);
        Task<List<IHObjektFilterModel>>         IHObjekteAsync(CancellationToken ct = default);
        Task<List<MaterialstammFilterModel>>    MaterialstammAsync(CancellationToken ct = default);

        Task<IList<EreignisViewItem>>           EreignisseAsync(int mnid, CancellationToken ct = default);
        Task<List<Media>>                       ReadFilesAsync(IEnumerable<String> paths, CancellationToken ct = default);
    }
}