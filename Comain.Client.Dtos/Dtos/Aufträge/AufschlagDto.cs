﻿using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Aufträge
{

    public class AufschlagDto : ChangeTrackingBase
    {
        
        public int        Aufschlagtyp_Id       { get; set; }
        public int?       Minuten               { get; set; }
        public decimal?   Prozent               { get; set; }
    }
}
