﻿using System;


namespace Comain
{

    public class ExceptionDto
    {

        public String       Message             { get; set; }
        public String       ExceptionMessage    { get; set; }
        public String       ExceptionType       { get; set; }
        public String       Details             { get; set; }
        public String[]     Explanations        { get; set; }
        public String       Error               { get; set; }
        public String       Error_Description   { get; set; }
        public String       StackTrace          { get; set; }

        public ExceptionDto InnerException      { get; set; }
    }
}
