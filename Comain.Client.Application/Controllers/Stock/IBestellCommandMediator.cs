﻿using System;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Bestellungen
{
    
    public interface IBestellCommandMediator
    {
        
        EditMode CurrentMode { get; }
        void NeuSetzen();
        void SetEditMode(EditMode newMode);
    }
}
