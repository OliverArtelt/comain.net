﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Reports.Filters;


namespace Comain.Client.ViewModels.Reports.Filters
{

    public class LbKstViewModel : ViewModel<ILbKstView>, IFilterViewModel
    {

        public LbKstViewModel(ILbKstView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            Mwst = true;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (Mwst) filter.Add("Mwst", true);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);     
        }


        public void Unload()
        {}


#region P R O P E R T I E S
      

        private bool mwst;
        public bool Mwst
        { 
            get { return mwst; }
            set { SetProperty(ref mwst, value); }
        } 


#endregion

    }
}
