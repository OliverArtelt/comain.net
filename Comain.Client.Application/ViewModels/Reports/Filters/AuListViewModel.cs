﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Reports.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class AuListViewModel : ViewModel<IAuListView>, IFilterViewModel
    {

        private readonly IFilterService  store;
        private readonly IHSelektorViewModel ihFilter;


        public AuListViewModel(IHSelektorViewModel ihFilter, IFilterService store, IAuListView view) : base(view)
        {

            this.store = store;
            this.ihFilter = ihFilter;
            ihFilter.ClearButtonVisible = false;
        }


        public void Reset()
        {

            ihFilter.Reset();
            TypOffen = true;
            FullRow = false;
        }


        public void Unload()
        {
            ihFilter.Unload();
        }


        public String Validiere()
        {
            
            if (typ == "Asset" && IHObjekt == null) return "Geben Sie das IH-Objekt an.";
            return null;
        }


        public String FilterAlsString()
        {
            
            String s = "Status " + typ;
            if (typ == "Asset" && IHObjekt != null) s += String.Format(" {0} - {1}", IHObjekt.Nummer, IHObjekt.Name);
            return s;
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("AuStatus", typ);
            filter.Add("FullRow", FullRow);
            if (typ == "Asset" && IHObjekt != null) filter.Add("IHObjekt", IHObjekt.Id);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return ihFilter.LoadAsync(store.IHObjekte.Select(p => new IHFilterItem(p)).ToList(), store.Standorte, store.Kostenstellen, ct);     
        }
        

        private void RaiseTyp()
        { 

            RaisePropertyChanged("TypOffen");
            RaisePropertyChanged("TypÜbergeben");
            RaisePropertyChanged("TypGeschlossen");
            RaisePropertyChanged("TypTermin");
            RaisePropertyChanged("TypIHObjekt");
            RaisePropertyChanged("TypAlles");
        } 


#region P R O P E R T I E S
   
         
        private bool fullRow;
        public bool FullRow
        { 
            get { return fullRow; }
            set { SetProperty(ref fullRow, value); }
        } 
     

        private String typ = "Offen";
        
        public bool TypOffen
        { 
            get { return typ == "Offen"; }
            set { 
            
                if (value && typ != "Offen") {
                    
                    typ = "Offen";
                    RaiseTyp();
                }
            }
        } 
        
        public bool TypÜbergeben
        { 
            get { return typ == "Übergeben"; }
            set { 
            
                if (value && typ != "Übergeben") {
                    
                    typ = "Übergeben";
                    RaiseTyp();
                }
            }
        } 
        
        public bool TypGeschlossen
        { 
            get { return typ == "Geschlossen"; }
            set { 
            
                if (value && typ != "Geschlossen") {
                    
                    typ = "Geschlossen";
                    RaiseTyp();
                }
            }
        } 
        
        public bool TypTermin
        { 
            get { return typ == "Termin"; }
            set { 
            
                if (value && typ != "Termin") {
                    
                    typ = "Termin";
                    RaiseTyp();
                }
            }
        } 
        
        public bool TypIHObjekt
        { 
            get { return typ == "Asset"; }
            set { 
            
                if (value && typ != "Asset") {
                    
                    typ = "Asset";
                    RaiseTyp();
                }
            }
        } 
        
        public bool TypAlles
        { 
            get { return typ == "Alles"; }
            set { 
            
                if (value && typ != "Alles") {
                    
                    typ = "Alles";
                    RaiseTyp();
                }
            }
        } 


        public object       IHSelektor  { get { return ihFilter.View; } }
        public IHFilterItem IHObjekt    { get { return ihFilter.CurrentIHObjekt; } }


#endregion

    }
}
