﻿using System;

namespace Comain.Factories
{
    
    /// <summary>
    /// Wrapper für transientes Objekt, das sich selbst bei seinem DIC-Container abmelden kann
    /// </summary>
    public class Container<T> : IDisposable
                                where T: class
    {

        private readonly T val;
        private readonly IFactory<T> fac;


        public Container(IFactory<T> fac)
        {
       
            this.val = fac.Create();
            this.fac = fac;
        }
        

        public T Value { get { return val; } }


        public static implicit operator T(Container<T> self)
        {
            return self.Value;
        }


#region D I S P O S I N G

        
        private bool disposed = false;


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {

            if (!this.disposed) {

                if (disposing) {

                    fac.Release(val);
                }

                disposed = true;
            }
        }

        ~Container()
        {
            Dispose(false);
        }

#endregion

    }
}
