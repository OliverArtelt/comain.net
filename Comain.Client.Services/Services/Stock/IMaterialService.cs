﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;

namespace Comain.Client.Services.Stock.Aufträge
{

    public interface IMaterialService
    {
       
        Task<Materialleistung>                  DetailAsync(int id, CancellationToken ct = default);
        Task<Materialleistung>                  SaveAsync(Materialleistung model, CancellationToken ct = default);
        Task                                    DeleteAsync(Materialleistung model, CancellationToken ct = default);

        Task<IList<NummerFilterModel>>          FilterEinheitenAsync(CancellationToken ct = default);
        Task<IList<LieferantFilterModel>>       FilterLieferantenAsync(CancellationToken ct = default);
        Task<IList<MaterialstammFilterModel>>   FilterMaterialstammAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>        FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<WLFilterDto>>                FilterEinlagerungenAsync(CancellationToken ct = default);

        Task<IHInfoDto>                         IHInfoAsync(int auid, CancellationToken ct = default);
        Task<decimal>                           FachRestmengeAsync(int lagerortid, int artikelid, CancellationToken ct = default);
    }
}
