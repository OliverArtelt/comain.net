﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Stock.Artikelstamm;


namespace Comain.Client.Commands
{

    public class ArtikelCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IArtikelController);
        public String       Name        => "Artikel";
        public String       Parameter   => "Stock.AT";
        public String       Gruppe      => "Ersatzteile";
        public int          Sortierung  => 1100;
        public String       Color       => "Goldenrod";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.cog.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null || !module.VerwendetModulStock) return false;
            return nutzer.IstAdministrator || nutzer.IstLagerAdministrator || nutzer.IstLagerVerwaltung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
