﻿using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class KstPzMap : ChangeTrackingBase
    {
        
        public int Kostenstelle_Id  { get; set; }
        public int Standort_Id      { get; set; }
    }
}
