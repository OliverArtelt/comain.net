﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX;
using CO.OpenXML.XLSX.ExcelStyles;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PreistabelleWriter : IExportVisitor
    {

        private WorksheetWriter writer;
        private IStyleFactory styles;
        private int currentRow = 1;
        private Plan plan;
        private PlanOptions options;
        private const float uraltExcelZeichenbreite = 1.416F;
        private int startPerioden = 0;
        private int startStunden = 0;
        private int startPreise = 0;


        public String Exportname => "Preistabelle";


        public void SetStyles(IStyleFactory s)
            => styles = s;


        public void SetOptions(PlanOptions o)
            => options = o;


        public void SetWriter(WorksheetWriter w)
            => writer = w;


        public void Visit(Plan model)
        {

            int ndx = 0;
            plan = model;

            startStunden = 0;
            startPreise = 0;
            startPerioden = 6 + (options.Preise? 1: 0) + (options.Stunden? 1: 0);
            if (options.Gruppensummen) {

                if (options.Stunden) startStunden = startPerioden + plan.Spalten.Count;
                if (options.Preise)  startPreise = startPerioden + plan.Spalten.Count + (options.Stunden? 2: 0);
            }

            var cols = new List<ColumnStyle>();
            cols.Add(new ColumnStyle(++ndx, 15F * uraltExcelZeichenbreite));   //Gewerk
            cols.Add(new ColumnStyle(++ndx, 32F * uraltExcelZeichenbreite));   //Name
            cols.Add(new ColumnStyle(++ndx, 12F * uraltExcelZeichenbreite));   //Turnus
            cols.Add(new ColumnStyle(++ndx, 20F * uraltExcelZeichenbreite));   //KST
            if (options.Stunden) cols.Add(new ColumnStyle(++ndx, 6F  * uraltExcelZeichenbreite));   //Normzeit
            cols.Add(new ColumnStyle(++ndx, 6F  * uraltExcelZeichenbreite));   //Menge
            if (options.Preise)  cols.Add(new ColumnStyle(++ndx, 6F  * uraltExcelZeichenbreite));   //Wert

            model.Spalten.ForEach(p => cols.Add(new ColumnStyle(++ndx, 6F * uraltExcelZeichenbreite)));

            if (options.Stunden) cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Geplant h
            if (options.Stunden) cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Verbrauch h
            if (options.Preise)  cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Geplant €
            if (options.Preise)  cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Verbrauch €
            if (options.Preise)  cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Budget 48
            if (options.Preise)  cols.Add(new ColumnStyle(++ndx, 8F  * uraltExcelZeichenbreite));   //Budget 52

            writer.SetColumnWidth(cols);


            writer.SetValue(model.Client, currentRow++, 2, styles.Head);
            writer.SetValue(model.Name, currentRow++, 2, styles.Head);

            writer.SetValue("Erstellt", currentRow, 1, styles.Params);
            writer.SetValue(DateTime.Now.ToString(), currentRow++, 2, styles.Params);
            writer.SetValue("Benutzer", currentRow, 1, styles.Params);
            writer.SetValue(model.Benutzer, currentRow++, 2, styles.Params);

            if (!String.IsNullOrEmpty(model.Statistik?.Zeitraum)) {

                writer.SetValue("Zeitraum", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.Zeitraum, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PlanungstypAsString)) {

                writer.SetValue("Planungstyp", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PlanungstypAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PlanungsstatusAsString)) {

                writer.SetValue("Planungsstatus", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PlanungsstatusAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.LeistungsartAsString)) {

                writer.SetValue("Leistungsart", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.LeistungsartAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.GewerkAsString)) {

                writer.SetValue("Gewerk", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.GewerkAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PersonalAsString)) {

                writer.SetValue("Personal", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PersonalAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            currentRow++;
        }


        public void Visit(PZPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            writer.SetRowHeight(currentRow, 26);
            writer.SetValue("Standort", currentRow, 1, styles.Standort);
            writer.SetValue(model.Name, currentRow, 2, styles.Standort);

            currentRow++;
        }


        public void Visit(IHPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            writer.SetRowHeight(currentRow, 24);
            writer.SetValue(model.Nummer, currentRow, 1, styles.IHObjekt);
            writer.SetValue(model.Name, currentRow, 2, styles.IHObjekt);

            currentRow++;
            int currentCol = 1;

            writer.SetValue("Gewerk",       currentRow, currentCol++, styles.AuKopfL);
            writer.SetValue("Beschreibung", currentRow, currentCol++, styles.AuKopfL);
            writer.SetValue("Tur",          currentRow, currentCol++, styles.AuKopfC);
            writer.SetValue("Kostenstelle", currentRow, currentCol++, styles.AuKopfC);
            if (options.Stunden) writer.SetValue("Minuten", currentRow, currentCol++, styles.AuKopfC);
            writer.SetValue("Menge",        currentRow, currentCol++, styles.AuKopfC);
            if (options.Preise) writer.SetValue("Wert", currentRow, currentCol++, styles.AuKopfC);

            foreach (var spalte in plan.Spalten) {

                writer.SetValue(spalte.Periode, currentRow, currentCol, spalte.IstAktuell? styles.AuKopfCWAktuell: styles.AuKopfCW);
                writer.SetValue(spalte.TagShort, currentRow - 1, currentCol, styles.GreaterKey);

                currentCol++;
            }

            if (options.Stunden) writer.SetValue("Geplant h",    currentRow, currentCol++, styles.AuKopfR);
            if (options.Stunden) writer.SetValue("Verbrauch h",  currentRow, currentCol++, styles.AuKopfR);
            if (options.Preise)  writer.SetValue("Geplant €",    currentRow, currentCol++, styles.AuKopfR);
            if (options.Preise)  writer.SetValue("Verbrauch €",  currentRow, currentCol++, styles.AuKopfR);
            if (options.Preise)  writer.SetValue("Budget 48",    currentRow, currentCol++, styles.AuKopfR);
            if (options.Preise)  writer.SetValue("Budget 52",    currentRow, currentCol++, styles.AuKopfR);

            currentRow++;
        }


        public void Visit(MNPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;


            Style MapStyle(Planzelle cell)
            {

                if (cell == null) return styles.KeinErgebnis;
                if (!cell.ImExportSichtbar(options)) return styles.KeinErgebnis;
                if (cell.Ergebnis == Checklistergebnis.OK) return styles.ErgebnisOK;
                if (cell.Ergebnis == Checklistergebnis.NotOK) return styles.ErgebnisNotOK;
                if (cell.Ergebnis == Checklistergebnis.WirdNichtErledigt) return styles.ErgebnisWirdNichtErledigt;
                if (cell.Ergebnis == Checklistergebnis.Fortgeführt) return styles.ErgebnisFortgeführt;
                return styles.ErgebnisOffen;
            }


            String Value(Planzelle cell)
            {

                if (cell == null) return String.Empty;
                if (!cell.ImExportSichtbar(options)) return String.Empty;
                return cell.Ergebniskurztext;
            }


            int currentCol = 1;

            writer.SetValue(model.Gewerk,               currentRow, currentCol++, styles.AuCode);
            writer.SetValue(model.NameNummer,           currentRow, currentCol++, styles.AuKurzB);
            writer.SetValue(model.ZyklusAsString,       currentRow, currentCol++, styles.AuText);
            writer.SetValue(model.Kostenstelle,         currentRow, currentCol++, styles.AuCode);
            if (options.Stunden) writer.SetValue(model.Normzeit, currentRow, currentCol++, styles.AuNumeric);
            writer.SetValue(model.Menge,                currentRow, currentCol++, styles.AuNumeric);
            if (options.Preise) writer.SetValue(model.Wert, currentRow, currentCol++, styles.AuMoney);


            model.Perioden.ForEach(periode => writer.SetValue(Value(periode), currentRow, currentCol++, MapStyle(periode)));


            if (options.Stunden) writer.SetValue(Math.Round(model.ExportierteGeplanteStunden(options), 1, MidpointRounding.AwayFromZero), currentRow, currentCol++, styles.AuNumeric);
            if (options.Stunden) writer.SetValue(Math.Round(model.ExportierteVerbrauchteStunden(options), 1, MidpointRounding.AwayFromZero), currentRow, currentCol++, styles.AuNumeric);

            if (options.Preise) writer.SetValue(model.ExportierteGeplanteKosten(options),    currentRow, currentCol++, styles.AuMoney);
            if (options.Preise) writer.SetValue(model.ExportierteVerbrauchteKosten(options), currentRow, currentCol++, styles.AuMoney);
            if (options.Preise) writer.SetValue(model.Budget48,                              currentRow, currentCol++, styles.AuMoney);
            if (options.Preise) writer.SetValue(model.Budget52,                              currentRow, currentCol++, styles.AuMoney);

            currentRow++;
        }


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void Visit(IHSpaltensumme model)
        {}


        public void Visit(PZSpaltensumme model)
        {}


        public void Visit(Planspaltensumme model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {

            if (!options.Gruppensummen) return;

            if (options.Stunden) {

                writer.SetValue($"Stundenvorgabe Asset {model.Name}", currentRow, startPerioden - 1, styles.FootValue);
                model.Summen.ForEach(p => writer.SetValue(p.ExportierteGeplanteStunden(options), currentRow, p.ColumnIndex + startPerioden, styles.AuNumeric));

                writer.SetValue(model.ExportierteGeplanteStunden(options), currentRow, startStunden, styles.AuNumeric);
                writer.SetValue(model.ExportierteVerbrauchteStunden(options), currentRow, startStunden + 1, styles.AuNumeric);

                currentRow++;
            }

            if (options.Preise) {

                writer.SetValue($"Auftragswert Asset {model.Name}", currentRow, startPerioden - 1, styles.FootValue);
                model.Summen.ForEach(p => writer.SetValue(p.ExportierteGeplanteKosten(options), currentRow, p.ColumnIndex + startPerioden, styles.AuMoney));

                writer.SetValue(model.ExportierteGeplanteKosten(options), currentRow, startPreise, styles.AuMoney);
                writer.SetValue(model.ExportierteVerbrauchteKosten(options), currentRow, startPreise + 1, styles.AuMoney);

                currentRow++;
            }

            currentRow++;
        }


        public void VisitAfter(PZPlanzeile model)
        {

            if (!options.Gruppensummen) return;

            if (options.Stunden) {

                writer.SetValue($"Stundenvorgabe Standort {model.Name}", currentRow, startPerioden - 1, styles.FootValue);
                model.Summen.ForEach(p => writer.SetValue(p.ExportierteGeplanteStunden(options), currentRow, p.ColumnIndex + startPerioden, styles.AuNumeric));

                writer.SetValue(model.ExportierteGeplanteStunden(options), currentRow, startStunden, styles.AuNumeric);
                writer.SetValue(model.ExportierteVerbrauchteStunden(options), currentRow, startStunden + 1, styles.AuNumeric);

                currentRow++;
            }

            if (options.Preise) {

                writer.SetValue($"Auftragswert Standort {model.Name}", currentRow, startPerioden - 1, styles.FootValue);
                model.Summen.ForEach(p => writer.SetValue(p.ExportierteGeplanteKosten(options), currentRow, p.ColumnIndex + startPerioden, styles.AuMoney));

                writer.SetValue(model.ExportierteGeplanteKosten(options), currentRow, startPreise, styles.AuMoney);
                writer.SetValue(model.ExportierteVerbrauchteKosten(options), currentRow, startPreise + 1, styles.AuMoney);

                currentRow++;
            }

            currentRow++;
        }


        public void VisitAfter(Plan model)
        {

            if (!options.Gruppensummen) return;

            if (options.Stunden) {

                writer.SetValue("Stundenvorgabe Planung", currentRow, startPerioden - 1, styles.FootValue);
                model.Spaltensummen.ForEach(p => writer.SetValue(p.ExportierteGeplanteStunden(options), currentRow, p.ColumnIndex + startPerioden, styles.AuNumeric));

                writer.SetValue(model.ExportierteGeplanteStunden(options), currentRow, startStunden, styles.AuNumeric);
                writer.SetValue(model.ExportierteVerbrauchteStunden(options), currentRow, startStunden + 1, styles.AuNumeric);

                currentRow++;
            }

            if (options.Preise) {

                writer.SetValue("Auftragswert Planung", currentRow, startPerioden - 1, styles.FootValue);
                model.Spaltensummen.ForEach(p => writer.SetValue(p.ExportierteGeplanteKosten(options), currentRow, p.ColumnIndex + startPerioden, styles.AuMoney));

                writer.SetValue(model.ExportierteGeplanteKosten(options), currentRow, startPreise, styles.AuMoney);
                writer.SetValue(model.ExportierteVerbrauchteKosten(options), currentRow, startPreise + 1, styles.AuMoney);

                currentRow++;
            }

            currentRow++;
        }
    }
}
