﻿using System.Threading;
using System.Waf.Applications;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IFerienController : IVwTypController 
    {}
    
    
    public class FerienController : SimpleController<FerienDto, Ferien, IFerienView>, IFerienController
    {

        private readonly DelegateCommand weekCommand;
        private readonly new FerienViewModel view;

        
        public FerienController(IMessageService messages, IControllerBus bus,
                                ISimpleService<FerienDto, Ferien> store, VerwaltungViewModel mainView, FerienViewModel view)
          : base(messages, store, bus, mainView, view)
        {
        
            this.view = view;
            weekCommand = new DelegateCommand(_ => SetWeek(), _ => view.Current != null);
            view.WeekCommand = weekCommand;
        }

        
        protected override string Url
        {
            get { return "/VW/FR"; }
        }


        protected override void UpdateCommands()
        {
            
            context.Post(new SendOrPostCallback((o) => { 
            
                deleteCommand.RaiseCanExecuteChanged(); 
                weekCommand.RaiseCanExecuteChanged(); 
            
            }), null);
        }


        private void SetWeek()
        {

            if (view.Current == null) return;
            if (view.Current.DatumVon.HasValue) view.Current.DatumVon = view.Current.DatumVon.Value.Date.MondayOf();
            if (view.Current.DatumBis.HasValue) view.Current.DatumBis = view.Current.DatumBis.Value.Date.MondayOf().AddDays(6);
        }
    }
}
