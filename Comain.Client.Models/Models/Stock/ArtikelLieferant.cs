﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{

    public class ArtikelLieferant : Entity, IDataErrorInfo
    {

        private String                  name;
        private String                  nummer;
        private decimal?                preis;
        private short?                  rank;
        private LieferantFilterModel    lieferant;
        private decimal?                mindestbestellmenge;
        private int?                    bestellzeit;


        public int      Materialstamm_Id        { get; set; }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public decimal? Preis
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }


        public decimal? Mindestbestellmenge
        {
            get { return mindestbestellmenge; }
            set { SetProperty(ref mindestbestellmenge, value); }
        }


        public int? Bestellzeit
        {
            get { return bestellzeit; }
            set { SetProperty(ref bestellzeit, value); }
        }


        public short? Rank
        {
            get { return rank; }
            set {

                if (SetProperty(ref rank, value)) {

                    RaisePropertyChanged("Rank0");
                    RaisePropertyChanged("Rank1");
                    RaisePropertyChanged("Rank2");
                    RaisePropertyChanged("Rank3");
                }
            }
        }


        public bool Rank0
        {
            get { return rank.GetValueOrDefault() == 0; }
            set { if (value) Rank = null; }
        }


        public bool Rank1
        {
            get { return rank.GetValueOrDefault() == 1; }
            set { if (value) Rank = 1; }
        }


        public bool Rank2
        {
            get { return rank.GetValueOrDefault() == 2; }
            set { if (value) Rank = 2; }
        }


        public bool Rank3
        {
            get { return rank.GetValueOrDefault() == 3; }
            set { if (value) Rank = 3; }
        }


        public LieferantFilterModel Lieferant
        {
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }


#region V A L I D A T I O N


        public string Error
        {
            get {  return null;  }
        }


        public string this[string columnName]
        {
            get {  return null;  }
        }


#endregion

    }
}
