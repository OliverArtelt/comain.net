﻿using System;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class ArtikelListViewItem : NotifyBase
    {

        private String      nummer;
        private String      name;
        private String      warengruppe;
        private String      iHObjekte;
        private String      lieferanten;
        private decimal?    mindestbestand;
        private decimal?    bestellmenge;
        private decimal?    bestandReal;
        private decimal?    mischpreis;
        private DateTime?   deaktiviertAm;


        public int Id  { get; set; }


        public DateTime? DeaktiviertAm
        {
            get { return deaktiviertAm; }
            set { if (SetProperty(ref deaktiviertAm, value)) UpdateStatus(); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Warengruppe
        {
            get { return warengruppe; }
            set { SetProperty(ref warengruppe, value); }
        }


        public String IHObjekte
        {
            get { return iHObjekte; }
            set { SetProperty(ref iHObjekte, value); }
        }


        public String Lieferanten
        {
            get { return lieferanten; }
            set { SetProperty(ref lieferanten, value); }
        }


        public decimal? Mindestbestand
        {
            get { return mindestbestand; }
            set { if (SetProperty(ref mindestbestand, value)) UpdateStatus(); }
        }


        public decimal? Bestellmenge
        {
            get { return bestellmenge; }
            set { if (SetProperty(ref bestellmenge, value)) UpdateStatus(); }
        }


        public decimal? BestandReal
        {
            get { return bestandReal; }
            set { if (SetProperty(ref bestandReal, value)) UpdateStatus(); }
        }


        public decimal? Mischpreis
        {
            get { return mischpreis; }
            set { SetProperty(ref mischpreis, value); }
        }


        public ArtikelStatus Status
        {
            get {

                if (model != null) return model.Status;

                //in der Berechnung fehlt reservierte Menge
                if (Mindestbestand.GetValueOrDefault() == 0) return ArtikelStatus.Frei;
                if (BestandReal.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return ArtikelStatus.Vorhanden;
                if (BestandReal.GetValueOrDefault() + Bestellmenge.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return ArtikelStatus.Bestellt;
                return ArtikelStatus.Fehlt;
            }
        }


        public String Color
        {
            get {

                if (DeaktiviertAm.HasValue) return "CornflowerBlue";
                if (Status == ArtikelStatus.Vorhanden) return "LimeGreen";
                if (Status == ArtikelStatus.Bestellt) return "Gold";
                if (Status == ArtikelStatus.Fehlt) return "Coral";
                return "LightSlateGray";
            }
        }


        public String SortStatus
        {
            get {

                if (DeaktiviertAm.HasValue) return "E";
                if (Status == ArtikelStatus.Vorhanden) return "C";
                if (Status == ArtikelStatus.Bestellt) return "B";
                if (Status == ArtikelStatus.Fehlt) return "A";
                return "D";
            }
        }


        private void UpdateStatus()
        {

            RaisePropertyChanged(nameof(Status));
            RaisePropertyChanged(nameof(Color));
            RaisePropertyChanged(nameof(SortStatus));
        }


        private Artikel model;
        public Artikel Model
        {
            get { return model; }
            set {

                if (value != model) {

                    if (model != null) PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    model = value;
                    if (model != null) PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                    Emboss();
                }
            }
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case "Nummer":

                Nummer = model.Nummer;
                break;

            case "Name":

                Name = model.Name;
                break;

            case "Warengruppe":

                if (model.Warengruppe != null) Warengruppe = model.Warengruppe.Nummer;
                break;

            case "IHObjekte":

                IHObjekte = String.Join(", ", model.MaterialIHObjektMap.Select(p => p.IHObjekt.Nummer));
                break;

            case "Lieferanten":

                Lieferanten = String.Join(", ", model.MaterialLieferantMap.Select(p => p.Lieferant.Name1));
                break;

            case "Mindestbestand":

                Mindestbestand = model.Mindestbestand;
                UpdateStatus();
                break;

            case "Bestellmenge":

                Bestellmenge = model.Bestellmenge;
                UpdateStatus();
                break;

            case "BestandReal":

                BestandReal = model.BestandReal;
                UpdateStatus();
                break;

            case "Mischpreis":

                Mischpreis = model.Mischpreis;
                break;

            case "DeaktiviertAm":

                DeaktiviertAm = model.DeaktiviertAm;
                break;
            }
        }


        public ArtikelListViewItem()
        {}


        public ArtikelListViewItem(Artikel model)
        {

            Model = model;
            Emboss();
        }


        public ArtikelListViewItem(ArtikelListDto dto)
        {

            Id               = dto.Id;
            Nummer           = dto.Nummer;
            Name             = dto.Name;
            Warengruppe      = dto.Warengruppe;
            IHObjekte        = dto.IHObjekte;
            Lieferanten      = dto.Lieferanten;
            Mindestbestand   = dto.Mindestbestand;
            Bestellmenge     = dto.Bestellmenge;
            BestandReal      = dto.BestandReal;
            Mischpreis       = dto.Mischpreis;
            DeaktiviertAm    = dto.DeaktiviertAm;
        }


        public void Emboss()
        {

            Id               = model.Id;
            Nummer           = model.Nummer;
            Name             = model.Name;
            if (model.Warengruppe != null) Warengruppe = model.Warengruppe.Nummer;
            IHObjekte        = null;
            Lieferanten      = null;
            if (!model.MaterialIHObjektMap.IsNullOrEmpty())  IHObjekte = String.Join(", ", model.MaterialIHObjektMap.Select(p => p.IHObjekt.Nummer));
            if (!model.MaterialLieferantMap.IsNullOrEmpty()) Lieferanten = String.Join(", ", model.MaterialLieferantMap.Select(p => p.Lieferant.Name1));
            Mindestbestand   = model.Mindestbestand;
            Bestellmenge     = model.Bestellmenge;
            BestandReal      = model.BestandReal;
            Mischpreis       = model.Mischpreis;
            DeaktiviertAm    = model.DeaktiviertAm;
        }
    }
}
