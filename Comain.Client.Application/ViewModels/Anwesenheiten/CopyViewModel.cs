﻿using System;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Models;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Anwesenheiten;

namespace Comain.Client.ViewModels.Anwesenheiten
{

    public class CopyViewModel: ViewModel<ICopyView>
    {

        public CopyViewModel(ICopyView view) : base(view)
        {

            AllDaysCommand     = new DelegateCommand(_ => { SetDays(Weekday.Komplett); });
            WorkingDaysCommand = new DelegateCommand(_ => { SetDays(Weekday.Werktage); });
            ClearDaysCommand   = new DelegateCommand(_ => { SetDays(Weekday.Undefiniert); });

            Von = DateTime.Today.AddDays(1);
            Bis = DateTime.Today.AddDays(7);
        }


        public void Unload()
        {
        }


        private void SetDays(Weekday pattern)
        {

            tage = pattern;

            RaisePropertyChanged(nameof(Montags));
            RaisePropertyChanged(nameof(Dienstags));
            RaisePropertyChanged(nameof(Mittwochs));
            RaisePropertyChanged(nameof(Donnerstags));
            RaisePropertyChanged(nameof(Freitags));
            RaisePropertyChanged(nameof(Samstags));
            RaisePropertyChanged(nameof(Sonntags));
            RaisePropertyChanged(nameof(Error));
        }


        public AssignedCopyRequest GebeFilter(Konfiguration cfg)
        {

            if (!Error.IsNullOrEmpty()) return null;
            return new AssignedCopyRequest { Asset_Id = Asset.Id, Datum = QuellDatum, Von = Von, Bis = Bis, Weekdays = (int)tage };
        }


#region P R O P E R T I E S


        private ICommand copyCommand;
        public ICommand CopyCommand
        {
            get => copyCommand;
            set => SetProperty(ref copyCommand, value);
        }


        private IHFilterItem asset;
        public IHFilterItem Asset
        {
            get => asset;
            set {

                if (SetProperty(ref asset, value)) {

                    RaisePropertyChanged(nameof(AssetAsString));
                    RaisePropertyChanged(nameof(Error));
                }
            }
        }


        public String AssetAsString => $"{Asset?.Name} - {Asset?.Nummer}";


        private DateTime quellDatum;
        public DateTime QuellDatum
        {
            get => quellDatum;
            set { if (SetProperty(ref quellDatum, value)) RaisePropertyChanged(nameof(Error)); }
        }


        private DateTime von;
        public DateTime Von
        {
            get => von;
            set { if (SetProperty(ref von, value)) RaisePropertyChanged(nameof(Error)); }
        }


        private DateTime bis;
        public DateTime Bis
        {
            get => bis;
            set { if (SetProperty(ref bis, value)) RaisePropertyChanged(nameof(Error)); }
        }


        private Weekday tage;


        public bool Montags
        {
            get => tage.HasFlag(Weekday.Montag);
            set {

                tage = tage.Flip(Weekday.Montag, value);
                RaisePropertyChanged(nameof(Montags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Dienstags
        {
            get => tage.HasFlag(Weekday.Dienstag);
            set {

                tage = tage.Flip(Weekday.Dienstag, value);
                RaisePropertyChanged(nameof(Dienstags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Mittwochs
        {
            get => tage.HasFlag(Weekday.Mittwoch);
            set {

                tage = tage.Flip(Weekday.Mittwoch, value);
                RaisePropertyChanged(nameof(Mittwochs));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Donnerstags
        {
            get => tage.HasFlag(Weekday.Donnerstag);
            set {

                tage = tage.Flip(Weekday.Donnerstag, value);
                RaisePropertyChanged(nameof(Donnerstags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Freitags
        {
            get => tage.HasFlag(Weekday.Freitag);
            set {

                tage = tage.Flip(Weekday.Freitag, value);
                RaisePropertyChanged(nameof(Freitags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Samstags
        {
            get => tage.HasFlag(Weekday.Samstag);
            set {

                tage = tage.Flip(Weekday.Samstag, value);
                RaisePropertyChanged(nameof(Samstags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        public bool Sonntags
        {
            get => tage.HasFlag(Weekday.Sonntag);
            set {

                tage = tage.Flip(Weekday.Sonntag, value);
                RaisePropertyChanged(nameof(Sonntags));
                RaisePropertyChanged(nameof(Error));
            }
        }


        private bool skipWhenAlreadyPlanned;
        public bool SkipWhenAlreadyPlanned
        {
            get => skipWhenAlreadyPlanned;
            set {

                if (value) {

                    skipWhenAlreadyPlanned = true;
                    RaisePropertyChanged(nameof(SkipWhenAlreadyPlanned));
                    RaisePropertyChanged(nameof(DeleteWhenAlreadyPlanned));
                }
            }
        }


        public bool DeleteWhenAlreadyPlanned
        {
            get => !skipWhenAlreadyPlanned;
            set {

                if (value) {

                    skipWhenAlreadyPlanned = false;
                    RaisePropertyChanged(nameof(SkipWhenAlreadyPlanned));
                    RaisePropertyChanged(nameof(DeleteWhenAlreadyPlanned));
                }
            }
        }


        public ICommand AllDaysCommand      { get; }
        public ICommand WorkingDaysCommand  { get; }
        public ICommand ClearDaysCommand    { get; }


        public String Error
        {
            get {

                if (Asset == null) return "Geben Sie das Asset an deren Anwesenheiten kopiert werden sollen.";
                if (QuellDatum == default) return "Geben Sie das Quelldatum an dessen Anwesenheiten kopiert werden sollen.";
                if (Von == default) return "Geben Sie das Startdatum des Zeitraumes an für den die Kopien erzeugt werden sollen.";
                if (Bis == default) return "Geben Sie das Enddatum des Zeitraumes an für den die Kopien erzeugt werden sollen.";
                if (Von <= QuellDatum) return "Das Startdatum des Zeitraumes darf nicht kleiner als das Quelldatum sein.";
                if (Bis < Von) return "Das Enddatum des Zeitraumes darf nicht kleiner als dessen Startdatum sein.";
                if (Von < DateTime.Today) return "Das Startdatum des Zeitraumes für den die Kopien erzeugt werden sollen darf nicht in der Vergangenheit liegen.";
                if ((Bis - Von).TotalDays > 500) return "Der Zeitraum ist zu groß. Verwenden Sie einen kleineren Zeitraum.";
                if (tage == Weekday.Undefiniert) return "Geben Sie die Wochentage an für die Kopien erstellt werden sollen.";

                return null;
            }
        }


#endregion


    }
}
