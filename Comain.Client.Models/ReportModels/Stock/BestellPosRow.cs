﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{
    
    public class BestellPosRow
    {
        
        private readonly Bestellposition model;


        public BestellPosRow(Bestellposition model)
        {
            this.model = model;
        }


        public int?     Position        { get { return model.Position; } }
        public String   LfArtikelnummer { get { return model.LfArtikelnummer; } }
        public String   Artikelnummer   { get { return model.Artikelnummer; } }
        public String   Artikelname     { get { return model.Artikelname; } }
        public String   Menge           { get { return (model.Menge.ToString() + " " + model.Einheit).Trim(); } }
        public decimal? EPreis          { get { return model.Preis; } }
        public decimal? GPreis          { get { return model.GPreis; } }
    }
}
