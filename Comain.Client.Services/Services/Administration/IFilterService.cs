﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;

namespace Comain.Client.Services.Administration
{

    public interface IFilterService
    {

        Task<IList<LeistungsartFilterModel>>    LeistungsartenAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>        PersonalAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>          ObjektgruppenAsync(CancellationToken ct = default);
        Task<IList<AuReopenDto>>                AuReopenAsync(CancellationToken ct = default);
        Task<IList<DringlichkeitDto>>           DringlichkeitenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>          GewerkeAsync(CancellationToken ct = default);
    }
}
