﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using AutoMapper;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Aufträge;
using Comain.Client.Controllers.Tickets;
using Comain.Client.ViewData;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Controllers.WPlan;

namespace Comain.Client.Controllers.Aufträge
{

    public class AuftragController : ControllerBase, IAuftragController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IAuftragService store;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IModulService module;
        private readonly IAuftragCommandMediator commands;
        private readonly IFlyoutService flyouts;
        private readonly IMapper mapper;
        private readonly MainViewModel mainView;
        private readonly AuftragViewModel view;
        private readonly AuftragFilterViewModel filterView;
        private readonly AuftragListViewModel listView;
        private readonly AuftragDetailViewModel detailView;
        private readonly AuftragDetailBasisViewModel basisView;
        private readonly AuftragDetailPersonalViewModel personalView;
        private readonly AuftragDetailProtokollViewModel protokollView;
        private readonly AuftragDetailLeistungViewModel leistungView;
        private readonly AuftragDetailPrintViewModel printView;
        private readonly ReopenViewModel reopenView;
        private IPersonalController leController;
        private IMaterialController mtController;
        private Konfiguration config;
        private bool loadCompleted;
        private SerializableCommand fbNummerCommand;
        private SerializableCommand mtEditCommand;
        private SerializableCommand leEditCommand;
        private IDringlichkeitenProvider drlist;
        private IChecklistPluginController wtController;
        private IDokumentAssignController docs;


        public AuftragController(IBreadcrumbService crumbs, IAuftragService store, MainViewModel mainView, IMessageService messages, ILoginService login,
                                 IAuftragCommandMediator commands, IFlyoutService flyouts, IKonfigService configs, IControllerBus bus, IModulService module,
                                 IMapper mapper,
                                 AuftragViewModel view, AuftragListViewModel listView, AuftragDetailViewModel detailView, AuftragDetailBasisViewModel basisView,
                                 AuftragDetailPersonalViewModel personalView, AuftragDetailProtokollViewModel protokollView, AuftragDetailLeistungViewModel leistungView,
                                 AuftragFilterViewModel filterView, AuftragDetailPrintViewModel printView, ReopenViewModel reopenView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.store = store;
            this.login = login;
            this.module = module;
            this.commands = commands;
            this.mapper = mapper;
            this.mainView = mainView;
            this.view = view;
            this.filterView = filterView;
            this.listView = listView;
            this.detailView = detailView;
            this.basisView = basisView;
            this.personalView = personalView;
            this.protokollView = protokollView;
            this.leistungView = leistungView;
            this.printView = printView;
            this.flyouts = flyouts;
            this.configs = configs;
            this.reopenView = reopenView;
        }


        public void Initialize()
        {

            docs = bus.GetController<IAUDokumentController>();
            leController = bus.GetController<IPersonalController>();
            mtController = bus.GetController<IMaterialController>();
            wtController = bus.GetController<IChecklistPluginController>();
            detailView.PositionenView = wtController.ChecklistView;
            view.PositionDetailCommand = wtController.OpenDetailCommand;

            view.BackCommand        = new AsyncDelegateCommand(_ => bus.BackAsync(detailView.Current == null || detailView.Current.IstAngefügt? null: detailView.Current));
            view.CancelCommand      = CreateCommand(_ => VerwerfenAsync());
            view.ReloadCommand      = CreateCommand(_ => LoadAsync());
            view.SaveCommand        = CreateCommand(_ => SaveAsync());
            view.EditCommand        = CreateCommand(_ => EditAsync());
            view.AddCommand         = CreateCommand(_ => AddAsync());
            view.LEEditCommand      = leEditCommand = CreateCommand(_ => LEEditAsync(), _ => leistungView.Personalleistung != null);
            view.LENeuCommand       = CreateCommand(_ => LEAddAsync());
            view.MTEditCommand      = mtEditCommand = CreateCommand(_ => MTEditAsync(), _ => leistungView.Materialleistung != null);
            view.MTNeuCommand       = CreateCommand(_ => MTAddAsync());
            view.StornoCommand      = CreateCommand(_ => StornoAsync());
            view.DeleteCommand      = CreateCommand(_ => DeleteAsync());
            view.CloseCommand       = CreateCommand(_ => AbschließenAsync());
            view.FbNummerCommand    = fbNummerCommand = CreateCommand(_ => FbNummerAsync(), _ => basisView.FbVisible);
            view.ReopenCommand      = CreateCommand(_ => ReopenAsync());
            listView.DetailCommand  = CreateCommand(_ => DetailAsync());
            leistungView.LEEditCommand = CreateCommand(async _ => { if (leistungView.Personalleistung == null) await LEAddAsync(); else await LEEditAsync(); });
            leistungView.MTEditCommand = CreateCommand(async _ => { if (leistungView.Materialleistung == null) await MTAddAsync(); else await MTEditAsync(); });
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());

            view.FilterView = filterView.View;
            view.ListView = listView.View;
            view.DetailView = detailView.View;
            detailView.BasisView = basisView.View;
            detailView.PersonalView = personalView.View;
            detailView.ProtokollView = protokollView.View;
            detailView.LeistungView = leistungView.View;
            detailView.PrintView = printView.View;
            detailView.DokumentView = docs?.View;

            PropertyChangedEventManager.AddHandler(listView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(printView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(basisView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(leistungView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Aufträge");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;

            filterView.HasTyp7 = module.Info.VerwendetModulTicket;
            filterView.HasTyp8 = module.Info.VerwendetModulBevor;
            filterView.HasTyp9 = module.Info.VerwendetModulWPlan;
            //wenn es nur Standardaufträge gibt dann nicht nur diese eine Checkbox anzeigen, Filter Standardaufträge ist dann implizit
            filterView.HasTyp0 = filterView.AuTyp7 || filterView.AuTyp8 || filterView.AuTyp9;

            await ExecuteWithLockAsync(_ => LoadAsync());
            if (parameter is ControllerMessage msg) {

                if (msg.Entity_Id > 0) await ExecuteWithLockAsync(_ => DetailAsync(msg.Entity_Id));
                else if (msg.EditMode == EditMode.Append) {

                    var au = msg.Parameter as Auftrag;
                    await ExecuteWithLockAsync(_ => AddAsync(au));
                }
            }

            await docs.RunAsync(parameter);
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(leistungView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(basisView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(printView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(listView, ViewPropertyChanged, "");
        }


        public void SetEditMode(EditMode newMode)
        {

            commands.SetEditMode(newMode);
            if (module.Info.VerwendetModulWPlan) wtController.SetEditMode(newMode);
            docs.SetEditMode(newMode);
        }


        private async Task LoadAsync()
        {

            loadCompleted = false;
            var oldlistmodel = listView.Current;
            filterView.IstKunde = login.AktuellerNutzer.IstProduktion;
            SetEditMode(EditMode.ReadOnly);
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                var pstask      = store.FilterPersonalAsync(bus.Token);
                var pztask      = store.FilterStandorteAsync(bus.Token);
                var gwtask      = store.FilterGewerkeAsync(bus.Token);
                var ihtask      = store.FilterIHObjekteAsync(bus.Token);
                var ksttask     = store.FilterKostenstellenAsync(bus.Token);
                var latask      = store.FilterLeistungsartenAsync(bus.Token);
                var sbtask      = store.FilterSchadensbilderAsync(bus.Token);
                var sutask      = store.FilterSchadensursachenAsync(bus.Token);
                var ssbtask     = store.FilterSubSchadensbilderAsync(bus.Token);
                var ssutask     = store.FilterSubSchadensursachenAsync(bus.Token);
                var ikstask     = store.FilterAuIHKSAsync(bus.Token);
                var intAuSttask = store.FilterInterneAuStelleAsync(bus.Token);
                var drtask      = store.FilterDringlichkeitAsync(bus.Token);
                var leTask      = leController.LoadAsync();
                var mtTask      = mtController.LoadAsync();
                var wtTask      = module.Info.VerwendetModulWPlan ? wtController.LoadAsync(): Task.FromResult(true);
                var cfgtask     = configs.GetAsync(bus.Token);

                var satask      = LoadListAsync();

                await Task.WhenAll(pstask, pztask, gwtask, ihtask, ksttask, latask, sbtask, sutask, ssbtask, ssutask,
                                   ikstask, intAuSttask, drtask, leTask, mtTask, wtTask, cfgtask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();

                drlist = drtask.Result;
                await filterView.LoadAsync(ihtask.Result.Select(p => new IHFilterItem(p)).ToList(),
                                           pztask.Result, ksttask.Result, pstask.Result, gwtask.Result, intAuSttask.Result, bus.Token);

                personalView.SetzeListen(pstask.Result, gwtask.Result);
                await basisView.SetzeListenAsync(mapper.Map<List<Leistungsart>>(latask.Result), sbtask.Result, sutask.Result, ssbtask.Result, ssutask.Result).ConfigureAwait(false);
                await basisView.SetAuIHKSListAsync(ikstask.Result, pztask.Result, ksttask.Result, bus.Token).ConfigureAwait(false);

                config = cfgtask.Result;
                basisView.Konfiguration = config;
                commands.Initialize(config);

                if (oldlistmodel != null) {

                    listView.Current = listView.Foundlist.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                    await DetailAsync();
                }
            }

            loadCompleted = true;
        }


        private async Task SearchAsync()
        {

            if (loadCompleted) await LoadListAsync();
            else               await LoadAsync();
        }


        private async Task LoadListAsync()
        {

            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                listView.Current = null;
                SetModel(null);

                var list = await store.AufträgeAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                await OnUIThreadAsync(() => {

                    listView.Foundlist = new ObservableCollection<AuftragListViewItem>(list.Select(p => new AuftragListViewItem(p)).ToList());

                    decimal? stunden = list.Sum(p => p.Stunden);

                    var bld = new StringBuilder();
                    if (list.Count == 0) bld.Append("FILTER: Keine Aufträge gefunden");
                    else if (list.Count == 1) bld.Append("FILTER: Ein Auftrag gefunden");
                    else bld.Append("FILTER: ").Append(list.Count.ToString("N0")).Append(" Aufträge gefunden");

                    if (stunden.GetValueOrDefault() != 0m) bld.Append(", Gesamtstunden: ").Append(stunden.Value.ToString("N"));
                    filterView.ResultText = bld.ToString();

                });

                await docs.LoadAsync();
            }
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await mtController?.UnloadAsync()) return false;
            if (!await leController?.UnloadAsync()) return false;

            if (detailView.Current != null && commands.CurrentMode != EditMode.ReadOnly &&
                (detailView.Current.IsChanged || detailView.Current.DocumentsChanged) ||
                module.Info.VerwendetModulWPlan && wtController.HasChanges) {

                detailView.ResetTabsIfPrint();
                if (!await messages.ConfirmExitAsync()) return false;
            }

            listView.Unload();
            await filterView.UnloadAsync();
            basisView.Unload();
            leistungView.Unload();
            personalView.Unload();
            if (module.Info.VerwendetModulWPlan) wtController.Unload();
            await docs.UnloadAsync();

            return true;
        }


        private async Task DetailAsync(int? auid = null)
        {

            if (listView.Current != null || auid.HasValue) {

                Auftrag model = null;

                using (new VisualDelay(detailView)) {

                    int id = auid.HasValue? auid.Value : listView.Current.Id;
                    model = await store.DetailAsync(id);
                    if (model == null) throw new ComainOperationException("Der Auftrag wurde nicht gefunden.");
                    model.Leistungsart = basisView.Leistungsarten?.FirstOrDefault(p => p.Id == model.Leistungsart_Id);
                    if (model.Leistungsart == null) throw new ComainOperationException("Die Leistungsart des Auftrages konnte nicht zugeordnet werden.");

                    await OnUIThreadAsync(() => { basisView.Dringlichkeiten = drlist.Liste(model.Typ); });
                    model.Dringlichkeit = drlist[model.Dringlichkeit_Id];

                    model.AcceptChanges();
                    SetModel(model);
                    await docs.DetailAsync(model);
                }

                if (module.Info.VerwendetModulWPlan) await wtController.LoadAuftragAsync(model);
                if (detailView.Tab == AuftragDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
            }
        }


        private void SetModel(Auftrag current)
        {

            if (current != null) current.Konfiguration = config;

            detailView.Current = current;
            basisView.Current = current;
            personalView.Current = current;
            leistungView.Current = current;

            if (!detailView.PositionenVisible && detailView.Tab == AuftragDetailViewModel.Tabs.Checkliste) detailView.ResetTabs();

            if (current != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null) listviewitem.Model = current;
            }

            UpdateCommands();
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if ((args.PropertyName == "Materialleistung" || args.PropertyName == "Personalleistung") && sender == leistungView) {

                    UpdateCommands();
                }

                if (args.PropertyName == "Unterauftrag") {

                    UpdateCommands();
                }

                if (args.PropertyName == "CurrentTab") {

                    UpdateCommands();
                    await UpdatePrintViewAsync();
                }

                if (args.PropertyName == "DruckVollständig") {

                    await UpdatePrintViewAsync();
                }

                if (args.PropertyName == "Leistungsart_Id" && sender == basisView) {

                    if (detailView.Current != null) detailView.Current.NormalisiereAuftragsnummer();
                    detailView.SetzeHeader();
                }

                if (args.PropertyName == "AuIHKS" && sender == basisView) {

                    detailView.SetzeHeader();
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private async Task UpdatePrintViewAsync()
        {

            printView.Unload();

            if (detailView.Tab != AuftragDetailViewModel.Tabs.Druck) return;
            if (detailView.Current == null) return;
            if (detailView.Current.IstAngefügt) return;
            printView.HatWartungsplan = module.Info.VerwendetModulWPlan && detailView.Current.Typ == Auftragstyp.Wartungsplan;

            if (printView.HatWartungsplan && printView.DruckWartungsplan) {

                var report = await wtController.ReportAsync(null).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();
                await printView.BindViewerAsync(report);

            } else {

                var address = printView.DruckVollständig ? "Comain.Client.Reports.AuftragReport.rdlc": "Comain.Client.Reports.AuftragLeerReport.rdlc";
                var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = detailView.Current.Nummer, HasLogo = true };
                var ihpath = await store.IHPathAsync(detailView.Current?.AuIHKS?.IHObjekt, bus.Token);
                var model = new AuftragRow(detailView.Current, ihpath, printView.DruckVollständig);
                report.AddDataSource("MainData", new List<AuftragRow> { model });
                if (printView.DruckVollständig) report.AddDataSource("LeDataSet", model.Leistungen);
                report.AddParameter("parPrice", login.AktuellerNutzer.IstProduktion || login.AktuellerNutzer.IstInstandhaltungInstandhalter? "0": "1");

                bus.Token.ThrowIfCancellationRequested();
                await printView.BindViewerAsync(report);
            }
        }


        private bool IsChanged => detailView.Current != null && (detailView.Current.IsChanged ||
                                                                 module.Info.VerwendetModulWPlan && wtController.HasChanges ||
                                                                 detailView.Current.DocumentsChanged);


        private List<String> ErrorList
        {
            get {

                var result = new List<String>();

                if (!IsChanged) return result;
                result.AddRange(detailView.Current.ErrorList);
                if (module.Info.VerwendetModulWPlan) result.AddRange(wtController.ErrorList);
                return result;
            }
        }


        private async Task SaveAsync()
        {

            if (detailView.Current == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (IsChanged) {

                if (ErrorList.Count > 0) {

                    await messages.ShowMessageAsync(new MessageToken { Header = "Der Auftrag kann nicht gespeichert werden.", Reasons = ErrorList });
                    return;
                }

                if (module.Info.VerwendetModulWPlan && !await wtController.ConfirmSaveAsync()) return;

                using (new VisualDelay(detailView)) {

                    var model = await store.SaveAsync(detailView.Current, bus.Token);
                    if (module.Info.VerwendetModulWPlan) await wtController.SaveAsync();
                    model.Dringlichkeit = drlist[model.Dringlichkeit_Id];

                    bus.Token.ThrowIfCancellationRequested();

                    model.AcceptChanges();
                    SetEditMode(EditMode.ReadOnly);
                    SetModel(model);

                    if (module.Info.VerwendetModulWPlan) await wtController.LoadAuftragAsync(model);
                    docs.UpdateEntityId(model);
                    await docs.SaveAsync();
                }
            }

            SetEditMode(EditMode.ReadOnly);
            await DetailAsync();
        }


        private Task EditAsync()
        {

            SetEditMode(EditMode.Edit);
            UpdateCommands();

            return Task.FromResult(true);
        }


        private async Task AddAsync(Auftrag model = null)
        {

            if (model?.AuIHKS != null && model.AuIHKS.Id == default) model.AuIHKS = basisView.IHObjekte.FirstOrDefault(p => p.IHObjekt_Id == model.AuIHKS.IHObjekt_Id && p.IstAktiv);
            model = new Auftrag(model ?? detailView.Current, config);

            SetModel(model);
            await OnUIThreadAsync(() => { basisView.Dringlichkeiten = drlist.Liste(model.Typ); });
            SetEditMode(EditMode.Append);
            detailView.ResetTabs();
            model.AcceptChanges();
            await docs.DetailAsync(model);

            UpdateCommands();
        }


        private async Task VerwerfenAsync()
        {

            await DetailAsync();
            SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            var model = detailView.Current;

            if (model == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (model.DeleteErrorList.Count > 0) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Auftrag kann nicht gelöscht werden.", Reasons = detailView.Current.DeleteErrorList });
                return;
            }

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich gelöscht werden?" })) return;

            using (new VisualDelay(detailView)) {

                await store.DeleteAsync(detailView.Current, bus.Token);
                SetEditMode(EditMode.ReadOnly);

                var listvm = listView.Foundlist.FirstOrDefault(p => p.Id == model.Id);
                if (listvm != null) await OnUIThreadAsync(() => { listView.Foundlist.Remove(listvm); });

                SetModel(null);
            }
        }


        private async Task StornoAsync()
        {

            if (detailView.Current == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (!detailView.Current.StornoErrorList.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Auftrag kann nicht storniert werden.", Reasons = detailView.Current.StornoErrorList });
                return;
            }

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich storniert werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.StornoAsync(detailView.Current, bus.Token);
                SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }


        private async Task AbschließenAsync()
        {

            if (detailView.Current == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (detailView.Current.AbschlussErrorList.Count > 0) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Auftrag kann nicht abgeschlossen werden.", Reasons = detailView.Current.AbschlussErrorList });
                return;
            }

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich abgeschlossen werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.CloseAsync(detailView.Current, bus.Token);
                SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }


        private async Task ReopenAsync()
        {

            if (detailView.Current == null) return;
            reopenView.Auftrag = detailView.Current;
            reopenView.Reason = null;
            reopenView.ReopenCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = reopenView.View, Position = MahApps.Metro.Controls.Position.Bottom })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.ReopenAsync(reopenView.Auftrag.Id, reopenView.Reason, bus.Token);
                SetEditMode(EditMode.ReadOnly);
                SetModel(model);
            }
        }


        private async Task LEEditAsync()
        {

            if (leistungView.Current == null) return;
            if (leistungView.Personalleistung == null) return;

            await leController.RunAsync(new LeMtParameter { Auftrag = leistungView.Current, Leistung = leistungView.Personalleistung, EditMode = commands.CurrentMode });
        }


        private async Task LEAddAsync()
        {

            if (leistungView.Current == null) return;
            if (leistungView.Current.KostenstelleVerlegt) return;
            if (commands.CurrentMode == EditMode.Append) return;

            await leController.RunAsync(new LeMtParameter { Auftrag = leistungView.Current, EditMode = commands.CurrentMode });
        }


        private async Task MTEditAsync()
        {

            if (leistungView.Current == null) return;
            if (leistungView.Materialleistung == null) return;

            var ctx = new LeMtParameter { Auftrag = leistungView.Current, Material = leistungView.Materialleistung, EditMode = commands.CurrentMode, Update = leistungView.Refresh };
            await mtController.RunAsync(ctx);
        }


        private async Task MTAddAsync()
        {

            if (leistungView.Current == null) return;
            if (leistungView.Current.KostenstelleVerlegt) return;
            if (commands.CurrentMode == EditMode.Append) return;

            var ctx = new LeMtParameter { Auftrag = leistungView.Current, EditMode = commands.CurrentMode, Update = leistungView.Refresh };
            await mtController.RunAsync(ctx);
        }


        private async Task FbNummerAsync()
        {

            if (!basisView.FbVisible) return;
            if (!String.IsNullOrWhiteSpace(basisView.FBNummer)) {

                if (!await messages.ConfirmAsync(new MessageToken { Header = "Der Auftrag hat bereits eine FB-Nummer. Soll diese überschrieben werden?" })) return;
            }

            basisView.Current.SetzeFbNummer();
            basisView.RefreshEnabledState();
        }


        private void UpdateCommands()
        {

            commands.NeuSetzen();

            context.Post(new SendOrPostCallback((o) => {

                leEditCommand.RaiseCanExecuteChanged();
                mtEditCommand.RaiseCanExecuteChanged();
                fbNummerCommand.RaiseCanExecuteChanged();

            }), null);
        }
    }
}
