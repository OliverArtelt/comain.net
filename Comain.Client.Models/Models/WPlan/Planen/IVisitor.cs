﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{
    public interface IVisitor
    {

        void Visit(Plan model);
        void Visit(PZPlanzeile model);
        void Visit(IHPlanzeile model);
        void Visit(MNPlanzeile model);
        void Visit(Planzelle model);
        void Visit(Planaufgabe model);

        /// <remarks>
        /// Kopfzeilenwert anpassen
        /// </remarks>
        void Visit(Planspaltensumme model);  
        /// <remarks>
        /// Kopfzeilenwert anpassen
        /// </remarks>
        void Visit(PZSpaltensumme model);  
        /// <remarks>
        /// Kopfzeilenwert anpassen
        /// </remarks>
        void Visit(IHSpaltensumme model); 

        /// <remarks>
        /// Zeilensummen für Export
        /// </remarks>
        void VisitAfter(Plan model);
        /// <remarks>
        /// Zeilensummen für Export
        /// </remarks>
        void VisitAfter(PZPlanzeile model);
        /// <remarks>
        /// Zeilensummen für Export
        /// </remarks>
        void VisitAfter(IHPlanzeile model);
    }
}
