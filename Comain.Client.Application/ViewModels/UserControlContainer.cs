﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;


namespace Comain.Client.ViewModels
{

    public class UserControlContainer
    {

        public UserControl Control { get; set; }
    }
}
