﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.ViewModels.Stock.Bestellungen;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Bestellungen
{

    public class AddBestellController : ControllerBase, IAddBestellController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly MainViewModel mainView;
        private readonly BestellNeuViewModel view;
        private BestellAddData current;


        public AddBestellController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts, IControllerBus bus,
                                    MainViewModel mainView, BestellNeuViewModel view)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
        }


        public void Initialize()
        {

            view.BackCommand = new DelegateCommand(Back);
            view.TakeCommand = CreateCommand(_ => TakeAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            current = parameter as BestellAddData;
            if (current != null) {

                mainView.PageView = view.View;
                view.Unload();
                crumbs.Add("Neu");
                view.Mandant = login.AktuelleVerbindung;
                view.SetzeListen(current);
            }
        }


        public void Shutdown()
        {}


        private async Task TakeAsync()
        {

            if (current == null || current.Callback == null) return;

            var atlist = view.GetSelectedAt();
            if (atlist.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Wählen Sie mindestens ein Artikel der bestellt werden soll." });
                return;
            }

            if (view.Lieferant == null && ((CollectionView)view.Lieferanten).Count == 1) view.Lieferant = ((CollectionView)view.Lieferanten).Cast<Lieferant>().First();
            if (view.Lieferant == null) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Wählen Sie den Lieferanten bei dem bestellt werden soll." });
                return;
            }

            if ((view.Mode == NeuSelektorMode.Artikel || view.Mode == NeuSelektorMode.Lieferant) &&
                 !atlist.All(p => p.Lieferanten.Any(q => q.LieferantId == view.Lieferant.Id))) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Lieferant führt nicht alle gewählte Artikel." });
                return;
            }

            var bs = new Bestellung();
            bs.Lieferant = view.Lieferant;
            bs.Positionen = new NotifyTrackableCollection<Bestellposition>(atlist.Select(p => p.AsBestellposition(bs.Lieferant)));
            int ndx = 1;
            bs.Positionen.ForEach(p => p.Position = ndx++);

            current.Callback(bs);
        }


        private void Back()
        {

            crumbs.Remove();
            if (current != null && current.Callback != null) current.Callback(null);
        }


        public Task<bool> UnloadAsync()
        {

            view.Unload();
            return Task.FromResult(true);
        }
    }
}
