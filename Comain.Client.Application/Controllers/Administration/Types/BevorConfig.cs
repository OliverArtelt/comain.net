﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Administration.Types
{

    public class BevorConfig : IAdminTyp
    {

        public String       Title           => "Besondere Vorkommnisse";
        public int          Sortierung      => 31000; 
        public Type         Controller      => typeof(IBevorConfigController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulBevor;
    }
}
