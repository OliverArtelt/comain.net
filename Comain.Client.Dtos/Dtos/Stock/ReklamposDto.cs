﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class ReklamposDto : ChangeTrackingBase
    {
        
        public int          Position                { get; set; }

        public int          ArtikelId               { get; set; }
        public int          EingangId               { get; set; }

        public decimal      Menge                   { get; set; }
        public decimal?     Preis                   { get; set; }
        public String       Notizen                 { get; set; }
        public String       LfArtikelnummer         { get; set; }
        public String       Artikelnummer           { get; set; }
        public String       Artikelbezeichnung      { get; set; }
        public String       Bestellposition         { get; set; }
    }
}
