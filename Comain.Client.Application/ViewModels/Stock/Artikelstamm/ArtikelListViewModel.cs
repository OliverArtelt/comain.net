﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Artikelstamm;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class ArtikelListViewModel : ViewModel<IArtikelListView>
    {
 

        public ArtikelListViewModel(IArtikelListView view) : base(view)
        {
        }


        public void Unload()
        {
            Foundlist = null;
        }


#region P R O P E R T I E S
 
        
        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private ObservableCollection<ArtikelListViewItem> foundlist;
        public ObservableCollection<ArtikelListViewItem> Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private ArtikelListViewItem current;
        public ArtikelListViewItem Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
