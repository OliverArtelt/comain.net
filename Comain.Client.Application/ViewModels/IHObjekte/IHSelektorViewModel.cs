﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;
using Comain.Client;
using Comain.Client.Controllers;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHSelektorViewModel : ViewModel<IIHSelektorView>, IDelayableViewModel
    {

        private readonly IKonfigService configs;
        private readonly SynchronizationContext context;
        private const int maxFilterCount = 100;

        /// <summary>
        /// IH-Objekte im PZ-Baum
        /// </summary>
        private Dictionary<int, TreeIHViewItem> pzTreeItems;
        /// <summary>
        /// IH-Objekte im KST-Baum
        /// </summary>
        private Dictionary<int, TreeIHViewItem> kstTreeItems;
        /// <summary>
        /// alle IH-Objekte
        /// </summary>
        private Dictionary<String, IHFilterItem> currentIHDic;
        private Konfiguration cfg;
        private int filterCount;


        public IHSelektorViewModel(IKonfigService configs, IIHSelektorView view) : base(view)
        {

            context = SynchronizationContext.Current;
            this.configs = configs;
            OpenCommand = new DelegateCommand(OpenPopup);
            ClearCommand = new DelegateCommand(Reset);
            SearchCommand = new Comain.Client.Controllers.AsyncDelegateCommand(_ => SearchAsync());
        }


        public async Task LoadAsync(List<IHFilterItem> ihlist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                    CancellationToken ct = default)
        {

            ct.ThrowIfCancellationRequested();

            using (new VisualDelay(this)) {

                cfg = await configs.GetAsync(ct);
                currentIHDic = ihlist.DistinctBy(p => p.Nummer).ToDictionary(p => p.Nummer);
                var kstreetask = GenerateKstTreeAsync(kstlist, ct);
                var pztreetask = GeneratePzTreeAsync(pzlist, ct);
                var pttreetask = CreatePathsAsync(ct);
                await Task.WhenAll(kstreetask, pztreetask, pttreetask).ConfigureAwait(false);
            }
        }


        private async Task CreatePathsAsync(CancellationToken ct)
        {

            await Task.Run(() => {

                foreach (var ih in currentIHDic.Values) {

                    ih.Baugruppen.Push(ih.Name);
                    var parent = currentIHDic.GetValueOrDefault(ih.ParentNummer);
                    while (parent != null) {

                        ih.Baugruppen.Push(parent.Name);
                        parent = currentIHDic.GetValueOrDefault(parent.ParentNummer);
                    }
                }
            });
        }


        private async Task GeneratePzTreeAsync(IEnumerable<PzFilterModel> pzs, CancellationToken ct)
        {

            await Task.Run(() => {

                Standorte = pzs.OrderBy(p => p.Nummer).Select(p => new TreePZViewItem(p)).ToList();

                var list = currentIHDic.Values.OrderBy(p => p.Nummer).Select(p => new TreeIHViewItem(p, cfg)).ToList();
                pzTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihdic = list.ToDictionary(p => p.IHNummer);

                list.ForEach(p => {

                    if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                        ihdic[p.NummerParent].Add(p);
                        p.Parent = ihdic[p.NummerParent];
                    }
                });

                ct.ThrowIfCancellationRequested();

                var pzdic = Standorte.ToDictionary(p => p.Model.Id);
                list.Where(p => p.IstMaschine && pzdic.ContainsKey(p.FilterModel.Standort_Id))
                    .ForEach(p => {

                        pzdic[p.FilterModel.Standort_Id].Add(p);
                        p.Parent = pzdic[p.FilterModel.Standort_Id];
                    });

                Standorte = Standorte.Where(p => !p.SubItems.IsNullOrEmpty()).OrderBy(p => p.Nummer).ToList();
                RaisePropertyChanged(nameof(Standorte));
            });
        }


        private async Task GenerateKstTreeAsync(IEnumerable<KstFilterModel> ksts, CancellationToken ct)
        {

            await Task.Run(() => {

                Kostenstellen = ksts.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).Select(p => new TreeKSTViewItem(p)).ToList();

                var list = currentIHDic.Values.OrderBy(p => p.Nummer).Select(p => new TreeIHViewItem(p, cfg)).ToList();
                kstTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihkstdict = list.ToMultiDictionary(p => p.FilterModel.Kostenstelle_Id);

                Kostenstellen.ForEach(kst => {

                    ct.ThrowIfCancellationRequested();
                    FillKst(kst, ihkstdict.GetValueOrDefault(kst.Model.Id));
                });

                Kostenstellen = Kostenstellen.Where(p => !p.SubItems.IsNullOrEmpty()).OrderBy(p => p.Nummer, new NaturalComparer()).ToList();
                RaisePropertyChanged(nameof(Kostenstellen));
            });
        }


        private void FillKst(TreeKSTViewItem kst, HashSet<TreeIHViewItem> ihlist)
        {

            if (ihlist == null) return;

            var ihdic = ihlist.ToDictionary(p => p.IHNummer);
            ihlist.ToList().ForEach(p => {

                if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                    ihdic[p.NummerParent].Add(p);
                    p.Parent = ihdic[p.NummerParent];
                    ihlist.Remove(p);
                }
            });

            ihlist.OrderBy(p => p.IHNummer)
                  .ForEach(p => {

                        kst.Add(p);
                        p.Parent = kst;
                    });
        }


        public void SetModel(int? ihid)
        {

            if (!ihid.HasValue) {

                CurrentIHObjekt = null;

            } else {

                CurrentIHObjekt = currentIHDic.Values.FirstOrDefault(p => p.Id == ihid);
                ExpandCurrentNode();
            }
        }


        /// <summary>
        /// von View aufgerufen: Benutzer hat auf einen Knoten geklickt -> IH laden
        /// </summary>
        public void SelectItem(object node)
        {

            if (!EditEnabled) return;
            var ihnode = node as TreeIHViewItem;
            if (ihnode != null) {       //IHO gewählt oder Tab-Umschaltung

                //schließen wenn IHO gewählt, aber nicht wenn nur Tab-Umschaltung
                if (CurrentIHObjekt != ihnode.FilterModel) PopupIsOpen = false;
                CurrentIHObjekt = ihnode.FilterModel;

            } else {

                CurrentIHObjekt = null;
            }
        }


        private void OpenPopup()
        {
            PopupIsOpen = true;
        }


        public void Reset()
        {

            PopupIsOpen = false;
            CurrentIHObjekt = null;
            Foundlist = null;
            SearchText = null;
            ViewCore.ResetSelection();
        }


        public void Unload()
        {

            PopupIsOpen = false;
            CurrentIHObjekt = null;
            Standorte = null;
            Kostenstellen = null;
            Foundlist = null;
            pzTreeItems = null;
            kstTreeItems = null;

            RaisePropertyChanged(nameof(Standorte));
            RaisePropertyChanged(nameof(Kostenstellen));
        }


        private Dictionary<int, TreeIHViewItem> CurrentDictionary
        {
            get {

                if (currentTab == 0) return pzTreeItems;
                if (currentTab == 1) return kstTreeItems;
                return null;
            }
        }


        private void ExpandCurrentNode()
        {

            if (CurrentIHObjekt == null) return;
            if (CurrentDictionary == null) return;
            var current = CurrentDictionary.GetValueOrDefault(CurrentIHObjekt.Id);
            if (current == null) return;

            ExpandCurrentNode(current);
            current.IsSelected = true;
        }


        private void ExpandCurrentNode(TreeViewItem node)
        {

            node.IsExpanded = true;
            if (node.Parent != null) ExpandCurrentNode(node.Parent);
        }


        private async Task SearchAsync()
        {

            List<IHFilterItem> list = null;
            if (!String.IsNullOrWhiteSpace(searchText)) {

                var terms = searchText.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                list = currentIHDic.Values.Where(p => p.Matches(terms)).ToList();
                filterCount = list.Count;
            }

            await context.SendAsync(new SendOrPostCallback((o) => { Foundlist = list.Take(maxFilterCount).ToList(); }), null);
        }


        public Func<bool> Validator     { get; set; }


#region P R O P E R T I E S


        public ICommand OpenCommand     { get; private set; }

        public ICommand ClearCommand    { get; private set; }

        public ICommand SearchCommand   { get; private set; }


        public bool ShowValidatorError => Validator == null? false: Validator();


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        public String ResultText
        {
            get {

                if (String.IsNullOrEmpty(searchText)) return "Geben Sie Suchbegriffe ein und bestätigen Sie mit Enter.";
                if (foundlist.IsNullOrEmpty()) return "Kein Asset gefunden.";
                if (filterCount == 1) return "Ein Asset gefunden.";
                if (filterCount <= maxFilterCount) return String.Format("{0} Assets gefunden.", filterCount);
                return String.Format("{0} Assets gefunden, die ersten {1} werden angezeigt.", filterCount, maxFilterCount);
            }
        }

        /// <summary>
        /// Gewähltes Objekt der Freisuche
        /// </summary>
        private IHFilterItem listSelected;
        public IHFilterItem ListSelected
        {
            get { return listSelected; }
            set {

                if (SetProperty(ref listSelected, value) && value != null) {

                    CurrentIHObjekt = value;
                    PopupIsOpen = false;
                }
            }
        }

        /// <summary>
        /// Ergebnis der Freisuche
        /// </summary>
        private List<IHFilterItem> foundlist;
        public List<IHFilterItem> Foundlist
        {
            get { return foundlist; }
            set {

                if (SetProperty(ref foundlist, value)) {

                    RaisePropertyChanged(nameof(ResultText));
                }
            }
        }


        private IHFilterItem currentIHObjekt;
        public IHFilterItem CurrentIHObjekt
        {
            get { return currentIHObjekt; }
            set {

                if (SetProperty(ref currentIHObjekt, value)) {

                    RaisePropertyChanged(nameof(IHNummer));
                    RaisePropertyChanged(nameof(IHName));
                    RaisePropertyChanged(nameof(IHBezeichnung));
                    RaisePropertyChanged(nameof(Baugruppenpfad));
                }

                RaisePropertyChanged(nameof(ShowValidatorError));
            }
        }


        public String IHNummer          => CurrentIHObjekt?.Nummer;
        public String IHName            => CurrentIHObjekt?.Name;
        public String IHBezeichnung     => UseWholePath? CurrentIHObjekt?.Pfad: CurrentIHObjekt?.Name;
        public String Baugruppenpfad    => CurrentIHObjekt?.Pfad;


        public bool UseWholePath    { get; set; }


        private bool clearButtonVisible = true;
        public bool ClearButtonVisible
        {
            get { return clearButtonVisible; }
            set { SetProperty(ref clearButtonVisible, value); }
        }


        private bool popupIsOpen;
        public bool PopupIsOpen
        {
            get { return popupIsOpen; }
            set { SetProperty(ref popupIsOpen, value); }
        }


        private bool editEnabled = true;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) ExpandCurrentNode(); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public  List<TreePZViewItem> Standorte { get; private set; }


        public  List<TreeKSTViewItem> Kostenstellen { get; private set; }


#endregion


    }
}
