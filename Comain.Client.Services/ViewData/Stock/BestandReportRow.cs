﻿using System;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock.Reports;

namespace Comain.Client.ViewData.Stock.Reports
{

    public class BestandReportRow
    {

        public int           ArtikelId              { get; set; }

        public String        PosTyp                 { get; set; }
        public String        Position               { get; set; }
        public String        Nummer                 { get; set; }
        public String        Name                   { get; set; }
        public String        Herstellernummer       { get; set; }
        public String        Warengruppe            { get; set; }

        public decimal?      Menge                  { get; set; }
        public decimal?      Mischpreis             { get; set; }
        public decimal?      Gesamtpreis            { get; set; }


        public BestandReportRow()
        {}


        public BestandReportRow(ArtikelReportDto dto)
        {

            PosTyp              = "Artikel";
            Position            = "Artikel";
            Nummer              = dto.Artikelnummer;
            Name                = dto.Artikelname;
            Warengruppe         = dto.Warengruppe;
            Herstellernummer    = dto.Herstellernummer;
        }


        public BestandReportRow(ArtikelReportDto dto, DateTime deaktiviertAm)
        {

            PosTyp              = "Deaktiviert";
            Position            = "Deaktiviert am";
            Nummer              = deaktiviertAm.ToShortDateString();
        }


        public BestandReportRow(ArtikelReportDto dto, bool istTotal)
        {

            PosTyp              = "Artikel gesamt";
            Herstellernummer    = "Artikel gesamt";
            Menge               = dto.Lager.Sum(p => p.Menge);
            Gesamtpreis         = dto.Lager.Sum(p => p.Gesamtpreis);
            if (Menge > 0) Mischpreis = Gesamtpreis / Menge;
        }


        public BestandReportRow(ArtikelReportDto dto, NummerFilterModel ihdto)
        {

            PosTyp              = "Asset";
            Position            = "Asset";
            Nummer              = ihdto.Nummer;
            Name                = ihdto.Name;
        }


        public BestandReportRow(ArtikelReportDto dto, LagerortReportDto lodto)
        {

            PosTyp              = "Lagerort";
            Herstellernummer    = lodto.Nummer;
            Menge               = lodto.Menge;
            Mischpreis          = lodto.Mischpreis;
            Gesamtpreis         = lodto.Gesamtpreis;
        }
    }
}
