﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class IntAuStelleViewModel : ViewModel<IIntAuStelleView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public IntAuStelleViewModel(IFilterService store, IIntAuStelleView view) : base(view)
        {
            this.store = store;
        }


        public void Reset()
        {
            Auftragsstelle = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (Auftragsstelle.HasValue) return String.Format("Interne Auftragsstelle {0}", Auftragsstelle);
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Auftragsstelle.HasValue) filter.Add("Interne Auftragsstelle", Auftragsstelle);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            
            RaisePropertyChanged("Auftragsstellen");
            return Task.FromResult(true);     
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        public IList<int> Auftragsstellen
        { 
            get { return store == null? null: store.InterneAuftragsstellen; }
        } 
   

        private int? auftragsstelle;
        public int? Auftragsstelle
        { 
            get { return auftragsstelle; }
            set { SetProperty(ref auftragsstelle, value); }
        } 
   

#endregion

    }
}
