﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Emma;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Emma;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.ViewModels.Emma;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Verwaltung;
using IMessageService = Comain.Client.Services.IMessageService;


namespace Comain.Client.Controllers.Emma
{

    public class EmmaController : ControllerBase, IEmmaController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFileDialogService dialogs;
        private readonly IEmmaService store;
        private readonly IModulService modules;
        private readonly MainViewModel mainView;
        private readonly EmmaViewModel view;
        private readonly EmmaDetailViewModel detailView;
        private readonly EmmaListViewModel listView;
        private readonly EmmaProtocolViewModel protocolView;
        private readonly EmmaFilterViewModel filterView;
        private EmmaSession session;
        private SerializableCommand ihOpenCommand;
        private SerializableCommand ihAddCommand;
        private SerializableCommand auOpenCommand;
        private SerializableCommand auAddCommand;
        private SerializableCommand psOpenCommand;
        private SerializableCommand psAddCommand;
        private SerializableCommand closeCommand;


        public EmmaController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IControllerBus bus, IFileDialogService dialogs,
                              IEmmaService store, IModulService modules,
                              MainViewModel mainView, EmmaViewModel view, EmmaDetailViewModel detailView, EmmaListViewModel listView,
                              EmmaProtocolViewModel protocolView, EmmaFilterViewModel filterView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.mainView = mainView;
            this.login = login;
            this.dialogs = dialogs;
            this.store = store;
            this.view = view;
            this.detailView = detailView;
            this.listView = listView;
            this.protocolView = protocolView;
            this.filterView = filterView;
            this.modules = modules;
        }


        public void Initialize()
        {

            view.FilterView                 = filterView.View;
            view.DetailView                 = detailView.View;
            view.ListView                   = listView.View;
            view.ProtokollView              = protocolView.View;

            view.BackCommand                = new AsyncDelegateCommand(_ => bus.BackAsync());
            view.ImportCommand              = CreateCommand(_ => ImportAsync());
            view.CloseCommand               = closeCommand = CreateCommand(_ => SaveAndCloseAsync());
            view.SaveCommand                = CreateCommand(_ => SaveAsync());
            view.AutoSaveCommand            = CreateCommand(_ => AutoSaveAsync());
            view.ReloadCommand              = CreateCommand(_ => LoadAsync());
            filterView.SearchCommand        = CreateCommand(_ => SearchAsync());
            detailView.IHObjektOpenCommand  = ihOpenCommand = CreateCommand(_ => IHObjektOpenAsync(), _ => detailView.Current?.IHObjekt != null);
            detailView.IHObjektAddCommand   = ihAddCommand  = CreateCommand(_ => IHObjektAddAsync(), _ => detailView.Current != null && detailView.Current.IstBearbeitbar);
            detailView.AuftragOpenCommand   = auOpenCommand = CreateCommand(_ => AuftragOpenAsync(), _ => detailView.Current?.Auftrag != null);
            detailView.AuftragAddCommand    = auAddCommand  = CreateCommand(_ => AuftragAddAsync(), _ => detailView.Current != null && detailView.Current.IstBearbeitbar);
            detailView.PersonalOpenCommand  = psOpenCommand = CreateCommand(_ => PersonalOpenAsync(), _ => detailView.Current?.Personal != null);
            detailView.PersonalAddCommand   = psAddCommand  = CreateCommand(_ => PersonalAddAsync(), _ => detailView.Current != null && detailView.Current.IstBearbeitbar);
            view.SelectAllCommand           = new DelegateCommand(_ => listView.Foundlist?.Where(p => p.KannBestätigtWerden).ForEach(p => p.IstBestätigt = true));
            view.SelectEorCommand           = new DelegateCommand(_ => listView.Foundlist?.Where(p => p.KannBestätigtWerden).ForEach(p => p.IstBestätigt = !p.IstBestätigt));

            PropertyChangedEventManager.AddHandler(listView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.PageView = view.View;

            crumbs.Add("EMMA Import");
            view.Mandant = login.AktuelleVerbindung;
            await LoadAsync();
            await SearchAsync();
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(listView, ViewPropertyChanged, "");
        }


        private bool HasChanges => !listView.Foundlist.IsNullOrEmpty() && listView.Foundlist.IsChanged;


        private async Task<bool> ConfirmUnsavedAsync()
            => !HasChanges || await messages.ConfirmExitAsync();


        private void UpdateCommands()
        {
            context.Post(new SendOrPostCallback((o) => {

                ihOpenCommand.RaiseCanExecuteChanged();
                ihAddCommand.RaiseCanExecuteChanged();
                auOpenCommand.RaiseCanExecuteChanged();
                auAddCommand.RaiseCanExecuteChanged();
                psOpenCommand.RaiseCanExecuteChanged();
                psAddCommand.RaiseCanExecuteChanged();
                closeCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private async Task LoadAsync()
        {

            if (!await ConfirmUnsavedAsync()) return;

            detailView.Current = null;
            listView.Unload();
            view.ResetTabs();

            using (new VisualDelay(view)) {

                filterView.ResultText = null;
                session = await store.CreateSessionAsync(bus.Token).ConfigureAwait(false);
                await Task.WhenAll(detailView.LoadAsync(session),
                                   filterView.LoadAsync(session));
            }

            UpdateCommands();
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await ConfirmUnsavedAsync()) return false;

            view.Unload();
            detailView.Unload();
            listView.Unload();
            protocolView.Unload();
            filterView.Unload();
            session = null;

            return true;
        }


        private async Task SearchAsync()
        {

            view.ResetTabs();
            if (!await ConfirmUnsavedAsync()) return;

            using (new VisualDelay(view)) {

                await listView.LoadAsync(null);
                var models = await store.SearchAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                await listView.LoadAsync(models);
                filterView.SetResultStatus(models);
            }

            UpdateCommands();
        }


        private async Task ImportAsync()
        {

            if (!await ConfirmUnsavedAsync()) return;

            var types = new List<FileType> { new FileType("CSV (Trennzeichen-getrennt)", ".csv"),
                                             new FileType("Alle Dateien (*.*)", ".*") };
            var res = dialogs.ShowOpenFileDialog(types, types[0], "PB_Zeiten.csv");
            if (!res.IsValid) return;

            using (new VisualDelay(view)) {

                await listView.LoadAsync(null);
                var models = await store.ImportAsync(res.FileName, bus.Token).ConfigureAwait(false);
                if (models.IsNullOrEmpty()) throw new ComainOperationException("Es wurden keine importierbaren Zeilen gefunden. Wurde die richtige Datei verwendet?");

                await listView.LoadAsync(models);
                filterView.SetResultStatus(models);
            }

            UpdateCommands();
        }


        private async Task SaveAsync()
        {

            if (!HasChanges) return;

            using (new VisualDelay(view)) {

                await store.SaveAsync(listView.Foundlist, bus.Token).ConfigureAwait(false);
                listView.Foundlist.AcceptChanges();
            }

            UpdateCommands();
        }


        private async Task AutoSaveAsync()
        {

            if (listView.Foundlist.IsNullOrEmpty()) return;
            int amt = listView.Foundlist.Count(p => !p.Personalleistung_Id.HasValue);
            if (amt == 0) return;

            if (!await messages.ConfirmAsync(new MessageToken { Header = "Sollen die Personalleistungen wirklich automatisch erstellt werden?",
                                                                Reasons = new List<String> { "• Es wird versucht fehlende Aufträge und Personal selbstständig anzulegen.",
                                                                                             "• Leistungen die fehlenden Assets zugeordnet sind können nicht erstellt werden.",
                                                                                             "• Anzahl noch nicht erstellter Personalleistungen: " + amt.ToString() }})) return;

            using (new VisualDelay(view)) {

                var models = await store.AutoSaveAsync(listView.Foundlist, bus.Token).ConfigureAwait(false);
                await listView.LoadAsync(models);
            }

            UpdateCommands();
        }


        private async Task SaveAndCloseAsync()
        {

            if (listView.Foundlist.IsNullOrEmpty()) return;
            int amt = listView.Foundlist.Count(p => p.IstBestätigt && p.IstBearbeitbar);
            if (amt == 0) return;

            using (new VisualDelay(view)) {

                var von = listView.Foundlist.Select(p => p.Datum).First();
                var bis = listView.Foundlist.Select(p => p.Datum).Last();
                if (bis > von.Date.MondayOf().AddDays(6)) throw new ComainOperationException("Es darf nur eine Woche abgeschlossen werden. Die Liste der Leistungen enthält mehr als eine Woche.");
                var info = await store.CloseInfoAsync(von, bus.Token).ConfigureAwait(false);

                if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Wochenbericht abgeschlossen werden?",
                                                                    Reasons = info.Explanations(listView.Foundlist) })) return;

                var models = await store.SaveAndCloseAsync(listView.Foundlist, bus.Token).ConfigureAwait(false);
                await listView.LoadAsync(models);
            }

            UpdateCommands();
        }


        private async Task IHObjektOpenAsync()
        {

            if (detailView.Current?.IHObjekt == null || detailView.Current?.IHObjekt.Id == 0) return;
            await bus.CallAsync(new ControllerMessage { Destination = typeof(IIHObjektController), Entity_Id = detailView.Current.IHObjekt.Id,
                                                        Source = this, EditMode = EditMode.ReadOnly });
        }


        private async Task IHObjektAddAsync()
        {

            if (detailView.Current == null) return;

            var ih = new IHObjekt { Inventarnummer = detailView.Current.EmmaInventarnummer, Name = detailView.Current.EmmaMaschine };
            var msg = await bus.CallAsync(new ControllerMessage { Destination = typeof(IIHObjektController), Source = this, EditMode = EditMode.Append, Parameter = ih });
            detailView.AddAndAssignIHObjekt(msg.Result as IHObjekt);
        }


        private async Task AuftragOpenAsync()
        {

            if (!modules.Info.VerwendetModulAuftrag) return;
            if (detailView.Current?.Auftrag == null || detailView.Current?.Auftrag.Id == 0) return;
            await bus.CallAsync(new ControllerMessage { Destination = typeof(IAuftragController), Entity_Id = detailView.Current.Auftrag.Id,
                                                        Source = this, EditMode = EditMode.ReadOnly });
        }


        private async Task AuftragAddAsync()
        {

            if (detailView.Current == null) return;
            if (!modules.Info.VerwendetModulAuftrag) return;
            var au = new Auftrag { ExterneNummer = detailView.Current.EmmaAuftragsnummer, Kurzbeschreibung = detailView.Current.EmmaAktion,
                                   AuIHKS = detailView.IHObjekt == null? null: new AuIHKSDto { IHObjekt_Id = detailView.IHObjekt.Id } };
            var msg = await bus.CallAsync(new ControllerMessage { Destination = typeof(IAuftragController), Source = this, EditMode = EditMode.Append, Parameter = au });
            detailView.AddAndAssignAuftrag(msg.Result as Auftrag);
        }


        private async Task PersonalOpenAsync()
        {

            if (detailView.Current?.Personal == null || detailView.Current?.Personal.Id == 0) return;
            await bus.CallAsync(new ControllerMessage { Destination = typeof(IVerwaltungController), Entity_Id = detailView.Current.Personal.Id,
                                                          Source = this, EditMode = EditMode.ReadOnly, Parameter = detailView.Current.Personal });
        }


        private async Task PersonalAddAsync()
        {
            if (detailView.Current == null) return;

            var ps = new Personal { Nummer = detailView.Current.EmmaPersonalnummer, Nachname = detailView.Current.EmmaNachname, Vorname = detailView.Current.EmmaVorname, Stundensatz = 0 };
            var msg = await bus.CallAsync(new ControllerMessage { Destination = typeof(IVerwaltungController), Source = this, EditMode = EditMode.Append, Parameter = ps });
            detailView.AddAndAssignPersonal(msg.Result as Personal);
        }


        private Task ProtocolAsync() => Task.FromResult(true);


        private void TesteZuordnung(Action<Leistung> setter)
        {

            listView.Foundlist?.ForEach(setter);
            UpdateCommands();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (args.PropertyName == nameof(listView.Current) && sender == listView) {

                    detailView.Current = listView.Current;
                    UpdateCommands();
                }

                if (args.PropertyName == nameof(detailView.IHObjekt) && sender == detailView) TesteZuordnung(p => p.TesteIHObjektZuordnung(detailView.Current));
                if (args.PropertyName == nameof(detailView.Auftrag)  && sender == detailView) TesteZuordnung(p => p.TesteAuftragZuordnung(detailView.Current));
                if (args.PropertyName == nameof(detailView.Personal) && sender == detailView) TesteZuordnung(p => p.TestePersonalZuordnung(detailView.Current));
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }
    }
}
