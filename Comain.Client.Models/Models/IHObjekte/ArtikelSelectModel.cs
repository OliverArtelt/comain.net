﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Models.IHObjekte
{
    public class ArtikelSelectModel : Entity, IDataErrorInfo
    {

        public int          ArtikelId           { get; set; }
        public String       Nummer              { get; set; }
        public String       Name                { get; set; }
        public String       Warengruppe         { get; set; }
        public String       IHObjekte           { get; set; }
        public String       Lieferanten         { get; set; }
        public decimal?     Mindestbestand      { get; set; }
        public decimal?     Mindestbestellmenge { get; set; }
        public decimal?     Bestellmenge        { get; set; }
        public decimal?     BestandReal         { get; set; }
        public decimal?     Mischpreis          { get; set; }
        public DateTime?    DeaktiviertAm       { get; set; }


        public ArtikelSelectModel() {}


        /// <summary>
        /// nachträglich angefügt
        /// </summary>
        public ArtikelSelectModel(MaterialstammFilterModel fmodel)
        {

            ArtikelId      = fmodel.Id;
            Nummer         = fmodel.Nummer;
            Name           = fmodel.Name;
            Warengruppe    = fmodel.Warengruppe;
            Mischpreis     = fmodel.Mischpreis;
            DeaktiviertAm  = fmodel.DeaktiviertAm;
        }


        public string this[string columnName] => null;

        public string Error => null;
    }
}
