﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Tickets
{
    public class MeineTicketAnzahl
    {

        public int Bearbeiten       { get; set; }
        public int Zuweisen         { get; set; }
        public int Abnehmen         { get; set; }
        public int Abschliessen     { get; set; }


        public int Summe { get { return Bearbeiten + Zuweisen + Abnehmen + Abschliessen; } }


        public String Wert
        {
            
            get {

                if (Zuweisen > 0 && Abschliessen > 0) return $"{Zuweisen} + {Abschliessen}";
                if (Zuweisen > 0) return Zuweisen.ToString();
                if (Abschliessen > 0) return Abschliessen.ToString();
                if (Abnehmen > 0) return Abnehmen.ToString();
                if (Bearbeiten > 0) return Bearbeiten.ToString();
                return null;
            }
        }


        public String Text
        {
            
            get {

                if (Zuweisen > 0 && Abschliessen > 0) return "Tickets müssen zugewiesen bzw. abgeschlossen werden";

                if (Zuweisen > 1) return "Tickets müssen zugewiesen werden";
                if (Zuweisen > 0) return "Ticket muss zugewiesen werden";

                if (Abschliessen > 1) return "Tickets müssen abgeschlossen werden";
                if (Abschliessen > 0) return "Ticket muss abgeschlossen werden";

                if (Abnehmen > 1) return "Tickets müssen abgenommen werden";
                if (Abnehmen > 0) return "Ticket muss abgenommen werden";

                if (Bearbeiten > 1) return "Tickets müssen bearbeitet werden";
                if (Bearbeiten > 0) return "Ticket muss bearbeitet werden";

                return null;
            }
        }
    }
}
