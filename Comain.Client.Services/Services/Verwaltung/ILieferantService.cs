﻿using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Services.Verwaltung
{

    public interface ILieferantService : ISimpleService<LieferantDto, Lieferant>
    {
        Task CombineAsync(int src, int dst, CancellationToken ct = default);
    }
}
