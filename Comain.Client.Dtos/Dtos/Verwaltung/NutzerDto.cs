﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class NutzerDto
    {

        public String           Id                  { get; set; }
        public String           Name                { get; set; }
        public String           NeuesPasswort       { get; set; }
        public String           Email               { get; set; }
        public DateTime?        LetzteAnmeldung     { get; set; }
        public int              Status              { get; set; }
        public IList<String>    Rollen              { get; set; }
        public String           Name1               { get; set; }
        public String           Name2               { get; set; }
        public String           Beschreibung        { get; set; }
        public int?             Personal_Id         { get; set; }
        public bool             UseStandortFilter   { get; set; }
        public String           StandortFilter      { get; set; }
    }
}
