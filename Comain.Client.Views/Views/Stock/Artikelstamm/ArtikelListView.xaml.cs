﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comain.Client.ViewModels.Stock.Artikelstamm;

namespace Comain.Client.Views.Stock.Artikelstamm
{
    /// <summary>
    /// Interaction logic for ArtikelListView.xaml
    /// </summary>
    public partial class ArtikelListView:UserControl ,IArtikelListView
    {
        public ArtikelListView()
        {
        InitializeComponent();
        }
 

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as ArtikelListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
