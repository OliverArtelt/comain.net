﻿using System;
using System.Collections.Generic;
using System.Threading;
using Comain.Client.Models.WPlan;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.Models;
using Comain.Client.Views.WPlan.Vorgaben;
using Comain.Client.ViewData.WPlan;
using System.Windows.Input;
using System.Waf.Applications;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailAuftragViewModel : ViewModel<IVorgabeDetailAuftragView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
 

        public VorgabeDetailAuftragViewModel(IVorgabeDetailAuftragView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }
                                                

        public void Unload()
        {
            Ereignisse = null;
        }


        public async Task LoadAsync(IList<EreignisViewItem> ereignisse)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Ereignisse = ereignisse;

            }), null);
        }


#region P R O P E R T I E S

   
        private IList<EreignisViewItem> ereignisse;
        public IList<EreignisViewItem> Ereignisse
        { 
            get { return ereignisse; }
            set { SetProperty(ref ereignisse, value); }
        }

   
        private EreignisViewItem ereignis;
        public EreignisViewItem Ereignis
        { 
            get { return ereignis; }
            set { SetProperty(ref ereignis, value); }
        }

   
        private ICommand openAuftragCommand;
        public ICommand OpenAuftragCommand
        { 
            get { return openAuftragCommand; }
            set { SetProperty(ref openAuftragCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

      
#endregion

    }
}
