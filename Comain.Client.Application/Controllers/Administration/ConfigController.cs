﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{
     
    public interface IConfigController<TView> : IController where TView : IConfigViewModel 
    {}


    public class ConfigController<TView> : ControllerBase, IConfigController<TView> where TView : IConfigViewModel
    {

        protected readonly IKonfigService store;
        protected readonly AdminViewModel view;
        private readonly TView configView;
        protected Konfiguration current;


        public ConfigController(IKonfigService store, IMessageService messages, IControllerBus bus,
                                AdminViewModel view, TView configView)
          : base(messages, bus)
        {
          
            this.store = store;
            this.view = view;
            this.configView = configView;
        }


        public virtual void Initialize()
        {}


        public virtual async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            configView.SaveCommand = CreateCommand(_ => SaveAsync());
            view.DetailView = configView.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                current = await store.GetAsync(bus.Token);
                configView.Current = current;
            }
        }


        private async Task SaveAsync()
        {

            if (current == null) return;

            using (new VisualDelay(configView)) {

                current = await store.SetAsync(current, bus.Token);
                configView.Current = current;
            }
        }


        protected override Task<bool> UnloadImplAsync()
        {

            configView.Unload();
            return Task.FromResult(true);    
        }


        public virtual void Shutdown()
        {}
    }
}
