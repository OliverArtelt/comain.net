﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Converters
{

    public class AuftragConverter : ITypeConverter<AuftragDetailDto, Auftrag>
    {

        public Auftrag Convert(AuftragDetailDto src, Auftrag dst, ResolutionContext context)
        {

            if (dst == null) dst = new Auftrag();
            if (src == null) return dst;
            if (src.Id <= 0) dst.Id = src.Id;

            dst.Id                       = src.Id;
            dst.ErstelltAm               = src.ErstelltAm.RoundToSeconds();
            dst.GeändertAm               = src.GeändertAm.RoundToSeconds();
            dst.Nummer                   = src.Nummer;
            dst.ExterneNummer            = src.ExterneNummer;
            dst.FBNummer                 = src.FBNummer;
            dst.UnveränderlicherTermin   = src.UnveränderlicherTermin;
            dst.ImFremdbudget            = src.ImFremdbudget;
            dst.InterneAuStelle          = src.InterneAuStelle;
            dst.Titel                    = src.Titel;
            dst.Kurzbeschreibung         = src.Kurzbeschreibung;
            dst.Aktivitätenbeschreibung  = src.Aktivitätenbeschreibung;
            dst.Typ                      = (Auftragstyp)src.Typ;
            dst.Status                   = (Auftragstatus)src.Status;
            dst.Fertigstellungstermin    = src.Fertigstellungstermin;
            dst.GemeldetDatum            = src.GemeldetAm.HasValue? src.GemeldetAm.Value.Date: (DateTime?)null;
            dst.ÜbergabeDatum            = src.ÜbergabeAm.HasValue? src.ÜbergabeAm.Value.Date: (DateTime?)null;
            dst.GemeldetZeit             = src.GemeldetAm.HasValue? src.GemeldetAm.Value.TimeOfDay.RoundToSeconds() : (TimeSpan?)null;
            dst.ÜbergabeZeit             = src.ÜbergabeAm.HasValue? src.ÜbergabeAm.Value.TimeOfDay.RoundToSeconds() : (TimeSpan?)null;
            dst.Störzeit                 = src.Störzeit;
            dst.Beauftragungsdatum       = src.Beauftragungsdatum;
            dst.Auftragsleiterdatum      = src.Auftragsleiterdatum;
            dst.Abnahmedatum             = src.Abnahmedatum;
            dst.Menge                    = src.Menge;
            dst.Wert                     = src.Wert;
            dst.Stunden                  = src.Stunden;
            dst.AuIHKS                   = src.AuIHKS;
            dst.Leistungsart_Id          = src.Leistungsart_Id;
            dst.Dringlichkeit_Id         = src.Dringlichkeit_Id;
            if (src.Leistungsart != null) dst.Leistungsart = new Leistungsart { Id = src.Leistungsart.Id, IstPlankosten = src.Leistungsart.IstPlankosten, IstPräventation = src.Leistungsart.IstPräventation,
                                                                                IstProdKennziffer = src.Leistungsart.IstProdKennziffer, IstSachkundigenprüfung = src.Leistungsart.IstSachkundigenprüfung,
                                                                                IstUnPlankosten = src.Leistungsart.IstUnPlankosten, IstWI = src.Leistungsart.IstWI, Name = src.Leistungsart.Name,
                                                                                Nummer = src.Leistungsart.Nummer };
            dst.Schadensbild             = src.Schadensbild;
            dst.Schadensursache          = src.Schadensursache;
            dst.SubSchadensbild          = src.SubSchadensbild;
            dst.SubSchadensursache       = src.SubSchadensursache;
            dst.Gewerk                   = src.Gewerk;
            dst.Auftraggeber             = src.Auftraggeber;
            dst.Auftragsleiter           = src.Auftragsleiter;
            dst.Abnahmeberechtigter      = src.Abnahmeberechtigter;
            dst.Auftragnehmer            = src.Auftragnehmer;

            dst.Personalleistungen       = context.Mapper.Map<List<Personalleistung>>(src.Personalleistungen);
            dst.Materialleistungen       = context.Mapper.Map<List<Materialleistung>>(src.Materialleistungen);

            dst.AcceptChanges();

            return dst;
        }
    }
}
