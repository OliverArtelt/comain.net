﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.WPlan;
using Comain.Client.Services.WPlan;
using Comain.Client.ViewData.WPlan;
using Comain.Client.ViewModels.WPlan.Aufträge;
using MahApps.Metro.Controls;


namespace Comain.Client.Controllers.WPlan
{

    public interface IChecklistController : IController
    {}

    /// <summary>
    /// Checkliste des Auftrages für die Erledigung anzeigen (Wartungsplan)
    /// </summary>
    public class ChecklistController : ControllerBase, IChecklistController, IChecklistPluginController
    {

        private readonly SynchronizationContext context;
        private readonly IFlyoutService flyouts;
        private readonly IMessageService messages;
        private readonly IControllerBus bus;
        private readonly IChecklistService store;
        private readonly ChecklistViewModel view;
        private readonly ChecklistDetailViewModel detailView;
        private readonly ConfirmViewModel confirmView;
        private readonly PersonalSelektorViewModel personalView;
        private SerializableCommand detailCommand;
        private EditMode currentMode;

        private TrackableCollection<Checklistposition> models;
        private Auftrag auftrag;


        public ChecklistController(IMessageService messages, IFlyoutService flyouts, IControllerBus bus,
                                   IChecklistService store,
                                   ChecklistViewModel view, ChecklistDetailViewModel detailView, ConfirmViewModel confirmView,
                                   PersonalSelektorViewModel personalView)
          :  base(messages, bus)
        {

            context = SynchronizationContext.Current;

            this.messages = messages;
            this.flyouts = flyouts;
            this.bus = bus;
            this.store = store;
            this.view = view;
            this.detailView = detailView;
            this.confirmView = confirmView;
            this.personalView = personalView;
        }


        public void Initialize()
        {

            view.DetailCommand = detailCommand = CreateCommand(_ => OpenPositionDetailAsync(), _ => view.Current != null);
            view.EditPersonalCommand = CreateCommand(_ => EditPersonalAsync());

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override Task RunAsync(object parameter)
        {
            return base.RunAsync(parameter);
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        public ICommand OpenDetailCommand => detailCommand;


        public void SetEditMode(EditMode newMode)
        {

            currentMode = newMode;
            view.EditEnabled = newMode == EditMode.Edit;
        }


        public bool HasChanges => !models.IsNullOrEmpty() && models.Any(p => p.IsChanged);

        public bool HatAuftrag => auftrag != null && auftrag.Typ == Auftragstyp.Wartungsplan;


        public List<String> ErrorList
        {
            get {

                if (!HasChanges) return new List<String>();
                return models.SelectMany(p => p.Errorlist).Where(p => !String.IsNullOrEmpty(p)).Distinct().ToList();
            }
        }


        public void Unload()
        {

            view.Unload();
            detailView.Unload();
            confirmView.Unload();
            personalView.Unload();
        }


        public Task<bool> UnloadAsync() => Task.FromResult(true);


        public object ChecklistView => view?.View;


        public async Task LoadAsync()
        {

            var pstask = store.PersonalAsync(bus.Token);
            var gwtask = store.GewerkeAsync(bus.Token);
            await Task.WhenAll(pstask, gwtask).ConfigureAwait(false);

            bus.Token.ThrowIfCancellationRequested();

            var pvtask = personalView.SetListAsync(pstask.Result, gwtask.Result);
            var dvtask = detailView.SetListAsync(pstask.Result, gwtask.Result);
            var cvtask = confirmView.SetListAsync(pstask.Result);
            await Task.WhenAll(pvtask, dvtask, cvtask).ConfigureAwait(false);
        }


        public async Task LoadAuftragAsync(Auftrag au)
        {

            if (au == null) {

                Unload();
                return;
            }


            using (new VisualDelay(view)) {

                auftrag = au;
                if (!HatAuftrag) return;
                models = await store.ChecklisteAsync(auftrag.Id, bus.Token).ConfigureAwait(false);

                var viewableModels = models.Where(p => p.IstErledigt || auftrag.IstOffen && !p.MassnahmeIstGesperrt).ToList();
                view.SetzePositionenliste(viewableModels);
            }
        }


        private async Task OpenPositionDetailAsync()
        {

            if (view.Current == null) return;
            detailView.Current = view.Current;
            detailView.EditEnabled = currentMode != EditMode.ReadOnly && view.Current != null && view.Current.IstBearbeitbar;

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = detailView.View, Position = Position.Right, Header = "Position bearbeiten" });


            using (new VisualDelay(detailView)) {

                detailView.Medien = null;
                var medien  = await store.MedienAsync(view.Current.Model.Massnahme_Id, bus.Token).ConfigureAwait(false);

                await context.SendAsync(new SendOrPostCallback((o) => { detailView.Medien = medien; }), null);
            }
        }


        public async Task<bool> ConfirmSaveAsync()
        {

            if (!HatAuftrag) return true;
            confirmView.SetzeCheckliste(ZuErledigendePositionen, auftrag);
            if (!confirmView.ConfirmNotwendig) return true;

            confirmView.SaveCommand = flyouts.YesCommand;
            confirmView.CancelCommand = flyouts.NoCommand;
            bool saveConfirmed = await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = confirmView.View, Position = Position.Bottom });
            return saveConfirmed;
        }


        public async Task SaveAsync()
        {

            if (!HatAuftrag || !HasChanges) return;

            var result = confirmView.GebeErgebnis();
            result.Erledigte = ZuErledigendePositionen;
            await store.SaveAsync(result, bus.Token).ConfigureAwait(false);
        }


        public async Task<IEnumerable> LEPositionAsync(int leid)
        {

            var result = await store.LEPositionAsync(leid, bus.Token).ConfigureAwait(false);
            return result;
        }


        public async Task<IEnumerable> MTPositionAsync(int mtid)
        {

            var result = await store.MTPositionAsync(mtid, bus.Token).ConfigureAwait(false);
            return result;
        }


        public async Task<ReportResult> ReportAsync(Suchkriterien criteria)
        {

            if (!HatAuftrag) return null;

            criteria = new Suchkriterien();
            criteria.Add("Auftrag", auftrag.Id);
            criteria.Add("ZeigeWerkzeug", true);
            criteria.Add("ZeigeDurchführung", true);
            criteria.Add("ZeigeSicherheit", true);
            criteria.Add("ZeigeBilder", true);

            var result = new ReportResult { AssemblyAddress = "Comain.Client.Reports", HasLogo = true, ReportAddress = "Comain.Client.Reports.Wartungsplan.rdlc",
                                            Title = $"Wartungsplan {auftrag.Nummer}" };
            var dto = await store.ReportAsync(criteria, bus.Token).ConfigureAwait(false);

            bus.Token.ThrowIfCancellationRequested();

            int count = dto.Positionen.Count();
            var maindata = dto.Positionen.Select((p, i) => new AuftragReportItem(dto, p, i, count)).ToList();
            var bilddata = dto.Positionen.SelectMany(p => p.Bilder.Select(q => new BildReportItem(p, q))).ToList();
            result.AddDataSource("MainData", maindata);
            result.AddDataSource("BildData", bilddata);
            return result;
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (sender == view && args.PropertyName == nameof(view.Current)) UpdateCommands();
        }


        private void UpdateCommands()
        {

            context.Send(new SendOrPostCallback((o) => {

                detailCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private async Task EditPersonalAsync()
        {

            await personalView.SetzeModelAsync(view.Current);
            await flyouts.OpenAsync(new FlyoutConfig { ChildView = personalView.View, Position = Position.Right });
        }


        private List<Checklistposition> ZuErledigendePositionen
        {
            get {

                if (models.IsNullOrEmpty()) return null;
                return models.Where(p => !p.InVorigerSitzungErledigt).ToList();
            }
        }
    }
}
