﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models
{

    [Flags]
    public enum Weekday
    {

        Undefiniert =  0,
        Montag      =  1,
        Dienstag    =  2,
        Mittwoch    =  4,
        Donnerstag  =  8,
        Freitag     = 16,
        Samstag     = 32,
        Sonntag     = 64,

        Werktage    = 31,
        Komplett    = 127
    }


    public static class WeekdayExtensions
    {

        public static String ToShortString(this Weekday week)
        {

            if (week == Weekday.Undefiniert) return null;

            var bld = new StringBuilder();
            if (week.HasFlag(Weekday.Montag))     bld.Append("MO, ");
            if (week.HasFlag(Weekday.Dienstag))   bld.Append("DI, ");
            if (week.HasFlag(Weekday.Mittwoch))   bld.Append("MI, ");
            if (week.HasFlag(Weekday.Donnerstag)) bld.Append("DO, ");
            if (week.HasFlag(Weekday.Freitag))    bld.Append("FR, ");
            if (week.HasFlag(Weekday.Samstag))    bld.Append("SA, ");
            if (week.HasFlag(Weekday.Sonntag))    bld.Append("SO, ");
            bld.Length -= 2;
            return bld.ToString();
        }


        public static String ToShortString(this Weekday? week)
        {

            if (week.GetValueOrDefault() == Weekday.Undefiniert) return null;
            return week.Value.ToShortString();
        }


        /// <summary>
        /// einzelnen Wochentag an- oder abwählen
        /// </summary>
        public static Weekday Flip(this Weekday week, Weekday member, bool set)
        {

            if (set) week |= member;
            else week &= ~member;

            return week;
        }


        public static int NumberOfDays(this Weekday? week)
        {

            if (!week.HasValue) return 0;
            return week.Value.NumberOfDays();
        }


        public static int NumberOfDays(this Weekday week)
        {

            int i = (int)week;
            i = i - ((i >> 1) & 0x55555555);
            i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
            return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
        }
    }
}
