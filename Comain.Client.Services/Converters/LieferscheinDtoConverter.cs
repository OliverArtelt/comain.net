﻿using AutoMapper;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;


namespace Comain.Client.Converters.Stock
{

    public class LieferscheinDtoConverter : ITypeConverter<LieferscheinDto, Lieferschein>
    {

        public Lieferschein Convert(LieferscheinDto src, Lieferschein dst, ResolutionContext context)
        {

            if (dst == null) dst = new Lieferschein();
            if (src == null) return dst;
            if (src.Id <= 0) dst.Id = src.Id;  

            dst.Id       = src.Id;         
            dst.Nummer   = src.Nummer;         
            dst.Notizen  = src.Notizen;          
            dst.Datum    = src.Datum;        
                        
            dst.Eingänge = new TrackableCollection<Wareneingang>();
            if (!src.Eingänge.IsNullOrEmpty()) {
            
                foreach (var srcWe in src.Eingänge) {

                    var dstWe = new Wareneingang();
                    dst.Eingänge.Add(dstWe);

                    dstWe.Id              = srcWe.Id;
                    dstWe.Position        = srcWe.Position;
                    dstWe.Liefermenge     = srcWe.Liefermenge;
                    dstWe.MengeOK         = srcWe.MengeOK;
                    dstWe.Notizen         = srcWe.Notizen;
                    dstWe.MengeGeprüft    = srcWe.MengeGeprüft;
                    dstWe.QualitätGeprüft = srcWe.QualitätGeprüft;
                    dstWe.Lieferschein    = dst;
                  
                    if (srcWe.Material != null) {
                    
                        dstWe.Material = new Artikel();
                         
                        dstWe.Material.Id               = srcWe.Material.Id            ;
                        dstWe.Material.Name             = srcWe.Material.Name            ;
                        dstWe.Material.Nummer           = srcWe.Material.Nummer          ;
                        dstWe.Material.Beschreibung     = srcWe.Material.Beschreibung    ;
                        dstWe.Material.Herstellernummer = srcWe.Material.Herstellernummer;
                        dstWe.Material.Mischpreis       = srcWe.Material.Mischpreis      ;
                        dstWe.Material.Klasse           = (Materialklasse?)srcWe.Material.Klasse;
                        dstWe.Material.Mindestbestand   = srcWe.Material.Mindestbestand  ;
                        dstWe.Material.Bestellmenge     = srcWe.Material.Bestellmenge    ;
                        dstWe.Material.ReservierteMenge = srcWe.Material.ReservierteMenge;
                        dstWe.Material.BestandReal      = srcWe.Material.BestandReal     ;
                        dstWe.Material.BestandVirtuell  = srcWe.Material.BestandVirtuell ;
                    }
                   
                    if (srcWe.Bestellposition != null) {
                    
                        dstWe.Bestellposition = new Bestellposition();
                         
                        dstWe.Bestellposition.Id                = srcWe.Bestellposition.PositionId;
                        dstWe.Bestellposition.Position          = srcWe.Bestellposition.Position;
                        dstWe.Bestellposition.Menge             = srcWe.Bestellposition.Menge;
                        dstWe.Bestellposition.Preis             = srcWe.Bestellposition.Preis;
                        dstWe.Bestellposition.LfArtikelnummer   = srcWe.Bestellposition.LfArtikelnummer;
                        dstWe.Bestellposition.Artikelname       = dstWe.Material.Name;
                        dstWe.Bestellposition.Artikelnummer     = dstWe.Material.Nummer;
                        dstWe.Bestellposition.Material_Id       = dstWe.Material.Id;
                                              
                        dstWe.Bestellposition.Bestellung = new Bestellung();
                        dstWe.Bestellposition.Bestellung.Id           = srcWe.Bestellposition.BestellungId;
                        dstWe.Bestellposition.Bestellung.Nummer       = srcWe.Bestellposition.Nummer;
                        dstWe.Bestellposition.Bestellung.Auftrag      = srcWe.Bestellposition.Auftrag;
                        dstWe.Bestellposition.Bestellung.Kostenstelle = srcWe.Bestellposition.Kostenstelle;
                        dstWe.Bestellposition.Bestellung.DatumAusgang = srcWe.Bestellposition.DatumAusgang;
                        dstWe.Bestellposition.Bestellung.Termin       = srcWe.Bestellposition.Termin;
                    }

                    dstWe.Einlagerungen = new TrackableCollection<Wareneinlagerung>();
                    if (!srcWe.Einlagerungen.IsNullOrEmpty()) {
            
                        foreach (var srcWl in srcWe.Einlagerungen) {

                            var dstWl = new Wareneinlagerung();
                            dstWe.Einlagerungen.Add(dstWl);

                            dstWl.Id              = srcWl.Id;
                            dstWl.Menge           = srcWl.Menge;
                            dstWl.Restmenge       = srcWl.Restmenge;
                            dstWl.Preis           = srcWl.Preis;
                            dstWl.Datum           = srcWl.Datum;
                            dstWl.Wareneingang    = dstWe;

                            if (dstWl.Lagerort != null) {
                    
                                dstWl.Lagerort = new Lagerort();
                               
                                dstWl.Lagerort.Halle       = srcWl.Lagerort.Halle;
                                dstWl.Lagerort.Regal       = srcWl.Lagerort.Regal;
                                dstWl.Lagerort.Fach        = srcWl.Lagerort.Fach;
                                dstWl.Lagerort.Nummer      = srcWl.Lagerort.Nummer;
                                dstWl.Lagerort.Bemerkungen = srcWl.Lagerort.Bemerkungen;
                            }
                        }
                    }

                    if (!srcWe.Reklampositionen.IsNullOrEmpty()) {

                        foreach (var srcRp in srcWe.Reklampositionen) {

                            var dstRp = new Reklamposition();
                            dstWe.Reklampositionen.Add(dstRp);

                            dstRp.Position           = srcRp.Position;          
                            dstRp.ArtikelId          = srcRp.ArtikelId;         
                            dstRp.EingangId          = srcRp.EingangId;         
                            dstRp.Menge              = srcRp.Menge;             
                            dstRp.Preis              = srcRp.Preis;             
                            dstRp.Notizen            = srcRp.Notizen;           
                            dstRp.LfArtikelnummer    = srcRp.LfArtikelnummer;   
                            dstRp.Artikelnummer      = srcRp.Artikelnummer;     
                            dstRp.Artikelbezeichnung = srcRp.Artikelbezeichnung;
                            dstRp.Bestellposition    = srcRp.Bestellposition;   
                        }
                    }
                }
            }
  
            return dst;   
        }
    }
}
