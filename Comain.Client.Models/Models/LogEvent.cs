﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Comain.Client.Models
{
    public class LogEvent
    {

        public long          Id             { get; set; }
        public String        Message        { get; set; }
        public DateTime      TimeStamp      { get; set; }
        public String        Level          { get; set; }
        public String        Exception      { get; set; }
        public String        Properties     { get; set; }
        public String        Username       { get; set; }
        public String        Category       { get; set; }
        public String        Source         { get; set; }
        public String        Event          { get; set; }


        public String Color
        {
            get {

                if (Category == "HTTP Request Queue") return "CornflowerBlue";
                if (Level == "Information") return "LimeGreen";
                if (Level == "Warning")     return "Gold";
                if (Level == "Error")       return "Coral";
                if (Level == "Fatal")       return "MediumVioletRed";
                return "White";
            }
        }
    }
}
