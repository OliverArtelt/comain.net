﻿using System;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views.Administration;


namespace Comain.Client.ViewModels.Administration
{

    public class ModulViewModel : ValidatableViewModel<IModulView, Konfiguration>, IConfigViewModel
    {

        public ModulViewModel(IModulView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {
            RaisePropertyChanged("Kunde");
        }


        public void Unload()
        {}


#region P R O P E R T I E S
   

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public String Kunde
        { 
            get { return Current == null? null: Current.Kunde; }
            set { 
                
                if (Current != null && Current.Kunde != value) {

                    Current.Kunde = value;
                    RaisePropertyChanged("Kunde");
                }
            }
        }


#endregion
    
    }
}
