﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Gateways;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Administration
{

    public class IHReactivateService : IIHReactivateService
    {

        private readonly IRepository store;


        public IHReactivateService(IRepository store)
        {
            this.store = store;
        }


        public async Task<IList<IHReactivateItem>> GetAsync(CancellationToken ct = default)
        {

            String url = "ih/reactivate";
            var dtolist = await store.GetAsync<IList<IHReactivateDto>>(url, ct);
            return dtolist.Select(p => new IHReactivateItem(p)).OrderBy(p => p.Nummer).ToList();
        }


        public Task SetAsync(IEnumerable<int> list, CancellationToken ct = default)
        {
            return store.PostAsync<IEnumerable<int>>("ih/reactivate", list, ct);
        }
    }
}
