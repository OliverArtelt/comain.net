﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.WPlan;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanFilterViewModel : ViewModel<IPlanFilterView>
    {

        private readonly SynchronizationContext context;
        private readonly PlanTreeViewModel treeView;
        private readonly ILoginService login;
        private readonly Dictionary<String, (int start, int length)> weekDic;


        private IList<PersonalFilterModel> pslist;


        public PlanFilterViewModel(PlanTreeViewModel treeView, IPlanFilterView view, ILoginService login, IPersonalSearchProvider searcher) : base(view)
        {

            this.login = login;
            this.treeView = treeView;

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();

            SelectDateCommand = new DelegateCommand(p => SetPeriod(p));

            weekDic = new Dictionary<String, (int start, int length)>();
            weekDic.Add("WM1", (-1, 1));
            weekDic.Add("WP0", ( 0, 1));
            weekDic.Add("WP1", ( 1, 1));
            weekDic.Add("WM2", (-2, 3));
            weekDic.Add("WP2", ( 0, 3));
            weekDic.Add("WM3", (-3, 4));
            weekDic.Add("WP3", ( 0, 4));
            weekDic.Add("WC3", (-1, 3));
            weekDic.Add("WC5", (-2, 5));

            PsSearchProvider = searcher;
        }


        public void Reset()
        {

            Von = null;
            Bis = null;
            Leistungsart = null;
            Gewerk = null;
            JahrP0 = true;
            NurRelevante = false;
        }


        public void ValidateAndThrow()
        {

            var result = new List<String>();

            if (!IstJahresplanung) {

                if (!Von.HasValue) result.Add("• Geben Sie das Startdatum der Planung an.");
                if (!Bis.HasValue) result.Add("• Geben Sie das Enddatum der Planung an.");
                if (Von.HasValue && Bis.HasValue && Von.Value > Bis.Value) result.Add("• Geben Sie einen plausiblen Zeitraum an.");
            }

            if (!result.IsNullOrEmpty()) throw new ComainValidationException("Die Planung kann nicht erstellt werden", result);
        }


        public async Task LoadAsync(IList<NummerFilterModel> gwlist, IList<LeistungsartFilterModel> lalist, IList<PersonalFilterModel> pslist)
        {

            this.pslist = pslist;

            await context.SendAsync(new SendOrPostCallback((o) => {

                Gewerke = gwlist;
                Leistungsarten = lalist;
                PsSearchProvider?.SetItems(pslist?.OrderBy(p => p.Name));

            }), null);
        }


        public void Unload()
        {

            Gewerke = null;
            Leistungsarten = null;
            PsSearchProvider?.ClearItems();
            pslist = null;
        }


        public WPlanCreateInfo GebeFilter()
        {

            var filter = new WPlanCreateInfo {

                IHList         = treeView.GewählteNummern(),
                StartDate      = Von,
                EndDate        = Bis,
                Leistungsart   = Leistungsart?.Id,
                Gewerk         = Gewerk?.Id,
                Personal       = Personal?.Id,
                Planungstyp    = Typ,
                NurRelevante   = NurRelevante,
                Planungsstatus = Planungsstatus.AlleMassnahmen
            };

            if (Typ == Planungstyp.Jahr) {

                filter.StartDate = new DateTime(Jahr,  1,  1);
                filter.EndDate   = new DateTime(Jahr, 12, 31);

            } else if (Typ == Planungstyp.JahrKw) {

                filter.StartDate = DateTimeExtended.FirstDateOfCalendarWeek(Jahr, 1);
                filter.EndDate   = DateTimeExtended.FirstDateOfCalendarWeek(Jahr + 1, 1).AddDays(-1);
            }

            return filter;
        }


        private void SetPeriod(object context)
        {

            if (context == null) return;
            String token = context.ToString();
            if (String.IsNullOrEmpty(token) || token.Length != 3) return;

            int iv;
            if (!Int32.TryParse(token.Substring(2), out iv)) return;
            char type = token[0];
            char sign = token[1];

            switch (type) {

                case 'Y':

                    switch (sign) {

                        case '0':

                            Von = new DateTime(DateTime.Today.Year - 1, 1, 1);
                            Bis = new DateTime(DateTime.Today.Year - 1, 12, 31);
                            break;

                        case '1':

                            Von = new DateTime(DateTime.Today.Year, 1, 1);
                            Bis = new DateTime(DateTime.Today.Year, 12, 31);
                            break;

                        case '2':

                            Von = DateTime.Today;
                            Bis = new DateTime(DateTime.Today.Year, 12, 31);
                            break;

                        case '3':

                            Von = new DateTime(DateTime.Today.Year + 1, 1, 1);
                            Bis = new DateTime(DateTime.Today.Year + 1, 12, 31);
                            break;
                    }

                    break;

                case 'Q':

                    var curQ = new DateTime(DateTime.Now.Year, (DateTime.Now.Month - 1) / 3 * 3 + 1, 1);
                    if (iv == 1 && sign == 'M') curQ = curQ.AddMonths(-3);
                    if (iv == 1 && sign == 'P') curQ = curQ.AddMonths(3);
                    Von = curQ;
                    Bis = curQ.AddMonths(3).AddDays(-1);
                    break;

                case 'M':

                    var curM = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    if (iv == 1 && sign == 'M') curM = curM.AddMonths(-1);
                    if (iv == 1 && sign == 'P') curM = curM.AddMonths(1);
                    Von = curM;
                    Bis = curM.AddMonths(1).AddDays(-1);
                    break;

                case 'W':

                    var curW = DateTime.Today.MondayOf().AddDays(7 * weekDic[token].start);
                    Von = curW;
                    Bis = curW.AddDays(7 * weekDic[token].length -1);
                    break;
            }
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        public String JahrM2Caption => (DateTime.Now.Year - 2).ToString();
        public String JahrM1Caption => (DateTime.Now.Year - 1).ToString();
        public String JahrP0Caption => DateTime.Now.Year.ToString();
        public String JahrP1Caption => (DateTime.Now.Year + 1).ToString();
        public String JahrP2Caption => (DateTime.Now.Year + 2).ToString();


        public bool JahrM2
        {
            get => Jahr == DateTime.Now.Year - 2;
            set => Jahr = DateTime.Now.Year - 2;
        }


        public bool JahrM1
        {
            get => Jahr == DateTime.Now.Year - 1;
            set => Jahr = DateTime.Now.Year - 1;
        }


        public bool JahrP2
        {
            get => Jahr == DateTime.Now.Year + 2;
            set => Jahr = DateTime.Now.Year + 2;
        }


        public bool JahrP1
        {
            get => Jahr == DateTime.Now.Year + 1;
            set => Jahr = DateTime.Now.Year + 1;
        }


        public bool JahrP0
        {
            get => Jahr == DateTime.Now.Year;
            set => Jahr = DateTime.Now.Year;
        }


        private int jahr;
        public int Jahr
        {
            get => jahr;
            set {

                if (jahr != value) {

                    jahr = value;
                    RaisePropertyChanged(nameof(Jahr));
                    RaisePropertyChanged(nameof(JahrM2));
                    RaisePropertyChanged(nameof(JahrM1));
                    RaisePropertyChanged(nameof(JahrP2));
                    RaisePropertyChanged(nameof(JahrP1));
                    RaisePropertyChanged(nameof(JahrP0));
                }
            }
        }


        public ICommand ResetCommand            { get; }
        public ICommand SelectDateCommand       { get; }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        private ICommand planCommand;
        public ICommand PlanCommand
        {
            get { return planCommand; }
            set { SetProperty(ref planCommand, value); }
        }


        private IList<NummerFilterModel> gewerke;
        public IList<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get { return personal; }
            set { SetProperty(ref personal, value); }
        }


        private IList<LeistungsartFilterModel> leistungsarten;
        public IList<LeistungsartFilterModel> Leistungsarten
        {
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }


        private LeistungsartFilterModel leistungsart;
        public LeistungsartFilterModel Leistungsart
        {
            get { return leistungsart; }
            set { SetProperty(ref leistungsart, value); }
        }


        private bool nurRelevante;
        public bool NurRelevante
        {
            get { return nurRelevante; }
            set { SetProperty(ref nurRelevante, value); }
        }


        private DateTime? von;
        public DateTime? Von
        {
            get { return von; }
            set { SetProperty(ref von, value); }
        }


        private DateTime? bis;
        public DateTime? Bis
        {
            get { return bis; }
            set { SetProperty(ref bis, value); }
        }


        private Planungstyp typ;
        public Planungstyp Typ
        {
            get => typ;
            set {

                if (typ != value) {

                    typ = value;
                    RaisePropertyChanged(nameof(Typ));
                    RaisePropertyChanged(nameof(Tagesplanung));
                    RaisePropertyChanged(nameof(Wochenplanung));
                    RaisePropertyChanged(nameof(Jahresplanung));
                    RaisePropertyChanged(nameof(JahresKwPlanung));
                    RaisePropertyChanged(nameof(IstJahresplanung));
                }
            }
        }


        public bool Tagesplanung
        {
            get => Typ == Planungstyp.Täglich;
            set => Typ = Planungstyp.Täglich;
        }


        public bool Wochenplanung
        {
            get => Typ == Planungstyp.Wöchentlich;
            set => Typ = Planungstyp.Wöchentlich;
        }


        public bool Jahresplanung
        {
            get => Typ == Planungstyp.Jahr;
            set => Typ = Planungstyp.Jahr;
        }


        public bool JahresKwPlanung
        {
            get => Typ == Planungstyp.JahrKw;
            set => Typ = Planungstyp.JahrKw;
        }


        public bool IstJahresplanung => Jahresplanung || JahresKwPlanung;


#endregion

    }
}
