﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Services;
using Comain.Client.Controllers.Stock.Reklamationen;
using Comain.Client.Models.Stock;
using Comain.Client.Services.Stock.Wareneingänge;
using Comain.Client.ViewModels.Stock.Wareneingänge;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Wareneingänge
{
    
    public class EingangController : ControllerBase, IEingangController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IEingangService store;
        private readonly IEingangCommandMediator commands;
        private bool loadCompleted;
        private IList<EingangListViewItem> currentList;
        private readonly MainViewModel mainView;
        private readonly EingangViewModel view;
        private readonly EingangListViewModel listView;
        private readonly EingangFilterViewModel filterView;
        private readonly EingangDetailViewModel detailView;
        private readonly EingangLagerViewModel lagerView;
        private readonly EingangBSPosViewModel posView;
        private IReklamController rkController;
        private SerializableCommand addPosCommand;
        private SerializableCommand lagerCommand;
        private SerializableCommand editCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand deletePosCommand;
        private SerializableCommand detailCommand;
        private SerializableCommand reklamCommand;
        private bool reklamMode;


        public EingangController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                 IEingangService store, IEingangCommandMediator commands, IControllerBus bus,
                                 MainViewModel mainView, EingangViewModel view, EingangListViewModel listView, EingangFilterViewModel filterView, 
                                 EingangDetailViewModel detailView, EingangLagerViewModel lagerView, EingangBSPosViewModel posView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.commands = commands;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.lagerView = lagerView;
            this.posView = posView;
        }


        public void Initialize()
        {

            rkController = bus.GetController<IReklamController>();

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.AddPosCommand = addPosCommand = CreateCommand(_ => AddPosAsync(), _ => detailView.Current != null);
            view.LagerCommand  = lagerCommand = CreateCommand(_ => LagerAsync(), _ => detailView.CurrentWE != null);
            view.EditCommand   = editCommand = CreateCommand(_ => EditAsync(), _ => detailView.Current != null);
            view.SaveCommand   = CreateCommand(_ => SaveAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.CancelCommand = CreateCommand(_ => CancelAsync());
            view.AddCommand    = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => detailView.Current != null);
            view.DeletePosCommand = deletePosCommand = CreateCommand(_ => DeletePosAsync(), _ => detailView.CurrentWE != null);
            view.ReklamCommand = reklamCommand = CreateCommand(_ => ReklamAsync(), _ => detailView.CurrentWE != null);
            listView.DetailCommand = detailCommand = CreateCommand(_ => DetailAsync(), _ => listView.Current != null);
            filterView.SearchCommand = CreateCommand(_ => LoadListAsync());

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Wareneingänge");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            reklamMode = false;
           
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, String.Empty);
        }
 
 
        protected override async Task<bool> UnloadImplAsync()
        {

            if (reklamMode) return await rkController.UnloadAsync().ConfigureAwait(false);

            if (detailView.Current != null && (detailView.Current.IsChanged || detailView.Current.Eingänge.IsChanged ||
                                               detailView.Current.Eingänge.Any(p => p.Einlagerungen.Any(q => q.IstAngefügt)))) {

                if (!await messages.ConfirmExitAsync()) return false;
            }

            listView.Unload();
            filterView.Unload(); 
            detailView.Unload(); 
            lagerView.Unload();
            posView.Unload();
            await SetModelAsync(null, null);

            return true;
        }


        private async Task SearchAsync()
        {

            if (loadCompleted) await LoadListAsync();
            else               await LoadAsync();
        }


        private async Task LoadAsync()
        {

            await SetModelAsync(null);
            commands.SetEditMode(EditMode.ReadOnly);

            loadCompleted = false;
            var oldlistmodel = listView.Current;

            listView.Unload();

            using (new VisualDelay(view)) {

                var autask = store.AuftragsnummernAsync(bus.Token);
                var lstask = store.LieferscheinnummernAsync(bus.Token);
                var bstask = store.BestellnummernAsync(bus.Token);
                var lotask = store.LagerorteAsync(bus.Token);
                var wetask = LoadListAsync();
                
                await Task.WhenAll(wetask, autask, lstask, bstask, lotask).ConfigureAwait(false);
                
                bus.Token.ThrowIfCancellationRequested();
                                
                await context.SendAsync(new SendOrPostCallback((o) => {
             
                    filterView.Aufträge = autask.Result.OrderBy(p => p, new StringExtensions.StringNaturalComparer()).ToList();
                    filterView.Lieferscheine = lstask.Result.OrderBy(p => p).ToList();
                    filterView.Bestellungen = bstask.Result.OrderBy(p => p, new StringExtensions.StringNaturalComparer()).ToList();
                    lagerView.Lagerorte = lotask.Result.OrderBy(p => p.Nummer, new StringExtensions.StringNaturalComparer()).ToList();
                             
                }), null);
            }
         
            if (oldlistmodel != null && !currentList.IsNullOrEmpty()) {
            
                listView.Current = currentList.FirstOrDefault(p => p.EingangId == oldlistmodel.EingangId);
                await DetailAsync();
            
            } else {
          
                await SetModelAsync(null);
            }    
        }


        private async Task LoadListAsync()
        {

            using (new VisualDelay(view)) {

                listView.Current = null;
                await SetModelAsync(null);    
          
                var qry = await store.FindAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                currentList = qry.Select(p => new EingangListViewItem(p)).ToList();
                await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist = new ObservableCollection<EingangListViewItem>(currentList); }), null);

                if (currentList.Count == 0) filterView.ResultText = "Filter: Keine Wareneingänge gefunden";
                else if (currentList.Count == 1) filterView.ResultText = "Filter: Ein Wareneingang gefunden";
                else filterView.ResultText = String.Format("Filter: {0} Wareneingänge gefunden", currentList.Count);
            }
        }
 

        private Task SetModelAsync(Lieferschein ls, Wareneingang we = null)
        {

            detailView.CurrentWE = we;
            detailView.Current = ls;

            if (we != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.EingangId == we.Id);
                if (listviewitem != null) listviewitem.Model = we;
            }      
 
            UpdateCommands();
            return Task.FromResult(true); 
        }
    

        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => { 

                addPosCommand.RaiseCanExecuteChanged();
                lagerCommand.RaiseCanExecuteChanged();
                editCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                deletePosCommand.RaiseCanExecuteChanged();
                detailCommand.RaiseCanExecuteChanged();
                reklamCommand.RaiseCanExecuteChanged();

            }), null);

            commands.NeuSetzen();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            
            if (args.PropertyName == "CurrentWE") { 
                
                UpdateCommands();
            }
        }
 

        private async Task DetailAsync()
        {

            if (listView.Current != null) {
           
                using (new VisualDelay(detailView)) {
               
                    var model = await store.DetailAsync(listView.Current.LieferscheinId, bus.Token).ConfigureAwait(false);
                    await SetModelAsync(model, listView.Current == null? null: model.Eingänge.FirstOrDefault(p => p.Id == listView.Current.EingangId));
                }
            
            } else {
           
                await SetModelAsync(null);    
            }
        }


        private async Task SaveAsync()
        {
            
            if (detailView.Current == null) return;
            var we = detailView.CurrentWE;
            if (!detailView.Current.ErrorList.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Lieferschein kann nicht gespeichert werden.", Reasons = detailView.Current.ErrorList });
                return;
            }

            using (new VisualDelay(detailView)) {

                var model = await store.UpdateAsync(detailView.Current, bus.Token);
                await listView.SynchronizeAsync(model);

                commands.SetEditMode(EditMode.ReadOnly);
                await SetModelAsync(model, we == null? null: model.Eingänge.FirstOrDefault(p => p.Id == we.Id));
            }
        }


        private async Task LagerAsync()
        {

            var we = detailView.CurrentWE;
            if (we == null) return;
            if (!we.WLValidation.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Dieser Wareneingang kann nicht eingelagert werden.", Reasons = we.WLValidation });
                return;
            }

            lagerView.TakeCommand = flyouts.YesCommand;
            lagerView.Menge = we.EinlagerbareMenge;
            lagerView.Preis = we.Bestellposition.Preis;
            lagerView.Lagerort = null;

            var lo = await store.LetzerLagerortAsync(detailView.CurrentWE.Material.Id, bus.Token);
            if (lo != null) {
            
                lagerView.LetzterLagerort = lo.Nummer;
                lagerView.Lagerort = lagerView.Lagerorte.FirstOrDefault(p => p.Id == lo.Id);
            }

            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = lagerView.View, Header = "Einlagern", Position = MahApps.Metro.Controls.Position.Right })) return;

            var wl = new Wareneinlagerung { Datum = lagerView.Datum, Preis = lagerView.Preis, Menge = lagerView.Menge, Lagerort = lagerView.Lagerort, Wareneingang = we };
            var validation = wl.ErrorListWithMaxMenge(we.EinlagerbareMenge);
            if (!validation.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Ware kann nicht eingelagert werden.", Reasons = validation });
                return;
            }

            we.Einlagerungen.Add(wl);
            we.RaisePropertyChanged("EingelagerteMenge");
        }


        private Task EditAsync()
        {

            if (detailView.Current != null) commands.SetEditMode(EditMode.Edit);
            return Task.FromResult(true);  
        }


        private async Task CancelAsync()
        {

            if (commands.CurrentMode == EditMode.Append) {

                await SetModelAsync(null);

            } else {

                await DetailAsync();
            }

            commands.SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task AddAsync()
        {

            var ls = new Lieferschein { Datum = DateTime.Today };
            ls.AcceptChanges();
            await SetModelAsync(ls);
            commands.SetEditMode(EditMode.Append);
            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            var ls = detailView.Current;

            if (ls == null) return;
            if (ls.HatEinlagerungen) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Lieferschein kann nicht gelöscht werden da Eingänge bereits eingelagert wurden." });
                return;
            }

            if (!ls.IstAngefügt) {

                if (!await messages.ConfirmDeleteAsync(new DeleteToken { IsDeleteable = true, Header = "Soll der Lieferschein tatsächlich gelöscht werden?" })) return;
                await store.DeleteAsync(ls.Id, bus.Token).ConfigureAwait(false);
            }

            if (!ls.Eingänge.IsNullOrEmpty()) await listView.RemoveItemsAsync(ls.Eingänge);
            await SetModelAsync(null);
            commands.SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task DeletePosAsync()
        {

            if (detailView.Current == null || detailView.CurrentWE == null) return;
            if (detailView.CurrentWE.HatEinlagerungen) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Wareneingang kann nicht gelöscht werden da er bereits eingelagert wurde." });
                return;
            }

            await context.SendAsync(new SendOrPostCallback((o) => {  

                detailView.Current.Eingänge.Remove(detailView.CurrentWE);
                detailView.CurrentWE = null;
            }), null);

            UpdateCommands();
        }


        private async Task AddPosAsync()
        {

            var ls = detailView.Current;
            if (ls == null) return;

            //alle offenen Bestellpositionen die auf diesem Lieferschein noch nicht verwendet wurden 
            var offeneBsPos = await store.OffenePositionenAsync(bus.Token).ConfigureAwait(false);
            var items = offeneBsPos.Where(p => !ls.Eingänge.Any(q => q.Bestellposition.Id == p.Id))
                                   .Select(p => new BestellposSelektorItem(p))
                                   .OrderBy(p => p.Nummer)
                                   .ToList();
            await context.SendAsync(new SendOrPostCallback((o) => { posView.SetzePositionen(items); }), null);

            posView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = posView.View, Header = "Bestellposition wählen", Position = MahApps.Metro.Controls.Position.Right })) return;

            var addWEList = posView.GetSelected().Select(p => p.AsModel()).ToList();
            addWEList.ForEach(p => p.Lieferschein = ls);
            await OnUIThreadAsync(() => { 
                
                ls.Eingänge.AddRange(addWEList); 
                listView.Foundlist.AddRange(addWEList.Select(p => new EingangListViewItem(p)));
            });
            UpdateCommands();
        }


        private async Task ReklamAsync()
        {

            if (detailView.CurrentWE == null) return;
            reklamMode = true;
            await rkController.RunAsync(new WEReklamData { Wareneingang = detailView.CurrentWE, Callback = AddReklamation });
        }


        /// <summary>
        /// mit dem ReklamController erstellte Reklamation übernehmen
        /// </summary>
        protected async void AddReklamation()
        {

            reklamMode = false;
            mainView.PageView = view.View;
            await DetailAsync();
        }
    }
}
