﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class PSReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public PSReport(IReportService store, PersonalViewModel personView, ZeitViewModel zeitView)
        {

            this.store = store;
            filterViews = new List<IFilterViewModel> { personView, zeitView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.PsAsync(GebeFilter(), ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Personal"; }
        }


        public string Gruppe
        {
            get { return "Personal"; }
        }


        public int Sortierung
        {
            get { return 100; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.PSReport.rdlc"; }
        }
    }
}
