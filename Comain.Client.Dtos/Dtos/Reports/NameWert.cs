﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class NameWert
    {

        public String   Name    { get; set; }
        public decimal? Wert    { get; set; }
        public decimal? Prozent { get; set; }
        
        public String   NameUndProzent
        {
            
            get {

                if (!Wert.HasValue || !Prozent.HasValue) return Name;
                return String.Format("{0} ({1:P})", Name, Prozent);
            }
        }
    }
}
