﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class KundenViewModel : ViewModel<IKundenView>, IFilterViewModel
    {

        public KundenViewModel(IKundenView view) : base(view)
        {}


        public void Reset()
        {
            NurKunden = false;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {

            if (nurKunden) return "nur Kundenkostenstellen und -standorte";
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (NurKunden) filter.Add("NurKundenKstPz", true);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private bool nurKunden;
        public bool NurKunden
        {
            get { return nurKunden; }
            set { SetProperty(ref nurKunden, value); }
        }


#endregion

    }
}
