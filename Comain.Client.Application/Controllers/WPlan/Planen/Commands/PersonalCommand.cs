﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan.Planen;


namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class PersonalCommand : IUndoRedoCommand, IResultCommand
    {

        private readonly IMapper mapper;
        private List<Tuple<int?, Planaufgabe>> models;
        private int? psid;


        public PersonalCommand(IEnumerable<Planaufgabe> models, int? psid, IMapper mapper)
        {

            this.psid = psid;
            this.models = models.Select(p => new Tuple<int?, Planaufgabe>(p.Auftragnehmer_Id, p)).ToList();
            this.mapper = mapper;
        }


        public void Redo() => models.ForEach(p => p.Item2.SetPersonal(psid));
        public void Undo() => models.ForEach(p => p.Item2.SetPersonal(p.Item1));


        public bool ModelHasChanged => true;


        public IEnumerable<PositionSubmitDto> Modifications()
        {

            var dtos = mapper.Map<List<PositionSubmitDto>>(models.Select(p => p.Item2));
            dtos.ForEach(p => p.ChangeStatus = ChangeStatus.Changed);
            return dtos;
        }
    }
}
