﻿using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.Models.Stock;
using Comain.Client.ViewModels.Stock.Verwaltung;
using Comain.Client.Views.Stock.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Stock.Verwaltung
{

    public class WarengruppenController : SimpleController<KeyNameDto, Warengruppe, IWarengruppenView>, IWarengruppenController
    {

        public WarengruppenController(IMessageService messages, ISimpleService<KeyNameDto, Warengruppe> store, IControllerBus bus,
                                      VerwaltungViewModel mainView, WarengruppenViewModel view)
          : base(messages, store, bus, mainView, view)
        {}


        protected override string Url
        {
            get { return "/VW/WG"; }
        }
    }
}
