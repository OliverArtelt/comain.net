﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Gateways;
using Comain.Client.Models.Stock;
using Comain.Client.ViewData.Stock.Reports;
using Comain.Client.Trackers;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models.Specs;

namespace Comain.Client.Services.Stock.Artikelstamm
{

    public class ArtikelService : IArtikelService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public ArtikelService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<Artikel> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/Artikel/Detail/{0}", id);
            var model = await store.GetAsync<Artikel>(url, ct).ConfigureAwait(false);
            model.AcceptChanges();
            model.MaterialIHObjektMap.AcceptChanges();
            model.MaterialLieferantMap.AcceptChanges();
            model.TechnischeWerte.AcceptChanges();
            return model;
        }


        public async Task<Artikel> SaveAsync(Artikel model, CancellationToken ct = default)
        {

            var url = "/Artikel/Detail";
            var dto = mapper.Map<Artikel, ArtikelDto>(model);
            dto.MaterialIHObjektMap = mapper.AsDtoList<ArtikelIHObjekt, ArtikelIhoMap>(model.MaterialIHObjektMap);
            dto.MaterialLieferantMap = mapper.AsDtoList<ArtikelLieferant, ArtikelLfMap>(model.MaterialLieferantMap);
            dto.TechnischeWerte = mapper.AsDtoList<TechnischerWert, TechWertDto>(model.TechnischeWerte);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<ArtikelDto, ArtikelDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<ArtikelDto, Artikel>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);


        public Task<IList<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct);


        public Task<IList<IHObjektFilterModel>> IHObjekteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<IHObjektFilterModel>>("/Filter/IH", ct);


        public Task<IList<LieferantFilterModel>> LieferantenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<LieferantFilterModel>>("/Filter/LF", ct);


        public Task<IList<NummerFilterModel>> WarengruppenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/WG", ct);


        public Task<IList<NummerFilterModel>> EinheitenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/EH", ct);


        public Task<IList<ArtikelListDto>> ArtikelAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IList<ArtikelListDto>>("/Artikel/Search", filter, ct);


        public Task<IList<SpecFilterModel>> SpecsAsync(CancellationToken ct = default)
            => store.GetAsync<IList<SpecFilterModel>>("/Spc/Filter/Artikel", ct);


        public async Task<IList<BestandReportRow>> BestandReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var dtos = await store.FindAsync<IList<ArtikelReportDto>>("/Artikel/BestandReport", filter, ct);
            ct.ThrowIfCancellationRequested();
            return MapBestandReportRows(dtos).ToList();
        }


        public async Task<IList<BestandExportRow>> BestandExportAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var dtos = await store.FindAsync<IList<ArtikelReportDto>>("/Artikel/BestandReport", filter, ct);
            ct.ThrowIfCancellationRequested();
            return MapBestandExportRows(dtos).ToList();
        }


        public async Task<IList<MtLfReportRow>> MtLfReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var dtos = await store.FindAsync<IList<MtLfReportDto>>("/Artikel/MtLfReport", filter, ct);
            ct.ThrowIfCancellationRequested();
            return MapMtLfReportRows(dtos).ToList();
        }


        public async Task<IList<MtLfExportRow>> MtLfExportAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var dtos = await store.FindAsync<IList<MtLfReportDto>>("/Artikel/MtLfReport", filter, ct);
            ct.ThrowIfCancellationRequested();
            return MapMtLfExportRows(dtos).ToList();
        }


        private IEnumerable<MtLfReportRow> MapMtLfReportRows(IEnumerable<MtLfReportDto> dtos)
        {

            foreach (var dto in dtos) {

                if (dto.MaterialIHObjektMap.IsNullOrEmpty() && dto.MaterialLieferantMap.IsNullOrEmpty()) yield return new MtLfReportRow(dto);
                else {

                    foreach (var p in dto.MaterialIHObjektMap.OrderBy(p => p.IHObjekt.Nummer)) { yield return new MtLfReportRow(dto, p); }
                    foreach (var p in dto.MaterialLieferantMap.OrderBy(p => p.Nummer))         { yield return new MtLfReportRow(dto, p); }
                }
            }
        }


        private IEnumerable<MtLfExportRow> MapMtLfExportRows(IEnumerable<MtLfReportDto> dtos)
        {

            foreach (var dto in dtos) {

                yield return new MtLfExportRow(dto);

                foreach (var p in dto.MaterialIHObjektMap.OrderBy(p => p.IHObjekt.Nummer)) { yield return new MtLfExportRow(dto, p); }
                foreach (var p in dto.MaterialLieferantMap.OrderBy(p => p.Nummer))         { yield return new MtLfExportRow(dto, p); }
            }
        }


        private IEnumerable<BestandReportRow> MapBestandReportRows(IEnumerable<ArtikelReportDto> dtos)
        {

            foreach (var dto in dtos) {

                yield return new BestandReportRow(dto);
                if (dto.DeaktiviertAm.HasValue) yield return new BestandReportRow(dto, dto.DeaktiviertAm.Value);

                foreach (var p in dto.IHObjekte.OrderBy(p => p.Nummer)) { yield return new BestandReportRow(dto, p); }    
                foreach (var p in dto.Lager.OrderBy(p => p.Nummer))     { yield return new BestandReportRow(dto, p); }    

                yield return new BestandReportRow(dto, true);
            }
        }


        private IEnumerable<BestandExportRow> MapBestandExportRows(IEnumerable<ArtikelReportDto> dtos)
        {

            foreach (var dto in dtos) {

                yield return new BestandExportRow(dto);

                foreach (var p in dto.IHObjekte.OrderBy(p => p.Nummer)) { yield return new BestandExportRow(dto, p); }
                foreach (var p in dto.Lager.OrderBy(p => p.Nummer))     { yield return new BestandExportRow(dto, p); }

                yield return new BestandExportRow(dto, true);
            }
        }


        public Task<IList<MtLfRankReportDto>> MtLfRankReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<MtLfRankReportDto>>("/Artikel/MtLfRankReport", filter, ct);
        }


        public Task<Artikel> DeactivateAsync(int id, CancellationToken ct = default(CancellationToken))
        {

            var uri = String.Format(@"/Artikel/Deactivate/{0}", id);
            return GetAsync(uri, ct);
        }


        public Task<Artikel> ReactivateAsync(int id, CancellationToken ct = default(CancellationToken))
        {

            var uri = String.Format(@"/Artikel/Reactivate/{0}", id);
            return GetAsync(uri, ct);
        }


        private async Task<Artikel> GetAsync(String uri, CancellationToken ct)
        {

            var model = await store.GetAsync<Artikel>(uri, ct).ConfigureAwait(false);
            model.AcceptChanges();
            model.MaterialIHObjektMap.AcceptChanges();
            model.MaterialLieferantMap.AcceptChanges();
            return model; 
        }


        public async Task DeleteAsync(Artikel model, CancellationToken ct = default)
        {

            var url = String.Format(@"/Artikel/{0}", model.Id);
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }
    }
}
