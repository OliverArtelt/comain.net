﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;
using Comain.Client.Dtos.Filters;
using Microsoft.Extensions.Logging;
using Utf8Json;

namespace Comain.Client.Gateways
{

    public class Repository : IRepository
    {

        private readonly ILogger logger;
        private ConnectionEndpoint endpoint;
        private TokenResponse accessToken;
        private readonly SemaphoreSlim loginLocker = new SemaphoreSlim(1, 1);
        private IJsonFormatterResolver resolver = Utf8Json.Resolvers.StandardResolver.CamelCase;


        public Repository(ILogger<Repository> logger)
        {
            this.logger = logger;
        }


        private HttpClient CreateAuthenticated()
        {

            if (accessToken == null) throw new InvalidOperationException("Der Benutzer ist nicht angemeldet.");

            var client = CreateUnauthenticated();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (endpoint.UseCompression) client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            client.DefaultRequestHeaders.Authorization = accessToken.Header;
            client.Timeout = TimeSpan.FromMinutes(30);

            return client;
        }


        private HttpClient CreateUnauthenticated()
        {

            var client = new HttpClient();
            client.BaseAddress = new Uri(endpoint.BaseUrl);
            return client;
        }


        public async Task LoginAsync(ConnectionEndpoint endpoint, CancellationToken ct = default)
        {

            if (accessToken != null) await LogoutAsync(ct).ConfigureAwait(false);
            this.endpoint = endpoint;
            accessToken = null;
            if (endpoint == null) throw new UserException("Es wurde keine Verbindung gewählt.");

            await loginLocker.WaitAsync(ct).ConfigureAwait(false);

            try {

                using var client = CreateUnauthenticated();
                var jsonBody = JsonSerializer.ToJsonString(endpoint.LoginData, resolver);
                var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
                using var response = await client.PostAsync("login", content, ct).ConfigureAwait(false);

                await EnsureSuccessStatusCodeAsync(response).ConfigureAwait(false);
                accessToken = await JsonSerializer.DeserializeAsync<TokenResponse>(response.Content.ReadAsStream(ct), resolver).ConfigureAwait(false);
                logger.LogInformation($"Login {endpoint.Username}@{endpoint.Name}");
            }
            finally {

                loginLocker.Release();
            }
        }


        public async Task LogoutAsync(CancellationToken ct = default)
        {

            if (accessToken == null || endpoint == null) return;

            try {

                using (var client = CreateAuthenticated()) {

                    await client.GetAsync("login/logout", ct).ConfigureAwait(false);
                    logger.LogInformation("Logout " + endpoint.Name);
                }
            }
            catch (Exception) {

                accessToken = null;
                endpoint = null;
                throw;
            }

            accessToken = null;
            endpoint = null;
        }


        public async Task<T> GetAsync<T>(String uri, CancellationToken ct = default)
        {

            logger.LogInformation("GET " + uri);

            using var client = CreateAuthenticated();
            using var response = await client.GetAsync(uri, cancellationToken:ct).ConfigureAwait(false);

            await EnsureSuccessStatusCodeAsync(response).ConfigureAwait(false);
            var result = await JsonSerializer.DeserializeAsync<T>(response.Content.ReadAsStream(ct), resolver).ConfigureAwait(false);
            return result;
        }


        public async Task PostAsync<T>(String uri, T dto, CancellationToken ct = default)
        {

            logger.LogInformation("POST " + uri);

            using var client = CreateAuthenticated();
            var jsonBody = JsonSerializer.ToJsonString(dto, resolver);
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            using var response = await client.PostAsync(uri, content, ct).ConfigureAwait(false);

            await EnsureSuccessStatusCodeAsync(response);
        }


        public async Task<T> FindAsync<T>(String uri, Suchkriterien filter, CancellationToken ct = default)
        {

            logger.LogInformation("Find " + uri);

            using var client = CreateAuthenticated();
            var jsonBody = JsonSerializer.ToJsonString(filter, resolver);
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            using var response = await client.PostAsync(uri, content, ct).ConfigureAwait(false);

            await EnsureSuccessStatusCodeAsync(response);
            var result = await JsonSerializer.DeserializeAsync<T>(response.Content.ReadAsStream(ct), resolver).ConfigureAwait(false);
            return result;
        }


        public async Task<TDst> PostAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default)
        {

            logger.LogInformation("POST " + uri);

            var jsonBody = JsonSerializer.ToJsonString(dto, resolver);
            var postContent = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            using var client = CreateAuthenticated();
            using var response = await client.PostAsync(uri, postContent, ct).ConfigureAwait(false);

            await EnsureSuccessStatusCodeAsync(response);
            var result = await JsonSerializer.DeserializeAsync<TDst>(response.Content.ReadAsStream(ct), resolver).ConfigureAwait(false);
            return result;
        }


        public async Task PutAsync<T>(String uri, T dto, CancellationToken ct = default)
        {

            logger.LogInformation("PUT " + uri);

            using var client = CreateAuthenticated();
            var jsonBody = JsonSerializer.ToJsonString(dto, resolver);
            var postContent = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            using var response = await client.PutAsync(uri, postContent, ct).ConfigureAwait(false);

            await EnsureSuccessStatusCodeAsync(response);
        }


        public async Task<TDst> PutAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default)
        {

            logger.LogInformation("PUT " + uri);

            using var client = CreateAuthenticated();
            var jsonBody = JsonSerializer.ToJsonString(dto, resolver);
            var postContent = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            using var response = await client.PutAsync(uri, postContent, ct);

            await EnsureSuccessStatusCodeAsync(response);
            var result = await JsonSerializer.DeserializeAsync<TDst>(response.Content.ReadAsStream(ct), resolver).ConfigureAwait(false);
            return result;
        }


        public async Task DeleteAsync(String uri, CancellationToken ct = default)
        {

            logger.LogInformation("DELETE " + uri);

            using var client = CreateAuthenticated();
            using var response = await client.DeleteAsync(uri, ct);

            await EnsureSuccessStatusCodeAsync(response);
        }


        private async Task EnsureSuccessStatusCodeAsync(HttpResponseMessage response)
        {

            if ((int)response.StatusCode < 300) return;

            if (response.StatusCode == System.Net.HttpStatusCode.RequestEntityTooLarge) throw new NotSupportedException("Die hochgeladenen Dokumente sind in ihrer Gesamtmenge zu groß. Bitte laden Sie die Dokumente einzeln hoch.");
            if (response.StatusCode == System.Net.HttpStatusCode.MethodNotAllowed)      throw new AuthenticationException("Der Server unterstützt nicht diese HTTP-Methode.");
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)              throw new NotSupportedException("Die angeforderte Ressource wurde auf dem Server nicht gefunden.");
            if (response.StatusCode == System.Net.HttpStatusCode.Conflict)              throw new NotSupportedException("Die Anfrage kann nicht verarbeitet werden da die Ressource inzwischen verändert wurde.");
            if (response.StatusCode == System.Net.HttpStatusCode.BadGateway)            throw new ComainOperationException("Die Anfrage kann nicht verarbeitet werden da der Server gerade gewartet wird oder überlastet ist.");

            var msg = await response.Content.ReadAsStringAsync();
            if (msg.Contains("Der Benutzer ist nicht freigeschaltet"))                  throw new AuthenticationException("Der Benutzer ist nicht freigeschaltet.");
            // Probleme mit Unicode-Zeichenklassen bei Passwortgenerierung: wenn Client fehlerhaftes Passwort nicht erkennt dann Server validieren lassen
            if (msg.Contains("PasswordRequiresLower"))                                  throw new ComainOperationException("Das Kennwort muss mindestens einen Kleinbuchstaben besitzen.");
            if (msg.Contains("PasswordRequiresUpper"))                                  throw new ComainOperationException("Das Kennwort muss mindestens einen Großbuchstaben besitzen.");
            if (msg == "Benutzer oder Passwort unbekannt.")                             throw new AuthenticationException("Die Zugangsdaten sind nicht korrekt.");

            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)             throw new UnauthorizedAccessException("Sie haben keinen Zugriff auf diese Ressource.");
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)          throw new AuthenticationException("Die Zugangsdaten sind nicht korrekt.");


            if (msg.Trim().StartsWith("{")) {

                var ex = JsonSerializer.Deserialize<ExceptionDto>(msg, resolver);
                if (ex.Error == "invalid_grant") throw new AuthenticationException("Die Zugangsdaten sind nicht korrekt.");
                if (!String.IsNullOrEmpty(ex.Details)) throw new ComainServerException(ex);
                if (ex.Explanations.IsNullOrEmpty()) throw new ComainServerException(ex);
                else throw new ComainValidationException(ex);
            }

            throw new ComainServerException(msg);
        }
    }
}
