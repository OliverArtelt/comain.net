﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Services.Dokumente
{
    public interface IDokumentService
    {

        Task<IList<Dokument>>       SearchAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<byte[]>                GetContentAsync(int id, CancellationToken ct = default);
        Task<Media>                 GetThumbnailAsync(Dokument model, CancellationToken ct = default);
        Task<IList<Dokument>>       SaveAsync(TrackableCollection<Dokument> list, CancellationToken ct = default);

        Task<IList<Dokument>>       GetAssignedAsync(Suchkriterien filter, CancellationToken ct = default);
        Task                        SaveAssignedAsync(DocEntity model, CancellationToken ct = default);
        Task                        SaveAssignedAsync(IEnumerable<DocEntity> models, CancellationToken ct = default);
        Task<IList<AssignedEntity>> AssignedEntitiesAsync(int documentId, CancellationToken ct = default);
        Task<IHObjekt>              GetIHObjektByMassnahmeIdAsync(int massnahmeId, CancellationToken ct = default);

        Task<IList<String>>         SchlagworteAsync(CancellationToken ct = default);
        Task<IList<Kategorie>>      KategorienAsync(CancellationToken ct = default);
    }
}