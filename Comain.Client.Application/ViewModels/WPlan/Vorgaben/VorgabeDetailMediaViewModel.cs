﻿using System;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan;
using Comain.Client.Views.WPlan.Vorgaben;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailMediaViewModel : ValidatableViewModel<IVorgabeDetailMediaView, WiMassnahme>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
 

        public VorgabeDetailMediaViewModel(IVorgabeDetailMediaView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(WiMassnahme oldSelected)
        {
            
            RaisePropertyChanged(nameof(Medien));
            RaisePropertyChanged(nameof(CurrentMedia));
            RaisePropertyChanged(nameof(ResultText));   
        }


        public void Unload()
        {
        }
  

#region P R O P E R T I E S


        public TrackableCollection<Media> Medien
        { 
            get { return Current?.Medien; }
            set { 
            
                if (Current != null && Current.Medien != value) {
                
                    Current.Medien = value;
                    RaisePropertyChanged(nameof(Medien));
                }
            }
        }
           

        public String ResultText
        {

            get {

                if (Current == null || Current.Medien == null) return null;

                switch (Current.Medien.Count) {
                    case 0: return "Keine Bilder gefunden.";
                    case 1: return "Ein Bild gefunden.";
                    default: return $"{Current.Medien.Count} Bilder gefunden.";

                }
            }
        }


        private Media currentMedia;
        public Media CurrentMedia
        { 
            get { return currentMedia; }
            set { SetProperty(ref currentMedia, value); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand editImageCommand;
        public ICommand EditImageCommand
        { 
            get { return editImageCommand; }
            set { SetProperty(ref editImageCommand, value); }
        }


#endregion

    }
}
