﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IStandortController : IVwTypController
    {}


    public class StandortController : ControllerBase, IStandortController
    {

        private readonly VerwaltungViewModel mainView;
        private readonly StandortViewModel view;
        private readonly IStandortService store;
        private Standort currentPz;
        private int idstamp;
        private SerializableCommand deleteCommand;


        public StandortController(IMessageService messages, IStandortService store, IControllerBus bus,
                                  VerwaltungViewModel mainView, StandortViewModel view)
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
            this.mainView = mainView;
        }


        public void Initialize()
        {

            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => view.Current != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;

            store.Reset();
            view.Unload();
            currentPz = null;
            return true;
        }


        private async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                await store.SaveAsync(bus.Token);
                await LoadAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var oldmodel = view.Current;

                await store.LoadAsync(bus.Token);
                await OnUIThreadAsync(() => {

                    view.Standorte = store.CurrentPzList;
                    if (oldmodel != null) view.Current = store.CurrentPzList.FirstOrDefault(p => p.Id == oldmodel.Id);

                });

                idstamp = 0;
            }
        }


        private async Task AddAsync()
        {

            if (view.Standorte == null) return;
            idstamp -= 1;

            await OnUIThreadAsync(() => {

                var model = new Standort { Id = idstamp };
                view.Standorte.Add(model);
                view.Current = model;

            });
        }


        private async Task DeleteAsync()
        {

            if (view.Standorte == null) return;
            if (view.Current == null) return;

            await OnUIThreadAsync(() => {

                view.Standorte.Remove(view.Current);
                view.Current = null;

            });
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (args.PropertyName == "Current" && currentPz != view.Current) {

                    currentPz = view.Current;
                    if (view.Current != null) {

                        var kstlist = store.Kostenstellen(view.Current);
                        context.Send(new SendOrPostCallback((o) => { view.Kostenstellen = kstlist; }), null);
                    }
                    else view.Kostenstellen = null;
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }

            if (args.PropertyName == "Current") UpdateCommands();
        }


        private void UpdateCommands()
        {
            context.Post(new SendOrPostCallback((o) => { deleteCommand.RaiseCanExecuteChanged(); }), null);
        }
    }
}
