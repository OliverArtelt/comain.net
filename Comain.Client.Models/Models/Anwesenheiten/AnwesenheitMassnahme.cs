﻿using System;
using System.ComponentModel;

namespace Comain.Client.Models.Anwesenheiten
{

    public class AnwesenheitMassnahme : Entity, IDataErrorInfo
    {

        private int menge;
        public int Menge
        {
            get => menge;
            set => SetProperty(ref menge, value);
        }


        public int      Anwesenheit_Id          { get; set; }
        public int      Massnahme_Id            { get; set; }
        public int?     Personalleistung_Id     { get; set; }
        public int?     Auftrag_Id              { get; set; }
        public String   Auftrag                 { get; set; }


#region V A L I D A T I O N


        public string Error => null;
        public string this[string columnName] => null;


#endregion

    }
}
