﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;

namespace Comain.Client.ViewModels
{

    public class AssignedEntityItem
    {

        private readonly AssignedEntity model;


        public AssignedEntityItem(AssignedEntity model)
        {
            this.model = model;
        }


        public String EntityType    => model.EntityType;
        public int    EntityId      => model.EntityId;
        public String Bezeichnung   => model.Bezeichnung;
        public String Nummer        => model.Nummer;


        public String Color
        {
            get
            {
                switch (EntityType) {

                    case "Asset":       return "Goldenrod";
                    case "Auftrag":     return "RoyalBlue";
                    case "Maßnahme":    return "Crimson";
                    case "Personal":    return "MediumOrchid";
                    case "Artikel":     return "#00b359";
                    default:            return "WhiteSmoke";
                }
            }
        }


        public String BackColor
        {
            get
            {
                switch (EntityType) {

                    case "Asset":       return "#eed490";
                    case "Auftrag":     return "#a7b9f1";
                    case "Maßnahme":    return "#f48aa0";
                    case "Personal":    return "#deaeea";
                    case "Artikel":     return "#99ffcc";
                    default:            return "LightGray";
                }
            }
        }


        public String Icon
        {
            get
            {
                switch (EntityType) {

                    case "Asset":       return "Cogs";
                    case "Auftrag":     return "ColumnOne";
                    case "Maßnahme":    return "CalendarWeek";
                    case "Personal":    return "User";
                    case "Artikel":     return "Cog";
                    default:            return "Question";
                }
            }
        }
    }
}
