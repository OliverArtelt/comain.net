﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Tickets;
using Comain.Client.ViewModels.Tickets;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.Views;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using System;

namespace Comain.Client.Controllers.Tickets
{

    public class AddTicketController : ControllerBase, IAddTicketController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IAddTicketService store;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService config;
        private readonly ITabletInfo tablets;
        private readonly MainViewModel mainView;
        private readonly AddTicketViewModel view;
        private readonly AddTicketResultViewModel resultView;


        public AddTicketController(IBreadcrumbService crumbs, IAddTicketService store, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                   IKonfigService config, IControllerBus bus, ITabletInfo tablets,
                                   MainViewModel mainView, AddTicketViewModel view, AddTicketResultViewModel resultView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.config = config;
            this.store = store;
            this.login = login;
            this.tablets = tablets;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.resultView = resultView;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            resultView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            tablets.TabletModeChanged += (o, e) => { view.TabletMode = tablets.InTabletMode; };
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ticket erstellen");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {}


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var gwtask = store.GewerkeAsync(bus.Token);
                var pstask = store.PersonalAsync(bus.Token);
                var drtask = store.DringlichkeitAsync(bus.Token);

                await Task.WhenAll(ihtask, pztask, gwtask, pstask, drtask).ConfigureAwait(false);
                await view.SetFilterAsync(ihtask.Result, gwtask.Result, pstask.Result, pztask.Result, drtask.Result.Liste(Auftragstyp.Ticket), bus.Token);
                view.Clear();
            }
        }


        protected override Task<bool> UnloadImplAsync()
        {

            view.Unload();
            return Task.FromResult(true);
        }


        private async Task BackAsync()
        {

            crumbs.Remove();
            await bus.BackAsync();
        }


        private async Task SaveAsync()
        {

            Auftrag result = null;
            String sendefehler = null;


            try {

                using (new VisualDelay(view)) {

                    (result, sendefehler) = await store.CreateAsync(view.GebeAuftrag(), bus.Token);
                }
            }
            catch (ComainValidationException x) {

                await messages.ShowMessageAsync(new MessageToken(x));
            }

            if (result == null) return;
            var title = $"Ticket {result.Nummer}";

            var address = "Comain.Client.Reports.TicketCreatedReport.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = title };
            var ihpath = await store.IHPathAsync(result.AuIHKS.IHObjekt, bus.Token);
            report.AddDataSource("MainData", new List<AuftragRow> { new AuftragRow(result, ihpath, false) });
            report.AddParameter("parTitle", title);
            report.AddParameter("parPrice", "0");
            report.HasLogo = true;

            await resultView.BindViewerAsync(report);
            resultView.SetzeSendefehler(sendefehler);
            mainView.PageView = resultView.View;
        }
    }
}
