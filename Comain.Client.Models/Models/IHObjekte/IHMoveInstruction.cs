﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.IHObjekte
{

    public class IHMoveInstruction 
    {

        public int                  SourceId                    { get; set; }
        public int                  DestinationId               { get; set; }
        public int                  DestinationType             { get; set; }
        /// <summary>
        /// Id der neuen Kostenstelle
        /// </summary>
        public int                  KostenstellenId             { get; set; }
        public IHMoveKstMode        KstMode                     { get; set; }
        public IHMoveAuftragMode    AuftragMode                 { get; set; }
        public int[]                UmziehendeAufträge          { get; set; }
    }
}
