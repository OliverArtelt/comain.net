﻿using System.Windows;
using System.Windows.Controls;
using Comain.Client.ViewModels.IHObjekte;

namespace Comain.Client.Views.IHObjekte
{

    public partial class IHSimpleSelektorView : UserControl, IIHSimpleSelektorView
    {

        public IHSimpleSelektorView()
        {
            InitializeComponent();
        }


        private void OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            
            var vm = DataContext as IHSimpleSelektorViewModel;
            if (vm != null) vm.SelectItem(e.NewValue);
        }
    }
}
