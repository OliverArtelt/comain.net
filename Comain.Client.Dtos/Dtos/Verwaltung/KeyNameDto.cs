﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class KeyNameDto : ChangeTrackingBase
    {
        
        public String     Nummer                  { get; set; }
        public String     Name                    { get; set; }
    }
}
