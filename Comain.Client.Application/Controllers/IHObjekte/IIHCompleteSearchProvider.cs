﻿using System.Collections.Generic;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.Controllers.IHObjekte
{

    public interface IIHCompleteSearchProvider
    {
        
        int                     Aktuell     { get; }
        int                     Gesamt      { get; }
        IHListDto               Current     { get; }
        
        void        SetzeIHObjekte(IEnumerable<IHListDto> list);
        void        Search(string searchTerm);
        IHListDto   Next();
        IHListDto   Prev();
    }
}
