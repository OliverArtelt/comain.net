﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Warenausgänge;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{
    
    public class AusgangListViewModel : ViewModel<IAusgangListView>
    {
 

        public AusgangListViewModel(IAusgangListView view) : base(view)
        {
        }


        public void Unload()
        {
            FoundList = null;
        }


#region P R O P E R T I E S   
 

        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private Warenausgang current;
        public Warenausgang Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


        private ObservableCollection<Warenausgang> foundlist;
        public ObservableCollection<Warenausgang> FoundList
        {
            get { return foundlist; }   
            set { SetProperty(ref foundlist, value); }
        }


#endregion

    }
}
