﻿namespace Comain.Client.Dtos.Reports
{

    public class EntwIHBudgetDto
    {

        public int          Jahr     { get; set; }
        public decimal?     Kosten   { get; set; }
    }
}
