﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class ProfilCommand : IControllerCommand 
    {

        public Type         Controller  => typeof(IProfilController);
        public String       Name        => "Profil";
        public String       Parameter   => "Profile";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 4000;
        public String       Color       => "SlateGray";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.user.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
            return nutzer != null;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
