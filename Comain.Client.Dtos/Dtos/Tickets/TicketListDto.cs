﻿using System;

namespace Comain.Client.Dtos.Tickets
{
    public class TicketListDto
    {

        public int          Id                  { get; set; }
        public String       Dringlichkeit       { get; set; }
        public int          DrPosition          { get; set; }
        public int          Status              { get; set; }
        public int          Typ                 { get; set; }
        public String       Nummer              { get; set; }
        public String       Kurzbeschreibung    { get; set; }
        public DateTime     GemeldetAm          { get; set; }
        public String       IHObjektNummer      { get; set; }
        public String       Maschine            { get; set; }
        public String       Auftraggeber        { get; set; }
        public String       Auftragnehmer       { get; set; }
        public DateTime?    Termin              { get; set; }
        public String       Kostenstelle        { get; set; }
        public String       Gewerk              { get; set; }
    }
}
