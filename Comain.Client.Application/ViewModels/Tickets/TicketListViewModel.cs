﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels.Aufträge;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketListViewModel : ViewModel<ITicketListView>
    {

        public TicketListViewModel(ITicketListView view) : base(view)
        {
        }


        public void Unload()
        {
            Foundlist = null;
        }


#region P R O P E R T I E S
 

        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private bool editEnabled = true;
        public bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


        private ObservableCollection<TicketListViewItem> foundlist;
        public ObservableCollection<TicketListViewItem> Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private TicketListViewItem current;
        public TicketListViewItem Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
