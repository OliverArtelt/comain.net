﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.ViewModels.Stock.Verwaltung
{
    
    public class LagerortViewModel : SimpleViewModel<Lagerort, ILagerortView>, IDelayableViewModel
    {

        public LagerortViewModel(ILagerortView view) : base(view)
        {}


        public override void CurrentChanged(Lagerort oldSelected)
        {
            
            RaisePropertyChanged("Halle");                  
            RaisePropertyChanged("Regal");               
            RaisePropertyChanged("Fach");              
            RaisePropertyChanged("Nummer");                  
            RaisePropertyChanged("Bemerkungen");                   
        }


#region P R O P E R T I E S


        public String Halle                 
        {
            get { return current == null? null: current.Halle; }
            set { 
            
                if (current != null && current.Halle != value) {

                    current.Halle = value;
                    RaisePropertyChanged("Halle");
                }
            }
        }
        

        public String Regal              
        {
            get { return current == null? null: current.Regal; }
            set { 
            
                if (current != null && current.Regal != value) {

                    current.Regal = value;
                    RaisePropertyChanged("Regal");
                }
            }
        }
        

        public String Fach              
        {
            get { return current == null? null: current.Fach; }
            set { 
            
                if (current != null && current.Fach != value) {

                    current.Fach = value;
                    RaisePropertyChanged("Fach");
                }
            }
        }
        

        public String Nummer  
        {
            get { return current == null? null: current.Nummer; }
            set { 
            
                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }
        

        public String Bemerkungen  
        {
            get { return current == null? null: current.Bemerkungen; }
            set { 
            
                if (current != null && current.Bemerkungen != value) {

                    current.Bemerkungen = value;
                    RaisePropertyChanged("Bemerkungen");
                }
            }
        }
        

#endregion

    }
}
