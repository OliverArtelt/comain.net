﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;

namespace Comain.Client.Models.WPlan
{

    /// <summary>
    /// Anweisung, welches Ersatzteil / Verbrauchsmaterial bei eienr Wartung zu ersetzen ist. Wird im Auftrag zu einer Materialleistung
    /// </summary>
    public class WiMaterialVorgabe : Entity, IDataErrorInfo
    {

        private WiMassnahme                 massnahme;
        private MaterialstammFilterModel    material;
        private String                      name;
        private String                      nummer;
        private NummerFilterModel           einheit;
        private decimal?                    menge;
        private decimal?                    epreis;
        private bool                        erforderlich;
        private String                      anweisung;


#region P R O P E R T I E S

        
        public WiMassnahme Massnahme
        {
            get { return massnahme; }
            set { SetProperty(ref massnahme, value); }
        }

        
        public MaterialstammFilterModel Material
        {
            get { return material; }
            set { 
            
                if (SetProperty(ref material, value) && value != null) {
                
                    Name = value.Name; 
                    Nummer = value.Nummer;
                }
            }
        }

        
        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        
        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        
        public NummerFilterModel Einheit
        {
            get { return einheit; }
            set { SetProperty(ref einheit, value); }
        }

        
        public decimal? Menge
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }

        
        public decimal? EPreis
        {
            get { return epreis; }
            set { SetProperty(ref epreis, value); }
        }

        
        public bool Erforderlich
        {
            get { return erforderlich; }
            set { SetProperty(ref erforderlich, value); }
        }

        
        public String Anweisung
        {
            get { return anweisung; }
            set { SetProperty(ref anweisung, value); }
        }


        public decimal? GPreis => Menge * EPreis;


#endregion
       
        
        public WiMaterialVorgabeDto AsDto()
        {

            var dto = new WiMaterialVorgabeDto();

            dto.Material      = Material;     
            dto.Name          = Name;         
            dto.Nummer        = Nummer;       
            dto.Einheit       = Einheit;      
            dto.Menge         = Menge;        
            dto.EPreis        = EPreis;       
            dto.Erforderlich  = Erforderlich; 
            dto.Anweisung     = Anweisung;    

            dto.Id = Id;
            dto.ChangedProps = ChangedProps?.ToHashSet();

            return dto;
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Menge)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Einheit)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Nummer)];
                return test;
            }
        }

       
        public IList<String> Errorlist
        {
            
            get { 

                var list = new List<String>();
            
                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Menge)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Einheit)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Nummer)];
                return list;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie den Namen des Materials an.";
                    if (Name.Length > 150) return "Der Name des Materials darf aus maximal 150 Zeichen bestehen."; 
                    break;

                case nameof(Menge):

                    if (Menge.GetValueOrDefault() == 0m) return "Geben Sie die Menge des Materials an.";
                    break;

                case nameof(Einheit):

                    if (Einheit == null) return "Das Material muss eine Einheit besitzen.";
                    break;

                case nameof(Nummer):

                    if (Nummer == null) return null;
                    if (Nummer.Length > 150) return "Die Nummer des Materials darf aus maximal 150 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
