﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services.IHObjekte;
using Comain.Client.Views.Bevor;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.ViewModels.Bevor
{

    public class BevorViewModel : ValidatableViewModel<IBevorView, BevorTicket>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly IHSimpleSelektorViewModel selektorView;
        private Dictionary<String, AuIHKSDto> ihdic;


        public BevorViewModel(IBevorView view, IIHFastSearchProvider ihSearcher, IPersonalSearchProvider psSearcher, IHSimpleSelektorViewModel selektorView)
          : base(view)
        {

            context = SynchronizationContext.Current;
            IHSearchProvider = ihSearcher;
            PsSearchProvider = psSearcher;
            this.selektorView = selektorView;
            TabletModeCommand = new DelegateCommand(_ => TabletMode = !TabletMode);
            PropertyChangedEventManager.AddHandler(selektorView, ViewPropertyChanged, "");
        }


        public async Task LoadAsync(IList<AuIHKSDto> ihlist, IList<PzFilterModel> pzlist, IList<PersonalFilterModel> pslist, IList<Dringlichkeit> drlist,
                                    CancellationToken ct = default)
        {

            ihdic = ihlist.DistinctBy(p => p.IHObjekt).ToDictionary(p => p.IHObjekt);
            var ifilst = ihdic.Values.Select(p => new IHFilterItem(p)).ToList();

            await context.SendAsync(new SendOrPostCallback((o) => {

                IHSearchProvider.SetItems(ifilst);
                PsSearchProvider.SetItems(pslist);
                Dringlichkeiten = drlist;

            }), null);

            await selektorView.LoadAsync(ifilst, pzlist, ct).ConfigureAwait(false);
            Current = new BevorTicket();
            Current.AcceptChanges();
            IHObjekt = null;
        }


        public override void CurrentChanged(BevorTicket oldSelected)
        {

            RaisePropertyChanged(nameof(IHObjekt));
            RaisePropertyChanged(nameof(IHNummer));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(Datum));
            RaisePropertyChanged(nameof(Uhrzeit));
            RaisePropertyChanged(nameof(Dringlichkeit));
            RaisePropertyChanged(nameof(Medien));
            RaisePropertyChanged(nameof(Auftragnehmer));
            RaisePropertyChanged(nameof(Baugruppenpfad));
        }


        public void Unload()
        {

            ihdic = null;
            Medien = null;
            selektorView.Unload();
            Dringlichkeiten = null;
            Current = null;
            IHObjekt = null;
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(selektorView.CurrentIHObjekt) && selektorView.CurrentIHObjekt != null) IHObjekt = selektorView.CurrentIHObjekt;
        }


#region P R O P E R T I E S


        public object SelektorView => selektorView?.View;


        public IIHFastSearchProvider IHSearchProvider { get; }


        public IPersonalSearchProvider PsSearchProvider { get; }


        private IHFilterItem iHObjekt;
        public IHFilterItem IHObjekt
        {
            get { return iHObjekt; }
            set {

                if (SetProperty(ref iHObjekt, value)) {

                    if (Current != null && value != null) Current.IHObjekt = ihdic.GetValueOrDefault(value.Nummer);
                    RaisePropertyChanged(nameof(IHNummer));
                    RaisePropertyChanged(nameof(Baugruppenpfad));
                }
            }
        }


        public String IHNummer          => iHObjekt?.Nummer;
        public String Baugruppenpfad    => iHObjekt?.Pfad ?? iHObjekt?.Name;


        public PersonalFilterModel Auftragnehmer
        {
            get { return Current?.Auftragnehmer; }
            set {

                if (Current != null && Current.Auftragnehmer != value) {

                    Current.Auftragnehmer = value;
                    RaisePropertyChanged(nameof(Auftragnehmer));
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return Current?.Kurzbeschreibung; }
            set {

                if (Current != null && Current.Kurzbeschreibung != value) {

                    Current.Kurzbeschreibung = value;
                    RaisePropertyChanged(nameof(Kurzbeschreibung));
                }
            }
        }


        public DateTime? Datum
        {
            get { return Current?.Datum; }
            set {

                if (Current != null && Current.Datum != value) {

                    Current.Datum = value;
                    RaisePropertyChanged(nameof(Datum));
                }
            }
        }


        public String Uhrzeit
        {
            get { return current == null || !current.Uhrzeit.HasValue ? null: current.Uhrzeit.Value.ToString("hh\\:mm"); }
            set {

                if (current != null) {

                    var time = value.ParseTime();
                    if (current.Uhrzeit != time) {

                        current.Uhrzeit = time;
                        RaisePropertyChanged(nameof(Uhrzeit));
                    }
                }
            }
        }


        private IList<Dringlichkeit> dringlichkeiten;
        public IList<Dringlichkeit> Dringlichkeiten
        {
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        public Dringlichkeit Dringlichkeit
        {
            get { return Current?.Dringlichkeit; }
            set {

                if (Current != null && Current.Dringlichkeit != value) {

                    Current.Dringlichkeit = value;
                    RaisePropertyChanged(nameof(Dringlichkeit));
                }
            }
        }


        public TrackableCollection<Media> Medien
        {
            get { return Current?.Medien; }
            set {

                if (Current != null && Current.Medien != value) {

                    Current.Medien = value;
                    RaisePropertyChanged(nameof(Medien));
                }
            }
        }


        private Media currentMedia;
        public Media CurrentMedia
        {
            get { return currentMedia; }
            set { if (SetProperty(ref currentMedia, value)) RaisePropertyChanged(nameof(MediaSelected)); }
        }


        public bool MediaSelected => CurrentMedia != null;


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand imageFromCameraCommand;
        public ICommand ImageFromCameraCommand
        {
            get { return imageFromCameraCommand; }
            set { SetProperty(ref imageFromCameraCommand, value); }
        }


        private ICommand imageFromFileCommand;
        public ICommand ImageFromFileCommand
        {
            get { return imageFromFileCommand; }
            set { SetProperty(ref imageFromFileCommand, value); }
        }


        private ICommand openImageCommand;
        public ICommand OpenImageCommand
        {
            get { return openImageCommand; }
            set { SetProperty(ref openImageCommand, value); }
        }


        private ICommand editImageCommand;
        public ICommand EditImageCommand
        {
            get { return editImageCommand; }
            set { SetProperty(ref editImageCommand, value); }
        }


        private ICommand saveImageCommand;
        public ICommand SaveImageCommand
        {
            get { return saveImageCommand; }
            set { SetProperty(ref saveImageCommand, value); }
        }


        private ICommand deleteImageCommand;
        public ICommand DeleteImageCommand
        {
            get { return deleteImageCommand; }
            set { SetProperty(ref deleteImageCommand, value); }
        }


        private ICommand deleteImagesCommand;
        public ICommand DeleteImagesCommand
        {
            get { return deleteImagesCommand; }
            set { SetProperty(ref deleteImagesCommand, value); }
        }


        private bool tabletMode;
        public bool TabletMode
        {
            get { return tabletMode; }
            set { if (SetProperty(ref tabletMode, value)) RaisePropertyChanged(nameof(Zoom)); }
        }


        public double Zoom => TabletMode? 1.4: 1.0;


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public ICommand TabletModeCommand { get; private set; }


#endregion

    }
}
