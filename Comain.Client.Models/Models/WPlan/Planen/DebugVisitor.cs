﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{
    public class DebugVisitor : IVisitor
    {

        public DebugVisitor()
        {
        }


        public void Visit(Plan model)
        {
            Console.WriteLine("Plan");
        }


        public void Visit(Planspaltensumme model)
        {
            Console.WriteLine("Planspaltensumme");
        }


        public void Visit(PZPlanzeile model)
        {
            Console.WriteLine("PZPlanzeile " + model.Info);
        }


        public void Visit(PZSpaltensumme model)
        {
            Console.WriteLine("PZSpaltensumme");
        }


        public void Visit(IHPlanzeile model)
        {
            Console.WriteLine("IHPlanzeile " + model.Info);
        }


        public void Visit(IHSpaltensumme model)
        {
            Console.WriteLine("IHSpaltensumme");
        }


        public void Visit(MNPlanzeile model)
        {
            Console.WriteLine("MNPlanzeile " + model.Info);
        }


        public void Visit(Planzelle model)
        {
            Console.WriteLine("Planzelle");
        }


        public void Visit(Planaufgabe model)
        {
            Console.WriteLine("Planaufgabe " + model.Info);
        }


        public void VisitAfter(Plan model)
        {
            Console.WriteLine("End Plan");
        }


        public void VisitAfter(PZPlanzeile model)
        {
            Console.WriteLine("End PZPlanzeile " + model.Info);
        }


        public void VisitAfter(IHPlanzeile model)
        {
            Console.WriteLine("End IHPlanzeile " + model.Info);
        }
    }
}
