﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Converters;
using Comain.Client.Gateways;
using Comain.Client.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Comain.Client
{

    public partial class App : Application
    {

        private IHost host;


        public App()
        {

            Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));
            Serilog.Debugging.SelfLog.Enable(Console.Error);

            host = Host.CreateDefaultBuilder()
                       .ConfigureServices((context, services) => {

                           DependencyInjectionConfig.RegisterServices(context, services);
                           services.AddLogging();

                           var mapperConfig = new MapperConfiguration(mc => {
                               mc.AddProfile(new MappingProfile());
                           });

                           IMapper mapper = mapperConfig.CreateMapper();
                           services.AddSingleton(mapper);
                       })
                       .ConfigureAppConfiguration((context, bld) => {

                           bld.SetBasePath(context.HostingEnvironment.ContentRootPath)
                              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                              .AddEnvironmentVariables();

                       })
                       .ConfigureLogging((context, cfg) => {

                           cfg.AddSerilog(Log.Logger, dispose: true);
                           cfg.AddConsole();
                       })
                       .Build();
        }


        private async void Application_Startup(object sender, StartupEventArgs e)
        {

            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
            TaskScheduler.UnobservedTaskException += UnhandledExceptionHandler;

            try {

                await host.StartAsync();

                Serilog.Context.LogContext.PushProperty("application version", Assembly.GetEntryAssembly().GetName().Version.ToString());
                Serilog.Context.LogContext.PushProperty("runtime version", RuntimeInformation.FrameworkDescription);
        #if DEBUG
                Serilog.Context.LogContext.PushProperty("build configuration", "debug");
        #else
                Serilog.Context.LogContext.PushProperty("build configuration", "release");
        #endif

                Serilog.Log.Information($"Start comain.Client");

                CultureInfo cultureInfo = new ("de-DE");
                CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
                CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;


                var controller = host.Services.GetRequiredService<ApplicationController>();
                controller.Initialize();
                controller.Run();
            }
            catch (Exception x) {

                Debug.WriteLine(x.ToString());
                MessageBox.Show(x.ToString());
                Serilog.Log.Fatal(x, "Host terminated unexpectedly");
                Current.Shutdown();
            }
            finally {

                Serilog.Log.CloseAndFlush();
            }
        }


        private async void Application_Exit(object sender, ExitEventArgs e)
        {

            TaskScheduler.UnobservedTaskException -= UnhandledExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException -= UnhandledExceptionHandler;

            using (host) {

                await host.StopAsync(TimeSpan.FromSeconds(5));
            }
        }


        private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Exception ex) Serilog.Log.Error(ex.ToString());
        }


        private void UnhandledExceptionHandler(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception != null) Serilog.Log.Error(e.Exception.ToString());
        }
    }
}
