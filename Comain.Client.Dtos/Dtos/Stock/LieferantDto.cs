﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Dtos.Stock
{
    public class LieferantDto
    {

        public int        Id                      { get; set; }
        public String     Name1                   { get; set; }
        public String     Name2                   { get; set; }
        public String     Strasse                 { get; set; }
        public String     Plz                     { get; set; }
        public String     Ort                     { get; set; }
        public String     Telefon                 { get; set; }
        public String     Fax                     { get; set; }
        public String     EMail                   { get; set; }
        public String     Rechnungsanschrift      { get; set; }

        public IEnumerable<LieferantApDto>   Ansprechpartnerliste     { get; set; }
    }
}
