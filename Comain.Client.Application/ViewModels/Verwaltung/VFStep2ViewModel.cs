﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class VFStep2ViewModel : ViewModel<IVFStep2View>, IDelayableViewModel
    {

        public VFStep2ViewModel(IVFStep2View view) : base(view)
        {}


        public void Unload()
        {
            Liste = null;
        }


#region P R O P E R T I E S


        private DateTime monat;
        public DateTime Monat
        { 
            get { return monat; }
            set { if (SetProperty(ref monat, value)) RaisePropertyChanged("Title"); }
        }


        public String Title
        { 
            
            get { 
            
                if (monat.Year < 2000) return "Verfügbarkeit / Anlagenausfall von Assets";
                return String.Format("Verfügbarkeit / Anlagenausfall von Assets {0}", monat.ToString("MMMM yyyy"));
            }
        }


        private ObservableCollection<VfViewItem> liste;
        public ObservableCollection<VfViewItem> Liste
        { 
            get { return liste; }
            set { SetProperty(ref liste, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand copyCommand;
        public ICommand CopyCommand
        { 
            get { return copyCommand; }
            set { SetProperty(ref copyCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }

 
#endregion

    }
}
