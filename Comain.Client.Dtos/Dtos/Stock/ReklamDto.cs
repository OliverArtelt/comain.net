﻿using System;
using System.Collections.Generic;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class ReklamDto : ChangeTrackingBase
    {
       
        public          String              Notizen         { get; set; }
        public          DateTime?           Datum           { get; set; }
        public virtual  BestellDto          Bestellung      { get; set; }
        public virtual  LieferscheinDto     Lieferschein    { get; set; }
        
        public virtual  ICollection<ReklamposDto> Positionen { get; set; }
    }
}
