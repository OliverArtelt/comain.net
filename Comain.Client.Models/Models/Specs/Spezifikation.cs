﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;

namespace Comain.Client.Models.Specs
{

    public class Spezifikation : DocEntity, IDataErrorInfo
    {

        private String      baugruppenname;
        private String      bauteilname;
        private bool        fürAssets;
        private bool        fürArtikel;


        public String Baugruppenname
        {
            get => baugruppenname;
            set => SetProperty(ref baugruppenname, value);
        }


        public String Bauteilname
        {
            get => bauteilname;
            set => SetProperty(ref bauteilname, value);
        }


        public bool FürAssets
        {
            get => fürAssets;
            set => SetProperty(ref fürAssets, value);
        }


        public bool FürArtikel
        {
            get => fürArtikel;
            set => SetProperty(ref fürArtikel, value);
        }


        private TrackableCollection<SpecWert> wertschablonen;
        public TrackableCollection<SpecWert> Wertschablonen
        {
            get => wertschablonen ?? (wertschablonen = new TrackableCollection<SpecWert>());
            set => SetProperty(ref wertschablonen, value);
        }


        private TrackableCollection<SpecArtikel> vorgeseheneArtikelliste;
        public TrackableCollection<SpecArtikel> VorgeseheneArtikelliste
        {
            get => vorgeseheneArtikelliste ?? (vorgeseheneArtikelliste = new TrackableCollection<SpecArtikel>());
            set => SetProperty(ref vorgeseheneArtikelliste, value);
        }


        private TrackableCollection<SpecMassnahme> vorgeseheneMassnahmen;
        public TrackableCollection<SpecMassnahme> VorgeseheneMassnahmen
        {
            get => vorgeseheneMassnahmen ?? (vorgeseheneMassnahmen = new TrackableCollection<SpecMassnahme>());
            set => SetProperty(ref vorgeseheneMassnahmen, value);
        }


        public virtual Spezifikation Clone()
        {

            var clone = new Spezifikation {

                Baugruppenname = $"{Baugruppenname} [2]",
                Bauteilname = Bauteilname,
                FürAssets = FürAssets,
                FürArtikel = FürArtikel,
                Wertschablonen = new TrackableCollection<SpecWert>(Wertschablonen.Select(p => p.Clone())),
            };

            if (FürAssets) clone.VorgeseheneArtikelliste = new TrackableCollection<SpecArtikel>(VorgeseheneArtikelliste);
            if (FürAssets) clone.VorgeseheneMassnahmen = new TrackableCollection<SpecMassnahme>(VorgeseheneMassnahmen.Select(p => p.Clone()));

            clone.AcceptChanges();
            return clone;
        }


        public static Spezifikation Create()
        {

            var model = new Spezifikation { Baugruppenname = "Neue Spezifikation" };
            model.AcceptChanges();
            return model;
        }


        public override bool IsChanged => base.IsChanged || IstAngefügt ||
                                          Wertschablonen != null && Wertschablonen.IsChanged || 
                                          VorgeseheneArtikelliste != null && VorgeseheneArtikelliste.IsChanged || 
                                          VorgeseheneMassnahmen != null && VorgeseheneMassnahmen.IsChanged;


        public IList<String> LöschKonsequenzen()
        {

            var result = new List<String>();

            if (Wertschablonen.Count == 1) result.Add("Eine Wertschablone wird mit gelöscht.");
            else if (Wertschablonen.Count > 1) result.Add($"{Wertschablonen.Count} Wertschablonen werden mit gelöscht.");
            if (VorgeseheneMassnahmen.Count == 1) result.Add("Eine Massnahme wird mit gelöscht.");
            else if (VorgeseheneMassnahmen.Count > 1) result.Add($"{VorgeseheneMassnahmen.Count} Massnahmen werden mit gelöscht.");
            if (VorgeseheneArtikelliste.Count == 1) result.Add("Eine Artikelverknüpfung wird mit gelöscht.");
            else if (VorgeseheneArtikelliste.Count > 1) result.Add($"{VorgeseheneArtikelliste.Count} Artikelverknüpfungen werden mit gelöscht.");
            if (Dokumente.Count == 1) result.Add("Eine Dokumentverknüpfung wird mit gelöscht.");
            else if (Dokumente.Count > 1) result.Add($"{Dokumente.Count} Dokumentverknüpfungen werden mit gelöscht.");

            return result;
        }


        public override void AcceptChanges()
        {

            base.AcceptChanges();
            Wertschablonen.AcceptChanges();
            VorgeseheneArtikelliste.AcceptChanges();
            VorgeseheneMassnahmen.AcceptChanges();
        }


        public override String EntityType => "Spec";


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Baugruppenname)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Bauteilname)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(FürAssets)];
                return test;
            }
        }


        public List<String> Errorlist
            => new String[] { this[nameof(Baugruppenname)], this[nameof(Bauteilname)], this[nameof(FürAssets)] }
                            .Where(p => !String.IsNullOrWhiteSpace(p))
                            .ToList();


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                    case nameof(Baugruppenname):

                        if (String.IsNullOrWhiteSpace(Baugruppenname)) return "Geben Sie die Baugruppe an.";
                        if (Baugruppenname.Length > 400) return "Die Baugruppenbezeichnung darf aus maximal 400 Zeichen bestehen.";
                        break;

                    case nameof(Bauteilname):

                        if (String.IsNullOrWhiteSpace(Bauteilname)) return null;
                        if (Bauteilname.Length > 400) return "Die Bauteilbezeichnung darf aus maximal 400 Zeichen bestehen.";
                        break;

                    case nameof(FürAssets):

                        if (FürAssets == FürArtikel) return "Die Spezifikation muß entweder für Assets oder für Artikel eingerichtet werden.";
                        break;
                }

                return null;
            }
        }

#endregion

    }
}
