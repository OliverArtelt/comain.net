﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class ArtikelLieferantViewItem
    {


        public ArtikelLieferant Model { get; private set; }


        public String   Nummer          { get { return Model.Nummer; } }
        public String   Name            { get { return Model.Lieferant.Name1; } }
        public String   Ort             { get { return Model.Lieferant.Ort; } }
        public short?   Rank            { get { return Model.Rank; } }
        public decimal? Preis           { get { return Model.Preis; } }
        public int      LieferantId     { get { return Model.Lieferant.Id; } }


        public String Color 
        { 
        
            get {

                if (Rank.GetValueOrDefault() == 1) return "LimeGreen";
                if (Rank.GetValueOrDefault() == 2) return "Gold ";
                if (Rank.GetValueOrDefault() == 3) return "Coral";
                return "LightSlateGray";
            }
        } 


        public ArtikelLieferantViewItem(ArtikelLieferant al)
        {
            Model = al;   
        }
    }
}
