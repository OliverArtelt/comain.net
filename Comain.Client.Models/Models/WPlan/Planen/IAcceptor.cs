﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{
    public interface IAcceptor
    {
        void Accept(IVisitor v);
    }
}
