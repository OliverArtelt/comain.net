﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Comain.Client.Dtos.Dokumente
{

    public class KategorieDto
    {

        public int          Id               { get; set; }
        public String       Bezeichnung      { get; set; }
        public String       Typ              { get; set; }
    }
}
