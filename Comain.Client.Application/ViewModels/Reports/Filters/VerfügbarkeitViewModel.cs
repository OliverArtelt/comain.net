﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class VerfügbarkeitViewModel : ViewModel<IVerfügbarkeitView>, IFilterViewModel
    {

        public VerfügbarkeitViewModel(IVerfügbarkeitView view) : base(view)
        {}


        public void Reset()
        {
            TypVerfügbarkeit = true;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            return String.Format("Typ: {0}", Typ);
        }


        public void AddFilter(Suchkriterien filter)
        {
            filter.Add("IHVFTyp", Typ);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);            
        }


        public void Unload()
        {}


#region P R O P E R T I E S
     

        public String Typ   { get; private set; } = "Verfügbarkeit";
        

        public bool TypVerfügbarkeit
        { 
            get { return Typ == "Verfügbarkeit"; }
            set { 
            
                if (value && Typ != "Verfügbarkeit") {
                    
                    Typ = "Verfügbarkeit";
                    RaisePropertyChanged("TypVerfügbarkeit");
                    RaisePropertyChanged("TypAusfallrate");
                    RaisePropertyChanged("TypAbweichungsgrad");
                }
            }
        } 
        
        public bool TypAusfallrate
        { 
            get { return Typ == "Ausfallrate"; }
            set { 
            
                if (value && Typ != "Ausfallrate") {
                    
                    Typ = "Ausfallrate";
                    RaisePropertyChanged("TypVerfügbarkeit");
                    RaisePropertyChanged("TypAusfallrate");
                    RaisePropertyChanged("TypAbweichungsgrad");
                }
            }
        } 
        
        public bool TypAbweichungsgrad
        { 
            get { return Typ == "Abweichungsgrad"; }
            set { 
            
                if (value && Typ != "Abweichungsgrad") {
                    
                    Typ = "Abweichungsgrad";
                    RaisePropertyChanged("TypVerfügbarkeit");
                    RaisePropertyChanged("TypAusfallrate");
                    RaisePropertyChanged("TypAbweichungsgrad");
                }
            }
        } 
   

#endregion
    }
}
