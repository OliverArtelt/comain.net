﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.ViewModels;

namespace Comain.Client.Controllers.Dokumente
{

    public interface IDokumentAssignController : IController
    {

        object      View                            { get; }
        bool        EntityIsChanged                 { get; }

        void        SetEditMode(EditMode mode);
        void        UpdateEntityId(DocEntity entity);

        Task        LoadAsync();
        Task        CancelAsync();
        Task        DetailAsync(DocEntity entity);
        Task        SaveAsync();
        Task        SaveAsync(IEnumerable<DocEntity> entities);

        Task        FillThumbnailsAsync();
        Task<Media> TitelbildAsync();
    }
}
