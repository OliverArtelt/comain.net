﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Specs
{
    public enum Wertetyp
    {
        Text = 0, Numeric = 1, Boolean = 2, Enumeration = 3
    }
}
