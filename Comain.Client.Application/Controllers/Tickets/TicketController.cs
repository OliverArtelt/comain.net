﻿using Comain.Client.ViewData.Verwaltung;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;
using Comain.Client.Services.Tickets;
using Comain.Client.ViewModels.Tickets;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Microsoft.Win32;
using Comain.Client.ReportModels;
using System;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Controllers.Tickets
{

    public class TicketController : ControllerBase, ITicketController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ITicketService store;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly ITicketFormsMediator commands;
        private readonly MainViewModel mainView;
        private readonly TicketViewModel view;
        private readonly TicketListViewModel listView;
        private readonly TicketDetailViewModel detailView;
        private readonly TicketFilterViewModel filterView;
        private readonly TicketDetailBasisViewModel basisView;
        private readonly TicketDetailHistorieViewModel historieView;
        private readonly TicketDetailLeistungViewModel leistungView;
        private readonly TicketDetailPersonalViewModel personalView;
        private readonly TicketDetailAbnahmeViewModel abnahmeView;
        private readonly TicketDetailMediaViewModel mediaView;
        private readonly TicketDetailPrintViewModel printView;
        private readonly Suchkriterien meineTicketsFilter;
        private bool nurMeineTickets;
        private IDringlichkeitenProvider drlist;


        public TicketController(IBreadcrumbService crumbs, ITicketService store, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                IKonfigService configs, IControllerBus bus, ITicketFormsMediator commands,
                                MainViewModel mainView, TicketViewModel view, TicketListViewModel listView, TicketDetailViewModel detailView,
                                TicketFilterViewModel filterView, TicketDetailBasisViewModel basisView, TicketDetailHistorieViewModel historieView,
                                TicketDetailAbnahmeViewModel abnahmeView, TicketDetailPersonalViewModel personalView, TicketDetailLeistungViewModel leistungView,
                                TicketDetailMediaViewModel mediaView, TicketDetailPrintViewModel printView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.store = store;
            this.login = login;
            this.flyouts = flyouts;
            this.configs = configs;
            this.commands = commands;

            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.detailView = detailView;
            this.filterView = filterView;
            this.basisView = basisView;
            this.historieView = historieView;
            this.abnahmeView = abnahmeView;
            this.personalView = personalView;
            this.leistungView = leistungView;
            this.mediaView = mediaView;
            this.printView = printView;

            meineTicketsFilter = new Suchkriterien();
            meineTicketsFilter.Add("MeineTickets", true);
        }


        public void Initialize()
        {

            view.FilterView = filterView.View;
            view.ListView = listView.View;
            view.DetailView = detailView.View;
            detailView.BasisView = basisView.View;
            detailView.AbnahmeView = abnahmeView.View;
            detailView.HistorieView = historieView.View;
            detailView.PersonalView = personalView.View;
            detailView.LeistungView = leistungView.View;
            detailView.MediaView = mediaView.View;
            detailView.PrintView = printView.View;

            filterView.SearchCommand = CreateCommand(_ => SearchAsync(false));
            filterView.MeineTicketsCommand = CreateCommand(_ => SearchAsync(true));
            listView.DetailCommand = CreateCommand(_ => DetailAsync());

            view.BackCommand      = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand      = CreateCommand(_ => SaveAsync());
            view.EditCommand      = CreateCommand(_ => EditAsync());
            view.CancelCommand    = CreateCommand(_ => CancelAsync());
            view.ReloadCommand    = CreateCommand(_ => LoadAsync());
            view.AddCommand       = CreateCommand(_ => AddAsync());
            view.DeleteCommand    = CreateCommand(_ => DeleteAsync());
            view.CloseCommand     = CreateCommand(_ => AbschließenAsync());
            view.StornoCommand    = CreateCommand(_ => StornoAsync());
            view.OpenImageCommand = CreateCommand(_ => OpenImageAsync());
            view.SaveImageCommand = CreateCommand(_ => SaveImageAsync());

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            nurMeineTickets = false;

            if (login.AktuellerNutzer.IstTicketInstandhalter || login.AktuellerNutzer.IstTicketSchichtführer) {

                filterView.MeineTicketsText = "Meine Tickets";
                nurMeineTickets = true;

            } else if (login.AktuellerNutzer.IstTicketLeiterInstandhaltung) {

                filterView.MeineTicketsText = "Nicht zugeordnete Tickets";
                nurMeineTickets = true;

            } else {

                filterView.MeineTicketsText = null;
            }

            crumbs.Add("Ticketübersicht");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            commands.NeuSetzen();
            filterView.ResetSearch();

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
        }


        private async Task LoadAsync()
        {

            var oldlistmodel = listView.Current;
            commands.SetEditMode(EditMode.ReadOnly);
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                var pstask      = store.FilterPersonalAsync(bus.Token);
                var pztask      = store.FilterStandorteAsync(bus.Token);
                var gwtask      = store.FilterGewerkeAsync(bus.Token);
                var ksttask     = store.FilterKostenstellenAsync(bus.Token);
                var sbtask      = store.FilterSchadensbilderAsync(bus.Token);
                var sutask      = store.FilterSchadensursachenAsync(bus.Token);
                var ikstask     = store.FilterAuIHKSAsync(bus.Token);
                var drtask      = store.FilterDringlichkeitAsync(bus.Token);
                var cfgtask     = configs.GetAsync(bus.Token);
                var satask      = LoadListAsync();

                await Task.WhenAll(satask, pstask, pztask, gwtask, ksttask, sbtask, sutask, ikstask, drtask, cfgtask).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                drlist = drtask.Result;
                await basisView.SetListsAsync(ikstask.Result, pztask.Result, ksttask.Result, sbtask.Result, sutask.Result, bus.Token).ConfigureAwait(false);
                await personalView.SetzeListenAsync(pstask.Result, gwtask.Result);

                if (oldlistmodel != null) {

                    listView.Current = listView.Foundlist.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                    await DetailAsync();
                }
            }
        }


        private async Task LoadListAsync()
        {

            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                listView.Current = null;
                SetModel(null);

                var filter = nurMeineTickets? meineTicketsFilter: filterView.GebeFilter();
                var list = await store.SearchAsync(filter, bus.Token).ConfigureAwait(false);
                if (list == null) return;
                bus.Token.ThrowIfCancellationRequested();

                await context.SendAsync(new SendOrPostCallback((o) => {

                    listView.Foundlist = new ObservableCollection<TicketListViewItem>(list.Select(p => new TicketListViewItem(p)).ToList());

                    var bld = new StringBuilder();
                    if (list.Count == 0) bld.Append("FILTER: Keine Aufträge gefunden");
                    else if (list.Count == 1) bld.Append("FILTER: Ein Auftrag gefunden");
                    else bld.Append("FILTER: ").Append(list.Count.ToString("N0")).Append(" Aufträge gefunden");

                    filterView.ResultText = bld.ToString();

                }), null);
            }
        }


        private async Task LoadMediaAsync()
        {

            mediaView.Medien = null;
            if (detailView.Current == null || detailView.Current.IstAngefügt || detailView.Current.Typ != Auftragstyp.BesonderesVorkommnis) return;

            using (new VisualDelay(mediaView)) {

                var media = await store.MediaAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
                await OnUIThreadAsync(() => {

                    if (media.IsNullOrEmpty()) mediaView.Medien = new ObservableCollection<Media>();
                    else mediaView.Medien = new ObservableCollection<Media>(media);
                });
            }
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (detailView.Current != null && detailView.Current.IsChanged) {

                if (!await messages.ConfirmExitAsync()) return false;
            }

            listView.Unload();
            filterView.Unload();
            view.Unload();
            basisView.Unload();
            historieView.Unload();
            leistungView.Unload();
            personalView.Unload();
            mediaView.Unload();
            printView.Unload();
            return true;
        }


        private async Task DetailAsync()
        {

            if (listView.Current != null) {

                using (new VisualDelay(detailView)) {

                    var model = await store.DetailAsync(listView.Current.Id);
                    if (model == null) throw new ComainOperationException("Der Auftrag wurde nicht gefunden.");

                    await context.SendAsync(new SendOrPostCallback((o) => { basisView.Dringlichkeiten = drlist.Liste(model.Typ); }), null);
                    model.Dringlichkeit = drlist[model.Dringlichkeit?.Id];
                    model.AcceptChanges();

                    SetModel(model);
                }

                await LoadMediaAsync();

            } else {

                SetModel(null);
            }
        }


        private async Task SaveAsync()
        {

            if (detailView.Current == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (detailView.Current.IsChanged) {

                if (detailView.Current.Errors.Count > 0) {

                    await messages.ShowMessageAsync(new MessageToken { Header = "Der Auftrag kann nicht gespeichert werden.", Reasons = detailView.Current.Errors });
                    return;
                }

                using (new VisualDelay(detailView)) {

                    (var model, var sendefehler) = await store.UpdateAsync(detailView.Current.AsDto(), bus.Token);
                    commands.SetEditMode(EditMode.ReadOnly);
                    await LoadListAsync();
                    model.Dringlichkeit = drlist[model.Dringlichkeit?.Id];

                    model.AcceptChanges();
                    SetModel(model);
                    if (!String.IsNullOrEmpty(sendefehler)) {

                        await messages.ShowMessageAsync(new MessageToken { Header = "Das Ticket wurde gespeichert. Bei der Benachrichtigung des Auftragleiters ist jedoch ein Fehler aufgetreten.",
                                                                           Reasons = new String[] { sendefehler } });
                    }
                }
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task SearchAsync(bool nurMeineTickets)
        {

            this.nurMeineTickets = nurMeineTickets;
            await LoadListAsync();
        }


        private Task EditAsync()
        {

            if (detailView.Current == null || detailView.Current.Auftragstatus == Auftragstatus.Abgeschlossen || detailView.Current.Auftragstatus == Auftragstatus.Storno)
                return Task.FromResult(true);

            commands.SetEditMode(EditMode.Edit);
            UpdateCommands();

            return Task.FromResult(true);
        }


        private async Task CancelAsync()
        {

            await DetailAsync();
            commands.SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            var model = detailView.Current;
            if (model == null) return;
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich gelöscht werden?" })) return;

            using (new VisualDelay(detailView)) {

                await store.DeleteAsync(detailView.Current, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);

                var listvm = listView.Foundlist.FirstOrDefault(p => p.Id == model.Id);
                if (listvm != null) await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist.Remove(listvm); }), null);

                await LoadListAsync();
                SetModel(null);
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task StornoAsync()
        {

            if (detailView.Current == null) return;
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich storniert werden?" })) return;

            using (new VisualDelay(detailView)) {

                (var model, var sendefehler) = await store.StornoAsync(detailView.Current, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                await LoadListAsync();
                SetModel(model);
                if (!String.IsNullOrEmpty(sendefehler)) {

                    await messages.ShowMessageAsync(new MessageToken { Header = "Das Ticket wurde storniert. Bei der Benachrichtigung des Auftragleiters ist jedoch ein Fehler aufgetreten.",
                                                                       Reasons = new String[] { sendefehler } });
                }
            }
        }


        private async Task AbschließenAsync()
        {

            if (detailView.Current == null) return;
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Auftrag wirklich abgeschlossen werden?" })) return;

            using (new VisualDelay(detailView)) {

                (var model, var sendefehler) = await store.CloseAsync(detailView.Current, bus.Token);
                commands.SetEditMode(EditMode.ReadOnly);
                await LoadListAsync();
                SetModel(model);
                if (!String.IsNullOrEmpty(sendefehler)) {

                    await messages.ShowMessageAsync(new MessageToken { Header = "Das Ticket wurde abgeschlossen. Bei der Benachrichtigung des Auftragleiters ist jedoch ein Fehler aufgetreten.",
                                                                       Reasons = new String[] { sendefehler } });
                }
            }
        }


        private async Task AddAsync()
        {

            await bus.JumpAsync(new ControllerMessage { Destination = typeof(IAddTicketController), Source = this });
            commands.SetEditMode(EditMode.Append);
        }


        private async Task OpenImageAsync()
        {

            if (mediaView.CurrentMedia == null) return;

            await mediaView.CurrentMedia.StoreTempFileAsync(bus.Token).ConfigureAwait(false);
            System.Diagnostics.Process.Start(mediaView.CurrentMedia.TempPath);
        }


        private async Task SaveImageAsync()
        {

            if (mediaView.CurrentMedia == null) return;

            var dialog = new SaveFileDialog();
            dialog.Filter = "JPEG file (*.jpg)|*.jpg";
            dialog.DefaultExt = "jpg";
            dialog.FileName = mediaView.CurrentMedia.Dateiname;
            dialog.Title = "Speicherort wählen";
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileName.IsNullOrEmpty()) return;

            await mediaView.CurrentMedia.StoreFileAsync(dialog.FileName, bus.Token).ConfigureAwait(false);
        }


        private async Task UpdatePrintViewAsync()
        {

            if (detailView.Current == null) return;
            printView.Unload();

            using (new VisualDelay(printView)) {

                var title = $"Ticket {detailView.Current.Nummer}";

                var address = "Comain.Client.Reports.TicketCreatedReport.rdlc";
                var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = title };
                var ihpath = basisView.Baugruppen;
                report.AddDataSource("MainData", new List<TicketPrintRow> { new TicketPrintRow(detailView.Current, ihpath, false) });
                report.AddParameter("parTitle", title);
                report.AddParameter("parPrice", "0");
                report.HasLogo = true;

                bus.Token.ThrowIfCancellationRequested();

                await printView.BindViewerAsync(report);
            }
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (args.PropertyName == nameof(detailView.CurrentTab)) {

                    if (detailView.Tab == TicketDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
                    UpdateCommands();
                }

                if (args.PropertyName == nameof(detailView.Current)) {

                    if (detailView.Tab == TicketDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private void UpdateCommands()
        {
            commands.NeuSetzen();
        }


        private void SetModel(Ticket current)
        {

            basisView.Current = current;
            detailView.Current = current;
            personalView.Current = current;
            leistungView.Current = current;
            historieView.Current = current;
            abnahmeView.Current = current;

            if (current != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null) listviewitem.Model = current;
            }

            UpdateCommands();
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }
    }
}
