﻿using System;
using System.Linq;
using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Models.Stock;
using Comain.Client.Models.Materialien;

namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class ArtikelBSAddPickItem : NotifyBase
    {
       
        private readonly Artikel at;


        public ArtikelBSAddPickItem(Artikel mt)
        {
            
            this.at = mt;
            if (mt.MaterialLieferantMap.IsNullOrEmpty()) Lieferanten = new List<ArtikelLieferantViewItem>();
            else Lieferanten = mt.MaterialLieferantMap.Select(p => new ArtikelLieferantViewItem(p)).OrderBy(p => p.Rank).ThenBy(p => p.Name).ToList();
        }


        private bool isSelected;
        public bool IsSelected
        { 
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }


        public Artikel  Model               { get { return at; } } 
        public String   Nummer              { get { return at.Nummer; } } 
        public String   Name                { get { return at.Name; } } 
        public String   Einheit             { get { return at.Einheit?.Nummer; } } 
        public decimal? Mindestbestand      { get { return at.Mindestbestand; } }
        public decimal? Mindestbestellmenge { get { return at.Mindestbestellmenge; } } 
        /// <summary>
        /// realer Lagerbestand, Berücksichtigung von Reservierungen fehlt
        /// </summary>
        public decimal? Lagerbestand        { get { return at.BestandReal; } } 
        /// <summary>
        /// bereits bestellte Menge
        /// </summary>
        public decimal? Bestellmenge        { get { return at.Bestellmenge; } } 
        /// <summary>
        /// zu bestellende Menge
        /// </summary>
        public decimal? OffeneMenge         { get { return at.OffeneMenge; } } 
        /// <summary>
        /// Mischpreis wenn kein Lieferant gewählt wurde, sonst EK Lieferant
        /// </summary>
        public decimal? Einzelpreis         { get { return at.Mischpreis; } } 
        /// <summary>
        /// Mischpreis * OffeneMenge wenn kein Lieferant gewählt wurde, sonst EK Lieferant * OffeneMenge
        /// </summary>
        public decimal? Gesamtpreis         { get { return at.Mischpreis * at.OffeneMenge; } } 


        public List<ArtikelLieferantViewItem> Lieferanten { get; private set; }


        public Bestellposition AsBestellposition(Lieferant lf)
        {

            var bp = new Bestellposition();
            var lfbs = Lieferanten.FirstOrDefault(p => p.LieferantId == lf.Id);

            bp.Artikelname = Name;
            bp.Artikelnummer = Nummer;
            if (lfbs != null) bp.LfArtikelnummer = lfbs.Nummer;
            bp.Material_Id = Model.Id;
            bp.Menge = Math.Max(Math.Max(Mindestbestellmenge.GetValueOrDefault(), OffeneMenge.GetValueOrDefault()), 1);
            bp.Preis = lfbs == null? at.Mischpreis: lfbs.Preis;
            bp.Einheit = Einheit;

            return bp;
        }


        public String        Color   { get { return at.StatusAsColor; } }
        public ArtikelStatus Status  { get { return at.Status; } }


        public bool Matches(IEnumerable<String> searchterms)
        {

            if (searchterms.IsNullOrEmpty()) return true;
            return searchterms.All(p => Matches(p));
        }


        public bool Matches(String searchText)
        {

            if (at?.Name != null && at.Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (at?.Nummer != null && at.Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (at?.Beschreibung != null && at.Beschreibung.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (at?.Herstellernummer != null && at.Herstellernummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            
            if (!at.MaterialLieferantMap.IsNullOrEmpty() && 
                at.MaterialLieferantMap.Any(p => p.Name != null && p.Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase) ||
                                                 p.Nummer != null && p.Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))) return true;
            return false;
        }
    }
}
