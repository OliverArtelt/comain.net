﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Stock.Wareneingänge;

namespace Comain.Client.Views.Stock.Wareneingänge
{
    /// <summary>
    /// Interaction logic for EingangListView.xaml
    /// </summary>
    public partial class EingangListView:UserControl ,IEingangListView
    {

        public EingangListView()
        {
        InitializeComponent();
        }
 

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as EingangListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
