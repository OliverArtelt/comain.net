﻿using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Stock;
using Comain.Client.Views.Stock.Reklamationen;

namespace Comain.Client.ViewModels.Stock.Reklamationen
{

    public class ReklamListViewModel : ViewModel<IReklamListView>
    {
 

        public ReklamListViewModel(IReklamListView view) : base(view)
        {
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
 
        
        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }   
            set { SetProperty(ref detailCommand, value); }
        }


        private ObservableCollection<RKListDto> foundlist;
        public ObservableCollection<RKListDto> Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private RKListDto current;
        public RKListDto Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
