﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Tickets;
using Comain.Client.Services.Tickets;

namespace Comain.Client.Commands
{

    public class TicketCommand : NotifyBase, IControllerCommand 
    {

        private readonly ITicketService store;


        public TicketCommand(ITicketService store)
        {
            this.store = store;
        }


        public Type         Controller  => typeof(ITicketController);
        public String       Name        => "Ticketübersicht"; 
        public String       Parameter   => "Tickets"; 
        public String       Gruppe      => "Ticket";
        public int          Sortierung  => 1000; 
        public String       Color       => "DarkMagenta"; 
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.ticket.png";
        public String       LiveValue   { get; private set; }
        public String       LiveText    { get; private set; }
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null || !module.VerwendetModulTicket) return false;
            return nutzer.IstAdministrator || nutzer.IstTicketNutzer;    
        }


        public async Task UpdateAsync(CancellationToken ct = default)
        {
            
            LiveValue = null;   
            LiveText = null;

            var meineTickets = await store.MeineTicketsAsync(ct);
            if (meineTickets.Summe > 0) {

                LiveValue = meineTickets.Wert;  
                LiveText = meineTickets.Text;   
            }

            RaisePropertyChanged("LiveValue");
            RaisePropertyChanged("LiveText");
        }
    }
}
