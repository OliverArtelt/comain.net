﻿using System;
using System.ComponentModel;


namespace Comain.Client.Models.IHObjekte
{

    public class Objektart : Entity, IDataErrorInfo, IComparable<Objektart>
    {

        private short nummer;
        private String name;
        private int haupt_Id;


#region P R O P E R T I E S


        public short Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public int Haupt_Id
        {
            get { return haupt_Id; }
            set { SetProperty(ref haupt_Id, value); }
        }


#endregion


        public int CompareTo(Objektart other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            var cmp = this.Haupt_Id.CompareTo(other.Haupt_Id);
            if (cmp != 0) return cmp;
            return this.Nummer.CompareTo(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung der Objektart an.";
                    if (Name.Length > 50) return "Die Bezeichnung der Objektart darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Nummer":

                    if (Nummer < 1 || Nummer > 99) return "Die Nummer der Objektart muss zwischen 1 und 99 liegen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
