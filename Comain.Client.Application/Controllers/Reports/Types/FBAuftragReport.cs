﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class FBAuftragReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly ILoginService login;
        private readonly FremdbudgetViewModel fremdView;
        private readonly LbAuViewModel lbView;


        public FBAuftragReport(IReportService store, ILoginService login,
                               FremdbudgetViewModel fremdView, ZeitViewModel zeitView, KostenstelleViewModel kstView, 
                               StandortViewModel pzView, IntAuStelleViewModel intView, LbAuViewModel lbView)
        {
            
            this.store = store;
            this.login = login;
            this.fremdView = fremdView;
            this.lbView = lbView;
            filterViews = new List<IFilterViewModel> { fremdView, lbView, zeitView, kstView, pzView, intView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await GetData(ct));
            result.AddParameter("parSubTyp", fremdView.FilterAlsString());
            result.AddParameter("parShowMwst", lbView.Mwst? "TRUE": "FALSE");
            return result;
        }
 

        private async Task<IEnumerable> GetData(CancellationToken ct)
        {

            var dtolist = await store.FbListAsync(GebeFilter(), ct);
            if (!lbView.TypPersonal) return dtolist;

            var result = new List<LbAuPsRow>();

            foreach (var au in dtolist) {

                result.Add(new LbAuPsRow(au));
                if (!au.Leistungen.IsNullOrEmpty()) au.Leistungen.ForEach(ps => result.Add(new LbAuPsRow(au, ps)));
            }

            return result.OrderBy(p => p.Nummer).ThenBy(p => p.Beginn).ToList();
        }


        public override Task DisplayAsync(CancellationToken ct = default)
        {
            
            lbView.ShowPersonal = !login.AktuellerNutzer.IstProduktion;
            return Task.FromResult(true);
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "FB-Aufträge"; }
        }


        public string Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 1100; }
        }


        public override string ReportAddress
        {
            
            get { 
            
                if (lbView.TypPersonal)     return "Comain.Client.Reports.FBAuftrag4Report.rdlc"; 
                if (lbView.TypIHObjekt)     return "Comain.Client.Reports.FBAuftrag3Report.rdlc"; 
                if (lbView.TypBeschreibung) return "Comain.Client.Reports.FBAuftrag2Report.rdlc"; 
               
                return "Comain.Client.Reports.FBAuftrag1Report.rdlc"; 
            }
        }
    }
}
