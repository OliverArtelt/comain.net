﻿using System;
using System.ComponentModel;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Validators
{

    public abstract class AuftragValidatorBase : IDataErrorInfo
    {

        protected readonly Auftrag model;


        public AuftragValidatorBase(Auftrag model)
        {
            this.model = model;
        }


        public string Error
        {

            get {

                String test = this["ExterneNummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["FBNummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Störzeit"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["ÜbergabeDatum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Leistungsart"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Auftraggeber"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["InterneAuStelle"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "ExterneNummer":

                    if (String.IsNullOrWhiteSpace(model.ExterneNummer)) return null;
                    if (model.ExterneNummer.Length > 20) return "Die externe Nummer darf aus maximal 20 Zeichen bestehen.";
                    break;

                case "Kurzbeschreibung":

                    if (String.IsNullOrWhiteSpace(model.Kurzbeschreibung)) return "Geben Sie die Kurzbeschreibung an.";
                    break;

                case "FBNummer":

                    if (String.IsNullOrWhiteSpace(model.FBNummer)) return null;
                    if (model.FBNummer.Length > 50) return "Die FB-Nummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Störzeit":

                    if (!model.Störzeit.HasValue) return null;
                    if (model.Störzeit.Value < 0) return "Die angegebene Störzeit darf nicht negativ sein.";
                    break;

                case "GemeldetDatum":

                    if (model.GemeldetDatum.HasValue != model.GemeldetZeit.HasValue)
                        return "Wenn der Auftrag gemeldet werden soll muß sowohl das Meldedatum als auch die Meldezeit angegeben werden.";
                    break;


                case "ÜbergabeDatum":

                    int? min = model.StörzeitInMinuten();
                    if (min.HasValue && min.Value < 0) return "Die Melde- und Übergabedaten ungültig. Der Auftrag kann nicht übergeben werden bevor er gemeldet wurde.";
                    if (model.ÜbergabeDatum.HasValue != model.ÜbergabeZeit.HasValue)
                        return "Wenn der Auftrag übergeben werden soll muß sowohl das Übergabedatum als auch die Übergabezeit angegeben werden.";
                    break;

                case "Leistungsart":

                    if (model.Leistungsart == null) return "Der Auftrag muss einer Leistungsart zugeordnet worden sein.";
                    break;

                case "AuIHKS":

                    if (model.AuIHKS == null) return "Der Auftrag muss einem Asset zugeordnet worden sein.";
                    break;

                case "Auftraggeber":

                    if (model.Konfiguration != null && model.Konfiguration.AuftraggeberImAuftragPflicht && model.Auftraggeber == null) return "Der Auftrag muss einen Auftraggeber besitzen.";
                    break;

                case "InterneAuStelle":

                    if (model.Konfiguration != null && model.Konfiguration.InterneAuStelleImAuftragPflicht && model.InterneAuStelle == null) return "Der Auftrag muss eine interne Auftragsstelle besitzen.";
                    if (model.InterneAuStelle.HasValue && (model.InterneAuStelle.Value < 1000000 || model.InterneAuStelle.Value > 9999999))
                        return "Die interne Auftragsstelle muss eine siebenstellige natürliche Zahl sein.";
                    break;
                }


                return null;
            }
        }
    }
}
