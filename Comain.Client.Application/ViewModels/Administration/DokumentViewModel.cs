﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Administration
{

    public class DokumentViewModel : ValidatableViewModel<IDokumentView, Konfiguration>, IConfigViewModel
    {

        public DokumentViewModel(IDokumentView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(DokumentReduce));
            RaisePropertyChanged(nameof(DokumentSingle));
        }



        public void Unload()
        {
        }


#region P R O P E R T I E S


        public bool DokumentReduce
        {
            get { return Current == null? false: Current.DokumentReduce; }
            set {

                if (Current != null && Current.DokumentReduce != value) {

                    Current.DokumentReduce = value;
                    RaisePropertyChanged(nameof(DokumentReduce));
                    RaisePropertyChanged(nameof(DokumentSingle));
                }
            }
        }


        public bool DokumentSingle
        {
            get { return Current == null? false: !Current.DokumentReduce; }
            set {

                if (Current != null && Current.DokumentReduce == value) {

                    Current.DokumentReduce = !value;
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
