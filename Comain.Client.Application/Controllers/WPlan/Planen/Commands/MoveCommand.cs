﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Trackers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class MoveCommand : IUndoRedoCommand, IResultCommand
    {

        private readonly IMapper mapper;
        private readonly Planaufgabe traveller;
        private readonly Planzelle   start;
        private readonly Planzelle   destination;
        private readonly DateTime    originalDate;
        private readonly bool        needHeightUpdate;


        public MoveCommand(Planaufgabe traveller, Planzelle ziel, IMapper mapper)
        {

            if (!traveller.DurchgeführtAm.HasValue && !traveller.FälligAm.HasValue) throw new ComainOperationException("Diese Position besitzt kein Datum.");

            this.traveller = traveller;
            this.start = traveller.Parent;
            this.destination = ziel;
            originalDate = traveller.DisplayDate.Value;
            needHeightUpdate = start.Aufgaben.Count - 1 != destination.Aufgaben.Count;
            this.mapper = mapper;
        }


        public void Redo()
        {

            start.RemovePosition(traveller);
            destination.AddPosition(traveller);
            traveller.Parent = destination;
            traveller.SetDate(destination.Begin);
            if (needHeightUpdate) traveller.Plan.CalculateHeights();
        }


        public void Undo()
        {

            destination.Aufgaben.Remove(traveller);
            start.AddPosition(traveller);
            traveller.Parent = start;
            traveller.SetDate(originalDate);
            if (needHeightUpdate) traveller.Plan.CalculateHeights();
        }


        public bool ModelHasChanged => true;


        public IEnumerable<PositionSubmitDto> Modifications()
        {

            var dto = mapper.Map<PositionSubmitDto>(traveller);
            dto.ChangeStatus = ChangeStatus.Changed;
            yield return dto;
        }
    }
}
