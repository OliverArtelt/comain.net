﻿using System;
using System.Collections.Generic;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class EingangDto : ChangeTrackingBase
    {

        public int?                 Position                { get; set; }
        public decimal              Liefermenge             { get; set; }
        public decimal?             MengeOK                 { get; set; }
        public String               Notizen                 { get; set; }
        public bool?                MengeGeprüft            { get; set; }
        public bool?                QualitätGeprüft         { get; set; }

        public int                  Bestellposition_Id      { get; set; }
        public int?                 Material_Id             { get; set; }
        public int                  Lieferschein_Id         { get; set; }

        public ArtikelDto           Material                { get; set; }
        public BestellInfoDto       Bestellposition         { get; set; }
        public LieferscheinDto      Lieferschein            { get; set; }
        public List<WLInfoDto>      Einlagerungen           { get; set; }
        public List<ReklamposDto>   Reklampositionen        { get; set; }
    }
}
