﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData.Verwaltung
{

    public class NummerItem : NotifyBase, IDataErrorInfo, IComparable<NummerItem>
    {

        private String name;
        private short nummer;


#region P R O P E R T I E S


        public int   Id    { get; set; }
        
        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        
        public short Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


#endregion

        
        public int CompareTo(NummerItem other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareTo(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    break;

                case "Nummer":

                    if (Nummer < 0 || Nummer > 9) return "Geben Sie eine gültige Nummer an.";
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
