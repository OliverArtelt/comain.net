﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface ILeistungsartController : IVwTypController 
    {}
    
    
    public class LeistungsartController : SimpleController<LeistungsartDto, Leistungsart, ILeistungsartView>, ILeistungsartController
    {
        
        public LeistungsartController(IMessageService messages, IControllerBus bus,
                                      ISimpleService<LeistungsartDto, Leistungsart> store, VerwaltungViewModel mainView, LeistungsartViewModel view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/LA"; }
        }
    }
}
