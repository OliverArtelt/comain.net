﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public interface IReport
    {

        String                  Title           { get; }
        String                  Gruppe          { get; }
        int                     Sortierung      { get; }
        String                  ReportAddress   { get; }
        IList<IFilterViewModel> FilterViews     { get; }

        Task                    DisplayAsync(CancellationToken ct = default);
        Task<ReportResult>      CreateAsync(CancellationToken ct = default);
        Task<ReportResult>      CreateDetailAsync(String report, List<Tuple<String, String>> parameters, CancellationToken ct = default);

        IEnumerable<String>     Validiere();
        bool                    HatZugriff(ModulInfo module, Nutzer user);
    }
}
