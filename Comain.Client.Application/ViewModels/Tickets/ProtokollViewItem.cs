﻿using System;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Tickets;

namespace Comain.Client.ViewModels.Tickets
{

    public class ProtokollViewItem
    {

        public Ticketstatus Status          { get; }
        /// <remarks>
        /// Sortierspalte
        /// </remarks>
        public DateTime     Beginn          { get; }

        public DateTime     Datum           { get; }
        public String       Uhrzeit         { get; }

        public String       Ersteller       { get; }
        public String       Zielperson      { get; }
        public String       Bemerkungen     { get; }
        public String       IstSauber       { get; }


        public ProtokollViewItem(Ticketprotokoll model)
        {

            Ersteller       = model.Ersteller?.Name;  
            Zielperson      = model.Zielperson?.Name;  
            Datum           = model.ErstelltAm.Date;
            Uhrzeit         = model.ErstelltAm.TimeOfDay.ToString("hh\\:mm");
            Bemerkungen     = model.Bemerkungen;
            Status          = model.Status;
            Beginn          = model.ErstelltAm;
            if (model.Status == Ticketstatus.Abgenommen) IstSauber = model.IstSauber? "Ja": "Nein";
        }


        public ProtokollViewItem(Personalleistung model)
        {

            Ersteller       = model.Personal.Name;  
            Datum           = model.BeginnDatum.Value;
            Bemerkungen     = model.Kurzbeschreibung;
            Uhrzeit         = String.Format(model.BeginnZeit.Value.ToString("hh\\:mm") + " - " + model.EndeZeit.Value.ToString("hh\\:mm"));
            Status          = Ticketstatus.LeistungErfasst;
            Beginn          = model.BeginnDatum.GetValueOrDefault().Add(model.BeginnZeit.GetValueOrDefault());
        }


        public String StatusAsText { get { return Status.StatusAsText(); } }
    }
}
