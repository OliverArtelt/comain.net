﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.Aufträge;
using System.Diagnostics;
using Comain.Client.ReportModels.WPlan;
using System.Windows.Media;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Eine Maßnahme mit ihren Positionen
    /// </summary>
    [DebuggerDisplay("{NameNummer}")]
    public class MNPlanzeile : NotifyBase, IPlanzeile, IAcceptor
    {

        private readonly MassnahmeDto mn;


        public MNPlanzeile(IHPlanzeile parent, MassnahmeDto mn)
        {

            this.mn = mn;
            Parent = parent;
            Perioden = Plan.Spalten.Select(p => new Planzelle(parent:this, p)).ToList();
            MaterialInfo = mn.MaterialInfo?.Select(p => new MaterialInfo(p)).ToList();
            mn.Positionen?.Select(p => new { Aufgabe = p, SpaltenIndex = Plan.DatumToSpaltenIndex(p.FälligAm ?? p.DurchgeführtAm) })
                          .Where(p => p.SpaltenIndex.HasValue)
                          .ForEach(p => { Perioden[p.SpaltenIndex.Value].AddPosition(new Planaufgabe(Perioden[p.SpaltenIndex.Value], p.Aufgabe)); });
        }


        public double Height
        {
            get {

                if (!IstSichtbar) return 0;
                if (!LeereZeileAnzeigen && MaxAmount == 0) return 0;
                return Plan.Aufgabenhöhe * Math.Max(1, MaxAmount) + Plan.Aufgabenrand * 2;
            }
        }

        public double                   YStartposition          { get; set; }


        public Plan                     Plan                    => Parent.Parent.Parent;
        public int                      Id                      => mn.Id;
        public IHPlanzeile              Parent                  { get; }
        public List<Planzelle>          Perioden                { get; }
        public List<MaterialInfo>       MaterialInfo            { get; }


        public bool                     AllowDrop               => true;
        public bool                     ExtendedTooltip         => true;
        public bool                     IstSichtbar             => LeereZeileAnzeigen || Perioden.Any(p => p.IstSichtbar);
        public bool                     IstDeaktiviert          => mn.IstInaktiv || Parent.IstDeaktiviert;
        public String                   DeaktiviertText         => mn.IstInaktiv? "Maßnahme wurde deaktiviert": Parent.IstDeaktiviert? "Asset wurde deaktiviert": null;
        public int                      MaxAmount               => Perioden.Max(p => p.AnzahlAufgaben);
        public Brush                    TypAsFarbe              => Zeilenfarbenindex % 2 == 0? Brushes.White: Brushes.WhiteSmoke;
        public double                   XStartposition          => 40d;

        public String                   AnzeigeName             => String.IsNullOrWhiteSpace(mn.Gewerk)? mn.Name: $"{mn.Gewerk}: {mn.Name}";
        public String                   ZyklusAsString          => WiMassnahme.ZyklusAsString((Termintyp)mn.Termintyp, mn.Zyklus, (Weekday)mn.Tagesmuster);
        public Termintyp                Termintyp               => (Termintyp)mn.Termintyp;
        public String                   TermintypText           => Termintyp.AsText();

        public String                   IHObjekt                => Parent.Nummer;
        public String                   Maschine                => Parent.Name;
        public String                   Nummer                  => mn.Nummer;
        public String                   Name                    => mn.Name;
        public String                   NameNummer              => String.IsNullOrEmpty(Nummer)? Name: $"{Nummer}: {Name}";
        public String                   Kurzbeschreibung        => mn.Kurzbeschreibung;
        public String                   Durchführung            => mn.Durchführung;
        public String                   Kostenstelle            => mn.Kostenstelle;
        public String                   Leistungsart            => mn.Leistungsart;
        public String                   Gewerk                  => mn.Gewerk;
        public String                   ExterneAuftragsnummer   => mn.ExterneAuftragsnummer;

        public DateTime?                Start                   => mn.Start;
        public bool                     IstBedarfsposition      => Termintyp == Termintyp.Bedarfsposition;
        public String                   Icon                    => IstBedarfsposition? "appbar_calendar_plus": "appbar_calendar";

        public int?                     Normzeit                => mn.Normzeit;
        public int?                     Menge                   => mn.Menge ?? 1;
        public decimal?                 Wert                    => mn.Wert;

        public decimal?                 Stunden                 => Positionen.Where(p => p.IstSichtbar).Sum(p => p.MinutenAngezeigt.GetValueOrDefault()) / 60m;
        public decimal?                 Kosten                  => Positionen.Where(p => p.IstSichtbar).Sum(p => p.KostenAngezeigt);
        public Weekday                  Tagesmuster             => (Weekday)mn.Tagesmuster;
        public decimal?                 Budget48                => mn.Budget48;
        public decimal?                 Budget52                => mn.Budget52;


        public bool ImExportSichtbar(PlanOptions opts) => !opts.NurRelevante || Positionen.Any(p => p.ImExportSichtbar(opts));


        public decimal ExportierteGeplanteStunden(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.Normzeit.GetValueOrDefault()) / 60m;

        public decimal ExportierteVerbrauchteStunden(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.MinutenAngezeigt.GetValueOrDefault()) / 60m;

        public decimal ExportierteGeplanteKosten(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.GeplanteKosten.GetValueOrDefault());

        public decimal ExportierteVerbrauchteKosten(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.VerbrauchteKosten.GetValueOrDefault());


        public String Info => $"Maßnahme {Name} gestartet am {Start}";


        public IEnumerable<Planaufgabe> Positionen  => Perioden.SelectMany(p => p.Aufgaben);


        public IEnumerable<Planaufgabe> GebePositionen(int? mincol = null, int? maxcol = null)
        {

            int max = maxcol.HasValue? Math.Min(maxcol.Value, Perioden.Count - 1): Perioden.Count - 1;
            int min = mincol.GetValueOrDefault();
            return Perioden.Skip(min).Take(max - min + 1).SelectMany(p => p.Aufgaben);
        }


        private bool leereZeileAnzeigen;
        public bool LeereZeileAnzeigen
        {
            get => leereZeileAnzeigen;
            set => SetProperty(ref leereZeileAnzeigen, value);
        }


        private int zeilenfarbenindex;
        public int Zeilenfarbenindex
        {
            get => zeilenfarbenindex;
            set {

                if (SetProperty(ref zeilenfarbenindex, value)) {

                    RaisePropertyChanged(nameof(TypAsFarbe));
                }
            }
        }


        public void Accept(IVisitor v)
        {

            v.Visit(this);
            Perioden.ForEach(p => p.Accept(v));
        }


        public bool Matches(String searchTerm)
            => IstSichtbar &&
               (Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                ExterneAuftragsnummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase));
    }
}
