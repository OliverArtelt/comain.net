﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.ReportModels;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;
using Comain.Client.ViewModels;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Reports
{

    public class AssetVerzeichnisReport : ReportBase, IReport
    {

        private readonly MultipleIHListViewModel view;
        private readonly IFilterService store;


        public AssetVerzeichnisReport(IFilterService store, MultipleIHListViewModel view)
        {

            this.view = view;
            this.store = store;
            filterViews = new List<IFilterViewModel> { view };
        }


        public Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            IEnumerable<IHObjektFilterModel> list = store.IHObjekte;
            if (!String.IsNullOrWhiteSpace(view.Assetliste)) {

                String[] assets = view.Assetliste.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
                list = list.Where(p => assets.Any(q => p.Nummer.StartsWith(q)));
            }

            var result = Create();
            var data = list.OrderBy(p => p.Nummer).Select(p => new IHTreeItem(p)).ToList();
            result.AddDataSource("MainData", data);
            result.HasLogo = true;
            return Task.FromResult<ReportResult>(result);
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Assetverzeichnis"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 1300; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.AssetVerzeichnisReport.rdlc"; }
        }
    }
}
