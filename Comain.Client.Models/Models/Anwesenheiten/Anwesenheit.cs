﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.Models.Anwesenheiten
{

    public class Anwesenheit : Entity, IDataErrorInfo
    {

        public String Personalnummer => Personal.Nummer;
        public String Personalname   => Personal.Name;


        private int asset_Id;
        public int Asset_Id
        {
            get => asset_Id;
            set => SetProperty(ref asset_Id, value);
        }


        private DateTime? datum;
        public DateTime? Datum
        {
            get => datum;
            set => SetProperty(ref datum, value);
        }


        private TimeSpan? uhrzeitVon;
        public TimeSpan? UhrzeitVon
        {
            get => uhrzeitVon;
            set => SetProperty(ref uhrzeitVon, value);
        }


        private TimeSpan? uhrzeitBis;
        public TimeSpan? UhrzeitBis
        {
            get => uhrzeitBis;
            set => SetProperty(ref uhrzeitBis, value);
        }


        private decimal? pause;
        public decimal? Pause
        {
            get => pause;
            set => SetProperty(ref pause, value);
        }


        private String tätigkeit;
        public String Tätigkeit
        {
            get => tätigkeit;
            set => SetProperty(ref tätigkeit, value);
        }


        private String bemerkung;
        public String Bemerkung
        {
            get => bemerkung;
            set => SetProperty(ref bemerkung, value);
        }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get => personal;
            set => SetProperty(ref personal, value);
        }


        private TrackableCollection<AnwesenheitMassnahme> massnahmen;
        public TrackableCollection<AnwesenheitMassnahme> Massnahmen
        {
            get => massnahmen ?? (massnahmen = new TrackableCollection<AnwesenheitMassnahme>());
            set => SetProperty(ref massnahmen, value);
        }


        public override void AcceptChanges()
        {

            base.AcceptChanges();
            Massnahmen.AcceptChanges();
        }


        public override bool IsChanged => base.IsChanged || Massnahmen.IsChanged;


#region V A L I D A T I O N


        public string Error => null;
        public string this[string columnName] => null;


#endregion

    }
}
