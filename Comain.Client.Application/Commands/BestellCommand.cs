﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Stock.Bestellungen;
using Comain.Client.Services.Stock.Bestellungen;


namespace Comain.Client.Commands
{
   
    public class BestellCommand : NotifyBase, IControllerCommand 
    {

        private readonly IBestellService store;


        public BestellCommand(IBestellService store)
        {
            this.store = store;
        }


        public Type         Controller  => typeof(IBestellController);
        public String       Name        =>"Bestellungen";
        public String       Parameter   =>"Stock.BS";
        public String       Gruppe      =>"Ersatzteile"; 
        public int          Sortierung  =>2000;
        public String       Color       =>"RoyalBlue";
        public String       Image       =>@"/Comain.Client.Views;component/Resources/appbar.shopping.png";
        public String       LiveValue   { get; private set; }
        public String       LiveText    { get; private set; }
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null || !module.VerwendetModulStock) return false;
            return nutzer.IstAdministrator || nutzer.IstLagerAdministrator || nutzer.IstLagerBestellungen;    
        }


        public async Task UpdateAsync(CancellationToken ct = default)
        {
            
            LiveValue = null;   
            LiveText = null;

            var anz = await store.AnzahlBSArtikelAsync(ct);
            switch (anz) {

            case 0:

                break;

            case 1:

                LiveValue = "1";   
                LiveText = "Artikel ist zu bestellen";
                break;

            default:

                LiveValue = anz.ToString();   
                LiveText = "Artikel sind zu bestellen";
                break;
            }

            RaisePropertyChanged("LiveValue");
            RaisePropertyChanged("LiveText");
        }
    }
}
