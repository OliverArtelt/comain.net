﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Reports
{

    public class LbAuDto
    {

        public bool         ImBudget              { get; set; }
        public String       FBNummer              { get; set; }

        public String       KstNummer             { get; set; }
        public String       KstName               { get; set; }
        public String       PzNummer              { get; set; }
        public String       PzName                { get; set; }
        public String       IhNummer              { get; set; }
        public String       IhName                { get; set; }
        public String       Inventarnummer        { get; set; }
        public String       Fabriknummer          { get; set; }

        public String       Nummer                { get; set; }
        public String       ExterneNummer         { get; set; }
        public String       Kurzbeschreibung      { get; set; }
        public String       Aktivbeschreibung     { get; set; }
        public DateTime?    Auftragsdatum         { get; set; }
        public DateTime?    Abschlußdatum         { get; set; }
        public String       Auftraggeber          { get; set; }
       
        public List<LbAuPsItem> Leistungen        { get; set; }
       
        public int?         InterneAuStelle       { get; set; }
        public decimal      Eigenkosten           { get; set; }
        public decimal      Materialkosten        { get; set; }
        public decimal      Fremdkosten           { get; set; }
        public decimal      MwSt                  { get; set; }
        public decimal      Personalstunden       { get; set; }
        
        
        public decimal Gesamtkosten { get { return Eigenkosten + Materialkosten + Fremdkosten; } }
    }
}
