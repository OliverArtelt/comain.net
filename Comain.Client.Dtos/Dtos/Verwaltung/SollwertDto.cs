﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class SollwertDto : ChangeTrackingBase
    {
       
        public int          Jahr                    { get; set; }
        public decimal?     Eigenleistungen         { get; set; }
        public decimal?     Materialkosten          { get; set; }
        public decimal?     Fremdleistungen         { get; set; }
        public int?         Störanzahl              { get; set; }
        public decimal?     Ausfallzeit             { get; set; }
        public decimal?     Störkosten              { get; set; }
        public decimal?     Produktionsmenge        { get; set; }
        public String       Einheit                 { get; set; }
    }
}
