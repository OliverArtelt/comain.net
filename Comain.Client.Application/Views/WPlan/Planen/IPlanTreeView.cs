﻿using System;
using System.Waf.Applications;


namespace Comain.Client.Views.WPlan.Planen
{

    public interface IPlanTreeView : IView
    {
        void ExpandTree(bool expand);
    }
}
