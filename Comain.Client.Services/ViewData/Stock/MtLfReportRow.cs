﻿using System;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;

namespace Comain.Client.ViewData.Stock.Reports
{

    public class MtLfReportRow
    {
        
        public int           ArtikelId               { get; set; }
        public String        Warengruppe             { get; set; }
        public String        Nummer                  { get; set; }
        public String        Name                    { get; set; }
        public decimal?      Mindestbestand          { get; set; }
        public decimal?      Mischpreis              { get; set; }

        public String        PosTyp                  { get; set; }
        public int?          Position                { get; set; }
        public decimal?      Preis                   { get; set; }
        public String        PosNummer               { get; set; }
        public String        PosName                 { get; set; }


        public MtLfReportRow()
        {}


        public MtLfReportRow(MtLfReportDto dto)
        {
        
            ArtikelId      = dto.Id; 
            Nummer         = dto.Nummer; 
            Name           = dto.Name; 
            Mindestbestand = dto.Mindestbestand; 
            Mischpreis     = dto.Mischpreis; 

            if (dto.Warengruppe != null) Warengruppe = dto.Warengruppe.Nummer; 
        }


        public MtLfReportRow(MtLfReportDto dto, ArtikelLfMap lfdto) : this(dto)
        {
        
            PosTyp    = "Lieferant";
            Position  = lfdto.Rank;
            Preis     = lfdto.Preis;
            PosNummer = lfdto.Nummer;
            if (String.IsNullOrEmpty(lfdto.Lieferant.Ort)) PosName = lfdto.Lieferant.Name1;
            else PosName = String.Format("{0} ({1})", lfdto.Lieferant.Name1, lfdto.Lieferant.Ort);
        }


        public MtLfReportRow(MtLfReportDto dto, ArtikelIhoMap ihdto) : this(dto)
        {
        
            PosTyp    = "Asset";
            Position  = ihdto.Position;
            PosNummer = ihdto.IHObjekt.Nummer;
            PosName   = ihdto.IHObjekt.Name;
        }
    }
}
