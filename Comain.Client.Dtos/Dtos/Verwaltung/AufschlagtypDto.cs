﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class AufschlagtypDto : ChangeTrackingBase
    {

        public String     NameKurz         { get; set; }
        public String     NameLang         { get; set; }
        public String     Bemerkung        { get; set; }

        public bool?      IstStandard      { get; set; }
        public bool?      Sonntags         { get; set; }
        public bool?      Samstags         { get; set; }
        public bool?      Feiertags        { get; set; }

        public TimeSpan?  UhrzeitVon       { get; set; }
        public TimeSpan?  UhrzeitBis       { get; set; }
        public decimal?   Prozent          { get; set; }
    }
}
