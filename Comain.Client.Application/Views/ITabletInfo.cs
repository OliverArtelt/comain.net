﻿using System;

namespace Comain.Client.Views
{
    public interface ITabletInfo
    {
        bool InTabletMode { get; }
        bool IsTablet { get; }

        event EventHandler TabletModeChanged;

        void Update();
    }
}