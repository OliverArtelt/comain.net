﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;

namespace Comain.Client.Controllers.WPlan.Planen
{

    public interface ITafelController : IController
    {
        void Unload();
    }
}
