﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models
{

    public class AssignedEntity
    {

        public String EntityType    { get; set; }
        public int    EntityId      { get; set; }

        public String Bezeichnung   { get; set; }
        public String Nummer        { get; set; }
    }
}
