﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewModels.WPlan.Reports
{
    public interface IPlanExport : IPlanOutput
    {
        IExportVisitor Exporter     { get; }
    }
}
