﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{

    public class LieferantAP : Entity, IDataErrorInfo
    {

        private String     name;                 
        private String     vorname;                 
        private String     telefon;               
        private String     fax;                   
        private String     eMail;                 
        private int        lieferant_Id;                 


#region P R O P E R T I E S

        
        public int Lieferant_Id                  
        {
            get { return lieferant_Id; }
            set { SetProperty(ref lieferant_Id, value); }
        }

        
        public String Name                  
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        
        public String Vorname               
        {
            get { return vorname; }
            set { SetProperty(ref vorname, value); }
        }
        
        public String Telefon             
        {
            get { return telefon; }
            set { SetProperty(ref telefon, value); }
        }
        
        public String Fax                 
        {
            get { return fax; }
            set { SetProperty(ref fax, value); }
        }
        
        public String EMail                   
        {
            get { return eMail; }
            set { SetProperty(ref eMail, value); }
        }
        

#endregion

        
        public String VollerName
        {
            
            get {

                if (String.IsNullOrWhiteSpace(vorname)) return name;
                return String.Format("{0}, {1}", name, vorname);
            }
        }


#region V A L I D A T I O N 
        

        public string Error
        {
           
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Vorname"];
                return test;
            }
        }

        
        public string this[string columnName]
        {
            
            get { 
            
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie den Namen des Ansprechpartners an."; 
                    if (Name.Length > 32) return "Der Name des Ansprechpartners darf aus maximal 32 Zeichen bestehen.";
                    break;

                case "Vorname":

                    if (String.IsNullOrWhiteSpace(Vorname)) return null; 
                    if (Vorname.Length > 32) return "Der Vorname des Ansprechpartners darf aus maximal 80 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
