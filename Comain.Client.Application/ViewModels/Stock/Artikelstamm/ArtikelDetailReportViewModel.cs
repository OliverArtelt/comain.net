﻿using System;
using System.Collections;
using System.Threading;
using Comain.Client.Views.Stock.Artikelstamm;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{
    
    public class ArtikelDetailReportViewModel : PrintViewModel<IArtikelDetailReportView>, IDelayableViewModel
    {

        public ArtikelDetailReportViewModel(IMessageService messages, IArtikelDetailReportView view) : base(messages, view)
        {
        }
    }
}
