﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecDetailMassnahmeViewModel : ValidatableViewModel<ISpecDetailMassnahmeView, SpecMassnahme>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecDetailMassnahmeViewModel(ISpecDetailMassnahmeView view)
          : base(view)
        {
        }


        public void Unload()
        {

            Gewerke = null;
            Leistungsarten = null;
        }


        public override void CurrentChanged(SpecMassnahme oldSelected)
        {

            RaisePropertyChanged(nameof(Zyklus));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(Start));
            RaisePropertyChanged(nameof(Normzeit));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Wert));
            RaisePropertyChanged(nameof(ExterneAuftragsnummer));
            RaisePropertyChanged(nameof(Qualifikation));
            RaisePropertyChanged(nameof(Werkzeug));
            RaisePropertyChanged(nameof(Durchführung));
            RaisePropertyChanged(nameof(Sicherheit));
            RaisePropertyChanged(nameof(Dokumentation));
            RaisePropertyChanged(nameof(Gewerk));
            RaisePropertyChanged(nameof(Leistungsart));

            RaiseTermintypChanged();
            RaiseWochenmusterChanged();
        }


        public void RaiseTermintypChanged()
        {

            RaisePropertyChanged(nameof(Termin1));
            RaisePropertyChanged(nameof(Termin2));
            RaisePropertyChanged(nameof(Termin3));
            RaisePropertyChanged(nameof(Termin4));
            RaisePropertyChanged(nameof(TageSichtbar));
            RaisePropertyChanged(nameof(PeriodeSichtbar));
            RaisePropertyChanged(nameof(ZyklusSichtbar));

            RaiseWochenmusterChanged();
        }


        public void RaiseWochenmusterChanged()
        {

            RaisePropertyChanged(nameof(Montags));
            RaisePropertyChanged(nameof(Dienstags));
            RaisePropertyChanged(nameof(Mittwochs));
            RaisePropertyChanged(nameof(Donnerstags));
            RaisePropertyChanged(nameof(Freitags));
            RaisePropertyChanged(nameof(Samstags));
            RaisePropertyChanged(nameof(Sonntags));
        }



#region P R O P E R T I E S


        public bool PeriodeSichtbar => TageSichtbar || ZyklusSichtbar;
        public bool TageSichtbar    => Termin3;
        public bool ZyklusSichtbar  => Termin1 || Termin2;


        public bool Montags
        {
            get { return Current != null && Current.Montags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Montags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Dienstags
        {
            get { return Current != null && Current.Dienstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Dienstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Mittwochs
        {
            get { return Current != null && Current.Mittwochs; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Mittwochs = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Donnerstags
        {
            get { return Current != null && Current.Donnerstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Donnerstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Freitags
        {
            get { return Current != null && Current.Freitags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Freitags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Samstags
        {
            get { return Current != null && Current.Samstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Samstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Sonntags
        {
            get { return Current != null && Current.Sonntags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Sonntags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Termin1
        {
            get { return Current != null && Current.Termintyp == Termintyp.UnveränderlicheKette; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.UnveränderlicheKette) {

                    Current.Termintyp = Termintyp.UnveränderlicheKette;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin2
        {
            get { return Current != null && Current.Termintyp == Termintyp.VeränderlicheKette; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.VeränderlicheKette) {

                    Current.Termintyp = Termintyp.VeränderlicheKette;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin3
        {
            get { return Current != null && Current.Termintyp == Termintyp.Tagesplanung; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.Tagesplanung) {

                    Current.Termintyp = Termintyp.Tagesplanung;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin4
        {
            get { return Current != null && Current.Termintyp == Termintyp.Bedarfsposition; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.Bedarfsposition) {

                    Current.Termintyp = Termintyp.Bedarfsposition;
                    RaiseTermintypChanged();
                }
            }
        }


        public int Zyklus
        {
            get { return Current == null? 0: Current.Zyklus; }
            set {

                if (Current != null && Current.Zyklus != value) {

                    Current.Zyklus = value;
                    RaisePropertyChanged(nameof(Zyklus));
                }
            }
        }


        public String Nummer
        {
            get { return Current?.Nummer; }
            set { 
            
                if (Current != null && Nummer != value) {

                    Current.Nummer = value;
                    RaisePropertyChanged(nameof(Nummer));
                }
            }
        }


        public String Name
        {
            get { return Current?.Name; }
            set { 
            
                if (Current != null && Name != value) {

                    Current.Name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return Current?.Kurzbeschreibung; }
            set { 
            
                if (Current != null && Kurzbeschreibung != value) {

                    Current.Kurzbeschreibung = value;
                    RaisePropertyChanged(nameof(Kurzbeschreibung));
                }
            }
        }


        public DateTime? Start
        {
            get { return Current?.Start; }
            set { 
            
                if (Current != null && Start != value) {

                    Current.Start = value;
                    RaisePropertyChanged(nameof(Start));
                }
            }
        }


        public int? Normzeit
        {
            get { return Current?.Normzeit; }
            set { 
            
                if (Current != null && Normzeit != value) {

                    Current.Normzeit = value;
                    RaisePropertyChanged(nameof(Normzeit));
                }
            }
        }


        public int? Menge
        {
            get { return Current?.Menge; }
            set { 
            
                if (Current != null && Menge != value) {

                    Current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public decimal? Wert
        {
            get { return Current?.Wert; }
            set { 
            
                if (Current != null && Wert != value) {

                    Current.Wert = value;
                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public String ExterneAuftragsnummer
        {
            get { return Current?.ExterneAuftragsnummer; }
            set { 
            
                if (Current != null && ExterneAuftragsnummer != value) {

                    Current.ExterneAuftragsnummer = value;
                    RaisePropertyChanged(nameof(ExterneAuftragsnummer));
                }
            }
        }


        public String Qualifikation
        {
            get { return Current?.Qualifikation; }
            set { 
            
                if (Current != null && Qualifikation != value) {

                    Current.Qualifikation = value;
                    RaisePropertyChanged(nameof(Qualifikation));
                }
            }
        }


        public String Werkzeug
        {
            get { return Current?.Werkzeug; }
            set { 
            
                if (Current != null && Werkzeug != value) {

                    Current.Werkzeug = value;
                    RaisePropertyChanged(nameof(Werkzeug));
                }
            }
        }


        public String Durchführung
        {
            get { return Current?.Durchführung; }
            set { 
            
                if (Current != null && Durchführung != value) {

                    Current.Durchführung = value;
                    RaisePropertyChanged(nameof(Durchführung));
                }
            }
        }


        public String Sicherheit
        {
            get { return Current?.Sicherheit; }
            set { 
            
                if (Current != null && Sicherheit != value) {

                    Current.Sicherheit = value;
                    RaisePropertyChanged(nameof(Sicherheit));
                }
            }
        }


        public String Dokumentation
        {
            get { return Current?.Dokumentation; }
            set { 
            
                if (Current != null && Dokumentation != value) {

                    Current.Dokumentation = value;
                    RaisePropertyChanged(nameof(Dokumentation));
                }
            }
        }


        public NummerFilterModel Gewerk
        {
            get { return Current?.Gewerk; }
            set {

                if (Current != null && Current.Gewerk != value) {

                    Current.Gewerk = value;
                    RaisePropertyChanged(nameof(Gewerk));
                }
            }
        }


        public LeistungsartFilterModel Leistungsart
        {
            get { return Current?.Leistungsart; }
            set {

                if (Current != null && Current.Leistungsart != value) {

                    Current.Leistungsart = value;
                    RaisePropertyChanged(nameof(Leistungsart));
                }
            }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get => editEnabled;
            set => SetProperty(ref editEnabled, value);
        }


        private Spezifikation currentSpezifikation;
        public Spezifikation CurrentSpezifikation
        {
            get => currentSpezifikation; 
            set { if (SetProperty(ref currentSpezifikation, value)) RaisePropertyChanged(nameof(VorgeseheneMassnahmen)); }
        }


        public TrackableCollection<SpecMassnahme> VorgeseheneMassnahmen
            => CurrentSpezifikation?.VorgeseheneMassnahmen;


        private IList<NummerFilterModel> gewerke;
        public IList<NummerFilterModel> Gewerke
        {
            get => gewerke;
            set => SetProperty(ref gewerke, value);
        }


        private IList<LeistungsartFilterModel> leistungsarten;
        public IList<LeistungsartFilterModel> Leistungsarten
        {
            get => leistungsarten;
            set => SetProperty(ref leistungsarten, value);
        }


#endregion

    }
}
