﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.Views;


namespace Comain.Client.ViewModels
{

    public class PrintViewModel<TPrintView> : ViewModel<TPrintView>, IDelayableViewModel where TPrintView: IPrintView
    {

        protected SynchronizationContext context;
       // protected ReportViewer viewer;
        protected IMessageService messages;
       // public event DrillthroughEventHandler DetailReportRequest;


        public PrintViewModel(IMessageService messages, TPrintView view) : base(view)
        {

            context = SynchronizationContext.Current;
          //  viewer = new ReportViewer();
          //  viewer.ProcessingMode = ProcessingMode.Local;
          //  viewer.LocalReport.EnableHyperlinks = true;
          //  viewer.Drillthrough += new DrillthroughEventHandler(DrillthroughEventHandler);
          //  viewer.Hyperlink += new HyperlinkEventHandler(CopyTextEventHandler);
          //
          //  this.messages = messages;
          //  messages.OverlayRaisedEvent +=  (o, e) => { ViewerVisible = false; };
          //  ViewCore.BindControl(viewer);
        }


        public async Task BindViewerAsync(ReportResult reportData)
        {

          //  if (reportData == null) return;
          //
          //  try {
          //
          //      await context.SendAsync(new SendOrPostCallback((o) => { viewer.Reset(); }), null);
          //
          //      if (reportData.DataSources != null) {
          //
          //          viewer.LocalReport.DataSources.Clear();
          //          reportData.DataSources.ForEach(p => viewer.LocalReport.DataSources.Add(new ReportDataSource(p.Item1, p.Item2)));
          //      }
          //      viewer.LocalReport.DisplayName = reportData.Title;
          //      viewer.LocalReport.EnableHyperlinks = true;
          //      viewer.LocalReport.EnableExternalImages = true;
          //
          //      Assembly assembly = Assembly.Load(reportData.AssemblyAddress);
          //
          //      using (var stream = assembly.GetManifestResourceStream(reportData.ReportAddress)) {
          //
          //          viewer.LocalReport.LoadReportDefinition(stream);
          //
          //          var pars = reportData.Parameters.Select(p => new ReportParameter(p.Item1, p.Item2)).ToList();
          //          if (reportData.HasLogo) pars.Add(new ReportParameter("parLogo", LogoPath));
          //          if (!pars.IsNullOrEmpty()) viewer.LocalReport.SetParameters(pars);
          //      }
          //
          //      ViewerVisible = true;
          //
          //      await context.SendAsync(new SendOrPostCallback((o) => {
          //
          //          viewer.RefreshReport();
          //
          //      }), null);
          //  }
          //  catch (Exception x) {
          //
          //      Unload();
          //      messages.Show(x);
          //  }
        }


        private String LogoPath
        {
            get {

                var path = AppDomain.CurrentDomain.BaseDirectory;
                path = Path.Combine(path, "logo.png");
                return @"file:///" + path;
            }
        }


        public virtual void Unload()
        {

            context.Send(new SendOrPostCallback((o) => {

                ViewerVisible = false;
            }), null);
        }


      //  private void DrillthroughEventHandler(object sender, DrillthroughEventArgs e)
      //  {
      //
      //      var handler = DetailReportRequest;
      //      if (handler != null) handler(this, e);
      //  }
      //
      //
      //  private void CopyTextEventHandler(object sender, HyperlinkEventArgs e)
      //  {
      //
      //      Uri uri = new Uri(e.Hyperlink);
      //      if (uri.Scheme.ToLower() != "copy") return;
      //
      //      System.Windows.Forms.Clipboard.SetText(uri.LocalPath);
      //      e.Cancel = true;
      //  }


#region P R O P E R T I E S


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { if (SetProperty(ref showDelay, value) && value) ViewerVisible = false; }
        }


        private String progressText;
        public String ProgressText
        {
            get { return progressText; }
            set { SetProperty(ref progressText, value); }
        }


        private bool viewerVisible;
        public bool ViewerVisible
        {
            get { return viewerVisible; }
            set { SetProperty(ref viewerVisible, value); }
        }

#endregion

    }
}
