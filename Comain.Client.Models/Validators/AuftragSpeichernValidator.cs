﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Validators
{

    public class AuftragSpeichernValidator : AuftragValidatorBase, IAuftragValidator
    {

        public AuftragSpeichernValidator(Auftrag model) : base(model)
        {}


        public virtual IList<String> ErrorList
        {

            get {

                var result = new List<string>();

                String test = this["ExterneNummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["FBNummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Störzeit"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["ÜbergabeDatum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["GemeldetDatum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Leistungsart"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Auftraggeber"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["InterneAuStelle"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }

   }
}
