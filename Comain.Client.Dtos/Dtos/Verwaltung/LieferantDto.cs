﻿using System;
using System.Collections.Generic;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class LieferantDto : ChangeTrackingBase
    {
        
        public String     Name1                   { get; set; }
        public String     Name2                   { get; set; }
        public String     Strasse                 { get; set; }
        public String     Plz                     { get; set; }
        public String     Ort                     { get; set; }
        public String     Telefon                 { get; set; }
        public String     Fax                     { get; set; }
        public String     EMail                   { get; set; }
        public String     Rechnungsanschrift      { get; set; }
        public bool       LieferantFürIHObjekte   { get; set; }
        public bool       LieferantFürAufträge    { get; set; }

        public IList<LieferantAPDto> Ansprechpartnerliste  { get; set; }
    }
}
