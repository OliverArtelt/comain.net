﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;


namespace Comain.Client.Services.Verwaltung
{

    public class LieferantService : ILieferantService
    {

        protected readonly IRepository store;
        protected TrackableCollection<Lieferant> currentList;
        private readonly IMapper mapper;


        public LieferantService(IRepository store, IMapper mapper)
        {

            this.store = store;
            this.mapper = mapper;
        }


        public async Task<TrackableCollection<Lieferant>> GetAsync(String url, CancellationToken ct = default)
        {

            currentList = null;
            var result = await store.GetAsync<IList<LieferantDto>>(url, ct).ConfigureAwait(false);
            var list = mapper.Map<List<Lieferant>>(result);
            list.Sort();
            currentList = new TrackableCollection<Lieferant>(list);
            currentList.Select(p => p.Ansprechpartnerliste).ForEach(p => p.AcceptChanges());
            return currentList;
        }


        public bool HasChanges
        {
            get { return currentList != null && currentList.IsChanged; }
        }


        public async Task SaveAsync(String url, CancellationToken ct = default)
        {

            var dto = new LieferantSubmitDto();
            dto.Lieferanten = mapper.AsDtoList<Lieferant, LieferantDto>(currentList);
            dto.LieferantenAP = new List<LieferantAPDto>();
            dto.LieferantenAP.AddRange(currentList.SelectMany(p => mapper.AsDtoList<LieferantAP, LieferantAPDto>(p.Ansprechpartnerliste)));
            if (dto.Lieferanten.IsNullOrEmpty() && dto.LieferantenAP.IsNullOrEmpty()) return;

            await store.PostAsync(url, dto, ct).ConfigureAwait(false);
            currentList.AcceptChanges();
            currentList.Where(p => p.Ansprechpartnerliste != null).SelectMany(p => p.Ansprechpartnerliste).ForEach(p => p.AcceptChanges());
        }


        public void Reset()
        {
            currentList = null;
        }


        public async Task CombineAsync(int src, int dst, CancellationToken ct = default)
        {

            var tuple = new Tuple<int, int>(src, dst);
            await store.PostAsync("/VW/LFMerge", tuple, ct).ConfigureAwait(false);
        }
    }
}
