﻿using Comain.Client.Services;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellDetailReportViewModel : PrintViewModel<IBestellDetailReportView>, IDelayableViewModel
    {

        public BestellDetailReportViewModel(IMessageService messages, IBestellDetailReportView view) : base(messages, view)
        {
        }
    }
}
