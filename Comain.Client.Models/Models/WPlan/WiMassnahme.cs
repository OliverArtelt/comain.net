﻿using System.Linq;
using System;
using System.ComponentModel;
using Comain.Client.Models;
using System.Collections.Generic;
using Comain;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Dokumente;

namespace Comain.Client.Models.WPlan
{

    /// <summary>
    /// Einzelne Wartungsposition, wird an den entsprechenden Terminen in ein Auftrag überführt
    /// </summary>
    public class WiMassnahme : DocEntity, IDataErrorInfo, IComparable<WiMassnahme>
    {

        private IHObjektFilterModel     ihobjekt;
        private String                  nummer;
        private String                  name;
        private String                  kurzbeschreibung;
        private int                     zyklus;
        private Termintyp               termintyp;
        private Weekday                 tagesmuster;
        private LeistungsartFilterModel leistungsart;
        private DateTime?               start;
        private bool                    istAktiv;
        private int?                    normzeit;
        private NummerFilterModel       gewerk;
        private String                  qualifikation;
        private String                  werkzeug;
        private String                  durchführung;
        private String                  sicherheit;
        private String                  dokumentation;
        private int?                    menge;
        private decimal?                wert;
        private String                  externeAuftragsnummer;


        private TrackableCollection<WiMaterialVorgabe>  materialien;
        private TrackableCollection<Media>              medien;


        public override String EntityType => "MN";


        public override void AcceptChanges()
        {

            if (!materialien.IsNullOrEmpty()) materialien.AcceptChanges();
            if (!medien.IsNullOrEmpty()) materialien.AcceptChanges();
            base.AcceptChanges();
        }


        public override bool IsChanged => base.IsChanged || Medien.IsChanged || Materialien.IsChanged;


        public static String ZyklusAsString(Termintyp typ, int zyklus, Weekday tage)
        {

            if (typ == Termintyp.Undefiniert) return null;
            if (typ == Termintyp.Bedarfsposition) return "Bedarf";
            if (typ != Termintyp.Tagesplanung && zyklus == 1) return "Woche";
            if (typ != Termintyp.Tagesplanung && zyklus < 1) return null;
            if (typ != Termintyp.Tagesplanung) return $"{zyklus} Wochen";
            if (tage == default) return "Woche";
            return tage.ToShortString();
        }


#region P R O P E R T I E S


        public IHObjektFilterModel IHObjekt
        {
            get { return ihobjekt; }
            set { SetProperty(ref ihobjekt, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Kurzbeschreibung
        {
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }


        public int Zyklus
        {
            get { return zyklus; }
            set { SetProperty(ref zyklus, value); }
        }


        public Termintyp Termintyp
        {
            get { return termintyp; }
            set { SetProperty(ref termintyp, value); }
        }


        public Weekday Tagesmuster
        {
            get { return tagesmuster; }
            set { SetProperty(ref tagesmuster, value); }
        }


        public bool Montags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Montag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Montag, value); }
        }


        public bool Dienstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Dienstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Dienstag, value); }
        }


        public bool Mittwochs
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Mittwoch);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Mittwoch, value); }
        }


        public bool Donnerstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Donnerstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Donnerstag, value); }
        }


        public bool Freitags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Freitag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Freitag, value); }
        }


        public bool Samstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Samstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Samstag, value); }
        }


        public bool Sonntags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Sonntag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Sonntag, value); }
        }


        public LeistungsartFilterModel Leistungsart
        {
            get { return leistungsart; }
            set { SetProperty(ref leistungsart, value); }
        }


        public DateTime? Start
        {
            get { return start; }
            set { SetProperty(ref start, value); }
        }


        public bool IstAktiv
        {
            get { return istAktiv; }
            set { SetProperty(ref istAktiv, value); }
        }


        public int? Normzeit
        {
            get { return normzeit; }
            set { SetProperty(ref normzeit, value); }
        }


        public int? Menge
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }


        public decimal? Wert
        {
            get { return wert; }
            set { SetProperty(ref wert, value); }
        }



        public String ExterneAuftragsnummer
        {
            get { return externeAuftragsnummer; }
            set { SetProperty(ref externeAuftragsnummer, value); }
        }


        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        public String Qualifikation
        {
            get { return qualifikation; }
            set { SetProperty(ref qualifikation, value); }
        }


        public String Werkzeug
        {
            get { return werkzeug; }
            set { SetProperty(ref werkzeug, value); }
        }


        public String Durchführung
        {
            get { return durchführung; }
            set { SetProperty(ref durchführung, value); }
        }


        public String Sicherheit
        {
            get { return sicherheit; }
            set { SetProperty(ref sicherheit, value); }
        }


        public String Dokumentation
        {
            get { return dokumentation; }
            set { SetProperty(ref dokumentation, value); }
        }


        public TrackableCollection<WiMaterialVorgabe> Materialien
        {
            get {

                if (materialien == null) materialien = new TrackableCollection<WiMaterialVorgabe>();
                return materialien;
            }
            set { SetProperty(ref materialien, value); }
        }


        public TrackableCollection<Media> Medien
        {
            get {

                if (medien == null) medien = new TrackableCollection<Media>();
                return medien;
            }
            set { SetProperty(ref medien, value); }
        }


#endregion


        public int CompareTo(WiMassnahme other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Name.CompareTo(other.Name);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Kurzbeschreibung)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Nummer)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(IHObjekt)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Zyklus)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Start)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Materialien)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Normzeit)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Termintyp)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Montags)];
                return test;
            }
        }


        public IList<String> Errorlist
        {

            get {

                var list = new List<String>();

                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Kurzbeschreibung)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Nummer)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(IHObjekt)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Zyklus)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Start)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Materialien)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Normzeit)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Termintyp)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Montags)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                return list;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 150) return "Die Bezeichnung darf aus maximal 150 Zeichen bestehen.";
                    break;

                case nameof(Nummer):

                    if (String.IsNullOrWhiteSpace(Nummer)) return null;
                    if (Nummer.Length > 50) return "Die Maßnahmenummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case nameof(Kurzbeschreibung):

                    if (String.IsNullOrWhiteSpace(Kurzbeschreibung)) return "Geben Sie die Kurzbeschreibung an.";
                    break;

                case nameof(IHObjekt):

                    if (IHObjekt == null) return "Die Maßnahme muss einem Asset zugeordnet werden.";
                    break;

                case nameof(Zyklus):

                    if ((Termintyp == Termintyp.VeränderlicheKette || Termintyp == Termintyp.UnveränderlicheKette) && Zyklus <= 0)
                        return "Geben Sie einen gültigen Zyklus (mindestens eine Woche) an.";
                    break;

                case nameof(Start):

                    if (!Start.HasValue) return null;
                    if (Start.Value.Year < 2010 || Start.Value.Year > 2100) return "Geben Sie ein plausibles Datum an.";
                    break;

                case nameof(Materialien):

                    return Materialien.FirstOrDefault(p => !String.IsNullOrEmpty(p.Error))?.Error;

                case nameof(Normzeit):

                    if (!Normzeit.HasValue) return null;
                    if (Normzeit.Value < 1) return "Wenn eine Normzeit angegeben wurde muß diese mindestens eine Minute betragen.";
                    break;

                case nameof(Termintyp):

                    if (!IstAktiv) return null;
                    if (Termintyp == Termintyp.Undefiniert) return "Der Planungstyp muß angegeben werden.";
                    if (Termintyp == Termintyp.Tagesplanung && (Tagesmuster & Weekday.Komplett) == 0) return "Bei einer tagesgenauen Planung muß mindestens ein Wochentag angegeben werden.";
                    if ((Termintyp == Termintyp.VeränderlicheKette || Termintyp == Termintyp.UnveränderlicheKette) && Zyklus <= 0) 
                        return "Geben Sie einen gültigen Zyklus (mindestens eine Woche) an.";
                    break;

                case nameof(Montags):

                    if (!IstAktiv) return null;
                    if (Termintyp == Termintyp.Tagesplanung && (Tagesmuster & Weekday.Komplett) == 0) return "Bei einer tagesgenauen Planung muß mindestens ein Wochentag angegeben werden.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
