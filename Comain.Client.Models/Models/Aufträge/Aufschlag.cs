﻿using System.ComponentModel;


namespace Comain.Client.Models.Aufträge
{

    public class Aufschlag : Entity, IDataErrorInfo
    {

        public int        Aufschlagtyp_Id       { get; set; }
        public int?       Minuten               { get; set; }
        public decimal?   Prozent               { get; set; }



#region V A L I D A T I O N


        public string Error
        {
            get { return null; }
        }


        public string this[string columnName]
        {
            get { return null; }
        }

#endregion

    }
}