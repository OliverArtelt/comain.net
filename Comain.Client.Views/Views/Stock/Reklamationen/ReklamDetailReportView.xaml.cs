﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Comain.Client.Views.Reports;

namespace Comain.Client.Views.Stock.Reklamationen
{
    /// <summary>
    /// Interaction logic for ReklamDetailReportView.xaml
    /// </summary>
    public partial class ReklamDetailReportView:UserControl  ,IReklamDetailReportView
    {
        public ReklamDetailReportView()
        {
        InitializeComponent();
        }
    }
}
