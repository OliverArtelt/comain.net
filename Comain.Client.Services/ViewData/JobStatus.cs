﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.ViewData
{

    public class JobStatus
    {

        public String       Id              { get; set; }
        public String       Publisher       { get; set; }
        public int?         Percent         { get; set; }
        public String       Status          { get; set; }
        public String       Ergebnis        { get; set; }
        public String       ErgebnisInfo    { get; set; }
        public String       LastUser        { get; set; }
        public DateTime?    LastUsed        { get; set; } 
    }
}
