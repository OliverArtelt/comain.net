﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Comain.Client.Models;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailHistorieViewModel : ValidatableViewModel<ITicketDetailHistorieView, Ticket>
    {

        private readonly SynchronizationContext context;

 
        public TicketDetailHistorieViewModel(ITicketDetailHistorieView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(Ticket oldSelected)
        {

            if (Current == null) {
            
                Protokoll = null;
                return;
            }

            Protokoll = Current.Protokoll.Select(p => new ProtokollViewItem(p))
                        .Union(Current.Personalleistungen.Select(p => new ProtokollViewItem(p)))
                        .OrderBy(p => p.Beginn)
                        .ThenBy(p => p.Status)
                        .ToList();
        }


        public void Unload()
        {
            Protokoll = null;
        }


#region P R O P E R T I E S


        private List<ProtokollViewItem> protokoll;
        public List<ProtokollViewItem> Protokoll
        { 
            get { return protokoll; }
            set { SetProperty(ref protokoll, value); }
        }


#endregion

    }
}
