﻿using System.Threading;
using System.Waf.Applications;
using Comain.Client.Views.Stock.Inventuren;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Inventuren
{

    public class InventurListViewModel : ViewModel<IInventurListView>, IDelayableViewModel
    {
 
        private readonly SynchronizationContext context;


        public InventurListViewModel(IInventurListView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
            Liste = null;
        }


#region P R O P E R T I E S
 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
 

        private TrackableCollection<InventurViewItem> liste;
        public TrackableCollection<InventurViewItem> Liste
        { 
            get { return liste; }
            set { SetProperty(ref liste, value); }
        }


#endregion

    }
}
