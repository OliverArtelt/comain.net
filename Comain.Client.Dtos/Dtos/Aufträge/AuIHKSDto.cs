﻿using System;
using System.Diagnostics;

namespace Comain.Client.Dtos.Aufträge
{

    [DebuggerDisplay("IHObjekt")]
    public class AuIHKSDto : EntityModelBase
    {

        public int      IHObjekt_Id             { get; set; }
        public int      Kostenstelle_Id         { get; set; }
        public int      Standort_Id             { get; set; }
        public String   IHObjekt                { get; set; }
        public String   Maschine                { get; set; }
        public String   Baugruppe               { get; set; }
        public String   Baugruppenpfad          { get; set; }
        public String   Inventarnummer          { get; set; }
        public String   Fabriknummer            { get; set; }
        public String   Standort                { get; set; }
        public String   Kostenstelle            { get; set; }
        public bool     IstAktiv                { get; set; }
        public int?     Erhaltklasse            { get; set; }
        public int?     Status                  { get; set; }
    }
}
