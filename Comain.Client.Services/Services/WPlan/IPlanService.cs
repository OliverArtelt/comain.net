﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.ViewData.WPlan;


namespace Comain.Client.Services.WPlan 
{

    public interface IPlanService
    {

        Task<ZeitstrahlDto>                  GetPlanAsync(WPlanCreateInfo nfo, CancellationToken ct = default);
        Task<Planstatistik>                  GetStatsAsync(WPlanCreateInfo nfo, CancellationToken ct = default);

        Task                                 StoreAsync(IEnumerable<PositionSubmitDto> dtos, CancellationToken ct = default);

        Task<List<PZFilterItem>>             IHTreeAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>       FilterGewerkeAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>     FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<LeistungsartFilterModel>> FilterLeistungsartenAsync(CancellationToken ct = default);

        Task<List<AuftragReportItem>>        DetailReportAsync(IEnumerable<int> auids, CancellationToken ct = default);
        Task<List<MassnahmeRow>>             MassnahmenReportAsync(WPlanCreateInfo filter, CancellationToken ct = default);

        Task<OrphanedCount>                  CountOrphanedAsync(WPlanCreateInfo args, CancellationToken ct = default);
        Task<String>                         CreateAsync(WPlanCreateInfo args, CancellationToken ct = default);
    }
}
