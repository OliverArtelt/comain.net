﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Validators
{

    public class AuftragLöschenValidator : IAuftragValidator
    {

        protected readonly Auftrag model;


        public AuftragLöschenValidator(Auftrag model)
        {
            this.model = model;
        }


        public virtual IList<String> ErrorList
        {
            
            get { 
            
                var result = new List<string>();
                if (model.Status == Auftragstatus.Abgeschlossen) result.Add("Der Auftrag ist bereits abgeschlossen.");
                if (model.Status == Auftragstatus.Storno) result.Add("Der Auftrag ist bereits storniert.");
                if (model.HatPersonalleistungenVorAbrechnungsschluß) result.Add("Der Auftrag besitzt Personalleistungen vor dem Abrechnungsschluss.");
                if (model.HatMaterialleistungenVorAbrechnungsschluß) result.Add("Der Auftrag besitzt Materialleistungen vor dem Abrechnungsschluss.");
                
                return result;
            }
        }
    }
}
