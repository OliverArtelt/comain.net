﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;


namespace Comain.Client.Models.Specs
{

    public class SpecArtikel : Entity, IDataErrorInfo
    {

        private  decimal?   menge;
        private  bool       erforderlich;
        private  String     anweisung;


        public decimal? Menge
        {
            get => menge;
            set => SetProperty(ref menge, value);
        }


        public bool Erforderlich
        {
            get => erforderlich;
            set => SetProperty(ref erforderlich, value);
        }


        public String Anweisung
        {
            get => anweisung;
            set => SetProperty(ref anweisung, value);
        }


        public  int                 Artikel_Id          { get; set; }
        public  int                 Spezifikation_Id    { get; set; }

        public  String              Name                { get; set; }
        public  String              Nummer              { get; set; }
        public  String              Beschreibung        { get; set; }
        public  decimal?            Mischpreis          { get; set; }
        public  NummerFilterModel   Einheit             { get; set; }
        public  NummerFilterModel   Warengruppe         { get; set; }


        public SpecArtikel()
        {}


        public SpecArtikel(MaterialstammFilterModel model)
        {

            Artikel_Id   = model.Id;
            Name         = model.Name;
            Nummer       = model.Nummer;
            Beschreibung = model.Beschreibung;
            Mischpreis   = model.Mischpreis;
            Einheit      = model.Einheit;
            Warengruppe  = new NummerFilterModel { Nummer = model.Warengruppe, Name = model.Warengruppe };
        }


#region V A L I D A T I O N


        public string Error => null;
        public IList<String> Errorlist => null;
        public string this[string columnName] => null;

#endregion

    }
}
