﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{

    public class Materialleistung : Entity, IDataErrorInfo
    {

        public Materialstatus?          Status              { get; set; }
        public String                   Nummer              { get; set; }
        public String                   Name                { get; set; }
        public String                   Beschreibung        { get; set; }
        public decimal                  Menge               { get; set; }
        public decimal                  Einzelpreis         { get; set; }
        public decimal?                 Gesamtpreis         { get; set; }
        public decimal?                 Mwst                { get; set; }
        public DateTime                 Datum               { get; set; }
        public decimal?                 Handlingszuschlag   { get; set; }
        public bool                     Versiegelt          { get; set; }
        public String                   Entnahmeschein      { get; set; }

        public int                      Auftrag_Id          { get; set; }
        public int?                     Lieferant_Id        { get; set; }
        public int?                     Einheit_Id          { get; set; }
        public int?                     Materialstamm_Id    { get; set; }
        public int?                     Lagerung_Id         { get; set; }
        public int?                     Nutzer_Id           { get; set; }

        public String                   EinheitName         { get; set; }


        public bool DatumIstPlausibel { get { return Datum.Year >= 1990 || Datum.Year < 2100; } }


#region V A L I D A T I O N


        public List<String> ErrorList
        {

            get {

                var errors = new List<String>();
                if (!String.IsNullOrEmpty(this["Einheit_Id"]))   errors.Add(this["Einheit_Id"]);
                if (!String.IsNullOrEmpty(this["Name"]))         errors.Add(this["Name"]);
                if (!String.IsNullOrEmpty(this["Nummer"]))       errors.Add(this["Nummer"]);
                if (!String.IsNullOrEmpty(this["Datum"]))        errors.Add(this["Datum"]);

                return errors;
            }
        }


        public string Error
        {

            get {

                String test = this["Einheit_Id"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Einheit_Id":

                    if (Einheit_Id == 0) return "Geben Sie die Mengeneinheit an.";
                    break;

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Materialbezeichnung an.";
                    if (Name.Length > 150) return "Die Materialbezeichnung darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return null;
                    if (Nummer.Length > 50) return "Die Materialnummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Datum":

                    if (!DatumIstPlausibel) return "Geben Sie ein plausibles Datum an.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
