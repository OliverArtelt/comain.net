﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.Reports;
 

namespace Comain.Client.Controllers.Reports
{
    
    public class ReportFactory : IReportFactory
    {

        private readonly List<IReport> reports;
        private readonly List<IFilterViewModel> forms;
        private readonly ILoginService login;
        private readonly IModulService module;


        public ReportFactory(IEnumerable<IReport> reports, IEnumerable<IFilterViewModel> forms, ILoginService login, IModulService module)
        {

            this.reports = new List<IReport>(reports); 
            this.forms = new List<IFilterViewModel>(forms); 
            this.login = login; 
            this.module = module; 
        }


        public IList<IReport> AlleReports()
        {

            return reports.Where(p => !String.IsNullOrEmpty(p.Gruppe) && p.HatZugriff(module.Info, login.AktuellerNutzer))
                          .OrderBy(p => p.Gruppe).ThenBy(p => p.Sortierung)
                          .ToList();
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.WhenAll(forms.Select(p => p.LoadAsync(ct)));
        }


        public void Unload()
        {
            forms.ForEach(p => p.Unload());
        }
    }
}
