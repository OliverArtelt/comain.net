﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Tickets;


namespace Comain.Client.Commands
{

    public class BevorCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IBevorController);
        public String       Name        => "Besonderes Vorkommnis";
        public String       Parameter   => "Bevor.Add";
        public String       Gruppe      => "Ticket";
        public int          Sortierung  => 500;
        public String       Color       => "BlueViolet";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.camera.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null || !module.VerwendetModulBevor) return false;
            return nutzer.IstAdministrator || nutzer.IstBevor;
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
