﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Artikelstamm;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class IhoPickViewModel : ViewModel<IIhoPickView>
    {

        private readonly SynchronizationContext context;
        private IEnumerable<IHObjektFilterModel> list;


        public IhoPickViewModel(IIhoPickView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void SetIHObjekte(IEnumerable<IHObjektFilterModel> list)
        {

            this.list = list.OrderBy(p => p.Nummer).ToList();
            IHObjekte = null;
        }


        public async Task SetArtikelAsync(Artikel mt)
        {
        
            await context.SendAsync(new SendOrPostCallback((o) => { 

                if (list.IsNullOrEmpty()) return;
                if (mt == null) return;

                IHObjekte = new CollectionViewSource { Source = list.Where(p => p != null).Select(ih => new IHPickItem(mt, ih)).ToList() }.View;
                IHObjekte.Filter = p => {

                    var ih = p as IHPickItem;
                    if (ih == null) return false;
                    return !AssignedOnly || ih.IsSelected;
                };        
                //IHObjekte.SortDescriptions.Add(new SortDescription { PropertyName = "Nummer", Direction = ListSortDirection.Ascending });
                IHObjekte?.Refresh();        

            }), null);
        }
        

        public void Unload()
        {
            list = null;
            IHObjekte = null;
        }


#region P R O P E R T I E S


        private ICollectionView iHObjekte;
        public ICollectionView IHObjekte
        { 
            get { return iHObjekte; }
            set { SetProperty(ref iHObjekte, value); }
        }


        private bool assignedOnly;
        public bool AssignedOnly
        { 
            get { return assignedOnly; }
            set { if (SetProperty(ref assignedOnly, value) && IHObjekte != null) IHObjekte?.Refresh(); }
        }
 

#endregion
 
    }
}
