﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Emma;

namespace Comain.Client.Services.Emma
{
    public interface IEmmaService
    {

        Task<EmmaSession>     CreateSessionAsync(CancellationToken ct);
        Task<IList<Leistung>> SearchAsync(Suchkriterien search, CancellationToken ct = default);
        Task<IList<Leistung>> ImportAsync(String filepath, CancellationToken ct = default);
        Task<IList<Leistung>> SaveAsync(IEnumerable<Leistung> models, CancellationToken ct = default);
        Task<IList<Leistung>> AutoSaveAsync(IEnumerable<Leistung> models, CancellationToken ct = default);
        Task<IList<Leistung>> SaveAndCloseAsync(IEnumerable<Leistung> models, CancellationToken ct = default);

        Task<EmmaCloseInfo>   CloseInfoAsync(DateTime week, CancellationToken ct = default);
    }
}
