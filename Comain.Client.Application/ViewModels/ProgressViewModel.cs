﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos;
using Comain.Client.Services;
using Comain.Client.ViewData;
using Comain.Client.Views;


namespace Comain.Client.ViewModels
{

    public class ProgressViewModel : ViewModel<IProgressView>
    {

        private const int pollDelay = 3000;
        private readonly IJobService store;
        private JobStatus current;


        public ProgressViewModel(IProgressView view, IJobService store) : base(view)
        {

            this.store = store;
            CancelCommand = new DelegateCommand(Cancel);
        }


        public void Reset()
        {

            current = null;
            Start = DateTime.Now.ToString();
            RaiseStatus();            
        }


        public async Task PollAsync(String sessionId, CancellationTokenSource cts)
        {

            while (!IstBeendet && !cts.Token.IsCancellationRequested) {

                current = await store.GetAsync(sessionId, cts.Token).ConfigureAwait(false);
                LastVisited = DateTime.Now.ToString();
                RaiseStatus();
                if (!cts.Token.IsCancellationRequested) await Task.Delay(pollDelay, cts.Token).ConfigureAwait(false);
            }

            RaiseStatus();
        }


        private void Cancel()
        {

            CloseCommand?.Execute(null);
        }


        private void RaiseStatus()
        {

            RaisePropertyChanged(nameof(Publisher));
            RaisePropertyChanged(nameof(Percent));
            RaisePropertyChanged(nameof(Status));
            RaisePropertyChanged(nameof(ErgebnisInfo));
            RaisePropertyChanged(nameof(LastUsed));
            RaisePropertyChanged(nameof(Ergebnis));
            RaisePropertyChanged(nameof(IsIndeterminate));
            RaisePropertyChanged(nameof(StatusIndifferent));
            RaisePropertyChanged(nameof(StatusCreated));
            RaisePropertyChanged(nameof(StatusActive));
            RaisePropertyChanged(nameof(StatusDone));
            RaisePropertyChanged(nameof(StatusFailed));
            RaisePropertyChanged(nameof(IstBeendet));
            RaisePropertyChanged(nameof(LastVisited));
            RaisePropertyChanged(nameof(Start));
            RaisePropertyChanged(nameof(CloseCommandName));
        }


        public ICommand CancelCommand   { get; }


        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }


        public String       Publisher       => current?.Publisher;
        public int          Percent         => IsIndeterminate? 0: current.Percent.Value;
        public bool         IsIndeterminate => current == null || !current.Percent.HasValue;
        public String       Status          => current?.Status;
        public String       ErgebnisInfo    => current?.ErgebnisInfo;
        public DateTime?    LastUsed        => current?.LastUsed;
        public bool         IstBeendet      => current != null && (current.Ergebnis == "DONE"     ||
                                                                   current.Ergebnis == "FAILURE"  ||
                                                                   current.Ergebnis == "CANCELLED");


        public String       Start               { get; set; }
        public String       LastVisited         { get; set; }
        public String       CloseCommandName    => IstBeendet? "Schließen": "Abbrechen und Schließen";


        public String Ergebnis
        {
            get {

                if (current == null) return null;
                if (current.Ergebnis == "CONNECTING") return "Verbindungsaufbau";
                if (current.Ergebnis == "CREATED")    return "Wartet auf Bearbeitung";
                if (current.Ergebnis == "ACTIVE")     return "Wird ausgeführt";
                if (current.Ergebnis == "DONE")       return "Erledigt";
                if (current.Ergebnis == "FAILURE")    return "Aufgrund von Fehlern abgebrochen";
                if (current.Ergebnis == "CANCELLED")  return "Aufgrund von Benutzerwunsch abgebrochen";
                return current.Ergebnis;
            }
        }

        public bool StatusIndifferent => !StatusCreated && !StatusActive && !StatusDone && !StatusFailed;
        public bool StatusCreated     => current?.Ergebnis == "CREATED";
        public bool StatusActive      => current?.Ergebnis == "ACTIVE";
        public bool StatusDone        => current?.Ergebnis == "DONE";
        public bool StatusFailed      => current?.Ergebnis == "FAILURE" || current?.Ergebnis == "CANCELLED";
    }
}
