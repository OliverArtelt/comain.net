﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Trackers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;


namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class InsertCommand : IUndoRedoCommand, IResultCommand
    {

        private readonly IMapper mapper;
        private Planaufgabe aufgabe;
        private readonly Planzelle cell;
        private readonly EditAction action;


        public InsertCommand(Planzelle cell, EditAction action, IMapper mapper)
        {

            this.cell = cell;
            this.action = action;
            this.mapper = mapper;
        }


        public void Redo()
        {

            if (aufgabe == null) aufgabe = cell.CreatePosition(action);
            cell.AddPosition(aufgabe);
        }


        public void Undo()
        {
            cell.Aufgaben.Remove(aufgabe);
        }


        public bool ModelHasChanged => true;


        public IEnumerable<PositionSubmitDto> Modifications()
        {

            if (aufgabe == null) yield break;

            var dto = mapper.Map<PositionSubmitDto>(aufgabe);
            dto.ChangeStatus = ChangeStatus.Added;
            yield return dto;
        }
    }
}
