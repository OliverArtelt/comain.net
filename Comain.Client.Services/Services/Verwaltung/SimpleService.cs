﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.ViewData;
using Comain.Client.Trackers;

namespace Comain.Client.Services.Verwaltung
{

    public class SimpleService<TDto, TModel> : ISimpleService<TDto, TModel> where TModel : NotifyBase, new()
                                                                            where TDto : ChangeTrackingBase
    {

        protected readonly IRepository store;
        private readonly IMapper mapper;
        protected TrackableCollection<TModel> currentList;


        public SimpleService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<TrackableCollection<TModel>> GetAsync(String url, CancellationToken ct = default)
        {

            currentList = null;
            var result = await store.GetAsync<IList<TDto>>(url, ct).ConfigureAwait(false);
            var list = mapper.Map<List<TModel>>(result);
            list.Sort();
            currentList = new TrackableCollection<TModel>(list);
            return currentList;
        }


        public bool HasChanges => currentList != null && currentList.IsChanged;


        public async Task SaveAsync(String url, CancellationToken ct = default)
        {

            var result = mapper.AsDtoList<TModel, TDto>(currentList);
            if (result == null) return;

            await store.PostAsync<List<TDto>>(url, result, ct).ConfigureAwait(false);
            currentList.AcceptChanges();
        }


        public void Reset()
        {
            currentList = null;
        }
    }
}
