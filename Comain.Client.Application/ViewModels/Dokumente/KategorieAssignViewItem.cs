﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;

namespace Comain.Client.ViewModels.Dokumente
{

    public class KategorieAssignViewItem : NotifyBase
    {

        private readonly Dokument doc;
        private readonly Kategorie kat;


        public KategorieAssignViewItem(Dokument doc, Kategorie kat)
        {

            this.doc = doc;
            this.kat = kat;
        }


        public String Bezeichnung   => kat.Bezeichnung;


        public bool IsSelected
        {
            get => doc?.Kategorien == null? false: doc.Kategorien.Contains(kat.Id);
            set {

                if (doc?.Kategorien == null) return;

                if (!value) doc.Kategorien.Remove(kat.Id);
                else doc.Kategorien.Add(kat.Id);

                RaisePropertyChanged(nameof(IsSelected));
                doc.RaisePropertyChanged(nameof(Dokument.Kategorien));
            }
        }
    }
}
