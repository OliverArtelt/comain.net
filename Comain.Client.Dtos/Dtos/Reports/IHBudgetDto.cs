﻿namespace Comain.Client.Dtos.Reports
{

    public class IHBudgetDto
    {

        public decimal?     SollEigenleistung    { get; set; }
        public decimal?     SollMaterialkosten   { get; set; }
        public decimal?     SollFremdleistung    { get; set; }
        
        public decimal?     IstEigenleistung     { get; set; }
        public decimal?     IstMaterialkosten    { get; set; }
        public decimal?     IstFremdleistung     { get; set; }

        public decimal?     DivEigenleistung     { get; set; }
        public decimal?     DivMaterialkosten    { get; set; }
        public decimal?     DivFremdleistung     { get; set; }

        public decimal?     ProzEigenleistung    { get; set; }
        public decimal?     ProzMaterialkosten   { get; set; }
        public decimal?     ProzFremdleistung    { get; set; }

        public decimal?     BudgetSoll           { get; set; }
        public decimal?     BudgetIst            { get; set; }
        public decimal?     SummeDifferenz       { get; set; }
        public decimal?     GesamtProzent        { get; set; }

        public decimal      JahrAnteil           { get; set; }
    }
}
