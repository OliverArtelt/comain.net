﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class VerwaltungCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IVerwaltungController);
        public String       Name        => "Stammdaten";
        public String       Parameter   => "Verwaltung";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 1000;
        public String       Color       => "Chocolate";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.cabinet.files.variant.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
