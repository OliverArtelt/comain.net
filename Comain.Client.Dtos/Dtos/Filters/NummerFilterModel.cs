﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.Filters
{

    [DebuggerDisplay("{Nummer}")]
    public class NummerFilterModel : EntityModelBase, IComparable<NummerFilterModel>
    {

        public String   Nummer         { get; set; }
        public String   Name           { get; set; }

        
        public int CompareTo(NummerFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            if (this.Nummer == null && other.Nummer == null) return 0;
            if (this.Nummer == null) return -1;

            return this.Nummer.CompareNatural(other.Nummer);
        }
    }
}
