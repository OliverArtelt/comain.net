﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData.WPlan;
using Comain.Client.ViewModels;

namespace Comain.Client.Controllers.IHObjekte
{

    public interface IMassnahmePluginController : IController
    {

        object      AuftragView             { get; }
        object      MassnahmeView           { get; }

        void        SetEditMode(EditMode mode);
        Task        DetailAsync(IHObjekt ih);
        Task        SaveAsync();
        Task        LoadAsync();

        Task        OpenMassnahmeAsync();
        Task        AddMassnahmeAsync();
        Task        OpenAuftragAsync();
        Task        CreateAufträgeAsync();


        List<MassnahmeViewItem>   GebeMassnahmen();
    }
}
