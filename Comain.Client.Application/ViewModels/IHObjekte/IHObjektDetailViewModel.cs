﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektDetailViewModel : ValidatableViewModel<IIHObjektDetailView, IHObjekt>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Specs, Massnahmen, Artikel, Dokumente, Personal, Memo, Wartung, Historie }


        private IList<IHListDto> ihlist;
        private readonly IModulService module;


        public IHObjektDetailViewModel(IIHObjektDetailView view, IModulService module) : base(view)
        {
            this.module = module;
        }


        public override void CurrentChanged(IHObjekt oldSelected)
        {

            SetzeHeader();

            RaisePropertyChanged(nameof(SpecsVisible));
            RaisePropertyChanged(nameof(MassnahmenVisible));
            RaisePropertyChanged(nameof(DokumentVisible));
            RaisePropertyChanged(nameof(ArtikelVisible));
        }


        protected override void ModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == "Name" && Current != null) {

                IHHeader2 = Current.Name;
                RaisePropertyChanged("IHHeader2");
            }
        }


        private void SetzeHeader()
        {

            if (Current == null) {

                IHHeader1 = null;
                IHHeader2 = null;

            } else if (Current.IstAngefügt) {

                IHHeader1 = "Neues Asset";
                IHHeader2 = null;

            } else {

                var anz = ihlist.Count(p => p.Nummer.StartsWith(Current.Nummer));
                var ihtype = anz > 1? "Hauptobjekt" : (Current.IstMaschine? "Standardobjekt": "Teilobjekt");

                IHHeader1 = String.Format("{0} {1}", ihtype, Current.Nummer);
                IHHeader2 = Current.Name;
            }

            RaisePropertyChanged("IHHeader1");
            RaisePropertyChanged("IHHeader2");
        }


        public void SetIHListe(IList<IHListDto> list)
        {
            ihlist = list;
        }


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public void ResetTabsIfPrint()
        {
            if (Tab == Tabs.Historie) CurrentTab = 0;
        }


#region P R O P E R T I E S


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public String IHHeader1 { get; private set; }
        public String IHHeader2 { get; private set; }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }


        private object basisView;
        public object BasisView
        {
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object artikelView;
        public object ArtikelView
        {
            get { return artikelView; }
            set { SetProperty(ref artikelView, value); }
        }


        private object specView;
        public object SpecView
        {
            get { return specView; }
            set { if (SetProperty(ref specView, value)) RaisePropertyChanged(nameof(SpecsVisible)); }
        }


        private object personalView;
        public object PersonalView
        {
            get => personalView;
            set => SetProperty(ref personalView, value);
        }


        public bool SpecsVisible => module.Info.VerwendetModulSpecs && SpecView != null;


        public bool MassnahmenVisible => module.Info.VerwendetModulWPlan;


        public bool ArtikelVisible => module.Info.VerwendetModulStock;


        private object memoView;
        public object MemoView
        {
            get { return memoView; }
            set { SetProperty(ref memoView, value); }
        }


        private object druckView;
        public object DruckView
        {
            get { return druckView; }
            set { SetProperty(ref druckView, value); }
        }


        private object ereignisView;
        public object EreignisView
        {
            get { return ereignisView; }
            set { SetProperty(ref ereignisView, value); }
        }


        private object auftragView;
        public object AuftragView
        {
            get { return auftragView; }
            set { SetProperty(ref auftragView, value); }
        }


        private object vorgabeView;
        public object VorgabeView
        {
            get { return vorgabeView; }
            set { SetProperty(ref vorgabeView, value); }
        }


        private object dokumentView;
        public object DokumentView
        {
            get { return dokumentView; }
            set { if (SetProperty(ref dokumentView, value)) RaisePropertyChanged(nameof(DokumentVisible)); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public bool DokumentVisible => DokumentView != null;


#endregion

    }
}
