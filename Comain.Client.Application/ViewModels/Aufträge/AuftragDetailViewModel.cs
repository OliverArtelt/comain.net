﻿using System;
using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Views;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragDetailViewModel : ValidatableViewModel<IAuftragDetailView, Auftrag>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Personal, Checkliste, Leistung, Dokumente, Protokoll, Druck }
        private readonly IModulService module;


        public AuftragDetailViewModel(IAuftragDetailView view, IModulService module) : base(view)
        {
            this.module = module;
        }


        public override void CurrentChanged(Auftrag oldSelected)
        {

            RaisePropertyChanged(nameof(PositionenVisible));
            SetzeHeader();
        }


        public void SetzeHeader()
        {

            AuHeader1 = null;
            AuHeader2 = null;

            if (Current == null) return;


            if (Current.AuIHKS != null && !Current.IstAngefügt) {

                if ((Current.Status == Auftragstatus.Offen || Current.Status == Auftragstatus.Überfällig) &&
                     Current.Typ == Auftragstyp.Wartungsplan && Current.UnveränderlicherTermin)
                     AuHeader2 = $"Unveränderlicher Termin, offen: {Current.AuIHKS.Maschine}";
                else AuHeader2 = $"{Current.StatusAsString}: {Current.AuIHKS.Maschine}";
            }


            if (Current.IstAngefügt) AuHeader1 = "Neuer Auftrag";
            else AuHeader1 = $"{Current.TypAsString} {Current.Nummer}";


            Hinweis = Current != null && Current.StatusBearbeitbar && Current.KostenstelleVerlegt?
                "Die Kostenstelle dieses Auftrages wurde verlegt. Dieser Auftrag darf nicht weiter belastet werden.": null;
        }


        public void ResetTabs() => CurrentTab = 0;


        public void ResetTabsIfPrint()
        {
            if (Tab == Tabs.Druck) CurrentTab = 0;
        }


#region P R O P E R T I E S


        private String auHeader1;
        public String AuHeader1
        {
            get => auHeader1;
            set => SetProperty(ref auHeader1, value);
        }


        private String auHeader2;
        public String AuHeader2
        {
            get => auHeader2;
            set => SetProperty(ref auHeader2, value);
        }


        private String hinweis;
        public String Hinweis
        {
            get { return hinweis; }
            set { if (SetProperty(ref hinweis, value)) RaisePropertyChanged("HinweisSichtbar"); }
        }


        public bool HinweisSichtbar { get { return !String.IsNullOrEmpty(hinweis); } }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }


        private object basisView;
        public object BasisView
        {
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object personalView;
        public object PersonalView
        {
            get { return personalView; }
            set { SetProperty(ref personalView, value); }
        }


        private object positionenView;
        public object PositionenView
        {
            get { return positionenView; }
            set { if (SetProperty(ref positionenView, value)) RaisePropertyChanged(nameof(PositionenVisible)); }
        }


        private object protokollView;
        public object ProtokollView
        {
            get { return protokollView; }
            set { SetProperty(ref protokollView, value); }
        }


        private object dokumentView;
        public object DokumentView
        {
            get { return dokumentView; }
            set { if (SetProperty(ref dokumentView, value)) RaisePropertyChanged(nameof(DokumentVisible)); }
        }


        private object leistungView;
        public object LeistungView
        {
            get { return leistungView; }
            set { SetProperty(ref leistungView, value); }
        }


        private object printView;
        public object PrintView
        {
            get { return printView; }
            set { SetProperty(ref printView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool istKunde;
        public bool IstKunde
        {
            get { return istKunde; }
            set { SetProperty(ref istKunde, value); }
        }


        public bool PositionenVisible => module.Info.VerwendetModulWPlan && PositionenView != null && Current != null && Current.Typ == Auftragstyp.Wartungsplan;

        public bool DokumentVisible => DokumentView != null;


#endregion

    }
}
