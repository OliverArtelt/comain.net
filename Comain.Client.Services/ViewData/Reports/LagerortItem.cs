﻿using System;


namespace Comain.Client.ViewData.Reports
{

    public class LagerortItem
    {
        
        public  String   Nummer         { get; set; }
        public  decimal? Restmenge      { get; set; }
    }
}
