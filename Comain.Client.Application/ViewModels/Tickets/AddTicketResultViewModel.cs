﻿using System;
using System.Windows.Input;
using Comain.Client.Services;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Tickets
{
    public class AddTicketResultViewModel : PrintViewModel<IAddTicketResultView>
    {

        public AddTicketResultViewModel(IMessageService messages, IAddTicketResultView view) : base(messages, view)
        {
        }


        public void SetzeSendefehler(string msg)
        {

            Hinweis = null;
            Sendefehler = null;
            if (String.IsNullOrEmpty(msg)) return;

            Hinweis = "Bei der E-Mail-Benachrichtigung des Auftragsleiters ist jedoch ein Fehler aufgetreten.";
            Sendefehler = msg;
        }


#region P R O P E R T I E S


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private String sendefehler;
        public String Sendefehler
        {
            get { return sendefehler; }
            set { SetProperty(ref sendefehler, value); }
        }


        private String hinweis;
        public String Hinweis
        {
            get { return hinweis; }
            set { SetProperty(ref hinweis, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


#endregion

    }
}
