﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{

    /// <summary>
    /// Mehrfacheinlagerungen: Einlagerungen in der Sitzung anzeigen
    /// </summary>
    public class WEItem
    {

        public int          Id                  { get { return Eingang.Id; } }
        public String       Nummer              { get { return Eingang.Nummer; } }
        public String       Artikelnummer       { get { return Eingang.Material.Nummer; } }
        public String       Artikelname         { get { return Eingang.Material.Name; } }
        public decimal?     Bestellt            { get { return Eingang.Bestellposition.Menge; } }
        public decimal?     MengeOK             { get { return Eingang.MengeOK; } }
        public decimal?     Eingelagert         { get { return Eingang.EingelagerteMenge + Einlagerungen.Sum(p => p.Menge); } }
        
        public Wareneingang Eingang             { get; private set; }
        public IEnumerable<Wareneinlagerung> Einlagerungen { get; private set; }


        public WEItem(Wareneingang eingang, IEnumerable<Wareneinlagerung> einlagerungen = null)
        {

            Eingang = eingang;
            Einlagerungen = einlagerungen ?? new List<Wareneinlagerung>();
        }
    }
}
