﻿using System;
using System.Collections;
using System.ComponentModel;


namespace Comain.Client.Views.Internal
{

    public class NaturalComparer : IComparer 
    {

        public String            SortMemberPath { get; set; }
        public ListSortDirection SortDirection  { get; set; }

        
        public int Compare(object x, object y)
        {
       
            var xs = x == null? String.Empty: x.ToString();
            var ys = y == null? String.Empty: y.ToString();

            int i = xs.CompareNatural(ys);
            return SortDirection == ListSortDirection.Ascending? i: -i;
        }
    }
}
