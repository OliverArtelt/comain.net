﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    [DebuggerDisplay("{Nummer}")]
    public class TreeIHViewItem : TreeViewItem
    {

        protected Konfiguration cfg;
        protected IHObjektFarbtyp? Colortype => cfg.IHObjektFarbtyp;


        public TreeIHViewItem(IHFilterItem model, Konfiguration cfg)
        {

            this.cfg = cfg;

            FilterModel = model;
            Level = model.Nummer.Count(p => p == '-');
            if (!IstMaschine) NummerParent = model.Nummer.Substring(0, model.Nummer.LastIndexOf('-'));

            IHNummer = model.Nummer;
            InventarNummer = model.Inventarnummer;
            Name = model.Name;

            PropertyChangedEventManager.AddHandler(model, ModelPropertyChanged, "");
        }


        private void ModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(args.PropertyName);
        }


        public void Add(TreeIHViewItem subItem)
        {

            if (SubItems == null) SubItems = new List<TreeIHViewItem>();
            SubItems.Add(subItem);
        }


        public IHFilterItem         FilterModel     { get; private set; }
        public List<TreeIHViewItem> SubItems        { get; private set; }
        public int                  Level           { get; private set; }
        public String               NummerParent    { get; private set; }


        protected String ihnummer;
        public String IHNummer
        {
            get => ihnummer;
            protected set {

                if (SetProperty(ref ihnummer, value)) {

                    RaisePropertyChanged(nameof(Nummer));
                    RaisePropertyChanged(nameof(NummerVisible));
                }
            }
        }


        protected String invnummer;
        public String InventarNummer
        {
            get => invnummer;
            protected set {

                if (SetProperty(ref invnummer, value)) {

                    RaisePropertyChanged(nameof(Nummer));
                    RaisePropertyChanged(nameof(NummerVisible));
                }
            }
        }


        public String Nummer => cfg.IHNummerAnzeigetyp == IHNummerAnzeigetyp.Nummer? IHNummer:
                                cfg.IHNummerAnzeigetyp == IHNummerAnzeigetyp.Inventarnummer? InventarNummer: null;


        public override bool NummerVisible => !String.IsNullOrEmpty(Nummer);


        protected String name;
        public String Name
        {
            get { return name; }
            protected set { SetProperty(ref name, value); }
        }


        /// <summary>
        /// Schriftgröße
        /// </summary>
        public int Size
        {
            get {

                if (IstMaschine && SubItems == null) return 14;
                if (SubItems == null) return 12;
                return 16;
            }
        }


        public bool IstMaschine
        {
            get { return Level == 3; }
        }


        public override bool IsParentOf(TreeIHViewItem dest)
        {
            return IsAnchestorOf(dest) && Level == dest.Level - 1;
        }


        public bool IsAnchestorOf(TreeIHViewItem dest)
        {
            return dest != null && dest.IHNummer.StartsWith(IHNummer);
        }


        public String Color
        {

            get {

                if (Colortype.HasValue && Colortype.Value == IHObjektFarbtyp.Erhaltklasse)   return ColorFromEk();
                if (Colortype.HasValue && Colortype.Value == IHObjektFarbtyp.IHObjektStatus) return ColorFromSt();
                return "Gray";
            }
        }


        protected virtual String ColorFromEk()
        {
            return ColorFromEk(FilterModel.Erhaltklasse);
        }


        protected virtual String ColorFromSt()
        {
            return ColorFromSt(FilterModel.Status);
        }


        protected static String ColorFromEk(int? klasse)
        {

            if (klasse.GetValueOrDefault() == 4) return "Crimson";
            else if (klasse.GetValueOrDefault() == 3) return "Gold";
            else if (klasse.GetValueOrDefault() == 2) return "ForestGreen";
            else return "Gray";
        }


        protected static String ColorFromSt(int? state)
        {

            if (state.GetValueOrDefault() == 1) return "Crimson";
            else if (state.GetValueOrDefault() == 2) return "Gold";
            else if (state.GetValueOrDefault() == 3) return "ForestGreen";
            else return "Gray";
        }
    }
}
