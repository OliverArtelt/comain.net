﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class StandortViewModel : ValidatableViewModel<IStandortView, Standort>, IDelayableViewModel
    {

        public StandortViewModel(IStandortView view) : base(view)
        {
        } 


        public void Unload()
        {

            Standorte = null;
            Kostenstellen = null;
        }


#region P R O P E R T I E S


        private ObservableCollection<Standort> standorte;
        public ObservableCollection<Standort> Standorte
        { 
            get { return standorte; }
            set { SetProperty(ref standorte, value); }
        }


        private IEnumerable<KstSelektorItem> kostenstellen;
        public IEnumerable<KstSelektorItem> Kostenstellen
        { 
            get { return kostenstellen; }
            set { SetProperty(ref kostenstellen, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
  

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
   

        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
   

        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }
   

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }

       
#endregion   

    }
}
