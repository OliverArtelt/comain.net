﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class BestellReportModel
    {
         
        private readonly Bestellung model;
        private readonly Konfiguration config;

       
        public BestellReportModel(Bestellung model, Konfiguration config)
        {

            this.model = model;
            this.config = config;
            Positionen = model.Positionen.OrderBy(p => p.Position).Select(p => new BestellPosRow(p)).ToList();
        }

        
        public String       Bestellnummer       { get { return model.Nummer; } }
        public String       Auftragsnummer      { get { return model.Auftrag; } }
        public String       Kostenstelle        { get { return model.Kostenstelle; } }
        public String       LieferantName       { get { return model.Lieferant.VollerName; } }
        public String       LieferantPlzUndOrt  { get { return model.Lieferant.PlzUndOrt; } }
        public String       LieferantStrasse    { get { return model.Lieferant.Strasse; } }
        public String       Rechnungsanschrift  { get { return String.IsNullOrEmpty(model.Anschrift2) ? config.Rechnungsanschrift: model.Anschrift2; } }
        public DateTime?    IhrAngebotVom       { get { return model.DatumAngebot; } }
        public String       IhreZeichen         { get { return model.IhreZeichen; } }
        public String       UnsereZeichen       { get { return model.UnsereZeichen; } }
        public String       Telefon             { get { return model.Telefon; } }
        public DateTime?    AusgangAm           { get { return model.DatumAusgang; } }
        public decimal?     Gesamtpreis         { get { return model.Positionen.Sum(p => p.GPreis); } }
        public DateTime?    Liefertermin        { get { return model.Termin; } }
        public String       Lieferanschrift     { get { return String.IsNullOrEmpty(model.Anschrift1) ? config.Lieferanschrift: model.Anschrift1; } }
        public String       Bestellfuss         { get { return config.Bestellfuß; } }


        public List<BestellPosRow> Positionen   { get; private set; }
    }
}
