﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Emma;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models.Users;
using Comain.Client.Models;

namespace Comain.Client.Commands
{

    public class EmmaCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IEmmaController);
        public String       Name        => "EMMA"; 
        public String       Parameter   => "EMMA"; 
        public String       Gruppe      => "Aufträge"; 
        public int          Sortierung  => 10000; 
        public String       Color       => "DodgerBlue"; 
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.connect.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
            => nutzer != null && module.VerwendetModulEmma &&
               (nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstInstandhaltungInstandhalter);    


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
