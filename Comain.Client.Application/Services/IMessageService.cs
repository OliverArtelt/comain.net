﻿using System;
using System.Threading.Tasks;
using Comain.Client.ViewData;


namespace Comain.Client.Services
{

    public interface IMessageService
    {
      
        /// <summary>
        /// Ereignis welches meldet dass eine blockierende Meldung angezeigt werden soll.
        /// </summary>
        /// <remarks>
        /// Der ReportViewer und andere WinForm-Controls werden mit GDI gezeichnet (HWND regions). 
        /// Diese werden nicht von der Meldung überdeckt so dass diese nicht mehr geschlossen werden kann. 
        /// </remarks>
        event EventHandler OverlayRaisedEvent;
        /// <summary>
        /// blockierende MessageBox ist offen
        /// </summary>
        bool DialogIsOpen { get; }
        /// <summary>
        /// nichtbehandelte Ausnahme blockierend anzeigen
        /// </summary>
        void Show(Exception x);
        /// <summary>
        /// allgemeine Meldung blockierend anzeigen
        /// </summary>
        void Show(String body, String header = null);
        /// <summary>
        /// Modul wird verlassen: blockierend nachfragen ob Änderungen verworfen werden sollen (true: Verlassen)
        /// </summary>
        /// <returns>true: Verlassen erlaubt</returns>
        Task<bool> ConfirmExitAsync(String head = null, String body = null);
        /// <summary>
        /// nachfragen ob gelöscht werden soll (true: Löschen)
        /// </summary>
        /// <returns>true: Löschen erlaubt</returns>
        Task<bool> ConfirmDeleteAsync(DeleteToken token);
        /// <summary>
        /// nachfragen ob Aktion ausgeführt werden soll (true: Ausführen)
        /// </summary>
        /// <returns>true: Löschen erlaubt</returns>
        Task<bool> ConfirmAsync(MessageToken token);
        /// <summary>
        /// nichtblockierende Meldung anzeigen (warum eine Aktion nicht ausgeführt werden kann)
        /// </summary>
        Task ShowMessageAsync(MessageToken token);
    }
}
