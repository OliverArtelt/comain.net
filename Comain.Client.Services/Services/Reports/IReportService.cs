﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;

namespace Comain.Client.Services.Reports
{

    public interface IReportService
    {

        Task<IEnumerable<MTListRow>>            MTListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IHBudgetDto>                       IHBudgetAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<EntwIHBudgetDto>>      EntwIHBudgetAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<EntwPkzDto>>           EntwPkzAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IHObjektDto>                       IHObjektAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<LAListDto>>            LAListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<SbSuListDto>>          SBListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<SbSuListDto>>          SUListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<StörDto>                           StörAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<PlanungsgradDto>                   PGAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<LieferantDto>>         LieferantenAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<LbKstDto>>             LeistungKstAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<LbAuDto>>              LeistungAuAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<LbAuDto>>              FbListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<PSListDto>>            PSListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<PsDto>>                PsAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<PsSollIstDto>>         PsSollIstAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<StundenDto>>           StundenAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<AuListDto>>            SimpleAUListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<AuFullListDto>>        FullAUListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<InaktiveIhoDto>>       InaktiveIHAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<Top10Dto>>             Top10IHAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<IHListDto>>            IHListAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<VfGesamtDto>>          GesamtverfügbarkeitAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<VfIhoDto>>             IhVerfügbarkeitAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IEnumerable<AnwesenheitReportDto>> AnwesenheitenAsync(Suchkriterien filter, CancellationToken ct = default);
    }
}
