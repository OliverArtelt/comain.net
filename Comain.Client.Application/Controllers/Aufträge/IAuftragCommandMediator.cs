﻿using System;
using Comain.Client.Models;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Aufträge
{

    public interface IAuftragCommandMediator
    {
        
        EditMode CurrentMode { get; }

        void Initialize(Konfiguration config);
        void SetEditMode(EditMode newMode);
        void NeuSetzen();
    }
}
