﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Services.IHObjekte;
using Comain.Client.Views.Bevor;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;


namespace Comain.Client.ViewModels.Bevor
{

    public class BevorResultViewModel : ViewModel<IBevorResultView>
    {

        public BevorResultViewModel(IBevorResultView view)
          : base(view)
        {
        }


        public void Unload()
        {
        }


        public void SetzeSendefehler(string msg)
        {

            Hinweis = null;
            Sendefehler = null;
            if (String.IsNullOrEmpty(msg)) return;

            Hinweis = "Bei der E-Mail-Benachrichtigung des Auftragsleiters ist jedoch ein Fehler aufgetreten.";
            Sendefehler = msg;
        }


#region P R O P E R T I E S


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private String sendefehler;
        public String Sendefehler
        {
            get { return sendefehler; }
            set { SetProperty(ref sendefehler, value); }
        }


        private String hinweis;
        public String Hinweis
        {
            get { return hinweis; }
            set { SetProperty(ref hinweis, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


#endregion

    }
}
