﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class ReklamReportModel
    {
         
        private readonly Reklamation model;
        private readonly Konfiguration config;

       
        public ReklamReportModel(Reklamation model, Konfiguration config)
        {

            this.model = model;
            this.config = config;
            Positionen = model.Positionen.OrderBy(p => p.Position).Select(p => new ReklamPosRow(p)).ToList();
        }

        
        public String       Bestellnummer       { get { return model.Bestellung.Nummer; } }
        public String       Lieferschein        { get { return model.Lieferschein.Nummer; } }
        public String       Auftragsnummer      { get { return model.Bestellung.Auftrag; } }
        public String       Kostenstelle        { get { return model.Bestellung.Kostenstelle; } }
        public String       LieferantName       { get { return model.Bestellung.Lieferant.VollerName; } }
        public String       LieferantPlzUndOrt  { get { return model.Bestellung.Lieferant.PlzUndOrt; } }
        public String       LieferantStrasse    { get { return model.Bestellung.Lieferant.Strasse; } }
        public String       Rechnungsanschrift  { get { return String.IsNullOrEmpty(model.Bestellung.Anschrift2) ? config.Rechnungsanschrift: model.Bestellung.Anschrift2; } }
        public String       Bemerkungen         { get { return model.Notizen; } }
        public String       IhreZeichen         { get { return model.Bestellung.IhreZeichen; } }
        public String       UnsereZeichen       { get { return model.Bestellung.UnsereZeichen; } }
        public String       Telefon             { get { return model.Bestellung.Telefon; } }
        public String       Lieferanschrift     { get { return String.IsNullOrEmpty(model.Bestellung.Anschrift1) ? config.Lieferanschrift: model.Bestellung.Anschrift1; } }
        public String       Bestellfuss         { get { return config.Bestellfuß; } }
        public DateTime     GeliefertAm         { get { return model.Lieferschein.Datum; } }


        public List<ReklamPosRow> Positionen   { get; private set; }
    }
}
