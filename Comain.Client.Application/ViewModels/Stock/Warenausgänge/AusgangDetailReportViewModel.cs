﻿using Comain.Client.Services;
using Comain.Client.Views.Stock.Warenausgänge;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{
    
    public class AusgangDetailReportViewModel : PrintViewModel<IAusgangDetailReportView>, IDelayableViewModel
    {

        public AusgangDetailReportViewModel(IMessageService messages, IAusgangDetailReportView view) : base(messages, view)
        {
        }
    }
}
