﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewModels;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanDetailCreateViewModel : ViewModel<IPlanDetailCreateView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public PlanDetailCreateViewModel(IPlanDetailCreateView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private ICommand createCommand;
        public ICommand CreateCommand
        {
            get { return createCommand; }
            set { SetProperty(ref createCommand, value); }
        }


        private ICommand testCreateCommand;
        public ICommand TestCreateCommand
        {
            get { return testCreateCommand; }
            set { SetProperty(ref testCreateCommand, value); }
        }


        private int auftragsanzahl;
        public int Auftragsanzahl
        {
            get { return auftragsanzahl; }
            set { SetProperty(ref auftragsanzahl, value); }
        }


        private int positionsanzahl;
        public int Positionsanzahl
        {
            get { return positionsanzahl; }
            set { SetProperty(ref positionsanzahl, value); }
        }


        private bool planStatusVisible;
        public bool PlanStatusVisible
        {
            get { return planStatusVisible; }
            set { SetProperty(ref planStatusVisible, value); }
        }


        private object progressView;
        public object ProgressView
        {
            get { return progressView; }
            set { SetProperty(ref progressView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
