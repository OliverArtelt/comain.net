﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.Services.Stock.Wareneinlagerungen;
using Comain.Client.ViewModels.Stock.Wareneinlagerungen;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Wareneinlagerungen
{

    public class EinlagerController : ControllerBase, IEinlagerController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IEinlagerService store;
        private readonly MainViewModel mainView;
        private readonly EinlagerViewModel view;
        private readonly EinlagerListViewModel listView;
        private readonly EinlagerFilterViewModel filterView;
        private readonly EinlagerDetailViewModel detailView;
        private readonly BestellabschlussViewModel abschlussView;
        private readonly EinlagerEingangViewModel addEingangView;
        private readonly EinlagerArtikelViewModel addArtikelView;
        private TrackableCollection<Wareneinlagerung> currentList;
        private SerializableCommand deleteCommand;


        public EinlagerController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                  IEinlagerService store, IControllerBus bus,
                                  MainViewModel mainView, EinlagerViewModel view, EinlagerListViewModel listView, EinlagerFilterViewModel filterView,
                                  EinlagerDetailViewModel detailView, BestellabschlussViewModel abschlussView,
                                  EinlagerEingangViewModel addEingangView, EinlagerArtikelViewModel addArtikelView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.abschlussView = abschlussView;
            this.addEingangView = addEingangView;
            this.addArtikelView = addArtikelView;
        }


        public void Initialize()
        {

            view.FilterView = filterView.View;
            view.ListView = listView.View;
            view.DetailView = detailView.View;

            filterView.SearchCommand = CreateCommand(_ => SearchAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            listView.DetailCommand = CreateCommand(_ => DetailAsync());
            view.AddFromEingangCommand = CreateCommand(_ => AddFromEingangAsync());
            view.AddFromArtikelCommand = CreateCommand(_ => AddFromArtikelAsync());
            view.CloseCommand = CreateCommand(_ => CloseAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => detailView.Current != null);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Wareneinlagerungen");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        public void Shutdown()
        {}


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await ConfirmAsync()) return false;

            listView.Unload();
            detailView.Unload();
            filterView.Unload();
            abschlussView.Unload();
            addEingangView.Unload();
            currentList = null;
            detailView.Current = null;

            return true;
        }


        private async Task<bool> ConfirmAsync()
        {

            if (currentList == null || !currentList.IsChanged) return true;
            return await messages.ConfirmExitAsync();
        }


        private async Task LoadAsync()
        {

            currentList = null;

            using (new VisualDelay(view)) {

                var lotask = store.LagerorteAsync(bus.Token);
                var mttask = store.ArtikelAsync(bus.Token);
                var wetask = store.EingängeAsync(bus.Token);
                var lgtask = store.LagerfähigeEingängeAsync(bus.Token);

                await Task.WhenAll(mttask, lotask, wetask, lgtask).ConfigureAwait(false);
                await SearchAsync();

                bus.Token.ThrowIfCancellationRequested();
                var lagerorte = lotask.Result.OrderBy(p => p.Nummer).ToList();

                await context.SendAsync(new SendOrPostCallback((o) => {

                    detailView.Lagerorte = lagerorte;
                    filterView.Lagerorte = lagerorte;
                    filterView.Eingänge = wetask.Result;
                    addEingangView.SetFoundList(lgtask.Result);
                    addArtikelView.SetzeArtikelliste(mttask.Result);

                }), null);
            }
        }


        private async Task SearchAsync()
        {

            if (!await ConfirmAsync()) return;

            var oldlistmodel = listView.Current;
            detailView.Current = null;
            currentList = null;
            listView.Current = null;

            currentList = await store.SearchAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
            filterView.SetzeAnzahlGefunden(currentList.Count);
            await context.SendAsync(new SendOrPostCallback((o) => { listView.FoundList = currentList; }), null);

            if (!currentList.IsNullOrEmpty()) {

                if (oldlistmodel != null) listView.Current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
            }

            await DetailAsync();
        }


        private async Task DetailAsync()
        {

            detailView.Current = listView.Current;
            detailView.LetzterLagerort = null;
            detailView.LetzteInventur = null;

            if (detailView.Current != null) {

                var lo = await store.LetzerLagerortAsync(detailView.Current.Material.Id, bus.Token);
                if (lo != null) detailView.LetzterLagerort = lo.Nummer;

                var iv = await store.LetzeInventurAsync(detailView.Current.Id, bus.Token);
                detailView.LetzteInventur = iv == null? (DateTime?)null: iv.Datum;
            }

            UpdateCommands();
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
            }), null);
        }


        private async Task SaveAsync()
        {

            using (new VisualDelay(view)) {

                if (currentList == null || !currentList.IsChanged) return;
                await store.SaveAsync(currentList, bus.Token).ConfigureAwait(false);
                await LoadAsync();
            }
        }


        private async Task AddFromEingangAsync()
        {

            addEingangView.TakeCommand = flyouts.YesCommand;
            addEingangView.SetAvailableList(currentList.Where(p => p.IstAngefügt));
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = addEingangView.View, Header = "Wareneingang einlagern",
                                                Position = MahApps.Metro.Controls.Position.Right })) return;
            if (addEingangView.Current == null) return;

            var wl = new Wareneinlagerung(addEingangView.Current.Eingang);
            var lo = await store.LetzerLagerortAsync(addEingangView.Current.Eingang.Material.Id, bus.Token);
            if (lo != null) wl.Lagerort = detailView.Lagerorte.FirstOrDefault(p => p.Id == lo.Id);

            await context.SendAsync(new SendOrPostCallback((o) => {

                currentList.Add(wl);
                listView.Current = wl;
                detailView.Current = wl;
                detailView.LetzterLagerort = lo?.Nummer;
            }), null);
        }


        private async Task AddFromArtikelAsync()
        {

            addArtikelView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = addArtikelView.View, Header = "Artikel frei einlagern", Position = MahApps.Metro.Controls.Position.Right })) return;
            if (addArtikelView.Current == null) return;

            var wl = new Wareneinlagerung(addArtikelView.Current);
            var lo = await store.LetzerLagerortAsync(addArtikelView.Current.Id, bus.Token);
            if (lo != null) wl.Lagerort = detailView.Lagerorte.FirstOrDefault(p => p.Id == lo.Id);

            await context.SendAsync(new SendOrPostCallback((o) => {

                currentList.Add(wl);
                listView.Current = wl;
                detailView.Current = wl;
                detailView.LetzterLagerort = lo == null? null: lo.Nummer;
            }), null);
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current != null) {

                await context.SendAsync(new SendOrPostCallback((o) => {

                    currentList.Remove(detailView.Current);
                }), null);
            }
        }


        private async Task CloseAsync()
        {

            var list = await store.AbschlussfähigeBestellungenAsync(bus.Token).ConfigureAwait(false);
            if (list.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Es wurden keine abschlussfähighen Bestellungen gefunden." });
                return;
            }

            abschlussView.ExecuteCommand = flyouts.YesCommand;
            abschlussView.Bestellungen = list;

            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = abschlussView.View, Header="Bestellungen abschliessen", Position = MahApps.Metro.Controls.Position.Right })) return;

            await store.BestellungenAbschliessenAsync(list.Select(p => p.Id).ToList());
        }
    }
}
