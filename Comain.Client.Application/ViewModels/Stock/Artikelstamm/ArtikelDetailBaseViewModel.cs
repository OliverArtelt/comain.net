﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Artikelstamm;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Specs;
using Comain.Client.Dtos.Specs;

namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class ArtikelDetailBaseViewModel : ValidatableViewModel<IArtikelDetailBaseView, Artikel>
    {

        private readonly SynchronizationContext context;


        public ArtikelDetailBaseViewModel(IArtikelDetailBaseView view, ISpecSearchProvider specSearcher) : base(view)
        {

            context = SynchronizationContext.Current;
            SpecSearchProvider = specSearcher;
        }


        public override void CurrentChanged(Artikel oldSelected)
        {

            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Herstellernummer));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(KlasseUndefiniert));
            RaisePropertyChanged(nameof(KlasseA));
            RaisePropertyChanged(nameof(KlasseB));
            RaisePropertyChanged(nameof(KlasseC));
            RaisePropertyChanged(nameof(Mischpreis));
            RaisePropertyChanged(nameof(Mindestbestand));
            RaisePropertyChanged(nameof(BestandReal));
            RaisePropertyChanged(nameof(Bestellmenge));
            RaisePropertyChanged(nameof(Mindestbestellmenge));
            RaisePropertyChanged(nameof(Einheit));
            RaisePropertyChanged(nameof(Warengruppe));
            RaisePropertyChanged(nameof(Beschreibung));
            RaisePropertyChanged(nameof(MaterialIHObjektMap));
            RaisePropertyChanged(nameof(Spezifikation));


            context.Send(new SendOrPostCallback((o) => {

                IHObjekte = null;
                if (Current != null && Current.MaterialIHObjektMap != null) {

                    IHObjekte = new CollectionViewSource { Source = Current.MaterialIHObjektMap }.View;
                    IHObjekte.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                    IHObjekte?.Refresh();
                    RaisePropertyChanged("IHObjekte");
                }

            }), null);
        }


        public void SetzeListen(IEnumerable<NummerFilterModel> wglist, IEnumerable<NummerFilterModel> ehlist, IEnumerable<SpecFilterModel> splist)
        {

            Warengruppen = wglist;
            Einheiten = ehlist;
            SpecSearchProvider.SetItems(splist);
        }


        public void Unload()
        {

            Warengruppen = null;
            Einheiten = null;
            SpecSearchProvider.ClearItems();
        }


#region P R O P E R T I E S


        public ISpecSearchProvider SpecSearchProvider  { get; }


        public SpecFilterModel Spezifikation
        {
            get { return current == null? null: current.Spezifikation; }
            set {

                if (current != null && current.Spezifikation != value) {

                    current.Spezifikation = value;
                    RaisePropertyChanged(nameof(Spezifikation));
                }
            }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public String Nummer
        {
            get { return current == null? null: current.Nummer; }
            set {

                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }


        public String Herstellernummer
        {
            get { return current == null? null: current.Herstellernummer; }
            set {

                if (current != null && current.Herstellernummer != value) {

                    current.Herstellernummer = value;
                    RaisePropertyChanged("Herstellernummer");
                }
            }
        }


        public String Name
        {
            get { return current == null? null: current.Name; }
            set {

                if (current != null && current.Name != value) {

                    current.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }


        public String Beschreibung
        {
            get { return current == null? null: current.Beschreibung; }
            set {

                if (current != null && current.Beschreibung != value) {

                    current.Beschreibung = value;
                    RaisePropertyChanged("Beschreibung");
                }
            }
        }


        public decimal? Mischpreis
        {
            get { return current == null? null: current.Mischpreis; }
            set {

                if (current != null && current.Mischpreis != value) {

                    current.Mischpreis = value;
                    RaisePropertyChanged("Mischpreis");
                }
            }
        }


        public decimal? Mindestbestand
        {
            get { return current == null? null: current.Mindestbestand; }
            set {

                if (current != null && current.Mindestbestand != value) {

                    current.Mindestbestand = value;
                    RaisePropertyChanged("Mindestbestand");
                }
            }
        }


        public decimal? Mindestbestellmenge
        {
            get { return current == null? null: current.Mindestbestellmenge; }
            set {

                if (current != null && current.Mindestbestellmenge != value) {

                    current.Mindestbestellmenge = value;
                    RaisePropertyChanged("Mindestbestellmenge");
                }
            }
        }


        public decimal? BestandReal
        {
            get { return current == null? null: current.BestandReal; }
            set {

                if (current != null && current.BestandReal != value) {

                    current.BestandReal = value;
                    RaisePropertyChanged("BestandReal");
                }
            }
        }


        public decimal? Bestellmenge
        {
            get { return current == null? null: current.Bestellmenge; }
            set {

                if (current != null && current.Bestellmenge != value) {

                    current.Bestellmenge = value;
                    RaisePropertyChanged("Bestellmenge");
                }
            }
        }


        public bool KlasseUndefiniert
        {
            get { return current == null? false: !current.Klasse.HasValue; }
            set {

                if (current != null && value && current.Klasse.HasValue) {

                    current.Klasse = null;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseA
        {
            get { return current == null? false: current.Klasse == Materialklasse.A; }
            set {

                if (current != null && value && current.Klasse != Materialklasse.A) {

                    current.Klasse = Materialklasse.A;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseB
        {
            get { return current == null? false: current.Klasse == Materialklasse.B; }
            set {

                if (current != null && value && current.Klasse != Materialklasse.B) {

                    current.Klasse = Materialklasse.B;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public bool KlasseC
        {
            get { return current == null? false: current.Klasse == Materialklasse.C; }
            set {

                if (current != null && value && current.Klasse != Materialklasse.C) {

                    current.Klasse = Materialklasse.C;
                    RaisePropertyChanged("KlasseUndefiniert");
                    RaisePropertyChanged("KlasseA");
                    RaisePropertyChanged("KlasseB");
                    RaisePropertyChanged("KlasseC");
                }
            }
        }


        public NummerFilterModel Einheit
        {
            get { return current == null? null: current.Einheit; }
            set {

                if (current != null && current.Einheit != value) {

                    current.Einheit = value;
                    RaisePropertyChanged("Einheit");
                }
            }
        }


        public NummerFilterModel Warengruppe
        {
            get { return current == null? null: current.Warengruppe; }
            set {

                if (current != null && current.Warengruppe != value) {

                    current.Warengruppe = value;
                    RaisePropertyChanged("Warengruppe");
                }
            }
        }


        public ICollection MaterialIHObjektMap
        {
            get { return current == null? null: current.MaterialIHObjektMap; }
        }


        private IEnumerable einheiten;
        public IEnumerable Einheiten
        {
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


        private IEnumerable warengruppen;
        public IEnumerable Warengruppen
        {
            get { return warengruppen; }
            set { SetProperty(ref warengruppen, value); }
        }


        private ICollectionView iHObjekte;
        public ICollectionView IHObjekte
        {
            get { return iHObjekte; }
            set { SetProperty(ref iHObjekte, value); }
        }


        private ICommand ihObjektCommand;
        public ICommand IhObjektCommand
        {
            get { return ihObjektCommand; }
            set { SetProperty(ref ihObjektCommand, value); }
        }


#endregion

    }
}
