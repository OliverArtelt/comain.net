﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class PzDto : ChangeTrackingBase
    {
        
        public int          Nummer                  { get; set; }
        public String       Name                    { get; set; }
        public String       Kurzname                { get; set; }
        public bool         IstKundenstandort       { get; set; }
    }
}
