﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Gateways;
using Comain.Client.ViewData;

namespace Comain.Client.Services
{

    public class JobService : IJobService
    {

        private readonly IRepository store;

        
        public JobService(IRepository store)
        {
            this.store = store;
        }


        public Task<JobStatus> GetAsync(String sessionId, CancellationToken ct = default)
 	        => store.GetAsync<JobStatus>($"/JobStatus/{sessionId}", ct);


        public Task CancelAsync(String sessionId, CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
