﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;
using Comain.Client.ReportModels;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Reports.Types
{
   
    public class VFIHObjektReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly VerfügbarkeitViewModel typView;


        public VFIHObjektReport(IReportService store, IHJahrViewModel jahrView, VerfügbarkeitViewModel typView)
        {

            this.store = store;
            this.typView = typView;
            filterViews = new List<IFilterViewModel> { jahrView, typView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var filter = GebeFilter();
            var type   = typView.Typ;
            var title  = TitlePerType(type);

            var result = new ReportResult { Title = title, ReportAddress = ReportAddress, AssemblyAddress = ReportAssembly };
            result.AddParameter("parTitle",     title);
            result.AddParameter("parFilter",    String.Join("; ", filterViews.Select(p => p.FilterAlsString()).Where(p => !String.IsNullOrWhiteSpace(p)).ToArray()));
            result.AddParameter("parFormel",    FormulaPerType(type));
            result.AddParameter("parSollzeit",  SollZeitPerType(type));
            result.AddParameter("parIstzeit",   IstZeitPerType(type));
            result.AddParameter("parWert",      WertPerType(type));

            var data = await store.IhVerfügbarkeitAsync(filter, ct);
            result.AddDataSource("MainData", data.Select(p => new VfIhoRow(p, type)));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            
            return false;
            //return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public string TitlePerType(String type)
        {
        
            if (type == "Verfügbarkeit")    return "Technische Verfügbarkeit";     
            if (type == "Ausfallrate")      return "Technische Ausfallrate";     
            if (type == "Abweichungsgrad")  return "Abweichungsgrad IH - bed. Ausfallzeit in %";
            return null;     
        }


        public string FormulaPerType(String type)
        {
        
            if (type == "Verfügbarkeit")    return "Vt = (1 - Tt / Tb) * 100%";     
            if (type == "Ausfallrate")      return "At = Tt / Tb * 100%";     
            if (type == "Abweichungsgrad")  return "Aa = (Sollausfallzeit - Istausfallzeit) / Sollausfallzeit * 100%";
            return null;     
        }


        public string SollZeitPerType(String type)
        {
        
            if (type == "Verfügbarkeit")    return "Belegungszeit Soll[h]";     
            if (type == "Ausfallrate")      return "Belegungszeit Soll[h]";     
            if (type == "Abweichungsgrad")  return "Sollausfallzeit [h]";
            return null;     
        }


        public string IstZeitPerType(String type)
        {
        
            if (type == "Verfügbarkeit")    return "Störzeit Ist[h]";     
            if (type == "Ausfallrate")      return "Störzeit Ist[h]";     
            if (type == "Abweichungsgrad")  return "Istausfallzeit [h]";
            return null;     
        }


        public string WertPerType(String type)
        {
        
            if (type == "Verfügbarkeit")    return "Technische Verfügbarkeit";     
            if (type == "Ausfallrate")      return "Technische Ausfallrate";     
            if (type == "Abweichungsgrad")  return "Abweichungsgrad";
            return null;     
        }


        public override string Title
        {
            get { return "Asset-Verfügbarkeit"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 420; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.VFIHObjektReport.rdlc"; }
        }
    }
}
