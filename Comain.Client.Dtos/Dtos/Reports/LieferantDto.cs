﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class LieferantDto
    {

        public String   Name        { get; set; }
        public String   Strasse     { get; set; }
        public String   Plz         { get; set; }
        public String   Ort         { get; set; }
        public String   Telefon     { get; set; }
        public String   Fax         { get; set; }
        public String   EMail       { get; set; }
    }
}
