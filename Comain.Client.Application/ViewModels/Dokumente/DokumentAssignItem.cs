﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;

namespace Comain.Client.ViewModels.Dokumente
{

    public class DokumentAssignItem : NotifyBase
    {

        private readonly Dokument model;


        public DokumentAssignItem(Dokument model, Dictionary<int, Kategorie> kdic = null)
        {

            this.model = model;
            if (!model.Kategorien.IsNullOrEmpty() && kdic != null)
                Kategorien = String.Join(", ", model.Kategorien.Select(p => kdic.GetValueOrDefault(p)?.Bezeichnung).Where(p => p != null));
        }


        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value);
        }


        public bool PräsentiertModell
        {
            get => model == null? false: model.PräsentiertModell;
            set {

                if (model != null && value != model.PräsentiertModell) {

                    model.PräsentiertModell = value;
                    RaisePropertyChanged(nameof(PräsentiertModell));
                }
            }
        }


        private Media media;
        public Media Media
        {
            get => media;
            set { if (SetProperty(ref media, value)) RaisePropertyChanged(nameof(Thumbnail)); }
        }


        private bool isAlreadyAssigned;
        public bool IsAlreadyAssigned
        {
            get => isAlreadyAssigned;
            set => SetProperty(ref isAlreadyAssigned, value);
        }


        public Dokument         Model                          => model;
        public int              Id                             => model.Id;
        /// <summary>
        /// kann bearbeitet werden weil Dokument nicht von anderen Objekten aus eingeblendet wurde
        /// </summary>
        public bool             IstDirektBezug                 => model.IstDirektBezug || model.IstAngefügt;
        public bool             AlsBildDarstellbar             => model.AlsBildDarstellbar;
        public String           Quelltyp                       => model.Quelltyp.AsText();
        public String           Quellobjekt                    => model.Quelle;
        public String           Bezeichnung                    => model.Bezeichnung;
        public String           Filename                       => model.Filename;
        public String           Kategorien                     { get; }
        public String           Details                        => $"Erstellt am {model.ErstelltAm.ToShortDateString()}";
        public BitmapSource     Thumbnail                      => Media?.Thumbnail;
        public byte[]           Content                        => model.Content;
        public void             SetDocumentFile(byte[] buffer) => model.SetDocumentFile(buffer);
        public String           TempPath                       => model.TempPath;
        public Task             StoreFileAsync(String path)    => model.StoreFileAsync(path);
        public Task             StoreTempFileAsync()           => StoreFileAsync(TempPath);


        public void SetAssigned(HashSet<int> assignedIds)
            => IsAlreadyAssigned = assignedIds.Contains(Id);
    }
}
