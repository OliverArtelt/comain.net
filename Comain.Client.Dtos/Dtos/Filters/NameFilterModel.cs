﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class NameFilterModel : EntityModelBase
    {

        public String   Name        { get; set; }
    }
}
