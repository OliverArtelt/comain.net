﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.WPlan.Reports
{
    public interface IPlanOutput
    {

        bool    BenötigtPlan            { get; }
        String  Name                    { get; }
        String  Text                    { get; }
        int     Sortierung              { get; }

        /// <summary>
        /// mit Stunden, mit Preise ...
        /// </summary>
        bool    UseFilterAusgabe        { get; } 
        /// <summary>
        /// Bedarfspositionen, Sonderleistungen, ...
        /// </summary>
        bool    UseFilterLeistungstyp   { get; }
        /// <summary>
        /// offen, erledigt, ...
        /// </summary>
        bool    UseFilterErgebnis       { get; }
    }
}
