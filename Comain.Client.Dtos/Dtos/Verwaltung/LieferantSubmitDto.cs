﻿using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class LieferantSubmitDto
    {

        public List<LieferantDto>   Lieferanten     { get; set; }
        public List<LieferantAPDto> LieferantenAP   { get; set; }
    }
}
