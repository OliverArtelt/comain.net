﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Models.Stock
{

    public class Bestellung : Entity, IDataErrorInfo, IComparable<Bestellung>
    {
     
        private String                       nummer;
        private Bestellstatus                status;
        private String                       auftrag;
        private String                       kostenstelle;
        private String                       ihreZeichen;
        private String                       telefon;
        private String                       unsereZeichen;
        private String                       anschrift1;
        private String                       anschrift2;
        private String                       notizen;
        private DateTime                     datumAnlage;
        private DateTime?                    datumAusgang;
        private DateTime?                    datumAngebot;
        private DateTime?                    termin;
        private Lieferant                    lieferant;
        private LieferantAP                  ansprechpartner;
        private PersonalFilterModel          bearbeiter;
        private ICollection<ReklamDto>       reklamationen;
        private NotifyTrackableCollection<Bestellposition> positionen;


        public decimal? GPreis
        {

            get {

                if (positionen.IsNullOrEmpty()) return null;
                return positionen.Sum(p => p.GPreis);
            }
        }


        public bool IsEditable => Status == Bestellstatus.Undefiniert || Status == Bestellstatus.Angelegt || Status == Bestellstatus.Offen || Status == Bestellstatus.Überfällig;
        public bool IstOffen   => Status == Bestellstatus.Offen || Status == Bestellstatus.Abschlussfähig || Status == Bestellstatus.Überfällig; 


#region P R O P E R T I E S


        public String Nummer           
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public Bestellstatus Status           
        {
            get { return status; }
            set { SetProperty(ref status, value); }
        }
        

        public String Auftrag          
        {
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }
        

        public String Kostenstelle     
        {
            get { return kostenstelle; }
            set { SetProperty(ref kostenstelle, value); }
        }
        

        public String IhreZeichen      
        {
            get { return ihreZeichen; }
            set { SetProperty(ref ihreZeichen, value); }
        }
        

        public String Telefon          
        {
            get { return telefon; }
            set { SetProperty(ref telefon, value); }
        }
        

        public String UnsereZeichen    
        {
            get { return unsereZeichen; }
            set { SetProperty(ref unsereZeichen, value); }
        }
        
        /// <summary>
        /// Lieferanschrift
        /// </summary>
        public String Anschrift1       
        {
            get { return anschrift1; }
            set { SetProperty(ref anschrift1, value); }
        }
        
        /// <summary>
        /// Rechnungsanschrift
        /// </summary>
        public String Anschrift2       
        {
            get { return anschrift2; }
            set { SetProperty(ref anschrift2, value); }
        }
        

        public String Notizen          
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }
        

        public DateTime DatumAnlage      
        {
            get { return datumAnlage; }
            set { SetProperty(ref datumAnlage, value); }
        }
        

        public DateTime? DatumAusgang     
        {
            get { return datumAusgang; }
            set { SetProperty(ref datumAusgang, value); }
        }
        

        public DateTime? DatumAngebot     
        {
            get { return datumAngebot; }
            set { SetProperty(ref datumAngebot, value); }
        }
        

        public DateTime? Termin           
        {
            get { return termin; }
            set { SetProperty(ref termin, value); }
        }
        

        public Lieferant Lieferant        
        {
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }
        

        public LieferantAP Ansprechpartner  
        {
            get { return ansprechpartner; }
            set { SetProperty(ref ansprechpartner, value); }
        }
        

        public PersonalFilterModel Bearbeiter       
        {
            get { return bearbeiter; }
            set { SetProperty(ref bearbeiter, value); }
        }
        

        public ICollection<ReklamDto> Reklamationen      
        {
            get { return reklamationen; }
            set { SetProperty(ref reklamationen, value); }
        }
        

        public NotifyTrackableCollection<Bestellposition> Positionen       
        {
            
            get {

                if (positionen == null) positionen = new NotifyTrackableCollection<Bestellposition>();
                return positionen;
            }
            set { SetProperty(ref positionen, value); }
        }
        

#endregion

#region V A L I D A T I O N


        public List<String> ErrorListWithLieferant(IList<Artikel> artikelliste)
        {

            var list = ErrorList;
           
            //Positionen werden in ErrorList bereits geprüft 
            if (Positionen.IsNullOrEmpty()) return list;
            
            //Lieferanten prüfen
            foreach (var pos in Positionen) {
            
                var at = artikelliste.FirstOrDefault(q => q.Id == pos.Material_Id);
                
                if (at == null) {
                
                    list.Add("Mindestens ein Artikel wird nicht geführt.");
                    return list;
                }
                    
                if (at.MaterialLieferantMap.All(r => r.Lieferant.Id != Lieferant.Id)) {
                
                    list.Add("Mindestens ein Artikel wird vom Lieferanten nicht bezogen.");
                    return list;
                }
            } 

            return list;
        }

       
        public string Error
        {
            
            get { 
            
                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Auftrag"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kostenstelle"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Lieferant"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["IhreZeichen"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["UnsereZeichen"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Telefon"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Bearbeiter"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Positionen"];
                return test;
            }
        }

       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Auftrag"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kostenstelle"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Bearbeiter"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Lieferant"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Positionen"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["IhreZeichen"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["UnsereZeichen"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Telefon"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return null;
                    if (Nummer.Length > 32) return "Die Nummer darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "Auftrag":

                    if (String.IsNullOrWhiteSpace(Auftrag)) return null;
                    if (Auftrag.Length > 32) return "Die Auftragsnummer darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "Kostenstelle":

                    if (String.IsNullOrWhiteSpace(Kostenstelle)) return "Die Kostenstelle muss angegeben werden.";
                    if (Kostenstelle.Length > 32) return "Die Kostenstelle darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "Bearbeiter":

                    if (Bearbeiter == null) return "Der Bearbeiter muss angegeben werden.";
                    break;

                case "Lieferant":

                    if (Lieferant == null) return "Der Lieferant muss angegeben werden.";
                    break;

                case "IhreZeichen":

                    if (String.IsNullOrWhiteSpace(IhreZeichen)) return null;
                    if (IhreZeichen.Length > 16) return "Das Feld 'Ihre Zeichen' darf aus maximal 16 Zeichen bestehen."; 
                    break;

                case "UnsereZeichen":

                    if (String.IsNullOrWhiteSpace(UnsereZeichen)) return null;
                    if (UnsereZeichen.Length > 32) return "Das Feld 'Unsere Zeichen' darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "Telefon":

                    if (String.IsNullOrWhiteSpace(Telefon)) return null;
                    if (Telefon.Length > 48) return "Die Telefondurchwahl darf aus maximal 48 Zeichen bestehen."; 
                    break;

                case "Positionen":

                    if (Positionen.IsNullOrEmpty()) return "Die Bestellung enthält keine Positionen.";
                    var pos = Positionen.FirstOrDefault(p => !String.IsNullOrEmpty(p.Error));
                    if (pos != null) return pos.Error; 
                    break;
                }

                return null;
            }
        }


#endregion

             
        public int CompareTo(Bestellung other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }   
    }
}
