﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Factories;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Reports;
using Comain.Client.Dtos.Filters;
using ILieferantenSearchProvider = Comain.Client.ViewModels.Verwaltung.ILieferantenSearchProvider;


namespace Comain.Client.ViewModels.Reports
{

    public class LagerFilterViewModel : ViewModel<ILagerFilterView>
    {

        private readonly SynchronizationContext context;
        private readonly IHSelektorViewModel ihFilter;


        public LagerFilterViewModel(IHSelektorViewModel ihFilter, ILagerFilterView view, ILieferantenSearchProvider lfSearcher) : base(view)
        {

            context = SynchronizationContext.Current;
            this.ihFilter = ihFilter;
            LfSearchProvider = lfSearcher;
            ResetCommand = new DelegateCommand(Reset);
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            if (!String.IsNullOrEmpty(searchText)) filter.Add("Searchterm", searchText);
            if (warengruppe != null)               filter.Add("Warengruppe", warengruppe.Id);
            if (ihFilter.CurrentIHObjekt != null)  filter.Add("IHObjekt", ihFilter.CurrentIHObjekt.Nummer);
            if (lieferant != null)                 filter.Add("Lieferant", lieferant.Id);
            filter.Add("Deaktivierte", deaktivierte);
            return filter;
        }


        public async Task LoadAsync(List<IHFilterItem> ihlist, List<PzFilterModel> pzlist, List<KstFilterModel> kstlist,
                                    IEnumerable<LieferantFilterModel> lieferanten, IEnumerable warengruppen, CancellationToken ct = default)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Warengruppen = warengruppen;
                LfSearchProvider.SetItems(lieferanten);

                RaisePropertyChanged("Warengruppen");
                RaisePropertyChanged("Lieferanten");

            }), null);

            await ihFilter.LoadAsync(ihlist, pzlist, kstlist, ct);
        }


        public void Unload()
        {

            Warengruppen = null;
            ihFilter.Unload();
            LfSearchProvider.ClearItems();
        }


        public void Reset()
        {

            Warengruppe = null;
            Lieferant = null;
            SearchText = null;
            ihFilter.Reset();
            deaktivierte = "bestand";
            RaiseRadios();
        }


        private void RaiseRadios()
        {

            RaisePropertyChanged(nameof(RadioAlle));
            RaisePropertyChanged(nameof(RadioNurAktive));
            RaisePropertyChanged(nameof(RadioMitBestand));
        }


#region P R O P E R T I E S


        public ILieferantenSearchProvider LfSearchProvider  { get; }


        private String deaktivierte = "bestand";
        public bool RadioNurAktive
        {
            get { return deaktivierte == "aktiv"; }
            set { deaktivierte = "aktiv"; RaiseRadios(); }
        }

        public bool RadioMitBestand
        {
            get { return deaktivierte == "bestand"; }
            set { deaktivierte = "bestand"; RaiseRadios(); }
        }

        public bool RadioAlle
        {
            get { return deaktivierte == null; }
            set { deaktivierte = null; RaiseRadios(); }
        }


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        public ICommand ResetCommand { get; private set; }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        public IEnumerable Warengruppen     { get; private set; }


        private NummerFilterModel warengruppe;
        public NummerFilterModel Warengruppe
        {
            get { return warengruppe; }
            set { SetProperty(ref warengruppe, value); }
        }


        private LieferantFilterModel lieferant;
        public LieferantFilterModel Lieferant
        {
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }


        public object IHSelektor { get { return ihFilter.View; } }


#endregion

    }
}
