﻿using System;
using Comain.Client.Dtos.Reports;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class IHObjektRow
    {
        
        public String   Nummer                  { get; private set; }
        public String   Name                    { get; private set; }
        public String   Inventarnummer          { get; private set; }
        public String   Fabriknummer            { get; private set; }
        public int      Position                { get; private set; }
    
    
        public IHObjektRow(ArtikelIhoMap model)
        {

            Nummer         = model.IHObjekt.Nummer;        
            Name           = model.IHObjekt.Name;          
            Inventarnummer = model.IHObjekt.Inventarnummer;
            Fabriknummer   = model.IHObjekt.Fabriknummer;  
            Position       = model.Position;  
        }
    
    
        public IHObjektRow(ArtikelIHObjekt model)
        {

            Nummer         = model.IHObjekt.Nummer;        
            Name           = model.IHObjekt.Name;          
            Inventarnummer = model.IHObjekt.Inventarnummer;
            Fabriknummer   = model.IHObjekt.Fabriknummer;  
            Position       = model.Position;  
        }
    }
}
