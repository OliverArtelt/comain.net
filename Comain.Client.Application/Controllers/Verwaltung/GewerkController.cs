﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IGewerkController : IVwTypController 
    {}
    
    
    public class GewerkController : SimpleController<KeyNameDto, Gewerk, IGewerkView>, IGewerkController
    {
        
        public GewerkController(IMessageService messages, IControllerBus bus,
                                ISimpleService<KeyNameDto, Gewerk> store, VerwaltungViewModel mainView, SimpleViewModel<Gewerk, IGewerkView> view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/GW"; }
        }
    }
}
