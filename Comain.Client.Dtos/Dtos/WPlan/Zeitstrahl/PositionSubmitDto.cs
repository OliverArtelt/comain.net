﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class PositionSubmitDto 
    {

        public ChangeStatus     ChangeStatus        { get; set; }
        public int              PositionId          { get; set; }
        public int              AuftragId           { get; set; }
        public int              MassnahmeId         { get; set; }
        public int              Ergebnis            { get; set; }
        public DateTime?        FälligAm            { get; set; }
        public DateTime?        DurchgeführtAm      { get; set; }
        public int?             MinutenGearbeitet   { get; set; }
        public int?             MengeGearbeitet     { get; set; }
        public String           Bemerkungen         { get; set; }
        public int?             Auftragnehmer_Id    { get; set; }
    }
}
