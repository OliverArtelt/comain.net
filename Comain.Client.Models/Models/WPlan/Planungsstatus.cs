﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan
{
    public enum Planungsstatus
    {
        NurPositionen, AlleMassnahmen, NurBedarfspositionen
    }


    public static class PlanungsstatusExtended
    {

        public static String AsText(this Planungsstatus value)
        {

            switch (value) {
                case Planungsstatus.NurPositionen: return "Maßnahmen mit Positionen";
                case Planungsstatus.NurBedarfspositionen: return "nur Bedarfspositionen";
                default: return "alle Maßnahmen";
            }
        }
    }
}
