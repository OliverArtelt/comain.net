﻿using Comain.Client.ViewModels;

namespace Comain.Client.Controllers.Stock.Artikelstamm
{
    public interface IArtikelCommandMediator
    {
        EditMode CurrentMode { get; }

        void NeuSetzen();
        void SetEditMode(EditMode newMode);
    }
}