﻿using System;
using System.Linq;
using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class ArtikelBSEditPickItem : NotifyBase
    {
       
        private readonly Artikel at;
        private readonly Bestellung bs;


        public ArtikelBSEditPickItem(Artikel at, Bestellung bs)
        {
          
            this.at = at;
            this.bs = bs;
        }


        public bool IsSelected
        { 
            get { return bs.Positionen.Any(p => p.Material_Id == at.Id); }
            set { 
            
                if (value) {

                    if (bs.Positionen.All(p => p.Material_Id != at.Id)) 
                        bs.Positionen.Add(AsBestellposition());   

                } else {

                    bs.Positionen.Remove(bs.Positionen.Where(p => p.Material_Id == at.Id));
                }
            
                RaisePropertyChanged("IsSelected");
            }
        }


        public bool IstBestellwürdig { get { return at.Status == ArtikelStatus.Fehlt; } }
        public bool HatLieferant     { get { return at.MaterialLieferantMap.Any(p => p.Lieferant.Id == bs.Lieferant.Id); } }


        public Artikel          Model               { get { return at; } } 
        public String           Nummer              { get { return at.Nummer; } } 
        public String           Name                { get { return at.Name; } } 
        public String           Einheit             { get { return at.Einheit?.Nummer; } } 
        public decimal?         Mindestbestand      { get { return at.Mindestbestand; } } 
        public decimal?         Mindestbestellmenge { get { return at.Mindestbestellmenge; } } 
        public decimal?         Lagerbestand        { get { return at.BestandReal; } } 
        public String           Color               { get { return at.StatusAsColor; } }
        public ArtikelStatus    Status              { get { return at.Status; } }


        public Bestellposition AsBestellposition()
        {

            var bp = new Bestellposition();
            var lfbs = at.MaterialLieferantMap.FirstOrDefault(p => p.Lieferant.Id == bs.Lieferant.Id);

            bp.Artikelname = Name;
            bp.Artikelnummer = Nummer;
            bp.LfArtikelnummer = lfbs?.Nummer;
            bp.Einheit = Einheit;
            bp.Material_Id = at.Id;
            bp.Menge = at.Mindestbestellmenge.GetValueOrDefault() == 0m? 1m: at.Mindestbestellmenge.Value;
            bp.Preis = lfbs == null? at.Mischpreis: lfbs.Preis;

            return bp;
        }
    }
}
