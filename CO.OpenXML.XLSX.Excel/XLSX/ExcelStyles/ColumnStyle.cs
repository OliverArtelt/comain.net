﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// (Z.Z.) Spaltenbreiten verwalten
    /// </summary>
    public class ColumnStyle: IComparable<ColumnStyle>
    {
    
        public int Min { get; private set; }
        public int Max { get; private set; }
        public float Width { get; private set; }


        public ColumnStyle(int min, float width) : this(min, min, width) {}

        public ColumnStyle(int min, int max, float width)
        {
        
            Min = min;
            Max = max;
            Width = width;
        }
        
        /// <summary>
        /// Vergleichen, funktioniert nur wenn Spaltenbereiche sich nicht überlappen
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ColumnStyle other)
        {
            
            return this.Min.CompareTo(other.Min);
        }
    }
}
