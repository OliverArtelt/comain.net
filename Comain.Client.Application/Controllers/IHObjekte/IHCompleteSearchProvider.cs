﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.Controllers.IHObjekte
{

    public class IHCompleteSearchProvider : IIHCompleteSearchProvider 
    {

        private IEnumerable<IHListDto> iHObjekte;
        private List<IHListDto> foundList;  
        
        public int Aktuell  { get; private set; }
        public int Gesamt   { get { return foundList == null? 0: foundList.Count; } }
        

        public void SetzeIHObjekte(IEnumerable<IHListDto> list)
        { 
        
            iHObjekte = list;   
            foundList = null;
        }


        public IHListDto Current 
        { 
            get { 
            
                if (foundList == null) return null;
                if (Aktuell >= Gesamt) return null;
                if (Aktuell < 0) return null;
                return foundList[Aktuell];
            }
        }
        

        public IHListDto Next() 
        { 
            
            Aktuell++;
            if (Aktuell >= Gesamt) Aktuell = 0;
            return Current;
        }


        public IHListDto Prev() 
        { 
            
            Aktuell--;
            if (Aktuell == -1) Aktuell = Gesamt - 1;
            return Current;
        }


        public void Search(String searchTerm)
        {

            Aktuell = 0;
            if (iHObjekte == null) return;
            if (String.IsNullOrWhiteSpace(searchTerm)) return;
            var terms = searchTerm.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foundList = iHObjekte.Where(p => terms.All(q => Matches(p, q))).ToList();
        }


        private bool Matches(IHListDto ih, String searchTerm)
        {

            if (ih == null) return false;

            if (ih.Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Inventarnummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Bauteil.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Baugruppe.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Fabriknummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Typ.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Anlage.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.TechnischeDaten.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (ih.Bemerkungen.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) return true;

            return false;
        }
    }
}
