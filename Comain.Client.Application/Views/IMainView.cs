﻿using System;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;

namespace Comain.Client.Views
{

    public interface IMainView : IView
    {

        void Show();
        void OpenFlyout(FlyoutConfig view);
        Task<bool> OpenFlyoutAsync(FlyoutConfig view);
        Task CloseFlyoutAsync();
        Task<bool> FlyoutIsOpenAsync();
        bool DialogIsOpen { get; }

        void ShowMessage(String header, String message);
        
        Task<bool> ConfirmExitAsync(String head, String body);
    }
}       
