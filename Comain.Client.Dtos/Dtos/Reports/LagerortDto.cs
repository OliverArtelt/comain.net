﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Reports
{

    public class LagerortDto : ChangeTrackingBase
    {
       
        public String   Halle           { get; set; }
        public String   Regal           { get; set; }
        public String   Fach            { get; set; }
        public String   Nummer          { get; set; }
        public String   Bemerkungen     { get; set; }
    }
}
