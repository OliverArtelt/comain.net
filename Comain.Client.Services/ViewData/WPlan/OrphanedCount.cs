﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan;

namespace Comain.Client.ViewData.WPlan
{

    public class OrphanedCount
    {

        public int Checklistpositionen  { get; set; }
        public int Aufträge             { get; set; }

        
        public bool HasOrphaned => Checklistpositionen + Aufträge > 0;


        public List<String> Results 
        {
            get {

                if (!HasOrphaned) return null;
                var result = new List<String>();

                if (Aufträge == 1) result.Add("• Ein Auftrag wird gelöscht.");
                if (Aufträge > 1)  result.Add($"• {Aufträge} Aufträge werden gelöscht.");

                if (Checklistpositionen == 1) result.Add("• Eine Checklistposition wird gelöscht.");
                if (Checklistpositionen > 1)  result.Add($"• {Checklistpositionen} Checklistpositionen werden gelöscht.");

                return result;
            }
        }
    }
}
