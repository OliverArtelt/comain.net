﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class ObjektgruppeViewModel : ViewModel<IObjektgruppeView>, IDelayableViewModel
    {

        protected readonly SynchronizationContext context;


        public ObjektgruppeViewModel(IObjektgruppeView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        private bool Predicate(object arg)
        {

            var sub = arg as Objektart;
            if (sub == null) return false;
            if (Mainitem == null) return false;

            return Mainitem.Id == sub.Haupt_Id;
        }


        private void Refresh()
        {

            context.Post(new SendOrPostCallback((o) => {

                if (Sublist != null) {

                    Sublist.SortDescriptions.Clear();
                    Sublist.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                    Sublist?.Refresh();
                }
                RaisePropertyChanged("Sublist");
            }), null);
        }


        public void Unload()
        {

            Mainlist = null;
            RaisePropertyChanged("Mainlist");
            Sublist = null;
            RaisePropertyChanged("Sublist");
        }


        public void SetLists(IEnumerable<Objektgruppe> mainlist, IEnumerable<Objektart> sublist)
        {

            Mainlist = mainlist;
            RaisePropertyChanged("Mainlist");
            Sublist = new CollectionViewSource { Source = sublist }.View;
            Sublist.Filter = Predicate;
            Refresh();
        }


#region P R O P E R T I E S


        public IEnumerable Mainlist { get; private set; }


        private Objektgruppe mainitem;
        public Objektgruppe Mainitem
        {
            get { return mainitem; }
            set { if (SetProperty(ref mainitem, value)) Refresh(); }
        }


        public ICollectionView Sublist { get; private set; }


        private Objektart subitem;
        public Objektart Subitem
        {
            get { return subitem; }
            set { SetProperty(ref subitem, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand addMainCommand;
        public ICommand AddMainCommand
        {
            get { return addMainCommand; }
            set { SetProperty(ref addMainCommand, value); }
        }


        private ICommand deleteMainCommand;
        public ICommand DeleteMainCommand
        {
            get { return deleteMainCommand; }
            set { SetProperty(ref deleteMainCommand, value); }
        }


        private ICommand addSubCommand;
        public ICommand AddSubCommand
        {
            get { return addSubCommand; }
            set { SetProperty(ref addSubCommand, value); }
        }


        private ICommand deleteSubCommand;
        public ICommand DeleteSubCommand
        {
            get { return deleteSubCommand; }
            set { SetProperty(ref deleteSubCommand, value); }
        }


#endregion

    }
}
