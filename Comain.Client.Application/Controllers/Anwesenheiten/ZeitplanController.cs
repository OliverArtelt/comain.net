﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.ReportModels.Anwesenheiten;
using Comain.Client.Services;
using Comain.Client.Services.Anwesenheiten;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Anwesenheiten;

namespace Comain.Client.Controllers.Anwesenheiten
{

    public interface IZeitplanController : IController
    {}


    public class ZeitplanController : ControllerBase, IZeitplanController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IAnwesenheitService store;
        private readonly IFlyoutService flyouts;
        private readonly IMapper mapper;
        private readonly MainViewModel mainView;
        private readonly ZeitplanViewModel zeitView;
        private readonly CopyViewModel copyView;
        private Konfiguration cfg;
        private SerializableCommand copyCommand;


        public ZeitplanController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                  IKonfigService config, IControllerBus bus, IAnwesenheitService store, IKonfigService configs,
                                  IMapper mapper,
                                  MainViewModel mainView, ZeitplanViewModel zeitView, CopyViewModel copyView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.configs = configs;
            this.store = store;
            this.flyouts = flyouts;
            this.mapper = mapper;
            this.mainView = mainView;
            this.zeitView = zeitView;
            this.copyView = copyView;
            this.configs = configs;
        }


        public void Initialize()
        {

            zeitView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            zeitView.SelectAssetCommand = CreateCommand(_ => SelectAssetAsync());
            zeitView.SelectDateTimeCommand = CreateCommand(_ => SelectDateTimeAsync());
            zeitView.SaveCommand = CreateCommand(_ => SaveAsync());
            zeitView.CopyFromCommand = CreateCommand(_ => CopyFromAsync());
            zeitView.CopyToCommand = CreateCommand(_ => OpenCopyViewAsync());
            zeitView.PrintCommand = CreateCommand(_ => PrintAsync());
            zeitView.CancelCommand = CreateCommand(_ => SelectAssetImplAsync());
            copyView.CopyCommand = copyCommand = CreateCommand(_ => CopyToAsync(), _ => String.IsNullOrEmpty(copyView.Error));

            PropertyChangedEventManager.AddHandler(copyView, ViewPropertyChanged, String.Empty);
        }


        public async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            zeitView.ViewerVisible = false;
            crumbs.Add("Erfassung Anwesenheiten");
            mainView.PageView = zeitView.View;
            zeitView.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task BackAsync()
        {

            if (zeitView.ViewerVisible) {

                zeitView.ViewerVisible = false;
                return;
            }

            crumbs.Remove();
            await bus.BackAsync();
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(zeitView)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var tatask = store.TätigkeitenAsync(bus.Token);
                var cfgtask = configs.GetAsync(bus.Token);

                await Task.WhenAll(ihtask, pztask, kstask, tatask, cfgtask).ConfigureAwait(false);
                cfg = cfgtask.Result;
                await zeitView.SetzeListenAsync(ihtask.Result, pztask.Result, kstask.Result, tatask.Result, bus.Token)
                              .ConfigureAwait(false);

                zeitView.UhrzeitVonInput = cfg.StandardSchichtUhrzeitTextVon;
                zeitView.UhrzeitBisInput = cfg.StandardSchichtUhrzeitTextBis;
                zeitView.Pause           = cfg.StandardSchichtPause;
            }
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await ConfirmLeaveAsync()) return false;
            zeitView.Unload();
            copyView.Unload();
            return true;
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(copyView, ViewPropertyChanged, String.Empty);
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (sender == copyView && args.PropertyName == nameof(copyView.Error)) {

                    UpdateCopyCommands();
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private void UpdateCopyCommands()
        {
            OnUIThreadFireAndForget(() => { copyCommand.RaiseCanExecuteChanged(); });
        }


        private async Task<bool> ConfirmLeaveAsync()
        {

            if (zeitView.Anwesenheiten == null) return true;
            if (!zeitView.Anwesenheiten.IsChanged) return true;
            return await messages.ConfirmExitAsync();
        }


        private async Task SelectAssetAsync()
        {

            if (!await ConfirmLeaveAsync()) return;
            await SelectAssetImplAsync();
        }


        private async Task SelectAssetImplAsync()
        {

            zeitView.Anwesenheiten = null;
            IList<PersonalFilterModel> pslist = new List<PersonalFilterModel>();
            if (zeitView.IHObjekt != null) {

                using (new VisualDelay(zeitView)) {

                    pslist = await store.AssetPersonalAsync(zeitView.IHObjekt.Id, bus.Token);
                }
            }

            await OnUIThreadAsync(() => { zeitView.SetzeAllePersonen(pslist); });
            await SelectDateTimeAsync();
        }


        private async Task SelectDateTimeAsync()
        {

            if (!await ConfirmLeaveAsync()) return;
            var args = zeitView.RequestFilter(cfg);
            if (args == null && zeitView.Anwesenheiten != null) zeitView.Anwesenheiten = null;
            if (args == null) return;

            using (new VisualDelay(zeitView)) {

                var qry = await store.AnwesenheitenAsync(args, bus.Token).ConfigureAwait(false);
                zeitView.Anwesenheiten = new TrackableCollection<Anwesenheit>(qry);
            }
        }


        private async Task SaveAsync()
        {

            if (zeitView.Anwesenheiten.IsNullOrEmpty()) return;
            var args = zeitView.ResponseFilter(cfg);
            if (args == null) return;
            args.List = mapper.AsDtoList<Anwesenheit, AnwesenheitDto>(zeitView.Anwesenheiten);
            if (args.List.IsNullOrEmpty()) return;

            using (new VisualDelay(zeitView)) {

                await store.SaveZeitplanAsync(args, bus.Token).ConfigureAwait(false);
                zeitView.Anwesenheiten = null;
                await SelectDateTimeAsync();
            }
        }


        private async Task CopyFromAsync()
        {

            var filter = zeitView.RequestFilter(cfg);
            if (filter == null) return;

            using (new VisualDelay(zeitView)) {

                var list = await store.CopyFromAsync(filter, bus.Token).ConfigureAwait(false);
                if (list.IsNullOrEmpty()) return;
                await OnUIThreadAsync(() => { zeitView.AddList(list); });
            }
        }


        private async Task OpenCopyViewAsync()
        {

            if (zeitView.Anwesenheiten.IsNullOrEmpty()) return;
            if (zeitView.Anwesenheiten.IsChanged) throw new ComainOperationException("Die Anwesenheitsliste wurde bearbeitet. Diese muß zuerst gespeichert werden bevor sie kopiert werden kann.");

            copyView.Asset = zeitView.IHObjekt;
            copyView.QuellDatum = zeitView.Datum.Value;

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = copyView.View, Header = "Anwesenheiten kopieren", Position = MahApps.Metro.Controls.Position.Right });
        }


        private async Task CopyToAsync()
        {

            var filter = copyView.GebeFilter(cfg);
            if (filter == null) return;
            await flyouts.CloseAsync();

            using (new VisualDelay(zeitView)) {

                await store.CopyToAsync(filter, bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                zeitView.UnloadReport();
                var result = new ReportResult { AssemblyAddress = "Comain.Client.Reports", HasLogo = false, ReportAddress = "Comain.Client.Reports.AnwesenheitReport.rdlc",
                                                Title = "Anwesenheiten - Liste" };
                var dtos = await store.ReportAsync(filter, bus.Token).ConfigureAwait(false);
                result.AddDataSource("MainData", dtos.Select(p => new AnwesenheitReportModel(p)).ToList());
                zeitView.ViewerVisible = true;
                await zeitView.BindViewerAsync(result);
            }
        }


        private async Task PrintAsync()
        {

            var filter = zeitView.RequestFilter(cfg);
            if (filter == null) return;

            using (new VisualDelay(zeitView)) {

                zeitView.UnloadReport();
                var result = new ReportResult { AssemblyAddress = "Comain.Client.Reports", HasLogo = false, ReportAddress = "Comain.Client.Reports.AnwesenheitReport.rdlc",
                                                Title = "Anwesenheiten - Liste" };
                var dtos = await store.ReportAsync(filter, bus.Token).ConfigureAwait(false);
                result.AddDataSource("MainData", dtos.Select(p => new AnwesenheitReportModel(p)).ToList());
                result.AddParameter("parTitle", "Planung Anwesenheiten");
                result.AddParameter("parFilter", $"Asset {zeitView.IHObjekt.Nummer} {zeitView.IHObjekt.Name}");
                zeitView.ViewerVisible = true;
                await zeitView.BindViewerAsync(result);
            }
        }
    }
}
