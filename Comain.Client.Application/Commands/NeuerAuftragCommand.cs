﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Commands
{

    public class NeuerAuftragCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IAssistentController);
        public String       Name        => "Neuer Auftrag";
        public String       Parameter   => "AU Add";
        public String       Gruppe      => "Aufträge";
        public int          Sortierung  => 2000;
        public String       Color       => "RoyalBlue";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.page.add.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null) return false;
            if (!module.VerwendetModulAuftrag) return false;
            return nutzer.IstAdministrator || nutzer.IstProduktionLeitung || nutzer.IstProduktionSchichtmeister || nutzer.IstInstandhaltungInstandhalter ||
                   nutzer.IstInstandhaltungLeitung || nutzer.IstInstandhaltungWerker;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
