﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;

namespace Comain.Client.Controllers
{

    public abstract class ControllerBase
    {

        protected enum CopyMode { Add, AddSub, Copy, Inject }


        protected readonly SemaphoreSlim locker = new SemaphoreSlim(1, 1);
        protected readonly SynchronizationContext context = SynchronizationContext.Current;
        protected readonly IMessageService messages;
        protected readonly IControllerBus bus;


        public ControllerBase(IMessageService messages, IControllerBus bus)
        {

            this.messages = messages;
            this.bus = bus;
        }


        public virtual Task RunAsync(object parameter)
            => Task.FromResult(true);


        protected virtual Task<bool> UnloadImplAsync() => Task.FromResult(true);


        public async Task<bool> UnloadAsync()
        {
            try {

                await locker.WaitAsync(bus.Token);
                try {

                    return await UnloadImplAsync();
                }
                finally { locker.Release(); }
            }
            catch (Exception x) {

                messages.Show(x);
                return false;
            }
        }


        protected SerializableCommand CreateCommand(Func<object, Task> execute, Func<object, bool> canExecute = null)
            => new SerializableCommand(messages, locker, bus, execute, canExecute);


        protected Task ExecuteWithLockAsync(Func<object, Task> execute)
            => CreateCommand(execute).ExecuteAsync(null);


        protected async Task ExecuteAsync(Func<object, Task> execute)
        {

            try {

                await Task.Run(async () => { await execute(null); });
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        protected Task OnUIThreadAsync(Action action)
            => context.SendAsync(new SendOrPostCallback((o) => { action.Invoke(); }), null);


        protected void OnUIThread(Action action)
            => context.Send(new SendOrPostCallback((o) => { action.Invoke(); }), null);


        protected void OnUIThreadFireAndForget(Action action)
            => context.Post(new SendOrPostCallback((o) => { action.Invoke(); }), null);
    }
}
