﻿using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Reports
{

    public class InaktiveIHReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public InaktiveIHReport(IReportService store)
        {
            this.store = store;
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.InaktiveIHAsync(GebeFilter(), ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string  Title           => "Inaktive Assets";
        public string           Gruppe          => "Assets";
        public int              Sortierung      => 1200;
        public override string  ReportAddress   => "Comain.Client.Reports.InaktiveIHReport.rdlc";
    }
}
