﻿using System;


namespace Comain.Client.Dtos.Aufträge
{

    ///<summary>
    ///DTO zum Anzeigen wiedergeöffneter Aufträge, zum Öffnen selbst wird ReopenDto verwendet
    ///</summary>
    public class AuReopenDto
    {

        public DateTime     Zeitpunkt           { get; set; }
        public String       Nummer              { get; set; }
        public String       Kurzbeschreibung    { get; set; }
        public String       Nutzer              { get; set; }
        public String       Begründung          { get; set; }
    }
}
