﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class DringlichkeitViewModel : SimpleViewModel<Dringlichkeit, IDringlichkeitView>, IDelayableViewModel
    {

        public DringlichkeitViewModel(IDringlichkeitView view) : base(view)
        {
        }


        private List<LeistungsartFilterModel> leistungsarten;
        public List<LeistungsartFilterModel> Leistungsarten
        { 
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }
    }
}
