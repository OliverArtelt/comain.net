﻿using System;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers;
using Comain.Client.Views;


namespace Comain.Client.Services
{

    public class FlyoutService : IFlyoutService
    {

        private readonly IMainView view;
        private bool confirmed;


        public ICommand YesCommand       { get; private set; }
        public ICommand NoCommand        { get; private set; }


        public FlyoutService(IMainView view)
        {

            this.view = view;
            YesCommand = new Comain.Client.Controllers.AsyncDelegateCommand(_ => CloseAsync(true));
            NoCommand = new Comain.Client.Controllers.AsyncDelegateCommand(_ => CloseAsync(false));
        }


        public async Task OpenAsync(FlyoutConfig config)
        {

            if (await IsOpenAsync()) await CloseAsync();
            view.OpenFlyout(config);
        }


        public async Task<bool> OpenConfirmAsync(FlyoutConfig config)
        {

            if (await IsOpenAsync()) await CloseAsync();
            confirmed = false;
            await view.OpenFlyoutAsync(config);
            return confirmed;
        }


        public async Task CloseAsync(bool confirmed = false)
        {

            this.confirmed = confirmed;
            await view.CloseFlyoutAsync();
        }


        public Task<bool> IsOpenAsync() => view.FlyoutIsOpenAsync();
    }
}
