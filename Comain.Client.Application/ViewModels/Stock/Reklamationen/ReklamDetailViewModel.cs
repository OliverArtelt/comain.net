﻿using System;
using System.Collections;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Reklamationen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Reklamationen
{
    
    public class ReklamDetailViewModel : ValidatableViewModel<IReklamDetailView, Reklamation>, IDelayableViewModel
    {
  
        public enum Tabs { Basis = 0, Druck } 


        public ReklamDetailViewModel(IReklamDetailView view) : base(view)
        {}


        public override void CurrentChanged(Reklamation oldSelected)
        {
            SetzeHeader();
        }


        private void SetzeHeader()
        {
            
            if (Current == null) {

                Header1 = null;
                Header2 = null;
            
            } else {
            
                Header1 = String.Format("Reklamation der Bestellung {0}", Current.Bestellung.Nummer);   
                Header2 = "erstellt am " + Current.Datum.Value.ToShortDateString();
            }
            
            RaisePropertyChanged("Header1");   
            RaisePropertyChanged("Header2"); 
        }


        public void ResetTabsIfPrint()
        {
            if (Tab == Tabs.Druck) CurrentTab = 0;
        }


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public Tabs Tab { get { return (Tabs)currentTab; } }


#region P R O P E R T I E S
                 

        public String Header1 { get; private set; }
        public String Header2 { get; private set; }

        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }


        private object basisView;
        public object BasisView
        { 
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object reportView;
        public object ReportView
        { 
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
 

#endregion

    }
}
