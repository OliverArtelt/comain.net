﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Aufträge
{

    public class MaterialleistungDto : ChangeTrackingBase
    {

        public int?         Status              { get; set; }
        public String       Nummer              { get; set; }
        public String       Name                { get; set; }
        public String       Beschreibung        { get; set; }
        public String       Entnahmeschein      { get; set; }
        public decimal      Menge               { get; set; }
        public decimal      Einzelpreis         { get; set; }
        public decimal?     Gesamtpreis         { get; set; }
        public decimal?     Mwst                { get; set; }
        public DateTime     Datum               { get; set; }
        public decimal?     Handlingszuschlag   { get; set; }
        public bool         Versiegelt          { get; set; }

        public int          Auftrag_Id          { get; set; }
        public int?         Lieferant_Id        { get; set; }
        public int?         Einheit_Id          { get; set; }
        public int?         Materialstamm_Id    { get; set; }

        public String       EinheitName         { get; set; }

        public int?         Lagerung_Id         { get; set; }
        public int?         Nutzer_Id           { get; set; }
    }
}
