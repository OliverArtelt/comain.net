﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.Services;
using Comain.Client.Services.Anwesenheiten;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Anwesenheiten;

namespace Comain.Client.Controllers.Anwesenheiten
{

    public interface ILeistungController : IController
    {}


    public class LeistungController : ControllerBase, ILeistungController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IAnwesenheitService store;
        private readonly MainViewModel mainView;
        private readonly LeistungViewModel leistungView;
        private Konfiguration cfg;


        public LeistungController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                  IKonfigService config, IControllerBus bus, IAnwesenheitService store, IKonfigService configs,
                                  MainViewModel mainView, LeistungViewModel leistungView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.configs = configs;
            this.store = store;
            this.mainView = mainView;
            this.leistungView = leistungView;
            this.configs = configs;
        }


        public void Initialize()
        {

            leistungView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            leistungView.SearchCommand = CreateCommand(_ => SearchAsync());
            leistungView.SaveCommand = CreateCommand(_ => SaveAsync());
            leistungView.AuftragCommand = CreateCommand(obj => OpenAuftragAsync(obj as AssignMassnahmeViewItem));
        }


        public async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Erfassung Leistungen");
            mainView.PageView = leistungView.View;
            leistungView.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task BackAsync()
        {
            crumbs.Remove();
            await bus.BackAsync();
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(leistungView)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var latask = store.LeistungsartenAsync(bus.Token);
                var gwtask = store.GewerkeAsync(bus.Token);
                var cfgtask = configs.GetAsync(bus.Token);

                await Task.WhenAll(ihtask, pztask, kstask, cfgtask, latask, gwtask).ConfigureAwait(false);
                cfg = cfgtask.Result;
                await leistungView.SetzeListenAsync(ihtask.Result, pztask.Result, kstask.Result, latask.Result, gwtask.Result, bus.Token)
                                  .ConfigureAwait(false);

                leistungView.UhrzeitVonInput = cfg.StandardSchichtUhrzeitTextVon;
            }
        }


        private async Task SearchAsync()
        {

            if (!await ConfirmLeaveAsync()) return;

            if (leistungView.IHObjekt != null) {

                using (new VisualDelay(leistungView)) {

                    var pstask = store.AnwesenheitenAsync(leistungView.GebeFilter(), bus.Token);
                    var mntask = store.MassnahmenAsync(leistungView.IHObjekt.Id, bus.Token);
                    await Task.WhenAll(pstask, mntask).ConfigureAwait(false);

                    await OnUIThreadAsync(() => { leistungView.SetzeSelektion(pstask.Result, mntask.Result); });
                }
            }
        }


        private async Task SaveAsync()
        {

            if (!leistungView.IsChanged) return;

            using (new VisualDelay(leistungView)) {

                await store.SaveMassnahmenAsync(leistungView.Anwesenheiten, bus.Token).ConfigureAwait(false);
                await SearchAsync().ConfigureAwait(false);
            }
        }


        private async Task OpenAuftragAsync(AssignMassnahmeViewItem model)
        {

            if (model == null || !model.Auftrag_Id.HasValue) return;
            await bus.JumpAsync(new ControllerMessage { Destination = typeof(IAuftragController), Entity_Id = model.Auftrag_Id.Value, Source = this });
        }


        private async Task<bool> ConfirmLeaveAsync()
        {

            if (!leistungView.IsChanged) return true;
            return await messages.ConfirmExitAsync();
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (!await ConfirmLeaveAsync()) return false;
            leistungView.Unload();
            return true;
        }


        public void Shutdown()
        {}
    }
}
