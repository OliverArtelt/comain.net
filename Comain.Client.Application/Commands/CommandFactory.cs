﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Models;
using Comain.Client.Services;


namespace Comain.Client.Commands
{

    public class CommandFactory : ICommandFactory
    {

        private readonly IEnumerable<IControllerCommand> actions;
        private readonly ILoginService login;


        public CommandFactory(ILoginService login, IEnumerable<IControllerCommand> actions)
        {

            this.login = login;
            this.actions = actions;
        }


        public IControllerCommand this[String cmdname]
        {
            get { return actions.SingleOrDefault(p => p.Parameter == cmdname); }
        }


        public IEnumerable<IControllerCommand> VerfügbareAktionen(ModulInfo module, Konfiguration cfg)
        {
            return actions.Where(p => p.CommandIsAvailable(login.AktuellerNutzer, module, cfg)).ToList();
        }
    }
}
