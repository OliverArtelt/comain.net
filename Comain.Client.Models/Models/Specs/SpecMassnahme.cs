﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.WPlan;

namespace Comain.Client.Models.Specs
{

    public class SpecMassnahme : Entity, IDataErrorInfo
    {

        public  int       Spezifikation_Id        { get; set; }


        private String    nummer;
        private String    name;
        private String    kurzbeschreibung;
        private int       zyklus;
        private Termintyp termintyp;
        private Weekday   tagesmuster;
        private DateTime? start;
        private int?      normzeit;
        private int?      menge;
        private decimal?  wert;
        private String    externeAuftragsnummer;
        private String    qualifikation;
        private String    werkzeug;
        private String    durchführung;
        private String    sicherheit;
        private String    dokumentation;


        public virtual SpecMassnahme Clone()
        {

            return new SpecMassnahme {

                nummer                = nummer,
                name                  = name,
                kurzbeschreibung      = kurzbeschreibung,
                zyklus                = zyklus,
                termintyp             = termintyp,
                tagesmuster           = tagesmuster,
                start                 = start,
                normzeit              = normzeit,
                menge                 = menge,
                wert                  = wert,
                externeAuftragsnummer = externeAuftragsnummer,
                qualifikation         = qualifikation,
                werkzeug              = werkzeug,
                durchführung          = durchführung,
                sicherheit            = sicherheit,
                dokumentation         = dokumentation,
                gewerk                = gewerk,
                leistungsart          = leistungsart
            };
        }


        public static String ZyklusAsString(Termintyp typ, int zyklus, Weekday tage)
            => WiMassnahme.ZyklusAsString(typ, zyklus, tage);


#region P R O P E R T I E S


        public bool Montags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Montag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Montag, value); }
        }


        public bool Dienstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Dienstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Dienstag, value); }
        }


        public bool Mittwochs
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Mittwoch);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Mittwoch, value); }
        }


        public bool Donnerstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Donnerstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Donnerstag, value); }
        }


        public bool Freitags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Freitag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Freitag, value); }
        }


        public bool Samstags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Samstag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Samstag, value); }
        }


        public bool Sonntags
        {
            get => Termintyp == Termintyp.Tagesplanung && Tagesmuster.HasFlag(Weekday.Sonntag);
            set { if (Termintyp == Termintyp.Tagesplanung) Tagesmuster = Tagesmuster.Flip(Weekday.Sonntag, value); }
        }


        public String Nummer
        {
            get => nummer;
            set => SetProperty(ref nummer, value);
        }


        public String Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }


        public String Kurzbeschreibung
        {
            get => kurzbeschreibung;
            set => SetProperty(ref kurzbeschreibung, value);
        }


        public int Zyklus
        {
            get => zyklus;
            set => SetProperty(ref zyklus, value);
        }


        public Termintyp Termintyp
        {
            get => termintyp;
            set => SetProperty(ref termintyp, value);
        }


        public Weekday Tagesmuster
        {
            get => tagesmuster;
            set => SetProperty(ref tagesmuster, value);
        }


        public DateTime? Start
        {
            get => start;
            set => SetProperty(ref start, value);
        }


        public int? Normzeit
        {
            get => normzeit;
            set => SetProperty(ref normzeit, value);
        }


        public int? Menge
        {
            get => menge;
            set => SetProperty(ref menge, value);
        }


        public decimal? Wert
        {
            get => wert;
            set => SetProperty(ref wert, value);
        }


        public String ExterneAuftragsnummer
        {
            get => externeAuftragsnummer;
            set => SetProperty(ref externeAuftragsnummer, value);
        }


        public String Qualifikation
        {
            get => qualifikation;
            set => SetProperty(ref qualifikation, value);
        }


        public String Werkzeug
        {
            get => werkzeug;
            set => SetProperty(ref werkzeug, value);
        }


        public String Durchführung
        {
            get => durchführung;
            set => SetProperty(ref durchführung, value);
        }


        public String Sicherheit
        {
            get => sicherheit;
            set => SetProperty(ref sicherheit, value);
        }


        public String Dokumentation
        {
            get => dokumentation;
            set => SetProperty(ref dokumentation, value);
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get => gewerk;
            set => SetProperty(ref gewerk, value);
        }


        private LeistungsartFilterModel leistungsart;
        public LeistungsartFilterModel Leistungsart
        {
            get => leistungsart;
            set => SetProperty(ref leistungsart, value);
        }

#endregion


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Kurzbeschreibung)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Nummer)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Zyklus)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Start)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Normzeit)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Termintyp)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Montags)];
                return test;
            }
        }


        public IList<String> Errorlist
        {

            get {

                var list = new List<String>();

                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Kurzbeschreibung)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Nummer)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Zyklus)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Start)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Normzeit)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Termintyp)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Montags)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                return list;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 150) return "Die Bezeichnung darf aus maximal 150 Zeichen bestehen.";
                    break;

                case nameof(Nummer):

                    if (String.IsNullOrWhiteSpace(Nummer)) return null;
                    if (Nummer.Length > 50) return "Die Maßnahmenummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case nameof(Kurzbeschreibung):

                    if (String.IsNullOrWhiteSpace(Kurzbeschreibung)) return "Geben Sie die Kurzbeschreibung an.";
                    break;

                case nameof(Zyklus):

                    if ((Termintyp == Termintyp.VeränderlicheKette || Termintyp == Termintyp.UnveränderlicheKette) && Zyklus <= 0)
                        return "Geben Sie einen gültigen Zyklus (mindestens eine Woche) an.";
                    break;

                case nameof(Start):

                    if (!Start.HasValue) return null;
                    if (Start.Value.Year < 2010 || Start.Value.Year > 2100) return "Geben Sie ein plausibles Datum an.";
                    break;

                case nameof(Normzeit):

                    if (!Normzeit.HasValue) return null;
                    if (Normzeit.Value < 1) return "Wenn eine Normzeit angegeben wurde muß diese mindestens eine Minute betragen.";
                    break;

                case nameof(Termintyp):

                    if (Termintyp == Termintyp.Undefiniert) return "Der Planungstyp muß angegeben werden.";
                    if (Termintyp == Termintyp.Tagesplanung && (Tagesmuster & Weekday.Komplett) == 0) return "Bei einer tagesgenauen Planung muß mindestens ein Wochentag angegeben werden.";
                    if ((Termintyp == Termintyp.VeränderlicheKette || Termintyp == Termintyp.UnveränderlicheKette) && Zyklus <= 0) 
                        return "Geben Sie einen gültigen Zyklus (mindestens eine Woche) an.";
                    break;

                case nameof(Montags):

                    if (Termintyp == Termintyp.Tagesplanung && (Tagesmuster & Weekday.Komplett) == 0) return "Bei einer tagesgenauen Planung muß mindestens ein Wochentag angegeben werden.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
