﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using MahApps.Metro.Controls;
using Comain.Client.Controllers.WPlan.Planen.Commands;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Services.WPlan;
using Comain.Client.ViewModels.WPlan.Planen;
using AutoMapper;

namespace Comain.Client.Controllers.WPlan.Planen
{

    public class TafelController : ControllerBase, ITafelController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly ICommandManager commands;
        private readonly IPlanService store;
        private readonly IKonfigService configs;
        private readonly IMapper mapper;
        private readonly MainViewModel mainView;
        private readonly TafelViewModel view;
        private readonly TafelFilterViewModel filterView;
        private readonly TafelDetailViewModel detailView;
        private readonly TafelDetailPersonalViewModel personalView;
        private readonly TafelDetailResultViewModel resultView;
        private Plan plan;
        private Konfiguration cfg;
        private object originalView;
        private SerializableCommand redoCommand;
        private SerializableCommand undoCommand;
        private SerializableCommand deleteCommand;
        private IPlanController planController;


        public TafelController(IMessageService messages, IControllerBus bus, IKonfigService configs, IMapper mapper,
                               IBreadcrumbService crumbs, ILoginService login, IFlyoutService flyouts, ICommandManager commands, IPlanService store,
                               MainViewModel mainView, TafelViewModel view, TafelFilterViewModel filterView, TafelDetailViewModel detailView,
                               TafelDetailPersonalViewModel personalView, TafelDetailResultViewModel resultView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.commands = commands;
            this.store = store;
            this.configs = configs;
            this.mapper = mapper;

            this.mainView = mainView;
            this.view = view;
            this.filterView = filterView;
            this.detailView = detailView;
            this.personalView = personalView;
            this.resultView = resultView;
        }


        public void Initialize()
        {

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            detailView.PersonalView = personalView.View;
            detailView.ResultView = resultView.View;

            view.BackCommand = CreateCommand(_ => BackAsync());
            view.DragCommand = CreateCommand(p => MoveAsync(p));
            view.PopupCommand = CreateCommand(p => OpenPopupAsync(p));
            view.DetailCommand = new DelegateCommand(_ => view.DetailsOpen = true);
            view.RedoCommand = redoCommand = CreateCommand(_ => RedoAsync(), _ => commands.HasRedo);
            view.UndoCommand = undoCommand = CreateCommand(_ => UndoAsync(), _ => commands.HasUndo);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.SelectCommand = new DelegateCommand(p => SelectAsync(p));
            view.DeleteCommand = deleteCommand = CreateCommand(p => DeleteAsync(), _ => plan != null && plan.HatSelektion);
            view.EditCommand = CreateCommand(p => EditAsync(p));
            filterView.SearchCommand = CreateCommand(p => SearchAsync());
            filterView.PrevCommand = CreateCommand(p => PrevFoundAsync());
            filterView.NextCommand = CreateCommand(p => NextFoundAsync());
            personalView.PersonalCommand = CreateCommand(p => PersonalAsync(p?.ToString()));

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(filterView, ViewPropertyChanged, "");

            planController = bus.GetController<IPlanController>();
        }


        public override async Task RunAsync(object parameter)
        {

            filterView.CurrentSearch = null;
            await base.RunAsync(parameter);

            var newplan = parameter as Plan;
            if (newplan == null) return;
            commands.Clear();
            crumbs.Add("Plantafel");
            originalView = mainView.PageView;
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            cfg = await configs.GetAsync(bus.Token).ConfigureAwait(false);

            plan = newplan;
            personalView.SetzeListen(plan.Personalverzeichnis.Values);
            UpdateAnzeigemodusAsync();
            UpdateVisibilityAsync();
        }


        private async Task BackAsync()
        {

            if (commands.HasModifications && !await messages.ConfirmExitAsync()) return;
            crumbs.Remove();
            mainView.PageView = originalView;
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(filterView, ViewPropertyChanged, "");
        }


        public void Unload()
        {

            view.Unload();
            filterView.Unload();
            detailView.Unload();
            personalView.Unload();
            resultView.Unload();
            plan = null;
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (commands.HasModifications && !await messages.ConfirmExitAsync()) return false;
            Unload();
            return true;
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if ((args.PropertyName == nameof(filterView.OffeneAnzeigen) || args.PropertyName == nameof(filterView.ErledigteAnzeigen) ||
                     args.PropertyName == nameof(filterView.NurEigeneAnzeigen) || args.PropertyName == nameof(filterView.LeereZeilenAusblenden) ||
                     args.PropertyName == nameof(filterView.NurMaterialAnzeigen))
                    && sender == filterView) await UpdateVisibilityAsync();

                if (args.PropertyName == nameof(filterView.Anzeigemodus) && sender == filterView) await UpdateAnzeigemodusAsync();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private async Task UpdateVisibilityAsync()
        {

            view.UnloadPlan();

            var visibility = new VisibilityVisitor(offene: filterView.OffeneAnzeigen, erledigte: filterView.ErledigteAnzeigen,
                                                   nurRelevante: filterView.LeereZeilenAusblenden, nurMaterial: filterView.NurMaterialAnzeigen,
                                                   filterView.NurEigeneAnzeigen? login?.AktuellerNutzer?.Personal_Id ?? -1: (int?)null);
            plan.Accept(visibility);
            plan.CalculateHeights();
            await view.LoadAsync(plan);
        }


        private async Task UpdateAnzeigemodusAsync()
        {

            var visitor = new AnzeigemodusVisitor(filterView.Anzeigemodus);
            plan.Accept(visitor);

            await OnUIThreadAsync(() => {

                view.Spaltenbreite = plan.Spaltenbreite;
                view.PlanWidth = plan.TotalWidth;
                view.PlanHeight = plan.TotalHeight;

            });
        }


        private void UpdateSummen()
        {

            var visitor = new UpdateSumsVisitor();
            plan.Accept(visitor);
        }


        private void UpdateCommands()
        {
            context.Post(new SendOrPostCallback((o) => {

                undoCommand.RaiseCanExecuteChanged();
                redoCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private async Task SaveAsync()
        {

            if (plan == null || !commands.HasModifications) return;
            var dtos = commands.GetModifications().Cast<IResultCommand>().SelectMany(p => p.Modifications()).Distinct().ToList();

            using (new VisualDelay(view)) {

                await store.StoreAsync(dtos, bus.Token).ConfigureAwait(false);
                commands.Clear();
                bus.Token.ThrowIfCancellationRequested();


                if (cfg.WPlanReloadAfterSave) {

                    view.Unload();

                    plan = await planController.LoadPlanAsync();
                    personalView.SetzeListen(plan.Personalverzeichnis.Values);

                    UpdateVisibilityAsync();
                    UpdateAnzeigemodusAsync();
                    await view.LoadAsync(plan);
                }
            }
        }


        private async Task RedoAsync()
        {

            await commands.ExecuteRedoAsync();

            UpdateSummen();
            UpdateCommands();
        }


        private async Task UndoAsync()
        {

            await commands.ExecuteUndoAsync();

            UpdateSummen();
            UpdateCommands();
        }


        private async Task SearchAsync()
        {

            if (plan == null) return;
            filterView.CurrentSearch = null;
            if (String.IsNullOrWhiteSpace(filterView.SearchText)) return;
            filterView.CurrentSearch = plan.FindAll(filterView.SearchText);

            if (!filterView.CurrentSearch.IsEmpty)
                await OnUIThreadAsync(() => { view.ScrollToYPosition(filterView.CurrentSearch.Current.YStartposition); });

            filterView.UpdateSearchPosition();
        }


        private async Task NextFoundAsync()
        {

            if (filterView.CurrentSearch == null) await SearchAsync();
            if (filterView.CurrentSearch == null) return;
            var current = filterView.CurrentSearch.Next();
            if (current == null) return;

            await OnUIThreadAsync(() => { view.ScrollToYPosition(current.YStartposition); });

            filterView.UpdateSearchPosition();
        }


        private async Task PrevFoundAsync()
        {

            if (filterView.CurrentSearch == null) await SearchAsync();
            if (filterView.CurrentSearch == null) return;
            var current = filterView.CurrentSearch.Prev();
            if (current == null) return;

            await OnUIThreadAsync(() => { view.ScrollToYPosition(current.YStartposition); });

            filterView.UpdateSearchPosition();
        }


        private async Task SelectAsync(object p)
        {

            if (plan == null) return;

            await commands.AddAndExecuteAsync(CreateSelection(p, addingToSelection: ShiftPressed, selectState: !CtrlPressed));
            view.StatusInfo = plan.StatusInfo;
            personalView.SetPositionen(plan?.SelektiertePositionen);

            UpdateSummen();
            UpdateCommands();
        }


        private async Task MoveAsync(object position)
        {

            if (plan == null || plan.SelektiertePositionen.IsNullOrEmpty()) return;
            if (!(position is Point)) return;

            await commands.AddAndExecuteAsync(CreateMove((Point)position));
            view.StatusInfo = plan.StatusInfo;

            UpdateSummen();
            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            if (plan == null || !plan.DarfRückmelden || plan.SelektiertePositionen.IsNullOrEmpty()) return;

            await commands.AddAndExecuteAsync(new DeleteCommand(plan.SelektiertePositionen.ToList(), mapper));
            view.StatusInfo = plan.StatusInfo;
            view.PopupIsOpen = false;

            UpdateSummen();
            UpdateCommands();
        }


        private async Task EditAsync(object operation)
        {

            if (plan == null || !plan.DarfRückmelden) return;

            var ergebnis = (Checklistergebnis)Enum.Parse(typeof(Checklistergebnis), operation.ToString());
            var currentAction = new EditAction { Ergebnis = ergebnis, Menge = view.EditMenge, HeuteErledigt = filterView.DurchführungHeute };

            if (contextMenuZelle != null && contextMenuAufgabe == null) await commands.AddAndExecuteAsync(new InsertCommand(contextMenuZelle, currentAction, mapper));
            else await commands.AddAndExecuteAsync(new EditCommand(plan.SelektiertePositionen.ToList(), currentAction, mapper));
            view.PopupIsOpen = false;

            UpdateSummen();
            UpdateCommands();
        }


        private async Task PersonalAsync(String operation)
        {

            if (plan == null || !plan.DarfPersonalZuordnen || plan.SelektiertePositionen.IsNullOrEmpty()) return;

            if (operation == "Delete")
                await commands.AddAndExecuteAsync(new PersonalCommand(plan.SelektiertePositionen.ToList(), null, mapper));
            if (operation == "Set" && personalView.Personal != null)
                await commands.AddAndExecuteAsync(new PersonalCommand(plan.SelektiertePositionen.ToList(), personalView.Personal?.Id, mapper));

            UpdateSummen();
            UpdateCommands();
        }


        private SelectCommand CreateSelection(object sender, bool addingToSelection, bool selectState = true)
            => new SelectCommand(plan.SelektiertePositionen.ToList(), plan.GebePositionen(sender).Where(p => p.IstSichtbar), addingToSelection, selectState);


        private MoveCommand CreateMove(Point ziel)
        {

            var traveller = plan.AllePositionen.FirstOrDefault(p => p.IstSelektiert);
            if (traveller == null) return null;
            var zelle = plan.GebeZelle(ziel);
            if (zelle == null) return null;
            if (traveller.MassnahmeId != zelle.Parent.Id) return null;  //nicht in andere Massnahmen verschieben

            return new MoveCommand(traveller, zelle, mapper);
        }


        private Planzelle   contextMenuZelle;
        private Planaufgabe contextMenuAufgabe;


        /// <remarks>
        /// 1. wurde nicht auf ein Objekt geklickt: Modus 'neue Position einfügen'
        ///     1.1: Bedarfspositionen können nur bis heute gemeldet werden
        ///     1.2: Leistungen können ab heute in der Zukunft erstellt werden
        ///     1.3: Sonderleistungen können nur bis heute gemeldet werden
        /// 2. wurde auf eine selektierte Position geklickt: Modus 'Selektion bearbeiten'
        /// 3. wurde auf ein nicht selektiertes Objekt geklickt: alles abwählen, dieses auswählen und Modus 'Selektion bearbeiten'
        /// </remarks>
        private async Task OpenPopupAsync(object position)
        {

            contextMenuZelle = null;
            contextMenuAufgabe = null;
            view.PopupIsOpen = false;
            if (!(position is Point)) return;
            if (!plan.DarfRückmelden) return;

            contextMenuZelle = plan.GebeZelle((Point)position);
            if (contextMenuZelle == null) return;
            contextMenuAufgabe = plan.GebeAufgabe((Point)position);
            if (!contextMenuZelle.IstBearbeitbar) return;

            if (contextMenuAufgabe != null && !contextMenuAufgabe.IstSelektiert)
                await commands.AddAndExecuteAsync(CreateSelection(contextMenuAufgabe, addingToSelection:false));
            else if (contextMenuAufgabe == null)
                await commands.AddAndExecuteAsync(CreateSelection(null, selectState:false, addingToSelection:false));

            var modanzahl = plan.SelektiertePositionen.Count();
            view.ErgebnisOffenErlaubt = contextMenuZelle.ErgebnisOffenErlaubt;
            view.ErgebnisOkNokErlaubt = contextMenuZelle.ErgebnisOkNokErlaubt;
            view.ErgebnisWneErlaubt   = contextMenuAufgabe != null && contextMenuZelle.ErgebnisWneErlaubt;

            view.OpenPopup(contextMenuZelle, contextMenuAufgabe);
            UpdateCommands();
        }


        private bool ShiftPressed => Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
        private bool CtrlPressed  => Keyboard.IsKeyDown(Key.LeftCtrl)  || Keyboard.IsKeyDown(Key.RightCtrl);
        private bool AltPressed   => Keyboard.IsKeyDown(Key.LeftAlt)   || Keyboard.IsKeyDown(Key.RightAlt);
    }
}
