﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Comain.Client.Views.Internal
{

    /// <remarks>
    /// https://stackoverflow.com/questions/2328497/is-it-possible-to-use-binding-in-a-datagridtemplatecolumn-property
    /// </remarks>
    public class DataGridBindableTemplateColumn : DataGridBoundColumn
    {

        internal static readonly DependencyProperty HeaderValueProperty =
            DependencyProperty.Register("HeaderValue", typeof(object), typeof(DataGridBindableTemplateColumn),
                new PropertyMetadata(null, OnHeaderValuePropertyChanged));

        internal static readonly DependencyProperty VisibilityValueProperty =
            DependencyProperty.Register("VisibilityValue", typeof(Visibility), typeof(DataGridBindableTemplateColumn),
                new PropertyMetadata(Visibility.Visible, OnVisibilityPropertyPropertyChanged));


        private static void OnVisibilityPropertyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            var sender = d as DataGridBindableTemplateColumn;
            sender?.OnVisibilityPropertyChanged((Visibility)e.OldValue, (Visibility)e.NewValue);
        }


        private static void OnHeaderValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
            var sender = d as DataGridBindableTemplateColumn;
            sender?.OnHeaderValueChanged((object)e.OldValue, (object)e.NewValue);
        }


        private Binding      headerBinding;
        private Binding      visibilityBinding;
        private DataTemplate cellEditingTemplate;
        private DataTemplate cellTemplate;


        public Binding HeaderBinding
        {
            get => headerBinding; 
            set 
            {
                if (headerBinding != value) {                    
                    headerBinding = value;

                    if (headerBinding != null) {                        
                        headerBinding.ValidatesOnExceptions = false;
                        headerBinding.NotifyOnValidationError = false;
                        BindingOperations.SetBinding(this, HeaderValueProperty, headerBinding);
                    }
                }
            }
        }


        public Binding VisibilityBinding
        {
            get => visibilityBinding; 
            set
            {
                if (visibilityBinding != value) {
                    visibilityBinding = value;

                    if (visibilityBinding != null) {
                        visibilityBinding.ValidatesOnExceptions = false;
                        visibilityBinding.NotifyOnValidationError = false;
                        BindingOperations.SetBinding(this, VisibilityValueProperty, visibilityBinding);
                    }
                }
            }
        }


        public DataTemplate CellEditingTemplate
        {
            get => cellEditingTemplate; 
            set => cellEditingTemplate = value;
        }


        public DataTemplate CellTemplate
        {
            get => cellTemplate; 
            set => cellTemplate = value;
        }


        protected override void CancelCellEdit(FrameworkElement editingElement, object uneditedValue)
            => editingElement = GenerateEditingElement(null, null);


        protected override FrameworkElement GenerateEditingElement(DataGridCell cell, object dataItem)
        {

            if (CellEditingTemplate != null) return (CellEditingTemplate.LoadContent() as FrameworkElement);
            if (CellTemplate != null) return (CellTemplate.LoadContent() as FrameworkElement);
           // if (!DesignerProperties.IsInDesignTool) throw new Exception($"Missing template for type '{typeof(DataGridBindableTemplateColumn)}'");
            return null;
        }


        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {

            if (CellTemplate != null) return (CellTemplate.LoadContent() as FrameworkElement);
            if (CellEditingTemplate != null) return (CellEditingTemplate.LoadContent() as FrameworkElement);
           // if (!DesignerProperties.IsInDesignTool) throw new Exception($"Missing template for type '{typeof(DataGridBindableTemplateColumn)}'");
            return null;
        }


        protected override object PrepareCellForEdit(FrameworkElement editingElement, RoutedEventArgs editingEventArgs)
            => null;


        protected virtual void OnHeaderValueChanged(object oldValue, object newValue)
            => Header = newValue;


        protected virtual void OnVisibilityPropertyChanged(Visibility oldValue, Visibility newValue)
            => Visibility = newValue;
    }
}
