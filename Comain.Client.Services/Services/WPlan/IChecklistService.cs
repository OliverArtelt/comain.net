﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Reports;
using Comain.Client.Models.WPlan;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.WPlan
{

    public interface IChecklistService
    {

        /// <summary>
        /// Positionen eines Auftrages (damit sie auf erledigt gesetzt werden können)
        /// </summary>
        Task<TrackableCollection<Checklistposition>> ChecklisteAsync(int auid, CancellationToken ct = default);
        /// <summary>
        /// Medien die für eine Position benötigt werden
        /// </summary>
        Task<List<Media>>                MedienAsync(int massnahmeId, CancellationToken ct = default);
        Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>   GewerkeAsync(CancellationToken ct = default);
        Task<List<PositionInfo>>         LEPositionAsync(int leid, CancellationToken ct = default);
        Task<List<PositionInfo>>         MTPositionAsync(int mtid, CancellationToken ct = default);
        Task<AuftragDruckDto>            ReportAsync(Suchkriterien criteria, CancellationToken ct = default);

        Task SaveAsync(Wartungsergebnis result, CancellationToken ct = default);
    }
}