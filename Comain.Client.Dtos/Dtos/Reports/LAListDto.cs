﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class LAListDto
    {

        public String   Nummer    { get; set; }
        public String   Name      { get; set; }
        public decimal  Wert      { get; set; }
        public decimal  Prozent   { get; set; }
        
        public String  NameUndProzent  { get { return String.Format("{1}  ({0:P})", Prozent, Name); } }
    }
}
