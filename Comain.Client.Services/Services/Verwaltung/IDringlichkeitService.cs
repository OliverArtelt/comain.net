﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Services.Verwaltung
{
    public interface IDringlichkeitService
    {

        Task<List<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default);
        Task<TrackableCollection<Dringlichkeit>> DringlichkeitenAsync(CancellationToken ct = default);
        
        Task SaveAsync(CancellationToken ct = default);
        bool HasChanges { get; }
        void Reset();
    }
}