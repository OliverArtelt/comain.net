﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{
    
    public class ZeitAUViewModel : ViewModel<IZeitAUView>, IFilterViewModel
    {

        public ZeitAUViewModel(IZeitAUView view) : base(view)
        {
            
            Reset();
        }


        public void Reset()
        {
            
            Bis1 = DateTime.Now;
            Von1 = Bis1.Value.AddMonths(-2);
            Von2 = null;
            Bis2 = null;
            Compare = false;
            TypWoche = true;
        }


        public String Validiere()
        {
           
            if (!Von1.HasValue || Bis1.HasValue) return "Der Zeitraum muss vollständig angegeben werden.";
            if (Compare && (!Von2.HasValue || !Bis2.HasValue)) return "Wenn eine Gegenüberstellung erstellt werden soll muss der zweite Zeitraum vollständig angegeben werden.";
            if (Von1 > Bis1) return "Das Startdatum darf nicht größer als des Enddatum sein.";
            if (Compare && Von2 > Bis2) return "Das Startdatum des zweiten Zeitraumes darf nicht größer als des Enddatum sein.";
            return null;
        }


        public string FilterAlsString()
        {
            
            if (!Compare) return String.Format("{0} - {1}", Von1.Value.ToShortDateString(), Bis1.Value.ToShortDateString());
            return String.Format("{0} - {1}, Vergleich {2} - {3}", Von1.Value.ToShortDateString(), Bis1.Value.ToShortDateString(), Von2.Value.ToShortDateString(), Bis2.Value.ToShortDateString());
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("ZeitVon1", Von1);   
            filter.Add("ZeitBis1", Bis1);   
            
            if (Compare) {

                filter.Add("ZeitVon2", Von2);   
                filter.Add("ZeitBis2", Bis2);   
            }
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);            
        }


        public void Unload()
        {}


#region P R O P E R T I E S
   

        private DateTime? von1;
        public DateTime? Von1
        { 
            get { return von1; }
            set { SetProperty(ref von1, value); }
        }
   

        private DateTime? von2;
        public DateTime? Von2
        { 
            get { return von2; }
            set { SetProperty(ref von2, value); }
        }
   

        private DateTime? bis1;
        public DateTime? Bis1
        { 
            get { return bis1; }
            set { SetProperty(ref bis1, value); }
        }
   

        private DateTime? bis2;
        public DateTime? Bis2
        { 
            get { return bis2; }
            set { SetProperty(ref bis2, value); }
        }
   

        private bool compare;
        public bool Compare
        { 
            get { return compare; }
            set { SetProperty(ref compare, value); }
        }


        private String gruppierung = "Woche";
        
        public bool TypTag
        { 
            get { return gruppierung == "Tag"; }
            set { 
            
                if (value && gruppierung != "Tag") {
                    
                    gruppierung = "Tag";
                    RaisePropertyChanged("TypTag");
                    RaisePropertyChanged("TypWoche");
                    RaisePropertyChanged("TypMonat");
                    RaisePropertyChanged("TypQuartal");
                    RaisePropertyChanged("TypJahr");
                }
            }
        } 
        
        public bool TypWoche
        { 
            get { return gruppierung == "Woche"; }
            set { 
            
                if (value && gruppierung != "Woche") {
                    
                    gruppierung = "Woche";
                    RaisePropertyChanged("TypTag");
                    RaisePropertyChanged("TypWoche");
                    RaisePropertyChanged("TypMonat");
                    RaisePropertyChanged("TypQuartal");
                    RaisePropertyChanged("TypJahr");
                }
            }
        } 

        public bool TypMonat
        { 
            get { return gruppierung == "Monat"; }
            set { 
            
                if (value && gruppierung != "Monat") {
                    
                    gruppierung = "Monat";
                    RaisePropertyChanged("TypTag");
                    RaisePropertyChanged("TypWoche");
                    RaisePropertyChanged("TypMonat");
                    RaisePropertyChanged("TypQuartal");
                    RaisePropertyChanged("TypJahr");
                }
            }
        } 
        
        public bool TypQuartal
        { 
            get { return gruppierung == "Quartal"; }
            set { 
            
                if (value && gruppierung != "Quartal") {
                    
                    gruppierung = "Quartal";
                    RaisePropertyChanged("TypTag");
                    RaisePropertyChanged("TypWoche");
                    RaisePropertyChanged("TypMonat");
                    RaisePropertyChanged("TypQuartal");
                    RaisePropertyChanged("TypJahr");
                }
            }
        } 
        
        public bool TypJahr
        { 
            get { return gruppierung == "Jahr"; }
            set { 
            
                if (value && gruppierung != "Jahr") {
                    
                    gruppierung = "Jahr";
                    RaisePropertyChanged("TypTag");
                    RaisePropertyChanged("TypWoche");
                    RaisePropertyChanged("TypMonat");
                    RaisePropertyChanged("TypQuartal");
                    RaisePropertyChanged("TypJahr");
                }
            }
        } 
        

#endregion

    }
}
