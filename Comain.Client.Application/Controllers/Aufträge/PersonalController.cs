﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using AutoMapper;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Aufträge;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.Controllers.WPlan;

namespace Comain.Client.Controllers.Aufträge
{

    public class PersonalController : ControllerBase, IPersonalController
    {

        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IPersonalService store;
        private readonly IMapper mapper;
        private readonly PersonalViewModel view;
        private readonly AuftragDetailLeistungViewModel listView;
        private Auftrag currentAu;
        private Personalleistung currentLe;
        private EditMode currentAuMode;
        private EditMode currentMode;


        public PersonalController(IFlyoutService flyouts, IMessageService messages, ILoginService login, IPersonalService store,
                                  IControllerBus bus, IMapper mapper, 
                                  PersonalViewModel view, AuftragDetailLeistungViewModel listView)
          : base(messages, bus)
        {

            this.flyouts = flyouts;
            this.login = login;
            this.store = store;
            this.mapper = mapper;
            this.view = view;
            this.listView = listView;
        }


        public void Initialize()
        {

            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = CreateCommand(_ => DeleteAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            if (login.AktuellerNutzer.IstProduktion) throw new ComainOperationException("Sie haben hierfür nicht die notwendige Berechtigung.");
            var current = (LeMtParameter)parameter;
            if (current.EditMode == EditMode.ReadOnly && current.Leistung == null) return;
            currentAu = current.Auftrag;
            currentLe = current.Leistung ?? new Personalleistung { Auftrag_Id = currentAu.Id };
            currentAuMode = current.EditMode;

            view.SetzeModel(currentAu, currentLe);

            currentMode = EditMode.ReadOnly;
            String header = "Personalleistung einsehen";

            if (currentAu.StatusBearbeitbar) {

                if (currentLe != null && !currentLe.IstAngefügt && currentAuMode == EditMode.Edit &&
                    (!currentLe.BeginnDatum.HasValue || !currentAu.Konfiguration.Abrechnungsschluß.HasValue || currentAu.Konfiguration.Abrechnungsschluß < currentLe.BeginnDatum)) {

                    currentMode = EditMode.Edit;
                    header = "Personalleistung bearbeiten";
                }

                if (currentLe == null || currentLe.IstAngefügt && !currentAu.KostenstelleVerlegt && currentAuMode == EditMode.Edit) {

                    currentMode = EditMode.Append;
                    header = "Personalleistung anfügen";
                }
            }

            view.EditEnabled = currentMode != EditMode.ReadOnly;
            view.Header = header;

            SetErrors(null);

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = view.View, Header = view.Header, Position = MahApps.Metro.Controls.Position.Right });

            view.ChecklistPositionen = null;
            var wtController = bus.GetController<IChecklistPluginController>();
            if (wtController != null) {

                view.ChecklistPositionen = await wtController.LEPositionAsync(currentLe.Id).ConfigureAwait(false);
            }
        }


        private Task AddAsync()
        {

            currentLe = new Personalleistung { Auftrag_Id = currentAu.Id };
            view.SetzeModel(currentAu, currentLe);
            view.Header = "Personalleistung anfügen";
            currentMode = EditMode.Append;
            view.EditEnabled = true;
            SetErrors(null);
            view.SetzeStartFocus();

            return Task.FromResult(true);
        }


        public void Shutdown()
        {
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (currentMode != EditMode.ReadOnly && currentLe != null && currentLe.IsChanged && !await messages.ConfirmExitAsync()) return false;
            view.Unload();
            return true;
        }


        public async Task LoadAsync()
        {

            var asTask = store.FilterAufschlagtypenAsync(bus.Token);
            var psTask = store.FilterPersonalAsync(bus.Token);

            await Task.WhenAll(asTask, psTask).ConfigureAwait(false);

            view.SetzeListen(psTask.Result, mapper.Map<List<Aufschlagtyp>>(asTask.Result));
        }


        private async Task SaveAsync()
        {

            if (currentMode == EditMode.ReadOnly) return;

            if (currentLe.ErrorList.Count > 0) {

                SetErrors(currentLe.ErrorList);
                return;

            } else {

                SetErrors(null);
            }

            try {

                using (new VisualDelay(view)) {

                    view.ErstelleKopie();
                    currentLe = await store.SaveAsync(currentLe, bus.Token);
                    view.SetzeModel(currentAu, currentLe);

                    var old = currentAu.Personalleistungen.FirstOrDefault(p => p.Id == currentLe.Id);
                    if (old != null) currentAu.Personalleistungen.Remove(old);
                    currentAu.Personalleistungen.Add(currentLe);
                    listView?.Refresh();

                    if (currentMode == EditMode.Append) await AddAsync();
                }
            }
            catch (Exception x) {

                view.SetErrors(x);
            }
        }


        private async Task DeleteAsync()
        {

            if (currentMode == EditMode.ReadOnly) return;

            using (new VisualDelay(view)) {

                await store.DeleteAsync(currentLe, bus.Token);
                if (!currentLe.IstAngefügt) currentAu.Personalleistungen.Remove(currentLe);
                currentLe = null;
                currentAu = null;
            }

            await flyouts.CloseAsync();
            listView?.Refresh();
        }


        private void SetErrors(IList<String> errors)
        {

            if (errors.IsNullOrEmpty()) {

                view.ShowValidator = false;
                view.Errors = null;
                return;
            }

            view.Errors = errors.Select(p => "• " + p).ToList();
            view.ShowValidator = true;
        }
    }
}
