﻿using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class SubListDto
    {

        public IList<NummerDto>     Mainlist    { get; set; }
        public IList<SubNummerDto>  Sublist     { get; set; }
    }
}
