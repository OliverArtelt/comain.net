﻿using System;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Objektgruppen : IVwTyp
    {

        public string       Title           => "Objektgruppen";
        public int          Sortierung      => 1500;
        public Type         Controller      => typeof(IObjektgruppenController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;    
        }
    }
}
