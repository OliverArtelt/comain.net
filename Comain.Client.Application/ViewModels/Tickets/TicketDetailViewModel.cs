﻿using System;
using System.Threading;
using Comain.Client.Models;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailViewModel : ValidatableViewModel<ITicketDetailView, Ticket>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Personal, Leistung, Abnahme, Druck, Verlauf, Media } 


        private const int titleLength = 200;
        private readonly SynchronizationContext context;

 
        public TicketDetailViewModel(ITicketDetailView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }
 

        public override void CurrentChanged(Ticket oldSelected)
        {
            
            RaisePropertyChanged(nameof(Titel));
            RaisePropertyChanged(nameof(Auftragsbeschreibung));
            RaisePropertyChanged(nameof(PrioBackground));
            RaisePropertyChanged(nameof(PrioForeground));
            RaisePropertyChanged(nameof(PrioAsText));
            RaisePropertyChanged(nameof(HeaderVisible));
            RaisePropertyChanged(nameof(TicketStatus));
        }


        public void ResetTabs()
        {
            CurrentTab = (int)Tabs.Basis;
        }


        public Tabs Tab 
        { 
            get { return (Tabs)currentTab; } 
            set { currentTab = (int)value; } 
        }


#region P R O P E R T I E S


        public String Auftragsbeschreibung  
        { 
            get {                                       
            
                if (Current == null) return null;
                return Current.Kurzbeschreibung.RemoveNewlines().CutWordAfter(titleLength); 
            } 
        }


        public bool HeaderVisible => Current != null;


        public String Titel                 { get { return Current == null? null: Current.TypAsString + " " + Current.Nummer; } }
        public String PrioBackground        { get { return Current == null || Current.Dringlichkeit == null ? "White": Current.Dringlichkeit.BackgroundColor; } }
        public String PrioForeground        { get { return Current == null || Current.Dringlichkeit == null ? "Black": Current.Dringlichkeit.ForegroundColor; } }
        public String PrioAsText            { get { return Current?.Dringlichkeit?.Nummer; } }
        

        public String TicketStatus
        { 
            get { 
            
                if (Current == null) return null;
                if (Current.Status == Ticketstatus.Undefiniert) return Current.GemeldetAm.ToString(@"\G\e\m\e\l\d\e\t\: dddd, dd.MM.yyyy HH:mm \U\h\r");
                return "Aktueller Status: " + Current.StatusAsString + ", " + Current.GemeldetAm.ToString(@"\G\e\m\e\l\d\e\t\: dddd, dd.MM.yyyy HH:mm \U\h\r");
            } 
        }
        

        private object basisView;
        public object BasisView
        { 
            get { return basisView; }
            set { SetProperty(ref basisView, value); }
        }


        private object personalView;
        public object PersonalView
        { 
            get { return personalView; }
            set { SetProperty(ref personalView, value); }
        }


        private object leistungView;
        public object LeistungView
        { 
            get { return leistungView; }
            set { SetProperty(ref leistungView, value); }
        }


        private object abnahmeView;
        public object AbnahmeView
        { 
            get { return abnahmeView; }
            set { SetProperty(ref abnahmeView, value); }
        }


        private object historieView;
        public object HistorieView
        { 
            get { return historieView; }
            set { SetProperty(ref historieView, value); }
        }
        

        private object mediaView;
        public object MediaView
        { 
            get { return mediaView; }
            set { SetProperty(ref mediaView, value); }
        }
        

        private object printView;
        public object PrintView
        { 
            get { return printView; }
            set { SetProperty(ref printView, value); }
        }
        

        private bool basisViewVisible;
        public bool BasisViewVisible
        { 
            get { return basisViewVisible; }
            set { SetProperty(ref basisViewVisible, value); }
        }


        private bool personalViewVisible;
        public bool PersonalViewVisible
        { 
            get { return personalViewVisible; }
            set { SetProperty(ref personalViewVisible, value); }
        }


        private bool leistungViewVisible;
        public bool LeistungViewVisible
        { 
            get { return leistungViewVisible; }
            set { SetProperty(ref leistungViewVisible, value); }
        }


        private bool abnahmeViewVisible;
        public bool AbnahmeViewVisible
        { 
            get { return abnahmeViewVisible; }
            set { SetProperty(ref abnahmeViewVisible, value); }
        }


        private bool historieViewVisible;
        public bool HistorieViewVisible
        { 
            get { return historieViewVisible; }
            set { SetProperty(ref historieViewVisible, value); }
        }


        private bool mediaViewVisible;
        public bool MediaViewVisible
        { 
            get { return mediaViewVisible; }
            set { SetProperty(ref mediaViewVisible, value); }
        }


        private bool printViewVisible;
        public bool PrintViewVisible
        { 
            get { return printViewVisible; }
            set { SetProperty(ref printViewVisible, value); }
        }

                      
        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }
 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
