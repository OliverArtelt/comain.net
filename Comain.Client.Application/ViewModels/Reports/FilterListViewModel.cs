﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports;


namespace Comain.Client.ViewModels.Reports
{
   
    public class FilterListViewModel : ViewModel<IFilterListView>
    {

        public FilterListViewModel(IFilterListView view) : base(view)
        {
        }


#region P R O P E R T I E S
   

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
   

        private ICommand resetCommand;
        public ICommand ResetCommand
        { 
            get { return resetCommand; }
            set { SetProperty(ref resetCommand, value); }
        }


        private IList<UserControlContainer> filters;
        public IList<UserControlContainer> Filters
        { 
            get { return filters; }
            set { SetProperty(ref filters, value); }
        }


#endregion
    
    }
}
