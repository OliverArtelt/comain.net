﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AssistentResultViewModel : PrintViewModel<IAssistentResultView>
    {
 
        public AssistentResultViewModel(IMessageService messages, IAssistentResultView view) : base(messages, view)
        {
        }


#region P R O P E R T I E S
          

        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }
 

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


#endregion

    }
}
