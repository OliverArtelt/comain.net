﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Controllers.Aufträge
{

    public interface IPersonalController : IController
    {
        Task LoadAsync();
    }
}
