﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Trackers;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{

    public class KostenstellenService : IKostenstellenService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;
        private const String uri = "/VW/KstPz";       
        private List<Standort> currentPzList;       
        
        public TrackableCollection<Kostenstelle> CurrentKstList { get; private set; }


        public KostenstellenService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            var current = await store.GetAsync<KstPzDto>(uri, ct).ConfigureAwait(false);
            var kstlist = mapper.Map<List<Kostenstelle>>(current.Kostenstellen);
            kstlist.Sort();
            currentPzList = mapper.Map<List<Standort>>(current.Standorte);
            currentPzList.Sort();
            var pzdic = currentPzList.ToDictionary(p => p.Id);
            var mapdic = current.Map.ToMultiDictionary(p => p.Kostenstelle_Id, p => pzdic[p.Standort_Id]);
            
            kstlist.ForEach(kst => {
            
                if (mapdic.ContainsKey(kst.Id)) kst.ZugeordneteStandorte = new TrackableCollection<Standort>(mapdic[kst.Id]); 
            });
           
            CurrentKstList = new TrackableCollection<Kostenstelle>(kstlist);
        }


        public IEnumerable<PzSelektorItem> Standorte(Kostenstelle kst)
        {

            var kstpzlist = currentPzList.Select(pz => new PzSelektorItem(pz, kst)).ToList();
            return kstpzlist;     
        }


        public bool HasChanges
        {
            get { return CurrentKstList != null && (CurrentKstList.IsChanged || CurrentKstList.Any(p => p.ZugeordneteStandorte.IsChanged)); }
        }


        public async Task SaveAsync(CancellationToken ct = default)
        {
            
            var result = new KstPzDto();
            result.Kostenstellen = mapper.AsDtoList<Kostenstelle, KstDto>(CurrentKstList);
            result.Map = new List<KstPzMap>();

            foreach (var kst in CurrentKstList) {
            
                if (kst.ZugeordneteStandorte == null || !kst.ZugeordneteStandorte.IsChanged) continue;
                result.Map.AddRange(kst.ZugeordneteStandorte.AddedItems().Select(pz => new KstPzMap { ChangeStatus = ChangeStatus.Added, Standort_Id = pz.Id, Kostenstelle_Id = kst.Id })); 
                result.Map.AddRange(kst.ZugeordneteStandorte.DeletedItems().Select(pz => new KstPzMap { ChangeStatus = ChangeStatus.Deleted, Standort_Id = pz.Id, Kostenstelle_Id = kst.Id })); 
            }
            
            await store.PostAsync<KstPzDto>(uri, result, ct);   
        }


        public void Reset()
        {
           
            currentPzList = null;
            CurrentKstList = null;
        }
    }
}
