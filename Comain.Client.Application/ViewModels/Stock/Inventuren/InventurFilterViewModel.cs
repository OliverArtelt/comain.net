﻿using System;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Inventuren;

namespace Comain.Client.ViewModels.Stock.Inventuren
{

    public class InventurFilterViewModel : ViewModel<IInventurFilterView>
    {
 
        private readonly SynchronizationContext context;


        public InventurFilterViewModel(IInventurFilterView view) : base(view)
        {
            
            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            if (!String.IsNullOrEmpty(SearchText)) filter.Add("Lagerort", SearchText);
            return filter;
        }


        public void Unload()
        {
        }


        public void Reset()
        {
            SearchText = null;
        }


#region P R O P E R T I E S
 

        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private String searchText;
        public String SearchText
        { 
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


#endregion

    }
}
