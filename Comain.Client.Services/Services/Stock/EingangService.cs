﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models.Stock;

namespace Comain.Client.Services.Stock.Wareneingänge
{

    public class EingangService : IEingangService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public EingangService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public Task<IList<String>> AuftragsnummernAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<String>>("/WE/AUNummer", ct);
        }


        public Task<IList<String>> BestellnummernAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<String>>("/WE/BSNummer", ct);
        }
 

        public Task<IList<String>> LieferscheinnummernAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<String>>("/WE/LSNummer", ct);
        }


        public Task<Lagerort> LetzerLagerortAsync(int artikelId, CancellationToken ct = default)
        {
            return store.GetAsync<Lagerort>(String.Format("wl/lastlo/{0}", artikelId), ct);
        }


        public async Task<IList<Lagerort>> LagerorteAsync(CancellationToken ct = default)
        {
            
            var dtos = await store.GetAsync<IList<LagerortDto>>("/VW/LO", ct).ConfigureAwait(false);
            var models = mapper.Map<List<Lagerort>>(dtos);
            return models;
        }


        public Task<IList<EingangBestellposDto>> OffenePositionenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<EingangBestellposDto>>("/WE/Open", ct);
        }


        public Task<IList<EingangListDto>> FindAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<EingangListDto>>("/WE/Search", filter, ct);
        }


        public Task<IList<WEReportDto>> ReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<WEReportDto>>("/WE/Report", filter, ct);
        }


        public async Task<Lieferschein> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format(@"/WE/{0}", id);
            var dto = await store.GetAsync<LieferscheinDto>(url, ct).ConfigureAwait(false);

            var model = mapper.Map<Lieferschein>(dto);
            model.AcceptChanges();

            if (!model.Eingänge.IsNullOrEmpty()) {
            
                model.Eingänge.ForEach(p => p.Lieferschein = model);
                model.Eingänge.AcceptChanges();
            }
            
            return model;
        }


        public async Task<Lieferschein> UpdateAsync(Lieferschein model, CancellationToken ct = default)
        {

            var dto = mapper.Map<Lieferschein, LieferscheinDto>(model);
            var result = await store.PostAsync<LieferscheinDto, LieferscheinDto>("/WE", dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<Lieferschein>(result);
            if (!newmodel.Eingänge.IsNullOrEmpty()) newmodel.Eingänge.ForEach(p => p.Lieferschein = newmodel);
            
            newmodel.AcceptChanges();
            if (!newmodel.Eingänge.IsNullOrEmpty()) newmodel.Eingänge.AcceptChanges();
            return newmodel;  
        }


        public Task DeleteAsync(int id, CancellationToken ct = default)
        {
            return store.DeleteAsync(String.Format(@"/WE/{0}", id), ct);
        }
    }
}
