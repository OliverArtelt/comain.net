﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;
using Comain.Client.Gateways;
using Comain.Client.ViewData.Reports;


namespace Comain.Client.Services.Reports
{

    public class LagerService : ILagerService
    {

        private readonly IRepository store;


        public LagerService(IRepository store)
        {
            this.store = store;
        }


        public Task<List<IHObjektFilterModel>> IHObjekteAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<IHObjektFilterModel>>("/Filter/IH", ct);
        }


        public Task<List<LieferantFilterModel>> LieferantenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<LieferantFilterModel>>("/Filter/LF", ct);
        }


        public Task<List<NummerFilterModel>> WarengruppenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<NummerFilterModel>>("/Filter/WG", ct);
        }


        public Task<List<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<PzFilterModel>>("/Filter/PZ", ct);
        }


        public Task<List<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<List<KstFilterModel>>("/Filter/KST", ct);
        }


        public Task<List<ArtikelListItem>> ArtikelAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<List<ArtikelListItem>>("/Artikel/Search", filter, ct);
        }


        public async Task<ArtikelDetailItem> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/Artikel/Detail/{0}", id);
            var dto = await store.GetAsync<ArtikelDto>(url, ct).ConfigureAwait(false);
            if (dto == null) return null;

            var model = new ArtikelDetailItem(dto);
            return model; 
        }
    }
}
