﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Services;

namespace Comain.Client.Controllers
{
    public class SerializableCommand : ICommand
    {

        private readonly Func<object, Task> execute;
        private readonly Func<object, bool> canExecute;
        private readonly IMessageService messages;
        private readonly IControllerBus bus;
        private readonly SemaphoreSlim locker;


        public SerializableCommand(IMessageService messages, SemaphoreSlim locker, IControllerBus bus,
                                   Func<object, Task> execute, Func<object, bool> canExecute = null)
        {

            this.messages = messages;
            this.locker = locker;
            this.bus = bus;
            this.execute = execute;
            this.canExecute = canExecute;
        }


        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) => canExecute != null ? canExecute(parameter) : true;
        public void RaiseCanExecuteChanged() => OnCanExecuteChanged(EventArgs.Empty);
        protected virtual void OnCanExecuteChanged(EventArgs e) => CanExecuteChanged?.Invoke(this, e);


        public async void Execute(object parameter)
            => await ExecuteAsync(parameter);


        public async Task ExecuteAsync(object p)
        {

            if (!CanExecute(p)) return;

            try {

                await locker.WaitAsync(bus.Token);
                try {

                    await Task.Run(async () => { await execute(p); });
                }
                finally { locker.Release(); }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }
    }
}
