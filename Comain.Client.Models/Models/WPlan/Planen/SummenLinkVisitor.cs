﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Zellen in den Summen verlinken, nach Panaufbau laufen lassen
    /// </summary>
    public class SummenLinkVisitor : IVisitor
    {

        public void Visit(Plan model)
        {
            model.Spaltensummen.ForEach((sum, ndx) => sum.Zellen.AddRange(model.AlleStandorte.Select(p => p.Summen[ndx])));
        }


        public void Visit(Planspaltensumme model)
        {}


        public void Visit(PZPlanzeile model)
        {
            model.Summen.ForEach((sum, ndx) => sum.Zellen.AddRange(model.Children.Select(p => p.Summen[ndx])));
        }


        public void Visit(PZSpaltensumme model)
        {}


        public void Visit(IHPlanzeile model)
        {
            model.Summen.ForEach((sum, ndx) => sum.Zellen.AddRange(model.Children.Select(p => p.Perioden[ndx])));
        }


        public void Visit(IHSpaltensumme model)
        {}


        public void Visit(MNPlanzeile model)
        {}


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
