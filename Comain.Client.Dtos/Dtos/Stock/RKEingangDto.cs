﻿using System;


namespace Comain.Client.Dtos.Stock
{

    /// <summary>
    /// Wareneingänge zum Erstellen von Reklamationen
    /// </summary>
    public class RKEingangDto
    {

        public int          Lieferschein_Id         { get; set; }
        public int          Lieferant_Id            { get; set; }
        public int          Eingang_Id              { get; set; }
        public int          Material_Id             { get; set; }
        public int          Bestellung_Id           { get; set; }

        /// <summary>
        /// Bestellnummer - Bestellposition
        /// </summary>
        public String       Bestellnummer           { get; set; }
        public String       Lieferschein            { get; set; }
        public String       Lieferant               { get; set; }
        public String       Artikelnummer           { get; set; }
        public String       LfArtikelnummer         { get; set; }
        public String       Artikelbezeichnung      { get; set; }
        public decimal?     Preis                   { get; set; }
        public decimal      Liefermenge             { get; set; }
        public decimal?     MengeOK                 { get; set; }
        public DateTime     GeliefertAm             { get; set; }
    }
}