﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Views.Administration;


namespace Comain.Client.ViewModels.Administration
{

    public class IHObjektViewModel : ValidatableViewModel<IIHObjektView, Konfiguration>, IConfigViewModel
    {

        public IHObjektViewModel(IIHObjektView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(IHObjektTrotzAuftragLöschen));
            RaisePropertyChanged(nameof(IHObjektTrotzLeistungenLöschen));
            RaisePropertyChanged(nameof(IHObjektTrotzProjekteLöschen));
            RaisePropertyChanged(nameof(IHObjektObjektgruppeÄnderbar));
            RaisePropertyChanged(nameof(MaximaleBauteilTiefe));
            RaisePropertyChanged(nameof(IHNummerAnzeigetyp0));
            RaisePropertyChanged(nameof(IHNummerAnzeigetyp1));
            RaisePropertyChanged(nameof(IHNummerAnzeigetyp2));
            RaisePropertyChanged(nameof(FabriknummernPflicht));
            RaisePropertyChanged(nameof(InventarnummernPflicht));
            RaisePropertyChanged(nameof(IHFarbe0));
            RaisePropertyChanged(nameof(IHFarbe1));
            RaisePropertyChanged(nameof(IHFarbe2));
            RaisePropertyChanged(nameof(AuDelete0));
            RaisePropertyChanged(nameof(AuDelete1));
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public int? MaximaleBauteilTiefe
        {
            get { return Current == null? null: Current.MaximaleBauteilTiefe; }
            set {

                if (Current != null && Current.MaximaleBauteilTiefe != value) {

                    Current.MaximaleBauteilTiefe = value;
                    RaisePropertyChanged("MaximaleBauteilTiefe");
                }
            }
        }


        public bool FabriknummernPflicht
        {
            get { return Current == null? false: Current.FabriknummernPflicht; }
            set {

                if (Current != null && Current.FabriknummernPflicht != value) {

                    Current.FabriknummernPflicht = value;
                    RaisePropertyChanged(nameof(FabriknummernPflicht));
                }
            }
        }


        public bool InventarnummernPflicht
        {
            get { return Current == null? false: Current.InventarnummernPflicht; }
            set {

                if (Current != null && Current.InventarnummernPflicht != value) {

                    Current.InventarnummernPflicht = value;
                    RaisePropertyChanged(nameof(InventarnummernPflicht));
                }
            }
        }


        public bool IHObjektTrotzAuftragLöschen
        {
            get { return Current == null? false: Current.IHObjektTrotzAuftragLöschen; }
            set {

                if (Current != null && Current.IHObjektTrotzAuftragLöschen != value) {

                    Current.IHObjektTrotzAuftragLöschen = value;
                    RaisePropertyChanged("IHObjektTrotzAuftragLöschen");
                }
            }
        }


        public bool IHObjektTrotzLeistungenLöschen
        {
            get { return Current == null? false: Current.IHObjektTrotzLeistungenLöschen; }
            set {

                if (Current != null && Current.IHObjektTrotzLeistungenLöschen != value) {

                    Current.IHObjektTrotzLeistungenLöschen = value;
                    RaisePropertyChanged("IHObjektTrotzLeistungenLöschen");
                }
            }
        }


        public bool IHObjektTrotzProjekteLöschen
        {
            get { return Current == null? false: Current.IHObjektTrotzProjekteLöschen; }
            set {

                if (Current != null && Current.IHObjektTrotzProjekteLöschen != value) {

                    Current.IHObjektTrotzProjekteLöschen = value;
                    RaisePropertyChanged("IHObjektTrotzProjekteLöschen");
                }
            }
        }


        public bool IHObjektObjektgruppeÄnderbar
        {
            get { return Current == null? false: Current.IHObjektObjektgruppeÄnderbar; }
            set {

                if (Current != null && Current.IHObjektObjektgruppeÄnderbar != value) {

                    Current.IHObjektObjektgruppeÄnderbar = value;
                    RaisePropertyChanged("IHObjektObjektgruppeÄnderbar");
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public bool IHNummerAnzeigetyp0
        {
            get { return Current == null? false: Current.IHNummerAnzeigetyp.GetValueOrDefault() == Models.IHObjekte.IHNummerAnzeigetyp.Nummer; }
            set {

                if (Current != null && value) {

                    Current.IHNummerAnzeigetyp = Models.IHObjekte.IHNummerAnzeigetyp.Nummer;
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp0));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp1));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp2));
                }
            }
        }


        public bool IHNummerAnzeigetyp1
        {
            get { return Current == null? false: Current.IHNummerAnzeigetyp.GetValueOrDefault() == Models.IHObjekte.IHNummerAnzeigetyp.Inventarnummer; }
            set {

                if (Current != null && value) {

                    Current.IHNummerAnzeigetyp = Models.IHObjekte.IHNummerAnzeigetyp.Inventarnummer;
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp0));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp1));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp2));
                }
            }
        }


        public bool IHNummerAnzeigetyp2
        {
            get { return Current == null? false: Current.IHNummerAnzeigetyp.GetValueOrDefault() == Models.IHObjekte.IHNummerAnzeigetyp.Keine; }
            set {

                if (Current != null && value) {

                    Current.IHNummerAnzeigetyp = Models.IHObjekte.IHNummerAnzeigetyp.Keine;
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp0));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp1));
                    RaisePropertyChanged(nameof(IHNummerAnzeigetyp2));
                }
            }
        }


        public bool IHFarbe0
        {
            get { return Current == null? false: !Current.IHObjektFarbtyp.HasValue; }
            set {

                if (Current != null && value && Current.IHObjektFarbtyp.HasValue) {

                    Current.IHObjektFarbtyp = null;
                    RaisePropertyChanged("IHFarbe0");
                    RaisePropertyChanged("IHFarbe1");
                    RaisePropertyChanged("IHFarbe2");
                }
            }
        }


        public bool IHFarbe1
        {
            get { return Current == null? false: Current.IHObjektFarbtyp.HasValue && Current.IHObjektFarbtyp.Value == IHObjektFarbtyp.IHObjektStatus; }
            set {

                if (Current != null && value && (!Current.IHObjektFarbtyp.HasValue || Current.IHObjektFarbtyp.Value != IHObjektFarbtyp.IHObjektStatus)) {

                    Current.IHObjektFarbtyp = IHObjektFarbtyp.IHObjektStatus;
                    RaisePropertyChanged("IHFarbe0");
                    RaisePropertyChanged("IHFarbe1");
                    RaisePropertyChanged("IHFarbe2");
                }
            }
        }


        public bool IHFarbe2
        {
            get { return Current == null? false: Current.IHObjektFarbtyp.HasValue && Current.IHObjektFarbtyp.Value == IHObjektFarbtyp.Erhaltklasse; }
            set {

                if (Current != null && value && (!Current.IHObjektFarbtyp.HasValue || Current.IHObjektFarbtyp.Value != IHObjektFarbtyp.Erhaltklasse)) {

                    Current.IHObjektFarbtyp = IHObjektFarbtyp.Erhaltklasse;
                    RaisePropertyChanged("IHFarbe0");
                    RaisePropertyChanged("IHFarbe1");
                    RaisePropertyChanged("IHFarbe2");
                }
            }
        }


        public bool AuDelete0
        {
            get { return Current == null? false: !Current.IHObjektAuftragMitLöschen; }
            set {

                if (Current != null && value && Current.IHObjektAuftragMitLöschen) {

                    Current.IHObjektAuftragMitLöschen = false;
                    RaisePropertyChanged("AuDelete0");
                    RaisePropertyChanged("AuDelete1");
                }
            }
        }


        public bool AuDelete1
        {
            get { return Current == null? false: Current.IHObjektAuftragMitLöschen; }
            set {

                if (Current != null && value && !Current.IHObjektAuftragMitLöschen) {

                    Current.IHObjektAuftragMitLöschen = true;
                    RaisePropertyChanged("AuDelete0");
                    RaisePropertyChanged("AuDelete1");
                }
            }
        }



#endregion

    }
}
