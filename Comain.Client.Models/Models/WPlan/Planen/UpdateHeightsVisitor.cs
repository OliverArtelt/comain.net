﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.WPlan.Planen
{

    public class UpdateHeightsVisitor : IVisitor
    {

        public double CurrentHeight    { get; private set; }


        public void Visit(Plan model)
        {
            CurrentHeight = 0;
        }


        public void Visit(PZPlanzeile model)
        {

            model.YStartposition = CurrentHeight;
            CurrentHeight += model.Height;

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.YStartposition));
        }


        public void Visit(IHPlanzeile model)
        {

            model.YStartposition = CurrentHeight;
            CurrentHeight += model.Height;

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.YStartposition));
        }


        public void Visit(MNPlanzeile model)
        {

            model.YStartposition = CurrentHeight;
            CurrentHeight += model.Height;

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.YStartposition));
        }


        public void Visit(Planzelle model)
        {

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.Top));
        }


        public void Visit(Planaufgabe model)
        {
        }


        public void Visit(Planspaltensumme model)
        {
        }


        public void Visit(PZSpaltensumme model)
        {

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.Top));
        }


        public void Visit(IHSpaltensumme model)
        {

            model.RaisePropertyChanged(nameof(model.Height));
            model.RaisePropertyChanged(nameof(model.Top));
        }


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
