﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration.Types
{

    public class AuftragConfig : IAdminTyp
    {

        public string       Title           => "Aufträge verwalten";
        public int          Sortierung      => 11000;
        public Type         Controller      => typeof(IAuftragConfigController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
