﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Bevor
{

    public class BevorService : IBevorService
    {

        private readonly IRepository store;


        public BevorService(IRepository store)
        {
            this.store = store;
        }


        public async Task<IList<AuIHKSDto>> IHObjekteAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<AuIHKSDto>>("/Filter/BVIHKS", ct);
            return qry.OrderBy(p => p.IHObjekt).ToList();
        }


        public async Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);
            return qry.OrderBy(p => p.Nummer).ToList();
        }


        public async Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);
            return qry.OrderBy(p => p.Name).ToList();
        }


        public async Task<IDringlichkeitenProvider> DringlichkeitAsync(CancellationToken ct = default)
        {

            var list = await store.GetAsync<IList<Dringlichkeit>>("/VW/DR", ct);
            return new DringlichkeitenProvider(list);
        }


        public async Task<String> CreateAsync(BevorTicket model, CancellationToken ct = default)
        {

            var url = "/Bevor/Create";
            var dto = model.AsDto();
            var result = await store.PostAsync<BevorTicketDto, TicketDetailDto>(url, dto, ct).ConfigureAwait(false);
            return result.Sendefehler;
        }


        public async Task<String> IHPathAsync(String ihnummer, CancellationToken ct = default)
        {

            var url = String.Format("/IH/Path/{0}", ihnummer);
            var dtolist = await store.GetAsync<List<IHInfoDto>>(url, ct).ConfigureAwait(false);
            if (dtolist.IsNullOrEmpty()) return null;
            return String.Join(" • ", dtolist.Select(p => p.Name).ToArray());
        }


        public Task<List<Media>> ReadFilesAsync(IEnumerable<String> paths, CancellationToken ct = default)
        {

            return Task.Run(() => {

                if (paths.IsNullOrEmpty()) return null;
                var models = paths.AsParallel().Select(p => new Media(p)).ToList();
                return models;
            });
        }
    }
}
