﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewData.Reports;


namespace Comain.Client.Services.Reports
{

    public interface ILagerService
    {

        Task<List<IHObjektFilterModel>>     IHObjekteAsync(CancellationToken ct = default);
        Task<List<LieferantFilterModel>>    LieferantenAsync(CancellationToken ct = default);
        Task<List<NummerFilterModel>>       WarengruppenAsync(CancellationToken ct = default);
        Task<List<PzFilterModel>>           StandorteAsync(CancellationToken ct = default);
        Task<List<KstFilterModel>>          KostenstellenAsync(CancellationToken ct = default);
    
        Task<List<ArtikelListItem>>         ArtikelAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<ArtikelDetailItem>             DetailAsync(int id, CancellationToken ct = default);
    }
}
