﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailPersonalViewModel : ValidatableViewModel<ITicketDetailPersonalView, Ticket>
    {

        private readonly SynchronizationContext context;

 
        public TicketDetailPersonalViewModel(ITicketDetailPersonalView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }


        public override void CurrentChanged(Ticket oldSelected)
        {

            RaisePropertyChanged(nameof(Gewerk));
            RaisePropertyChanged(nameof(Auftraggeber));
            RaisePropertyChanged(nameof(Auftragnehmer));
            RaisePropertyChanged(nameof(Auftragsleiter));
            RaisePropertyChanged(nameof(Abnahmeberechtigter));
        }


        public async Task SetzeListenAsync(IEnumerable<PersonalFilterModel> pslist, IEnumerable<NummerFilterModel> gwlist)
        {
 
            await context.SendAsync(new SendOrPostCallback((o) => { 
                
                PsSearchProvider.SetItems(pslist);
                Gewerkliste = gwlist;

            }), null);
        }


        public void Unload()
        {

            Gewerkliste = null;      
            PsSearchProvider.ClearItems(); 
        }
 

#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private IEnumerable<NummerFilterModel> gewerkliste;
        public IEnumerable<NummerFilterModel> Gewerkliste
        {
            get { return gewerkliste; }   
            set { SetProperty(ref gewerkliste, value); }
        }


        public NummerFilterModel Gewerk
        {
            get { return current?.Gewerk; }   
            set { 
                
                if (current != null && current.Gewerk != value) {
                    
                    current.Gewerk = value;
                    RaisePropertyChanged("Gewerk");
                }
            }
        }


        public PersonalFilterModel Auftraggeber
        {
            get { return current?.Auftraggeber; }   
            set { 
                
                if (current != null && current.Auftraggeber != value) {
                    
                    current.Auftraggeber = value;
                    RaisePropertyChanged("Auftraggeber");
                }
            }
        }


        public PersonalFilterModel Auftragnehmer
        {
            get { return current?.Auftragnehmer; }   
            set { 
                
                if (current != null && current.Auftragnehmer != value) {
                    
                    current.Auftragnehmer = value;
                    RaisePropertyChanged("Auftragnehmer");
                }
            }
        }


        public PersonalFilterModel Auftragsleiter
        {
            get { return current?.Auftragsleiter; }   
            set { 
                
                if (current != null && current.Auftragsleiter != value) {
                    
                    current.Auftragsleiter = value;
                    RaisePropertyChanged("Auftragsleiter");
                }
            }
        }


        public PersonalFilterModel Abnahmeberechtigter
        {
            get { return current?.Abnahmeberechtigter; }   
            set { 
                
                if (current != null && current.Abnahmeberechtigter != value) {
                    
                    current.Abnahmeberechtigter = value;
                    RaisePropertyChanged("Abnahmeberechtigter");
                }
            }
        }

       
        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


#endregion

    }
}
