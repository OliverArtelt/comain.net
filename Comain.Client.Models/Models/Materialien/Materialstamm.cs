﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{

    [DebuggerDisplay("{Name}")]
    public class Materialstamm : Entity, IDataErrorInfo, IComparable<Materialstamm>
    {
       
        private String              name;
        private String              nummer;
        private String              beschreibung;
        private String              herstellernummer;
        private decimal?            mischpreis;
        private Materialklasse?     klasse;
        private int?                einheit_Id;
        private int?                warengruppe_Id;

        public TrackableCollection<MaterialLieferant> MaterialLieferantMap { get; set; }


#region P R O P E R T I E S


        public String Name 
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        

        public String Nummer 
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public String Beschreibung 
        {
            get { return beschreibung; }
            set { SetProperty(ref beschreibung, value); }
        }
        

        public String Herstellernummer 
        {
            get { return herstellernummer; }
            set { SetProperty(ref herstellernummer, value); }
        }
        

        public decimal? Mischpreis 
        {
            get { return mischpreis; }
            set { SetProperty(ref mischpreis, value); }
        }
        

        public Materialklasse? Klasse 
        {
            get { return klasse; }
            set { SetProperty(ref klasse, value); }
        }
        

        public int? Einheit_Id 
        {
            get { return einheit_Id; }
            set { SetProperty(ref einheit_Id, value); }
        }
        

        public int? Warengruppe_Id 
        {
            get { return warengruppe_Id; }
            set { SetProperty(ref warengruppe_Id, value); }
        }
      

#endregion

        
        public int CompareTo(Materialstamm other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Herstellernummer"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 50) return "Die Nummer darf aus maximal 50 Zeichen bestehen."; 
                    break;

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie den Namen an.";
                    if (Name.Length > 150) return "Der Name darf aus maximal 150 Zeichen bestehen."; 
                    break;

                case "Herstellernummer":

                    if (String.IsNullOrWhiteSpace(Herstellernummer)) return null;
                    if (Herstellernummer.Length > 48) return "Die Herstellernummer darf aus maximal 48 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
