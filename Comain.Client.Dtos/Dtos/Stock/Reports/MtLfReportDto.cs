﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Dtos.Stock.Reports
{

    public class MtLfReportDto
    {

        public int                          Id                      { get; set; }
        public String                       Nummer                  { get; set; }
        public String                       Name                    { get; set; }
        public decimal?                     Mindestbestand          { get; set; }
        public decimal?                     Mischpreis              { get; set; }
       
        public NummerFilterModel            Warengruppe             { get; set; }
        public ICollection<ArtikelLfMap>    MaterialLieferantMap    { get; set; }
        public ICollection<ArtikelIhoMap>   MaterialIHObjektMap     { get; set; }
    }
}
