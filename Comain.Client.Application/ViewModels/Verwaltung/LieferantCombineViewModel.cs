﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class LieferantCombineViewModel : ViewModel<ILieferantCombineView>
    {

        protected readonly SynchronizationContext context;


        public LieferantCombineViewModel(ILieferantCombineView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


#region P R O P E R T I E S
  

        private ICommand executeCommand;
        public ICommand ExecuteCommand
        { 
            get { return executeCommand; }
            set { SetProperty(ref executeCommand, value); }
        }
  

        private IEnumerable lieferanten;
        public IEnumerable Lieferanten
        { 
            get { return lieferanten; }
            set { SetProperty(ref lieferanten, value); }
        }
  

        private Lieferant quelle;
        public Lieferant Quelle
        { 
            get { return quelle; }
            set { SetProperty(ref quelle, value); }
        }
  

        private String ziel;
        public String Ziel
        { 
            get { return ziel; }
            set { SetProperty(ref ziel, value); }
        }


#endregion


    }
}
