﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.Controllers.Verwaltung
{

    public interface ISUController : IVwTypController 
    {}


    public class SUController : SubListController, ISUController
    {

        public SUController(IMessageService messages, ISubListService store, IControllerBus bus,
                            VerwaltungViewModel mainView, SubListViewModel view) 
          : base(messages, store, bus, mainView, view)
        {}


        public override string Url          => @"/VW/SU";
        public override string Itemname     => @"Schadensursache";
        public override string SubItemname  => @"Subschadensursache";
    }
}
