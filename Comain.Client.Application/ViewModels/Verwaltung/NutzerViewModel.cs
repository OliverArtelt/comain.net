﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Views.Verwaltung;
using Comain.Client.Models.Users;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class NutzerViewModel : ValidatableViewModel<INutzerView, Nutzer>, IDelayableViewModel
    {

        protected readonly SynchronizationContext context = SynchronizationContext.Current;


        public NutzerViewModel(INutzerView view) : base(view)
        {}


        public override void CurrentChanged(Nutzer oldSelected)
        {

            Password1 = null;
            Password2 = null;

            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Name1));
            RaisePropertyChanged(nameof(Name2));
            RaisePropertyChanged(nameof(Password1));
            RaisePropertyChanged(nameof(Password2));
            RaisePropertyChanged(nameof(Beschreibung));
            RaisePropertyChanged(nameof(Email));
            RaisePropertyChanged(nameof(Status2));
            RaisePropertyChanged(nameof(Status3));
            RaisePropertyChanged(nameof(LetzteAnmeldung));
            RaisePropertyChanged(nameof(Personal_Id));

            RaisePropertyChanged(nameof(IstAdmin));
            RaisePropertyChanged(nameof(IstPie1));
            RaisePropertyChanged(nameof(IstPie2));
            RaisePropertyChanged(nameof(IstPie3));
            RaisePropertyChanged(nameof(IstCust1));
            RaisePropertyChanged(nameof(IstCust2));
            RaisePropertyChanged(nameof(IstCust3));
            RaisePropertyChanged(nameof(IstL1));
            RaisePropertyChanged(nameof(IstL2));
            RaisePropertyChanged(nameof(IstL3));
            RaisePropertyChanged(nameof(IstL4));
            RaisePropertyChanged(nameof(IstL5));
            RaisePropertyChanged(nameof(IstL6));
            RaisePropertyChanged(nameof(IstT1));
            RaisePropertyChanged(nameof(IstT2));
            RaisePropertyChanged(nameof(IstT3));
            RaisePropertyChanged(nameof(IstT4));
            RaisePropertyChanged(nameof(IstBevor));
            RaisePropertyChanged(nameof(IstAssetDelete));

            RaisePropertyChanged(nameof(RollenAsString));
            RaisePropertyChanged(nameof(UseStandortFilter));
            RaisePropertyChanged(nameof(StandortFilter));
            Standorte = null;

            if (Current == null) return;

            pzdic = Current.ParseStandortFilter();
            Standorte = pzlist.Select(p => new NutzerStandortViewItem(p, pzdic)).ToList();
        }


        public async Task LoadAsync(IList<Nutzer> uslist, IList<PersonalFilterModel> pslist, IList<PzFilterModel> pzlist)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Personalliste = pslist;
                this.pzlist = pzlist;

                var oldmodel = Current;
                Nutzerliste = new ObservableCollection<Nutzer>(uslist.OrderBy(p => p.Name));
                if (oldmodel != null) Current = uslist.FirstOrDefault(p => p.Id == oldmodel.Id);
                RaisePropertyChanged(nameof(Nutzerliste));
                RaisePropertyChanged(nameof(Personalliste));

            }), null);
        }


        public Nutzer GetCurrent()
        {

            if (Current == null) return Current;
            Current.SetStandortFilter(pzdic);
            return Current;
        }


        public void Unload()
        {

            Personalliste = null;
            Nutzerliste = null;
            Standorte = null;
            pzlist = null;
            pzdic = null;
        }


        private void ChangeRole(String name, bool add)
        {

            if (current == null) return;
            if (!add) current.Rollen.Remove(name);
            else if (!current.Rollen.Contains(name)) current.Rollen.Add(name);
            RaisePropertyChanged("RollenAsString");
            current.RaisePropertyChanged("RollenAsString");
        }


        private bool IsInRole(String name)
        {
            return current != null && current.Rollen != null && current.Rollen.Contains(name);
        }


#region P R O P E R T I E S


        private IList<PzFilterModel> pzlist;
        private HashSet<int> pzdic;


        public IList<PersonalFilterModel>       Personalliste       { get; private set; }
        public ObservableCollection<Nutzer>     Nutzerliste         { get; private set; }


        private IList<NutzerStandortViewItem>   standorte;
        public IList<NutzerStandortViewItem>    Standorte
        {
            get => standorte;
            private set => SetProperty(ref standorte, value);
        }


        public int? Personal_Id
        {

            get => current?.Personal_Id;
            set {

                if (current != null && current.Personal_Id != value) {

                    current.Personal_Id = value;
                    RaisePropertyChanged("Personal_Id");
                }
            }
        }


        public DateTime? LetzteAnmeldung
        {
            get => current?.LetzteAnmeldung;
        }


        public String RollenAsString
        {
            get => current?.RollenAsString;
        }


        public bool UseStandortFilter
        {
            get => current == null? false: current.UseStandortFilter;
            set {

                if (current != null && current.UseStandortFilter != value) {

                    current.UseStandortFilter = value;
                    RaisePropertyChanged(nameof(UseStandortFilter));
                }
            }
        }


        public String StandortFilter
        {

            get => current?.StandortFilter;
            set {

                if (current != null && current.StandortFilter != value) {

                    current.StandortFilter = value;
                    RaisePropertyChanged(nameof(StandortFilter));
                }
            }
        }


        public String Name
        {

            get => current?.Name;
            set {

                if (current != null && current.Name != value) {

                    current.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }


        public String Name1
        {

            get => current?.Name1;
            set {

                if (current != null && current.Name1 != value) {

                    current.Name1 = value;
                    RaisePropertyChanged("Name1");
                }
            }
        }


        public String Password1
        {

            get => current?.Password1;
            set {

                if (current != null && current.Password1 != value) {

                    current.Password1 = value;
                    RaisePropertyChanged("Password1");
                }
            }
        }


        public String Password2
        {

            get => current?.Password2;
            set {

                if (current != null && current.Password2 != value) {

                    current.Password2 = value;
                    RaisePropertyChanged("Password2");
                }
            }
        }


        public String Beschreibung
        {

            get => current?.Beschreibung;
            set {

                if (current != null && current.Beschreibung != value) {

                    current.Beschreibung = value;
                    RaisePropertyChanged("Beschreibung");
                }
            }
        }


        public String Name2
        {

            get => current?.Name2;
            set {

                if (current != null && current.Name2 != value) {

                    current.Name2 = value;
                    RaisePropertyChanged("Name2");
                }
            }
        }


        public String Email
        {

            get => current?.Email;
            set {

                if (current != null && current.Email != value) {

                    current.Email = value;
                    RaisePropertyChanged("Email");
                }
            }
        }


        public bool Status2
        {

            get { return current == null? false: current.Status == LoginStatus.Aktiv; }
            set {

                if (current != null && value && current.Status != LoginStatus.Aktiv) {

                    current.Status = LoginStatus.Aktiv;
                    RaisePropertyChanged("Status2");
                    RaisePropertyChanged("Status3");
                }
            }
        }


        public bool Status3
        {

            get { return current == null? false: current.Status == LoginStatus.Gesperrt; }
            set {

                if (current != null && value && current.Status != LoginStatus.Gesperrt) {

                    current.Status = LoginStatus.Gesperrt;
                    RaisePropertyChanged("Status2");
                    RaisePropertyChanged("Status3");
                }
            }
        }


        public bool IstAdmin
        {

            get { return IsInRole("ADMIN"); }
            set {

                ChangeRole("ADMIN", value);
                RaisePropertyChanged("IstAdmin");
            }
        }


        public bool IstPie1
        {

            get { return IsInRole("PIE1"); }
            set {

                ChangeRole("PIE1", value);
                RaisePropertyChanged("IstPie1");
            }
        }


        public bool IstPie2
        {

            get { return IsInRole("PIE2"); }
            set {

                ChangeRole("PIE2", value);
                RaisePropertyChanged("IstPie2");
            }
        }


        public bool IstPie3
        {

            get { return IsInRole("PIE3"); }
            set {

                ChangeRole("PIE3", value);
                RaisePropertyChanged("IstPie3");
            }
        }


        public bool IstCust1
        {

            get { return IsInRole("CUST1"); }
            set {

                ChangeRole("CUST1", value);
                RaisePropertyChanged("IstCust1");
            }
        }


        public bool IstCust2
        {

            get { return IsInRole("CUST2"); }
            set {

                ChangeRole("CUST2", value);
                RaisePropertyChanged("IstCust2");
            }
        }


        public bool IstCust3
        {

            get { return IsInRole("CUST3"); }
            set {

                ChangeRole("CUST3", value);
                RaisePropertyChanged("IstCust3");
            }
        }


        public bool IstL1
        {

            get { return IsInRole("L1"); }
            set {

                ChangeRole("L1", value);
                RaisePropertyChanged("IstL1");
            }
        }


        public bool IstL2
        {

            get { return IsInRole("L2"); }
            set {

                ChangeRole("L2", value);
                RaisePropertyChanged("IstL2");
            }
        }


        public bool IstL3
        {

            get { return IsInRole("L3"); }
            set {

                ChangeRole("L3", value);
                RaisePropertyChanged("IstL3");
            }
        }


        public bool IstL4
        {

            get { return IsInRole("L4"); }
            set {

                ChangeRole("L4", value);
                RaisePropertyChanged("IstL4");
            }
        }


        public bool IstL5
        {

            get { return IsInRole("L5"); }
            set {

                ChangeRole("L5", value);
                RaisePropertyChanged("IstL5");
            }
        }


        public bool IstL6
        {

            get { return IsInRole("L6"); }
            set {

                ChangeRole("L6", value);
                RaisePropertyChanged("IstL6");
            }
        }



        public bool IstT1
        {

            get { return IsInRole("Ticket1"); }
            set {

                ChangeRole("Ticket1", value);
                RaisePropertyChanged("IstT1");
            }
        }


        public bool IstT2
        {

            get { return IsInRole("Ticket2"); }
            set {

                ChangeRole("Ticket2", value);
                RaisePropertyChanged("IstT2");
            }
        }


        public bool IstT3
        {

            get { return IsInRole("Ticket3"); }
            set {

                ChangeRole("Ticket3", value);
                RaisePropertyChanged("IstT3");
            }
        }


        public bool IstT4
        {

            get { return IsInRole("Ticket4"); }
            set {

                ChangeRole("Ticket4", value);
                RaisePropertyChanged("IstT4");
            }
        }


        public bool IstBevor
        {

            get { return IsInRole("Bevor"); }
            set {

                ChangeRole("Bevor", value);
                RaisePropertyChanged("IstBevor");
            }
        }


        public bool IstAssetDelete
        {

            get { return IsInRole("AssetDelete"); }
            set {

                ChangeRole("AssetDelete", value);
                RaisePropertyChanged("AssetDelete");
            }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


#endregion


    }
}
