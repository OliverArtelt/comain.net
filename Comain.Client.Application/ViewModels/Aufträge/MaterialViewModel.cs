﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.Aufträge;
using Comain.Client.Dtos.Filters;
using System.Collections;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{

    public class MaterialViewModel : ViewModel<IMaterialView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly ILoginService login;

        private Auftrag          currentAuftrag;
        private Materialleistung currentMaterial;
        private IEnumerable<LieferantFilterModel> lieferanten;


        public MaterialViewModel(IMaterialView view, ILoginService login) : base(view)
        {
            
            context = SynchronizationContext.Current;
            this.login = login;
        }


        public void SetzeModel(Auftrag au, Materialleistung mt)
        {
            
            currentAuftrag = au;
            currentMaterial = mt;
            FreieMaterialWahl = !mt.Materialstamm_Id.HasValue;
                                   
            RaisePropertyChanged();
        }


        public void SetzeListen(IEnumerable<Dtos.Filters.NummerFilterModel> ehlist,
                                IEnumerable<Dtos.Filters.MaterialstammFilterModel> mtlist,
                                IEnumerable<LieferantFilterModel> lflist)
        {

            context.Post(new SendOrPostCallback((o) => { 
        
                Einheiten = ehlist;
                Materialstammliste = mtlist;
                lieferanten = lflist;
                UpdateLfList();
                                   
                RaisePropertyChanged("Einheiten");
                RaisePropertyChanged("Materialstammliste");
            }), null);
        }


        private void UpdateLfList()
        {

            if (Materialstamm_Id.HasValue && !lieferanten.IsNullOrEmpty()) {

                var mtstamm = Materialstammliste.FirstOrDefault(p => p.Id == Materialstamm_Id.Value);
                if (mtstamm != null && !mtstamm.MaterialLieferantMap.IsNullOrEmpty()) {

                    Lieferanten = lieferanten.Where(p => mtstamm.MaterialLieferantMap.Any(q => q.Lieferant_Id == p.Id)).OrderBy(p => p.Name1).ToList();
                    if (EditEnabled && currentMaterial != null && Lieferant_Id == 0 && Lieferanten.Count() == 1) Lieferant_Id = Lieferanten.First().Id;
                    RaisePropertyChanged("Lieferanten");
                    return;
                }
            }    

            Lieferanten = lieferanten;
            RaisePropertyChanged("Lieferanten");
        }


        private void RaisePropertyChanged()
        {

            FreieMaterialWahl = currentMaterial != null && !currentMaterial.Materialstamm_Id.HasValue;
            UpdateLfList();

            RaisePropertyChanged("Datum");
            RaisePropertyChanged("Mwst");
            RaisePropertyChanged("Handlingszuschlag");
            RaisePropertyChanged("Gesamtpreis");
            RaisePropertyChanged("Einzelpreis");
            RaisePropertyChanged("Menge");
            RaisePropertyChanged("Beschreibung");
            RaisePropertyChanged("Name");
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Materialstamm_Id");
            RaisePropertyChanged("Lieferant_Id");
            RaisePropertyChanged("Einheit_Id");
            RaisePropertyChanged("SperrenSichtbar");
            RaisePropertyChanged("EntsperrenSichtbar");
            RaisePropertyChanged("HinweisAbrechnungsschluss");

            RaiseStatusPropertyChanged();
        }


        private void RaiseStatusPropertyChanged()
        {

            RaisePropertyChanged("StatusUndefiniert");
            RaisePropertyChanged("StatusOffen");
            RaisePropertyChanged("StatusAngebotPlanung");
            RaisePropertyChanged("StatusAuftrag");
            RaisePropertyChanged("StatusKauf");
            RaisePropertyChanged("StatusLieferung");
            RaisePropertyChanged("StatusEingebaut");
        }


        public void Unload()
        {

            Einheiten = null;
            Materialstammliste = null;
            Lieferanten = null;
            ChecklistPositionen = null;
        }


        public void ClearErrors()
        {

            ShowValidator = false;
            Errors = null;
        }


        public void SetErrors(IList<String> errors)
        {

            Errors = errors.Select(p => "• " + p).ToList();
            ShowValidator = true;
        }


        public virtual void SetErrors(Exception x)
        {

            Errors = new List<String> { "• " + x.Message };
            ShowValidator = true;
        }


#region P R O P E R T I E S
  
     
        public IEnumerable<NummerFilterModel>           Einheiten           { get; private set; }
        public IEnumerable<MaterialstammFilterModel>    Materialstammliste  { get; private set; }
        public IEnumerable<LieferantFilterModel>        Lieferanten         { get; private set; }


        public bool ChecklistVisible => !ChecklistPositionen.IsNullOrEmpty();


        private IEnumerable checklistPositionen;
        public IEnumerable ChecklistPositionen
        {
            get { return checklistPositionen; }   
            set { if (SetProperty(ref checklistPositionen, value)) RaisePropertyChanged(nameof(ChecklistVisible)); }   
        }

     
        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }   
            set { SetProperty(ref saveCommand, value); }   
        }
 
     
        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }   
            set { SetProperty(ref addCommand, value); }   
        }
 
     
        private ICommand sperrenCommand;
        public ICommand SperrenCommand
        {
            get { return sperrenCommand; }   
            set { SetProperty(ref sperrenCommand, value); }   
        }
 
     
        private ICommand entsperrenCommand;
        public ICommand EntsperrenCommand
        {
            get { return entsperrenCommand; }   
            set { SetProperty(ref entsperrenCommand, value); }   
        }
 
     
        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }   
            set { SetProperty(ref deleteCommand, value); }   
        }
 
     
        private String header;
        public String Header
        {
            get { return header; }   
            set { SetProperty(ref header, value); }   
        }
 
     
        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }   
        }
 

        private bool showValidator;
        public bool ShowValidator
        {
            get { return showValidator; }   
            set { SetProperty(ref showValidator, value); }   
        }
 

        private List<String> errors;
        public List<String> Errors
        { 
            get { return errors; }
            set { SetProperty(ref errors, value); }
        }
 
     
        private bool freieMaterialWahl;
        public bool FreieMaterialWahl
        {
            get { return freieMaterialWahl; }   
            set { if (SetProperty(ref freieMaterialWahl, value) && value) Materialstamm_Id = null; }   
        }

  
        public bool StatusUndefiniert
        {
            get { return currentMaterial == null? false: !currentMaterial.Status.HasValue; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.HasValue) {

                    currentMaterial.Status = null;
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusOffen
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.Offen; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.Offen) {

                    currentMaterial.Status = Materialstatus.Offen;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusAngebotPlanung
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.AngebotPlanung; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.AngebotPlanung) {

                    currentMaterial.Status = Materialstatus.AngebotPlanung;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusAuftrag
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.Auftrag; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.Auftrag) {

                    currentMaterial.Status = Materialstatus.Auftrag;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusKauf
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.Kauf; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.Kauf) {

                    currentMaterial.Status = Materialstatus.Kauf;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusLieferung
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.Lieferung; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.Lieferung) {

                    currentMaterial.Status = Materialstatus.Lieferung;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public bool StatusEingebaut
        {
            get { return currentMaterial == null? false: currentMaterial.Status.GetValueOrDefault() == Materialstatus.Eingebaut; }   
            set { 
            
                if (currentMaterial != null && value && currentMaterial.Status.GetValueOrDefault() != Materialstatus.Eingebaut) {

                    currentMaterial.Status = Materialstatus.Eingebaut;    
                    RaiseStatusPropertyChanged();    
                }
            }   
        }

  
        public int? Einheit_Id
        {
            get { return currentMaterial == null? 0: currentMaterial.Einheit_Id; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Einheit_Id != value) {

                    currentMaterial.Einheit_Id = value;    
                    RaisePropertyChanged("Einheit_Id");
                }
            }   
        }
 
  
        public int? Lieferant_Id
        {
            get { return currentMaterial == null? 0: currentMaterial.Lieferant_Id; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Lieferant_Id != value) {

                    currentMaterial.Lieferant_Id = value;    
                    RaisePropertyChanged("Lieferant_Id");

                    if (value != 0 && !Materialstammliste.IsNullOrEmpty() && Materialstamm_Id.HasValue) {
                        
                        var mtstamm = Materialstammliste.FirstOrDefault(p => p.Id == Materialstamm_Id.Value);
                        if (mtstamm != null && !mtstamm.MaterialLieferantMap.IsNullOrEmpty()) {

                            var lfmap = mtstamm.MaterialLieferantMap.FirstOrDefault(p => p.Lieferant_Id == value);
                            if (lfmap != null) {

                                if (!String.IsNullOrEmpty(lfmap.Nummer)) Nummer = lfmap.Nummer;
                                if (!String.IsNullOrEmpty(lfmap.Name)) Name = lfmap.Name;
                                if (lfmap.Preis.HasValue) Einzelpreis = lfmap.Preis.Value;
                            }
                        }
                    }
                }
            }   
        }
 
  
        public int? Materialstamm_Id
        {
            get { return currentMaterial == null? null: currentMaterial.Materialstamm_Id; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Materialstamm_Id != value) {

                    currentMaterial.Materialstamm_Id = value;    
                    RaisePropertyChanged("Materialstamm_Id");

                    if (value.HasValue) {

                        var mtstamm = Materialstammliste.FirstOrDefault(p => p.Id == value.Value);
                        if (mtstamm != null) {

                            Name = mtstamm.Name;
                            Nummer = mtstamm.Nummer;
                            if (mtstamm.Mischpreis.HasValue) Einzelpreis = mtstamm.Mischpreis.Value;
                            if (mtstamm.Einheit != null) Einheit_Id = mtstamm.Einheit.Id;
                            UpdateLfList();
                        }
                    }
                }
            }   
        }
 
  
        public String Nummer
        {
            get { return currentMaterial == null? null: currentMaterial.Nummer; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Nummer != value) {

                    currentMaterial.Nummer = value;    
                    RaisePropertyChanged("Nummer");
                }
            }   
        }
 
  
        public String Name
        {
            get { return currentMaterial == null? null: currentMaterial.Name; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Name != value) {

                    currentMaterial.Name = value;    
                    RaisePropertyChanged("Name");
                }
            }   
        }
 
  
        public String Beschreibung
        {
            get { return currentMaterial == null? null: currentMaterial.Beschreibung; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Beschreibung != value) {

                    currentMaterial.Beschreibung = value;    
                    RaisePropertyChanged("Beschreibung");
                }
            }   
        }
 
  
        public decimal Menge
        {
            get { return currentMaterial == null? 1: currentMaterial.Menge; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Menge != value) {

                    currentMaterial.Menge = value;    
                    RaisePropertyChanged("Menge");
                }
            }   
        }
 
  
        public decimal Einzelpreis
        {
            get { return currentMaterial == null? 0: currentMaterial.Einzelpreis; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Einzelpreis != value) {

                    currentMaterial.Einzelpreis = value;    
                    RaisePropertyChanged("Einzelpreis");
                }
            }   
        }
 
  
        public decimal? Gesamtpreis
        {
            get { return currentMaterial == null? null: currentMaterial.Gesamtpreis; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Gesamtpreis != value) {

                    currentMaterial.Gesamtpreis = value;    
                    RaisePropertyChanged("Gesamtpreis");
                }
            }   
        }
 
  
        public decimal? Handlingszuschlag
        {
            get { return currentMaterial == null? null: currentMaterial.Handlingszuschlag; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Handlingszuschlag != value) {

                    currentMaterial.Handlingszuschlag = value;    
                    RaisePropertyChanged("Handlingszuschlag");
                }
            }   
        }
 
  
        public decimal? Mwst
        {
            get { return currentMaterial == null? null: currentMaterial.Mwst; }   
            set { 
            
                if (currentMaterial != null && currentMaterial.Mwst != value) {

                    currentMaterial.Mwst = value;    
                    RaisePropertyChanged("Mwst");
                }
            }   
        }
 
  
        public DateTime? Datum
        {
            get { return currentMaterial == null? default(DateTime): currentMaterial.Datum; }   
            set { 
            
                if (currentMaterial != null && value.HasValue && currentMaterial.Datum != value.GetValueOrDefault()) {

                    currentMaterial.Datum = value.Value;    
                    RaisePropertyChanged("Datum");
                }
            }   
        }
 
  
        public bool SperrenSichtbar
        {
            get { return login.AktuellerNutzer != null && login.AktuellerNutzer.IstAdministrator && currentMaterial != null && !currentMaterial.Versiegelt && EditEnabled; }   
        }
 
  
        public bool EntsperrenSichtbar
        {
            get { return login.AktuellerNutzer != null && login.AktuellerNutzer.IstAdministrator && currentMaterial != null && currentMaterial.Versiegelt && EditEnabled; }   
        }
  
     
        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }   
            set { SetProperty(ref showDelay, value); }   
        }
   
     
        public String HinweisAbrechnungsschluss 
        { 
        
            get { 
            
                if (currentAuftrag == null || currentAuftrag.Konfiguration == null) return null;
                if (currentMaterial != null && currentMaterial.Versiegelt) return "Material/Subleistung ist versiegelt";
                if (!currentAuftrag.Konfiguration.Abrechnungsschluß.HasValue) return null;
                if (!currentAuftrag.IstOffen) return null;
                return String.Format("Abrechnungsschluß: {0}", currentAuftrag.Konfiguration.Abrechnungsschluß.Value.ToShortDateString());
            } 
        }  


#endregion

    }
}
