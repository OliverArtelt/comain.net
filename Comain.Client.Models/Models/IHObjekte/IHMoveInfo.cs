﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Models.IHObjekte
{

    public class IHMoveInfo
    {

        public String                   Objekttyp           { get; set; }
        public int                      AnzahlTeilobjekte   { get; set; }
        public int                      AnzahlMassnahen     { get; set; }
        public String                   AlteNummer          { get; set; }
        public String                   NeueNummer          { get; set; }
        public String                   NeuePosition        { get; set; }
        public int                      SourceId            { get; set; }
        public int                      DestinationId       { get; set; }
        public MoveDestinationType      Umzugtyp            { get; set; }

        public PzFilterModel            StandortAlt         { get; set; }
        public PzFilterModel            StandortNeu         { get; set; }
        public KstFilterModel           KostenstelleAlt     { get; set; }
        public KstFilterModel           KostenstelleNeu     { get; set; }
        public List<int>                KstAmStandort       { get; set; }

        public List<AuftragSelektor>    OffeneAufträge      { get; set; }
    }
}
