﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class LbAuViewModel : ViewModel<ILbAuView>, IFilterViewModel
    {

        public LbAuViewModel(ILbAuView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            
            TypNummer = true;
            Mwst = true;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            String s = "Ausgabe mit " + typ;
            if (Mwst) s += ", mit Mwst";
            return s;
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Mwst) filter.Add("Mwst", true);
            filter.Add("Ausgabe", typ);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);     
        }


        public void Unload()
        {}


#region P R O P E R T I E S
      

        private bool mwst;
        public bool Mwst
        { 
            get { return mwst; }
            set { SetProperty(ref mwst, value); }
        } 
      

        private bool showPersonal;
        public bool ShowPersonal
        { 
            get { return showPersonal; }
            set { SetProperty(ref showPersonal, value); }
        } 
      

        private String typ = "Nummer";
        
        public bool TypNummer
        { 
            get { return typ == "Nummer"; }
            set { 
            
                if (value && typ != "Nummer") {
                    
                    typ = "Nummer";
                    RaisePropertyChanged("TypNummer");
                    RaisePropertyChanged("TypBeschreibung");
                    RaisePropertyChanged("TypIHObjekt");
                    RaisePropertyChanged("TypPersonal");
                }
            }
        } 
        
        public bool TypBeschreibung
        { 
            get { return typ == "Beschreibung"; }
            set { 
            
                if (value && typ != "Beschreibung") {
                    
                    typ = "Beschreibung";
                    RaisePropertyChanged("TypNummer");
                    RaisePropertyChanged("TypBeschreibung");
                    RaisePropertyChanged("TypIHObjekt");
                    RaisePropertyChanged("TypPersonal");
                }
            }
        } 
        
        public bool TypIHObjekt
        { 
            get { return typ == "Asset"; }
            set { 
            
                if (value && typ != "Asset") {
                    
                    typ = "Asset";
                    RaisePropertyChanged("TypNummer");
                    RaisePropertyChanged("TypBeschreibung");
                    RaisePropertyChanged("TypIHObjekt");
                    RaisePropertyChanged("TypPersonal");
                }
            }
        } 
        
        public bool TypPersonal
        { 
            get { return typ == "Personal"; }
            set { 
            
                if (value && typ != "Personal") {
                    
                    typ = "Personal";
                    RaisePropertyChanged("TypNummer");
                    RaisePropertyChanged("TypBeschreibung");
                    RaisePropertyChanged("TypIHObjekt");
                    RaisePropertyChanged("TypPersonal");
                }
            }
        } 


#endregion

    }
}
