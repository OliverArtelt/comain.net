﻿using System.Collections.Generic;
using System.Linq;
using Comain.Client.Services;

namespace Comain.Client.Controllers.Verwaltung
{

    public class VwTypFactory : IVwTypFactory
    {

        private readonly List<IVwTyp> einheiten;
        private readonly ILoginService login;


        public VwTypFactory(IEnumerable<IVwTyp> einheiten, ILoginService login)
        {
            
            this.einheiten = einheiten.OrderBy(p => p.Sortierung).ToList(); 
            this.login = login; 
        }


        public IList<IVwTyp> AlleEinheiten()
        {
            return einheiten;
        }


        public IList<IVwTyp> VerfügbareEinheiten()
        {
            return einheiten.Where(p => p.HatZugriff(login.AktuellerNutzer)).ToList();
        }
    }
}
