﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Dtos.WPlan;
using Comain.Client.ReportModels.WPlan;
using System.Windows.Media;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Ein Standort mit Summeninformationen
    /// </summary>
    [DebuggerDisplay("{Bezeichnung}")]
    public class PZPlanzeile : NotifyBase, IPlanzeile, IAcceptor
    {

        private readonly StandortDto  pz;


        public PZPlanzeile(Plan parent, StandortDto pz)
        {

            this.pz = pz;
            Parent = parent;
            Summen = Enumerable.Range(0, Parent.Spalten.Count).Select(ndx => new PZSpaltensumme(this, Parent.Spalten[ndx])).ToList();
        }


        public bool                     IstSichtbar             => Children.Any(p => p.IstSichtbar);
        public double                   Height                  => IstSichtbar? 40: 0;
        public double                   YStartposition          { get; set; }
        public Plan                     Parent                  { get; }

        public int                      Id                      => pz.Id;
        public String                   Nummer                  => pz.Nummer.ToString();
        public String                   AnzeigeName             => Name;
        public String                   Name                    => pz.Name;
        public String                   Bezeichnung             => $"{Nummer} - {Name}";
        public String                   DeaktiviertText         => default;
        public String                   Kurzbeschreibung        => default;
        public String                   Durchführung            => default;
        public String                   Kostenstelle            => default;
        public String                   Leistungsart            => default;
        public String                   Gewerk                  => default;
        public DateTime?                Start                   => default;
        public String                   ZyklusAsString          => default;
        public String                   ExterneAuftragsnummer   => default;
        public String                   TermintypText           => default;
        public List<MaterialInfoDto>    MaterialInfo            => default;

        public decimal?                 Stunden                 => Children.Where(p => p.IstSichtbar).Sum(p => p.Stunden);
        public decimal?                 Kosten                  => Children.Where(p => p.IstSichtbar).Sum(p => p.Kosten);
        public decimal?                 Budget48                => default;
        public decimal?                 Budget52                => default;

        public bool                     AllowDrop               => false;
        public bool                     ExtendedTooltip         => false;
        public Brush                    TypAsFarbe              => Brushes.Wheat;
        public double                   XStartposition          => 0d;
        public String                   Icon                    => "appbar_pin";
        public int                      Zeilenfarbenindex       => -1;
        public int?                     Menge                   => null;


        public String Info => $"Standort {Nummer} {Name}";


        public List<PZSpaltensumme> Summen { get; }


        public bool    ImExportSichtbar(PlanOptions opts)               => Children.Any(p => p.ImExportSichtbar(opts));
        public decimal ExportierteGeplanteStunden(PlanOptions opts)     => Children.Sum(p => p.ExportierteGeplanteStunden(opts));
        public decimal ExportierteVerbrauchteStunden(PlanOptions opts)  => Children.Sum(p => p.ExportierteVerbrauchteStunden(opts));
        public decimal ExportierteGeplanteKosten(PlanOptions opts)      => Children.Sum(p => p.ExportierteGeplanteKosten(opts));
        public decimal ExportierteVerbrauchteKosten(PlanOptions opts)   => Children.Sum(p => p.ExportierteVerbrauchteKosten(opts));


        private List<IHPlanzeile> children;
        public List<IHPlanzeile> Children
        {
            get => children ?? (children = new List<IHPlanzeile>());
            set => children = value;
        }


        public IEnumerable<Planaufgabe> GebePositionen(int? mincol = null, int? maxcol = null)
            => Children.SelectMany(p => p.GebePositionen(mincol, maxcol));


        public void Accept(IVisitor v)
        {

            v.Visit(this);
            Children.ForEach(p => p.Accept(v));
            Summen.ForEach(p => p.Accept(v));
            v.VisitAfter(this);
        }


        public bool Matches(String searchTerm)
            => Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
               Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase);
    }
}
