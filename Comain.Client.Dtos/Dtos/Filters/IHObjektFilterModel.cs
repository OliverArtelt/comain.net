﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class IHObjektFilterModel : EntityModelBase
    {

        public int      Standort_Id             { get; set; }
        public int      Kostenstelle_Id         { get; set; }
        public String   Nummer                  { get; set; }
        public String   Name                    { get; set; }
        public String   Inventarnummer          { get; set; }
        public String   Fabriknummer            { get; set; }
        public int?     Status                  { get; set; }
        public int?     Erhaltklasse            { get; set; }
    }
}
