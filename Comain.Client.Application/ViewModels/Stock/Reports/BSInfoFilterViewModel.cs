﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class BSInfoFilterViewModel : ViewModel<IBSInfoFilterView>, IFilterViewModel
    {

        public BSInfoFilterViewModel(IBSInfoFilterView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            SortArtikelname = true;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (SortArtikelnummer) return "Sortierung Artikelnummer";
            else if (SortLieferant) return "Sortierung Lieferant";
            else return "Sortierung Artikelbezeichnung";
        }


        public void AddFilter(Suchkriterien filter)
        {
            
            if (SortArtikelnummer) filter.Add("Sortierung", "Artikelnummer");
            else if (SortLieferant) filter.Add("Sortierung", "Lieferant");
            else filter.Add("Sortierung", "Artikelbezeichnung");
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private bool sortArtikelname;
        public bool SortArtikelname
        { 
            get { return sortArtikelname; }
            set { 
            
                if (SetProperty(ref sortArtikelname, value)) {
                
                    if (value) SortArtikelnummer = false;   
                    if (value) SortLieferant = false;   
                }
            }
        } 
   

        private bool sortArtikelnummer;
        public bool SortArtikelnummer
        { 
            get { return sortArtikelnummer; }
            set { 
            
                if (SetProperty(ref sortArtikelnummer, value)) {
                
                    if (value) SortArtikelname = false;   
                    if (value) SortLieferant = false;   
                }
            }
        } 
   

        private bool sortLieferant;
        public bool SortLieferant
        { 
            get { return sortLieferant; }
            set { 
            
                if (SetProperty(ref sortLieferant, value)) {
                
                    if (value) SortArtikelnummer = false;   
                    if (value) SortArtikelname = false;   
                }
            }
        } 
     

#endregion

    }
}
