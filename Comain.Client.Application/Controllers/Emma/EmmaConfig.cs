﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Controllers.Emma;
using Comain.Client.ViewModels.Emma;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Stock.Administration.Types
{

    public class EmmaConfig : IAdminTyp
    {
        
        public String       Title           => "EMMA-Import"; 
        public int          Sortierung      => 100000; 
        public Type         Controller      => typeof(IEmmaConfigController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulEmma;
    }
}

