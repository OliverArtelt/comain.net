﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan.Zeitstrahl;

namespace Comain.Client.Controllers.WPlan.Planen.Commands
{
    public interface IResultCommand
    {
        IEnumerable<PositionSubmitDto> Modifications();
    }
}
