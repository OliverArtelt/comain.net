﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos;
using Comain.Client.Gateways;
using Comain.Client.Models;

namespace Comain.Client.Services
{

    public class ModulService : IModulService
    {

        private readonly IRepository store;
        private ModulInfo module;


        public ModulInfo Info => module ?? new ModulInfo();


        public ModulService(IRepository store)
            => this.store = store;


        public async Task LoadAsync(CancellationToken ct = default)
            => module = await store.GetAsync<ModulInfo>("/Module", ct).ConfigureAwait(false);


        public void Clear()
            => module = null;
    }
}
