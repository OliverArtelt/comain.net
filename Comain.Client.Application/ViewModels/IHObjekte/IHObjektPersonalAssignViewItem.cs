﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Users;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektPersonalAssignViewItem : NotifyBase
    {

        private PersonalFilterModel psmodel;
        private IHObjekt ihmodel;
        private IHObjektPersonal model;


        public IHObjektPersonalAssignViewItem(IHObjekt ihmodel, PersonalFilterModel psmodel, IHObjektPersonal model)
        {

            this.psmodel = psmodel;
            this.ihmodel = ihmodel;
            this.model = model;
        }


        public bool IsSelected
        {
            get => model != null;
            set => Select(value);
        }


        public void Select(bool select)
        {

            if (!select && model != null) {

                ihmodel.Personalliste.Remove(model);
                model = null;
                RaisePropertyChanged(nameof(IsSelected));

            } else if (select) {

                model = new IHObjektPersonal { IHObjekt_Id = ihmodel.Id, Personal_Id = psmodel.Id, Rolle = Rolle };
                ihmodel.Personalliste.Add(model);
                RaisePropertyChanged(nameof(IsSelected));
            }
        }


        private Personalrolle Rolle { get; set; }


        public bool IstTeamleiter
        {
            get => model != null && model.Rolle == Personalrolle.Teamleiter;
            set {
                if (model != null) {

                    model.Rolle = value? Personalrolle.Teamleiter: Personalrolle.Nutzer;
                    RaisePropertyChanged(nameof(IstTeamleiter));
                }
            }
        }


        public String Assetnummer       => ihmodel.Nummer;
        public String Asset             => ihmodel.Name;

        public String Personalklammer   => psmodel.Klammer;
        public String Personalnummer    => psmodel.Nummer;
        public String Nachname          => psmodel.Nachname;
        public String Vorname           => psmodel.Vorname;
    }
}
