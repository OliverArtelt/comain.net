﻿using System;


namespace Comain.Client.ViewData
{

    public class ConnectionViewItem
    {
      
        public String Key               { get; set; }
        public String Name              { get; set; }
        public String Color             { get; set; }
    }
}
