﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain;
using Comain.Client.Dtos.Reports;

namespace Comain.Client.ViewData.Reports
{

    public class ArtikelDetailItem 
    {
        
        public  String      Name                { get; private set; }
        public  String      Nummer              { get; private set; }
        public  String      Beschreibung        { get; private set; }
        public  String      Herstellernummer    { get; private set; }
        public  decimal?    Mischpreis          { get; private set; }
        public  String      Klasse              { get; private set; }
        public  decimal?    Mindestbestand      { get; private set; }
        public  decimal?    Mindestbestellmenge { get; private set; }
        public  decimal?    BestandReal         { get; private set; }
        public  String      Einheit             { get; private set; }
        public  String      Warengruppe         { get; private set; }
        public  String      StandardLagerort    { get; private set; }
        public  DateTime?   DeaktiviertAm       { get; private set; }


        public  IEnumerable<LagerortItem>   Lagerorte     { get; private set; }
        public  IEnumerable<LieferantItem>  Lieferanten   { get; private set; }
        public  IEnumerable<IHObjektItem>   IHObjekte     { get; private set; }

    
        public ArtikelDetailItem(ArtikelDto model)
        {

            Name                = model.Name;            
            Nummer              = model.Nummer;          
            Beschreibung        = model.Beschreibung;    
            Herstellernummer    = model.Herstellernummer;
            Mischpreis          = model.Mischpreis;      
            Mindestbestand      = model.Mindestbestand;  
            Mindestbestellmenge = model.Mindestbestellmenge;  
            BestandReal         = model.BestandReal;     
            DeaktiviertAm       = model.DeaktiviertAm;     

            Klasse = model.Klasse?.ToString();          

            if (model.Einheit != null) {

                if (!String.IsNullOrEmpty(model.Einheit.Nummer) && !String.IsNullOrEmpty(model.Einheit.Name)) Einheit = String.Format("{0} - {1}", model.Einheit.Nummer, model.Einheit.Name);
                if (!String.IsNullOrEmpty(model.Einheit.Name)) Einheit = model.Einheit.Name;
                Einheit = model.Einheit.Nummer;
            }

            if (model.Warengruppe != null) {

                if (!String.IsNullOrEmpty(model.Warengruppe.Nummer) && !String.IsNullOrEmpty(model.Warengruppe.Name)) Warengruppe = String.Format("{0} - {1}", model.Warengruppe.Nummer, model.Warengruppe.Name);
                else if (!String.IsNullOrEmpty(model.Warengruppe.Name)) Warengruppe = model.Warengruppe.Name;
                else Warengruppe = model.Warengruppe.Nummer;
            }

            Lagerorte = new List<LagerortItem>();
            Lieferanten = new List<LieferantItem>();
            IHObjekte = new List<IHObjektItem>();
            if (!model.MaterialLieferantMap.IsNullOrEmpty()) Lieferanten = model.MaterialLieferantMap.Select(p => new LieferantItem(p)).OrderBy(p => p.Rank).ThenBy(p => p.Name1).ToList();
            if (!model.MaterialIHObjektMap.IsNullOrEmpty())  IHObjekte = model.MaterialIHObjektMap.Select(p => new IHObjektItem(p)).OrderBy(p => p.Nummer).ThenBy(p => p.Position).ToList();


            if (!model.Einlagerungen.IsNullOrEmpty()) {
            
                Lagerorte = model.Einlagerungen.Where(p => p.Restmenge > 0)
                                               .GroupBy(p => p.Lagerort.Nummer)
                                               .Select(p => new LagerortItem { Nummer = p.Key, Restmenge = p.Sum(q => q.Restmenge) })
                                               .OrderBy(p => p.Nummer)
                                               .ToList();
            }

            if (!model.Einlagerungen.IsNullOrEmpty()) {

                var lo = model.Einlagerungen.OrderByDescending(p => p.Datum).Select(p => p.Lagerort).FirstOrDefault();
                if (lo != null) StandardLagerort = lo.Nummer;
            }
        }
    }
}
