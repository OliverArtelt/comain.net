﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.ViewModels.Stock.Reports
{

    public class AUFilterViewModel : ViewModel<IAUFilterView>, IFilterViewModel
    {


        public AUFilterViewModel(IAUFilterView view) : base(view)
        {}


        public void Reset()
        {
            Auftrag = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (!String.IsNullOrWhiteSpace(Auftrag)) return "Auftrag " + Auftrag;
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (!String.IsNullOrWhiteSpace(Auftrag)) filter.Add("Auftrag", Auftrag);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private String auftrag;
        public String Auftrag
        { 
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        } 
     

#endregion

    }
}
