﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Comain.Client.Controllers
{

    public interface ICommandManager
    {

        void Add(IUndoRedoCommand cmd);
        void Clear();
        IEnumerable<IUndoRedoCommand> GetModifications();

        Task AddAndExecuteAsync(IUndoRedoCommand cmd);
        Task ExecuteUndoAsync();
        Task ExecuteRedoAsync();

        bool HasUndo            { get; }
        bool HasRedo            { get; }
        bool HasModifications   { get; }
    }
}
