﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;


namespace Comain.Client.Models.WPlan.Planen
{

    public class EditAction
    {

        public Checklistergebnis Ergebnis           { get; set; }
        public int?              Menge              { get; set; }
        public bool              HeuteErledigt      { get; set; }
    }
}
