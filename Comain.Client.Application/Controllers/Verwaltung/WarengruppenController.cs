﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;

namespace Comain.Client.Controllers.Verwaltung
{

    public interface IWarengruppenController : IVwTypController 
    {}
    
    
    public class WarengruppenController : SimpleController<KeyNameDto, Warengruppe, IWarengruppenView>, IWarengruppenController
    {
        
        public WarengruppenController(IMessageService messages, IControllerBus bus,
                                      ISimpleService<KeyNameDto, Warengruppe> store, VerwaltungViewModel mainView, SimpleViewModel<Warengruppe, IWarengruppenView> view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/WG"; }
        }
    }
}
