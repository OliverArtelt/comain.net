﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;

namespace Comain.Client.Services.Administration
{

    public class FilterService : IFilterService
    {

        private readonly IRepository store;


        public FilterService(IRepository store)
        {
            this.store = store;
        }


        public Task<IList<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default)
        {
            return FilterAsync<LeistungsartFilterModel>("/Filter/LA", ct);
        }


        public Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {
            return FilterAsync<PersonalFilterModel>("/Filter/PS", ct);
        }


        public Task<IList<NummerFilterModel>> ObjektgruppenAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/OGr", ct);
        }


        public Task<IList<AuReopenDto>> AuReopenAsync(CancellationToken ct = default)
        {
            return FilterAsync<AuReopenDto>("/Auftrag/Reopen", ct);
        }


        public Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/GW", ct);
        }


        public Task<IList<DringlichkeitDto>> DringlichkeitenAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<DringlichkeitDto>>("/VW/DR", ct);
        }


        private Task<IList<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<IList<TFilterModel>>(url, ct);
        }
    }
}
