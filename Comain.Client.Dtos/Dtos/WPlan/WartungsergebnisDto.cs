﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Comain.Client.Dtos.WPlan
{

    public class WartungsergebnisDto 
    {

        public DateTime?   DatumLeistung            { get; set; }
        public TimeSpan?   UhrzeitVon               { get; set; }
        public TimeSpan?   UhrzeitBis               { get; set; }
        public DateTime?   Folgetermin              { get; set; }
        public int?        NeuerInstandhalter       { get; set; }
        public int?        Folgeauftrag             { get; set; }
        public bool        MinutenWerdenGeteilt     { get; set; }

        public IList<ChecklistSubmitDto>    Erledigte   { get; set; }
    }
}
