﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Models.Stock;
using Comain.Client.ViewData.Stock.Reports;

namespace Comain.Client.Services.Stock.Artikelstamm
{

    public interface IArtikelService
    {

        Task<IList<ArtikelListDto>>         ArtikelAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<IHObjektFilterModel>>    IHObjekteAsync(CancellationToken ct = default);
        Task<IList<LieferantFilterModel>>   LieferantenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>      WarengruppenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>      EinheitenAsync(CancellationToken ct = default);
        Task<IList<BestandReportRow>>       BestandReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<BestandExportRow>>       BestandExportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<MtLfReportRow>>          MtLfReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<MtLfExportRow>>          MtLfExportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<MtLfRankReportDto>>      MtLfRankReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<PzFilterModel>>          StandorteAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>         KostenstellenAsync(CancellationToken ct = default);
        Task<IList<SpecFilterModel>>        SpecsAsync(CancellationToken ct = default);
    
        Task<Artikel>                       DetailAsync(int id, CancellationToken ct = default);
        Task<Artikel>                       SaveAsync(Artikel model, CancellationToken ct = default);
        Task                                DeleteAsync(Artikel model, CancellationToken ct = default);
        Task<Artikel>                       DeactivateAsync(int id, CancellationToken ct = default(CancellationToken));
        Task<Artikel>                       ReactivateAsync(int id, CancellationToken ct = default(CancellationToken));
    }
}
