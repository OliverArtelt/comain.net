﻿using System;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewData;
using Comain.Client.Views.Services;


namespace Comain.Client.ViewModels.Services
{
    
    public class DeleteViewModel : ViewModel<IDeleteView>
    {

        public DeleteViewModel(IDeleteView view) : base(view)
        {
        }


        public void SetContent(DeleteToken data)
        {

            Header = data.Header;
            Arguments = data.Reasons;
            HasExecuteCommand = data.IsDeleteable;
        }


#region P R O P E R T I E S
 

        private IEnumerable<String> arguments;
        public IEnumerable<String> Arguments
        { 
            get { return arguments; }
            set { SetProperty(ref arguments, value); }
        }
 

        private String header;
        public String Header
        { 
            get { return header; }
            set { SetProperty(ref header, value); }
        }


        private bool hasExecuteCommand;
        public bool HasExecuteCommand
        { 
            get { return hasExecuteCommand; }
            set { SetProperty(ref hasExecuteCommand, value); }
        }


        private ICommand executeCommand;
        public ICommand ExecuteCommand
        { 
            get { return executeCommand; }
            set { SetProperty(ref executeCommand, value); }
        }


#endregion

    }
}
