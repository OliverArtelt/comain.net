﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Stock;

namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class LfPickItem
    {

        private readonly Artikel at;
        private readonly LieferantFilterModel lf;


        public LfPickItem(Artikel mt, LieferantFilterModel lf)
        {
        
            Contract.Assert(mt != null);
            Contract.Assert(lf != null);
            
            this.at = mt;
            this.lf = lf;
        }
        

        public String Lieferant 
        {
            
            get { 
            
                if (String.IsNullOrWhiteSpace(lf.Name2)) return lf.Name1;
                return String.Format("{0} {1}", lf.Name1, lf.Name2);            
            }
        }
        

        public bool IstZugeordnet 
        {
            get { return at.MaterialLieferantMap.Any(p => p.Lieferant.Id == lf.Id); }
            set {

                if (value) {

                    if (at.MaterialLieferantMap.Any(p => p.Lieferant.Id == lf.Id)) return;
                    at.MaterialLieferantMap.Add(new ArtikelLieferant { Lieferant = lf, Materialstamm_Id = at.Id });  

                } else {

                    at.MaterialLieferantMap.Remove(at.MaterialLieferantMap.First(p => p.Lieferant.Id == lf.Id));       
                }
            }
        }
    }
}
