﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewData.Verwaltung
{

    public class PzSelektorItem : NotifyBase
    {

        private readonly Standort pz;
        private readonly Kostenstelle kst;


        public PzSelektorItem(Standort pz, Kostenstelle kst)
        {

            Contract.Assert(pz != null);
            Contract.Assert(kst != null);

            this.pz = pz;
            this.kst = kst;
        }


        public String Kurzname  { get { return pz.Kurzname; } }
        public String Name      { get { return pz.Name; } }
        public int    Nummer    { get { return pz.Nummer; } }


        public bool IstZugeordnet
        {
            get { return kst.ZugeordneteStandorte.Contains(pz); }
            set {

                if (value && !kst.ZugeordneteStandorte.Contains(pz)) kst.ZugeordneteStandorte.Add(pz);
                else if (!value && kst.ZugeordneteStandorte.Contains(pz)) kst.ZugeordneteStandorte.Remove(pz);
                else return;

                RaisePropertyChanged("IstZugeordnet");
            }
        }
    }
}
