﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Reklamationen
{

    public class ReklamService : IReklamService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;
        private readonly IKonfigService config;


        public ReklamService(IRepository store, IMapper mapper, IKonfigService config)
        {
            this.store = store;
            this.config = config;
            this.mapper = mapper;
        }


        public Task<IList<RKListDto>> ListAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<RKListDto>>("/RK/List", ct);
        }


        public Task<IList<RKReportDto>> ReportAsync(Suchkriterien filter, CancellationToken ct = default)
        {
            return store.FindAsync<IList<RKReportDto>>("/RK/Report", filter, ct);
        }


        public Task<IList<RKEingangDto>> EingängeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<RKEingangDto>>("/RK/WE", ct);
        }
       
       
        public Task<Bestellung> BestellungAsync(int id, CancellationToken ct = default)
        {
            return store.GetAsync<Bestellung>("/RK/BS/" + id.ToString(), ct);
        }


        public Task<Konfiguration> KonfigAsync(CancellationToken ct = default)
        {
            return config.GetAsync(ct);
        }


        public async Task<Reklamation> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/RK/{0}", id);
            var dto = await store.GetAsync<ReklamDto>(url, ct).ConfigureAwait(false);
            var model = mapper.Map<Reklamation>(dto);
            model.Positionen.AcceptChanges();
            return model;
        }


        public async Task<Reklamation> UpdateAsync(Reklamation model, CancellationToken ct = default)
        {
            
            var dto = mapper.Map<Reklamation, ReklamDto>(model);
            dto.Positionen = mapper.AsDtoList<Reklamposition, ReklamposDto>(model.Positionen);
            var result = await store.PostAsync<ReklamDto, ReklamDto>("/RK", dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<Reklamation>(result);
            newmodel.Positionen.AcceptChanges();
            return newmodel;  
        }


        public Task DeleteAsync(int id, CancellationToken ct = default)
        {
            return store.DeleteAsync(String.Format(@"/RK/{0}", id), ct);
        }
    }
}
