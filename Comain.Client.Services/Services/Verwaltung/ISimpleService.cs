﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Services.Verwaltung
{

    public interface ISimpleService<TDto, TModel> where TModel : NotifyBase, new() 
                                                  where TDto : ChangeTrackingBase
    {

        bool HasChanges { get; }
        void Reset();

        Task<TrackableCollection<TModel>>   GetAsync(String url, CancellationToken ct = default);
        Task                                SaveAsync(String url, CancellationToken ct = default);
    }
}
