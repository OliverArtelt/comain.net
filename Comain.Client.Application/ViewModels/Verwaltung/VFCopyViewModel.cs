﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{
    
    public class VFCopyViewModel : ViewModel<IVFCopyView>
    {


        public VFCopyViewModel(IVFCopyView view) : base(view)
        {
        }
 

        public void Unload()
        {
            Monate = null;            
        }


#region P R O P E R T I E S


        private ICommand takeCommand;
        public ICommand TakeCommand
        { 
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


        private IEnumerable<DateTime> monate;
        public IEnumerable<DateTime> Monate
        { 
            get { return monate; }
            set { if (SetProperty(ref monate, value) && monate != null) Monat = monate.FirstOrDefault(); }
        }


        private DateTime monat;
        public DateTime Monat
        { 
            get { return monat; }
            set { SetProperty(ref monat, value); }
        }


        private bool ersetzen = true;
        public bool Ersetzen
        { 
            get { return ersetzen; }
            set { if (SetProperty(ref ersetzen, value) && value) Anfügen = false; }
        }


        private bool anfügen;
        public bool Anfügen
        { 
            get { return anfügen; }
            set { if (SetProperty(ref anfügen, value) && value) Ersetzen = false; }
        }

 
#endregion
 
    }
}
