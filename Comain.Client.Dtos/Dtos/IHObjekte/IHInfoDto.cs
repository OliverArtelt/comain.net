﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.IHObjekte
{

    [DebuggerDisplay("{Nummer}")]
    public class IHInfoDto
    {

        public int          Id                      { get; set; }
        public String       Nummer                  { get; set; }
        public String       Name                    { get; set; }
    }
}
