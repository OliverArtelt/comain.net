﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragFilterViewModel : ViewModel<IAuftragFilterView>
    {

        private readonly SynchronizationContext context;
        private readonly IHObjekte.IHSelektorViewModel ihFilter;


        public AuftragFilterViewModel(IHObjekte.IHSelektorViewModel ihFilter, IAuftragFilterView view, IPersonalSearchProvider searcher) : base(view)
        {

            this.ihFilter = ihFilter;
            PsSearchProvider = searcher;
            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(ResetSearch);
            ResetSearch();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();

            filter.Add("AuOhneLE",                      AuOhneLE);
            filter.Add("AuOhneMT",                      AuOhneMT);
            filter.Add("AuTyp0",                        AuTyp0);
            filter.Add("AuTyp7",                        AuTyp7);
            filter.Add("AuTyp8",                        AuTyp8);
            filter.Add("AuTyp9",                        AuTyp9);
            filter.Add("UseD1",                         UseD1);
            filter.Add("UseD2",                         UseD2);
            filter.Add("UseD3",                         UseD3);
            filter.Add("UseD4",                         UseD4);
            filter.Add("UseD5",                         UseD5);
            filter.Add("UseD6",                         UseD6);
            filter.Add("Auftrag",                       Auftrag);
            filter.Add("DatumVon",                      DatumVon);
            filter.Add("DatumBis",                      DatumBis);
            filter.Add("TerminVon",                     TerminVon);
            filter.Add("TerminBis",                     TerminBis);
            filter.Add("ExtAucode",                     ExtAucode);
            filter.Add("IntAuStelle",                   IntAuStelle);
            filter.Add("Massnahme",                     Massnahme);

            filter.Add("StatusOffen",                   StatusOffen);
            filter.Add("StatusÜberfällig",              StatusÜberfällig);
            filter.Add("StatusÜbergeben",               StatusÜbergeben);
            filter.Add("StatusAbgeschlossen",           StatusAbgeschlossen);
            filter.Add("StatusStorniert",               StatusStorniert);

            if (Gewerk != null)                         filter.Add("Gewerk", Gewerk.Id);
            if (Augeber != null)                        filter.Add("Augeber", Augeber.Id);
            if (Aunehmer != null)                       filter.Add("Aunehmer", Aunehmer.Id);
            if (Standort != null)                       filter.Add("Standort", Standort.Id);
            if (Kst != null)                            filter.Add("Kst", Kst.Id);
            if (!String.IsNullOrEmpty(InventarNummer))  filter.Add("InventarNummer", InventarNummer);
            if (ihFilter.CurrentIHObjekt != null)       filter.Add("Iho", ihFilter.CurrentIHObjekt.Nummer);

            return filter;
        }


        public void ResetSearch()
        {

            AuOhneLE            = false;
            AuOhneMT            = false;

            AuTyp0              = true;
            AuTyp7              = true;
            AuTyp8              = true;
            AuTyp9              = true;

            UseD1               = false;
            UseD2               = false;
            UseD3               = false;
            UseD4               = false;
            UseD5               = false;
            UseD6               = false;

            StatusOffen         = true;
            StatusÜberfällig    = true;
            StatusÜbergeben     = true;
            StatusAbgeschlossen = false;
            StatusStorniert     = false;

            Auftrag             = null;
            Gewerk              = null;
            Augeber             = null;
            Aunehmer            = null;
            Massnahme           = null;

            DatumVon            = null;
            DatumBis            = null;
            TerminVon           = DateTime.Today.AddDays(-14);
            TerminBis           = DateTime.Today.AddDays(14);
            ExtAucode           = null;
            IntAuStelle         = null;

            InventarNummer      = null;
            Standort            = null;
            Kst                 = null;

            ihFilter.Reset();
        }


        public async Task LoadAsync(List<IHFilterItem> ihlist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                    IList<PersonalFilterModel> pslist, IList<NummerFilterModel> gwlist,
                                    IList<int> interneAuStellen, CancellationToken ct = default)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Gewerkliste = gwlist;
                InterneAuStellen = interneAuStellen;
                Standortliste = pzlist;
                Kstliste = kstlist.OrderBy(p => p.Nummer, new Comain.NaturalComparer()).ToList();
                PsSearchProvider.SetItems(pslist);
                Personalliste = pslist;

            }), null);

            await ihFilter.LoadAsync(ihlist, pzlist, kstlist, ct);
        }


        public Task UnloadAsync()
        {

            Gewerkliste          = null;
            Standortliste        = null;
            Kstliste             = null;
            InterneAuStellen     = null;
            Personalliste        = null;

            ihFilter.Unload();
            PsSearchProvider.ClearItems();

            return Task.FromResult(true);
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool hasTyp0;
        public bool HasTyp0
        {
            get { return hasTyp0; }
            set { SetProperty(ref hasTyp0, value); }
        }


        private bool hasTyp7;
        public bool HasTyp7
        {
            get { return hasTyp7; }
            set { SetProperty(ref hasTyp7, value); }
        }


        private bool hasTyp8;
        public bool HasTyp8
        {
            get { return hasTyp8; }
            set { SetProperty(ref hasTyp8, value); }
        }


        private bool hasTyp9;
        public bool HasTyp9
        {
            get { return hasTyp9; }
            set { SetProperty(ref hasTyp9, value); }
        }


        private bool auTyp0;
        public bool AuTyp0
        {
            get { return auTyp0; }
            set { SetProperty(ref auTyp0, value); }
        }


        private bool auTyp7;
        public bool AuTyp7
        {
            get { return auTyp7; }
            set { SetProperty(ref auTyp7, value); }
        }


        private bool auTyp8;
        public bool AuTyp8
        {
            get { return auTyp8; }
            set { SetProperty(ref auTyp8, value); }
        }


        private bool auTyp9;
        public bool AuTyp9
        {
            get { return auTyp9; }
            set { SetProperty(ref auTyp9, value); }
        }


        private bool statusOffen;
        public bool StatusOffen
        {
            get { return statusOffen; }
            set { SetProperty(ref statusOffen, value); }
        }


        private bool statusPeriodisch;
        public bool StatusPeriodisch
        {
            get { return statusPeriodisch; }
            set { SetProperty(ref statusPeriodisch, value); }
        }


        private bool statusÜberfällig;
        public bool StatusÜberfällig
        {
            get { return statusÜberfällig; }
            set { SetProperty(ref statusÜberfällig, value); }
        }


        private bool statusÜbergeben;
        public bool StatusÜbergeben
        {
            get { return statusÜbergeben; }
            set { SetProperty(ref statusÜbergeben, value); }
        }


        private bool statusAbgeschlossen;
        public bool StatusAbgeschlossen
        {
            get { return statusAbgeschlossen; }
            set { SetProperty(ref statusAbgeschlossen, value); }
        }


        private bool statusStorniert;
        public bool StatusStorniert
        {
            get { return statusStorniert; }
            set { SetProperty(ref statusStorniert, value); }
        }


        private bool istKunde;
        public bool IstKunde
        {
            get { return istKunde; }
            set { SetProperty(ref istKunde, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        public ICommand ResetCommand { get; private set; }


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private String massnahme;
        public String Massnahme
        {
            get { return massnahme; }
            set { SetProperty(ref massnahme, value); }
        }


        private bool auOhneLE;
        public bool AuOhneLE
        {
            get { return auOhneLE; }
            set { SetProperty(ref auOhneLE, value); }
        }


        private bool auOhneMT;
        public bool AuOhneMT
        {
            get { return auOhneMT; }
            set { SetProperty(ref auOhneMT, value); }
        }


        private bool useD1;
        public bool UseD1
        {
            get { return useD1; }
            set { SetProperty(ref useD1, value); }
        }


        private bool useD2;
        public bool UseD2
        {
            get { return useD2; }
            set { SetProperty(ref useD2, value); }
        }


        private bool useD3;
        public bool UseD3
        {
            get { return useD3; }
            set { SetProperty(ref useD3, value); }
        }


        private bool useD4;
        public bool UseD4
        {
            get { return useD4; }
            set { SetProperty(ref useD4, value); }
        }


        private bool useD5;
        public bool UseD5
        {
            get { return useD5; }
            set { SetProperty(ref useD5, value); }
        }


        private bool useD6;
        public bool UseD6
        {
            get { return useD6; }
            set { SetProperty(ref useD6, value); }
        }


        private String auftrag;
        public String Auftrag
        {
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }


        private IList<NummerFilterModel> gewerkliste;
        public IList<NummerFilterModel> Gewerkliste
        {
            get { return gewerkliste; }
            set { SetProperty(ref gewerkliste, value); }
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        private IList<PersonalFilterModel> personalliste;
        public IList<PersonalFilterModel> Personalliste
        {
            get { return personalliste; }
            set { SetProperty(ref personalliste, value); }
        }


        private PersonalFilterModel augeber;
        public PersonalFilterModel Augeber
        {
            get { return augeber; }
            set { SetProperty(ref augeber, value); }
        }


        private PersonalFilterModel aunehmer;
        public PersonalFilterModel Aunehmer
        {
            get { return aunehmer; }
            set { SetProperty(ref aunehmer, value); }
        }


        private DateTime? datumVon;
        public DateTime? DatumVon
        {
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }


        private DateTime? datumBis;
        public DateTime? DatumBis
        {
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }


        private DateTime? terminVon;
        public DateTime? TerminVon
        {
            get { return terminVon; }
            set { SetProperty(ref terminVon, value); }
        }


        private DateTime? terminBis;
        public DateTime? TerminBis
        {
            get { return terminBis; }
            set { SetProperty(ref terminBis, value); }
        }


        private String extAucode;
        public String ExtAucode
        {
            get { return extAucode; }
            set { SetProperty(ref extAucode, value); }
        }


        private String inventarNummer;
        public String InventarNummer
        {
            get { return inventarNummer; }
            set { SetProperty(ref inventarNummer, value); }
        }


        private IList<int> interneAuStellen;
        public IList<int> InterneAuStellen
        {
            get { return interneAuStellen; }
            set { SetProperty(ref interneAuStellen, value); }
        }


        private int? intAuStelle;
        public int? IntAuStelle
        {
            get { return intAuStelle; }
            set { SetProperty(ref intAuStelle, value); }
        }


        private IList<PzFilterModel> standortliste;
        public IList<PzFilterModel> Standortliste
        {
            get { return standortliste; }
            set { SetProperty(ref standortliste, value); }
        }


        private PzFilterModel standort;
        public PzFilterModel Standort
        {
            get { return standort; }
            set { SetProperty(ref standort, value); }
        }


        private IList<KstFilterModel> kstliste;
        public IList<KstFilterModel> Kstliste
        {
            get { return kstliste; }
            set { SetProperty(ref kstliste, value); }
        }


        private KstFilterModel kst;
        public KstFilterModel Kst
        {
            get { return kst; }
            set { SetProperty(ref kst, value); }
        }


        public object IHSelektor { get { return ihFilter.View; } }


#endregion

    }
}
