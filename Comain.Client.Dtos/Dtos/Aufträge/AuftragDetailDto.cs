﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.Aufträge
{

    public class AuftragDetailDto : ChangeTrackingBase
    {

        public DateTime                                 ErstelltAm              { get; set; }
        public DateTime?                                GeändertAm              { get; set; }
        public String                                   Nummer                  { get; set; }
        public String                                   ExterneNummer           { get; set; }
        public String                                   FBNummer                { get; set; }
        public bool?                                    ImFremdbudget           { get; set; }
        public bool                                     UnveränderlicherTermin  { get; set; }
        public int?                                     InterneAuStelle         { get; set; }
        public int                                      Projektnummer           { get; set; }
        public String                                   Titel                   { get; set; }
        public String                                   Kurzbeschreibung        { get; set; }
        public String                                   Aktivitätenbeschreibung { get; set; }
        public int                                      Typ                     { get; set; }
        public int                                      Status                  { get; set; }
        public int?                                     Dringlichkeit_Id        { get; set; }
        public DateTime?                                Fertigstellungstermin   { get; set; }
        public DateTime?                                GemeldetAm              { get; set; }
        public DateTime?                                ÜbergabeAm              { get; set; }
        public int?                                     Störzeit                { get; set; }
        public DateTime?                                Beauftragungsdatum      { get; set; }
        public DateTime?                                Auftragsleiterdatum     { get; set; }
        public DateTime?                                Abnahmedatum            { get; set; }

        public short?                                   Menge                   { get; set; }
        public decimal?                                 Wert                    { get; set; }
        public decimal?                                 Stunden                 { get; set; }

        public int?                                     IHObjektKst_Id          { get; set; }
        public int                                      Leistungsart_Id         { get; set; }
        public NummerFilterModel                        Schadensbild            { get; set; }
        public NummerFilterModel                        Schadensursache         { get; set; }
        public SubNummerFilterModel                     SubSchadensbild         { get; set; }
        public SubNummerFilterModel                     SubSchadensursache      { get; set; }
        public NummerFilterModel                        Gewerk                  { get; set; }
        public PersonalFilterModel                      Auftraggeber            { get; set; }
        public PersonalFilterModel                      Auftragsleiter          { get; set; }
        public PersonalFilterModel                      Abnahmeberechtigter     { get; set; }
        public PersonalFilterModel                      Auftragnehmer           { get; set; }
        public int?                                     Parent_Id               { get; set; }

        public AuIHKSDto                                AuIHKS                  { get; set; }
        public LeistungsartFilterModel                  Leistungsart            { get; set; }

        public ICollection<PersonalleistungDto>         Personalleistungen      { get; set; }
        public ICollection<MaterialleistungDto>         Materialleistungen      { get; set; }
    }
}
