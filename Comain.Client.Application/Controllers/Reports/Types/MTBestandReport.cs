﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Artikelstamm;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class MTBestandReport : ReportBase, IReport
    {

        private readonly DisplayFilterViewModel displayView;
        private readonly IArtikelService store;
        

        public MTBestandReport(IArtikelService store, DisplayFilterViewModel displayView, 
                               ArtikelFilterViewModel artikelView, MTBestandFilterViewModel bestandView, IHTreeViewModel treeView)
        {
            
            this.displayView = displayView;
            this.store = store;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, treeView, bestandView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }


        public override string Title
        {
            get { return "Artikelbestand"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 1000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.BestandsExport.rdlc": "Comain.Client.Reports.Stock.BestandsReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            if (displayView.DisplayExport) result.AddDataSource("MainData", await store.BestandExportAsync(GebeFilter(), ct));
            else                           result.AddDataSource("MainData", await store.BestandReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
