﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class BSFilterViewModel : ViewModel<IBSFilterView>, IFilterViewModel
    {

        public BSFilterViewModel(IBSFilterView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
           
            StatusAngelegt = true;
            StatusOffen = true;
            StatusAbgeschlossen = true;
            StatusStorniert = true;
            Bestellung = null;
            TerminVon = null;
            TerminBis = null;
        }


        public String Validiere()
        {

            if (TerminVon.HasValue && TerminBis.HasValue && TerminVon.Value > TerminBis.Value) return "Der Starttermin ist größer als sein Ende.";
            return null;
        }


        public string FilterAlsString()
        {
            
            var bld = new StringBuilder();

            if (StatusAngelegt || StatusOffen || StatusAbgeschlossen || StatusStorniert) {

                bld.Append("Status ");
                if (StatusAngelegt)      bld.Append("angelegt,");
                if (StatusOffen)         bld.Append("offen,");
                if (StatusAbgeschlossen) bld.Append("abgeschlossen,");
                if (StatusStorniert)     bld.Append("storniert,");
                bld.Length -= 1;
                bld.Append("; ");
            }

            if (!String.IsNullOrWhiteSpace(Bestellung)) bld.Append("Bestellung ").Append(Bestellung).Append("; ");
            if (TerminVon.HasValue || TerminBis.HasValue) {

                bld.Append("Eingang erwartet");
                if (TerminVon.HasValue) bld.Append(" von ").Append(TerminVon.Value.ToShortDateString());
                if (TerminBis.HasValue) bld.Append(" bis ").Append(TerminBis.Value.ToShortDateString());
                bld.Append("; ");
            }

            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString();
        }


        public void AddFilter(Suchkriterien filter)
        {
            
            if (StatusAngelegt)      filter.Add("StatusAngelegt", true);    
            if (StatusOffen)         filter.Add("StatusOffen", true);     
            if (StatusAbgeschlossen) filter.Add("StatusAbgeschlossen", true); 
            if (StatusStorniert)     filter.Add("StatusStorniert", true); 
            if (!String.IsNullOrWhiteSpace(Bestellung))
                                     filter.Add("Bestellung", Bestellung); 
            if (TerminVon.HasValue)  filter.Add("TerminVon",  TerminVon);
            if (TerminBis.HasValue)  filter.Add("TerminBis",  TerminBis);          
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
    

        private bool statusAngelegt;
        public bool StatusAngelegt
        { 
            get { return statusAngelegt; }
            set { SetProperty(ref statusAngelegt, value); }
        } 
    

        private bool statusOffen;
        public bool StatusOffen
        { 
            get { return statusOffen; }
            set { SetProperty(ref statusOffen, value); }
        } 
    

        private bool statusAbgeschlossen;
        public bool StatusAbgeschlossen
        { 
            get { return statusAbgeschlossen; }
            set { SetProperty(ref statusAbgeschlossen, value); }
        } 
    

        private bool statusStorniert;
        public bool StatusStorniert
        { 
            get { return statusStorniert; }
            set { SetProperty(ref statusStorniert, value); }
        } 
   

        private String bestellung;
        public String Bestellung
        { 
            get { return bestellung; }
            set { SetProperty(ref bestellung, value); }
        } 
   

        private DateTime? terminVon;
        public DateTime? TerminVon
        { 
            get { return terminVon; }
            set { SetProperty(ref terminVon, value); }
        } 
    

        private DateTime? terminBis;
        public DateTime? TerminBis
        { 
            get { return terminBis; }
            set { SetProperty(ref terminBis, value); }
        } 
    

#endregion

    }
}
