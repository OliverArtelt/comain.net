﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;

namespace Comain.Client.Controllers.Administration
{

    public interface IBevorConfigController : IController
    {}


    public class BevorConfigController : ConfigController<BevorAdminViewModel>, IBevorConfigController
    {

        private readonly IFilterService filterStore;
        private readonly BevorAdminViewModel configView;


        public BevorConfigController(IKonfigService store, IMessageService messages, IControllerBus bus, IFilterService filterStore,
                                     AdminViewModel view, BevorAdminViewModel configView)

            : base(store, messages, bus, view, configView)

        {

            this.filterStore = filterStore;
            this.configView = configView;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                var cftask = store.GetAsync(bus.Token);
                var pstask = filterStore.PersonalAsync(bus.Token);
                await Task.WhenAll(cftask, pstask).ConfigureAwait(false);

                current = cftask.Result;
                configView.SetzeListen(pstask.Result);
                configView.Current = current;
            }
        }
    }
}
