﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class DisplayFilterViewModel : ViewModel<IDisplayFilterView>, IFilterViewModel
    {

        public DisplayFilterViewModel(IDisplayFilterView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            DisplayPrint = true;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            return null;            
        }


        public void AddFilter(Suchkriterien filter)
        {
            
            if (DisplayExport) filter.Add("Display", "Export");
            else filter.Add("Display", "Report");
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private bool displayExport;
        public bool DisplayExport
        { 
            get { return displayExport; }
            set { 
            
                if (SetProperty(ref displayExport, value)) {
                
                    if (value) DisplayPrint = false;   
                }
            }
        } 
   

        private bool displayPrint;
        public bool DisplayPrint
        { 
            get { return displayPrint; }
            set { 
            
                if (SetProperty(ref displayPrint, value)) {
                
                    if (value) DisplayExport = false;   
                }
            }
        } 
     

#endregion

    }
}
