﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX;
using CO.OpenXML.XLSX.ExcelStyles;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class WartungsplanWriter : IExportVisitor
    {

        private WorksheetWriter writer;
        private IStyleFactory styles;
        private Plan plan;
        private PlanOptions options;
        private int currentRow;
        private const int weekstartColumn = 4;


        public String Exportname => "Wartungsplan";


        public void SetStyles(IStyleFactory s)
            => styles = s;


        public void SetOptions(PlanOptions opts)
            => options = opts;


        public void SetWriter(WorksheetWriter w)
            => writer = w;


        public void Visit(Plan model)
        {

            plan = model;
            currentRow = 0;

            writer.SetValue(plan.Name, "A1", CellStyles.Head);
            writer.SetValue($"Erstellt am {DateTime.Today.ToShortDateString()}", "A2", CellStyles.Parameter);
            writer.SetValue($"Hinweis: rötlich eingefärbte Zellen enthalten unvollständige Zeitangaben", "A3", CellStyles.HintText);


            writer.SetRowHeight(1, 40f);
            writer.SetColumnWidth(new List<ColumnStyle> {

                                    new ColumnStyle(1, 22f),    //Auftragsnummern
                                    new ColumnStyle(2, 3, 16f),  //GW, LA
                                    new ColumnStyle(4, 4 + plan.Spalten.Count, 10f)   //KW
                                });

            currentRow = 5;
        }


        public void Visit(PZPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            currentRow++;
            writer.SetValue("Standort:", currentRow, 1, CellStyles.Standort);
            writer.SetValue(model.Bezeichnung, currentRow, 2, CellStyles.Standort);
            Enumerable.Range(3, 1 + plan.Spalten.Count).ForEach(colndx => writer.SetEmpty(currentRow, colndx, CellStyles.Standort));

            writer.SetRowHeight(currentRow, 30f);
            currentRow++;
        }


        public void Visit(IHPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            bool nodeIstMaschine = IHObjekt.IstMaschinennummer(model.Nummer);

            String caption = nodeIstMaschine ?  "Anlage / Maschine": "Baugruppe:";
            Style style = nodeIstMaschine ?  CellStyles.IHObjekt: CellStyles.Baugruppe;

            writer.SetValue(caption, currentRow, 1, style);
            writer.SetValue(model.Name, currentRow, 2, style);
            Enumerable.Range(3, 1 + plan.Spalten.Count).ForEach(colndx => writer.SetEmpty(currentRow, colndx, style));
            writer.SetRowHeight(currentRow, 20f);
            currentRow++;

            writer.SetValue("Assetnummer:", currentRow, 1, style);
            writer.SetValue(model.Nummer, currentRow, 2, style);
            Enumerable.Range(3, 1 + plan.Spalten.Count).ForEach(colndx => writer.SetEmpty(currentRow, colndx, style));
            writer.SetRowHeight(currentRow, 20f);
            currentRow++;


            var currentCol = weekstartColumn;
            plan.Spalten.ForEach(p => {

                writer.SetValue(p.Periode, currentRow, currentCol, p.IstAktuell? CellStyles.KopfWocheToday : CellStyles.KopfWoche);
                writer.SetValue(p.Begin, currentRow + 1, currentCol++, p.IstAktuell? CellStyles.KopfDatumToday : CellStyles.KopfDatum);
            });

            writer.SetValue(String.Empty, currentRow, 1, CellStyles.KopfLeer);
            writer.SetValue(String.Empty, currentRow, 2, CellStyles.KopfLeer);
            writer.SetValue(String.Empty, currentRow, 3, CellStyles.KopfLeer);
            currentRow++;

            writer.SetValue("Auftrag", currentRow, 1, CellStyles.KopfLeer);
            writer.SetValue("GW", currentRow, 2, CellStyles.KopfLeerMitte);
            writer.SetValue("LA", currentRow, 3, CellStyles.KopfLeerMitte);
            currentRow++;

            model.Aufträge.ForEach(aumodel => {

                writer.SetValue(aumodel.Auftrag, currentRow, 1, CellStyles.Text);
                writer.SetValue(aumodel.Gewerk, currentRow, 2, CellStyles.TextCentered);
                writer.SetValue(aumodel.Leistungsart, currentRow, 3, CellStyles.TextCentered);

                aumodel.Positionen.GroupBy(p => plan.DatumToSpaltenIndex(p.DisplayDate))
                                  .Where(p => p.Key != null)
                                  .ForEach(p => writer.SetValue($"{p.Sum(q => q.MinutenAngezeigt.GetValueOrDefault() / 60m).RoundTwoDecimals(Rundungstyp.Kaufmännisch)} Std",
                                                                currentRow, weekstartColumn + p.Key.Value,
                                                                p.Any(q => !q.MinutenAngezeigt.HasValue)? CellStyles.MinutesCenteredRed: CellStyles.TextCentered));

                currentRow++;
            });


            writer.SetValue("Gesamt",     currentRow, 1, CellStyles.FootValue);
            writer.SetValue(String.Empty, currentRow, 2, CellStyles.FootValue);
            writer.SetValue(String.Empty, currentRow, 3, CellStyles.FootValue);

            model.Summen.ForEach((p, ndx) => writer.SetValue(p.Stunden.HasValue? $"{p.Stunden.Value.RoundTwoDecimals(Rundungstyp.Kaufmännisch)} Std": String.Empty, 
                                                             currentRow, weekstartColumn + ndx, CellStyles.FootValue));
            currentRow++;
        }


        public void Visit(MNPlanzeile model)
        {}


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void Visit(Planspaltensumme model)
        {}


        public void Visit(PZSpaltensumme model)
        {}


        public void Visit(IHSpaltensumme model)
        {}


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
