﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IDringlichkeitController : IVwTypController 
    {}
    
    
    public class DringlichkeitController : ControllerBase, IDringlichkeitController
    {

        private readonly VerwaltungViewModel mainView;
        private readonly DringlichkeitViewModel view;
        private readonly IDringlichkeitService store;


        public DringlichkeitController(IMessageService messages, IDringlichkeitService store, IControllerBus bus,
                                       VerwaltungViewModel mainView, DringlichkeitViewModel view) 
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
            this.mainView = mainView;
        }


        public void Initialize()
        {

            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.CancelCommand = CreateCommand(_ => LoadAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            mainView.DetailView = view.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
        }


        protected override async Task<bool> UnloadImplAsync()
        {
            
            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;
            
            store.Reset();
            view.Unload();
            return true;
        }


        private async Task SaveAsync()
        {
            
            using (new VisualDelay(view)) {
            
                await store.SaveAsync(bus.Token);
                await LoadAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var drtask = store.DringlichkeitenAsync(bus.Token);
                var latask = store.LeistungsartenAsync(bus.Token);

                await Task.WhenAll(drtask, latask).ConfigureAwait(false);

                await OnUIThreadAsync(() => { 
                
                    view.Leistungsarten = latask.Result;
                    view.SetList(drtask.Result);
                     
                });
            }
        }
    }   
}
