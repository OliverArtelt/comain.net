﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;


namespace Comain
{

    public class TrackableCollection<T> : ObservableCollection<T>, IChangeTracking where T : NotifyBase
    {

        private HashSet<T> addedItems = new HashSet<T>();
        private HashSet<T> deletedItems = new HashSet<T>();


        public TrackableCollection() : base()
        {}


        public TrackableCollection(IEnumerable<T> collection) : base(collection)
        {

            Items.ForEach(p => p.AcceptChanges());
            AcceptChanges();
        }


        public TrackableCollection(List<T> list) : base(list)
        {

            Items.ForEach(p => p.AcceptChanges());
            AcceptChanges();
        }


        private void MarkAsAdded(T item)
        {

            if (deletedItems.Contains(item)) deletedItems.Remove(item);
            addedItems.Add(item);
        }


        private void MarkAsDeleted(T item)
        {

            if (addedItems.Contains(item)) addedItems.Remove(item);
            else deletedItems.Add(item);
        }


        public virtual bool IsChanged => addedItems.Any() || deletedItems.Any() || Items.Any(p => p.IsChanged);


        public virtual void AcceptChanges()
        {

            addedItems.Clear();
            deletedItems.Clear();
            Items.ForEach(p => p.AcceptChanges());
        }


        public virtual IEnumerable<T> DeletedItems()
            => deletedItems.ToList();


        public virtual IEnumerable<T> AddedItems()
            => addedItems.ToList();


        public virtual IEnumerable<T> ChangedItems()
            => Items.Where(p => p.IsChanged && !addedItems.Contains(p) && !deletedItems.Contains(p)).ToList();


        public virtual new void Add(T item)
        {

            if (base.Contains(item)) return;
            base.Add(item);
            MarkAsAdded(item);
        }


        public virtual void AddRange(IEnumerable<T> items)
            => items.ForEach(p => Add(p));


        public virtual new void Clear()
        {

            Items.ForEach(p => {

                if (addedItems.Contains(p)) addedItems.Remove(p);
                else deletedItems.Add(p);
            });

            base.Clear();
        }


        public virtual new void ClearItems()
        {

            Items.ForEach(p => {

                if (addedItems.Contains(p)) addedItems.Remove(p);
                else deletedItems.Add(p);
            });

            base.ClearItems();
        }


        public virtual new void Insert(int index, T item)
        {

            if (base.Contains(item)) return;
            base.Insert(index,item);
            MarkAsAdded(item);
        }


        protected override void InsertItem(int index, T item)
        {

            if (base.Contains(item)) return;
            base.InsertItem(index,item);
            MarkAsAdded(item);
        }


        public virtual new void Remove(T item)
        {

            base.Remove(item);
            MarkAsDeleted(item);
        }


        protected virtual new void RemoveAt(int index)
        {

            var item = Items[index];
            base.RemoveItem(index);
            MarkAsDeleted(item);
        }


        protected override void RemoveItem(int index)
        {

            var item = Items[index];
            base.RemoveItem(index);
            MarkAsDeleted(item);
        }


        protected override void SetItem(int index, T item)
            => throw new NotSupportedException();
    }
}
