﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{
    
    public class Reklamation : Entity, IDataErrorInfo
    {
       
        private String          notizen;
        private DateTime?       datum;
        private Bestellung      bestellung;
        private Lieferschein    lieferschein;
        
        private TrackableCollection<Reklamposition> positionen;


        public override bool IsChanged => base.IsChanged || positionen != null && positionen.Any(p => p.IsChanged);


#region P R O P E R T I E S


        public String Notizen    
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }
        

        public DateTime? Datum  
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }


        public  Bestellung Bestellung  
        {
            get { return bestellung; }
            set { SetProperty(ref bestellung, value); }
        }


        public  Lieferschein Lieferschein 
        {
            get { return lieferschein; }
            set { SetProperty(ref lieferschein, value); }
        }

        
        public TrackableCollection<Reklamposition> Positionen 
        {
            get { 
            
                if (positionen == null) positionen = new TrackableCollection<Reklamposition>();
                return positionen; 
            }
            set { SetProperty(ref positionen, value); }
        }
        

#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 

                var test = this["Notizen"];
                return test;
            }
        }
 
       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Notizen"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                result.AddRange(Positionen.SelectMany(p => p.ErrorList).Distinct());

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Notizen":

                    if (String.IsNullOrWhiteSpace(Notizen)) return "Die Reklamation muss begründet werden."; 
                    break;
                }

                return null;
            }
        }


#endregion    
        
    }
}
