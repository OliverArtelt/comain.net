﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Users
{

    public class Gewerk : Entity, IDataErrorInfo, IComparable<Gewerk>
    {

        private String name;
        private String nummer;


#region P R O P E R T I E S


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


#endregion


        public int CompareTo(Gewerk other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 48) return "Die Bezeichnung darf aus maximal 48 Zeichen bestehen.";
                    break;

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 8) return "Die Nummer darf aus maximal 8 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
