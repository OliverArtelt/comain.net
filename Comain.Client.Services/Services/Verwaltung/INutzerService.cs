﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Services.Verwaltung
{

    public interface INutzerService
    {
       
        Task<IList<Nutzer>> NutzerAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default);
        Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default);

        Task UpdateAsync(Nutzer model, CancellationToken ct = default);
        Task DeleteAsync(Nutzer model, CancellationToken ct = default);
    }
}
