﻿using System;
using System.Text;
using System.Collections.Generic;


namespace CO.OpenXML.XLSX
{
  
    /// <summary>
    /// Zellnamen konvertieren "B11" vs. 11, 2, hashkey-fähig
    /// </summary>
    public class CellAddress : IEquatable<CellAddress>, IComparable<CellAddress>
    {

        //als Hashkey dienende Elemente dürfen im Container nicht mehr veränderbar sein.
        private readonly int _row;
        private readonly int _column;
        private readonly String _address;
        
        
        /// <summary>
        /// Zeile der Adresse (1 .. 1048576)
        /// </summary>
        public int Row { get { return _row; } }
        /// <summary>
        /// Spalte der Adresse (1 .. 16384)
        /// </summary>
        public int Column { get { return _column; } }
        /// <summary>
        /// Adresse als String (z.B. A13, fec234)
        /// </summary>
        public String Address { get { return _address; } }
        
        
        /// <summary>
        /// Zellenadresse aus Zellenindizies ermitteln
        /// </summary>
        /// <remarks>
        /// http://dotnet-snippets.de/dns/c-integer-nach-excel-spalten-bezeichnung-SID323.aspx
        /// </remarks>
        /// <param name="row">Zeilennummer ab 1</param>
        /// <param name="col">Spaltennummer ab 1</param>
        public CellAddress(int row, int col)
        {
        
            if (row < 1 || row > 1048576) throw new IndexOutOfRangeException(String.Format("Ungültige Excel-Zeilennummer {0}", row));
            if (col < 1 || col > 16384)   throw new IndexOutOfRangeException(String.Format("Ungültige Excel-Spaltennummer {0}", col));
        
            _row = row;
            _column = col;
            
            StringBuilder bld = new StringBuilder();            
            int rest = 0;
    		
            if (col > 26) {
			    
                do {

                    col = Math.DivRem(col, 26, out rest);
				    
                    if (rest == 0) {
					    
                        col -= 1;
                        rest = 26;
                    }
         
                    bld.Append((char)(rest + 64));
			    
                } while (col > 26);
            }
		    
            bld.Insert(0, (char)(col + 64));
            bld.Append(row);
            
            _address = bld.ToString();
        }

        /// <summary>
        /// Zellenadresse aus String ermitteln (z.B. B5)
        /// </summary>
        /// <param name="address">Zellenadresse</param>
        public CellAddress(String address)
        {
        
            String s = address.Trim().ToUpper();
            StringBuilder bld = new StringBuilder();
            
            _row = 0;
            _column = 0;
            int pow = 1;
            int ndx = 0;
            
            foreach (char c in s) {
            
                if (!Char.IsUpper(c)) break;
                _column += (c - 'A' + 1) * pow;
                pow *= 26;
                ndx++;
                bld.Append(c);
            }
            
            if (_column == 0 || ndx > 3 || _column > 16384) throw new IndexOutOfRangeException(String.Format("Ungültige Excel-Spaltennummer {0}", _column));
            
            s = s.Substring(ndx);
            
            int r;
            if (!Int32.TryParse(s, out r)) throw new IndexOutOfRangeException(String.Format("Ungültige Excel-Zeilennummer {0}", s));
            if (r < 1 || r > 1048576) throw new IndexOutOfRangeException(String.Format("Ungültige Excel-Zeilennummer {0}", r));
            
            _row = r;
            bld.Append(_row);
            
            _address = bld.ToString();
        }


        public override string ToString()
        {
            return _address;
        }
        
        /// <summary>
        /// Um als Key in Hashcontainern dienen zu können
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return _address.GetHashCode();
        }
        
        /// <summary>
        /// Um prüfen zu können ob Key in Hashcontainern bereits vorhanden ist
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(CellAddress other)
        {
            return this._row == other._row && this._column == other._column;
        }

        public override bool Equals(object obj)
        {
            
            CellAddress other = obj as CellAddress;
            if (obj == null) return false;
            
            return Equals(other);
        }

        /// <summary>
        /// Sheet schreiben: Zellen müssen sortiert werden
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(CellAddress other)
        {
            
            if (other == null) return 1;
            
            int i = this._row.CompareTo(other._row);
            if (i != 0) return i;
            return this._column.CompareTo(other._column);
        }
    }
}
