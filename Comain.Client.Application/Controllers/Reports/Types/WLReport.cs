﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Wareneinlagerungen;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class WLReport : ReportBase, IReport
    {

        private readonly IEinlagerService store;


        public WLReport(IEinlagerService store, ArtikelFilterViewModel artikelView, LSFilterViewModel lsView, 
                        LOFilterViewModel loView, WLFilterViewModel wlView, ZeitViewModel zeitView)
        {

            this.store = store;
            filterViews = new List<IFilterViewModel> { artikelView, lsView, loView, wlView, zeitView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }
 

        public override string Title
        {
            get { return "Wareneinlagerung"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 6000; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.Stock.WareneinlagerungReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.ReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
