﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailAbnahmeViewModel : ValidatableViewModel<ITicketDetailAbnahmeView, Ticket>
    {

        private readonly SynchronizationContext context;

 
        public TicketDetailAbnahmeViewModel(ITicketDetailAbnahmeView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
        }


        public override void CurrentChanged(Ticket oldSelected)
        {

            Personalleistung letzteLE = Current?.Personalleistungen?.OrderByDescending(p => p.Beginn).FirstOrDefault();
            Instandhalter = letzteLE?.Personal?.Name;
            BemerkungenLeistung = letzteLE?.Kurzbeschreibung;
            ZeitpunktLeistung = null;
            if (letzteLE != null && letzteLE.Beginn.HasValue && letzteLE.Ende.HasValue) ZeitpunktLeistung = letzteLE.ZeitAsString;

            RaisePropertyChanged("Instandhalter");
            RaisePropertyChanged("ZeitpunktLeistung");
            RaisePropertyChanged("BemerkungenLeistung");
            RaisePropertyChanged("AbnahmeBegründung");
            RaisePropertyChanged("AbnahmeAbgelehnt");
            RaisePropertyChanged("KeineAbnahme");
            RaisePropertyChanged("AbnahmeAbgenommen");
            RaisePropertyChanged("IstSauber");
            RaisePropertyChanged("BezeichnungBemerkung");

            IstSauber = false;
        }


#region P R O P E R T I E S


        public String       Instandhalter         { get; private set; }
        public String       ZeitpunktLeistung     { get; private set; }
        public String       BemerkungenLeistung   { get; private set; }  
        
        public String       BezeichnungBemerkung  { get { return AbnahmeAbgelehnt? "Begründung": "Bemerkungen"; } }   


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


        public bool IstSauber
        {
            get { return Current == null? false: Current.IstSauber; }   
            set { 
                
                if (current != null && current.IstSauber != value) {
                    
                    current.IstSauber = value;
                    RaisePropertyChanged("IstSauber");
                }
            }
        }


        public bool KeineAbnahme
        {
            get { return Current == null || !Current.AbnahmeAbgelehnt && !Current.AbnahmeAbgenommen; }   
            set { 
                
                if (current != null && value) {
                    
                    current.AbnahmeAbgelehnt = false;
                    current.AbnahmeAbgenommen = false;
                    RaisePropertyChanged("AbnahmeAbgelehnt");
                    RaisePropertyChanged("AbnahmeAbgenommen");
                    RaisePropertyChanged("KeineAbnahme");
                    RaisePropertyChanged("KeineAbnahme");
                    RaisePropertyChanged("BezeichnungBemerkung");
                }
            }
        }


        public bool AbnahmeAbgelehnt
        {
            get { return Current == null? false: Current.AbnahmeAbgelehnt; }   
            set { 
                
                if (current != null && current.AbnahmeAbgelehnt != value) {
                    
                    current.AbnahmeAbgelehnt = value;
                    RaisePropertyChanged("AbnahmeAbgelehnt");
                    RaisePropertyChanged("AbnahmeAbgenommen");
                    RaisePropertyChanged("KeineAbnahme");
                    RaisePropertyChanged("BezeichnungBemerkung");
                }
            }
        }


        public bool AbnahmeAbgenommen
        {
            get { return Current == null? false: Current.AbnahmeAbgenommen; }   
            set { 
                
                if (current != null && current.AbnahmeAbgenommen != value) {
                    
                    current.AbnahmeAbgenommen = value;
                    RaisePropertyChanged("AbnahmeAbgelehnt");
                    RaisePropertyChanged("AbnahmeAbgenommen");
                    RaisePropertyChanged("KeineAbnahme");
                    RaisePropertyChanged("BezeichnungBemerkung");
                }
            }
        }


        public String AbnahmeBegründung
        {
            get { return Current?.AbnahmeBegründung; }   
            set { 
                
                if (current != null && current.AbnahmeBegründung != value) {
                    
                    current.AbnahmeBegründung = value;
                    RaisePropertyChanged("AbnahmeBegründung");
                }
            }
        }


#endregion

    }
}
