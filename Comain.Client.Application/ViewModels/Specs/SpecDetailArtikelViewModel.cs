﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecDetailArtikelViewModel : ValidatableViewModel<ISpecDetailArtikelView, SpecArtikel>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecDetailArtikelViewModel(ISpecDetailArtikelView view)
          : base(view)
        {
        }


        public void Unload()
        {
            Einheiten = null;
        }


        public override void CurrentChanged(SpecArtikel oldSelected)
        {

            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Beschreibung));
            RaisePropertyChanged(nameof(Mischpreis));
            RaisePropertyChanged(nameof(Einheit));
            RaisePropertyChanged(nameof(Warengruppe));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Erforderlich));
            RaisePropertyChanged(nameof(Anweisung));
        }


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        {
            get => editEnabled; 
            set => SetProperty(ref editEnabled, value);
        }


        private Spezifikation currentSpezifikation;
        public Spezifikation CurrentSpezifikation
        {
            get => currentSpezifikation; 
            set { if (SetProperty(ref currentSpezifikation, value)) RaisePropertyChanged(nameof(VorgeseheneArtikelliste)); }
        }


        public TrackableCollection<SpecArtikel> VorgeseheneArtikelliste
            => CurrentSpezifikation?.VorgeseheneArtikelliste;


        public decimal? Menge
        {
            get => Current?.Menge; 
            set {

                if (Current != null && value != Menge) {

                    Current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public bool? Erforderlich
        {
            get => Current?.Erforderlich; 
            set {

                if (Current != null && value.HasValue && value != Erforderlich) {

                    Current.Erforderlich = value.Value;
                    RaisePropertyChanged(nameof(Erforderlich));
                }
            }
        }


        public String Anweisung
        {
            get => Current?.Anweisung; 
            set {

                if (Current != null && value != Anweisung) {

                    Current.Anweisung = value;
                    RaisePropertyChanged(nameof(Anweisung));
                }
            }
        }


        public  String   Name          => Current?.Name;
        public  String   Nummer        => Current?.Nummer;
        public  String   Beschreibung  => Current?.Beschreibung;
        public  decimal? Mischpreis    => Current?.Mischpreis;
        public  String   Einheit       => Current?.Einheit?.Name;
        public  String   Warengruppe   => Current?.Warengruppe?.Name;


        private List<NummerFilterModel> einheiten;
        public List<NummerFilterModel> Einheiten
        {
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


#endregion

    }
}
