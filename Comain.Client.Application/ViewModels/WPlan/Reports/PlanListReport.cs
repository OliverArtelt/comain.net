﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PlanListReport : IPlanReport
    {

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Wartungsplan offen Liste";
        public int      Sortierung              => 200;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;


        public Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo info, PlanOptions options, CancellationToken ct = default)
        {

            var report = new ReportResult { ReportAddress = "Comain.Client.Reports.WPlanListReport.rdlc",
                                            AssemblyAddress = "comain.Client.Reports",
                                            Title = "Wartungsplan" };
            report.AddDataSource("MainData", model.OffenePositionen
                                                  .OrderBy(p => p.FälligAm)
                                                  .ThenBy(p => p.IHObjekt)
                                                  .Select(p => new WartungsplanRow(p))
                                                  .ToList());
            report.AddParameter("parFilter", model.FilterInfo);
            return Task.FromResult(report);
        }


        public String   Text                => @"
Der Bericht listet die Maßnahmen auf, die im gewählten Zeitraum geplant und noch zu erledigen sind. Verwenden Sie diesen Bericht, wenn Sie eine einfache Übersicht mit den zu erledigenden Aufgaben benötigen oder weitergeben möchten.
Wählen Sie die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein, Gehen Sie auf Suchen und betätigen Sie danach die Schaltfläche ""Wartungsplan offen Liste"".
Bitte beachten Sie, es werden nur offene Positionen angezeigt. Bereits erledigte sehen Sie an anderen geeigneten Stellen im comain.";
    }
}
