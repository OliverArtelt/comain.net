﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Tickets
{

    public class TicketDetailDto : ChangeTrackingBase
    {

        public int                                 Auftragstatus            { get; set; }
        public int                                 Status                   { get; set; }
        public int                                 Typ                      { get; set; }
        public String                              Nummer                   { get; set; }
        public String                              Kurzbeschreibung         { get; set; }
        public String                              Aktivitätenbeschreibung  { get; set; }
        public DringlichkeitDto                    Dringlichkeit            { get; set; }
        public DateTime?                           Beauftragungsdatum       { get; set; }
        public DateTime                            GemeldetAm               { get; set; }
        public DateTime?                           ÜbergabeAm               { get; set; }
        public int?                                Störzeit                 { get; set; }
        public AuIHKSDto                           AuIHKS                   { get; set; }
        public NummerFilterModel                   Gewerk                   { get; set; }
        public NummerFilterModel                   Schadensbild             { get; set; }
        public NummerFilterModel                   Schadensursache          { get; set; }
        public PersonalFilterModel                 Auftragnehmer            { get; set; }
        public PersonalFilterModel                 Auftraggeber             { get; set; }
        public PersonalFilterModel                 Auftragsleiter           { get; set; }
        public PersonalFilterModel                 Abnahmeberechtigter      { get; set; }
        /// <remarks>
        /// nicht entfernen, wird für Auftragsdruck benötigt
        /// </remarks>
        public LeistungsartFilterModel             Leistungsart             { get; set; }
        public DateTime?                           Fertigstellungstermin    { get; set; }
        public DateTime?                           Auftragsleiterdatum      { get; set; }
        public DateTime?                           Abnahmedatum             { get; set; }
        public ICollection<PersonalleistungDto>    Personalleistungen       { get; set; }
        public ICollection<TicketprotokollDto>     Protokoll                { get; set; }
        /// <remarks>
        /// E-Mail-Notification: vorige Operation gelunden, E-Mail-Versand aber fehlgeschlagen
        /// </remarks>
        public String                              Sendefehler              { get; set; }
    }
}
