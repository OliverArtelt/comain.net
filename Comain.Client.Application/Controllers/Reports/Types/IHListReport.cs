﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class IHListReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public IHListReport(IReportService store, IHListViewModel ihView, IHStatusViewModel statusView, ObjektgruppeViewModel ogrView, KostenstelleViewModel kstView)
        {
            
            this.store = store;
            filterViews = new List<IFilterViewModel> { ihView, statusView, ogrView, kstView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var qry = await store.IHListAsync(GebeFilter(), ct);
            List<IHListRow> rows = new List<IHListRow>();
            foreach (var ih in qry) {

                if (ih.Details != null && ih.Details.Count > 0) {

                    ih.Details.ForEach(p => rows.Add(new IHListRow(ih, p)));
                                        
                } else {

                    rows.Add(new IHListRow(ih));  
                }
            }

            var result = Create();
            result.AddDataSource("MainData", rows);
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Maschinenliste"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 1100; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.IHListReport.rdlc"; }
        }
    }
}
