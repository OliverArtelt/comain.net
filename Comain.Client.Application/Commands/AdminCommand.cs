﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class AdminCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IAdminController);
        public String       Name        => "Administration";
        public String       Parameter   => "Admin";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 2000;
        public String       Color       => "SlateGray";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.tools.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
