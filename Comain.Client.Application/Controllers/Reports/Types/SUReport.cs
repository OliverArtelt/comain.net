﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class SUReport : ReportBase, IReport
    {

        private readonly LeistungsartViewModel laView;
        private readonly IReportService store;


        public SUReport(IReportService store, ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, LeistungsartViewModel laView)
        {
            
            this.store = store;
            this.laView = laView;
            filterViews = new List<IFilterViewModel> { zeitView, kstView, pzView, laView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.SUListAsync(GebeFilter(), ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Schadensursachen"; }
        }


        public string Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 300; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.SUReport.rdlc"; }
        }
    }
}
