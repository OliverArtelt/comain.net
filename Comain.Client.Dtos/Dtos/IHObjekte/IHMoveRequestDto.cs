﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Dtos.IHObjekte
{

    public class IHMoveRequestDto
    {

        public int SourceId                     { get; set; }
        public int DestinationId                { get; set; }
        public int DestinationType              { get; set; }
    }
}
