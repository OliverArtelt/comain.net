﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Dokumente;

namespace Comain.Client.Dtos.Dokumente
{

    public class DokumentLinkDto
    {

        public String                   EntityType      { get; set; }
        public int                      Entity_Id       { get; set; }
        public IEnumerable<int>         Link_Ids        { get; set; }
        public IEnumerable<int>         Presenters      { get; set; }
        public IEnumerable<DokumentDto> Uploads         { get; set; }
    }
}
