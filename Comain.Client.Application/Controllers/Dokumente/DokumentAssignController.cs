﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos;
using Comain.Client.Dtos.Dokumente;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Services;
using Comain.Client.Services.Dokumente;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Dokumente;
using Microsoft.Win32;

namespace Comain.Client.Controllers.Dokumente
{

    public abstract class DokumentAssignController : ControllerBase, IDokumentAssignController
    {

        private readonly IDokumentService store;
        private readonly DokumentAssignViewModel view;
        private Dictionary<int, Kategorie> kdic;
        private SerializableCommand assignCommand;
        private SerializableCommand releaseCommand;
        private DelegateCommand selectCommand;
        private DelegateCommand deselectCommand;
        private int currentId;


        protected abstract String EntityType                    { get; }

        protected abstract bool   ShowDisplayAsset              { get; }
        protected abstract bool   ShowDisplayAssetWithParents   { get; }
        protected abstract bool   ShowDisplayAssetWithChildren  { get; }
        protected abstract bool   ShowDisplayAuftrag            { get; }
        protected abstract bool   ShowDisplayMassnahme          { get; }

        protected abstract bool   ShowSearchAllDocs             { get; }
        protected abstract bool   ShowSearchAssets              { get; }
        protected abstract bool   ShowSearchMassnahmen          { get; }


        public object View => view.View;


        public DokumentAssignController(IDokumentService store, IMessageService messages, IControllerBus bus,
                                        DokumentAssignViewModel view)
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
        }


        public void Initialize()
        {

            view.SearchCommand = CreateCommand(_ => SearchAsync());
            view.OpenCommand = CreateCommand(p => OpenFileAsync(p as DokumentAssignItem));
            view.DownloadCommand = CreateCommand(p => DownloadAsync(p as DokumentAssignItem));
            view.AssignCommand = assignCommand = CreateCommand(_ => AssignAsync(), _ => view.EditEnabled && view.IsAddMode);
            view.ReleaseCommand = releaseCommand = CreateCommand(_ => ReleaseAsync(), _ => view.EditEnabled && !view.IsAddMode);
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.SelectCommand   = selectCommand = new DelegateCommand(() => Select(true), () => view.EditEnabled);
            view.DeselectCommand = deselectCommand = new DelegateCommand(() => Select(false), () => view.EditEnabled);

            view.ShowDisplayAsset = ShowDisplayAsset;
            view.ShowDisplayAssetWithParents = ShowDisplayAssetWithParents;
            view.ShowDisplayAssetWithChildren = ShowDisplayAssetWithChildren;
            view.ShowDisplayAuftrag = ShowDisplayAuftrag;
            view.ShowDisplayMassnahme = ShowDisplayMassnahme;

            view.ShowSearchAllDocs = ShowSearchAllDocs;
            view.ShowSearchAssets = ShowSearchAssets;
            view.ShowSearchMassnahmen = ShowSearchMassnahmen;

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        public async Task RunAsync(object parameter)
        {

            view.IsAddMode = false;
            currentId = 0;
        }


        protected override Task<bool> UnloadImplAsync()
        {

            view.Unload();
            return Task.FromResult(true);
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (sender == view && args.PropertyName.StartsWith("Display")) await DetailAsync(view.Current);
                if (sender == view && args.PropertyName == "UpdateCommands") UpdateCommands();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private void Select(bool isSelected)
        {

            if (!view.EditEnabled) return;
            if (view.IsAddMode && view.FoundList != null) view.FoundList.ForEach(p => p.IsSelected = isSelected);
            if (!view.IsAddMode && view.AssignedList != null) view.AssignedList.ForEach(p => p.IsSelected = isSelected);
        }


        public void SetEditMode(EditMode mode) => view.EditEnabled = mode != EditMode.ReadOnly;


        public bool EntityIsChanged => view.Current != null && (view.Current.DocumentsChanged ||
                                            view.Current.HasChangedProp(nameof(view.Current.Dokumente)));


        public async Task LoadAsync()
        {

            var kattask = store.KategorienAsync(bus.Token);
            var tagtask = store.SchlagworteAsync(bus.Token);
            await Task.WhenAll(kattask, tagtask).ConfigureAwait(false);

            kdic = kattask.Result.ToDictionary(p => p.Id);
            await OnUIThreadAsync(() => {

                view.Load(kattask.Result, tagtask.Result);
            });
            bus.Token.ThrowIfCancellationRequested();
            view.IsAddMode = false;
        }


        public async Task DetailAsync(DocEntity entity)
        {

            view.Current = entity;
            view.FoundList = null;
            view.AssignedList = null;
            if (entity == null) return;

            view.IsAddMode = entity.IstAngefügt;
            if (entity.IstAngefügt) return;

            var filter = view.GebeAnzeigefilter();
            filter.Add(nameof(EntityType), EntityType);
            filter.Add(nameof(entity.Id), entity.Id);

            var models = await store.GetAssignedAsync(filter, bus.Token).ConfigureAwait(false);
            entity.Dokumente = models;
            entity.AcceptChange(nameof(entity.Dokumente));
        }


        public async Task<Media> TitelbildAsync()
        {

            if (view.Current?.Dokumente == null) return null;

            var titel = view.Current?.Dokumente?.Where(p => p.AlsBildDarstellbar)
                                                .FirstOrDefault(p => p.PräsentiertModell);
            if (titel == null) return null;
            return await store.GetThumbnailAsync(titel, bus.Token).ConfigureAwait(false);
        }


        public async Task FillThumbnailsAsync()
        {

            if (view.Current?.Dokumente == null) return;

            var assignedDocs = new TrackableCollection<DokumentAssignItem>(ToViewItems(view.Current.Dokumente));
            await OnUIThreadAsync(() => { view.AssignedList = assignedDocs; });
            await assignedDocs.Where(p => p.AlsBildDarstellbar)
                              .ForEachAsync(async p => { p.Media = await store.GetThumbnailAsync(p.Model, bus.Token); }).ConfigureAwait(false);
        }


        /// <summary>
        /// bereits zugeordnete als solche markieren
        /// </summary>
        private void SetAssigned()
        {

            if (view.AssignedList.IsNullOrEmpty() || view.FoundList.IsNullOrEmpty()) return;

            var assignedIds = view.AssignedList.Select(p => p.Id).ToHashSet();
            view.FoundList.ForEach(p => p.SetAssigned(assignedIds));
        }


        private async Task SearchAsync()
        {

            using (new VisualDelay(view)) {

                var filter = view.GebeSuchfilter();
                filter.Add(nameof(EntityType), EntityType);
                filter.Add(nameof(view.Current.Id), view.Current.Id);

                var searchResults = ToViewItems(await store.SearchAsync(filter, bus.Token).ConfigureAwait(false));
                bus.Token.ThrowIfCancellationRequested();

                await OnUIThreadAsync(() => { view.FoundList = searchResults; });
                SetAssigned();
            }
        }


        private async Task AssignAsync()
        {

            if (!view.EditEnabled || view.Current == null) return;

            var winners = view.FoundList?.Where(p => p.IsSelected && view.AssignedList.All(q => q.Id != p.Id))
                                         .Select(p => p.Model)
                                         .ToList();
            if (winners.IsNullOrEmpty()) return;

            winners.ForEach(p => p.IstDirektBezug = true);
            view.Current.AddDocs(winners);
            await OnUIThreadAsync(() => { view.AssignedList.AddRange(ToViewItems(winners)); });
            SetAssigned();
        }


        private async Task ReleaseAsync()
        {

            if (!view.EditEnabled || view.Current == null) return;

            var losers = view.AssignedList?.Where(p => p.IsSelected).ToList();
            if (losers.IsNullOrEmpty()) return;
            view.Current.RemoveDocs(losers.Select(p => p.Model).ToList());

            await OnUIThreadAsync(() => { losers.ForEach(p => { view.AssignedList.Remove(p); }); });
            SetAssigned();
        }


        private async Task AddAsync()
        {

            if (!view.EditEnabled || view.Current == null) return;

            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Dokumente wählen";

			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            var models = dialog.FileNames.Select(p => new Dokument(p, --currentId)).ToList();
            view.Current.AddDocs(models);
            var viewmodels = models.Select(p => new DokumentAssignItem(p));
            await OnUIThreadAsync(() => { view.AssignedList.AddRange(viewmodels); });
        }


        public Task CancelAsync() => DetailAsync(view.Current);


        public async Task SaveAsync(IEnumerable<DocEntity> entities)
        {

            if (entities.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                await store.SaveAssignedAsync(entities, bus.Token).ConfigureAwait(false);
                view.IsAddMode = false;
                view.EditEnabled = false;
                if (view.Current == null) return;
            }
        }


        public async Task SaveAsync()
        {

            if (view.AssignedList == null || view.Current == null || !view.Current.DocumentsChanged) return;
            await SaveAsync(new DocEntity[] { view.Current });
        }


        /// <summary>
        /// Entity wurde erst erstellt und durch den aktualisiert =>
        /// wird brauchen den gültigen Primärschlüssel des Entities um Dokumente speichern zu können
        /// </summary>
        public void UpdateEntityId(DocEntity entity)
        {

            if (entity == null) return;
            if (view.Current == null) return;
            if (view.Current.Id == entity.Id) return;

            view.Current.Id = entity.Id;
        }


        private async Task OpenFileAsync(DokumentAssignItem model)
        {

            model = model ?? view.CurrentListItem;
            if (model == null) return;
            using (new VisualDelay(view)) {

                if (model.Content == null) model.SetDocumentFile(await store.GetContentAsync(model.Id));
                await model.StoreTempFileAsync();
                if (File.Exists(model.TempPath)) System.Diagnostics.Process.Start(model.TempPath);
            }
        }


        private async Task DownloadAsync(DokumentAssignItem model)
        {

            if (model == null) return;

            var dialog = new SaveFileDialog();
            dialog.Title = "Dokument speichern";
            dialog.FileName = model.Filename;
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                if (model.Content == null) model.SetDocumentFile(await store.GetContentAsync(model.Id));
                await model.StoreFileAsync(dialog.FileName);
            }
        }


        private List<DokumentAssignItem> ToViewItems(IEnumerable<Dokument> models)
        {

            if (view.Current == null || models.IsNullOrEmpty()) return new List<DokumentAssignItem>();
            var foundlist = models.Select(p => new DokumentAssignItem(p, kdic))
                                  .OrderBy(p => p.Quelltyp)
                                  .ThenBy(p => p.Bezeichnung)
                                  .ToList();
            return foundlist;
        }


        private void UpdateCommands()
        {

            OnUIThreadFireAndForget(() => {

                selectCommand.RaiseCanExecuteChanged();
                deselectCommand.RaiseCanExecuteChanged();
                assignCommand.RaiseCanExecuteChanged();
                releaseCommand.RaiseCanExecuteChanged();
            });
        }
    }
}
