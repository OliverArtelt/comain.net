﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewData.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    
    public class LagerController : ControllerBase, ILagerController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly ILagerService store;
        private readonly MainViewModel mainView;
        private readonly LagerViewModel view;
        private readonly LagerListViewModel listView;
        private readonly LagerFilterViewModel filterView;
        private readonly LagerDetailViewModel detailView;
        private IList<ArtikelListItem> currentList;
        private bool loadCompleted;


        public LagerController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, ILagerService store, IControllerBus bus, 
                               MainViewModel mainView, LagerViewModel view, LagerListViewModel listView, LagerFilterViewModel filterView, 
                               LagerDetailViewModel detailView)
          : base(messages, bus)        
        {

            this.crumbs = crumbs;
            this.login = login;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
        }


        public void Initialize()
        {

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;
            
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            listView.DetailCommand = CreateCommand(_ => DetailAsync());
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Lagerauskunft");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;         
            
            await ExecuteWithLockAsync(_ => LoadAsync());      
        }
 

        private async Task BackAsync()
        {
            
            crumbs.Remove();
            crumbs.Remove();
            await bus.BackAsync();
        }


        public void Shutdown()
        {
        }
 
 
        protected override Task<bool> UnloadImplAsync()
        {
            
            filterView.Unload();
            listView.Unload();
            detailView.Unload();
            currentList = null;
            return Task.FromResult(true);
        }


        private async Task LoadAsync()
        {

            loadCompleted = false;
            var oldlistmodel = listView.Current;
            listView.Unload();

            using (new VisualDelay(view)) {

                var lftask = store.LieferantenAsync(bus.Token);
                var wgtask = store.WarengruppenAsync(bus.Token);
                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var attask = LoadListAsync();
                
                await Task.WhenAll(attask, lftask, wgtask, ihtask, pztask, kstask).ConfigureAwait(false);
                
                bus.Token.ThrowIfCancellationRequested();
                                
                await filterView.LoadAsync(ihtask.Result.Select(p => new IHFilterItem(p)).ToList(), pztask.Result, kstask.Result,
                                           lftask.Result, wgtask.Result, bus.Token).ConfigureAwait(false);
            }
    
            if (oldlistmodel != null && !currentList.IsNullOrEmpty()) {
            
                listView.Current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                await DetailAsync();
            
            } else {

                detailView.Unload();
            }
  
            loadCompleted = true;
        }


        private async Task SearchAsync()
        {

            if (loadCompleted) await LoadListAsync();
            else               await LoadAsync();
        }


        private async Task LoadListAsync()
        {
            
            using (new VisualDelay(view)) {

                listView.Current = null;
                detailView.Unload();

                currentList = await store.ArtikelAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                await OnUIThreadAsync(() => { 
                
                    listView.Foundlist = new ObservableCollection<ArtikelListItem>(currentList);
                    filterView.ResultText = String.Format("Filter: {0} Artikel gefunden", currentList.Count);
                       
                });
            }
        }


        private async Task DetailAsync()
        {

            if (listView.Current != null) {

                using (new VisualDelay(detailView)) {
               
                    var reportmodel = await store.DetailAsync(listView.Current.Id);
                    var address = "Comain.Client.Reports.LagerReport.rdlc";
                    var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = reportmodel.Nummer };
           
                    report.AddDataSource("MainData", new List<ArtikelDetailItem> { reportmodel });
                    report.AddDataSource("IHData", reportmodel.IHObjekte);
                    report.AddDataSource("LFData", reportmodel.Lieferanten);
                    report.AddDataSource("LOData", reportmodel.Lagerorte);

                    await detailView.BindViewerAsync(report);
                }
            
            } else {

                detailView.Unload();
            }
        }
    }
}
