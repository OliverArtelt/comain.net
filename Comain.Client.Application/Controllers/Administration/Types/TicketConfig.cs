﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class TicketConfig : IAdminTyp
    {
        
        public String       Title           => "Tickets"; 
        public int          Sortierung      => 30000; 
        public Type         Controller      => typeof(ITicketConfigController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulTicket;
    }
}
