﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Controllers.Reports;
using Comain.Client.Services;
using Comain.Client.Views.Reports;


namespace Comain.Client.ViewModels.Reports
{

    public class ReportViewModel : PrintViewModel<IReportView>, IDelayableViewModel
    {

        public ReportViewModel(IMessageService messages, IReportView view) : base(messages, view)
        {}


        public void SetzeReports(IEnumerable liste)
        {

            context.Post(new SendOrPostCallback((o) => {

                Reports = new CollectionViewSource { Source = liste }.View;
                if (liste == null) return;

                reports.GroupDescriptions.Clear();
                reports.GroupDescriptions.Add(new PropertyGroupDescription("Gruppe"));
            }), null);
        }


#region P R O P E R T I E S


        private bool filterIsExpanded;
        public bool FilterIsExpanded
        {
            get { return filterIsExpanded; }
            set { SetProperty(ref filterIsExpanded, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICollectionView reports;
        public ICollectionView Reports
        {
            get { return reports; }
            set { SetProperty(ref reports, value); }
        }


        private IReport current;
        public IReport Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


#endregion

    }
}
