﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models
{
    
    public interface IFilterable
    {
        bool Matches(String searchText, int? id = null);
    }
}
