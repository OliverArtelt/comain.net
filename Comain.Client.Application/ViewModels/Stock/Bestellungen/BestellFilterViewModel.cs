﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Views.Stock.Bestellungen;

namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class BestellFilterViewModel : ViewModel<IBestellFilterView>
    {

        private readonly SynchronizationContext context;


        public BestellFilterViewModel(IBestellFilterView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            
            if (!String.IsNullOrEmpty(SearchText)) filter.Add("Searchterm", SearchText);
            if (Lieferant != null)      filter.Add("Lieferant", Lieferant.Id);
            if (StatusOffen)            filter.Add("StatusOffen", true);
            if (StatusAbgeschlossen)    filter.Add("StatusAbgeschlossen", true);
            if (StatusStorniert)        filter.Add("StatusStorniert", true);
            if (StatusAngelegt)         filter.Add("StatusAngelegt", true);
            if (TerminVon.HasValue)     filter.Add("TerminVon", TerminVon);
            if (TerminBis.HasValue)     filter.Add("TerminBis", TerminBis);
            if (DatumVon.HasValue)      filter.Add("BestelltVon", DatumVon);
            if (DatumBis.HasValue)      filter.Add("BestelltBis", DatumBis);
            
            return filter;
        }


        public void Unload()
        {
            Lieferanten = null;
        }


        public void SetzeListen(IEnumerable<Lieferant> lflist)
        {
            context.Send(new SendOrPostCallback((o) => { Lieferanten = lflist; }), null);
        }


        public void Reset()
        {

            SearchText          = null;
            Lieferant           = null;
            StatusOffen         = true;
            StatusAbgeschlossen = false;
            StatusStorniert     = false;
            StatusAngelegt      = true;
            TerminVon           = null;
            TerminBis           = null;
            DatumVon            = null;
            DatumBis            = null;
        }


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private String searchText;
        public String SearchText
        { 
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private IEnumerable lieferanten;
        public IEnumerable Lieferanten
        { 
            get { return lieferanten; }
            set { SetProperty(ref lieferanten, value); }
        }

     
        private Lieferant lieferant;
        public Lieferant Lieferant
        { 
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }

     
        private bool statusOffen;
        public bool StatusOffen
        { 
            get { return statusOffen; }
            set { SetProperty(ref statusOffen, value); }
        }

     
        private bool statusAbgeschlossen;
        public bool StatusAbgeschlossen
        { 
            get { return statusAbgeschlossen; }
            set { SetProperty(ref statusAbgeschlossen, value); }
        }

     
        private bool statusStorniert;
        public bool StatusStorniert
        { 
            get { return statusStorniert; }
            set { SetProperty(ref statusStorniert, value); }
        }

     
        private bool statusAngelegt;
        public bool StatusAngelegt
        { 
            get { return statusAngelegt; }
            set { SetProperty(ref statusAngelegt, value); }
        }

     
        private DateTime? terminVon;
        public DateTime? TerminVon
        { 
            get { return terminVon; }
            set { SetProperty(ref terminVon, value); }
        }

     
        private DateTime? terminBis;
        public DateTime? TerminBis
        { 
            get { return terminBis; }
            set { SetProperty(ref terminBis, value); }
        }

     
        private DateTime? datumVon;
        public DateTime? DatumVon
        { 
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }

     
        private DateTime? datumBis;
        public DateTime? DatumBis
        { 
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }
 

#endregion

    }
}
