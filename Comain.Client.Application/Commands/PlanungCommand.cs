﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.WPlan.Planen;


namespace Comain.Client.Commands
{

    public class PlanungCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IPlanController);
        public String       Name        => "Planung";
        public String       Gruppe      => "Aufträge";
        public String       Parameter   => "Planung";
        public int          Sortierung  => 5000;
        public String       Color       => "Crimson";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.calendar.week.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null || !module.VerwendetModulWPlan) return false;
            return nutzer.IstAdministrator ||
                   nutzer.IstProduktionLeitung && cfg.WPlanProdDarfPlantafelSehen ||
                   nutzer.IstProduktionSchichtmeister && cfg.WPlanProdDarfPlantafelSehen ||
                   nutzer.IstInstandhaltungInstandhalter ||
                   nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
