﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{

    public class Ferien : Entity, IDataErrorInfo, IComparable<Ferien>
    {

        private String      name;
        private DateTime?   datumVon;
        private DateTime?   datumBis;


#region P R O P E R T I E S

        
        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        
        public DateTime? DatumVon
        {
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }
        
        public DateTime? DatumBis
        {
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }


#endregion

        
        public int CompareTo(Ferien other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            if (!other.DatumVon.HasValue) return 1;
            if (!this.DatumVon.HasValue) return -1;
            return this.DatumVon.Value.CompareTo(other.DatumVon.Value);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["DatumVon"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["DatumBis"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 32) return "Die Bezeichnung darf aus maximal 32 Zeichen bestehen."; 
                    break;

                case "DatumVon":

                    if (!DatumVon.HasValue) return "Geben Sie das Datum an.";
                    if (DatumVon.Value.Year < 1900 || DatumVon.Value.Year > 2100) return "Geben Sie ein plausibles Datum an."; 
                    break;

                case "DatumBis":

                    if (!DatumBis.HasValue) return "Geben Sie das Datum an.";
                    if (DatumBis.Value.Year < 1900 || DatumBis.Value.Year > 2100) return "Geben Sie ein plausibles Datum an."; 
                    if (DatumBis < DatumVon) return "Das Startdatum muss vor dem Ende liegen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
