﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels.Anwesenheiten;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class AnwesenheitReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly ZeitViewModel zeitView;
        private readonly StandortViewModel pzView;


        public AnwesenheitReport(IReportService store, ZeitViewModel zeitView, StandortViewModel pzView)
        {

            this.store = store;
            this.zeitView = zeitView;
            this.pzView = pzView;
            filterViews = new List<IFilterViewModel> { zeitView, pzView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            var dtos = await store.AnwesenheitenAsync(GebeFilter(), ct);
            result.AddDataSource("MainData", dtos.Select(p => new AnwesenheitReportModel(p)).ToList());
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;


        public override string Title => "Anwesenheiten - Liste";
        public string Gruppe => "Personal";
        public int Sortierung => 810;
        public override string ReportAddress => "Comain.Client.Reports.AnwesenheitReport.rdlc";
    }
}
