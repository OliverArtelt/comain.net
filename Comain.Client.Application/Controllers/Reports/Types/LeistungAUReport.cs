﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{         

    public class LeistungAUReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly ILoginService login;
        private readonly LbAuViewModel lbView;


        public LeistungAUReport(IReportService store, ILoginService login,
                                LbAuViewModel lbView, ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, IntAuStelleViewModel intView)
        {
            
            this.store = store;
            this.login = login;
            this.lbView = lbView;
            filterViews = new List<IFilterViewModel> { lbView, zeitView, kstView, pzView, intView };
        }


        public override Task DisplayAsync(CancellationToken ct = default)
        {
            
            lbView.ShowPersonal = !login.AktuellerNutzer.IstProduktion;
            return Task.FromResult(true);
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await GetData(ct));
            result.AddParameter("parShowMwst", lbView.Mwst? "TRUE": "FALSE");
            return result;
        }


        private async Task<IEnumerable> GetData(CancellationToken ct)
        {

            var dtolist = await store.LeistungAuAsync(GebeFilter(), ct);
            if (!lbView.TypPersonal) return dtolist;

            var result = new List<LbAuPsRow>();

            foreach (var au in dtolist) {

                result.Add(new LbAuPsRow(au));
                if (!au.Leistungen.IsNullOrEmpty()) au.Leistungen.ForEach(ps => result.Add(new LbAuPsRow(au, ps)));
            }

            return result.OrderBy(p => p.Nummer).ThenBy(p => p.Beginn).ToList();
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Leistungsbericht - Aufträge"; }
        }


        public string Gruppe
        {
            get { return "Personal"; }
        }


        public int Sortierung
        {
            get { return 600; }
        }


        public override string ReportAddress
        {
            
            get { 
            
                if (lbView.TypPersonal)     return "Comain.Client.Reports.LeistungAU4Report.rdlc"; 
                if (lbView.TypIHObjekt)     return "Comain.Client.Reports.LeistungAU3Report.rdlc"; 
                if (lbView.TypBeschreibung) return "Comain.Client.Reports.LeistungAU2Report.rdlc"; 
               
                return "Comain.Client.Reports.LeistungAU1Report.rdlc"; 
            }
        }
    }
}
