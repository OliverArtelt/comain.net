﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;


namespace Comain.Client.Services
{

    public interface IFlyoutService
    {

        /// <summary>
        /// Benutzer hat Flyout bestätigt (auf OK-Button o.ä. gedrückt), Flyout wird geschlossen
        /// </summary>
        ICommand YesCommand      { get; }
        ICommand NoCommand       { get; }

        Task<bool> IsOpenAsync();
        /// <summary>
        /// Flyout wird nicht blockierend geöffnet (wenn mehrere abfolgende Schritte präsentiert werden sollen)
        /// </summary>
        Task OpenAsync(FlyoutConfig config);
        /// <summary>
        /// Task wird blockiert bis Flyout geschlossen wird
        /// </summary>
        /// <returns>
        /// true: YesCommand oder ContinueCommand wurde ausgeführt
        /// </returns>
        Task<bool> OpenConfirmAsync(FlyoutConfig view);
        Task CloseAsync(bool confirmed = false);
    }
}
