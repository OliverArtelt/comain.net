﻿using System;

namespace Comain.Client.Dtos.Stock
{

    public class WAReportDto 
    {

        public int              Id                  { get; set; }
        public int              ArtikelId           { get; set; }
        public String           Artikelnummer       { get; set; }
        public String           Artikelname         { get; set; }
        public decimal          Bestand             { get; set; }
        public decimal          Bestellt            { get; set; }
        public String           Entnahmeschein      { get; set; }
        public DateTime         Datum               { get; set; }
        public String           Kostenstelle        { get; set; }
        public String           Auftrag             { get; set; }
        public decimal          Menge               { get; set; }
        public decimal?         Preis               { get; set; }
        public String           Lagerort            { get; set; }
    }
}
