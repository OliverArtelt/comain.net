﻿using System;

namespace Comain.Client
{

    public interface ISettings
    {
        
        String       ApplicationVersion         { get; }
        String       ProductName                { get; }

        String       Theme                      { get; set; }
        String       Accent                     { get; set; }
        bool         ConfirmLogout              { get; set; }

        String       LayoutAuftragList          { get; set; }

        void         Save();
    }
}