﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class IHBudgetReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly ILoginService login;
        private IList<IFilterViewModel> kundenFilterViews;


        public IHBudgetReport(IReportService store, ILoginService login, 
                              ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, KundenViewModel kundenView)
        {
            
            this.store = store;
            this.login = login;
            kundenFilterViews = new List<IFilterViewModel> { zeitView, kstView, pzView };
            filterViews = new List<IFilterViewModel> { zeitView, kstView, pzView, kundenView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            IHBudgetDto dto = await store.IHBudgetAsync(GebeFilter(), ct).ConfigureAwait(false);
            
            result.AddDataSource("MainData", new List<IHBudgetDto> { dto });
            result.AddDataSource("GraphData", GraphData(dto));
            if (dto.JahrAnteil != 1m) result.AddParameter("parAnteil", "Hinweis: Die Sollwerte wurden anteilig vom Jahr ermittelt.");
           
            return result;
        }


        private List<GraphValue> GraphData(IHBudgetDto budget)
        {

            var list = new List<GraphValue>();
            list.Add(new GraphValue { Name = "Eigen",    Value = budget.IstEigenleistung });
            list.Add(new GraphValue { Name = "Material", Value = budget.IstMaterialkosten });
            list.Add(new GraphValue { Name = "Fremd",    Value = budget.IstFremdleistung });

            return list;
        }


        public override IList<IFilterViewModel> FilterViews
        {
            get {
            
                if (login.AktuellerNutzer.IstProduktion) return kundenFilterViews;
                return filterViews;           
            }
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Budget"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 100; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.IHBudgetReport.rdlc"; }
        }
    }
}
