﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class TafelDetailViewModel : ViewModel<ITafelDetailView>
    {

        private readonly SynchronizationContext context;


        public TafelDetailViewModel(ITafelDetailView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private object personalView;
        public object PersonalView
        {
            get { return personalView; }
            set { SetProperty(ref personalView, value); }
        }


        private object resultView;
        public object ResultView
        {
            get { return resultView; }
            set { SetProperty(ref resultView, value); }
        }


#endregion

    }
}
