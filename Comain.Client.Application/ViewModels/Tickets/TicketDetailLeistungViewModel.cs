﻿using System;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailLeistungViewModel : ValidatableViewModel<ITicketDetailLeistungView, Ticket>
    {

        public TicketDetailLeistungViewModel(ITicketDetailLeistungView view) : base(view)
        {
            PrefillCommand = new DelegateCommand(Prefill);
        }


        public override void CurrentChanged(Ticket oldSelected)
        {

            RaisePropertyChanged("Termin");
            RaisePropertyChanged("IHObjekt");
            RaisePropertyChanged("Maschine");
            RaisePropertyChanged("Dringlichkeit");
            RaisePropertyChanged("LeDatum");
            RaisePropertyChanged("LeUhrzeitVon");
            RaisePropertyChanged("LeUhrzeitBis");
            RaisePropertyChanged("LeAnmerkung");
            RaisePropertyChanged("LeMaterial");
            RaisePropertyChanged("LeIstFertig");
        }


        public void Unload()
        {
        }


        private void Prefill()
        {
            
            if (current == null) return;

            current.LeDatum = DateTime.Today;
            current.LeUhrzeitVon = DateTime.Now.TimeOfDay;
            current.LeUhrzeitBis = DateTime.Now.TimeOfDay;
        }
 

#region P R O P E R T I E S


        public ICommand PrefillCommand  { get; private set; }


        public DateTime?    Termin              => current?.Fertigstellungstermin;    
        public String       IHObjekt            => current?.AuIHKS?.IHObjekt;    
        public String       Maschine            => current?.AuIHKS?.Maschine;    
        public String       Dringlichkeit       => current?.Dringlichkeit?.Fullname;    
        public bool         IstAbgelehnt        => current == null? false: Current.IstAbgelehnt;    
        public String       GrundDerAblehnung   => current?.GrundDerAblehnung;    


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


        public DateTime? LeDatum
        {

            get { return current?.LeDatum; }   
            set { 
                
                if (current != null && current.LeDatum != value) {
                    
                    current.LeDatum = value;
                    RaisePropertyChanged(nameof(LeDatum));
                }
            }
        }

     
        public String LeUhrzeitVon
        {
            get { return Current == null || !Current.LeUhrzeitVon.HasValue ? null: Current.LeUhrzeitVon.Value.ToString("hh\\:mm"); }   
            set { 
                
                if (Current != null) {
                
                    var time = value.ParseTime();
                    if (Current.LeUhrzeitVon != time) {
                    
                        Current.LeUhrzeitVon = time;
                        RaisePropertyChanged(nameof(LeUhrzeitVon));
                    }
                }
            }
        }
 
     
        public String LeUhrzeitBis
        {
            get { return Current == null || !Current.LeUhrzeitBis.HasValue ? null: Current.LeUhrzeitBis.Value.ToString("hh\\:mm"); }   
            set { 
                
                if (Current != null) {
                
                    var time = value.ParseTime();
                    if (Current.LeUhrzeitBis != time) {
                    
                        Current.LeUhrzeitBis = time;
                        RaisePropertyChanged(nameof(LeUhrzeitBis));
                    }
                }
            }
        }


        public String LeAnmerkung
        {

            get { return current?.LeAnmerkung; }   
            set { 
                
                if (current != null && current.LeAnmerkung != value) {
                    
                    current.LeAnmerkung = value;
                    RaisePropertyChanged(nameof(LeAnmerkung));
                }
            }
        }


        public String LeMaterial
        {

            get { return current?.LeMaterial; }   
            set { 
                
                if (current != null && current.LeMaterial != value) {
                    
                    current.LeMaterial = value;
                    RaisePropertyChanged(nameof(LeMaterial));
                }
            }
        }


        public bool LeIstFertig
        { 
        
            get { 
            
                if (Current == null) return false;
                return Current.LeIstFertig;           
            }   
            set { 
                
                if (current != null) {
                    
                    current.LeIstFertig = value;
                    RaisePropertyChanged(nameof(LeIstFertig));
                }
            }
        }


#endregion


    }
}
