﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Models.Stock
{

    [DebuggerDisplay("{Nummer}")]
    public class ArtikelIHObjekt : Entity, IDataErrorInfo
    {
        
        private int                 position;
        private IHObjektFilterModel iHObjekt;


        public int                  Material_Id     { get; set; }
       

        public int Position 
        {
            get { return position; }
            set { SetProperty(ref position, value); }
        }
        

        public IHObjektFilterModel IHObjekt 
        {
            get { return iHObjekt; }
            set { SetProperty(ref iHObjekt, value); }
        }


        public String Nummer { get { return iHObjekt == null? null: iHObjekt.Nummer; } }


#region V A L I D A T I O N

       
        public string Error
        {
            get {  return null;  }
        }


        public string this[string columnName]
        {
            get {  return null;  }
        }


#endregion

    }
}
