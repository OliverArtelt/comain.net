﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Stock.Wareneinlagerungen;


namespace Comain.Client.Commands
{
   
    public class WareneinlagerungCommand : IControllerCommand 
    {

        public Type         Controller  => typeof(IEinlagerController);
        public String       Name        => "Waren- einlagerungen";
        public String       Parameter   => "Stock.WL";
        public String       Gruppe      => "Ersatzteile";
        public int          Sortierung  => 5000;
        public String       Color       => "DodgerBlue";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.layer.add.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null || !module.VerwendetModulStock) return false;
            return nutzer.IstAdministrator || nutzer.IstLagerAdministrator || nutzer.IstLagerEinlagerungen;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
