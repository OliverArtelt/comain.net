﻿using System.Collections;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Stock;
using Comain.Client.Views.Stock.Reklamationen;

namespace Comain.Client.ViewModels.Stock.Reklamationen
{

    public class ReklamBSPosViewModel : ViewModel<IReklamBSPosView>
    {
 
        public ReklamBSPosViewModel(IReklamBSPosView view) : base(view)
        {
        }


        internal void Unload()
        {
            List = null;  
        }


#region P R O P E R T I E S
 

        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }
 

        private IEnumerable list;
        public IEnumerable List
        {
            get { return list; }
            set { SetProperty(ref list, value); }
        }
 

        private RKEingangDto current;
        public RKEingangDto Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}