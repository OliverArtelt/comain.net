﻿using System;
using System.Collections.Generic;

namespace Comain.Client.Controllers.Verwaltung
{
    
    public interface IVwTypFactory
    {
       
        IList<IVwTyp> AlleEinheiten();
        IList<IVwTyp> VerfügbareEinheiten();
    }
}
