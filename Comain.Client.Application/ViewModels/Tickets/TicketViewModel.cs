﻿using System;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketViewModel : ViewModel<ITicketView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;

 
        public TicketViewModel(ITicketView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
 

        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
 

        private bool saveVisible;
        public bool SaveVisible
        { 
            get { return saveVisible; }
            set { SetProperty(ref saveVisible, value); }
        }
 

        private bool editVisible;
        public bool EditVisible
        { 
            get { return editVisible; }
            set { SetProperty(ref editVisible, value); }
        }
 

        private bool cancelVisible;
        public bool CancelVisible
        { 
            get { return cancelVisible; }
            set { SetProperty(ref cancelVisible, value); }
        }
 

        private bool reloadVisible;
        public bool ReloadVisible
        { 
            get { return reloadVisible; }
            set { SetProperty(ref reloadVisible, value); }
        }
 

        private bool addVisible;
        public bool AddVisible
        { 
            get { return addVisible; }
            set { SetProperty(ref addVisible, value); }
        }
 

        private bool deleteVisible;
        public bool DeleteVisible
        { 
            get { return deleteVisible; }
            set { SetProperty(ref deleteVisible, value); }
        }
 

        private bool closeVisible;
        public bool CloseVisible
        { 
            get { return closeVisible; }
            set { SetProperty(ref closeVisible, value); }
        }
 

        private bool stornoVisible;
        public bool StornoVisible
        { 
            get { return stornoVisible; }
            set { SetProperty(ref stornoVisible, value); }
        }
 

        private bool openImageVisible;
        public bool OpenImageVisible
        { 
            get { return openImageVisible; }
            set { SetProperty(ref openImageVisible, value); }
        }
 

        private bool saveImageVisible;
        public bool SaveImageVisible
        { 
            get { return saveImageVisible; }
            set { SetProperty(ref saveImageVisible, value); }
        }
 

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
 

        private ICommand editCommand;
        public ICommand EditCommand
        { 
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }
 

        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
 

        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }
 

        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }
 

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }
 

        private ICommand closeCommand;
        public ICommand CloseCommand
        { 
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }
 

        private ICommand stornoCommand;
        public ICommand StornoCommand
        { 
            get { return stornoCommand; }
            set { SetProperty(ref stornoCommand, value); }
        }
 

        private ICommand openImageCommand;
        public ICommand OpenImageCommand
        { 
            get { return openImageCommand; }
            set { SetProperty(ref openImageCommand, value); }
        }
 

        private ICommand saveImageCommand;
        public ICommand SaveImageCommand
        { 
            get { return saveImageCommand; }
            set { SetProperty(ref saveImageCommand, value); }
        }
 

#endregion

    }
}
