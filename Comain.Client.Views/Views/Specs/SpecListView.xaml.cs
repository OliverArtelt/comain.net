﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Specs.Verwaltung;


namespace Comain.Client.Views.Specs.Verwaltung
{

    public partial class SpecListView : UserControl, ISpecListView
    {

        public SpecListView()
        {
            InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            if (DataContext is SpecListViewModel vm)
                vm.DetailCommand.Execute(sender);
        }
    }
}
