﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using System.Collections.ObjectModel;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Models.WPlan
{

    public class Checklistposition : Entity, IDataErrorInfo
    {

        private Checklistergebnis            ergebnis;
        private DateTime?                    erledigtZeitVon;
        private DateTime?                    erledigtZeitBis;
        private int?                         minutenGearbeitet;
        private int?                         mengeErledigt;
        private String                       bemerkungen;


        public bool IstErledigt => ergebnis != Checklistergebnis.Undefiniert;
        public int? MinutenErmittelt => MinutenGearbeitet ?? Normzeit;
        public int? MinutenErmitteltProPerson
        {
            get {

                if (MinutenErmittelt.HasValue && Personalliste.Count > 1)
                    return (int)Math.Ceiling((double)MinutenErmittelt.Value / Personalliste.Count);
                return MinutenErmittelt;
            }
        }


        public void PrüfePersonal()
        {

            if (!Personalliste.IsNullOrEmpty()) return;
            if (EmpfohlenesPersonal != null) Personalliste.Add(EmpfohlenesPersonal);
        }


        public ChecklistSubmitDto AsDto()
        {

            var dto = new ChecklistSubmitDto {

                Id                   = Id,
                Auftrag_Id           = Auftrag_Id,
                Ergebnis             = (int)Ergebnis,
                MinutenGearbeitet    = MinutenErmittelt,
                Bemerkungen          = Bemerkungen,
                MengeErledigt        = MengeErledigt,
                Personalliste        = Personalliste.ToList()
            };

            return dto;
        }


        public bool IstBedarfsposition  => Termintyp == Termintyp.Bedarfsposition;
        public bool IstSonderleistung   => Termintyp != Termintyp.Bedarfsposition && !FälligAm.HasValue;
        public DateTime? DisplayDate    => FälligAm ?? DurchgeführtAm;


#region P R O P E R T I E S


        public int                          Auftrag_Id                  { get; set; }
        public int                          Massnahme_Id                { get; set; }
        public PersonalFilterModel          EmpfohlenesPersonal         { get; set; }
        public String                       Name                        { get; set; }
        public String                       Kurzbeschreibung            { get; set; }
        public String                       Qualifikation               { get; set; }
        public String                       Werkzeug                    { get; set; }
        public String                       Durchführung                { get; set; }
        public String                       Sicherheit                  { get; set; }
        public String                       Dokumentation               { get; set; }
        public String                       Massnahmenummer             { get; set; }
        public String                       ExterneAuftragsnummer       { get; set; }
        public DateTime?                    FälligAm                    { get; set; }
        public DateTime?                    ZuletztAm                   { get; set; }
        public DateTime?                    NächsterAm                  { get; set; }
        public int?                         Normzeit                    { get; set; }
        public int?                         Zyklus                      { get; set; }
        public int?                         MengeGeplant                { get; set; }
        public decimal?                     Wert                        { get; set; }
        public Termintyp                    Termintyp                   { get; set; }
        public Weekday                      Tagesmuster                 { get; set; }
        public DateTime?                    DurchgeführtAm              { get; set; }
        public String                       Folgeauftrag                { get; set; }
        public String                       Baugruppe                   { get; set; }
        public bool                         InVorigerSitzungErledigt    { get; set; }
        public bool                         MassnahmeIstGesperrt        { get; set; }
        public List<MaterialInfoDto>        Materialliste               { get; set; }


        private ObservableCollection<PersonalFilterModel> personalliste;
        public  ObservableCollection<PersonalFilterModel> Personalliste
        {
            get => personalliste ?? (personalliste = new ObservableCollection<PersonalFilterModel>());
            set => personalliste = value;
        }


        public Checklistergebnis Ergebnis
        {
            get { return ergebnis; }
            set { SetProperty(ref ergebnis, value); }
        }


        public DateTime? ErledigtZeitVon
        {
            get { return erledigtZeitVon; }
            set { SetProperty(ref erledigtZeitVon, value); }
        }


        public DateTime? ErledigtZeitBis
        {
            get { return erledigtZeitBis; }
            set { SetProperty(ref erledigtZeitBis, value); }
        }


        public int? MinutenGearbeitet
        {
            get { return minutenGearbeitet; }
            set { SetProperty(ref minutenGearbeitet, value); }
        }


        public int? MengeErledigt
        {
            get { return mengeErledigt; }
            set { SetProperty(ref mengeErledigt, value); }
        }


        public String Bemerkungen
        {
            get { return bemerkungen; }
            set { SetProperty(ref bemerkungen, value); }
        }


#endregion


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(MinutenGearbeitet)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Personalliste)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(MengeErledigt)];
                return test;
            }
        }


        public IList<String> Errorlist
        {

            get {

                var list = new List<String>();

                String test = this[nameof(MinutenGearbeitet)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(Personalliste)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                test = this[nameof(MengeErledigt)];
                if (!String.IsNullOrEmpty(test)) list.Add(test);
                return list;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(MinutenGearbeitet):

                    if (!MinutenGearbeitet.HasValue) return null;
                    if (InVorigerSitzungErledigt) return null;
                    if (MinutenGearbeitet.Value < 1 && (Ergebnis == Checklistergebnis.OK || Ergebnis == Checklistergebnis.NotOK))
                        return "Wenn mehrere als OK oder Nicht OK beendete Position gemeldet werden muss in jeder mindestens eine Minute gearbeitet worden sein.";
                    break;

                case nameof(MengeErledigt):

                    if (!MengeErledigt.HasValue) return null;
                    if (InVorigerSitzungErledigt) return null;
                    if (MengeErledigt.Value < 0 && (Ergebnis == Checklistergebnis.OK || Ergebnis == Checklistergebnis.NotOK))
                        return "Die Menge darf nicht kleiner 0 sein.";
                    break;

                case nameof(Personalliste):

                    if (InVorigerSitzungErledigt) return null;
                    if (Personalliste.IsNullOrEmpty() && (Ergebnis == Checklistergebnis.OK || Ergebnis == Checklistergebnis.NotOK))
                        return "Jede als OK oder Nicht OK beendete Position muß Personal aufweisen";
                    break;
                }

                return null;
            }
        }


#endregion

    }
}
