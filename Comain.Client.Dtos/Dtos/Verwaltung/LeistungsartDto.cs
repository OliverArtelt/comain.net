﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class LeistungsartDto : ChangeTrackingBase
    {
        
        public String          Nummer                   { get; set; }
        public String          Name                     { get; set; }
        public bool            IstProdKennziffer        { get; set; }
        public bool            IstWI                    { get; set; }
        public bool            IstPlankosten            { get; set; }
        public bool            IstUnPlankosten          { get; set; }
        public bool            IstPräventation          { get; set; }
        public bool            IstSachkundigenprüfung   { get; set; }
    }
}
