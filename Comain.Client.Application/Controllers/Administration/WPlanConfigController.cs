﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;

namespace Comain.Client.Controllers.Administration
{

    public interface IWPlanConfigController : IController
    {}

    public class WPlanConfigController : ConfigController<WPlanAdminViewModel>, IWPlanConfigController
    {

        private readonly IFilterService filterStore;
        private readonly WPlanAdminViewModel configView;


        public WPlanConfigController(IKonfigService store, IMessageService messages, IControllerBus bus, IFilterService filterStore,
                                     AdminViewModel view, WPlanAdminViewModel configView)

            : base(store, messages, bus, view, configView)

        {

            this.filterStore = filterStore;
            this.configView = configView;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                var cftask = store.GetAsync(bus.Token);
                var latask = filterStore.LeistungsartenAsync(bus.Token);
                var pstask = filterStore.PersonalAsync(bus.Token);
                var drtask = filterStore.DringlichkeitenAsync(bus.Token);
                var gwtask = filterStore.GewerkeAsync(bus.Token);
                await Task.WhenAll(cftask, latask, pstask, drtask, gwtask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();

                current = cftask.Result;
                var drlist = drtask.Result.Where(p => p.Auftragstyp == (int)Auftragstyp.Standardauftrag).ToList();

                configView.SetzeListen(latask.Result, pstask.Result, drlist, gwtask.Result);
                configView.Current = current;
            }
        }
    }
}
