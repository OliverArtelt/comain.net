﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using FeserWard.Controls;


namespace Comain.Client.ViewModels.Verwaltung
{

    public interface ILieferantenSearchProvider : IIntelliboxResultsProvider
    {

        void SetItems(IEnumerable<LieferantFilterModel> lflist);
        void ClearItems();
        void AddModel(LieferantFilterModel model);
    }


    public class LieferantenSearchProvider : ILieferantenSearchProvider
    {

        private ICollection<LieferantFilterModel> lieferanten;


        public void SetItems(IEnumerable<LieferantFilterModel> lflist) => lieferanten = lflist?.ToList();
        public void AddModel(LieferantFilterModel model) => lieferanten.Add(model);
        public void ClearItems() => lieferanten = null;


        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (lieferanten == null) yield break;

            int count = 0;
            foreach (var lf in lieferanten) {

                if (String.IsNullOrWhiteSpace(searchTerm) ||
                    lf.Name1.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    lf.Name2.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    lf.Ort.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) {

                    count++;
                    if (count > maxResults) yield break;

                    yield return lf;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (lieferanten.IsNullOrEmpty()) yield break;
            foreach (var lf in lieferanten) yield return lf;
        }
    }
}
