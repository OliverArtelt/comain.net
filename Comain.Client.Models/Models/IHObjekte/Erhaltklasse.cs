﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.IHObjekte
{
   
    public enum Erhaltklasse
    {

        Undefiniert = 0,
        ///<summary>
        ///gering
        ///</summary>
        Klasse1 = 1,
        ///<summary>
        ///mittel
        ///</summary>
        Klasse2 = 2,
        ///<summary>
        ///hoch
        ///</summary>
        Klasse3 = 3,
        ///<summary>
        ///sehr hoch
        ///</summary>
        Klasse4 = 4,
    }


    public static class ErhaltklasseExtensions
    {
    
        public static String AsString(this Erhaltklasse? value)
        {
               
            switch (value.GetValueOrDefault()) {

                case Erhaltklasse.Klasse1: return "I";
                case Erhaltklasse.Klasse2: return "II";
                case Erhaltklasse.Klasse3: return "III";
                case Erhaltklasse.Klasse4: return "IV";
            }

            return null;
        }
    } 
}
