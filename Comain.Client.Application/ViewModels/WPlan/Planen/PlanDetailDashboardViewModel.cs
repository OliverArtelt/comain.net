﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanDetailDashboardViewModel : ViewModel<IPlanDetailDashboardView>
    {

        private readonly SynchronizationContext context;
        private Plan model;


        public PlanDetailDashboardViewModel(IPlanDetailDashboardView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Load(Plan model)
        {

            this.model = model;

            RaisePropertyChanged(nameof(PlanVisible));
            RaisePropertyChanged(nameof(Anlagen));
            RaisePropertyChanged(nameof(LeistungsartAsString));
            RaisePropertyChanged(nameof(GewerkAsString));
            RaisePropertyChanged(nameof(PersonalAsString));
            RaisePropertyChanged(nameof(PlanungstypAsString));
            RaisePropertyChanged(nameof(PlanungsstatusAsString));
            RaisePropertyChanged(nameof(Zeitraum));

            RaisePropertyChanged(nameof(AnzahlPositionen));
            RaisePropertyChanged(nameof(AnzahlOffenePositionen));
            RaisePropertyChanged(nameof(AnzahlErledigtePositionen));
            RaisePropertyChanged(nameof(AnzahlÜberfälligePositionen));
            RaisePropertyChanged(nameof(AnzahlPositionenOhneAuftragnehmer));
            RaisePropertyChanged(nameof(AnzahlNotwendigePositionen));
            RaisePropertyChanged(nameof(AnzahlFehlendePositionen));
            RaisePropertyChanged(nameof(AnzahlWartendePositionen));

            RaisePropertyChanged(nameof(HatLAFilter));
            RaisePropertyChanged(nameof(HatPSFilter));
            RaisePropertyChanged(nameof(HatGWFilter));

            RaisePropertyChanged(nameof(KostenGesamt));
            RaisePropertyChanged(nameof(KostenOffen));
            RaisePropertyChanged(nameof(KostenErledigt));
            RaisePropertyChanged(nameof(KostenOffenProzent));
            RaisePropertyChanged(nameof(KostenErledigtProzent));
            RaisePropertyChanged(nameof(StundenGesamt));
            RaisePropertyChanged(nameof(StundenOffen));
            RaisePropertyChanged(nameof(StundenErledigt));
            RaisePropertyChanged(nameof(StundenOffenProzent));
            RaisePropertyChanged(nameof(StundenErledigtProzent));

            RaisePropertyChanged(nameof(SeriesOffenErledigt));
            RaisePropertyChanged(nameof(SeriesWartendÜberfällig));
            RaisePropertyChanged(nameof(SeriesGeplantFehlend));
            RaisePropertyChanged(nameof(SeriesKostenOffenErledigt));
            RaisePropertyChanged(nameof(SeriesStundenOffenErledigt));
            RaisePropertyChanged(nameof(SeriesMaterialVerfügbarkeit));

            RaisePropertyChanged(nameof(SeriesOffenErledigtToolTip));
            RaisePropertyChanged(nameof(SeriesWartendÜberfälligToolTip));
            RaisePropertyChanged(nameof(SeriesGeplantFehlendToolTip));
            RaisePropertyChanged(nameof(SeriesKostenOffenErledigtToolTip));
            RaisePropertyChanged(nameof(SeriesStundenOffenErledigtToolTip));

            RaisePropertyChanged(nameof(SeriesOffenErledigtToolTipVisible));
            RaisePropertyChanged(nameof(SeriesWartendÜberfälligToolTipVisible));
            RaisePropertyChanged(nameof(SeriesGeplantFehlendToolTipVisible));
            RaisePropertyChanged(nameof(SeriesKostenOffenErledigtToolTipVisible));
            RaisePropertyChanged(nameof(SeriesStundenOffenErledigtToolTipVisible));

            RaisePropertyChanged(nameof(AnzahlMaterialGesamt));
            RaisePropertyChanged(nameof(AnzahlMaterialVorhanden));
            RaisePropertyChanged(nameof(AnzahlMaterialFehlend));
            RaisePropertyChanged(nameof(ProzentMaterialVorhanden));
            RaisePropertyChanged(nameof(ProzentMaterialFehlend));
        }


        public void Unload()
        {
            model = null;
        }


#region P R O P E R T I E S


        public bool     PlanVisible                         => model?.Statistik != null;

        public String   Anlagen                             => model?.Statistik?.Anlagen;
        public String   LeistungsartAsString                => model?.Statistik?.LeistungsartAsString;
        public String   GewerkAsString                      => model?.Statistik?.GewerkAsString;
        public String   PersonalAsString                    => model?.Statistik?.PersonalAsString;
        public String   PlanungstypAsString                 => model?.Statistik?.PlanungstypAsString;
        public String   PlanungsstatusAsString              => model?.Statistik?.PlanungsstatusAsString;
        public String   Zeitraum                            => model?.Statistik?.Zeitraum;

        public bool     HatLAFilter                         => !String.IsNullOrEmpty(LeistungsartAsString);
        public bool     HatPSFilter                         => !String.IsNullOrEmpty(PersonalAsString);
        public bool     HatGWFilter                         => !String.IsNullOrEmpty(GewerkAsString);

        public int?     AnzahlPositionen                    => model?.Statistik?.AnzahlPositionen;
        public int?     AnzahlOffenePositionen              => model?.Statistik?.AnzahlOffenePositionen;
        public int?     AnzahlErledigtePositionen           => model?.Statistik?.AnzahlErledigtePositionen;
        public int?     AnzahlÜberfälligePositionen         => model?.Statistik?.AnzahlÜberfälligePositionen;
        public int?     AnzahlPositionenOhneAuftragnehmer   => model?.Statistik?.AnzahlPositionenOhneAuftragnehmer;
        public int?     AnzahlNotwendigePositionen          => model?.Statistik?.AnzahlNotwendigePositionen;
        public int?     AnzahlWartendePositionen            => model?.Statistik == null? 0: Math.Max(0, (int)(AnzahlOffenePositionen - AnzahlÜberfälligePositionen));
        public int?     AnzahlFehlendePositionen            => model?.Statistik == null? 0: Math.Max(0, (int)(AnzahlNotwendigePositionen - AnzahlPositionen));

        public int?     AnzahlMaterialVorhanden             => model?.AnzahlMaterialVorhanden;
        public int?     AnzahlMaterialFehlend               => model?.AnzahlMaterialFehlend;
        public int?     AnzahlMaterialGesamt                => AnzahlMaterialVorhanden + AnzahlMaterialFehlend;
        public double?  ProzentMaterialVorhanden            => AnzahlMaterialGesamt.GetValueOrDefault() <= 0? 0: AnzahlMaterialVorhanden / (double)AnzahlMaterialGesamt;
        public double?  ProzentMaterialFehlend              => AnzahlMaterialGesamt.GetValueOrDefault() <= 0? 0: AnzahlMaterialFehlend / (double)AnzahlMaterialGesamt;

        public decimal? KostenGesamt                        => KostenErledigt + KostenOffen;
        public decimal? KostenOffen                         => model?.Statistik?.KostenOffen;
        public decimal? KostenErledigt                      => model?.Statistik?.KostenErledigt;
        public decimal? KostenOffenProzent                  => KostenGesamt.GetValueOrDefault() == 0? 0: KostenOffen / KostenGesamt;
        public decimal? KostenErledigtProzent               => KostenGesamt.GetValueOrDefault() == 0? 0: KostenErledigt / KostenGesamt;
        public decimal? StundenGesamt                       => StundenErledigt + StundenOffen;
        public decimal? StundenOffen                        => model?.Statistik?.StundenOffen;
        public decimal? StundenErledigt                     => model?.Statistik?.StundenErledigt;
        public decimal? StundenOffenProzent                 => StundenGesamt.GetValueOrDefault() == 0? 0: StundenOffen / StundenGesamt;
        public decimal? StundenErledigtProzent              => StundenGesamt.GetValueOrDefault() == 0? 0: StundenErledigt / StundenGesamt;


        public List<KeyValuePair<String, int>> SeriesOffenErledigt
            => new List<KeyValuePair<String, int>> {

                    new KeyValuePair<String, int>("offen",          AnzahlOffenePositionen.GetValueOrDefault()),
                    new KeyValuePair<String, int>("erledigt",       AnzahlErledigtePositionen.GetValueOrDefault())
               };


        public List<KeyValuePair<String, int>> SeriesWartendÜberfällig
            => new List<KeyValuePair<String, int>> {

                    new KeyValuePair<String, int>("wartend",        AnzahlWartendePositionen.GetValueOrDefault()),
                    new KeyValuePair<String, int>("überfällig",     AnzahlÜberfälligePositionen.GetValueOrDefault())
               };


        public List<KeyValuePair<String, int>> SeriesGeplantFehlend
            => new List<KeyValuePair<String, int>> {

                    new KeyValuePair<String, int>("geplant",        AnzahlPositionen.GetValueOrDefault()),
                    new KeyValuePair<String, int>("fehlend",        AnzahlFehlendePositionen.GetValueOrDefault())
               };


        public List<KeyValuePair<String, decimal>> SeriesKostenOffenErledigt
            => new List<KeyValuePair<String, decimal>> {

                    new KeyValuePair<String, decimal>("offen",      Math.Round(KostenOffen.GetValueOrDefault(), 2)),
                    new KeyValuePair<String, decimal>("erledigt",   Math.Round(KostenErledigt.GetValueOrDefault(), 2))
               };


        public List<KeyValuePair<String, decimal>> SeriesStundenOffenErledigt
            => new List<KeyValuePair<String, decimal>> {

                    new KeyValuePair<String, decimal>("offen",      Math.Round(StundenOffen.GetValueOrDefault(), 1)),
                    new KeyValuePair<String, decimal>("erledigt",   Math.Round(StundenErledigt.GetValueOrDefault(), 1))
               };


        public List<KeyValuePair<String, decimal>> SeriesMaterialVerfügbarkeit
            => new List<KeyValuePair<String, decimal>> {

                    new KeyValuePair<String, decimal>("vorhanden",  AnzahlMaterialVorhanden.GetValueOrDefault()),
                    new KeyValuePair<String, decimal>("fehlend",    AnzahlMaterialFehlend.GetValueOrDefault())
               };


        public String SeriesOffenErledigtToolTip
        {
            get {

                if (!PlanVisible) return null;
                if (model.Statistik.AnzahlZusätzlicherPositionen <= 0) return null;
                if (model.Statistik.AnzahlZusätzlicherPositionen == 1)
                    return "Eine Position war in diesem Zeitraum nicht fällig, wurde in diesem jedoch durchgeführt. " +
                           "Die Anzahl der Positionen in der Plantafel weichen somit von denen im Dashboard ab.";
                return $"{model.Statistik.AnzahlZusätzlicherPositionen} Positionen waren in diesem Zeitraum nicht fällig, wurden in diesem jedoch durchgeführt. " +
                        "Die Anzahl der Positionen in der Plantafel weichen somit von denen im Dashboard ab.";
            }
        }


        public String SeriesWartendÜberfälligToolTip
        {
            get {

                if (!PlanVisible) return null;
                if (AnzahlÜberfälligePositionen > 0 && model.Statistik.Planungstyp == Planungstyp.Täglich &&
                    model.Statistik.Von >= DateTime.Today && model.Statistik.Von.DayOfWeek != DayOfWeek.Monday)
                    return "Die tagesgenaue Planung wird von Montag zu Montag angezeigt um wochengenaue Positionen einzubeziehen. " +
                           $"Möglicherweise werden die tagesgenauen Positionen vor dem {model.Statistik.Von.ToShortDateString()} als überfällig angezeigt.";
                return null;
            }
        }


        public String SeriesGeplantFehlendToolTip
        {
            get {

                if (AnzahlFehlendePositionen == 0) return null;
                return "Fehlende Positionen zeigen dass möglicherweise die Planung nicht vollständig ist: " +
                       "Entweder wurden nicht alle dargestellten Standorte geplant " +
                       "oder es wird ein Zeitraum betrachtet der noch nicht geplant wurde.";
            }
        }


        public String SeriesKostenOffenErledigtToolTip => null;


        public String SeriesStundenOffenErledigtToolTip => null;


        public bool SeriesOffenErledigtToolTipVisible         => !String.IsNullOrWhiteSpace(SeriesOffenErledigtToolTip);
        public bool SeriesWartendÜberfälligToolTipVisible     => !String.IsNullOrWhiteSpace(SeriesWartendÜberfälligToolTip);
        public bool SeriesGeplantFehlendToolTipVisible        => !String.IsNullOrWhiteSpace(SeriesGeplantFehlendToolTip);
        public bool SeriesKostenOffenErledigtToolTipVisible   => !String.IsNullOrWhiteSpace(SeriesKostenOffenErledigtToolTip);
        public bool SeriesStundenOffenErledigtToolTipVisible  => !String.IsNullOrWhiteSpace(SeriesStundenOffenErledigtToolTip);


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand planCommand;
        public ICommand PlanCommand
        {
            get { return planCommand; }
            set { SetProperty(ref planCommand, value); }
        }


#endregion

    }
}
