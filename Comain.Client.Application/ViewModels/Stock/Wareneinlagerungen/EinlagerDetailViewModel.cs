﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{
   
    public class EinlagerDetailViewModel : ValidatableViewModel<IEinlagerDetailView, Wareneinlagerung>
    {

        private readonly SynchronizationContext context;


        public EinlagerDetailViewModel(IEinlagerDetailView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
            Lagerorte = null;
        }


        public override void CurrentChanged(Wareneinlagerung oldSelected)
        {
           
            RaisePropertyChanged("Menge");
            RaisePropertyChanged("Datum");
            RaisePropertyChanged("Restmenge");
            RaisePropertyChanged("Preis");
            RaisePropertyChanged("Wareneingang");
            RaisePropertyChanged("Artikelnummer");
            RaisePropertyChanged("Artikelbezeichnung");
            RaisePropertyChanged("Lagerort");
            if (Current == null) LetzterLagerort = null;
        }


#region P R O P E R T I E S
                 

        private String letzterLagerort;
        public String LetzterLagerort
        { 
            get { return letzterLagerort; }
            set { SetProperty(ref letzterLagerort, value); }
        }


        private DateTime? letzteInventur;
        public DateTime? LetzteInventur
        { 
            get { return letzteInventur; }
            set { SetProperty(ref letzteInventur, value); }
        }
                 

        public DateTime? Datum
        { 
            get { return Current == null? (DateTime?)null: Current.Datum; }
            set { 
            
                if (current != null && current.Datum != value && value.HasValue) {

                    current.Datum = value.Value;
                    RaisePropertyChanged("Datum");
                }
            }
        }


        public decimal? Menge
        {
            get { return Current == null? (decimal?)null: Current.Menge; }
            set { 
            
                if (current != null && current.Menge != value && value.HasValue) {

                    current.Menge = value.Value;
                    RaisePropertyChanged("Menge");
                }
            }
        }
        

        public decimal? Restmenge
        {
            get { return Current == null? (decimal?)null: Current.Restmenge; }
            set { 
            
                if (current != null && current.Restmenge != value && value.HasValue) {

                    current.Restmenge = value.Value;
                    RaisePropertyChanged("Restmenge");
                }
            }
        }
        

        public decimal? Preis
        {
            get { return Current == null? null: Current.Preis; }
            set { 
            
                if (current != null && current.Preis != value) {

                    current.Preis = value;
                    RaisePropertyChanged("Preis");
                }
            }
        }


        public String Wareneingang
        {
            get { return Current == null || Current.Wareneingang == null? null: Current.Wareneingang.Nummer; }
        }


        public String Artikelnummer
        {
            get { return Current == null || Current.Material == null? null: Current.Material.Nummer; }
        }


        public String Artikelbezeichnung
        {
            get { return Current == null || Current.Material == null? null: Current.Material.Name; }
        }


        public Lagerort Lagerort
        {
            get { 
            
                if (Current == null || Current.Lagerort == null || Lagerorte == null) 
                    return null;
                var lo = Lagerorte.FirstOrDefault(p => p.Id == Current.Lagerort.Id); 
                return lo; 
            }
            set { 
            
                if (current != null && current.Lagerort != value) {

                    current.Lagerort = value;
                    RaisePropertyChanged("Lagerort");
                }
            }
        }


        public IList<Lagerort> lagerorte;
        public IList<Lagerort> Lagerorte
        {
            get { return lagerorte; }
            set { SetProperty(ref lagerorte, value); }
        }


#endregion

    }
}
