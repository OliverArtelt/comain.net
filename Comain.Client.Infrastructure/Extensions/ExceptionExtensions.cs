﻿using System;
using System.Collections.Generic;


namespace Comain
{
   
    public static class ExceptionExtensions
    {

        public static String UnwrapMessage(this Exception source) 
        {

            if (source == null) return null;

            var msgs = new List<String>();
            Exception currEx = source;

            do {

                if (!currEx.Message.Contains("See the inner exception for")) msgs.Add(currEx.Message);
                currEx = currEx.InnerException;
            }
            while (currEx != null);

            return String.Join(System.Environment.NewLine, msgs);
        }
    }
}
