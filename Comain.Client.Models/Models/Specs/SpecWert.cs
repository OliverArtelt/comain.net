﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;

namespace Comain.Client.Models.Specs
{

    public class SpecWert : Entity, IDataErrorInfo
    {

        private String              name;
        private bool                pflichtfeld;
        private NummerFilterModel   einheit;
        private Wertetyp            wertetyp     ;
        private String              enumeration  ;
        private String              assetTab     ;
        private String              artikelTab   ;


        public SpecWert()
        {}


        public SpecWert(SpecWertDto dto)
        {

            Id           = dto.Id;
            Name         = dto.Name;
            Pflichtfeld  = dto.Pflichtfeld;
            Wertetyp = (Wertetyp)dto.Wertetyp;
            Enumeration = dto.Enumeration;
            AssetTab = dto.AssetTab;
            ArtikelTab = dto.ArtikelTab;
            Einheit      = dto.Einheit;

            AcceptChanges();
        }


        public String Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }


        public bool Pflichtfeld
        {
            get => pflichtfeld;
            set => SetProperty(ref pflichtfeld, value);
        }


        public Wertetyp Wertetyp
        {
            get => wertetyp;
            set => SetProperty(ref wertetyp, value);
        }


        public String Enumeration
        {
            get => enumeration;
            set => SetProperty(ref enumeration, value);
        }


        public String AssetTab
        {
            get => assetTab;
            set => SetProperty(ref assetTab, value);
        }


        public String ArtikelTab
        {
            get => artikelTab;
            set => SetProperty(ref artikelTab, value);
        }


        public NummerFilterModel Einheit
        {
            get => einheit;
            set => SetProperty(ref einheit, value);
        }


        public virtual SpecWert Clone()
        {

            var clone = new SpecWert {

                Name         = Name,
                Pflichtfeld  = Pflichtfeld,
                ArtikelTab = ArtikelTab,
                AssetTab = AssetTab,
                Enumeration = Enumeration,
                Wertetyp = Wertetyp,
                Einheit      = Einheit
            };

            clone.AcceptChanges();
            return clone;
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Name)];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie den Namen an.";
                    if (Name.Length > 512) return "Der Name darf aus maximal 512 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
