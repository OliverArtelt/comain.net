﻿using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Auth;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Services.Verwaltung
{

    public class NutzerService : INutzerService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public NutzerService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<Nutzer>> NutzerAsync(CancellationToken ct = default)
        {

            var url = "/Nutzer";
            var result = await store.GetAsync<IList<NutzerDto>>(url, ct).ConfigureAwait(false);
            return mapper.Map<List<Nutzer>>(result);
        }


        public async Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);
            return qry.OrderBy(p => p.Nummer).ToList();
        }


        public async Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {

            var url = "/Filter/PS";
            var result = await store.GetAsync<IList<PersonalFilterModel>>(url, ct).ConfigureAwait(false);
            return result;
        }


        public async Task UpdateAsync(Nutzer model, CancellationToken ct = default)
        {

            var url = "/Nutzer";
            var dto = mapper.Map<NutzerDto>(model);
            await store.PutAsync(url, dto, ct).ConfigureAwait(false);
        }


        public async Task DeleteAsync(Nutzer model, CancellationToken ct = default)
        {

            var url = "/Nutzer/" + model.Id;
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }
    }
}
