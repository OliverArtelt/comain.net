﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class IHStatusViewModel : ViewModel<IIHStatusView>, IFilterViewModel
    {

        public IHStatusViewModel(IIHStatusView view) : base(view)
        {}


        public void Reset()
        {

            Status1 = false;
            Status2 = false;
            Status3 = false;
            Status4 = false;
            Klasse1 = false;
            Klasse2 = false;
            Klasse3 = false;
            Klasse4 = false;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (!HasStatus && !HasKlasse) return null;

            var bld = new StringBuilder();

            if (HasStatus) {

                bld.Append("IH-Status: ");
                if (Status1) bld.Append("Sehr hoch, ");
                if (Status2) bld.Append("Hoch, ");
                if (Status3) bld.Append("Gering, ");
                if (Status4) bld.Append("Keine, ");
                bld.Length -= 2;
            }
            
            if (HasKlasse) {

                if (HasStatus) bld.Append("; ");
                bld.Append("Erhaltklasse: ");

                if (Klasse4) bld.Append("IV, ");
                if (Klasse3) bld.Append("III, ");
                if (Klasse2) bld.Append("II, ");
                if (Klasse1) bld.Append("I, ");
                bld.Length -= 2;
            }

            return bld.ToString();
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (!HasStatus && !HasKlasse) return;

            String s = String.Empty;
            if (HasStatus) {

                if (Status1) s += "1,";
                if (Status2) s += "2,";
                if (Status3) s += "3,";
                if (Status4) s += "4,";
                filter.Add("IHStatus", s.Trim(','));
            }
            
            s = String.Empty;
            if (HasKlasse) {

                if (Klasse1) s += "1,";
                if (Klasse2) s += "2,";
                if (Klasse3) s += "3,";
                if (Klasse4) s += "4,";
                filter.Add("EKlasse", s.Trim(','));
            }
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);     
        }


        public void Unload()
        {
        }


        private bool HasStatus
        {
            get { return Status1 != Status2 || Status1 != Status3 || Status1 != Status4; }
        }


        private bool HasKlasse
        {
            get { return Klasse1 != Klasse2 || Klasse1 != Klasse3 || Klasse1 != Klasse4; }
        }



#region P R O P E R T I E S
      

        private bool status1;
        public bool Status1
        { 
            get { return status1; }
            set { SetProperty(ref status1, value); }
        } 
   

        private bool status2;
        public bool Status2
        { 
            get { return status2; }
            set { SetProperty(ref status2, value); }
        } 
   

        private bool status3;
        public bool Status3
        { 
            get { return status3; }
            set { SetProperty(ref status3, value); }
        } 
   

        private bool status4;
        public bool Status4
        { 
            get { return status4; }
            set { SetProperty(ref status4, value); }
        } 
   

        private bool klasse1;
        public bool Klasse1
        { 
            get { return klasse1; }
            set { SetProperty(ref klasse1, value); }
        } 
   

        private bool klasse2;
        public bool Klasse2
        { 
            get { return klasse2; }
            set { SetProperty(ref klasse2, value); }
        } 
   

        private bool klasse3;
        public bool Klasse3
        { 
            get { return klasse3; }
            set { SetProperty(ref klasse3, value); }
        } 
   

        private bool klasse4;
        public bool Klasse4
        { 
            get { return klasse4; }
            set { SetProperty(ref klasse4, value); }
        } 
   

#endregion

    }
}
