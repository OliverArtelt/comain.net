﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Summen eines IH-Objektes einer Periode
    /// </summary>
    public class IHSpaltensumme : NotifyBase, IAcceptor
    {

        private Planspalte period;


        public IHSpaltensumme(IHPlanzeile parent, Planspalte period)
        {

            Parent = parent;
            this.period = period;
        }


        public IHPlanzeile              Parent  { get; }
        public List<Planzelle>          Zellen  { get; } = new List<Planzelle>();

        public Plan                     Plan         => Parent.Plan;
        public IEnumerable<Planaufgabe> Positionen   => Zellen.SelectMany(p => p.Aufgaben);
        public int                      ColumnIndex  => period.Index;

        public decimal? Stunden         => Zellen?.Sum(p => p.Stunden.GetValueOrDefault());
        public decimal? Kosten          => Zellen?.Sum(p => p.Kosten.GetValueOrDefault());

        public double   Height          => Parent.Height;
        public double   Width           => Plan.Spaltenbreite;
        public double   Left            => ColumnIndex * Width;
        public double   Top             => Parent.YStartposition;
        public Brush    TypAsFarbe      => Parent.TypAsFarbe;


        public String Wert
        {
            get {

               if (Anzeigemodus == Plananzeigemodus.Zeiten && Stunden.HasValue) return String.Format("{0:N1} h", Stunden.Value);
               if (Anzeigemodus == Plananzeigemodus.Kosten && Kosten.HasValue) return Kosten.Value.ToString("c");
                return null;
            }
        }


        public decimal ExportierteGeplanteStunden(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.Normzeit.GetValueOrDefault()) / 60m;

        public decimal ExportierteGeplanteKosten(PlanOptions opts)
            => Positionen.Where(p => p.ImExportSichtbar(opts)).Sum(p => p.GeplanteKosten.GetValueOrDefault());


        private Plananzeigemodus anzeigemodus;
        public Plananzeigemodus Anzeigemodus
        {
            get { return anzeigemodus; }
            set {

                if (SetProperty(ref anzeigemodus, value)) {

                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public void NotifyPositionChanged()
        {

            RaisePropertyChanged(nameof(Height));
            RaisePropertyChanged(nameof(Width));
            RaisePropertyChanged(nameof(Left));
            RaisePropertyChanged(nameof(Top));
            RaisePropertyChanged(nameof(TypAsFarbe));
        }


        public void Accept(IVisitor v)
        {
            v.Visit(this);
        }
    }
}
