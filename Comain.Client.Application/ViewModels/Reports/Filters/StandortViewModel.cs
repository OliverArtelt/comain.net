﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class StandortViewModel : ViewModel<IStandortView>, IFilterViewModel
    {

        private readonly IFilterService store;
        private readonly SynchronizationContext context;


        public StandortViewModel(IFilterService store, IStandortView view) : base(view)
        {

            this.store = store;
            context = SynchronizationContext.Current;
        }


        public void Reset()
        {

            if (store.Standorte.IsNullOrEmpty()) return;

            context.Send(new SendOrPostCallback((o) => {

                PzVon = store.Standorte.First();
                PzBis = store.Standorte.Last();
            }), null);
        }


        public String Validiere()
        {

            if (PzVon == null || PzBis == null) return "Geben Sie zwei Standorte an.";
            if (PzVon.CompareTo(PzBis) > 0) return "Der erste Standort darf nicht größer als der zweite sein.";
            return null;
        }


        public string FilterAlsString()
        {
            return String.Format("Standort {0} - {1}", PzVon.Nummer, PzBis.Nummer);
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("PzVon", PzVon.Nummer);
            filter.Add("PzBis", PzBis.Nummer);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            RaisePropertyChanged("Standorte");
            Reset();
            return Task.FromResult(true);
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public IList<PzFilterModel> Standorte
        {
            get { return store == null? null: store.Standorte; }
        }


        private PzFilterModel pzVon;
        public PzFilterModel PzVon
        {
            get { return pzVon; }
            set { SetProperty(ref pzVon, value); }
        }


        private PzFilterModel pzBis;
        public PzFilterModel PzBis
        {
            get { return pzBis; }
            set { SetProperty(ref pzBis, value); }
        }


#endregion

    }
}
