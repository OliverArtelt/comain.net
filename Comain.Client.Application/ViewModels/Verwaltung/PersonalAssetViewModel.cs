﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Views.Verwaltung;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Users;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class PersonalAssetViewModel : ValidatableViewModel<IPersonalAssetView, Personal>
    {

        public PersonalAssetViewModel(IPersonalAssetView view) : base(view)
        {
        }


        public override void CurrentChanged(Personal oldSelected)
        {

            if (Current == null) {

                AssignList = null;
                RaisePropertyChanged(nameof(AssignList));
                return;
            }

            var dic = Current.Assets.ToDictionary(p => p.IHObjekt_Id);
            AssignList = Maschinen.Select(p => new PersonalAssetAssignViewItem(Current, p, dic.GetValueOrDefault(p.Id))).ToList();

            RaisePropertyChanged(nameof(AssignList));
        }


        public void Select(bool select)
            => AssignList?.ForEach(p => p.Select(select));


        public virtual void Unload()
        {
            AssignList = null;
            Maschinen = null;
        }


#region P R O P E R T I E S


        private IList<IHObjektFilterModel> maschinen;
        public IList<IHObjektFilterModel> Maschinen
        {
            get => maschinen;
            set => SetProperty(ref maschinen, value);
        }


        private IList<PersonalAssetAssignViewItem> assignList;
        public IList<PersonalAssetAssignViewItem> AssignList
        {
            get => assignList;
            set => SetProperty(ref assignList, value);
        }


#endregion

    }
}
