﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Services.Verwaltung
{

    public class DringlichkeitService : IDringlichkeitService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;
        private const String url = "/VW/DR";
        protected TrackableCollection<Dringlichkeit> currentList;


        public DringlichkeitService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<TrackableCollection<Dringlichkeit>> DringlichkeitenAsync(CancellationToken ct = default)
        {

            currentList = null;
            var result = await store.GetAsync<IList<DringlichkeitDto>>(url, ct).ConfigureAwait(false);
            var list = mapper.Map<List<Dringlichkeit>>(result);
            list.Sort();
            currentList = new TrackableCollection<Dringlichkeit>(list);
            return currentList;
        }


        public Task<List<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default)
 	        => store.GetAsync<List<LeistungsartFilterModel>>("/Filter/LA", ct);


        public async Task SaveAsync(CancellationToken ct = default)
        {

            var result = mapper.AsDtoList<Dringlichkeit, DringlichkeitDto>(currentList);
            if (result == null) return;

            await store.PostAsync<List<DringlichkeitDto>>(url, result, ct).ConfigureAwait(false);
            currentList.AcceptChanges();
        }


        public bool HasChanges
        {
            get { return currentList != null && currentList.IsChanged; }
        }


        public void Reset()
        {
            currentList = null;
        }
    }
}
