﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Dtos.Stock
{
    public class LieferantApDto
    {

        public int        Id              { get; set; }
        public String     Name            { get; set; }
        public String     Vorname         { get; set; }
        public String     Telefon         { get; set; }
        public String     Fax             { get; set; }
        public String     EMail           { get; set; }
        public int?       Lieferant_Id    { get; set; }
    }
}
