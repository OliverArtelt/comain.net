﻿using System;
using System.Runtime.InteropServices;


namespace Comain.Client.Views
{

    /// <remarks>
    /// https://stackoverflow.com/questions/31153664/how-can-i-detect-when-window-10-enters-tablet-mode-in-a-windows-forms-applicatio/32417768
    /// </remarks>
    public class TabletInfo : ITabletInfo
    {

        private static readonly int SM_CONVERTIBLESLATEMODE = 0x2003;
        private static readonly int SM_TABLETPC = 0x56;

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto, EntryPoint = "GetSystemMetrics")]
        private static extern int GetSystemMetrics (int nIndex);


        public bool IsTablet  { get; }

        public bool InTabletMode
        {
            get {
            
                int state = GetSystemMetrics(SM_CONVERTIBLESLATEMODE);
                return (state == 0) && IsTablet;
            }
        }


        public TabletInfo()
        {
            IsTablet = GetSystemMetrics(SM_TABLETPC) != 0;
        }


        public event EventHandler TabletModeChanged;


        public void Update()
        {
            if (TabletModeChanged != null) TabletModeChanged(this, new TabletModeChangedEventArgs(InTabletMode));
        }
    }
}
