﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.Reports
{

    public class FilterService : IFilterService
    {

        private readonly IRepository store;


        public FilterService(IRepository store)
        {
            this.store = store;
        }


        public PeriodeDto                       Periode                 { get; private set; }
        public IList<KstFilterModel>            Kostenstellen           { get; private set; }
        public IList<PzFilterModel>             Standorte               { get; private set; }
        public IList<IHObjektFilterModel>       IHObjekte               { get; private set; }
        public IList<IHObjektFilterModel>       IHObjekteVerfügbarkeit  { get; private set; }
        public IList<NummerFilterModel>         Objektgruppen           { get; private set; }
        public IList<SubNummerFilterModel>      Objektarten             { get; private set; }
        public IList<PersonalFilterModel>       Personalnummerliste     { get; private set; }
        public IList<PersonalFilterModel>       Personalklammerliste    { get; private set; }
        public IList<NummerFilterModel>         Schadensbilder          { get; private set; }
        public IList<NummerFilterModel>         Schadensursachen        { get; private set; }
        public IList<LeistungsartFilterModel>   Leistungsarten          { get; private set; }
        public IList<NummerFilterModel>         Warengruppen            { get; private set; }
        public IList<NummerFilterModel>         Lieferscheine           { get; private set; }
        public IList<LagerortDto>               Lagerorte               { get; private set; }
        public IList<LieferantFilterModel>      Lieferanten             { get; private set; }
        public IList<PZFilterItem>              IHTreeListe             { get; private set; }
        public IList<int>                       InterneAuftragsstellen  { get; private set; }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            var jahre = store.GetAsync<PeriodeDto>("/Filter/Periode", ct);
            var kst   = store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct);
            var pz    = store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);
            var ih    = store.GetAsync<IList<IHObjektFilterModel>>("/Filter/IH", ct);
            var ihvf  = store.GetAsync<IList<IHObjektFilterModel>>("/Filter/IHVF", ct);
            var ogr   = store.GetAsync<IList<NummerFilterModel>>("/Filter/OGr", ct);
            var oart  = store.GetAsync<IList<SubNummerFilterModel>>("/Filter/OArt", ct);
            var ps    = store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);
            var sb    = store.GetAsync<IList<NummerFilterModel>>("/Filter/SB", ct);
            var su    = store.GetAsync<IList<NummerFilterModel>>("/Filter/SU", ct);
            var la    = store.GetAsync<IList<LeistungsartFilterModel>>("/Filter/LA", ct);
            var intau = store.GetAsync<IList<int>>("/Filter/IntAuStelle", ct);
            var wg    = store.GetAsync<IList<NummerFilterModel>>("/Filter/WG", ct);
            var ls    = store.GetAsync<IList<NummerFilterModel>>("/Filter/LS", ct);
            var lo    = store.GetAsync<IList<LagerortDto>>("/VW/LO", ct);
            var lf    = store.GetAsync<IList<LieferantFilterModel>>("/Filter/LF", ct);
            var pt    = store.GetAsync<IList<PZFilterItem>>("/WPlan/IHTree", ct);

            await Task.WhenAll(jahre, kst, pz, ih, ihvf, ogr, oart, ps, sb, su, la, intau, lf, wg, ls, lo, pt).ConfigureAwait(false);

            Periode                = jahre.Result;
            Kostenstellen          = kst.Result.OrderBy(p => p.Sortierung).ToList();
            Standorte              = pz.Result;
            IHObjekte              = ih.Result;
            IHObjekteVerfügbarkeit = ihvf.Result;
            Objektgruppen          = ogr.Result;
            Objektarten            = oart.Result;
            Personalnummerliste    = ps.Result;
            Personalklammerliste   = Personalnummerliste.GroupBy(p => p.Klammer)
                                                        .Select(p => p.First())
                                                        .Where(p => !String.IsNullOrEmpty(p.Klammer))
                                                        .OrderBy(p => p.Klammer)
                                                        .ToList();
            Schadensbilder         = sb.Result;
            Schadensursachen       = su.Result;
            Leistungsarten         = la.Result;
            InterneAuftragsstellen = intau.Result;
            Warengruppen           = wg.Result.OrderBy(p => p.Nummer).ToList();
            Lieferscheine          = ls.Result.OrderBy(p => p.Nummer).ToList();
            Lagerorte              = lo.Result.OrderBy(p => p.Nummer).ToList();
            Lieferanten            = lf.Result.OrderBy(p => p.Name1).ToList();
            IHTreeListe            = pt.Result;
        }


        public void Unload()
        {

            Periode                = null;
            Kostenstellen          = null;
            Standorte              = null;
            IHObjekte              = null;
            IHObjekteVerfügbarkeit = null;
            Objektgruppen          = null;
            Objektarten            = null;
            Personalnummerliste    = null;
            Personalklammerliste   = null;
            Schadensbilder         = null;
            Schadensursachen       = null;
            Leistungsarten         = null;
            Warengruppen           = null;
            Lieferscheine          = null;
            Lagerorte              = null;
            Lieferanten            = null;
            IHTreeListe            = null;
        }
    }
}
