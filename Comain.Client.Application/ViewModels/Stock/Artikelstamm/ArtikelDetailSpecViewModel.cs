﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models.Specs;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Artikelstamm;

namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class ArtikelDetailSpecViewModel : ValidatableViewModel<IArtikelDetailSpecView, Artikel>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public ArtikelDetailSpecViewModel(IArtikelDetailSpecView view)
          : base(view)
        {
        }


        public override void CurrentChanged(Artikel oldSelected)
        {
            RaisePropertyChanged(nameof(TechnischeWerte));
        }


        public void Unload()
        {
            Current = null;
        }


#region P R O P E R T I E S


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private TechnischerWert currentWert;
        public TechnischerWert CurrentWert
        {
            get { return currentWert; }
            set { SetProperty(ref currentWert, value); }
        }


        public TrackableCollection<TechnischerWert> TechnischeWerte
            => Current?.TechnischeWerte;


#endregion

    }
}
