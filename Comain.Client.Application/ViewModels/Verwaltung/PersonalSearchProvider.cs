﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using FeserWard.Controls;


namespace Comain.Client.ViewModels.Verwaltung
{

    public interface IPersonalSearchProvider : IIntelliboxResultsProvider
    {

        void SetItems(IEnumerable<PersonalFilterModel> pslist);
        void ClearItems();
        void AddModel(PersonalFilterModel model);
    }


    public class PersonalSearchProvider : IPersonalSearchProvider
    {

        private ICollection<PersonalFilterModel> personalliste;


        public void SetItems(IEnumerable<PersonalFilterModel> pslist) => personalliste = pslist?.ToList();
        public void AddModel(PersonalFilterModel model) => personalliste.Add(model);
        public void ClearItems() => personalliste = null;


        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (personalliste == null) yield break;

            int count = 0;
            foreach (var ps in personalliste) {

                if (String.IsNullOrWhiteSpace(searchTerm) ||
                    ps.Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    ps.Nachname.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    ps.Vorname.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)) {

                    count++;
                    if (count > maxResults) yield break;

                    yield return ps;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (personalliste.IsNullOrEmpty()) yield break;
            foreach (var ps in personalliste) yield return ps;
        }
    }
}
