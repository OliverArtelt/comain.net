﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Verwaltung;

namespace Comain.Client.Dtos.Filters
{

    public class MaterialstammFilterModel : EntityModelBase, IComparable<MaterialstammFilterModel>
    {

        public String               Name                    { get; set; }
        public String               Nummer                  { get; set; }
        public String               Beschreibung            { get; set; }
        public String               Herstellernummer        { get; set; }
        public decimal?             Mischpreis              { get; set; }
        public DateTime?            DeaktiviertAm           { get; set; }

        public NummerFilterModel    Einheit                 { get; set; }
        public String               Warengruppe             { get; set; }
        public List<MtLfMap>        MaterialLieferantMap    { get; set; }


        public int CompareTo(MaterialstammFilterModel other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            if (this.Nummer == null && other.Nummer == null) return 0;
            if (this.Nummer == null) return -1;

            return this.Nummer.CompareNatural(other.Nummer);
        }
    }
}
