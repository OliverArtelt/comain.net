﻿using System;

namespace Comain.Client.Models.Tickets
{

    public enum Ticketstatus
    {

        Undefiniert = 0,
        Erstellt = 1,
        InstandhalterZugewiesen = 2,
        InstandhalterEntfernt = 3,
        Schichtleiterwechsel = 4,
        LeistungErfasst = 5,
        ÜbergabeZurAbnahme = 6,
        Abgenommen = 7,
        AbnahmeAbgelehnt = 8,
        Storniert = 9,
        Abgeschlossen = 10
    }


    public static class TicketstatusExtensions
    {

        public static String StatusAsText(this Ticketstatus status)
        { 

            if (status == Ticketstatus.Erstellt)                return "Erstellt";
            if (status == Ticketstatus.InstandhalterZugewiesen) return "In Bearbeitung";
            if (status == Ticketstatus.InstandhalterEntfernt)   return "Unterbrochen";
            if (status == Ticketstatus.Schichtleiterwechsel)    return "Schichtleiterwechsel";
            if (status == Ticketstatus.LeistungErfasst)         return "Arbeit wurde durchgeführt";
            if (status == Ticketstatus.ÜbergabeZurAbnahme)      return "Übergabe zur Abnahme";
            if (status == Ticketstatus.Abgenommen)              return "Abgenommen";
            if (status == Ticketstatus.AbnahmeAbgelehnt)        return "Abnahme abgelehnt";
            if (status == Ticketstatus.Storniert)               return "Storniert";
            if (status == Ticketstatus.Abgeschlossen)           return "Abgeschlossen";
            
            return null;
        }
    }
}
