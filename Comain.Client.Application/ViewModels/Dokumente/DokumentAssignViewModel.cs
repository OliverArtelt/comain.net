﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Views.Dokumente;


namespace Comain.Client.ViewModels.Dokumente
{

    public class DokumentAssignViewModel: ViewModel<IDokumentAssignView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public DokumentAssignViewModel(IDokumentAssignView view) : base(view)
        {

            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public void Reset()
        {

            SearchText = null;
            Kategorien?.ForEach(p => p.IsSelected = false);
            SearchAllDocs = true;
        }


        public void Load(IList<Kategorie> katlist, IList<String> taglist)
        {

            Kategorien = katlist.Select(p => new KategorieFilterItem(p)).ToList();
            Schlagworte = taglist;
        }


        public void Unload()
        {

            FoundList = null;
            AssignedList = null;
            Kategorien = null;
            Schlagworte = null;
        }


        public Suchkriterien GebeSuchfilter()
        {

            var filter = new Suchkriterien();

            if (!String.IsNullOrEmpty(SearchText)) filter.Add(nameof(SearchText), SearchText);
            if (!Kategorien.IsNullOrEmpty()) filter.Add(nameof(Kategorien), String.Join(",", Kategorien.Where(p => p.IsSelected).Select(p => p.Id)));
            if (SearchAssets) filter.Add(nameof(SearchAssets), true);
            if (SearchMassnahmen) filter.Add(nameof(SearchMassnahmen), true);
            return filter;
        }


        public Suchkriterien GebeAnzeigefilter()
        {

            var filter = new Suchkriterien();

            filter.Add(nameof(DisplayAsset), DisplayAsset);
            filter.Add(nameof(DisplayAssetWithParents), DisplayAssetWithParents);
            filter.Add(nameof(DisplayAssetWithChildren), DisplayAssetWithChildren);
            filter.Add(nameof(DisplayAuftrag), DisplayAuftrag);
            filter.Add(nameof(DisplayMassnahme), DisplayMassnahme);

            return filter;
        }


#region P R O P E R T I E S


        private DocEntity current;
        public DocEntity Current
        {
            get => current;
            set {

                if (SetProperty(ref current, value)) RaisePropertyChanged(nameof(AssignedList));
            }
        }


        private bool searchAllDocs;
        public bool SearchAllDocs
        {
            get => searchAllDocs;
            set {

                if (SetProperty(ref searchAllDocs, value)) {

                    if (value) {

                        SearchMassnahmen = false;
                        SearchAssets = false;
                    }
                }
            }
        }


        private bool searchAssets;
        public bool SearchAssets
        {
            get => searchAssets;
            set {

                if (SetProperty(ref searchAssets, value)) {

                    if (value) {

                        SearchMassnahmen = false;
                        SearchAllDocs = false;
                    }
                }
            }
        }


        private bool searchMassnahmen;
        public bool SearchMassnahmen
        {
            get => searchMassnahmen;
            set {

                if (SetProperty(ref searchMassnahmen, value)) {

                    if (value) {

                        SearchAllDocs = false;
                        SearchAssets = false;
                    }
                }
            }
        }


        private bool showSearchAllDocs;
        public bool ShowSearchAllDocs
        {
            get => showSearchAllDocs;
            set { if (SetProperty(ref showSearchAllDocs, value)) UpdateCommands(); }
        }


        private bool showSearchAssets;
        public bool ShowSearchAssets
        {
            get => showSearchAssets;
            set { if (SetProperty(ref showSearchAssets, value)) UpdateCommands(); }
        }


        private bool showSearchMassnahmen;
        public bool ShowSearchMassnahmen
        {
            get => showSearchMassnahmen;
            set { if (SetProperty(ref showSearchMassnahmen, value)) UpdateCommands(); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }


        private bool isEnabled;
        public bool EditEnabled
        {
            get => isEnabled;
            set { if (SetProperty(ref isEnabled, value)) UpdateCommands(); }
        }


        private bool displayAsset;
        public bool DisplayAsset
        {
            get => displayAsset;
            set { if (SetProperty(ref displayAsset, value)) UpdateCommands(); }
        }


        private bool displayAssetWithParents;
        public bool DisplayAssetWithParents
        {
            get => displayAssetWithParents;
            set { if (SetProperty(ref displayAssetWithParents, value)) UpdateCommands(); }
        }


        private bool displayAssetWithChildren;
        public bool DisplayAssetWithChildren
        {
            get => displayAssetWithChildren;
            set { if (SetProperty(ref displayAssetWithChildren, value)) UpdateCommands(); }
        }


        private bool displayAuftrag;
        public bool DisplayAuftrag
        {
            get => displayAuftrag;
            set { if (SetProperty(ref displayAuftrag, value)) UpdateCommands(); }
        }


        private bool displayMassnahme;
        public bool DisplayMassnahme
        {
            get => displayMassnahme;
            set { if (SetProperty(ref displayMassnahme, value)) UpdateCommands(); }
        }


        private bool showDisplayAsset;
        public bool ShowDisplayAsset
        {
            get => showDisplayAsset;
            set { if (SetProperty(ref showDisplayAsset, value)) UpdateCommands(); }
        }


        private bool showDisplayAssetWithParents;
        public bool ShowDisplayAssetWithParents
        {
            get => showDisplayAssetWithParents;
            set { if (SetProperty(ref showDisplayAssetWithParents, value)) UpdateCommands(); }
        }


        private bool showDisplayAssetWithChildren;
        public bool ShowDisplayAssetWithChildren
        {
            get => showDisplayAssetWithChildren;
            set { if (SetProperty(ref showDisplayAssetWithChildren, value)) UpdateCommands(); }
        }


        private bool showDisplayAuftrag;
        public bool ShowDisplayAuftrag
        {
            get => showDisplayAuftrag;
            set { if (SetProperty(ref showDisplayAuftrag, value)) UpdateCommands(); }
        }


        private bool showDisplayMassnahme;
        public bool ShowDisplayMassnahme
        {
            get => showDisplayMassnahme;
            set { if (SetProperty(ref showDisplayMassnahme, value)) UpdateCommands(); }
        }


        public bool ShowDisplayCaption => ShowDisplayMassnahme || ShowDisplayAuftrag || ShowDisplayAssetWithParents || ShowDisplayAssetWithChildren || ShowDisplayAsset;


        /// <summary>
        /// Suchstring
        /// </summary>
        private String searchText;
        public String SearchText
        {
            get => searchText;
            set => SetProperty(ref searchText, value);
        }


        private bool isAddMode;
        public bool IsAddMode
        {
            get => isAddMode;
            set {

                if (SetProperty(ref isAddMode, value)) {

                    RaisePropertyChanged(nameof(AssignVisible));
                    RaisePropertyChanged(nameof(RemoveVisible));
                    UpdateCommands();
                }
            }
        }


        public bool AssignVisible =>  IsAddMode && !ThumbnailsVisible;
        public bool RemoveVisible => !IsAddMode && !ThumbnailsVisible;


        private ICollection<DokumentAssignItem> foundList;
        public ICollection<DokumentAssignItem> FoundList
        {
            get => foundList;
            set => SetProperty(ref foundList, value);
        }


        private DokumentAssignItem currentListItem;
        public DokumentAssignItem CurrentListItem
        {
            get => currentListItem;
            set => SetProperty(ref currentListItem, value);
        }


        private TrackableCollection<DokumentAssignItem> assignedList;
        public TrackableCollection<DokumentAssignItem> AssignedList
        {
            get => assignedList ?? (assignedList = new TrackableCollection<DokumentAssignItem>());
            set => SetProperty(ref assignedList, value);
        }


        private IList<KategorieFilterItem> kategorien;
        public IList<KategorieFilterItem> Kategorien
        {
            get => kategorien;
            set => SetProperty(ref kategorien, value);
        }


        private IList<String> schlagworte;
        public IList<String> Schlagworte
        {
            get => schlagworte;
            set => SetProperty(ref schlagworte, value);
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get => searchCommand;
            set => SetProperty(ref searchCommand, value);
        }


        private ICommand assignCommand;
        public ICommand AssignCommand
        {
            get => assignCommand;
            set => SetProperty(ref assignCommand, value);
        }


        private ICommand releaseCommand;
        public ICommand ReleaseCommand
        {
            get => releaseCommand;
            set => SetProperty(ref releaseCommand, value);
        }


        private ICommand selectCommand;
        public ICommand SelectCommand
        {
            get => selectCommand;
            set => SetProperty(ref selectCommand, value);
        }


        private ICommand deselectCommand;
        public ICommand DeselectCommand
        {
            get => deselectCommand;
            set => SetProperty(ref deselectCommand, value);
        }


        private ICommand downloadCommand;
        public ICommand DownloadCommand
        {
            get => downloadCommand;
            set => SetProperty(ref downloadCommand, value);
        }


        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get => openCommand;
            set => SetProperty(ref openCommand, value);
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get => addCommand;
            set => SetProperty(ref addCommand, value);
        }


        public ICommand ResetCommand    { get; }


        private void UpdateCommands()
        {

            RaisePropertyChanged("UpdateCommands");
            RaisePropertyChanged(nameof(ShowDisplayCaption));
        }


        private bool thumbnailsVisible;
        public bool ThumbnailsVisible
        {
            get => thumbnailsVisible;
            set {

                if (SetProperty(ref thumbnailsVisible, value)) {

                    RaisePropertyChanged(nameof(AssignVisible));
                    RaisePropertyChanged(nameof(RemoveVisible));
                    UpdateCommands();
                }
            }
        }


#endregion

    }
}
