﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;


namespace Comain
{

    [DebuggerStepThrough]
    public static class EnumerableExtended
    {

        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach(T item in enumeration) action(item);
        }


        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T, int> action)
        {

            int ndx = 0;
            foreach(T item in enumeration) action(item, ndx++);
        }


        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }


        public static Dictionary<TKey, HashSet<TSource>> ToMultiDictionary<TKey, TSource>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {

            var dic = new Dictionary<TKey, HashSet<TSource>>();

            foreach (var v in source) {

                HashSet<TSource> values;

                if (dic.TryGetValue(keySelector(v), out values)) values.Add(v);
                else dic[keySelector(v)] = new HashSet<TSource> { v };
            }

            return dic;
        }


        public static Dictionary<TKey, HashSet<TValue>> ToMultiDictionary<TKey, TValue, TSource>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TValue> elementSelector)
        {

            var dic = new Dictionary<TKey, HashSet<TValue>>();

            foreach (var v in source) {

                HashSet<TValue> values;

                if (dic.TryGetValue(keySelector(v), out values)) values.Add(elementSelector(v));
                else dic[keySelector(v)] = new HashSet<TValue> { elementSelector(v) };
            }

            return dic;
        }


        public static ReadOnlyCollection<T> ToReadOnlyList<T>(this IEnumerable<T> source)
        {

            var lst = source as IList<T>;
            if (lst != null) return new ReadOnlyCollection<T>(lst);
            return new List<T>(source).AsReadOnly();
        }


        public static Task ForEachAsync<T>(this IEnumerable<T> source, Func<T, Task> body)
        {

            return Task.WhenAll(
                from item in source
                select body(item));
        }


        public static ICollection<T> AddRange<T>(this ICollection<T> source, IEnumerable<T> newlist)
        {

            newlist.ForEach(p => source.Add(p));
            return source;
        }


        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {

            if(enumerable == null) return true;
            return !enumerable.Any();
        }


        public static bool IsNullOrEmpty(this IEnumerable enumerable)
        {

            if(enumerable == null) return true;
            foreach (var obj in enumerable) { return false; }
            return true;
        }


        public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();

            foreach (TSource element in source)
                if (seenKeys.Add(keySelector(element))) yield return element;
        }
    }
}
