﻿using System;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Models.WPlan;
using Comain.Client.ViewModels;
using Comain.Client.Views.WPlan.Vorgaben;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailMemoViewModel : ValidatableViewModel<IVorgabeDetailMemoView, WiMassnahme>
    {

        private readonly SynchronizationContext context;
 

        public VorgabeDetailMemoViewModel(IVorgabeDetailMemoView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(WiMassnahme oldSelected)
        {

            RaisePropertyChanged(nameof(Qualifikation));
            RaisePropertyChanged(nameof(Werkzeug));
            RaisePropertyChanged(nameof(Durchführung));
            RaisePropertyChanged(nameof(Sicherheit));   
            RaisePropertyChanged(nameof(Dokumentation));   
        }

        
        public void Unload()
        {
        }
  

#region P R O P E R T I E S


        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public String Qualifikation
        { 
            get { return current?.Qualifikation; }
            set { 
                
                if (Current != null && Current.Qualifikation != value) {

                    Current.Qualifikation = value;
                    RaisePropertyChanged(nameof(Qualifikation));
                }
            }
        }


        public String Werkzeug
        { 
            get { return current?.Werkzeug; }
            set { 
                
                if (Current != null && Current.Werkzeug != value) {

                    Current.Werkzeug = value;
                    RaisePropertyChanged(nameof(Werkzeug));
                }
            }
        }


        public String Durchführung
        { 
            get { return current?.Durchführung; }
            set { 
                
                if (Current != null && Current.Durchführung != value) {

                    Current.Durchführung = value;
                    RaisePropertyChanged(nameof(Durchführung));
                }
            }
        }


        public String Sicherheit
        { 
            get { return current?.Sicherheit; }
            set { 
                
                if (Current != null && Current.Sicherheit != value) {

                    Current.Sicherheit = value;
                    RaisePropertyChanged(nameof(Sicherheit));
                }
            }
        }


        public String Dokumentation
        { 
            get { return current?.Dokumentation; }
            set { 
                
                if (Current != null && Current.Dokumentation != value) {

                    Current.Dokumentation = value;
                    RaisePropertyChanged(nameof(Dokumentation));
                }
            }
        }


#endregion

    }
}


