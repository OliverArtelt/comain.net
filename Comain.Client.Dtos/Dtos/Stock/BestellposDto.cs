﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class BestellposDto : ChangeTrackingBase
    {
       
        public int?          Position           { get; set; }  
        public decimal       Menge              { get; set; }  
        public decimal?      Preis              { get; set; }  

        public int           Material_Id        { get; set; }  
        public String        LfArtikelnummer    { get; set; }  
        public String        Artikelnummer      { get; set; }  
        public String        Artikelname        { get; set; }
        public String        Einheit            { get; set; }
    }
}
