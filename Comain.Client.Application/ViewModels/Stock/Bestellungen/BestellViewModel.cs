﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellViewModel : ViewModel<IBestellView>, IDelayableViewModel
    {
 
        public BestellViewModel(IBestellView view) : base(view)
        {
        }


#region P R O P E R T I E S
 

        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }
 

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
                 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        { 
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
 

        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }
 

        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
 

        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }
 

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand addPosCommand;
        public ICommand AddPosCommand
        { 
            get { return addPosCommand; }
            set { SetProperty(ref addPosCommand, value); }
        }


        private ICommand deletePosCommand;
        public ICommand DeletePosCommand
        { 
            get { return deletePosCommand; }
            set { SetProperty(ref deletePosCommand, value); }
        }


        private ICommand setOrderedCommand;
        public ICommand SetOrderedCommand
        { 
            get { return setOrderedCommand; }
            set { SetProperty(ref setOrderedCommand, value); }
        }


        private ICommand setClosedCommand;
        public ICommand SetClosedCommand
        { 
            get { return setClosedCommand; }
            set { SetProperty(ref setClosedCommand, value); }
        }


        private ICommand setStornoCommand;
        public ICommand SetStornoCommand
        { 
            get { return setStornoCommand; }
            set { SetProperty(ref setStornoCommand, value); }
        }


        private bool editVisible;
        public bool EditVisible
        { 
            get { return editVisible; }
            set { SetProperty(ref editVisible, value); }
        }


        private bool saveVisible;
        public bool SaveVisible
        { 
            get { return saveVisible; }
            set { SetProperty(ref saveVisible, value); }
        }
 

        private bool reloadVisible;
        public bool ReloadVisible
        { 
            get { return reloadVisible; }
            set { SetProperty(ref reloadVisible, value); }
        }
 

        private bool cancelVisible;
        public bool CancelVisible
        { 
            get { return cancelVisible; }
            set { SetProperty(ref cancelVisible, value); }
        }
 

        private bool addVisible;
        public bool AddVisible
        { 
            get { return addVisible; }
            set { SetProperty(ref addVisible, value); }
        }
 

        private bool deleteVisible;
        public bool DeleteVisible
        { 
            get { return deleteVisible; }
            set { SetProperty(ref deleteVisible, value); }
        }


        private bool addPosVisible;
        public bool AddPosVisible
        { 
            get { return addPosVisible; }
            set { SetProperty(ref addPosVisible, value); }
        }


        private bool deletePosVisible;
        public bool DeletePosVisible
        { 
            get { return deletePosVisible; }
            set { SetProperty(ref deletePosVisible, value); }
        }


        private bool setOrderedVisible;
        public bool SetOrderedVisible
        { 
            get { return setOrderedVisible; }
            set { SetProperty(ref setOrderedVisible, value); }
        }


        private bool setClosedVisible;
        public bool SetClosedVisible
        { 
            get { return setClosedVisible; }
            set { SetProperty(ref setClosedVisible, value); }
        }


        private bool setStornoVisible;
        public bool SetStornoVisible
        { 
            get { return setStornoVisible; }
            set { SetProperty(ref setStornoVisible, value); }
        }


#endregion

    }
}
