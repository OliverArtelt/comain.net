﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.Materialien;
using Comain.Client.Validators;

namespace Comain.Client.Models.Aufträge
{

    public class Auftrag : DocEntity, IDataErrorInfo
    {

        private DateTime                erstelltAm;
        private DateTime?               geändertAm;
        private String                  nummer;
        private String                  externeNummer;
        private String                  fBNummer;
        private bool?                   imFremdbudget;
        private int?                    interneAuStelle;
        private String                  titel;
        private String                  kurzbeschreibung;
        private String                  aktivitätenbeschreibung;
        private Auftragstyp             typ;
        private Auftragstatus           status;
        private DateTime?               fertigstellungstermin;
        private DateTime?               gemeldetDatum;
        private DateTime?               übergabeDatum;
        private TimeSpan?               gemeldetZeit;
        private TimeSpan?               übergabeZeit;
        private int?                    störzeit;
        private DateTime?               beauftragungsdatum;
        private DateTime?               auftragsleiterdatum;
        private DateTime?               abnahmedatum;

        private short?                  menge;
        private decimal?                wert;
        private decimal?                stunden;

        private int                     leistungsart_Id;
        private NummerFilterModel       schadensbild;
        private NummerFilterModel       schadensursache;
        private NummerFilterModel       gewerk;
        private SubNummerFilterModel    subSchadensbild;
        private SubNummerFilterModel    subSchadensursache;
        private PersonalFilterModel     auftraggeber;
        private PersonalFilterModel     auftragsleiter;
        private PersonalFilterModel     abnahmeberechtigter;
        private PersonalFilterModel     auftragnehmer;
        private int?                    dringlichkeit_Id;

        private AuIHKSDto               auIHKS;

        private Leistungsart            leistungsart;

        private ICollection<Personalleistung>   personalleistungen;
        private ICollection<Materialleistung>   materialleistungen;


        private readonly AuftragSpeichernValidator validator;


        public bool UnveränderlicherTermin  { get; set; }


        public override String EntityType => "AU";


        public Auftrag()
        {
            validator = new AuftragSpeichernValidator(this);
        }


        public Auftrag(Auftrag tocopy, Konfiguration cfg) : this()
        {

            this.ErstelltAm = DateTime.Now.RoundToSeconds();
            this.GeändertAm = DateTime.Now.RoundToSeconds();

            if (tocopy == null || Typ != Auftragstyp.Standardauftrag) {

                this.InterneAuStelle = cfg.StandardInterneAuStelle;

            } else {

                if (tocopy.AuIHKS != null && tocopy.AuIHKS.IstAktiv) this.AuIHKS = tocopy.AuIHKS;
                this.InterneAuStelle = tocopy.InterneAuStelle;
                this.Leistungsart = tocopy.Leistungsart;
                this.Dringlichkeit_Id = tocopy.Dringlichkeit_Id;
                this.Status = tocopy.Status;
                this.Typ = tocopy.Typ;
                this.Menge = tocopy.Menge;
                this.Wert = tocopy.Wert;
                this.Stunden = tocopy.Stunden;

                this.GemeldetDatum = DateTime.Today;
                this.GemeldetZeit = DateTime.Now.TimeOfDay.RoundToSeconds();
            }
        }


        public void RaiseStatus()
        {
            RaisePropertyChanged("Status");
        }


        public AuftragListDto AsListDto()
            => new AuftragListDto { Id               = Id,
                                    Typ              = (int)Typ,
                                    Projekt          = Projektnummer,
                                    Nummer           = Nummer,
                                    Kurzbeschreibung = Kurzbeschreibung,
                                    Auftragsdatum    = ErstelltAm,
                                    IHObjektNummer   = AuIHKS?.IHObjekt,
                                    Maschine         = AuIHKS?.Maschine,
                                    Auftraggeber     = Auftraggeber?.NameNummer,
                                    Dringlichkeit    = Dringlichkeit?.Fullname,
                                    Termin           = Fertigstellungstermin,
                                    ExterneNummer    = ExterneNummer,
                                    Kostenstelle     = AuIHKS?.Kostenstelle,
                                    Gewerk           = Gewerk?.Nummer,
                                    Status           = Status.ToString(),
                                    Stunden          = Stunden };


#region P R O P E R T I E S


        public int                      Projektnummer           { get; set; }

        public DateTime                 ErstelltAm              { get { return erstelltAm               ; } set { SetProperty(ref erstelltAm                , value); } }
        public DateTime?                GeändertAm              { get { return geändertAm               ; } set { SetProperty(ref geändertAm                , value); } }
        public String                   Nummer                  { get { return nummer                   ; } set { SetProperty(ref nummer                    , value); } }
        public String                   ExterneNummer           { get { return externeNummer            ; } set { SetProperty(ref externeNummer             , value); } }
        public String                   FBNummer                { get { return fBNummer                 ; } set { SetProperty(ref fBNummer                  , value); } }
        public bool?                    ImFremdbudget           { get { return imFremdbudget            ; } set { SetProperty(ref imFremdbudget             , value); } }
        public int?                     InterneAuStelle         { get { return interneAuStelle          ; } set { SetProperty(ref interneAuStelle           , value); } }
        public String                   Titel                   { get { return titel                    ; } set { SetProperty(ref titel                     , value); } }
        public String                   Kurzbeschreibung        { get { return kurzbeschreibung         ; } set { SetProperty(ref kurzbeschreibung          , value); } }
        public String                   Aktivitätenbeschreibung { get { return aktivitätenbeschreibung  ; } set { SetProperty(ref aktivitätenbeschreibung   , value); } }
        public Auftragstyp              Typ                     { get { return typ                      ; } set { SetProperty(ref typ                       , value); } }
        public Auftragstatus            Status                  { get { return status                   ; } set { SetProperty(ref status                    , value); } }
        public int?                     Dringlichkeit_Id        { get { return dringlichkeit_Id         ; } set { SetProperty(ref dringlichkeit_Id          , value); } }
        public DateTime?                Fertigstellungstermin   { get { return fertigstellungstermin    ; } set { SetProperty(ref fertigstellungstermin     , value); } }
        public DateTime?                GemeldetDatum           { get { return gemeldetDatum            ; } set { SetProperty(ref gemeldetDatum             , value); } }
        public DateTime?                ÜbergabeDatum           { get { return übergabeDatum            ; } set { SetProperty(ref übergabeDatum             , value); } }
        public TimeSpan?                GemeldetZeit            { get { return gemeldetZeit             ; } set { SetProperty(ref gemeldetZeit              , value); } }
        public TimeSpan?                ÜbergabeZeit            { get { return übergabeZeit             ; } set { SetProperty(ref übergabeZeit              , value); } }
        public int?                     Störzeit                { get { return störzeit                 ; } set { SetProperty(ref störzeit                  , value); } }
        public DateTime?                Auftragsleiterdatum     { get { return auftragsleiterdatum      ; } set { SetProperty(ref auftragsleiterdatum       , value); } }
        public DateTime?                Abnahmedatum            { get { return abnahmedatum             ; } set { SetProperty(ref abnahmedatum              , value); } }

        public short?                   Menge                   { get { return menge                    ; } set { SetProperty(ref menge                     , value); } }
        public decimal?                 Wert                    { get { return wert                     ; } set { SetProperty(ref wert                      , value); } }
        public decimal?                 Stunden                 { get { return stunden                  ; } set { SetProperty(ref stunden                   , value); } }

        public NummerFilterModel        Schadensbild            { get { return schadensbild             ; } set { SetProperty(ref schadensbild              , value); } }
        public NummerFilterModel        Schadensursache         { get { return schadensursache          ; } set { SetProperty(ref schadensursache           , value); } }
        public NummerFilterModel        Gewerk                  { get { return gewerk                   ; } set { SetProperty(ref gewerk                    , value); } }
        public SubNummerFilterModel     SubSchadensbild         { get { return subSchadensbild          ; } set { SetProperty(ref subSchadensbild           , value); } }
        public SubNummerFilterModel     SubSchadensursache      { get { return subSchadensursache       ; } set { SetProperty(ref subSchadensursache        , value); } }
        public PersonalFilterModel      Auftraggeber            { get { return auftraggeber             ; } set { SetProperty(ref auftraggeber              , value); } }
        public PersonalFilterModel      Auftragsleiter          { get { return auftragsleiter           ; } set { SetProperty(ref auftragsleiter            , value); } }
        public PersonalFilterModel      Abnahmeberechtigter     { get { return abnahmeberechtigter      ; } set { SetProperty(ref abnahmeberechtigter       , value); } }
        public PersonalFilterModel      Auftragnehmer           { get { return auftragnehmer            ; } set { SetProperty(ref auftragnehmer             , value); } }
        public AuIHKSDto                AuIHKS                  { get { return auIHKS                   ; } set { SetProperty(ref auIHKS                    , value); } }

        public int                      Leistungsart_Id         { get { return leistungsart_Id          ; } set { SetProperty(ref leistungsart_Id           , value); } }
        public Leistungsart             Leistungsart            { get { return leistungsart             ; } set { SetProperty(ref leistungsart              , value); } }

        public Konfiguration            Konfiguration           { get; set; }


        public DateTime? Beauftragungsdatum
        {
            get { return beauftragungsdatum; }
            set { if (SetProperty(ref beauftragungsdatum, value) && value.HasValue) ErstelltAm = value.Value; }
        }


        public ICollection<Personalleistung> Personalleistungen
        {
            get {

                if (personalleistungen == null) personalleistungen = new TrackableCollection<Personalleistung>();
                return personalleistungen;
            }
            set { SetProperty(ref personalleistungen, value); }
        }


        public ICollection<Materialleistung> Materialleistungen
        {
            get {

                if (materialleistungen == null) materialleistungen = new TrackableCollection<Materialleistung>();
                return materialleistungen;
            }
            set { SetProperty(ref materialleistungen, value); }
        }


        private Dringlichkeit dringlichkeit;
        public Dringlichkeit Dringlichkeit
        {
            get { return dringlichkeit; }
            set {

                if (SetProperty(ref dringlichkeit, value)) {

                    Dringlichkeit_Id = value?.Id;
                }
            }
        }


#endregion


        /// <summary>
        /// Der Status des Auftrages für Bearbeiter des Auftrages
        /// </summary>
        public String StatusAsString
        {
            get {

                if (Status == Auftragstatus.Abgeschlossen) return "Abgeschlossen";
                if (Status == Auftragstatus.Storno) return "Storniert";
                if (ÜbergabeDatum.HasValue) return "Übergeben";
                if (Status == Auftragstatus.Überfällig) return "Überfällig";
                return "Offen";
            }
        }


        public String TypAsString => Typ.AsText();


        public int? BerechneteStörzeit
        {
            get {

                int? min = StörzeitInMinuten();
                if (min.GetValueOrDefault() < 1) return null;
                return min;
            }
        }


        public decimal? KostenPersonalleistungen
        {
            get {

                if (Personalleistungen == null) return null;
                return Personalleistungen.Sum(p => p.Kosten.GetValueOrDefault());
            }
        }


        public decimal? KostenMaterialleistungen
        {
            get {

                if (Materialleistungen == null) return null;
                return Materialleistungen.Sum(p => p.Gesamtpreis.GetValueOrDefault());
            }
        }


        public bool KostenstelleVerlegt { get { return auIHKS != null && !auIHKS.IstAktiv; } }

        public bool StatusBearbeitbar { get { return status == Auftragstatus.Offen || status == Auftragstatus.Überfällig; } }

        public bool IstHauptOderFbAuftrag { get { return Typ == Auftragstyp.Standardauftrag || Typ == Auftragstyp.FremdBudget; } }

        public bool IstOffen { get { return Status == Auftragstatus.Offen || Status == Auftragstatus.Überfällig; } }


        public bool StörzeitIstPlausibel
        {
            get {

                if (Leistungsart.IstUnPlankosten) return Störzeit.HasValue && Störzeit.Value >= 0 && Störzeit <= BerechneteStörzeit;
                return !Störzeit.HasValue || Störzeit.Value >= 0 && Störzeit <= BerechneteStörzeit;
            }
        }


        public DateTime? MeldedatumUndZeit { get { return GemeldetDatum + GemeldetZeit; } }


        public DateTime? ÜbergabedatumUndZeit { get { return ÜbergabeDatum + ÜbergabeZeit; } }


        public bool LeistungenVorMeldezeit
        {
            get {

                if (!MeldedatumUndZeit.HasValue) return false;
                if (!personalleistungen.IsNullOrEmpty() && personalleistungen.Any(p => p.Beginn < MeldedatumUndZeit)) return true;
                //Materialleistungen berücksichtigen keine Uhrzeit
                if (!materialleistungen.IsNullOrEmpty() && materialleistungen.Any(p => p.Datum < GemeldetDatum)) return true;
                return false;
            }
        }


        public bool HatPersonalleistungenVorAbrechnungsschluß
        {

            get {

                if (Konfiguration == null || !Konfiguration.Abrechnungsschluß.HasValue) return false;
                return !personalleistungen.IsNullOrEmpty() && personalleistungen.Any(p => p.Ende < Konfiguration.Abrechnungsschluß);
            }
        }


        public bool HatMaterialleistungenVorAbrechnungsschluß
        {

            get {

                if (Konfiguration == null || !Konfiguration.Abrechnungsschluß.HasValue) return false;
                return !materialleistungen.IsNullOrEmpty() && materialleistungen.Any(p => p.Datum < Konfiguration.Abrechnungsschluß);
            }
        }


        public int? StörzeitInMinuten()
        {

            if (!ÜbergabeDatum.HasValue || !ÜbergabeZeit.HasValue || !GemeldetDatum.HasValue || !GemeldetZeit.HasValue) return null;

            TimeSpan ts = ÜbergabeDatum.Value.Date + ÜbergabeZeit.Value - GemeldetDatum.Value.Date - GemeldetZeit.Value;
            return (int)ts.TotalMinutes;
        }


        public void NormalisiereAuftragsnummer()
        {

            if (Leistungsart == null || Leistungsart.Nummer == null || String.IsNullOrEmpty(Nummer)) return;
            Nummer = Leistungsart.Nummer[0] + Nummer.Substring(1);
        }


        public void SetzeFbNummer()
        {

            String pz = auIHKS != null && !String.IsNullOrEmpty(auIHKS.IHObjekt) && auIHKS.IHObjekt.Length > 2? auIHKS.IHObjekt.Substring(0, 3): String.Empty;
            FBNummer = String.Format("{0}-{1:D6}", pz, Projektnummer);
            ImFremdbudget = true;
        }


        public Personalleistung NeuePersonalleistung()
        {

            var le = new Personalleistung { Auftrag_Id = this.Id };
            Personalleistungen.Add(le);
            return le;
        }


#region V A L I D A T I O N


        public string Error                     { get { return validator.Error; } }

        public IList<String> DeleteErrorList    { get { return new AuftragLöschenValidator(this).ErrorList; } }
        public IList<String> AbschlussErrorList { get { return new AuftragAbschließenValidator(this).ErrorList; } }
        public IList<String> StornoErrorList    { get { return new AuftragStornierenValidator(this).ErrorList; } }
        public IList<String> ErrorList          { get { return new AuftragSpeichernValidator(this).ErrorList; } }

        public string this[string columnName]   { get { return validator[columnName]; } }


#endregion

    }
}
