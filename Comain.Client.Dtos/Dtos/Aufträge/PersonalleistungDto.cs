﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Aufträge
{

    public class PersonalleistungDto : ChangeTrackingBase
    {

        public int                  Auftrag_Id          { get; set; }
        public int                  Personal_Id         { get; set; }
        public String               Kurzbeschreibung    { get; set; }
        public DateTime             BeginnDatum         { get; set; }
        public DateTime             EndeDatum           { get; set; }
        public TimeSpan             BeginnZeit          { get; set; }
        public TimeSpan             EndeZeit            { get; set; }
        public int?                 Minuten             { get; set; }
        public int                  Menge               { get; set; }
        public decimal?             Kosten              { get; set; }
        public decimal?             Stundensatz         { get; set; }

        public String               AufschlägeAsString  { get; set; }
        public PersonalFilterModel  Personal            { get; set; }

        public ICollection<AufschlagDto> Aufschläge     { get; set; }
    }
}
