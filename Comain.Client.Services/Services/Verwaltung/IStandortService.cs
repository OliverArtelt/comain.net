﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{
  
    public interface IStandortService
    {
        
        Task LoadAsync(CancellationToken ct = default);
        TrackableCollection<Standort> CurrentPzList { get; }
        IEnumerable<KstSelektorItem> Kostenstellen(Standort pz);

        bool HasChanges { get; }
        void Reset();
        Task SaveAsync(CancellationToken ct = default);
    }
}
