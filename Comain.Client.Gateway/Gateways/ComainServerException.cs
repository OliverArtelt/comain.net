﻿using System;
using System.Text;


namespace Comain.Client.Gateways
{

    public class ComainServerException : Exception
    {

        public  String          ServerMessage      { get; private set; }
        public  String          ServerDetails      { get; private set; }
        public  String          ServerStackTrace   { get; private set; }
        public  String[]        ServerExplanations { get; private set; }


        public ComainServerException(String text) : base(ParseMessage(text))
        {

            if (text.Contains(@"<!DOCTYPE html")) {

                int start = text.IndexOf("<title>");
                int ende = text.IndexOf("</title>");

                if (start > -1 && ende > start) ServerMessage = text.Substring(start + 7, ende - start - 7);
            }
        }


        public ComainServerException(ExceptionDto dto) : base(dto.Message)
        {

            ServerMessage      = dto.Message;
            ServerDetails      = dto.Details;
            ServerStackTrace   = dto.Error_Description;
            ServerExplanations = dto.Explanations;
        }


        private static String ParseMessage(String text)
        {

            if (text.Contains(@"<!DOCTYPE html")) {

                int start = text.IndexOf("<title>");
                int ende = text.IndexOf("</title>");

                if (start > -1 && ende > start) return text.Substring(start + 7, ende - start - 7);
            }

            return null;
        }


        public override string ToString()
        {

            var bld = new StringBuilder();
            if (Message != ServerMessage) bld.Append(Message).Append(": ");
            if (!String.IsNullOrWhiteSpace(ServerMessage)) bld.Append(ServerMessage);

            if (!String.IsNullOrWhiteSpace(ServerDetails) || !String.IsNullOrWhiteSpace(ServerStackTrace)) bld.AppendLine().AppendLine("SERVER:");
            if (!String.IsNullOrWhiteSpace(ServerDetails)) bld.Append(ServerDetails);
            if (!String.IsNullOrWhiteSpace(ServerStackTrace)) bld.Append(ServerStackTrace);

            bld.AppendLine().AppendLine("CLIENT:").Append(StackTrace);
            return bld.ToString();
        }
    }
}
