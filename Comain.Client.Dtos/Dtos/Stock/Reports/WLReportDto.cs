﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class WLReportDto : ChangeTrackingBase
    {

        public decimal          Menge               { get; set; }
        public decimal          Restmenge           { get; set; }
        public decimal?         Preis               { get; set; }
        public decimal          Bestand             { get; set; }
        public decimal          Bestellt            { get; set; }
        public DateTime         Datum               { get; set; }
        public String           Lagerort            { get; set; }
        public int?             Position            { get; set; }
        public String           Lieferschein        { get; set; }
        public int              ArtikelId           { get; set; }
        public String           Artikelnummer       { get; set; }
        public String           Artikelname         { get; set; }
    }
}
