﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class LagerCommand : IControllerCommand
    {

        public Type         Controller  => typeof(ILagerController);
        public String       Name        => "Lagerauskunft";
        public String       Parameter   => "Ersatzteile";
        public String       Gruppe      => "Auswertungen";
        public int          Sortierung  => 1000;
        public String       Color       => "#CC3399";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.forklift.load.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstProduktionLeitung || nutzer.IstProduktionLagermitarbeiter || nutzer.IstInstandhaltungInstandhalter ||
                   nutzer.IstInstandhaltungLeitung || nutzer.IstLagerReporting;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
