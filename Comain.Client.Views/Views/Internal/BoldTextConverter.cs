﻿using System;
using System.Windows;
using System.Windows.Data;


namespace Comain.Client.Views.Internal
{

    public class BoldTextConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            
            if (!(value is bool)) return DependencyProperty.UnsetValue;
            return (bool)value ? "Bold": "Normal";            
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
