﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class PersonalViewModel : ViewModel<IPersonalView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public PersonalViewModel(IFilterService store, IPersonalView view, IPersonalSearchProvider searcher) : base(view)
        {

            this.store = store;
            PsSearchProvider = searcher;
            Reset();
        }


        public void Reset()
        {

            AlsPersonalnummer = true;
            Person = null;
        }


        public String Validiere()
        {

            if (Person == null) return "Wählen Sie eine Person.";
            if (AlsPersonalklammer && String.IsNullOrEmpty(Person.Klammer)) return "Die Person besitzt keine Personalklammer.";
            return null;
        }


        public string FilterAlsString()
        {

            if (Person == null) return null;
            if (AlsPersonalnummer) return $"Person: {Person.Name} [Nummer {Person.Nummer}]";
            else return $"Person: {Person.Name} [Klammer {Person.Klammer}]";
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Person == null) return;
            if (AlsPersonalklammer) filter.Add("PSKlammer", Person.Klammer);
            else filter.Add("PSNummer", Person.Nummer);
        }


        public Task LoadAsync(CancellationToken ct = default(CancellationToken))
        {

            PsSearchProvider.SetItems(store?.Personalnummerliste);
            return Task.FromResult(true);
        }


        public void Unload()
        {
            PsSearchProvider.ClearItems();
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private PersonalFilterModel person;
        public PersonalFilterModel Person
        {
            get { return person; }
            set { SetProperty(ref person, value); }
        }


        private bool alsPersonalnummer;
        public bool AlsPersonalnummer
        {
            get { return alsPersonalnummer; }
            set { if (SetProperty(ref alsPersonalnummer, value) && value) AlsPersonalklammer = false; }
        }


        private bool alsPersonalklammer;
        public bool AlsPersonalklammer
        {
            get { return alsPersonalklammer; }
            set { if (SetProperty(ref alsPersonalklammer, value) && value) AlsPersonalnummer = false; }
        }


#endregion

    }
}
