﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class ArtikelDto : ChangeTrackingBase
    {

        public  String                      Name                    { get; set; }
        public  String                      Nummer                  { get; set; }
        public  String                      Beschreibung            { get; set; }
        public  String                      Herstellernummer        { get; set; }
        public  decimal?                    Mischpreis              { get; set; }
        public  int?                        Klasse                  { get; set; }
        public  decimal?                    Mindestbestand          { get; set; }
        public  decimal?                    Mindestbestellmenge     { get; set; }
        public  decimal?                    Bestellmenge            { get; set; }
        public  decimal?                    ReservierteMenge        { get; set; }
        public  decimal?                    BestandReal             { get; set; }
        public  decimal?                    BestandVirtuell         { get; set; }
        public  DateTime?                   DeaktiviertAm           { get; set; }

        public  NummerFilterModel           Einheit                 { get; set; }
        public  NummerFilterModel           Warengruppe             { get; set; }
        public  SpecFilterModel             Spezifikation           { get; set; }

        public  ICollection<ArtikelLfMap>   MaterialLieferantMap    { get; set; }
        public  ICollection<ArtikelIhoMap>  MaterialIHObjektMap     { get; set; }
        public  ICollection<WLInfoDto>      Einlagerungen           { get; set; }
        public  IEnumerable<TechWertDto>    TechnischeWerte         { get; set; }
    }
}
