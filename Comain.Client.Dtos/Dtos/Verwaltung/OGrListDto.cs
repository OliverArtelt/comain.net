﻿using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class OGrListDto
    {

        public IList<ObjektgruppeDto>   Mainlist    { get; set; }
        public IList<ObjektartDto>      Sublist     { get; set; }
    }
}
