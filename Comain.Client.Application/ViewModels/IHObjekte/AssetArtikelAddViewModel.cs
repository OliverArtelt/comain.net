﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Specs;
using Comain.Client.Models.Stock;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Specs;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class AssetArtikelAddViewModel : ViewModel<IAssetArtikelAddView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public AssetArtikelAddViewModel(IAssetArtikelAddView view)
          : base(view)
        {
            SearchCommand = new DelegateCommand(Refresh);
        }


        public void Unload()
        {
            Foundlist = null;
        }


        public IEnumerable<MaterialstammFilterModel> GetSelected()
        {

            if (Foundlist == null) yield break;
            foreach (ArtikelAddItem at in (CollectionView)Foundlist) if (at.IsSelected) yield return at.Model;
        }


        public void LoadMaterial(List<MaterialstammFilterModel> mtlist)
        {

            if (mtlist == null) {

                Foundlist = null;
                return;
            }

            Foundlist = new CollectionViewSource { Source = mtlist.Select(p => new ArtikelAddItem(p)) }.View;
            Foundlist.Filter = Predicate;
            Foundlist.SortDescriptions.Clear();
            Foundlist.SortDescriptions.Add(new SortDescription { PropertyName="Name" });

            Refresh();
        }


        private bool Predicate(object arg)
        {

            var at = arg as ArtikelAddItem;
            if (at == null) return false;
            if (at.Model.DeaktiviertAm.HasValue) return false;
            if (String.IsNullOrEmpty(searchText)) return true;

            return at.Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase) ||
                   at.Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase);
        }


        private void Refresh()
        {

            if (Foundlist == null) return;
            Foundlist?.Refresh();
            RaisePropertyChanged(nameof(Foundlist));
        }


#region P R O P E R T I E S


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


        public ICommand SearchCommand   { get; private set; }


        private ICollectionView foundlist;
        public ICollectionView Foundlist
        {
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private String searchText;
        public String SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


#endregion


    }
}
