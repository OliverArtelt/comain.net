﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.Anwesenheiten
{

    public class AssignedPSRequest
    {

        public int         Asset_Id        { get; set; }
        public DateTime    Datum           { get; set; }
        public TimeSpan?   UhrzeitVon      { get; set; }
        public bool        MitMassnahmen   { get; set; }
    }
}
