﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHDeleteDto
    {

        public bool          IsDeleteable    { get; set; }
        public List<String>  Reasons         { get; set; }
    }
}
