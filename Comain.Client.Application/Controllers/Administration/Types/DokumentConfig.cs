﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Administration.Types
{

    public class DokumentConfig : IAdminTyp
    {

        public string       Title           => "Dokumente verwalten";
        public int          Sortierung      => 80000;
        public Type         Controller      => typeof(IConfigController<DokumentViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
