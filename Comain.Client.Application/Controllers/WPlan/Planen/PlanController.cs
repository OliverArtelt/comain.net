﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;
using CO.OpenXML.XLSX;
using Comain.Client.Controllers;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.Services.WPlan;
using Comain.Client.ViewData.WPlan;
using Comain.Client.ViewModels.WPlan.Planen;
using Microsoft.Win32;
using Comain.Client.ViewModels.WPlan.Reports;

namespace Comain.Client.Controllers.WPlan.Planen
{

    public interface IPlanController : IController
    {
        Task<Plan> LoadPlanAsync();
    }


    public class PlanController : ControllerBase, IPlanController
    {

        private readonly String[] filterChangedProps = new String[] { "Von", "Bis", "Leistungsart", "Gewerk", "JahrP0", "Personal" };


        private readonly IBreadcrumbService crumbs;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IPlanService store;
        private readonly IPlanTableBuilder builder;
        private readonly IStyleFactory styles;
        private readonly MainViewModel mainView;
        private readonly PlanViewModel view;
        private readonly PlanTreeViewModel treeView;
        private readonly PlanFilterViewModel filterView;
        private readonly PlanDetailViewModel detailView;
        private readonly PlanDetailReportViewModel reportView;
        private readonly PlanDetailDashboardViewModel dashboardView;
        private readonly PlanDetailCreateViewModel createView;
        private readonly ProgressViewModel progressView;
        private Planstatistik stats;
        private Plan plan;
        private CancellationTokenSource pollTokenSource = new CancellationTokenSource();
        private Dictionary<int, PersonalFilterModel> psdic;
        private SerializableCommand planCommmand;


        public PlanController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                              IControllerBus bus, IKonfigService configs, IPlanService store, IPlanTableBuilder builder, IStyleFactory styles,
                              MainViewModel mainView, PlanViewModel view, PlanTreeViewModel treeView, PlanFilterViewModel filterView,
                              PlanDetailViewModel detailView, PlanDetailReportViewModel reportView, PlanDetailDashboardViewModel dashboardView,
                              PlanDetailCreateViewModel createView, ProgressViewModel progressView)

          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.configs = configs;
            this.store = store;
            this.builder = builder;
            this.styles = styles;

            this.mainView = mainView;
            this.view = view;
            this.treeView = treeView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.reportView = reportView;
            this.dashboardView = dashboardView;
            this.createView = createView;
            this.progressView = progressView;
        }


        public void Initialize()
        {

            view.DetailView = detailView.View;
            view.TreeView = treeView.View;
            view.FilterView = filterView.View;
            detailView.ReportView = reportView.View;
            detailView.DashboardView = dashboardView.View;
            detailView.CreateView = createView.View;

            createView.TestCreateCommand = CreateCommand(_ => TestCreatePlanAsync());
            createView.CreateCommand = CreateCommand(_ => CreatePlanAsync());

            reportView.PlanCommand = planCommmand = CreateCommand(_ => OpenPlanAsync());
            reportView.ExecuteCommand = CreateCommand(_ => OutputAsync());
            filterView.PlanCommand = planCommmand;
            dashboardView.PlanCommand = planCommmand;
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());
            view.BackCommand = new AsyncDelegateCommand(_ => bus.BackAsync());
            progressView.CloseCommand = new AsyncDelegateCommand(_ => CloseCreateAsync());

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(reportView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(filterView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(treeView,   ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            await UnloadImplAsync();

            crumbs.Add("Wartungsplanung");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            detailView.CreateIsVisible = login.AktuellerNutzer.IstAdministrator || login.AktuellerNutzer.IstInstandhaltungLeitung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(treeView,   ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(filterView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(reportView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
        }


        protected override Task<bool> UnloadImplAsync()
        {

            view.Unload();
            treeView.Unload();
            filterView.Unload();
            detailView.Unload();
            dashboardView.Unload();
            createView.Unload();
            reportView.Unload();
            psdic = null;
            plan  = null;
            stats = null;
            bus.GetController<ITafelController>()?.Unload();

            return Task.FromResult(true);
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (sender == detailView && args.PropertyName == nameof(detailView.CurrentTab)) await UpdateReportViewAsync();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private async Task LoadAsync()
        {

            view.Unload();

            using (new VisualDelay(view)) {

                var ihTask = store.IHTreeAsync();
                var gwTask = store.FilterGewerkeAsync();
                var laTask = store.FilterLeistungsartenAsync();
                var psTask = store.FilterPersonalAsync();

                await Task.WhenAll(ihTask, gwTask, laTask, psTask).ConfigureAwait(false);

                psdic = psTask.Result.ToDictionary(p => p.Id);
                await filterView.LoadAsync(gwTask.Result, laTask.Result, psTask.Result);
                await treeView.LoadAsync(ihTask.Result);
            }
        }


        private async Task SearchAsync()
        {

            await LoadPlanAsync();
            await UpdateReportViewAsync();
            TestCreatePlanAsync();
            dashboardView.Load(plan);
        }


        public async Task<Plan> LoadPlanAsync()
        {

            filterView.ValidateAndThrow();

            plan = null;
            stats = null;
            view.Unload();
            reportView.Unload();
            var filter = filterView.GebeFilter();

            using (new VisualDelay(view)) {

                stats = await store.GetStatsAsync(filter, bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                var plntask = store.GetPlanAsync(filterView.GebeFilter(), bus.Token);
                var cfgtask = configs.GetAsync(bus.Token);
                await Task.WhenAll(plntask, cfgtask).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                plan = builder.BuildPlan(filter, plntask.Result, cfgtask.Result);
                plan.Statistik = stats;
                plan.Personalverzeichnis = psdic;
            }

            return plan;
        }


        private async Task OpenPlanAsync()
        {

            if (plan == null) await SearchAsync();
            await bus.JumpAsync(typeof(ITafelController), plan);
        }


        private async Task OutputAsync()
        {

            if (reportView.CurrentReport == null) return;
            if (reportView.CurrentReport.BenötigtPlan && plan == null) await SearchAsync();

            if (reportView.CurrentReport is IPlanExport) await ExportAsync();
            else await UpdateReportViewAsync();
        }


        private async Task ExportAsync()
        {

            var export = reportView.CurrentReport as IPlanExport;
            var exporter = export?.Exporter;
            if (exporter == null) return;
            exporter.SetStyles(styles);
            exporter.SetOptions(reportView.GebeOptions());

            var dialog = new SaveFileDialog();
            dialog.Filter = "Excel Tabelle (*.xlsx)|*.xlsx";
            dialog.FileName = $"{export.Name} {plan.Name}";
            dialog.Title = "Speicherort wählen";
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileName.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                var bw = new WorkbookWriter(dialog.FileName);
                WorksheetWriter sw = bw.OpenSheet(1);
                sw.SetName(export.Name);

                exporter.SetWriter(sw);
                plan.Accept(exporter);

                sw.Save();
                bw.WriteDocument();
            }
        }


        private async Task UpdateReportViewAsync()
        {

            reportView.Unload();
            if (detailView.Tab != PlanDetailViewModel.Tabs.Report) return;
            var report = reportView.CurrentReport as IPlanReport;
            if (report == null) return;

            using (new VisualDelay(reportView)) {

                await reportView.BindViewerAsync(await report.GetReportAsync(plan, filterView.GebeFilter(), reportView.GebeOptions(), bus.Token));
                reportView.GoToReport();
            }
        }


        private async Task TestCreatePlanAsync()
        {

            if (detailView.Tab != PlanDetailViewModel.Tabs.Create) return;

            using (new VisualDelay(createView)) {

                var orphaned = await store.CountOrphanedAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                createView.Auftragsanzahl = orphaned.Aufträge;
                createView.Positionsanzahl = orphaned.Checklistpositionen;
            }
        }


        private async Task CreatePlanAsync()
        {

            progressView.Reset();
            detailView.CreateView = progressView.View;

            var sessionId = await store.CreateAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
            bus.Token.ThrowIfCancellationRequested();

            pollTokenSource = CancellationTokenSource.CreateLinkedTokenSource(bus.Token);
            await progressView.PollAsync(sessionId, pollTokenSource);
            pollTokenSource.Cancel();

            await SearchAsync();
        }


        private async Task CloseCreateAsync()
        {
            detailView.CreateView = createView.View;
        }


        private void ClearCurrentPlan()
        {

            dashboardView.Unload();
            reportView.Unload();
            createView.Unload();
        }
    }
}
