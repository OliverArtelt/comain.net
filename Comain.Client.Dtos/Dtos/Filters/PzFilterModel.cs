﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class PzFilterModel : EntityModelBase, IComparable<PzFilterModel>
    {

        public int      Nummer      { get; set; }
        public String   Name        { get; set; }
        public String   Kurzname    { get; set; }

        
        public int CompareTo(PzFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareTo(other.Nummer);
        }
    }
}
