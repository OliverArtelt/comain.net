﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Aufträge;


namespace Comain.Client.Views.Aufträge
{

    public partial class AuftragDetailLeistungView : UserControl, IAuftragDetailLeistungView
    {


        public AuftragDetailLeistungView()
        {
            InitializeComponent();
        }


        private void LeMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            var ctx = DataContext as AuftragDetailLeistungViewModel;
            ctx.LEEditCommand.Execute(null);
        }


        private void MtMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            var ctx = DataContext as AuftragDetailLeistungViewModel;
            ctx.MTEditCommand.Execute(null);
        }
    }
}
