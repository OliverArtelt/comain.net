﻿using System;
using System.ComponentModel;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketListViewItem : NotifyBase
    {
       
        private String          dringlichkeit;
        private int             drposition;
        private String          typ;
        private Ticketstatus    status;
        private String          nummer;
        private String          kurzbeschreibung;
        private DateTime        gemeldetAm;
        private String          iHObjektNummer;
        private String          maschine;
        private String          auftragnehmer;
        private String          auftraggeber;
        private DateTime?       termin;
        private String          kostenstelle;
        private String          gewerk;


        public int Id      { get; set; }


        public String Dringlichkeit     
        {           
            get { return dringlichkeit; }
            set { SetProperty(ref dringlichkeit, value); }
        }

        public int DrPosition     
        {           
            get { return drposition; }
            set { if (SetProperty(ref drposition, value)) RaisePropertyChanged(nameof(DBackground)); }
        }
  
        public String Typ     
        {           
            get { return typ; }
            set { SetProperty(ref typ, value); }
        }

        public Ticketstatus Status            
        {           
            get { return status; }
            set { SetProperty(ref status, value); }
        }

        public String Nummer 
        {           
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        public String Kurzbeschreibung  
        {           
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }

        public DateTime GemeldetAm     
        {           
            get { return gemeldetAm; }
            set { SetProperty(ref gemeldetAm, value); }
        }

        public String IHObjektNummer    
        {           
            get { return iHObjektNummer; }
            set { SetProperty(ref iHObjektNummer, value); }
        }

        public String Maschine          
        {           
            get { return maschine; }
            set { SetProperty(ref maschine, value); }
        }

        public String Auftraggeber      
        {           
            get { return auftraggeber; }
            set { SetProperty(ref auftraggeber, value); }
        }

        public String Auftragnehmer      
        {           
            get { return auftragnehmer; }
            set { SetProperty(ref auftragnehmer, value); }
        }

        public DateTime? Termin            
        {           
            get { return termin; }
            set { SetProperty(ref termin, value); }
        }

        public String Kostenstelle      
        {           
            get { return kostenstelle; }
            set { SetProperty(ref kostenstelle, value); }
        }

        public String Gewerk      
        {           
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }

        public String DBackground
        {
            get {
            
                if (Status == Ticketstatus.Abgeschlossen || Status == Ticketstatus.Storniert) return "Gray";
                return Comain.Client.Models.Aufträge.Dringlichkeit.GetBackgroundColor(DrPosition); 
            }
        }
        
        public String DForeground
        {
            get {
            
                if (Status == Ticketstatus.Abgeschlossen || Status == Ticketstatus.Storniert) return "Gray";
                if (Dringlichkeit == null) return "Black";
                return Comain.Client.Models.Aufträge.Dringlichkeit.GetForegroundColor(DrPosition); 
            }
        }


        public String StatusAsText { get { return Status.StatusAsText(); } }
             

        private Ticket model; 
        public Ticket Model 
        {
            get { return model; }
            set {
            
                if (value != model) {

                    if (model != null) PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    model = value;
                    if (model != null) PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                    Emboss();
                }
            }
        }      
               
        
        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case nameof(Dringlichkeit):
            
                Dringlichkeit = model.Dringlichkeit?.Nummer;   
                if (model.Dringlichkeit != null) DrPosition = model.Dringlichkeit.Position;
                
                RaisePropertyChanged(nameof(Dringlichkeit));
                RaisePropertyChanged(nameof(DrPosition));
                RaisePropertyChanged(nameof(DBackground));
                RaisePropertyChanged(nameof(DForeground));
                break;

            case nameof(Status):
            
                Status = model.Status;
                RaisePropertyChanged(nameof(Status));
                break;

            case nameof(Nummer):
            
                Nummer = model.Nummer;
                RaisePropertyChanged(nameof(Nummer));
                break;

            case nameof(Kurzbeschreibung):
            
                Kurzbeschreibung = model.Kurzbeschreibung.RemoveNewlines();
                RaisePropertyChanged(nameof(Kurzbeschreibung));
                break;

            case nameof(GemeldetAm):
            
                GemeldetAm = model.GemeldetAm;
                RaisePropertyChanged(nameof(GemeldetAm));
                break;

            case nameof(Ticket.Fertigstellungstermin):
            
                Termin = model.Fertigstellungstermin;
                RaisePropertyChanged(nameof(Termin));
                break;

            case nameof(Ticket.AuIHKS):
            
                IHObjektNummer = model.AuIHKS?.IHObjekt;  
                Maschine       = model.AuIHKS?.Baugruppenpfad;
                RaisePropertyChanged(nameof(IHObjektNummer));
                RaisePropertyChanged(nameof(Maschine));
                break;

            case nameof(Auftraggeber):
            
                Auftraggeber = model.Auftraggeber?.NameNummer; 
                RaisePropertyChanged(nameof(Auftraggeber));
                break;

            case nameof(Auftragnehmer):
            
                Auftragnehmer = model.Auftragnehmer?.NameNummer; 
                RaisePropertyChanged(nameof(Auftragnehmer));
                break;

            case nameof(Kostenstelle):
            
                Kostenstelle = model.AuIHKS?.Kostenstelle; 
                RaisePropertyChanged(nameof(Kostenstelle));
                break;

            case nameof(Gewerk):
            
                Gewerk = model.Gewerk?.Nummer;  
                RaisePropertyChanged(nameof(Gewerk));
                break;
            }
        }


        public TicketListViewItem()
        {}


        public TicketListViewItem(Ticket model)
        {

            Model = model;
            Emboss();
        }


        public TicketListViewItem(TicketListDto model)
        {

            Id               = model.Id;              
            Dringlichkeit    = model.Dringlichkeit; 
            DrPosition       = model.DrPosition;
            Status           = (Ticketstatus)model.Status;          
            Typ              = ((Auftragstyp)model.Typ).AsText();
            Nummer           = model.Nummer;          
            Kurzbeschreibung = model.Kurzbeschreibung.RemoveNewlines();
            GemeldetAm       = model.GemeldetAm;   
            IHObjektNummer   = model.IHObjektNummer;  
            Maschine         = model.Maschine;        
            Auftraggeber     = model.Auftraggeber;    
            Auftragnehmer    = model.Auftragnehmer;   
            Termin           = model.Termin;          
            Kostenstelle     = model.Kostenstelle;    
            Gewerk           = model.Gewerk;          
        }


        public void Emboss()
        {
        
            Id = model.Id;
            
            Dringlichkeit    = model.Dringlichkeit?.Nummer;   
            if (model.Dringlichkeit != null) DrPosition = model.Dringlichkeit.Position;
            Status           = model.Status;          
            Nummer           = model.Nummer; 
            Typ              = ((Auftragstyp)model.Typ).AsText();
            Kurzbeschreibung = model.Kurzbeschreibung.RemoveNewlines();
            GemeldetAm       = model.GemeldetAm;   
            IHObjektNummer   = model.AuIHKS?.IHObjekt;  
            if (model.AuIHKS?.Baugruppenpfad != null) Maschine = model.AuIHKS?.Baugruppenpfad;       
            Auftraggeber     = model.Auftraggeber?.Name;    
            Auftragnehmer    = model.Auftragnehmer?.Name;   
            Termin           = model.Fertigstellungstermin;          
            Kostenstelle     = model.AuIHKS?.Kostenstelle;    
            Gewerk           = model.Gewerk?.Nummer;     
        }
    }
}
