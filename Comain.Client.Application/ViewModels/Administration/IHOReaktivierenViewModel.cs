﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Views.Administration;


namespace Comain.Client.ViewModels.Administration
{

    public class IHOReaktivierenViewModel : ViewModel<IIHOReaktivierenView>, IDelayableViewModel
    {

        public IHOReaktivierenViewModel(IIHOReaktivierenView view) : base(view)
        {
        }


        public void Unload()
        {
            List = null;
        }


#region P R O P E R T I E S
   

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private IEnumerable<IHReactivateItem> list;
        public IEnumerable<IHReactivateItem> List
        { 
            get { return list; }
            set { SetProperty(ref list, value); }
        }


#endregion

    }
}
