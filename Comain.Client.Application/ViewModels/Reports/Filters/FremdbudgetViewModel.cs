﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class FremdbudgetViewModel : ViewModel<IFremdbudgetView>, IFilterViewModel
    {

        public FremdbudgetViewModel(IFremdbudgetView view) : base(view)
        {}


        public void Reset()
        {
            InFbYes = true;
            InFbNo = true;
        }


        public String Validiere()
        {
            return null;
        }


        public String FilterAlsString()
        {
            
            if (InFbYes == InFbNo) return String.Empty;
            if (InFbYes) return "Im Budget enthaltene FB-Nummern";
            return "Nicht im Budget enthaltene FB-Nummern";
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("InFbYes", InFbYes);
            filter.Add("InFbNo", InFbNo);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);     
        }
        

        public void Unload()
        {}


#region P R O P E R T I E S
   

        private bool inFbYes = true;
        public bool InFbYes
        { 
            get { return inFbYes; }
            set { SetProperty(ref inFbYes, value); }
        } 


        private bool inFbNo = true;
        public bool InFbNo
        { 
            get { return inFbNo; }
            set { SetProperty(ref inFbNo, value); }
        } 
        

#endregion

    }
}
