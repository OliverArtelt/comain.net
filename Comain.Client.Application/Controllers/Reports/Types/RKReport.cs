﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Reklamationen;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class RKReport : ReportBase, IReport
    {

        private readonly DisplayFilterViewModel displayView;
        private readonly IReklamService store;


        public RKReport(DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, BSFilterViewModel bsView, LSFilterViewModel lsView, 
                        ZeitViewModel zeitView, IReklamService store)
        {

            this.displayView = displayView;
            this.store = store;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, bsView, lsView, zeitView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }
 

        public override string Title
        {
            get { return "Reklamation"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 5000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.ReklamationExport.rdlc": "Comain.Client.Reports.Stock.ReklamationReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.ReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
