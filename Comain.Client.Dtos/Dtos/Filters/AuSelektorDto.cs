﻿using Comain.Client.Dtos.Filters;

namespace Comain.Client.Dtos.Filters
{

    public class AuSelektorDto : NummerFilterModel
    {
        
        public bool  IstOffen            { get; set; }
        public int?  Kostenstelle_Id     { get; set; }
    }
}
