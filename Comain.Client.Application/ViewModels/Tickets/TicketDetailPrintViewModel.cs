﻿using Comain.Client.Services;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Tickets
{
    
    public class TicketDetailPrintViewModel : PrintViewModel<ITicketDetailPrintView>, IDelayableViewModel
    {

        public TicketDetailPrintViewModel(IMessageService messages, ITicketDetailPrintView view) : base(messages, view)
        {
        }
    }
}
