﻿using System.Windows.Controls;
using Comain.Client.Views.Reports;

namespace Comain.Client.Views.Stock.Bestellungen
{
    /// <summary>
    /// Interaction logic for BestellDetailReportView.xaml
    /// </summary>
    public partial class BestellDetailReportView:UserControl  ,IBestellDetailReportView
    {
        public BestellDetailReportView()
        {
        InitializeComponent();
        }
    }
}
