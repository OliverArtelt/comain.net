﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;

namespace Comain.Client.ReportModels.WPlan
{
    
    /// <summary>
    /// Ausgabeoptionen (Preisspalten exportieren, Summen angeben, nur relevante Massnahmen ...
    /// </summary>
    public class PlanOptions
    {

        public bool    Stunden              { get; set; }
        public bool    Preise               { get; set; }
        public bool    Gruppensummen        { get; set; }

        public bool    Planleistungen       { get; set; }
        public bool    Sonderleistungen     { get; set; }
        public bool    Bedarfspositionen    { get; set; }

        public bool    Erledigt             { get; set; }
        public bool    Offen                { get; set; }
        public bool    NurRelevante         { get; set; }
    }
}
