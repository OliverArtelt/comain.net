﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class BestellListViewItem : NotifyBase
    {
      
        private String       nummer;        
        private DateTime?    ausgangAm;     
        private String       lieferant;     
        private DateTime?    liefertermin;  
        private String       status;        
        private bool         reklamiert;    
        private decimal?     gesamtpreis;   


        public int Id  { get; set; }           
        
                 
        public String Nummer        
        {           
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        public DateTime? AusgangAm     
        {           
            get { return ausgangAm; }
            set { SetProperty(ref ausgangAm, value); }
        }

        public String Lieferant     
        {           
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }

        public DateTime? Liefertermin  
        {           
            get { return liefertermin; }
            set { SetProperty(ref liefertermin, value); }
        }

        public String Status        
        {           
            get { return status; }
            set { if (SetProperty(ref status, value)) RaisePropertyChanged("Color"); }
        }

        public bool Reklamiert    
        {           
            get { return reklamiert; }
            set { if (SetProperty(ref reklamiert, value)) RaisePropertyChanged("ReklamiertText"); }
        }

        public decimal? Gesamtpreis   
        {           
            get { return gesamtpreis; }
            set { SetProperty(ref gesamtpreis, value); }
        }

       
        public String ReklamiertText { get { return Reklamiert? "Ja": "Nein"; } }
       
       
        public String Color 
        { 
            get { 

                switch (Status) {
                
                    case "Abgeschlossen":   return "LimeGreen";
                    case "Offen":           return "Gold";
                    case "Angelegt":        return "DodgerBlue";
                    case "Überfällig":      return "Coral";
                    case "Abschlussfähig":  return "MediumPurple";
                    case "Storniert":       return "LightSlateGray";
                    default:                return "White";
                } 
            } 
        }
             
             
        private Bestellung model; 
        public Bestellung Model 
        {
            get { return model; }
            set {
            
                if (value != model) {

                    if (model != null) {
                    
                        PropertyChangedEventManager.RemoveHandler(model.Positionen, ViewPropertyChanged, "");
                        PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    }

                    model = value;

                    if (model != null) {
                    
                        PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                        PropertyChangedEventManager.AddHandler(model.Positionen, ViewPropertyChanged, "");
                        Emboss();                
                    }            
                }
            }
        }      
               
        
        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case "Nummer":
                
                Nummer = model.Nummer;
                RaisePropertyChanged("Nummer");
                break;

            case "AusgangAm":
                
                AusgangAm = model.DatumAusgang;
                RaisePropertyChanged("AusgangAm");
                break;

            case "Lieferant":
                
                Lieferant = null;
                if (model.Lieferant != null) Lieferant = model.Lieferant.NameUndOrt;
                RaisePropertyChanged("Lieferant");
                break;

            case "Liefertermin":
                
                Liefertermin = model.Termin;
                RaisePropertyChanged("Liefertermin");
                break;

            case "Status":
                
                Status = model.Status.ToString();
                RaisePropertyChanged("Status");
                break;

            case "Reklamiert":
                
                Reklamiert = !model.Reklamationen.IsNullOrEmpty();
                RaisePropertyChanged("Reklamiert");
                RaisePropertyChanged("ReklamiertText");
                break;

            case "GPreis":
            case "Total":
                
                Gesamtpreis = model.GPreis;
                RaisePropertyChanged("Gesamtpreis");
                break;
            }
        }


        public BestellListViewItem()
        {}


        public BestellListViewItem(Bestellung model)
        {
         
            Model = model;
            Emboss();
        }


        public BestellListViewItem(BestellListDto dto)
        {

            Id               = dto.Id;
            Nummer           = dto.Nummer;       
            AusgangAm        = dto.AusgangAm;   
            Lieferant        = dto.Lieferant;   
            Liefertermin     = dto.Liefertermin;
            Status           = dto.Status;      
            Reklamiert       = dto.Reklamiert;  
            Gesamtpreis      = dto.Gesamtpreis;  
        }


        public void Emboss()
        {
        
            Id               = model.Id;
            Nummer           = model.Nummer;
            AusgangAm        = model.DatumAusgang;   
            Lieferant        = model.Lieferant.NameUndOrt;   
            Liefertermin     = model.Termin;
            Status           = model.Status.ToString();      
            Reklamiert       = !model.Reklamationen.IsNullOrEmpty();  
            Gesamtpreis      = model.GPreis;  
        }  
    }
}
