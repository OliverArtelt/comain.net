﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData.WPlan
{

    public class IHFilterItem : NotifyBase
    {

        public int      Id              { get; set; }
        public String   Nummer          { get; set; }
        public String   Name            { get; set; }
        public int?     VerantwProd     { get; set; }
        public int?     VerantwIH       { get; set; }


        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set { SetProperty(ref isChecked, value); }
        }

      
        public bool IsExpanded          { get; set; }
    }
}
