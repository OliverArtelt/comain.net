﻿using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.Stock
{

    public class ArtikelIhoMap : ChangeTrackingBase
    {
       
        public int                  Position        { get; set; }
        public int                  Material_Id     { get; set; }

        public IHObjektFilterModel  IHObjekt        { get; set; }
    }
}
