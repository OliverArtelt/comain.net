﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
   
    public enum NeuSelektorMode
    {
        
        Undefiniert, 
        /// <summary>
        /// Wählen Sie einen Lieferanten und mindestens ein Artikel.
        /// </summary> 
        Frei,
        /// <summary>
        /// Wählen Sie die zu bestellenden Artikel, es werden die Lieferanten angezeigt welche die gewählten Artikel führen.
        /// </summary> 
        Artikel, 
        /// <summary>
        /// Wählen Sie den Lieferanten, es werden alle seine Artikel angezeigt.
        /// </summary> 
        Lieferant
    }
}
