﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels;


namespace Comain.Client.Services.IHObjekte
{

    public interface IIHObjektService
    {

        Task<IList<IHListDto>>                      IHObjekteAsync(CancellationToken ct = default);
        Task<IHObjekt>                              DetailAsync(int id, CancellationToken ct = default);

        Task<IHObjekt>                              SaveAsync(IHObjekt model, CancellationToken ct = default);
        Task<IHDeleteDto>                           DeleteKonsequenzenAsync(IHObjekt model, CancellationToken ct = default);
        Task                                        DeleteAsync(IHObjekt model, CancellationToken ct = default);

        Task<IList<PzFilterModel>>                  StandorteAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>                 KostenstellenAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>              ObjektgruppenAsync(CancellationToken ct = default);
        Task<IList<SubNummerFilterModel>>           ObjektartenAsync(CancellationToken ct = default);
        Task<IList<LieferantFilterModel>>           LieferantenAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>            PersonalAsync(CancellationToken ct = default);
        Task<IList<SpecFilterModel>>                SpecsAsync(CancellationToken ct = default);
        Task<List<MaterialstammFilterModel>>        MaterialstammAsync(CancellationToken ct = default);

        Task<IHMoveInfo>                            MoveInfoAsync(IHMoveRequestDto dto, CancellationToken ct = default);
        Task                                        MoveAsync(IHMoveInstruction request, CancellationToken ct = default);
        Task<IEnumerable<IHEreignisReportModel>>    EreignisseAsync(int id, bool withSub, CancellationToken ct = default);
        Task                                        SelfServiceAsync(IEnumerable<int> mnidlist, CancellationToken ct = default);
    }
}
