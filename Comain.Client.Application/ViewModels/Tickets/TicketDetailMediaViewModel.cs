﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailMediaViewModel : ViewModel<ITicketDetailMediaView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;

 
        public TicketDetailMediaViewModel(ITicketDetailMediaView view) : base(view)
        {
            context = SynchronizationContext.Current;
        } 


        public void Unload()
        {
            Medien = null;
        }
 

#region P R O P E R T I E S


        private ObservableCollection<Media> medien;
        public ObservableCollection<Media> Medien
        { 
            get => medien; 
            set => SetProperty(ref medien, value); 
        }


        private Media currentMedia;
        public Media CurrentMedia
        { 
            get { return currentMedia; }
            set { SetProperty(ref currentMedia, value); }
        }
 
       
        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }
 
       
        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }   
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
