﻿using Comain.Client.Services;
using Comain.Client.Views.Stock.Inventuren;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Inventuren
{

    public class InventurReportViewModel : PrintViewModel<IInventurReportView>, IDelayableViewModel
    {

        public InventurReportViewModel(IMessageService messages, IInventurReportView view) : base(messages, view)
        {
        }
    }
}
