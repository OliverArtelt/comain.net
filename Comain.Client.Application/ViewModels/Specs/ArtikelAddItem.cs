﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.ViewModels.Specs
{

    public class ArtikelAddItem : NotifyBase
    {

        public MaterialstammFilterModel Model { get; }


        public ArtikelAddItem(MaterialstammFilterModel model)
            => Model = model;


        public String    Name              => Model.Name;
        public String    Nummer            => Model.Nummer;
        public String    Beschreibung      => Model.Beschreibung;
        public decimal?  Mischpreis        => Model.Mischpreis;
        public String    Einheit           => Model.Einheit?.Name;
        public String    Warengruppe       => Model.Warengruppe;


        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value); 
        }
    }
}
