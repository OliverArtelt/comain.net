﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;

namespace Comain.Client.ViewModels.Stock.Reports
{

    public class LOFilterViewModel : ViewModel<ILOFilterView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public LOFilterViewModel(IFilterService store, ILOFilterView view) : base(view)
        {
            this.store = store;
        }


        public void Reset()
        {
            Lagerort = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (Lagerort != null) return "Lagerort " + Lagerort.Nummer;
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (Lagerort != null) filter.Add("Lagerort", Lagerort.Id);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            RaisePropertyChanged("Lagerorte");
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   
    
        public IList<LagerortDto> Lagerorte
        { 
            get { return store == null? null: store.Lagerorte; }
        } 
   

        private LagerortDto lagerort;
        public LagerortDto Lagerort
        { 
            get { return lagerort; }
            set { SetProperty(ref lagerort, value); }
        } 
     

#endregion

    }
}
