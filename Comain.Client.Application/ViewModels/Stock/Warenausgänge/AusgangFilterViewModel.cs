﻿using System;
using System.Collections;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Views.Stock.Warenausgänge;

namespace Comain.Client.ViewModels.Stock.Warenausgänge
{

    public class AusgangFilterViewModel : ViewModel<IAusgangFilterView>
    {

        private readonly SynchronizationContext context;


        public AusgangFilterViewModel(IAusgangFilterView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            
            if (Von.HasValue)         filter.Add("Von", Von);
            if (Bis.HasValue)         filter.Add("Bis", Bis);
            if (Lagerort != null)     filter.Add("Lagerort", Lagerort.Id);
            if (Kostenstelle != null) filter.Add("Kostenstelle", Kostenstelle.Id);
            if (SearchText != null)   filter.Add("SearchText", SearchText);

            return filter;
        }

 
        public void SetzeAnzahlGefunden(int count)
        {

            if (count == 0) ResultText = "Keinen Warenausgang gefunden.";
            else if (count == 1) ResultText = "Ein Warenausgang gefunden.";
            else ResultText = String.Format("{0} Warenausgänge gefunden.", count);
        }


        public void Unload()
        {

            Lagerorte = null;
            Kostenstellen = null;
        }


        public void Reset()
        {

            Von = DateTime.Today.AddMonths(-6);
            Bis = DateTime.Today;
            Lagerort = null;
            Kostenstelle = null;
            SearchText = null;
        }


#region P R O P E R T I E S


        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private String searchText;
        public String SearchText
        { 
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }


        private DateTime? von;
        public DateTime? Von
        { 
            get { return von; }
            set { SetProperty(ref von, value); }
        }


        private DateTime? bis;
        public DateTime? Bis
        { 
            get { return bis; }
            set { SetProperty(ref bis, value); }
        }


        private IEnumerable kostenstellen;
        public IEnumerable Kostenstellen
        { 
            get { return kostenstellen; }
            set { SetProperty(ref kostenstellen, value); }
        }


        private IEnumerable lagerorte;
        public IEnumerable Lagerorte
        { 
            get { return lagerorte; }
            set { SetProperty(ref lagerorte, value); }
        }


        private KstFilterModel kostenstelle;
        public KstFilterModel Kostenstelle
        { 
            get { return kostenstelle; }
            set { SetProperty(ref kostenstelle, value); }
        }


        private LagerortDto lagerort;
        public LagerortDto Lagerort
        { 
            get { return lagerort; }
            set { SetProperty(ref lagerort, value); }
        }


#endregion

    }
}
