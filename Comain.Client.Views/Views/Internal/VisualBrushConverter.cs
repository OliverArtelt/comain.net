﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Comain.Client.Views.Internal
{

    public class VisualBrushConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (String.IsNullOrWhiteSpace(value?.ToString())) return DependencyProperty.UnsetValue;
            var res = Application.Current.FindResource(value.ToString()) as Visual;
            return res;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
