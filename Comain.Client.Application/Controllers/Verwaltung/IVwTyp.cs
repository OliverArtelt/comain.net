﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung
{

    public interface IVwTyp
    {

        String      Title           { get; }
        int         Sortierung      { get; }
        Type        Controller      { get; }

        bool        HatZugriff(Nutzer user);
    }
}
