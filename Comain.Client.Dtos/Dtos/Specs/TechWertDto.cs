﻿using System;
using System.Collections.Generic;
using System.Text;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Specs
{
    public class TechWertDto : ChangeTrackingBase
    {

        public String       Name            { get; set; }
        public bool         Pflichtfeld     { get; set; }
        public String       Einheit         { get; set; }
        public int          Wertetyp        { get; set; }
        public String       Enumeration     { get; set; }
        public String       AssetTab        { get; set; }
        public String       ArtikelTab      { get; set; }

        public decimal?     Wert            { get; set; }
        public String       Text            { get; set; }
    }
}
