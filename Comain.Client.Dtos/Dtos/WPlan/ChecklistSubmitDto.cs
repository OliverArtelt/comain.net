﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.WPlan
{

    public class ChecklistSubmitDto : ChangeTrackingBase
    {

        public int                          Auftrag_Id              { get; set; }

        public int                          Ergebnis                { get; set; }
        public int?                         MinutenGearbeitet       { get; set; }
        public int?                         MengeErledigt           { get; set; }
        public String                       Bemerkungen             { get; set; }

        public List<PersonalFilterModel>    Personalliste           { get; set; }
    }
}
