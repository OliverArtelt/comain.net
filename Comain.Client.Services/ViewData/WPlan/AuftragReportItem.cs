﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan.Reports;

namespace Comain.Client.ViewData.WPlan
{
   
    public class AuftragReportItem 
    {

        public int          SortIndex               { get; set; }

        public String       Auftragsnummer          { get; set; }
        public String       Title                   { get; set; }
        public DateTime?    Termin                  { get; set; }
        public String       IHObjekt                { get; set; }
        public String       LeistungsartGewerk      { get; set; }
        public String       Positionsname           { get; set; }
        public String       Positionsbeschreibung   { get; set; }
        public int          Ergebnis                { get; set; }
        public bool         LetztePosition          { get; set; }

        public String       Werkzeuge               { get; set; }
        public String       Durchführung            { get; set; }
        public String       Sicherheit              { get; set; }
        public String       Material                { get; set; }

        public String       Abnahmeberechtigter     { get; set; }
        public String       Auftragsleiter          { get; set; }


        public AuftragReportItem()
        {}


        public AuftragReportItem(AuftragDruckDto au, PositionDruckDto pos, int ndx, int count)
        {

             if (au == null) throw new ComainOperationException("Es wurde kein Auftrag gewählt.");

             Auftragsnummer         = au.Nummer;
             Title                  = $"Wartungs- / Inspektionsauftrag{System.Environment.NewLine}{au.Nummer}";
             Termin                 = au.Termin;
             IHObjekt               = $"{au.IHObjektNummer} - {au.IHObjektPfad}";
             LeistungsartGewerk     = $"{au.Leistungsart} / {au.Gewerk}";

             if (pos == null) return;

             Positionsname          = $"Position {ndx + 1} von {count}:{System.Environment.NewLine}{pos.Name}";
             Positionsbeschreibung  = pos.Kurzbeschreibung;
             LetztePosition         = ndx + 1 == count;

             Werkzeuge              = pos.Werkzeuge;
             Durchführung           = pos.Durchführung;
             Sicherheit             = pos.Sicherheit;
             Abnahmeberechtigter    = au.Abnahmeberechtigter;
             Auftragsleiter         = au.Auftragsleiter;
             
             
             if (!pos.Materialien.IsNullOrEmpty()) {

                var bld = new StringBuilder();

                pos.Materialien.ForEach(p => {

                    if (p.Menge.HasValue) {

                        bld.Append(p.Menge.Value);
                        if (!String.IsNullOrEmpty(p.Einheit)) bld.Append(" ").Append(p.Einheit);
                        bld.Append(" ");
                    }

                    bld.Append(p.Nummer).Append(" ").AppendLine(p.Name);
                });

                Material = bld.ToString();
             }
        }
    }
}
