﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Excel-Textwörterbuch als Fliegengewichte implementiert
    /// </summary>
    public class StringPoolEncoder : StringPoolBase
    {

        int currentIndex;
        Dictionary<String, int> myDictionary;
        /// <summary>
        /// Anzahl Wörterbuchverknüpfungen (weiss der Geier wozu MSExcel diese Redundanz braucht)
        /// </summary>
        int count;

        
        /// <summary>
        /// leeres Wörterbuch einrichten
        /// </summary>
        public StringPoolEncoder()
        {
        
            count = 0;
            currentIndex = 0;
            myDictionary = new Dictionary<string,int>();
        }

        /// <summary>
        /// Wörterbuch aus Vorlage einrichten
        /// </summary>
        /// <param name="src">Wörterbuch als XML-Knoten</param>
        public StringPoolEncoder(String src) : this()
        {

            count = ParseMap(src);
        }

        /// <summary>
        /// Fliegengewichtsfabrik
        /// </summary>
        /// <param name="text">referenzierter Text</param>
        /// <returns>Index im Wörterbuch</returns>
        public int this[String text]
        {
       
            get {
             
                count++;

                int ndx;
                
                if (text == null) text = String.Empty;
                if (myDictionary.TryGetValue(text, out ndx)) return ndx;
                
                ndx = myDictionary.Count;
                myDictionary.Add(text, ndx);
                return ndx;
            }
        }
        

        protected override void Add(String key)
        {
        
            myDictionary.Add(key, currentIndex++);
        }


        /// <summary>
        /// Wörterbuch als sharedStrings-XML ausgeben
        /// </summary>
        /// <returns>Wörterbuch als XML-Knoten</returns>
        public String WriteXML()
        {

            StringBuilder bld = new StringBuilder();

            bld.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                     + "<sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" ")
               .Append(String.Format(@"count=""{0}"" ", count))
               .Append(String.Format(@"uniqueCount=""{0}"">", myDictionary.Count));

            //Umkopieren a.k.a. in Originalreihenfolge sortieren
            Dictionary<int, String> dic = new Dictionary<int,string>(myDictionary.Count);
            foreach (var entry in myDictionary) dic.Add(entry.Value, entry.Key);
            
            foreach (var entry in dic) {

                bld.Append("<si><t");

                //Strings mit führenden/abschließenden Leerzeichen nicht zerstören
                if (entry.Value.Trim() != entry.Value) {

                    bld.Append(@" xml:space=""preserve""");
                }

                bld.Append(">");
                bld.Append(entry.Value.EscapeXMLValue());
                bld.Append("</t></si>");
            }

            bld.Append("</sst>");
            return bld.ToString();
        }
    }
}
