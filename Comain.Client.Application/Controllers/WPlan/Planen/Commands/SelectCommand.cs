﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class SelectCommand : IUndoRedoCommand, IResultCommand
    {

        private List<Tuple<bool, Planaufgabe>> models;


        /// <remarks>
        /// wir müssen die Selektion umkehren können (redo / undo): nur die Objekte merken deren Selektstatus sich ändert
        /// [ADD]  selectState &&  addingToSelection: merkt sich neue Objekte die ausgewählt werden
        /// [SET]  selectState && !addingToSelection: neue Auswahl ersetzt alte: alte Objekte die nicht mehr dabei sind deselektieren, neu hinzukommende Objekte selektieren 
        /// [SUB] !selectState &&  addingToSelection: entfernt neue Objekte aus der Auswahl (deselektieren)
        /// [DEL] !selectState && !addingToSelection: komplette Auswahl verwerfen
        /// </remarks>
        public SelectCommand(IEnumerable<Planaufgabe> oldSelection, IEnumerable<Planaufgabe> newSelection, bool addingToSelection, bool selectState = true)
        {

            if (selectState && addingToSelection)
                models = newSelection.Except(oldSelection).Select(p => new Tuple<bool, Planaufgabe>(true, p)).ToList();

            if (selectState && !addingToSelection)
                models = oldSelection.Except(newSelection).Select(p => new Tuple<bool, Planaufgabe>(false, p))
                                     .Union(newSelection.Except(oldSelection).Select(p => new Tuple<bool, Planaufgabe>(true, p)))
                                     .ToList();

            if (!selectState && addingToSelection) 
                models = newSelection.Where(p => p.IstSelektiert).Select(p => new Tuple<bool, Planaufgabe>(false, p)).ToList(); 

            if (!selectState && !addingToSelection) 
                models = newSelection.Select(p => new Tuple<bool, Planaufgabe>(false, p)).ToList();
        }


        public void Redo() => models.ForEach(p => p.Item2.SetzeAuswahl( p.Item1));
        public void Undo() => models.ForEach(p => p.Item2.SetzeAuswahl(!p.Item1));


        public bool ModelHasChanged => false;


        public IEnumerable<PositionSubmitDto> Modifications() => new List<PositionSubmitDto>();
    }
}
