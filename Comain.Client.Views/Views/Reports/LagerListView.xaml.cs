﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Reports;

namespace Comain.Client.Views.Reports
{

    public partial class LagerListView : UserControl   ,ILagerListView
    {

        public LagerListView()
        {
            InitializeComponent();
        }
 

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as LagerListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
