﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Dokumente
{

    public class DokumentDto : ChangeTrackingBase
    {

        public String       Bezeichnung         { get; set; }
        public String       Beschreibung        { get; set; }
        public String       Filename            { get; set; }
        public long?        Filesize            { get; set; }
        public String       Typ                 { get; set; }
        public String       Url                 { get; set; }
        public String       PreviewUrl          { get; set; }

        public int          Quelltyp            { get; set; }
        public String       Quelle              { get; set; }
        public bool         IstDirektBezug      { get; set; }
        public bool         PräsentiertModell   { get; set; }

        public int          Version             { get; set; }
        public DateTime     ErstelltAm          { get; set; }
        public String       ErstelltVon         { get; set; }
        public DateTime     GeändertAm          { get; set; }
        public String       GeändertVon         { get; set; }
        public DateTime?    GeöffnetAm          { get; set; }

        public byte[]       Content             { get; set; }
        public byte[]       Preview             { get; set; }

        public String[]     Tags                { get; set; }
        public int[]        Kategorien          { get; set; }
    }
}
