﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.Services.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PlanDetailReport : IPlanReport
    {

        private readonly IPlanService store;

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Wartungsplan offen Detail";
        public int      Sortierung              => 300;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;


        public PlanDetailReport(IPlanService store)
            => this.store = store;


        public async Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo info, PlanOptions options, CancellationToken ct = default)
        {

            var report = new ReportResult { ReportAddress = "Comain.Client.Reports.BulkWartungsplan.rdlc",
                                            AssemblyAddress = "comain.Client.Reports",
                                            Title = "Wartungsplan", HasLogo = true };
            var data = await store.DetailReportAsync(model.OffeneAufträge.Select(p => p.AuftragId), ct).ConfigureAwait(false);
            report.AddDataSource("MainData", data);
            return report;
        }


        public String   Text                => @"
Der Bericht erzeugt die Auftragspapiere für die im gewählten Zeitraum geplanten und noch zu erledigenden Maßnahmen. Verwenden Sie diesen Bericht, wenn Sie Auftragspapiere mit der Angabe von Ersatzteilen und Material benötigen oder wenn Sie eine Rückmeldung im Detail auf Papier erwarten.
Wählen Sie die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein, Gehen Sie auf Suchen und betätigen Sie danach die Schaltfläche ""Wartungsplan offen Details"".
Bitte beachten Sie, es werden nur offene Positionen angezeigt. Bereits erledigte sehen Sie an anderen geeigneten Stellen im comain.";
    }
}
