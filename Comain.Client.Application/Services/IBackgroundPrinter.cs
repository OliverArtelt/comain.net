﻿using Comain.Client.ViewModels;

namespace Comain.Client.Services
{

    public interface IBackgroundPrinter
    {
        void Print(ReportResult reportData);
    }
}
