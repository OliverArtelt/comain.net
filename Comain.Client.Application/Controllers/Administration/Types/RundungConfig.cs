﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class RundungConfig : IAdminTyp
    {
        
        public string       Title           => "Runden von Gesamtpreisen";
        public int          Sortierung      => 16000;
        public Type         Controller      => typeof(IConfigController<RundenViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
