﻿using System;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Stock.Administration
{

    public class BestellViewModel : ValidatableViewModel<IBestellView, Konfiguration>, IConfigViewModel
    {

        public BestellViewModel(IBestellView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged("Bestellfuß");
            RaisePropertyChanged("Rechnungsanschrift");
            RaisePropertyChanged("Lieferanschrift");
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public String Bestellfuß
        {
            get { return Current == null? null: Current.Bestellfuß; }
            set {

                if (Current != null && Current.Bestellfuß != value) {

                    Current.Bestellfuß = value;
                    RaisePropertyChanged("Bestellfuß");
                }
            }
        }


        public String Rechnungsanschrift
        {
            get { return Current == null? null: Current.Rechnungsanschrift; }
            set {

                if (Current != null && Current.Rechnungsanschrift != value) {

                    Current.Rechnungsanschrift = value;
                    RaisePropertyChanged("Rechnungsanschrift");
                }
            }
        }


        public String Lieferanschrift
        {
            get { return Current == null? null: Current.Lieferanschrift; }
            set {

                if (Current != null && Current.Lieferanschrift != value) {

                    Current.Lieferanschrift = value;
                    RaisePropertyChanged("Lieferanschrift");
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
