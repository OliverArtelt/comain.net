﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Anwesenheiten;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class AnwesenheitZeitplanCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IZeitplanController);
        public String       Name        => "Zuordnung Anwesenheiten";
        public String       Parameter   => "Anwesenheit PS";
        public String       Gruppe      => "Aufträge";
        public int          Sortierung  => 5600;
        public String       Color       => "BlueViolet";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.calendar.week.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null || !module.VerwendetModulAnwesenheit) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungInstandhalter || nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
