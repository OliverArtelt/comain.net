﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Lieferanten : IVwTyp
    {
       
        public string       Title           => "Lieferanten";
        public int          Sortierung      => 2100;
        public Type         Controller      => typeof(ILieferantController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator;    
        }
    }
}
