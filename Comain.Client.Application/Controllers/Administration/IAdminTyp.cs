﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Administration
{

    public interface IAdminTyp
    {

        String      Title           { get; }
        int         Sortierung      { get; }
        Type        Controller      { get; }


        bool HatZugriff(ModulInfo module, Nutzer user);
    }
}
