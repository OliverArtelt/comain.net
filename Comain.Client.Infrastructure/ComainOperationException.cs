﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain
{
   
    [Serializable]
    public class ComainOperationException : UserException
    {

        public ComainOperationException(String message) : base(message)
        {}                           

        public ComainOperationException(String message, Exception source) : base(message, source)
        {}
    }
}
