﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.WPlan
{

    public class ChecklistPositionDto
    {

        public int                          Id                      { get; set; }
        public int                          Auftrag_Id              { get; set; }
        public int                          Massnahme_Id            { get; set; }

        public String                       Name                    { get; set; }
        public String                       Kurzbeschreibung        { get; set; }
        public String                       Qualifikation           { get; set; }
        public String                       Werkzeug                { get; set; }
        public String                       Durchführung            { get; set; }
        public String                       Sicherheit              { get; set; }
        public String                       Dokumentation           { get; set; }
        public String                       Massnahmenummer         { get; set; }
        public String                       ExterneAuftragsnummer   { get; set; }

        public bool                         MassnahmeIstGesperrt    { get; set; }
        public int                          Ergebnis                { get; set; }

        public DateTime?                    FälligAm                { get; set; }
        public DateTime?                    ZuletztAm               { get; set; }
        public DateTime?                    NächsterAm              { get; set; }
        public int?                         Normzeit                { get; set; }
        public int?                         Zyklus                  { get; set; }
        public int                          Termintyp               { get; set; }
        public int                          Tagesmuster             { get; set; }
        public int?                         MengeGeplant            { get; set; }
        public int?                         MengeErledigt           { get; set; }
        public decimal?                     Wert                    { get; set; }

        public DateTime?                    DurchgeführtAm          { get; set; }
        public DateTime?                    ErledigtZeitVon         { get; set; }
        public DateTime?                    ErledigtZeitBis         { get; set; }
        public int?                         MinutenGearbeitet       { get; set; }
        public String                       Bemerkungen             { get; set; }
        public String                       Folgeauftrag            { get; set; }
        public String                       Baugruppe               { get; set; }

        public List<PersonalFilterModel>    Personalliste           { get; set; }
        public List<MaterialInfoDto>        Materialliste           { get; set; }
        public PersonalFilterModel          EmpfohlenesPersonal     { get; set; }
    }
}
