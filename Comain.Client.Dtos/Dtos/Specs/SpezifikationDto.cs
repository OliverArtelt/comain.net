﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Specs
{

    public class SpezifikationDto : ChangeTrackingBase
    {

        public String   Baugruppenname      { get; set; }
        public String   Bauteilname         { get; set; }
        public bool     FürAssets           { get; set; }
        public bool     FürArtikel          { get; set; }

        public ICollection<SpecWertDto>       Wertschablonen              { get; set; }
        public ICollection<SpecArtikelDto>    VorgeseheneArtikelliste     { get; set; }
        public ICollection<SpecMassnahmeDto>  VorgeseheneMassnahmen       { get; set; }
    }
}
