﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class PlanExport : IPlanExport
    {

        public bool     BenötigtPlan            => true;
        public String   Name                    => "Wartungsplan";
        public int      Sortierung              => 500;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;

        public IExportVisitor Exporter      => new WartungsplanWriter();


        public String   Text                => @"
Verwenden Sie den Wartungsplan, um die von der Instandhaltung geplanten Wartungsaufträge und die vorgesehenen Zeiten an den Anlagen zu sehen. Die Auswertung zeigt die Zeiten pro Asset und Kalenderwoche.
Wählen Sie die relevanten Standorte bzw. Assets, den gewünschten Zeitraum. Schränken Sie die Ausgabe zusätzlich auf die Leistungsarten und Gewerke ein, Mit der Schaltfläche ""Wartungsplan"" werden die Daten nach Excel exportiert und können dort weiter verarbeitet werden.
Bitte beachten Sie, es werden nur Maßnahmen ausgewertet, die im gewählten Zeitraum geplant wurden. Geben Sie die auszuwertenden Zeiten bitte als Normzeit in den Maßnahmedetails ein. ";
    }
}
