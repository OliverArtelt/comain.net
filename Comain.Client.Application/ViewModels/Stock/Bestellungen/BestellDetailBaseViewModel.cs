﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Comain.Client.Models;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.Materialien;

namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellDetailBaseViewModel : ValidatableViewModel<IBestellDetailBaseView, Bestellung>
    {
 
        private readonly SynchronizationContext context;


        public BestellDetailBaseViewModel(IBestellDetailBaseView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }


        public override void CurrentChanged(Bestellung oldSelected)
        {
            
            RaisePropertyChanged("StatusString");
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Auftrag");
            RaisePropertyChanged("Kostenstelle");
            RaisePropertyChanged("DatumAnlage");
            RaisePropertyChanged("DatumAngebot");
            RaisePropertyChanged("IhreZeichen");
            RaisePropertyChanged("UnsereZeichen");
            RaisePropertyChanged("Telefon");
            RaisePropertyChanged("DatumAusgang");
            RaisePropertyChanged("Termin");
            RaisePropertyChanged("Total");
            RaisePropertyChanged("Anschrift1");
            RaisePropertyChanged("Anschrift2");
            RaisePropertyChanged("LieferantEMail");
            RaisePropertyChanged("Bearbeiter");
            RaisePropertyChanged("Lieferant");
            RaisePropertyChanged("Ansprechpartner");
            RaisePropertyChanged("Positionen");
       
            if (Current != null && Current.Lieferant != null && Current.Lieferant.Ansprechpartnerliste != null) {
            
                LfApListe = Current.Lieferant.Ansprechpartnerliste.ToList();
                if (Current.Ansprechpartner != null) Current.Ansprechpartner = Current.Lieferant.Ansprechpartnerliste.FirstOrDefault(p => p.Id == Current.Ansprechpartner.Id);

            } else {
            
                LfApListe = null;
            }

            if (oldSelected != null) PropertyChangedEventManager.RemoveHandler(oldSelected.Positionen, ViewPropertyChanged, "");
            if (Current != null) PropertyChangedEventManager.AddHandler(Current.Positionen, ViewPropertyChanged, "");
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist)
        {
            context.Send(new SendOrPostCallback((o) => { PsSearchProvider.SetItems(pslist); }), null);
        }

        
        public void Unload()
        {
            PsSearchProvider.ClearItems(); 
        }  


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Preis") RaisePropertyChanged("Total");
        }    


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        public PersonalFilterModel Bearbeiter                  
        {
            get { return current == null? null: current.Bearbeiter; }
            set { 
            
                if (current != null && current.Bearbeiter != value) {

                    current.Bearbeiter = value;
                    RaisePropertyChanged("Bearbeiter");
                }
            }
        }


        public String Lieferant                  
        {
            get { return current == null? null: current.Lieferant.NameUndOrt; }
        }
        

        private IList<LieferantAP> lfApListe;
        public IList<LieferantAP> LfApListe
        { 
            get { return lfApListe; }
            set { SetProperty(ref lfApListe, value); }
        }


        public LieferantAP Ansprechpartner                  
        {
            get { return current == null? null: current.Ansprechpartner; }
            set { 
            
                if (current != null && current.Ansprechpartner != value) {

                    current.Ansprechpartner = value;
                    RaisePropertyChanged("Ansprechpartner");
                }
            }
        }
        

        private Bestellposition position;
        public Bestellposition Position
        { 
            get { return position; }
            set { SetProperty(ref position, value); }
        }
        

        public ObservableCollection<Bestellposition> Positionen
        {
            get { return current == null? null: current.Positionen; }
        }
  
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public String StatusString                  
        {
            get { return current == null? null: current.Status.ToString(); }
        }


        public decimal? Total                  
        {
            get { return current == null? (decimal?)null: current.GPreis; }
        }


        public String LieferantEMail                  
        {
            get { return current == null || current.Lieferant == null? null: current.Lieferant.EMail; }
        }


        public String Anschrift1                  
        {
            get { return current == null? null: current.Anschrift1; }
            set { 
            
                if (current != null && current.Anschrift1 != value) {

                    current.Anschrift1 = value;
                    RaisePropertyChanged("Anschrift1");
                }
            }
        }


        public String Anschrift2                  
        {
            get { return current == null? null: current.Anschrift2; }
            set { 
            
                if (current != null && current.Anschrift2 != value) {

                    current.Anschrift2 = value;
                    RaisePropertyChanged("Anschrift2");
                }
            }
        }


        public DateTime? Termin                  
        {
            get { return current == null? null: current.Termin; }
            set { 
            
                if (current != null && current.Termin != value) {

                    current.Termin = value;
                    RaisePropertyChanged("Termin");
                }
            }
        }


        public String Nummer                  
        {
            get { return current == null? null: current.Nummer; }
            set { 
            
                if (current != null && current.Nummer != value) {

                    current.Nummer = value;
                    RaisePropertyChanged("Nummer");
                }
            }
        }


        public String Auftrag                  
        {
            get { return current == null? null: current.Auftrag; }
            set { 
            
                if (current != null && current.Auftrag != value) {

                    current.Auftrag = value;
                    RaisePropertyChanged("Auftrag");
                }
            }
        }


        public String Kostenstelle                  
        {
            get { return current == null? null: current.Kostenstelle; }
            set { 
            
                if (current != null && current.Kostenstelle != value) {

                    current.Kostenstelle = value;
                    RaisePropertyChanged("Kostenstelle");
                }
            }
        }


        public DateTime? DatumAnlage                  
        {
            get { return current == null? (DateTime?)null: current.DatumAnlage; }
            set { 
            
                if (current != null && current.DatumAnlage != value) {

                    current.DatumAnlage = value.Value;
                    RaisePropertyChanged("DatumAnlage");
                }
            }
        }


        public DateTime? DatumAngebot                  
        {
            get { return current == null? (DateTime?)null: current.DatumAngebot; }
            set { 
            
                if (current != null && current.DatumAngebot != value) {

                    current.DatumAngebot = value;
                    RaisePropertyChanged("DatumAngebot");
                }
            }
        }


        public String IhreZeichen                  
        {
            get { return current == null? null: current.IhreZeichen; }
            set { 
            
                if (current != null && current.IhreZeichen != value) {

                    current.IhreZeichen = value;
                    RaisePropertyChanged("IhreZeichen");
                }
            }
        }


        public String UnsereZeichen                  
        {
            get { return current == null? null: current.UnsereZeichen; }
            set { 
            
                if (current != null && current.UnsereZeichen != value) {

                    current.UnsereZeichen = value;
                    RaisePropertyChanged("UnsereZeichen");
                }
            }
        }


        public String Telefon                  
        {
            get { return current == null? null: current.Telefon; }
            set { 
            
                if (current != null && current.Telefon != value) {

                    current.Telefon = value;
                    RaisePropertyChanged("Telefon");
                }
            }
        }


        public DateTime? DatumAusgang                  
        {
            get { return current == null? (DateTime?)null: current.DatumAusgang; }
            set { 
            
                if (current != null && current.DatumAusgang != value) {

                    current.DatumAusgang = value;
                    RaisePropertyChanged("DatumAusgang");
                }
            }
        }


#endregion

    }
}
