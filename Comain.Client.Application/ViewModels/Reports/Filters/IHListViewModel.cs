﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class IHListViewModel : ViewModel<IIHListView>, IFilterViewModel
    {

        private readonly IFilterService  store;


        public IHListViewModel(IFilterService store, IIHListView view) : base(view)
        {
            this.store = store;
        }


        public void Reset()
        {

            Standort = null;
            Baujahr = null;
            PlusKst = false;
            PlusAu = false;
            SortIHO = true;
        }


        public String Validiere()
        {

            if (Baujahr.HasValue && (Baujahr < 1900 || Baujahr > DateTime.Now.Year + 1)) return "Geben Sie ein sinnvolles vierstelliges Baujahr an.";
            return null;
        }


        public string FilterAlsString()
        {

            var bld = new StringBuilder();
            if (Baujahr.HasValue) bld.Append("Baujahr ").Append(Baujahr).Append(", ");
            if (Standort != null) bld.Append("Standort ").Append(Standort.Nummer).Append(", ");
            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString();
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (Baujahr.HasValue) filter.Add("Baujahr", Baujahr);
            if (PlusKst) filter.Add("PlusKst", true);
            if (PlusAu) filter.Add("PlusAu", true);
            if (Standort != null) filter.Add("PZ", Standort.Id);
            filter.Add("Sortierung", sort);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            RaisePropertyChanged("Standorte");
            return Task.FromResult(true);
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public IList<PzFilterModel> Standorte => store?.Standorte;


        private PzFilterModel standort;
        public PzFilterModel Standort
        {
            get { return standort; }
            set { SetProperty(ref standort, value); }
        }


        private bool plusKst;
        public bool PlusKst
        {
            get { return plusKst; }
            set { SetProperty(ref plusKst, value); }
        }


        private bool plusAu;
        public bool PlusAu
        {
            get { return plusAu; }
            set { SetProperty(ref plusAu, value); }
        }


        private int? baujahr;
        public int? Baujahr
        {
            get { return baujahr; }
            set { SetProperty(ref baujahr, value); }
        }


        private String sort = "SortIHO";

        public bool SortIHO
        {
            get { return sort == "SortIHO"; }
            set {

                if (value && sort != "SortIHO") {

                    sort = "SortIHO";
                    RaisePropertyChanged("SortIHO");
                    RaisePropertyChanged("SortEK");
                    RaisePropertyChanged("SortStatus");
                }
            }
        }

        public bool SortEK
        {
            get { return sort == "SortEK"; }
            set {

                if (value && sort != "SortEK") {

                    sort = "SortEK";
                    RaisePropertyChanged("SortIHO");
                    RaisePropertyChanged("SortEK");
                    RaisePropertyChanged("SortStatus");
                }
            }
        }

        public bool SortStatus
        {
            get { return sort == "SortStatus"; }
            set {

                if (value && sort != "SortStatus") {

                    sort = "SortStatus";
                    RaisePropertyChanged("SortIHO");
                    RaisePropertyChanged("SortEK");
                    RaisePropertyChanged("SortStatus");
                }
            }
        }


#endregion

    }
}
