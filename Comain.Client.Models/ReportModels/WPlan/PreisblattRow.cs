﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.ReportModels.WPlan
{

    public class PreisblattRow
    {

        public int          MassnahmeId                 { get; set; }
        public String       Standortnummer              { get; set; }
        public String       Standortname                { get; set; }
        public String       Maschinennummer             { get; set; }
        public String       IHObjektnummer              { get; set; }
        public String       IHObjektname                { get; set; }
        public String       IHObjektinventarnummer      { get; set; }
        public String       Gewerk                      { get; set; }
        public String       Massnahmenummer             { get; set; }
        public String       Massnahmename               { get; set; }
        public String       Auftragnummer               { get; set; }
        public String       ExterneAuftragnummer        { get; set; }
        public DateTime?    Datum                       { get; set; }
        public String       ZyklusAsString              { get; set; }
        public decimal?     Kosten                      { get; set; }
        public String       AnzeigeTerm                 { get; set; }


        public PreisblattRow(Planaufgabe model)
        {

            var mn = model.Parent.Parent;
            var ih = mn.Parent;
            var pz = ih.Parent;

            MassnahmeId             = mn.Id;
            Standortnummer          = pz.Nummer;
            Standortname            = pz.Name;
            Maschinennummer         = IHObjekt.GebeMaschinennummer(ih.Nummer);
            IHObjektnummer          = ih.Nummer;
            IHObjektname            = ih.Name;
            IHObjektinventarnummer  = ih.Inventarnummer;
            Gewerk                  = mn.Gewerk;
            Massnahmenummer         = mn.Nummer;
            Massnahmename           = mn.Name;
            Auftragnummer           = model.Nummer;
            ExterneAuftragnummer    = mn.ExterneAuftragsnummer;
            Datum                   = model.DurchgeführtAm ?? model.FälligAm;
            ZyklusAsString          = "Zyklus " + mn.ZyklusAsString;
            Kosten                  = model.KostenAngezeigt;
            AnzeigeTerm             = model.AnzeigeTerm;
        }
    }
}
