﻿using System;
using System.Threading;
namespace Comain.Client.Services.Verwaltung
{
    public interface ISubListService
    {
        System.Threading.Tasks.Task<Tuple<TrackableCollection<Comain.Client.ViewData.Verwaltung.NummerItem>, TrackableCollection<Comain.Client.ViewData.Verwaltung.SubNummerItem>>> GetAsync(string url,System.Threading.CancellationToken ct = default);
        bool HasChanges { get; }
        void Reset();
        System.Threading.Tasks.Task SaveAsync(string url,System.Threading.CancellationToken ct = default);
    }
}
