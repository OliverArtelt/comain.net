﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using Comain.Client.Models;
using Comain.Client.Views.Verwaltung;
using Comain.Client.Dtos.Filters;
using System.Windows.Input;
using System;
using System.Waf.Applications;
using System.Threading.Tasks;
using Comain.Client.Models.Users;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class PersonalViewModel : SimpleViewModel<Personal, IPersonalView>
    {

        public enum Tabs { Basis, Assets, Docs }


        public PersonalViewModel(IPersonalView view) : base(view)
        {

            ClearCommand = new DelegateCommand(p => { Gewerk = null;
                                                      SearchTerm = null; });
        }


        public void SetList(ObservableCollection<Personal> liste)
        {

            base.SetList(liste);

            FilterListe = new CollectionViewSource { Source = liste }.View;
            FilterListe.Filter = Predicate;
            Refresh();
        }


        public override void CurrentChanged(Personal oldSelected)
        {
            base.CurrentChanged(oldSelected);
        }


        public override void Unload()
        {

            base.Unload();
            Gewerke = null;
        }


        private bool Predicate(object arg)
        {

            var ps = arg as Personal;
            if (ps == null) return false;
            if (!String.IsNullOrEmpty(SearchTerm) && !ps.Matches(SearchTerm)) return false;
            if (Gewerk != null && Gewerk.Id != ps.Gewerk_Id) return false;
            return true;
        }


        private void Refresh()
        {

            if (FilterListe == null) return;
            context.Send(new SendOrPostCallback((o) => {

                FilterListe?.Refresh();
                RaisePropertyChanged("FilterListe");
            }), null);
        }


        public void ResetTabs() => CurrentTab = 0;


        public ICollectionView FilterListe { get; private set; }


        public ICommand ClearCommand                { get; }
        public ICommand SelectAllAssetsCommand      { get; set; }
        public ICommand DeselectAllAssetsCommand    { get; set; }


        private IList<NummerFilterModel> gewerke;
        public IList<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { if (SetProperty(ref gewerk, value)) Refresh(); }
        }


        private String searchTerm;
        public String SearchTerm
        {
            get { return searchTerm; }
            set { if (SetProperty(ref searchTerm, value)) Refresh(); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private object assetView;
        public object AssetView
        {
            get { return assetView; }
            set { SetProperty(ref assetView, value); }
        }


        private object dokumentView;
        public object DokumentView
        {
            get { return dokumentView; }
            set { SetProperty(ref dokumentView, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }
    }
}
