﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.Reports
{

    [DebuggerDisplay("{Auftragsnummer}")]
    public class AuListDto
    {

        public String       KstNummer                   { get; set; }
        public String       KstName                     { get; set; }
        public String       PzNummer                    { get; set; }
        public String       PzName                      { get; set; }
        public String       IhNummer                    { get; set; }
        public String       IhName                      { get; set; }
        public String       Gewerk                      { get; set; }
        public String       Auftragsnummer              { get; set; }
        public String       DringlichkeitNummer         { get; set; }
        public String       DringlichkeitName           { get; set; }
        public int          Auftragstyp                 { get; set; }
        public int          Auftragstatus               { get; set; }
        public String       TypAsString                 { get; set; }
        public String       StatusAsString              { get; set; }
        public DateTime     Auftragsdatum               { get; set; }
        public String       Kurzbeschreibung            { get; set; }
        public DateTime?    Termin                      { get; set; }
        public decimal?     Wert                        { get; set; }
        public int?         Menge                       { get; set; }
        public decimal?     LeKosten                    { get; set; }
        public decimal?     MtKosten                    { get; set; }
       
        public virtual decimal? Kosten { get { return LeKosten.GetValueOrDefault() + MtKosten.GetValueOrDefault(); } }

        public virtual decimal? EffektiverWert 
        { 
            get {

                int menge = Menge.GetValueOrDefault() == 0? 1: Menge.Value;
                return Wert * menge;
            }
        }
    }
}
