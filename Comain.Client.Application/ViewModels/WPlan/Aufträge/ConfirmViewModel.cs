﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.WPlan.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.WPlan;
using System.ComponentModel;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.WPlan.Aufträge
{

    public class ConfirmViewModel : ViewModel<IConfirmView>, IDataErrorInfo
    {

        private readonly SynchronizationContext context;
        private const int anzahlTageFolgetermin = 14;
        private IList<Checklistposition> zuErledigendePositionen;


        public ConfirmViewModel(IConfirmView view)
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void SetzeCheckliste(IList<Checklistposition> zuErledigendePositionen, Auftrag model)
        {

            this.zuErledigendePositionen = zuErledigendePositionen ?? new List<Checklistposition>();

            AnzahlUnbearbeitet      = this.zuErledigendePositionen.Count(p => p.Ergebnis == Checklistergebnis.Undefiniert);
            AnzahlOK                = this.zuErledigendePositionen.Count(p => p.Ergebnis == Checklistergebnis.OK);
            AnzahlNotOK             = this.zuErledigendePositionen.Count(p => p.Ergebnis == Checklistergebnis.NotOK);
            AnzahlWirdNichtErledigt = this.zuErledigendePositionen.Count(p => p.Ergebnis == Checklistergebnis.WirdNichtErledigt);
            AnzahlFortgeführt       = this.zuErledigendePositionen.Count(p => p.Ergebnis == Checklistergebnis.Fortgeführt);

            MinutenTeilerVisible = this.zuErledigendePositionen.Any(p => p.Personalliste.Count > 1);

            RaisePropertyChanged(nameof(AnzahlUnbearbeitet));
            RaisePropertyChanged(nameof(AnzahlOK));
            RaisePropertyChanged(nameof(AnzahlNotOK));
            RaisePropertyChanged(nameof(AnzahlWirdNichtErledigt));
            RaisePropertyChanged(nameof(AnzahlFortgeführt));
            RaisePropertyChanged(nameof(AnzahlVerändert));
            RaisePropertyChanged(nameof(HatUnbearbeitete));
            RaisePropertyChanged(nameof(HatOKNotOK));
            RaisePropertyChanged(nameof(HatWirdNichtErledigt));
            RaisePropertyChanged(nameof(HatFortgeführte));
            RaisePropertyChanged(nameof(ConfirmNotwendig));
            RaisePropertyChanged(nameof(ShowFortgeführtHint));
            RaisePropertyChanged(nameof(Header));
            RaisePropertyChanged(nameof(FortgeführtHint));
            RaisePropertyChanged(nameof(TextUnbearbeitet));
            RaisePropertyChanged(nameof(TextOkNotOk));
            RaisePropertyChanged(nameof(TextWirdNichtErledigt));
            RaisePropertyChanged(nameof(TextFortgeführt));
            RaisePropertyChanged(nameof(TextFortgeführt));
            RaisePropertyChanged(nameof(MinutenLeistung));
            RaisePropertyChanged(nameof(LeistungVon));
            RaisePropertyChanged(nameof(LeistungBis));
            RaisePropertyChanged(nameof(DatumLeistung));
            RaisePropertyChanged(nameof(UhrzeitVon));
            RaisePropertyChanged(nameof(UhrzeitBis));
            RaisePropertyChanged(nameof(Folgetermin));
            RaisePropertyChanged(nameof(NeuerInstandhalter));
            RaisePropertyChanged(nameof(MinutenHint));

            if (model.Auftragnehmer != null) NeuerInstandhalter = personalliste.FirstOrDefault(p => p.Id == model.Auftragnehmer.Id);
            SetzeZeiten(false);
            Folgetermin = DateTime.Today.AddDays(anzahlTageFolgetermin);
        }


        private void SetzeZeiten(bool fixierterStart)
        {

            if (!DatumLeistung.HasValue) DatumLeistung = DateTime.Today;
            if (!MinutenErmittelt.HasValue) return;
            if (fixierterStart && !LeistungVon.HasValue) return;


            void SetzeZeiten(DateTime von, DateTime bis)
            {

                uhrzeitBis = bis.TimeOfDay.RoundToMinutes();
                uhrzeitVon = von.TimeOfDay.RoundToMinutes();
                RaisePropertyChanged(nameof(UhrzeitBis));
                RaisePropertyChanged(nameof(UhrzeitVon));
            }


            if (fixierterStart) SetzeZeiten(LeistungVon.Value, LeistungVon.Value.AddMinutes(MinutenErmittelt.Value));
            else SetzeZeiten(DateTime.Now.AddMinutes(-MinutenErmittelt.Value), DateTime.Now);

            RaisePropertyChanged(nameof(MinutenHint));
        }


        public Wartungsergebnis GebeErgebnis()
        {

            var result = new Wartungsergebnis { DatumLeistung = DatumLeistung, Folgetermin = Folgetermin,
                                                Instandhalter = Instandhalter?.Id, NeuerInstandhalter = NeuerInstandhalter?.Id,
                                                UhrzeitBis = uhrzeitBis, UhrzeitVon = uhrzeitVon, MinutenWerdenGeteilt = MinutenWerdenGeteilt };
            return result;
        }


        public Task SetListAsync(IList<PersonalFilterModel> pslist)
        {

            return context.SendAsync(new SendOrPostCallback((o) => {

                Personalliste = pslist;

            }), null);
        }


        public void Unload()
        {

            zuErledigendePositionen = null;
            Personalliste = null;
        }


#region P R O P E R T I E S


        public int  AnzahlUnbearbeitet      { get; private set; }
        public int  AnzahlOK                { get; private set; }
        public int  AnzahlNotOK             { get; private set; }
        public int  AnzahlWirdNichtErledigt { get; private set; }
        public int  AnzahlFortgeführt       { get; private set; }

        public int  AnzahlVerändert         => AnzahlOK + AnzahlNotOK + AnzahlWirdNichtErledigt + AnzahlFortgeführt;
        public bool HatUnbearbeitete        => AnzahlUnbearbeitet > 0;
        public bool HatOKNotOK              => AnzahlOK + AnzahlNotOK > 0;
        public bool HatWirdNichtErledigt    => AnzahlWirdNichtErledigt > 0;
        public bool HatFortgeführte         => AnzahlFortgeführt > 0;
        public bool ConfirmNotwendig        => AnzahlVerändert > 0;
        public bool ShowFortgeführtHint     => Folgetermin.HasValue && Folgetermin.Value < DateTime.Today;


        public IList<Checklistposition> GearbeitetePositionen =>
            zuErledigendePositionen.Where(p => !p.InVorigerSitzungErledigt && p.Ergebnis == Checklistergebnis.OK || p.Ergebnis == Checklistergebnis.NotOK).ToList();


        public int? MinutenErmittelt => MinutenWerdenGeteilt?
            GearbeitetePositionen.Sum(p => p.MinutenErmitteltProPerson):
            GearbeitetePositionen.Sum(p => p.MinutenErmittelt);


        public bool PersonalVollständig => GearbeitetePositionen.All(p => p.Personalliste.Any());


        public String Header
        {
            get {

                if (AnzahlVerändert == 1) return "Folgende Maßnahme wird durchgeführt";
                return "Folgende Maßnahmen werden durchgeführt";
            }
        }


        public String MinutenHint
        {
            get {

                if (!MinutenLeistung.HasValue) return null;
                if (MinutenLeistung.Value == 1) return "Gesamtarbeitszeit: 1 Minute.";
                return $"Gesamtarbeitszeit: {MinutenLeistung.Value} Minuten.";
            }
        }


        public String FortgeführtHint
        {
            get {

                if (!ShowFortgeführtHint) return null;
                return "Hinweis: Der Fertigstellungstermin des Folgeauftrages befindet sich in der Vergangenheit. Bitte überprüfen Sie den angegebenen Termin.";
            }
        }


        public String TextUnbearbeitet
        {
            get {

                if (!HatUnbearbeitete) return null;
                if (AnzahlUnbearbeitet == 1) return "Eine Position bleibt offen.";
                return $"{AnzahlUnbearbeitet} Positionen bleiben offen.";
            }
        }


        public String TextOkNotOk
        {
            get {

                if (!HatOKNotOK) return null;
                if (AnzahlOK > 0 && AnzahlNotOK > 0) return $"Es werden {AnzahlOK} als OK befundene und {AnzahlNotOK} als nicht OK befundene Positionen eingetragen.";
                if (AnzahlOK == 1) return $"Es wird eine als OK befundene Position eingetragen.";
                if (AnzahlOK > 1) return $"Es werden {AnzahlOK} als OK befundene Positionen eingetragen.";
                if (AnzahlNotOK == 1) return $"Es wird eine als nicht OK befundene Position eingetragen.";
                return $"Es werden {AnzahlNotOK} als nicht OK befundene Positionen eingetragen.";
            }
        }


        public String TextWirdNichtErledigt
        {
            get {

                if (!HatWirdNichtErledigt) return null;
                if (AnzahlWirdNichtErledigt == 1) return "Eine Position wird nicht durchgeführt.";
                return $"{AnzahlWirdNichtErledigt} Positionen werden nicht durchgeführt.";
            }
        }


        public String TextFortgeführt
        {
            get {

                if (!HatFortgeführte) return null;
                if (AnzahlFortgeführt == 1) return "Eine Position wird in einem neuen Auftrag fortgeführt.";
                return $"{AnzahlFortgeführt} Positionen werden in einem neuen Auftrag fortgeführt.";
            }
        }


        public int? MinutenLeistung
        {
            get {

                if (!LeistungVon.HasValue) return null;
                if (!LeistungBis.HasValue) return null;
                return (int)(LeistungBis.Value - LeistungVon.Value).TotalMinutes;
            }
        }


        public DateTime? LeistungVon
        {
            get {

                if (!DatumLeistung.HasValue) return null;
                if (!uhrzeitVon.HasValue) return null;
                return DatumLeistung.Value.Add(uhrzeitVon.Value);
            }
        }


        public DateTime? LeistungBis
        {
            get {

                if (!DatumLeistung.HasValue) return null;
                if (!uhrzeitBis.HasValue) return null;
                if (!LeistungVon.HasValue) return null;
                var end = DatumLeistung.Value.Add(uhrzeitBis.Value);
                return (end < LeistungVon.Value)? end.AddDays(1): end;
            }
        }


        private DateTime? datumLeistung;
        public DateTime? DatumLeistung
        {
            get { return datumLeistung; }
            set { SetProperty(ref datumLeistung, value); }
        }


        private TimeSpan? uhrzeitVon;
        public String UhrzeitVon
        {
            get { return !uhrzeitVon.HasValue ? null: uhrzeitVon.Value.ToString("hh\\:mm"); }
            set {

                var time = value.ParseTime();
                if (uhrzeitVon != time) {

                    uhrzeitVon = time;
                    SetzeZeiten(true);
                    RaisePropertyChanged(nameof(UhrzeitVon));
                    RaisePropertyChanged(nameof(MinutenHint));
                }
            }
        }


        private TimeSpan? uhrzeitBis;
        public String UhrzeitBis
        {
            get { return !uhrzeitBis.HasValue ? null: uhrzeitBis.Value.ToString("hh\\:mm"); }
            set {

                var time = value.ParseTime();
                if (uhrzeitBis != time) {

                    uhrzeitBis = time;
                    RaisePropertyChanged(nameof(UhrzeitBis));
                    RaisePropertyChanged(nameof(MinutenHint));
                }
            }
        }


        private DateTime? folgetermin;
        public DateTime? Folgetermin
        {
            get { return folgetermin; }
            set {

                if (SetProperty(ref folgetermin, value)) {

                    RaisePropertyChanged(nameof(ShowFortgeführtHint));
                    RaisePropertyChanged(nameof(FortgeführtHint));
                }
            }
        }


        private bool minutenWerdenGeteilt;
        public bool MinutenWerdenGeteilt
        {
            get { return minutenWerdenGeteilt; }
            set {

                if (SetProperty(ref minutenWerdenGeteilt, value)) {

                    RaisePropertyChanged(nameof(MinutenHint));
                    SetzeZeiten(false);
                }
            }
        }


        private bool minutenTeilerVisible;
        public bool MinutenTeilerVisible
        {
            get { return minutenTeilerVisible; }
            set { SetProperty(ref minutenTeilerVisible, value); }
        }


        private IList<PersonalFilterModel> personalliste;
        public IList<PersonalFilterModel> Personalliste
        {
            get { return personalliste; }
            set { SetProperty(ref personalliste, value); }
        }


        private PersonalFilterModel instandhalter;
        public PersonalFilterModel Instandhalter
        {
            get { return instandhalter; }
            set { SetProperty(ref instandhalter, value); }
        }


        private PersonalFilterModel neuerInstandhalter;
        public PersonalFilterModel NeuerInstandhalter
        {
            get { return neuerInstandhalter; }
            set { SetProperty(ref neuerInstandhalter, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


#endregion


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(UhrzeitBis)];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(UhrzeitBis):

                    if (!MinutenLeistung.HasValue) return null;
                    if (MinutenLeistung.Value < 1) return "Es muß mindestens eine Minute gearbeitet worden sein.";
                    break;
                }

                return null;
            }
        }


#endregion

    }
}
