﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class NameDto : ChangeTrackingBase
    {

        public String     Name                    { get; set; }
    }
}
