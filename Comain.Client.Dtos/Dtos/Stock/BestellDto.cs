﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.Stock
{

    public class BestellDto : ChangeTrackingBase
    {
        
        public String                       Nummer              { get; set; }
        public int                          Status              { get; set; }
        public String                       Auftrag             { get; set; }
        public String                       Kostenstelle        { get; set; }
        public String                       IhreZeichen         { get; set; }
        public String                       Telefon             { get; set; }
        public String                       UnsereZeichen       { get; set; }
        public String                       Anschrift1          { get; set; }
        public String                       Anschrift2          { get; set; }
        public String                       Notizen             { get; set; }
        public DateTime                     DatumAnlage         { get; set; }
        public DateTime?                    DatumAusgang        { get; set; }
        public DateTime?                    DatumAngebot        { get; set; }
        public DateTime?                    Termin              { get; set; }

        public LieferantDto                 Lieferant           { get; set; }
        public LieferantApDto               Ansprechpartner     { get; set; }
        public PersonalFilterModel          Bearbeiter          { get; set; }
       
        public ICollection<BestellposDto>   Positionen          { get; set; }
        public ICollection<ReklamDto>       Reklamationen       { get; set; }
    }
}
