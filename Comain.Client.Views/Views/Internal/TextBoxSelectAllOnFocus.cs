﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace Comain.Client.Views.Internal
{

    /// <remarks>
    /// http://www.intertech.com/Blog/Post/How-to-Select-All-Text-in-a-WPF-Content-on-Focus.aspx
    /// </remarks>
    public static class TextBoxSelectAllOnFocus
    {


        public static void RegisterTextBox()
        {

            EventManager.RegisterClassHandler(typeof(TextBox), UIElement.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(SelectivelyHandleMouseButton), true);
            EventManager.RegisterClassHandler(typeof(TextBox), UIElement.GotFocusEvent, new RoutedEventHandler(SelectAllText), true);
        }


        private static void SelectAllText(object sender, RoutedEventArgs e)
        {

            var textBox = e.OriginalSource as TextBox;
            if (textBox == null) return;
            //nicht in Memofeldern
            if (!textBox.AcceptsReturn) textBox.SelectAll();
            else if (!String.IsNullOrEmpty(textBox.Text)) textBox.CaretIndex = textBox.Text.Length;
        }


        private static void SelectivelyHandleMouseButton(object sender, MouseButtonEventArgs e)
        {

            var textbox = (sender as TextBox);

            if (textbox != null && !textbox.IsKeyboardFocusWithin) {

                if (e.OriginalSource.GetType().Name == "TextBoxView" ) {
                    e.Handled = true;
                    textbox.Focus();
                }
            }
        }
    }
}
