﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class EingangLagerDto 
    {
       
        public int                  Id                      { get; set; }

        public String               LieferscheinPosition    { get; set; }
        public String               Artikelnummer           { get; set; }
        public String               Artikelname             { get; set; }
        
        public decimal              MengeBestellt           { get; set; }
        public decimal              MengeOK                 { get; set; }
        public decimal              MengeEingelagert        { get; set; }
    }
}
