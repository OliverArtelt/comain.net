﻿using System;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHReactivateDto
    {

        public int          Id                      { get; set; }
        public String       Nummer                  { get; set; }
        public String       Name                    { get; set; }
        public DateTime?    DeaktiviertAm           { get; set; }
    }
}
