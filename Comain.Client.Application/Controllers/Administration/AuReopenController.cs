﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{

    public interface IAuReopenController : IController  
    {}


    public class AuReopenController : ControllerBase, IAuReopenController
    {

        private readonly IFilterService store;
        private readonly AdminViewModel view;
        private readonly AuftragReopenViewModel reopenView;


        public AuReopenController(IMessageService messages, IFilterService store, IControllerBus bus, 
                                  AdminViewModel view, AuftragReopenViewModel reopenView)
          : base(messages, bus)
        {
           
            this.store = store;
            this.view = view;
            this.reopenView = reopenView;
        }

        
        public void Initialize()
        {}


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            reopenView.ViewerVisible = false;
            view.DetailView = reopenView.View;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(reopenView)) {

                var list = await store.AuReopenAsync(bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                var result = new ReportResult { Title = "Wiedergeöffnete Aufträge", AssemblyAddress = "comain.Client.Reports", 
                                                ReportAddress = "Comain.Client.Reports.AuReopenReport.rdlc" };
                result.AddDataSource("MainData", list);
                await reopenView.BindViewerAsync(result);
            }
        }


        protected override Task<bool> UnloadImplAsync()
        {
           
            reopenView.Unload();
            return Task.FromResult(true);
        }


        public void Shutdown()
        {}
    }
}
