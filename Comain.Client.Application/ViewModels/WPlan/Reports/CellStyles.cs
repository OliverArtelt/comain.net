﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX.ExcelStyles;
using FontStyle = CO.OpenXML.XLSX.ExcelStyles.FontStyle;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    internal static class CellStyles
    {

        public static Style Head                        = new Style { Font   = new FontStyle(12, Color.Black, true),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style Parameter                   = new Style { Font   = new FontStyle(10, Color.Black, true),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style Standort                    = new Style { Font   = new FontStyle(10, Color.Black, true),
                                                                      Color  = new BackColor(Color.FromArgb(0, 153, 255)),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style IHObjekt                    = new Style { Font   = new FontStyle(10, Color.Black, true),
                                                                      Color  = new BackColor(Color.FromArgb(51, 204, 255)),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style Baugruppe                   = new Style { Font   = new FontStyle(10, Color.Black, true),
                                                                      Color  = new BackColor(Color.FromArgb(102, 255, 255)),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style KopfLeer                    = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.LightCyan) };
        public static Style KopfLeerMitte               = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.LightCyan) };
        public static Style KopfWoche                   = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.LightCyan) };
        public static Style KopfDatum                   = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.LightCyan),
                                                                      Format = new NumberFormat(Format.Date) };
        public static Style KopfWocheToday              = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.Gold) };
        public static Style KopfDatumToday              = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center),
                                                                      Color  = new BackColor(Color.Gold),
                                                                      Format = new NumberFormat(Format.Date) };
        public static Style ElementLeer                 = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style Today                       = new Style { Color  = new BackColor(Color.LightYellow),
                                                                      Border = new Border(BorderType.Thin) };
        public static Style Nummer                      = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style Text                        = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center, true) };
        public static Style HintText                    = new Style { Font   = new FontStyle(8),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center, false) };
        public static Style LongText                    = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Top, true) };
        public static Style TextCentered                = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true) };
        public static Style Auftragsnummer              = new Style { Font   = new FontStyle(7),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true) };
        public static Style Numeric                     = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Right, AlignTypes.Center) };
        public static Style NumericCentered             = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style MinutesCentered             = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.BlanchedAlmond),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style MinutesCenteredRed          = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.Coral),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style Money                       = new Style { Font   = new FontStyle(8),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Right, AlignTypes.Center),
                                                                      Format = new NumberFormat(Format.Money) };
        public static Style Datum                       = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style SumMoney                    = new Style { Font   = new FontStyle(8),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style FootText                    = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.BlanchedAlmond),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style FootValue                   = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.BlanchedAlmond),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style FootValueRed                = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.Coral),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style TotalText                   = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.BurlyWood),
                                                                      Align  = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public static Style TotalValue                  = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.BurlyWood),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public static Style TotalValueRed               = new Style { Font   = new FontStyle(8, Color.Black, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.OrangeRed),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center) };


        public static Style ChecklistOpenFuture         = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.White) };
        public static Style ChecklistOpenPast           = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.Coral) };
        public static Style ChecklistOk                 = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.PaleGreen) };
        public static Style ChecklistNotOk              = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.Red) };
        public static Style ChecklistWirdNichtErledigt  = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.Goldenrod) };
        public static Style ChecklistFortgeführt        = new Style { Font   = new FontStyle(8, Color.Black),
                                                                      Align  = new Alignment(AlignTypes.Center, AlignTypes.Center, true),
                                                                      Border = new Border(BorderType.Thin),
                                                                      Color  = new BackColor(Color.SkyBlue) };

    }
}
