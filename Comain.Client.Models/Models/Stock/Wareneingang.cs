﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{

    public class Wareneingang : Entity, IDataErrorInfo
    {

        private int?            position; 
        private decimal         liefermenge; 
        private decimal?        mengeOK; 
        private String          notizen; 
        private bool?           mengeGeprüft; 
        private bool?           qualitätGeprüft; 
        private Lieferschein    lieferschein;
        private Artikel         material;
        private Bestellposition bestellposition;
        private ICollection<Wareneinlagerung> einlagerungen;
        private ICollection<Reklamposition> reklampositionen;
        

        public String Bestellnummer 
        { 
            get { 
            
                if (Bestellposition == null) return null;
                if (Bestellposition.Bestellung == null) return Bestellposition.Position.ToString();
                return String.Format("{0} - {1}", Bestellposition.Bestellung.Nummer, Bestellposition.Position);
            }
        }
        

        public String Nummer 
        { 
            get { 
            
                if (Lieferschein == null) return Position.ToString();
                return String.Format("{0} - {1}", Lieferschein.Nummer, Position);
            }
        }
        

        public String Artikelnummer 
        { 
            get { 
            
                if (material == null) return null;
                return material.Nummer;
            }
        }


        public bool IsEditable { get { return !HatEinlagerungen; } } 

        public bool HatEinlagerungen { get { return Einlagerungen.Any(); } } 

        public bool HatReklamationen { get { return Reklampositionen.Any(); } } 

        public decimal EingelagerteMenge { get { return Einlagerungen.Sum(p => p.Menge); } } 

        public decimal EinlagerbareMenge { get { return Math.Max(0m, mengeOK.GetValueOrDefault() - EingelagerteMenge); } } 
        
        /// <summary>
        /// reklamierte Menge bezieht sich auf alle Reklamationen dieser Bestellposition, nicht nur dieses Wareneinganges
        /// </summary>
        public decimal ReklamierteMenge { get { return Reklampositionen.Sum(p => p.Menge); } } 


#region P R O P E R T I E S


        public ICollection<Reklamposition> Reklampositionen   
        {
            get { 
            
                if (reklampositionen == null) reklampositionen = new HashSet<Reklamposition>();
                return reklampositionen; 
            }
            set { SetProperty(ref reklampositionen, value); }
        }


        public ICollection<Wareneinlagerung> Einlagerungen   
        {
            get { 
            
                if (einlagerungen == null) einlagerungen = new HashSet<Wareneinlagerung>();
                return einlagerungen; 
            }
            set { SetProperty(ref einlagerungen, value); }
        }
        

        public Lieferschein Lieferschein   
        {
            get { return lieferschein; }
            set { SetProperty(ref lieferschein, value); }
        }
        

        public Artikel Material          
        {
            get { return material; }
            set { if (SetProperty(ref material, value)) RaisePropertyChanged("Artikelnummer"); }
        }
        

        public Bestellposition Bestellposition    
        {
            get { return bestellposition; }
            set { SetProperty(ref bestellposition, value); }
        }


        public int? Position             
        {
            get { return position; }
            set { SetProperty(ref position, value); }
        }
        

        public decimal Liefermenge          
        {
            get { return liefermenge; }
            set { SetProperty(ref liefermenge, value); }
        }
        

        public decimal? MengeOK              
        {
            get { return mengeOK; }
            set { SetProperty(ref mengeOK, value); }
        }
        

        public String Notizen              
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }
        

        public bool? MengeGeprüft         
        {
            get { return mengeGeprüft; }
            set { SetProperty(ref mengeGeprüft, value); }
        }
        

        public bool? QualitätGeprüft      
        {
            get { return qualitätGeprüft; }
            set { SetProperty(ref qualitätGeprüft, value); }
        }
        

#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Liefermenge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["MengeOK"];
                return test;
            }
        }

       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Liefermenge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["MengeOK"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }

       
        public List<String> WLValidation
        {
            
            get { 

                var result = new List<String>();
            
                if (IstAngefügt) result.Add("Der Wareneingang muss zunächst gespeichert werden.");
                if (!mengeGeprüft.GetValueOrDefault() || !qualitätGeprüft.GetValueOrDefault()) 
                    result.Add("Ohne Prüfung der gelieferten Artikel auf Menge und Qualität kann keine Einlagerung erfolgen.");
                if (!mengeOK.HasValue) result.Add("Sie müssen zunächst die einlagerbare Menge im Feld Menge OK eintragen.");
                else if (mengeOK.Value <= 0) result.Add("Es ist keine einlagerbare Menge vorhanden.");
                else if (EinlagerbareMenge == 0) result.Add("Die angelieferte Menge wurde bereits vollständig eingelagert.");

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Liefermenge":

                    if (Liefermenge <= 0) return "Die Liefermenge jedes Einganges muss größer 0 sein."; 
                    break;

                case "MengeOK":

                    if (!MengeOK.HasValue) return null; 
                    if (MengeOK > Liefermenge) return "Die als OK befundene Menge jedes Einganges darf nicht größer als die Liefermenge sein."; 
                    if (MengeOK < 0) return "Die als OK befundene Menge jedes Einganges darf nicht negativ sein."; 
                    break;
                }

                return null;
            }
        }

#endregion    
        
    }
}
