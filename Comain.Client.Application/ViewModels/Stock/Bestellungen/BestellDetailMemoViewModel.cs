﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Comain.Client.Models;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{
    
    public class BestellDetailMemoViewModel : ValidatableViewModel<IBestellDetailMemoView, Bestellung>
    {
 
        private readonly SynchronizationContext context;


        public BestellDetailMemoViewModel(IBestellDetailMemoView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(Bestellung oldSelected)
        {
            RaisePropertyChanged("Notizen");
        }

        
        public void Unload()
        {
        }  


#region P R O P E R T I E S
        
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        public String Notizen                  
        {
            get { return current == null? null: current.Notizen; }
            set { 
            
                if (current != null && current.Notizen != value) {

                    current.Notizen = value;
                    RaisePropertyChanged("Notizen");
                }
            }
        }


#endregion

    }
}
