﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Commands;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Settings;

namespace Comain.Client.Controllers
{

    public class ApplicationController
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;
        private readonly IMessageService messages;
        private readonly MainViewModel mainView;
        private readonly HomeViewModel homeView;
        private readonly LoginViewModel loginView;
        private readonly IFlyoutService flyouts;
        private readonly IBreadcrumbService crumbs;
        private readonly ICommandFactory commands;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly IModulService module;
        private readonly ISettings settings;
        private readonly IControllerMediator bus;
        private Konfiguration cfg;


        /// <remarks>
        /// ThemeView erzeugen weil in .ctor Apptheme aus den Einstellungen geladen wird
        /// </remarks>
        public ApplicationController(MainViewModel mainView, HomeViewModel homeView, LoginViewModel loginView, AppearanceViewModel themeView,
                                     IMessageService messages, ICommandFactory commands, ILoginService login,
                                     IFlyoutService flyouts, IBreadcrumbService crumbs, IKonfigService configs, IModulService module,
                                     ISettings settings, IControllerMediator bus)
        {

            this.messages = messages;
            this.mainView = mainView;
            this.homeView = homeView;
            this.loginView = loginView;
            this.flyouts = flyouts;
            this.crumbs = crumbs;
            this.commands = commands;
            this.login = login;
            this.configs = configs;
            this.module = module;
            this.settings = settings;
            this.bus = bus;
        }


        public void Initialize()
        {

            try {

                mainView.CancelCommand = new DelegateCommand(Stop);
                mainView.ConfigCommand = new AsyncDelegateCommand(_ => OpenConfigAsync());
                mainView.ExitCommand = new AsyncDelegateCommand(_ => BeendenAsync());
                mainView.HomeCommand = new AsyncDelegateCommand(_ => HomeAsync());
                mainView.HelpCommand = new DelegateCommand(Help);
                loginView.ActionCommand = new AsyncDelegateCommand(p => LoginAsync(p));
                homeView.ActionCommand = new AsyncDelegateCommand(p => ExecuteAsync(p));
                homeView.BackCommand = new AsyncDelegateCommand(_ => LogoutAsync());

                crumbs.Clear();
                loginView.Hosts = login.Endpoints;
                bus.CurrentControllerChanged += CurrentControllerChanged;
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        public void Run()
        {

            try {

                mainView.PageView = loginView.View;
                mainView.Show();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        public async Task LoginAsync(object p)
        {

            try {

                using (new VisualDelay(loginView)) {

                    var user = await login.LoginAsync(p.ToString(), loginView.Username, loginView.Password, bus.Token);
                    cfg = await configs.GetAsync(bus.Token).ConfigureAwait(false);
                    await module.LoadAsync(bus.Token).ConfigureAwait(false);

                    homeView.SetCommands(commands.VerfügbareAktionen(module.Info, cfg));
                    mainView.PageView = homeView.View;
                    crumbs.SetLogin(login.AktuellesProfil);
                    homeView.Mandant = login.AktuelleVerbindung;
                    loginView.Password = null;
                    await UpdateTilesAsync(module.Info, cfg).ConfigureAwait(false);
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        public async Task LogoutAsync()
        {

            try {

                if (settings.ConfirmLogout && !await messages.ConfirmExitAsync("Möchten Sie sich wirklich vom Server abmelden?", String.Empty)) return;

                using (new VisualDelay(loginView)) {

                    await login.LogoutAsync(bus.Token);
                    module.Clear();
                    cfg = null;
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }

            mainView.PageView = loginView.View;
            crumbs.SetLogin(null);
        }


        /// <summary>
        /// Kachel - dazugehörigen Controller starten
        /// </summary>
        public async Task ExecuteAsync(object obj)
        {

            try {

                if (obj == null) return;
                var action = commands[obj.ToString()];
                if (action == null) return;
                var command = action as IControllerCommand;
                if (command == null) return;

                crumbs.Clear();
                mainView.IsFlyoutOpen = false;

                await bus.JumpAsync(command.Controller, command);
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        /// <summary>
        /// Button Start - zurück in das Hauptmenü
        /// </summary>
        public async Task HomeAsync()
        {

            try {

                await flyouts.CloseAsync();
                if (bus.Current != null && !await bus.Current.UnloadAsync()) return;
                bus.Reset();

                if (login.AktuellerNutzer != null) {

                    mainView.PageView = homeView.View;
                    await UpdateTilesAsync(module.Info, cfg).ConfigureAwait(false);

                } else {

                    mainView.PageView = loginView.View;
                }
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        /// <summary>
        /// Button Stop - alle Vorgänge abbrechen
        /// </summary>
        private void Stop()
        {
            bus.CancelToken();
        }


        private async void CurrentControllerChanged(object sender, EventArgs e)
        {

            var args = e as CurrentControllerChangedEventArgs;
            if (args != null && args.Current == null) await HomeAsync();
        }


        public void Shutdown()
        {

            bus.CurrentControllerChanged -= CurrentControllerChanged;
            bus.Shutdown();
        }


        private void Help()
        {

            try {

                System.Diagnostics.Process.Start(@"""comain Anwenderhandbuch.pdf""");
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private async Task BeendenAsync()
        {

            if (bus.Current != null && !await bus.Current.UnloadAsync()) return;
            context.Send(new SendOrPostCallback((o) => { System.Windows.Application.Current.Shutdown(); }), null);
        }


        private async Task OpenConfigAsync()
        {

            try {

                if (bus.Current != null && !await bus.Current.UnloadAsync()) return;

                crumbs.Clear();
                mainView.IsFlyoutOpen = false;

                await bus.JumpAsync(typeof(IConfigController));
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        private async Task UpdateTilesAsync(ModulInfo module, Konfiguration cfg)
        {

            if (login.AktuellerNutzer == null) return;
            await Task.WhenAll(commands.VerfügbareAktionen(module, cfg).Select(q => q.UpdateAsync(bus.Token)));
        }
    }
}
