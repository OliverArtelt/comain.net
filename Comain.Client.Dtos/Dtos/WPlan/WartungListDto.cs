﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.WPlan
{

    public class WartungListDto : ChangeTrackingBase
    {       

        public int                  Status                  { get; set; }
        public String               Nummer                  { get; set; }
        public String               Kurzbeschreibung        { get; set; }
        public DateTime?            Termin                  { get; set; }
        public DateTime             ErstelltAm              { get; set; }
        public String               IHObjekt                { get; set; }
        public String               Baugruppe               { get; set; }
        public String               Gewerk                  { get; set; }
        public String               Dringlichkeit           { get; set; }
        public String               Kostenstelle            { get; set; }
        public String               Leistungsart            { get; set; }
        public PersonalFilterModel  Auftragnehmer           { get; set; }
        public bool                 HatErledigte            { get; set; }
        public bool                 HatOffene               { get; set; }
        public int?                 Auftraggeber            { get; set; }
        public int?                 Auftragsleiter          { get; set; }
        public int?                 Gewerk_Id               { get; set; }
        public int?                 Leistungsart_Id         { get; set; }
    }
}
