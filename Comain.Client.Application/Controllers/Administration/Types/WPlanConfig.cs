﻿using System;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Administration.Types
{

    public class WPlanConfig : IAdminTyp
    {

        public String       Title           => "Wartungspläne";
        public int          Sortierung      => 50000;
        public Type         Controller      => typeof(IWPlanConfigController);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulWPlan;
    }
}
