﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecDetailBaseViewModel : ValidatableViewModel<ISpecDetailBaseView, Spezifikation>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecDetailBaseViewModel(ISpecDetailBaseView view)
          : base(view)
        {

            ArtikelTabs = new String[] { "Base data", "TechSpecs", "Lieferanten" };
            AssetTabs = new String[] { "Base data", "TechSpecs", "Measures", "Material", "Administration", "Consumption/Equipment", "Remarks", "Wartungsplan" };
        }


        public override void CurrentChanged(Spezifikation oldSelected)
        {
            RaisePropertyChanged(nameof(Wertschablonen));
        }


        public void SchabloneChanged()
        {

            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Pflichtfeld));
            RaisePropertyChanged(nameof(Enumeration));
            RaisePropertyChanged(nameof(AssetTab));
            RaisePropertyChanged(nameof(ArtikelTab));
            RaisePropertyChanged(nameof(Einheit));

            WertetypChanged();
        }


        public void WertetypChanged()
        {

            RaisePropertyChanged(nameof(HasTextValue));
            RaisePropertyChanged(nameof(HasNumericValue));
            RaisePropertyChanged(nameof(HasBoolValue));
            RaisePropertyChanged(nameof(HasEnumeration));
        }


        public void Unload()
        {

            Einheiten = null;
        }


#region P R O P E R T I E S


        public bool HasTextValue
        {
            get { return Wertschablone != null && Wertschablone.Wertetyp == Wertetyp.Text; }
            set {

                if (Current != null && value && Wertschablone != null && Wertschablone.Wertetyp != Wertetyp.Text) {

                    Wertschablone.Wertetyp = Wertetyp.Text;
                    WertetypChanged();
                }
            }
        }


        public bool HasNumericValue
        {
            get { return Wertschablone != null && Wertschablone.Wertetyp == Wertetyp.Numeric; }
            set {

                if (Current != null && value && Wertschablone != null && Wertschablone.Wertetyp != Wertetyp.Numeric) {

                    Wertschablone.Wertetyp = Wertetyp.Numeric;
                    WertetypChanged();
                }
            }
        }


        public bool HasBoolValue
        {
            get { return Wertschablone != null && Wertschablone.Wertetyp == Wertetyp.Boolean; }
            set {

                if (Current != null && value && Wertschablone != null && Wertschablone.Wertetyp != Wertetyp.Boolean) {

                    Wertschablone.Wertetyp = Wertetyp.Boolean;
                    WertetypChanged();
                }
            }
        }


        public bool HasEnumeration
        {
            get { return Wertschablone != null && Wertschablone.Wertetyp == Wertetyp.Enumeration; }
            set {

                if (Current != null && value && Wertschablone != null && Wertschablone.Wertetyp != Wertetyp.Enumeration) {

                    Wertschablone.Wertetyp = Wertetyp.Enumeration;
                    WertetypChanged();
                }
            }
        }


        public String Name
        {
            get { return Wertschablone?.Name; }
            set {

                if (Wertschablone != null && Wertschablone.Name != value) {

                    Wertschablone.Name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }


        public bool Pflichtfeld
        {
            get { return Wertschablone?.Pflichtfeld ?? false; }
            set {

                if (Wertschablone != null && Wertschablone.Pflichtfeld != value) {

                    Wertschablone.Pflichtfeld = value;
                    RaisePropertyChanged(nameof(Pflichtfeld));
                }
            }
        }


        public String Enumeration
        {
            get { return Wertschablone?.Enumeration; }
            set {

                if (Wertschablone != null && Wertschablone.Enumeration != value) {

                    Wertschablone.Enumeration = value;
                    RaisePropertyChanged(nameof(Enumeration));
                }
            }
        }


        public String AssetTab
        {
            get { return Wertschablone?.AssetTab; }
            set {

                if (Wertschablone != null && Wertschablone.AssetTab != value) {

                    Wertschablone.AssetTab = value;
                    RaisePropertyChanged(nameof(AssetTab));
                }
            }
        }


        public String ArtikelTab
        {
            get { return Wertschablone?.ArtikelTab; }
            set {

                if (Wertschablone != null && Wertschablone.ArtikelTab != value) {

                    Wertschablone.ArtikelTab = value;
                    RaisePropertyChanged(nameof(ArtikelTab));
                }
            }
        }


        public NummerFilterModel Einheit
        {
            get { return Wertschablone?.Einheit == null || Einheiten == null? null: Einheiten.FirstOrDefault(p => p.Id == Wertschablone.Einheit.Id); }
            set {

                if (Wertschablone != null && Wertschablone.Einheit != value) {

                    Wertschablone.Einheit = value;
                    RaisePropertyChanged(nameof(Einheit));
                }
            }
        }


        public String[] ArtikelTabs { get; }
        public String[] AssetTabs   { get; }
        public TrackableCollection<SpecWert> Wertschablonen => Current?.Wertschablonen;


        private SpecWert wertschablone;
        public SpecWert Wertschablone
        {
            get { return wertschablone; }
            set { if (SetProperty(ref wertschablone, value)) SchabloneChanged(); }
        }


        private IList<NummerFilterModel> einheiten;
        public IList<NummerFilterModel> Einheiten
        {
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


#endregion

    }
}
