﻿using System;

namespace Comain.Client.Dtos.Auth
{

    public class UserDto
    {

        public String       Id              { get; set; }
        public String       UserName        { get; set; }
        public String       Email           { get; set; }
        public DateTime     Created         { get; set; }
        public DateTime     Updated         { get; set; }
        public DateTime?    LastLogin       { get; set; }
        public String       Name1           { get; set; }
        public String       Name2           { get; set; }
        public String       Beschreibung    { get; set; }
    }
}
