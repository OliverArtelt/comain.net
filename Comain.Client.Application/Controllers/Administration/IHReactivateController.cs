﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{

    public interface IIHReactivateController : IController 
    {}


    public class IHReactivateController : ControllerBase, IIHReactivateController
    {

        private readonly IIHReactivateService store;
        private readonly AdminViewModel view;
        private readonly IHOReaktivierenViewModel reactView;


        public IHReactivateController(IIHReactivateService store, IMessageService messages, IControllerBus bus, 
                                      AdminViewModel view, IHOReaktivierenViewModel reactView)
          : base(messages, bus)
        {

            this.store = store;
            this.view = view;
            this.reactView = reactView;
        }


        public virtual void Initialize()
        {}


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(reactView)) {

                reactView.SaveCommand = CreateCommand(_ => SaveAsync());
                view.DetailView = reactView.View;
                reactView.List = await store.GetAsync(bus.Token);
            }
        }


        private async Task SaveAsync()
        {

            if (reactView.List.IsNullOrEmpty()) return;
            var sellist = reactView.List.Where(p => p.IsSelected).ToList();
            var cnt = sellist.Count;
            if (cnt == 0) return;

            var alllist = reactView.List.Where(p => sellist.Any(q => q.Nummer.StartsWith(p.Nummer)) && !p.IsSelected).ToList();
            var token = new MessageToken { Header = cnt == 1? "Soll das Asset reaktiviert werden?": String.Format("Sollen die {0} Assets reaktiviert werden?", sellist.Count)};
            if (alllist.Count == 1) token.Reasons = new List<String> { "Zusätzlich muss ein übergeordnetes Objekt aktiviert werden, damit die Objekte im Assetverzeichnis erscheinen." };
            if (alllist.Count > 1)  token.Reasons = new List<String> { String.Format("Zusätzlich müssen {0} übergeordnete Objekte aktiviert werden, damit die Objekte im Assetverzeichnis erscheinen.", alllist.Count) };
            if (!await messages.ConfirmAsync(token)) return;

            using (new VisualDelay(reactView)) {

                var idlist = reactView.List.Where(p => p.IsSelected).Select(p => p.Id).ToList();
                await store.SetAsync(idlist, bus.Token);

                bus.Token.ThrowIfCancellationRequested();

                view.DetailView = reactView.View;
                reactView.List = await store.GetAsync(bus.Token);
            }
        }


        public virtual void Shutdown()
        {}


        protected virtual Task<bool> UnloadImplAsync()
        {

            reactView.Unload();
            return Task.FromResult(true);    
        }
    }
}
