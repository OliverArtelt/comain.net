﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class FeiertagDto : ChangeTrackingBase
    {
        
        public String     Name                    { get; set; }
        public DateTime?  Datum                   { get; set; }
    }
}
