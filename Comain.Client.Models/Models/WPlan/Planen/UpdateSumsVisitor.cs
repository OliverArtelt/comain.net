﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{
    public class UpdateSumsVisitor : IVisitor
    {

        public void Visit(Plan model)
        {}


        public void Visit(Planspaltensumme model)
        {
            model.RaisePropertyChanged(nameof(model.Wert));
        }


        public void Visit(PZPlanzeile model)
        {}


        public void Visit(PZSpaltensumme model)
        {
            model.RaisePropertyChanged(nameof(model.Wert));
        }


        public void Visit(IHPlanzeile model)
        {}


        public void Visit(IHSpaltensumme model)
        {
            model.RaisePropertyChanged(nameof(model.Wert));
        }


        public void Visit(MNPlanzeile model)
        {}


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
