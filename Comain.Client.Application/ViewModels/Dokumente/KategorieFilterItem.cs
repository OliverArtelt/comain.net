﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;

namespace Comain.Client.ViewModels.Dokumente
{

    public class KategorieFilterItem : NotifyBase
    {

        private readonly Kategorie model;


        public KategorieFilterItem(Kategorie model)
        {

            this.model = model;
        }


        public String Bezeichnung => model.Bezeichnung;


        public int Id => model.Id;


        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value);
        }
    }
}
