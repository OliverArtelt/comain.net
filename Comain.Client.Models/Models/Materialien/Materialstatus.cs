﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{

    public enum Materialstatus
    {

        Offen = 1,
        AngebotPlanung = 2,
        Auftrag = 3,
        Kauf = 4,
        Lieferung = 5,
        Eingebaut = 6
    }
}
