﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class TafelFilterViewModel : ViewModel<ITafelFilterView>
    {

        private readonly SynchronizationContext context;


        public TafelFilterViewModel(ITafelFilterView view) : base(view)
        {

            context = SynchronizationContext.Current;
            Reset();
        }


        public void Reset()
        {

            OffeneAnzeigen = true;
            ErledigteAnzeigen = true;
            NurEigeneAnzeigen = false;
            Anzeigemodus = Plananzeigemodus.Zeiten;
            LeereZeilenAusblenden = false;
            DurchführungZurFälligkeit = true;
            NurMaterialAnzeigen = false;
        }


        public void Unload()
        {}


        public void UpdateSearchPosition()
        {

            if (currentSearch == null) {

                CurrentSearchPosition = null;
                return;
            }

            if (currentSearch.Count == 0) CurrentSearchPosition = "nichts gefunden.";
            else CurrentSearchPosition = $"{currentSearch.Position + 1} / {currentSearch.Count}";
        }


#region P R O P E R T I E S


        private RingCursor<IPlanzeile> currentSearch;
        public RingCursor<IPlanzeile> CurrentSearch
        {
            get { return currentSearch; }
            set { if (SetProperty(ref currentSearch, value)) UpdateSearchPosition(); }
        }


        public bool SearchVisible => !String.IsNullOrWhiteSpace(SearchText);


        private String searchText;
        public String SearchText
        {
            get => searchText;
            set { if (SetProperty(ref searchText, value)) RaisePropertyChanged(nameof(SearchVisible)); }
        }


        private bool leereZeilenAusblenden;
        public bool LeereZeilenAusblenden
        {
            get { return leereZeilenAusblenden; }
            set { SetProperty(ref leereZeilenAusblenden, value); }
        }


        private bool nurMaterialAnzeigen;
        public bool NurMaterialAnzeigen
        {
            get { return nurMaterialAnzeigen; }
            set { SetProperty(ref nurMaterialAnzeigen, value); }
        }


        private bool offeneAnzeigen;
        public bool OffeneAnzeigen
        {
            get { return offeneAnzeigen; }
            set { SetProperty(ref offeneAnzeigen, value); }
        }


        private bool erledigteAnzeigen;
        public bool ErledigteAnzeigen
        {
            get { return erledigteAnzeigen; }
            set { SetProperty(ref erledigteAnzeigen, value); }
        }


        private bool nurEigeneAnzeigen;
        public bool NurEigeneAnzeigen
        {
            get => nurEigeneAnzeigen;
            set => SetProperty(ref nurEigeneAnzeigen, value);
        }


        private String currentSearchPosition;
        public String CurrentSearchPosition
        {
            get => currentSearchPosition;
            set => SetProperty(ref currentSearchPosition, value);
        }


        private ICommand prevCommand;
        public ICommand PrevCommand
        {
            get => prevCommand;
            set => SetProperty(ref prevCommand, value);
        }


        private ICommand nextCommand;
        public ICommand NextCommand
        {
            get => nextCommand;
            set => SetProperty(ref nextCommand, value);
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get => searchCommand;
            set => SetProperty(ref searchCommand, value);
        }


        private Plananzeigemodus anzeigemodus;
        public Plananzeigemodus Anzeigemodus
        {
            get { return anzeigemodus; }
            set {

                if (SetProperty(ref anzeigemodus, value)) {

                    RaisePropertyChanged(nameof(ZeitenAnzeigen));
                    RaisePropertyChanged(nameof(KostenAnzeigen));
                    RaisePropertyChanged(nameof(PersonalAnzeigen));
                    RaisePropertyChanged(nameof(AuftragAnzeigen));
                    RaisePropertyChanged(nameof(MaterialAnzeigen));
                }
            }
        }


        public bool ZeitenAnzeigen
        {
            get => Anzeigemodus == Plananzeigemodus.Zeiten;
            set => Anzeigemodus = Plananzeigemodus.Zeiten;
        }


        public bool KostenAnzeigen
        {
            get => Anzeigemodus == Plananzeigemodus.Kosten;
            set => Anzeigemodus = Plananzeigemodus.Kosten;
        }


        public bool PersonalAnzeigen
        {
            get => Anzeigemodus == Plananzeigemodus.Personal;
            set => Anzeigemodus = Plananzeigemodus.Personal;
        }


        public bool AuftragAnzeigen
        {
            get => Anzeigemodus == Plananzeigemodus.Auftrag;
            set => Anzeigemodus = Plananzeigemodus.Auftrag;
        }


        public bool MaterialAnzeigen
        {
            get => Anzeigemodus == Plananzeigemodus.Material;
            set => Anzeigemodus = Plananzeigemodus.Material;
        }


        private bool heuteErledigt;
        public bool DurchführungZurFälligkeit
        {
            get => !heuteErledigt;
            set {

                if (heuteErledigt && value) {

                    heuteErledigt = false;
                    RaisePropertyChanged(nameof(DurchführungZurFälligkeit));
                    RaisePropertyChanged(nameof(DurchführungHeute));
                }
            }
        }


        public bool DurchführungHeute
        {
            get => heuteErledigt;
            set {

                if (!heuteErledigt && value) {

                    heuteErledigt = true;
                    RaisePropertyChanged(nameof(DurchführungZurFälligkeit));
                    RaisePropertyChanged(nameof(DurchführungHeute));
                }
            }
        }


#endregion

    }
}
