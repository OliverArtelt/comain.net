﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Warenausgänge
{

    public interface IAusgangService
    {

        Task<IList<AuSelektorDto>>              AufträgeAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>             KostenstellenAsync(CancellationToken ct = default);
        Task<IList<LagerortDto>>                LagerorteAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>        PersonalAsync(CancellationToken ct = default);
        Task<IList<EinlagerungDto>>             VerfügbareArtikelAsync(CancellationToken ct = default);
        Task<IHInfoDto>                         IHInfoAsync(int auid, CancellationToken ct = default);
        Task<decimal>                           FachRestmengeAsync(int lagerortid, int artikelid, CancellationToken ct = default);

        Task<TrackableCollection<Warenausgang>> FindAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<WAReportDto>>                ReportAsync(Suchkriterien filter, CancellationToken ct = default);
        Task                                    SaveAsync(TrackableCollection<Warenausgang> list, CancellationToken ct = default);
    }
}