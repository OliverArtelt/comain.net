﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Specs
{

    public class SpecListItem : NotifyBase
    {

        public int       Id                  { get; set; }

        private String   baugruppenname;
        private String   bauteilname;
        private bool     fürAssets;
        private bool     fürArtikel;


        public String Baugruppenname
        {
            get { return baugruppenname; }
            set { SetProperty(ref baugruppenname, value); }
        }


        public String Bauteilname
        {
            get { return bauteilname; }
            set { SetProperty(ref bauteilname, value); }
        }


        public bool FürAssets
        {
            get { return fürAssets; }
            set { SetProperty(ref fürAssets, value); }
        }


        public bool FürArtikel
        {
            get { return fürArtikel; }
            set { SetProperty(ref fürArtikel, value); }
        }


        private Spezifikation model;
        public Spezifikation Model
        {
            get => model;
            set {

                if (value != model) {

                    if (model != null) PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    model = value;
                    if (model != null) PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                    Emboss();
                }
            }
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case nameof(Baugruppenname):

                Baugruppenname = model.Baugruppenname;
                RaisePropertyChanged(nameof(Baugruppenname));
                break;

            case nameof(Bauteilname):

                Bauteilname = model.Bauteilname;
                RaisePropertyChanged(nameof(Bauteilname));
                break;

            case nameof(FürAssets):

                FürAssets = model.FürAssets;
                RaisePropertyChanged(nameof(FürAssets));
                break;

            case nameof(FürArtikel):

                FürArtikel = model.FürArtikel;
                RaisePropertyChanged(nameof(FürArtikel));
                break;
            }
        }


        public SpecListItem()
        {}


        public SpecListItem(Spezifikation model)
        {
            Model = model;
            Emboss();
        }


        public void Emboss()
        {
            Id              = model.Id;
            Baugruppenname  = model.Baugruppenname;
            Bauteilname     = model.Bauteilname;
            FürAssets       = model.FürAssets;
            FürArtikel      = model.FürArtikel;
        }
    }
}
