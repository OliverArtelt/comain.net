﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class AUListReport : ReportBase, IReport
    {

        private readonly IReportService store;
        private readonly AuListViewModel aulistView;
        private readonly ZeitViewModel zeitView;
        private readonly KostenstelleViewModel kstView;
        private readonly StandortViewModel pzView;


        public AUListReport(IReportService store, AuListViewModel aulistView, ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView)
        {

            this.store = store;
            this.aulistView = aulistView;
            this.zeitView = zeitView;
            this.kstView = kstView;
            this.pzView = pzView;
            filterViews = new List<IFilterViewModel> { aulistView, zeitView, kstView, pzView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();

            if (!aulistView.FullRow) {

                var qry = await store.SimpleAUListAsync(GebeFilter(), ct);
                result.AddDataSource("MainData", qry);

            } else {

                var qry = await store.FullAUListAsync(GebeFilter(), ct);
                result.AddDataSource("MainData", qry);
            }

            return result;
        }


        public override IEnumerable<string> Validiere()
        {

            var errors = new List<IFilterViewModel> { aulistView, kstView, pzView }.Select(p => p.Validiere()).Where(p => !String.IsNullOrEmpty(p)).ToList();
            String zeiterror = null;
            if (aulistView.TypGeschlossen) zeiterror = zeitView.Validiere();
            if (aulistView.TypIHObjekt) zeiterror = zeitView.Validiere();
            if (aulistView.TypTermin) zeiterror = zeitView.ValidiereEndtermin();
            if (!String.IsNullOrEmpty(zeiterror)) errors.Add(zeiterror);

            return errors;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Auftragsliste"; }
        }


        public string Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 50; }
        }


        public override string ReportAddress
        {
            get { return aulistView.FullRow? "Comain.Client.Reports.AUFullListReport.rdlc": "Comain.Client.Reports.AUListReport.rdlc"; }
        }
    }
}
