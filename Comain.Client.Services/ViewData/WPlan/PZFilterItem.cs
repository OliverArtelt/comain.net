﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData.WPlan
{

    [DebuggerDisplay("{Nummer}")]
    public class PZFilterItem : NotifyBase
    {

        public int      Id                  { get; set; }
        public String   Kurzname            { get; set; }
        public String   Nummer              { get; set; }
        public String   Name                { get; set; }

        public List<IHFilterItem> IHObjekte { get; set; }


        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set {

                SetProperty(ref isChecked, value);
                IHObjekte.ForEach(p => { p.IsChecked = value; });
            }
        }


        private bool isExpanded;
        public bool IsExpanded
        {
            get => isExpanded;
            set => SetProperty(ref isExpanded, value);
        }
    }
}
