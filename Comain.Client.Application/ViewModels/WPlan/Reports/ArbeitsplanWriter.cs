﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX;
using CO.OpenXML.XLSX.ExcelStyles;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class ArbeitsplanWriter : IExportVisitor
    {

        private WorksheetWriter writer;
        private IStyleFactory styles;
        private int currentRow = 1;
        private int spaltenanzahl;
        private Plan plan;
        private PlanOptions options;
        private const float uraltExcelZeichenbreite = 1.416F;


        public String Exportname => "Arbeitsplan";


        public void SetStyles(IStyleFactory s)
            => styles = s;


        public void SetOptions(PlanOptions opts)
            => options = opts;


        public void SetWriter(WorksheetWriter w)
            => writer = w;


        public void Visit(Plan model)
        {

            int ndx = 0;
            plan = model;
            spaltenanzahl = 14 + model.Spalten.Count;

            var cols = new List<ColumnStyle>();
            cols.Add(new ColumnStyle(++ndx, 15F * uraltExcelZeichenbreite));   //Gewerk
            cols.Add(new ColumnStyle(++ndx, 12F * uraltExcelZeichenbreite));   //MN Nummer
            cols.Add(new ColumnStyle(++ndx, 32F * uraltExcelZeichenbreite));   //MN Name
            cols.Add(new ColumnStyle(++ndx, 12F * uraltExcelZeichenbreite));   //Turnus
            cols.Add(new ColumnStyle(++ndx, 6F  * uraltExcelZeichenbreite));   //Normzeit
            cols.Add(new ColumnStyle(++ndx, 6F  * uraltExcelZeichenbreite));   //Menge
            model.Spalten.ForEach(p => cols.Add(new ColumnStyle(++ndx, 3.5F * uraltExcelZeichenbreite)));
            writer.SetColumnWidth(cols);


            writer.SetValue(model.Client, currentRow++, 2, styles.Head);
            writer.SetValue(model.Name, currentRow++, 2, styles.Head);

            writer.SetValue("Erstellt", currentRow, 1, styles.Params);
            writer.SetValue(DateTime.Now.ToString(), currentRow++, 2, styles.Params);
            writer.SetValue("Benutzer", currentRow, 1, styles.Params);
            writer.SetValue(model.Benutzer, currentRow++, 2, styles.Params);

            if (!String.IsNullOrEmpty(model.Statistik?.Zeitraum)) {

                writer.SetValue("Zeitraum", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.Zeitraum, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PlanungstypAsString)) {

                writer.SetValue("Planungstyp", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PlanungstypAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PlanungsstatusAsString)) {

                writer.SetValue("Planungsstatus", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PlanungsstatusAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.LeistungsartAsString)) {

                writer.SetValue("Leistungsart", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.LeistungsartAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.GewerkAsString)) {

                writer.SetValue("Gewerk", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.GewerkAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            if (!String.IsNullOrEmpty(model.Statistik?.PersonalAsString)) {

                writer.SetValue("Personal", currentRow, 1, styles.Params);
                writer.SetValue(model.Statistik?.PersonalAsString, currentRow, 2, styles.Params);
                currentRow++;
            }

            currentRow++;
        }


        public void Visit(PZPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            writer.SetRowHeight(currentRow, 26);
            writer.SetValue("Standort", currentRow, 1, styles.Standort);
            writer.SetValue(model.Name, currentRow, 2, styles.Standort);

            for (int i = 3; i <= spaltenanzahl; i++) writer.SetValue(String.Empty, currentRow, i, styles.Standort);
            currentRow++;
        }


        public void Visit(IHPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;

            writer.SetRowHeight(currentRow, 24);
            writer.SetValue(model.Nummer, currentRow, 1, styles.IHObjekt);
            writer.SetValue(model.Name, currentRow, 2, styles.IHObjekt);

            currentRow++;
            int currentCol = 1;

            writer.SetValue("Gewerk",       currentRow, currentCol++, styles.AuKopfL);
            writer.SetValue("Nummer",       currentRow, currentCol++, styles.AuKopfC);
            writer.SetValue("Beschreibung", currentRow, currentCol++, styles.AuKopfL);
            writer.SetValue("Tur",          currentRow, currentCol++, styles.AuKopfC);
            writer.SetValue("Minuten",      currentRow, currentCol++, styles.AuKopfC);
            writer.SetValue("Menge",        currentRow, currentCol++, styles.AuKopfC);

            foreach (var spalte in plan.Spalten) {

                writer.SetValue(spalte.Periode, currentRow, currentCol, spalte.IstAktuell? styles.AuKopfCWAktuell: styles.AuKopfCW);
                writer.SetValue(spalte.TagShort, currentRow - 1, currentCol, styles.GreaterKey);

                currentCol++;
            }

            currentRow++;
        }


        public void Visit(MNPlanzeile model)
        {

            if (!model.ImExportSichtbar(options)) return;


            Style MapStyle(Planzelle cell)
            {

                if (cell == null) return styles.KeinErgebnis;
                if (!cell.ImExportSichtbar(options)) return styles.KeinErgebnis;
                if (cell.Ergebnis == Checklistergebnis.OK) return styles.ErgebnisOK;
                if (cell.Ergebnis == Checklistergebnis.NotOK) return styles.ErgebnisNotOK;
                if (cell.Ergebnis == Checklistergebnis.WirdNichtErledigt) return styles.ErgebnisWirdNichtErledigt;
                if (cell.Ergebnis == Checklistergebnis.Fortgeführt) return styles.ErgebnisFortgeführt;
                return styles.ErgebnisOffen;
            }


            int? Value(Planzelle cell)
            {

                if (cell == null) return null;
                if (!cell.ImExportSichtbar(options)) return null;
                if (cell.Menge.GetValueOrDefault() == 0) return null;
                return cell.Menge;
            }


            int currentCol = 1;

            writer.SetValue(model.Gewerk,         currentRow, currentCol++, styles.AuCode);
            writer.SetValue(model.Nummer,         currentRow, currentCol++, styles.AuKurzB);
            writer.SetValue(model.Name,           currentRow, currentCol++, styles.AuKurzB);
            writer.SetValue(model.ZyklusAsString, currentRow, currentCol++, styles.AuText);
            writer.SetValue(model.Normzeit,       currentRow, currentCol++, styles.AuNumeric);
            writer.SetValue(model.Menge,          currentRow, currentCol++, styles.AuNumeric);

            model.Perioden.ForEach(periode => writer.SetValue(Value(periode), currentRow, currentCol++, MapStyle(periode)));

            currentRow++;
        }


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void Visit(Planspaltensumme model)
        {}


        public void Visit(PZSpaltensumme model)
        {}


        public void Visit(IHSpaltensumme model)
        {}


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
