﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain
{

    public static class DictionaryExtensions
    {

        public static V GetValueOrDefault<K, V>(this Dictionary<K, V> dict, K key)
        {

            V value;
            dict.TryGetValue(key, out value);
            return value;
        }
    }
}
