﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using AutoMapper;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Controllers.Verwaltung.Types;
using Comain.Client.Views.Emma;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Aufträge;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.Emma;
using IHSelektorViewModel = Comain.Client.ViewModels.IHObjekte.IHSelektorViewModel;


namespace Comain.Client.ViewModels.Emma
{

    public class EmmaDetailViewModel : ValidatableViewModel<IEmmaDetailView, Leistung>
    {

        private readonly SynchronizationContext context;
        private IList<IHListDto> ihlist;


        public EmmaDetailViewModel(IEmmaDetailView view, IAuftragSearchProvider aufträge, IPersonalSearchProvider personal,
                                   IIHFastSearchProvider ihOjekte)
          : base(view)
        {

            context = SynchronizationContext.Current;
            AuSearchProvider = aufträge;
            PsSearchProvider = personal;
            IHSearchProvider = ihOjekte;
        }


        public override void CurrentChanged(Leistung oldSelected)
        {

            RaisePropertyChanged(nameof(EmmaMaschine));
            RaisePropertyChanged(nameof(EmmaIHObjekt));
            RaisePropertyChanged(nameof(EmmaAuftrag));
            RaisePropertyChanged(nameof(EmmaAktion));
            RaisePropertyChanged(nameof(EmmaPersonal));
            RaisePropertyChanged(nameof(EmmaPersonalname));
            RaisePropertyChanged(nameof(EmmaBis));
            RaisePropertyChanged(nameof(EmmaVon));
            RaisePropertyChanged(nameof(Datum));
            RaisePropertyChanged(nameof(UhrzeitVon));
            RaisePropertyChanged(nameof(UhrzeitBis));
            RaisePropertyChanged(nameof(Minuten));
            RaisePropertyChanged(nameof(KeinObjektGeladen));
            RaisePropertyChanged(nameof(ArbeitszeitInkonsistent));
            RaisePropertyChanged(nameof(ArbeitszeitKonsistent));
            RaisePropertyChanged(nameof(IstBestätigt));
            RaisePropertyChanged(nameof(IstBearbeitbar));
            RaisePropertyChanged(nameof(KannBestätigtWerden));

            RaiseIHObjektAuftragChanged();
            RaisePersonalChanged();

            AuSearchProvider.IHObjekt = current?.IHObjekt;
        }


        private void RaiseIHObjektAuftragChanged()
        {

            RaisePropertyChanged(nameof(ComainAuftragNummer));
            RaisePropertyChanged(nameof(ComainAuftragName));
            RaisePropertyChanged(nameof(AuftragZugeordnet));
            RaisePropertyChanged(nameof(AuftragNichtZugeordnet));
            RaisePropertyChanged(nameof(Auftrag));
            RaisePropertyChanged(nameof(KannBestätigtWerden));
            RaisePropertyChanged(nameof(Statuslist));
            RaisePropertyChanged(nameof(IHObjektZugeordnet));
            RaisePropertyChanged(nameof(IHObjektNichtZugeordnet));
            RaisePropertyChanged(nameof(IHObjekt));
            RaisePropertyChanged(nameof(KannBestätigtWerden));
            RaisePropertyChanged(nameof(Statuslist));
        }


        private void RaisePersonalChanged()
        {

            RaisePropertyChanged(nameof(ComainPersonalNummer));
            RaisePropertyChanged(nameof(ComainPersonalName));
            RaisePropertyChanged(nameof(PersonalZugeordnet));
            RaisePropertyChanged(nameof(PersonalNichtZugeordnet));
            RaisePropertyChanged(nameof(Personal));
            RaisePropertyChanged(nameof(KannBestätigtWerden));
            RaisePropertyChanged(nameof(Statuslist));
        }


        public async Task LoadAsync(EmmaSession session)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                this.ihlist = session.IHObjekte;
                AuSearchProvider.SetItems(session.Aufträge);
                PsSearchProvider.SetItems(session.Personalliste);
                IHSearchProvider.SetItems(session.IHObjekte);
            }), null);
        }


        public void Unload()
        {

            AuSearchProvider.ClearItems();
            PsSearchProvider.ClearItems();
            IHSearchProvider.ClearItems();
        }


        public void AddAndAssignIHObjekt(IHObjekt model)
        {

            if (Current == null || model == null) return;

            var dto = model.AsListDto();
            ihlist.Add(dto);
            Current.IHObjekt = dto;
            RaiseIHObjektAuftragChanged();
        }


        public void AddAndAssignAuftrag(Auftrag model)
        {

            if (Current == null || model == null) return;

            var dto = model.AsListDto();
            Current.Auftrag = dto;
            RaiseIHObjektAuftragChanged();
        }


        public void AddAndAssignPersonal(Comain.Client.Models.Users.Personal model)
        {

            if (Current == null || model == null) return;

            var dto = model.AsListDto();
            Current.Personal = dto;
            RaisePersonalChanged();
        }


        public override bool EditEnabled => Current != null && !Current.Personalleistung_Id.HasValue;


        #region P R O P E R T I E S


        public  List<String>    Statuslist               => Current?.Statuslist;

        public  String          EmmaIHObjekt             => Current?.EmmaInventarnummer;
        public  String          EmmaMaschine             => Current?.EmmaMaschine;
        public  String          EmmaAuftrag              => Current?.EmmaAuftragsnummer;
        public  String          EmmaAktion               => Current?.EmmaAktion;
        public  String          EmmaPersonal             => Current?.EmmaPersonalnummer;
        public  String          EmmaPersonalname         => Current?.EmmaPersonalname;
        public  String          EmmaVon                  => Current?.EmmaVon.ToString();
        public  String          EmmaBis                  => Current?.EmmaBis.ToString();

        public  String          ComainAuftragNummer      => Current?.Auftrag  == null ? "Nicht zugeordnet.": Current.Auftrag.Nummer;
        public  String          ComainPersonalNummer     => Current?.Personal == null ? "Nicht zugeordnet.": Current.Personal.Nummer;
        public  String          ComainAuftragName        => Current?.Auftrag  == null ? String.Empty: Current.Auftrag.Kurzbeschreibung;
        public  String          ComainPersonalName       => Current?.Personal == null ? String.Empty: Current.Personal.Nachname + ", " + Current.Personal.Vorname;

        public  bool            KeinObjektGeladen        => Current == null;
        public  bool            IHObjektZugeordnet       => Current?.IHObjekt != null;
        public  bool            AuftragZugeordnet        => Current?.Auftrag != null;
        public  bool            PersonalZugeordnet       => Current?.Personal != null;
        public  bool            IHObjektNichtZugeordnet  => !KeinObjektGeladen && !IHObjektZugeordnet;
        public  bool            AuftragNichtZugeordnet   => !KeinObjektGeladen && !AuftragZugeordnet;
        public  bool            PersonalNichtZugeordnet  => !KeinObjektGeladen && !PersonalZugeordnet;
        public  bool            ArbeitszeitKonsistent    => Current != null && !Current.Errors.Any();
        public  bool            ArbeitszeitInkonsistent  => Current != null && Current.Errors.Any();
        public  bool            KannBestätigtWerden      => Current == null? false: Current.KannBestätigtWerden;
        public  bool            IstBearbeitbar           => Current == null? false: Current.IstBearbeitbar;



        public bool IstBestätigt
        {
            get { return Current == null? false: Current.IstBestätigt; }
            set {

                if (Current != null && value != Current.IstBestätigt) {

                    Current.IstBestätigt = value;
                    RaisePropertyChanged(nameof(IstBestätigt));
                }
            }
        }


        public DateTime? Datum
        {
            get { return Current?.Datum; }
            set {

                if (Current != null && value.HasValue && value != Current.Datum) {

                    Current.Datum = value.Value;
                    RaisePropertyChanged(nameof(Datum));
                }
            }
        }


        public String UhrzeitVon
        {
            get { return current?.UhrzeitVon.ToString("hh\\:mm"); }
            set {

                if (current != null) {

                    var time = value.ParseTime();
                    if (current.UhrzeitVon != time && time.HasValue) {

                        current.UhrzeitVon = time.Value;
                        RaisePropertyChanged(nameof(UhrzeitVon));
                        RaisePropertyChanged(nameof(Minuten));
                        RaisePropertyChanged(nameof(Statuslist));
                    }
                }
            }
        }


        public String UhrzeitBis
        {
            get { return current?.UhrzeitBis.ToString("hh\\:mm"); }
            set {

                if (current != null) {

                    var time = value.ParseTime();
                    if (current.UhrzeitBis != time && time.HasValue) {

                        current.UhrzeitBis = time.Value;
                        RaisePropertyChanged(nameof(UhrzeitBis));
                        RaisePropertyChanged(nameof(Minuten));
                        RaisePropertyChanged(nameof(Statuslist));
                    }
                }
            }
        }


        public int? Minuten
        {
            get { return Current?.Minuten; }
            set {

                if (Current != null && value != Current.Minuten) {

                    Current.Minuten = value;
                    RaisePropertyChanged(nameof(Minuten));
                    RaisePropertyChanged(nameof(Statuslist));
                }
            }
        }


        private ICommand ihObjektOpenCommand;
        public ICommand IHObjektOpenCommand
        {
            get { return ihObjektOpenCommand; }
            set { SetProperty(ref ihObjektOpenCommand, value); }
        }


        private ICommand ihObjektAddCommand;
        public ICommand IHObjektAddCommand
        {
            get { return ihObjektAddCommand; }
            set { SetProperty(ref ihObjektAddCommand, value); }
        }


        private ICommand auftragOpenCommand;
        public ICommand AuftragOpenCommand
        {
            get { return auftragOpenCommand; }
            set { SetProperty(ref auftragOpenCommand, value); }
        }


        private ICommand auftragAddCommand;
        public ICommand AuftragAddCommand
        {
            get { return auftragAddCommand; }
            set { SetProperty(ref auftragAddCommand, value); }
        }


        private ICommand personalOpenCommand;
        public ICommand PersonalOpenCommand
        {
            get { return personalOpenCommand; }
            set { SetProperty(ref personalOpenCommand, value); }
        }


        private ICommand personalAddCommand;
        public ICommand PersonalAddCommand
        {
            get { return personalAddCommand; }
            set { SetProperty(ref personalAddCommand, value); }
        }


        public IAuftragSearchProvider  AuSearchProvider  { get; }

        public IPersonalSearchProvider PsSearchProvider  { get; }

        public IIHFastSearchProvider   IHSearchProvider  { get; }


        public IHFilterItem IHObjekt
        {
            get => current?.IHObjekt == null? null: new IHFilterItem(current.IHObjekt);
            set {

                if (current != null && current.IHObjekt?.Id != value?.Id) {

                    current.IHObjekt = value == null? null: ihlist.FirstOrDefault(p => p.Id == value.Id);
                    AuSearchProvider.IHObjekt = current.IHObjekt;
                    RaiseIHObjektAuftragChanged();
                }
            }
        }


        public AuftragListDto Auftrag
        {
            get => current?.Auftrag;
            set {

                if (current != null && current.Auftrag != value) {

                    current.Auftrag = value;
                    if (value != null) current.IHObjekt = ihlist.FirstOrDefault(p => p.Nummer == value.IHObjektNummer);
                    RaiseIHObjektAuftragChanged();
                }
            }
        }


        public PersonalFilterModel Personal
        {
            get => current?.Personal;
            set {

                if (current != null && current.Personal != value) {

                    current.Personal = value;
                    RaisePersonalChanged();
                }
            }
        }


#endregion

    }
}
