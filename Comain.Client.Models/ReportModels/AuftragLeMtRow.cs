﻿using System;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Materialien;

namespace Comain.Client.ReportModels
{

    public class AuftragLeMtRow
    {

        public String       LENummer      { get; private set; }
        public String       LEDauer       { get; private set; }
        public int?         LEZeit        { get; private set; }
        public String       MTMaterial    { get; private set; }
        public decimal?     MTMenge       { get; private set; }
        public String       MTEinheit     { get; private set; }
        public decimal?     MTPreis       { get; private set; }


        public AuftragLeMtRow(Personalleistung le, Materialleistung mt)
        {

            if (le != null) {

                LENummer   = le.Personal.Nummer;
                LEDauer    = $"{le.BeginnDatum.Value.ToShortDateString()}  {le.BeginnZeit.Value.ToString("hh':'mm")} - {le.EndeZeit.Value.ToString("hh':'mm")}";
                LEZeit     = le.Minuten;
            }

            if (mt != null) {

                MTMaterial = mt.Name;
                MTMenge    = mt.Menge;
                MTEinheit  = mt.EinheitName;
                MTPreis    = mt.Gesamtpreis;
            }
        }
    }
}
