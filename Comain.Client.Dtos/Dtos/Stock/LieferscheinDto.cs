﻿using System;
using System.Collections.Generic;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class LieferscheinDto : ChangeTrackingBase
    {
        
        public String     Nummer            { get; set; }
        public String     Notizen           { get; set; }
        public DateTime   Datum             { get; set; }

        public ICollection<EingangDto> Eingänge   { get; set; }
    }
}
