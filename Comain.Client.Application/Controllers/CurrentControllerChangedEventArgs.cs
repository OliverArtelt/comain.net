﻿using System;


namespace Comain.Client.Controllers
{

    public class CurrentControllerChangedEventArgs : EventArgs
    {
        
        public IController Current { get; private set; }


        public CurrentControllerChangedEventArgs(IController current)
        {
            Current = current;
        }
    }
}
