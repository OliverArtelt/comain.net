﻿using System;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;

namespace Comain.Client.ViewData.Stock.Reports
{

    public class MtLfExportRow
    {
        
        public int?          ArtikelId               { get; set; }
        public String        Warengruppe             { get; set; }
        public String        Nummer                  { get; set; }
        public String        Name                    { get; set; }
        public decimal?      Mindestbestand          { get; set; }
        public decimal?      Mischpreis              { get; set; }

        public String        PosTyp                  { get; set; }
        
        public int?          LieferantRank           { get; set; }
        public decimal?      LieferantPreis          { get; set; }
        public String        LieferantNummer         { get; set; }
        public String        LieferantName           { get; set; }

        public int?          Position                { get; set; }
        public String        IHObjekt                { get; set; }
        public String        Maschine                { get; set; }


        public MtLfExportRow()
        {}


        public MtLfExportRow(MtLfReportDto dto)
        {
        
            ArtikelId      = dto.Id; 
            Nummer         = dto.Nummer; 
            Name           = dto.Name; 
            Mindestbestand = dto.Mindestbestand; 
            Mischpreis     = dto.Mischpreis; 

            if (dto.Warengruppe != null) Warengruppe = dto.Warengruppe.Nummer; 
        }


        public MtLfExportRow(MtLfReportDto dto, ArtikelLfMap lfdto)
        {
        
            Nummer          = dto.Nummer; 
            LieferantRank   = lfdto.Rank;
            LieferantPreis  = lfdto.Preis;
            LieferantNummer = lfdto.Nummer;
            if (String.IsNullOrEmpty(lfdto.Lieferant.Ort)) LieferantName = lfdto.Lieferant.Name1;
            else LieferantName = String.Format("{0} ({1})", lfdto.Lieferant.Name1, lfdto.Lieferant.Ort);
        }


        public MtLfExportRow(MtLfReportDto dto, ArtikelIhoMap ihdto)
        {
        
            Nummer    = dto.Nummer; 
            Position  = ihdto.Position;
            IHObjekt  = ihdto.IHObjekt.Nummer;
            Maschine  = ihdto.IHObjekt.Name;
        }
    }
}
