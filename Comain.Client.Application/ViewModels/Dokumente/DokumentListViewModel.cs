﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Dokumente;
using Comain.Client.Views.Dokumente;

namespace Comain.Client.ViewModels.Dokumente
{

    public class DokumentListViewModel : ViewModel<IDokumentListView>
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public DokumentListViewModel(IDokumentListView view) : base(view)
        {}


        public Task LoadAsync(IEnumerable<Dokument> list)
        {

            return context.SendAsync(
                new SendOrPostCallback((o) => {

                    Foundlist = list.IsNullOrEmpty()? null: new TrackableCollection<Dokument>(list);
                    RaisePropertyChanged(nameof(Foundlist));
                }), null);
        }


        public void Add(IEnumerable<Dokument> list)
        {

            if (list.IsNullOrEmpty()) return;

            if (Foundlist == null) Foundlist = new TrackableCollection<Dokument>();
            Foundlist.AddRange(list);
            RaisePropertyChanged(nameof(Foundlist));
            Current = list.FirstOrDefault();
            DetailCommand.Execute(this);
        }


        public void Remove(Dokument entity)
        {

            Foundlist.Remove(entity);
            RaisePropertyChanged(nameof(Foundlist));
            Current = null;
            DetailCommand.Execute(this);
        }


        public void Unload()
        {

            Foundlist = null;
            Current = null;
        }


#region P R O P E R T I E S


        public TrackableCollection<Dokument> Foundlist  { get; private set; }


        public bool HasChanges => !Foundlist.IsNullOrEmpty() && Foundlist.IsChanged;


        private Dokument current;
        public Dokument Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private ICommand openCommand;
        public ICommand OpenCommand
        {
            get { return openCommand; }
            set { SetProperty(ref openCommand, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


#endregion

    }
}
