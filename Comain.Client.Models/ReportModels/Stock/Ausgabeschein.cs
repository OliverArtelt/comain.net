﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{

    public class Ausgabeschein
    {

        private readonly Warenausgang model;


        public Ausgabeschein(Warenausgang model, decimal restmenge, String maschine, String ihobjekt)
        {
           
            this.model = model;
            Fachrestmenge = restmenge;
            IHObjekt      = ihobjekt;
            Maschine      = maschine;
        }


        public String   Nummer                { get { return model.Nummer; } }
        public String   Datum                 { get { return model.Datum.ToShortDateString(); } }
        public String   Artikelbezeichnung    { get { return model.Material == null? null: model.Material.Name; } }
        public String   Artikelnummer         { get { return model.Material == null? null: model.Material.Nummer; } }
        public String   Auftragsnummer        { get { return model.Auftrag == null? null: model.Auftrag.Nummer; } }
        public String   Kostenstelle          { get { return model.Kostenstelle == null? null: model.Kostenstelle.Nummer; } }
        public String   Besteller             { get { return model.Nutzer == null? null: model.Nutzer.Nachname + ", " + model.Nutzer.Vorname + " [" + model.Nutzer.Nummer + "]"; } }
        public String   AusgabeDurch          { get { return model.Lagerist == null? null: model.Lagerist.Nachname + ", " + model.Lagerist.Vorname + " [" + model.Lagerist.Nummer + "]"; } }
        public decimal  Entnahmemenge         { get { return model.Menge; } }
        public String   Lagerort              { get { return model.Lagerort.Nummer; } }

        public decimal  Fachrestmenge         { get; private set; }
        public String   IHObjekt              { get; private set; }
        public String   Maschine              { get; private set; }
    }
}
