﻿using System;
using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Views.Verwaltung;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class LeistungsartViewModel : SimpleViewModel<Leistungsart, ILeistungsartView>
    {

        public class TypViewItem
        {

            public Termintyp    Typ     { get; set; }    
            public String       Name    { get; set; }    
        }


        public List<TypViewItem> Termintypen { get; }


        public LeistungsartViewModel(ILeistungsartView view) : base(view)
        {
        
            Termintypen = new List<TypViewItem> { new TypViewItem { Typ = Termintyp.Undefiniert, Name = "Keine Terminplanung" },
                                                  new TypViewItem { Typ = Termintyp.UnveränderlicheKette, Name = "Unveränderliche Terminkette" },
                                                  new TypViewItem { Typ = Termintyp.VeränderlicheKette, Name = "Veränderliche Terminkette" } };
        }
    }
}
