﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class BestellNeuViewModel : ViewModel<IBestellNeuView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private IEnumerable<String> searchterms;
        private IList<ArtikelBSAddPickItem> selectedAt;


        public BestellNeuViewModel(IBestellNeuView view) : base(view)
        {

            context = SynchronizationContext.Current;
            ClearCommand = new DelegateCommand(Reset);
            SearchCommand = new DelegateCommand(Refresh);
        }


        public void SetzeListen(BestellAddData current)
        {

            selectedAt = null;

            Artikelliste = new CollectionViewSource { Source = current.Artikelliste.Select(p => new ArtikelBSAddPickItem(p)).OrderBy(p => p.Nummer).ToList() }.View;
            Artikelliste.Filter = ArtikelPredicate;
            Lieferanten = new CollectionViewSource { Source = current.Lieferanten }.View;
            Lieferanten.Filter = LieferantPredicate;
            selectedAt = null;

            Artikelliste.SourceCollection.Cast<ArtikelBSAddPickItem>().ForEach(at => PropertyChangedEventManager.AddHandler(at, ViewPropertyChanged, "IsSelected"));
        }


        private bool ArtikelPredicate(object o)
        {

            var at = o as ArtikelBSAddPickItem;
            if (at == null) return false;

            if (NurBestellwürdige && at.OffeneMenge == 0m) return false;
            if (!at.Matches(searchterms)) return false;
            if (Mode == NeuSelektorMode.Lieferant && Lieferant != null) return !at.Lieferanten.IsNullOrEmpty() && at.Lieferanten.Any(p => p.LieferantId == Lieferant.Id);

            return true;
        }


        private bool LieferantPredicate(object o)
        {

            var lf = o as Lieferant;
            if (lf == null) return false;
            if (Mode == NeuSelektorMode.Artikel && !selectedAt.IsNullOrEmpty()) return selectedAt.All(p => p.Lieferanten.Any(q => q.LieferantId == lf.Id));

            return true;
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (Mode == NeuSelektorMode.Artikel) {

                Lieferant = null;
                selectedAt = GetSelectedAt();
                Lieferanten?.Refresh();
            }
        }


        private void Refresh()
        {

            if (Artikelliste == null) return;
            searchterms = null;
            if (!String.IsNullOrWhiteSpace(Filter)) searchterms = Filter.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            context.Post(new SendOrPostCallback((o) => {

                Artikelliste?.Refresh();
                RaisePropertyChanged("Artikelliste");
                var cnt = ((CollectionView)Artikelliste).Count;

                switch (cnt) {

                case 0:

                    Result = "Es wurde kein Artikel gefunden.";
                    break;

                case 1:

                    Result = "Es wurde ein Artikel gefunden.";
                    break;

                default:

                    Result = String.Format("Es wurden {0} Artikel gefunden.", cnt);
                    break;
                }

            }), null);
        }


        public void Reset()
        {

            Artikelliste.SourceCollection.Cast<ArtikelBSAddPickItem>().ForEach(p => p.IsSelected = false);
            Lieferant = null;
            RaiseModeChanged();
        }


        public void Unload()
        {

            Artikelliste = null;
            Lieferanten = null;
        }


        public IList<ArtikelBSAddPickItem> GetSelectedAt()
        {

            if (Artikelliste == null) return null;
            return Artikelliste.SourceCollection.Cast<ArtikelBSAddPickItem>().Where(p => p.IsSelected).ToList();
        }


#region P R O P E R T I E S


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICollectionView artikelliste;
        public ICollectionView Artikelliste
        {
            get { return artikelliste; }
            set { SetProperty(ref artikelliste, value); }
        }


        private ICollectionView lieferanten;
        public ICollectionView Lieferanten
        {
            get { return lieferanten; }
            set { SetProperty(ref lieferanten, value); }
        }


        private Lieferant lieferant;
        public Lieferant Lieferant
        {
            get { return lieferant; }
            set { if (SetProperty(ref lieferant, value) && mode == NeuSelektorMode.Lieferant && Artikelliste != null) Artikelliste?.Refresh(); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        public ICommand ClearCommand    { get; private set; }


        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool nurBestellwürdige;
        public bool NurBestellwürdige
        {
            get { return nurBestellwürdige; }
            set { if (SetProperty(ref nurBestellwürdige, value) && Artikelliste != null) Artikelliste?.Refresh(); }
        }


        private void RaiseModeChanged()
        {

            RaisePropertyChanged("Mode");
            RaisePropertyChanged("MtMode");
            RaisePropertyChanged("LfMode");
            RaisePropertyChanged("FreeMode");

            if (Artikelliste != null) Artikelliste?.Refresh();
            if (Lieferanten != null) Lieferanten?.Refresh();
        }


        private String filter;
        public String Filter
        {
            get { return filter; }
            set { SetProperty(ref filter, value); }
        }


        private String result;
        public String Result
        {
            get { return result; }
            set { SetProperty(ref result, value); }
        }


        private NeuSelektorMode mode = NeuSelektorMode.Artikel;
        public NeuSelektorMode Mode
        {
            get { return mode; }
            set {

                if (mode != value) {

                    mode = value;
                    RaiseModeChanged();
                }
            }
        }


        public bool MtMode
        {

            get { return mode == NeuSelektorMode.Artikel; }
            set {

                if (value && mode != NeuSelektorMode.Artikel) {

                    mode = NeuSelektorMode.Artikel;
                    RaiseModeChanged();
                }
            }
        }


        public bool LfMode
        {

            get { return mode == NeuSelektorMode.Lieferant; }
            set {

                if (value && mode != NeuSelektorMode.Lieferant) {

                    mode = NeuSelektorMode.Lieferant;
                    RaiseModeChanged();
                }
            }
        }


        public bool FreeMode
        {

            get { return mode == NeuSelektorMode.Frei; }
            set {

                if (value && mode != NeuSelektorMode.Frei) {

                    mode = NeuSelektorMode.Frei;
                    RaiseModeChanged();
                }
            }
        }


#endregion

    }
}
