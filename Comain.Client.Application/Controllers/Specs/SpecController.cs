﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Threading.Tasks;
using Comain.Client.Models.Specs;
using Comain.Client.Services.Specs;
using Comain.Client.ViewModels.Specs.Verwaltung;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Specs;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain;
using Comain.Client.ViewData;
using Comain.Client.Controllers.Dokumente;
using MahApps.Metro.Controls;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Controllers.Stock.Artikelstamm;

namespace Comain.Client.Controllers.Specs
{

    public class SpecController : ControllerBase, ISpecController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService configs;
        private readonly ISpecService store;
        private readonly IModulService modules;
        private ISpecDokumentController docs;
        private readonly MainViewModel mainView;
        private readonly SpecViewModel view;
        private readonly SpecListViewModel listView;
        private readonly SpecFilterViewModel filterView;
        private readonly SpecDetailViewModel detailView;
        private readonly SpecDetailReportViewModel reportView;
        private readonly SpecDetailBaseViewModel baseView;
        private readonly SpecDetailArtikelViewModel artikelView;
        private readonly SpecDetailMassnahmeViewModel mnView;
        private readonly SpecArtikelAddViewModel artikelAddView;
        private EditMode editMode;
        private SerializableCommand editCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand copyCommand;
        private SerializableCommand addWertCommand;
        private SerializableCommand deleteWertCommand;
        private SerializableCommand addMassnahmeCommand;
        private SerializableCommand deleteMassnahmeCommand;
        private SerializableCommand addArtikelCommand;
        private SerializableCommand removeArtikelCommand;


        public SpecController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                              IControllerBus bus, IKonfigService configs, ISpecService store, IModulService modules,
                              MainViewModel mainView, SpecViewModel view, SpecListViewModel listView, SpecFilterViewModel filterView,
                              SpecDetailViewModel detailView, SpecDetailReportViewModel reportView, SpecDetailBaseViewModel baseView,
                              SpecDetailArtikelViewModel artikelView, SpecDetailMassnahmeViewModel mnView, SpecArtikelAddViewModel artikelAddView)

          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.configs = configs;
            this.store = store;
            //this.docs = docs;
            this.modules = modules;

            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.reportView = reportView;
            this.baseView = baseView;
            this.artikelView = artikelView;
            this.mnView = mnView;
            this.artikelAddView = artikelAddView;
        }


        public void Initialize()
        {

            docs = bus.GetController<ISpecDokumentController>();

            view.FilterView = filterView.View;
            view.ListView = listView.View;
            view.DetailView = detailView.View;
            detailView.BaseView = baseView.View;
            detailView.ReportView = reportView.View;
            detailView.ArtikelView = artikelView.View;
            detailView.DocView = docs.View;
            detailView.MnView = mnView.View;

            filterView.SearchCommand     = CreateCommand(_ => SearchAsync());
            listView.DetailCommand       = CreateCommand(_ => DetailAsync());
            view.SaveCommand             = CreateCommand(_ => SaveAsync());
            view.EditCommand             = editCommand = CreateCommand(_ => EditAsync(), _ => CurrentModel != null);
            view.CancelCommand           = CreateCommand(_ => CancelAsync());
            view.ReloadCommand           = CreateCommand(_ => LoadAsync());
            view.DeleteCommand           = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => CurrentModel != null);
            view.AddCommand              = CreateCommand(_ => AddAsync());
            view.CopyCommand             = copyCommand = CreateCommand(_ => CopyAsync(), _ => CurrentModel != null);
            view.BackCommand             = new AsyncDelegateCommand(_ => bus.BackAsync());
            view.AddWertCommand          = addWertCommand = CreateCommand(_ => AddWertAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && detailView.Tab == SpecDetailViewModel.Tabs.Basis);
            view.DeleteWertCommand       = deleteWertCommand = CreateCommand(_ => DeleteWertAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && baseView.Wertschablone != null && detailView.Tab == SpecDetailViewModel.Tabs.Basis);
            view.AddMassnahmeCommand     = addMassnahmeCommand = CreateCommand(_ => AddMassnahmeAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && detailView.Tab == SpecDetailViewModel.Tabs.Massnahmen);
            view.DeleteMassnahmeCommand  = deleteMassnahmeCommand = CreateCommand(_ => DeleteMassnahmeAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && mnView.Current != null && detailView.Tab == SpecDetailViewModel.Tabs.Massnahmen);
            view.AddArtikelCommand       = addArtikelCommand = CreateCommand(_ => AddArtikelAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && detailView.Tab == SpecDetailViewModel.Tabs.Artikel);
            view.RemoveArtikelCommand    = removeArtikelCommand = CreateCommand(_ => RemoveArtikelAsync(), _ => editMode != EditMode.ReadOnly && CurrentModel != null && artikelView.Current != null && detailView.Tab == SpecDetailViewModel.Tabs.Artikel);
            reportView.OpenEntityCommand = CreateCommand(p => CallEntityAsync(p as AssignedEntityItem));

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(mnView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.AddHandler(artikelView, ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            mainView.PageView = view.View;
            if (parameter is ControllerMessage) return;
            await UnloadImplAsync();

            crumbs.Add("Technische Spezifikation");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            SetEditMode(EditMode.ReadOnly);

            await LoadAsync();
        }


        public async Task LoadAsync()
        {

            var ehtask = store.EinheitenAsync(bus.Token);
            var mttask = store.MaterialstammAsync(bus.Token);
            var gwtask = store.GewerkeAsync(bus.Token);
            var latask = store.LeistungsartenAsync(bus.Token);
            await Task.WhenAll(ehtask, mttask, gwtask, latask).ConfigureAwait(false);

            await OnUIThreadAsync(() => {

                baseView.Einheiten = ehtask.Result;
                artikelAddView.LoadMaterial(mttask.Result);
                mnView.Gewerke = gwtask.Result;
                mnView.Leistungsarten = latask.Result.Where(p => p.IstWI).ToList();
            });

            await docs.LoadAsync();
            bus.Token.ThrowIfCancellationRequested();

            await SearchAsync();
        }


        private async Task SearchAsync()
        {

            SetModel(null);
            baseView.Wertschablone = null;
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                listView.Current = null;

                var list = await store.SearchAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();

                await OnUIThreadAsync(() => {

                    listView.Foundlist = new ObservableCollection<SpecListItem>(list);

                    var bld = new StringBuilder();
                    if (list.Count == 0) bld.Append("FILTER: Keine Spezifikationen gefunden");
                    else if (list.Count == 1) bld.Append("FILTER: Eine Spezifikation gefunden");
                    else bld.Append("FILTER: ").Append(list.Count.ToString("N0")).Append(" Spezifikationen gefunden");
                    filterView.ResultText = bld.ToString();
                });
            }

            SetEditMode(EditMode.ReadOnly);
        }


        private Spezifikation CurrentModel  => listView.Current?.Model;


        private async Task DetailAsync()
        {

            SetEditMode(EditMode.ReadOnly);
            if (listView.Current == null) return;

            using (new VisualDelay(detailView)) {

                int id = listView.Current.Id;
                listView.Current.Model = await store.DetailAsync(id, bus.Token);
                await docs.DetailAsync(listView.Current.Model);
                SetModel(listView.Current.Model);
            }

            SetEditMode();

            using (new VisualDelay(reportView)) {

                var assigned = await store.AssignedEntitiesAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
                await OnUIThreadAsync(() => { reportView.Entities = assigned.Select(p => new AssignedEntityItem(p)).ToList(); });
            }
        }


        private async Task SaveAsync()
        {

            if (CurrentModel == null) return;
            if (!CurrentModel.IsChanged) return;

            if (!CurrentModel.Errorlist.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Spezifikation kann nicht gespeichert werden.", Reasons = detailView.Current.Errorlist });
                return;
            }


            if (CurrentModel.FürArtikel && (!CurrentModel.VorgeseheneArtikelliste.IsNullOrEmpty() || !CurrentModel.VorgeseheneMassnahmen.IsNullOrEmpty())) {

                var reasons = new List<String>();
                if (CurrentModel.VorgeseheneArtikelliste.Count == 1) reasons.Add("Eine Artikelverknüpfung wird entfernt.");
                else if (CurrentModel.VorgeseheneArtikelliste.Count > 1) reasons.Add($"{CurrentModel.VorgeseheneArtikelliste.Count} Artikelverknüpfungen werden entfernt.");
                if (CurrentModel.VorgeseheneMassnahmen.Count == 1) reasons.Add("Eine Maßnahmenschablone wird entfernt.");
                else if (CurrentModel.VorgeseheneMassnahmen.Count > 1) reasons.Add($"{CurrentModel.VorgeseheneArtikelliste.Count} Maßnahmenschablonen werden entfernt.");

                if (!await messages.ConfirmAsync(new MessageToken { Header = "Der Spezifikation sind Artikel oder Maßnahmen zugeordnet.",
                                                                    Reasons = reasons })) return;
            }


            using (new VisualDelay(view)) {

                listView.Current.Model = await store.UpdateAsync(CurrentModel, bus.Token);
                docs.UpdateEntityId(listView.Current.Model);
                await docs.SaveAsync().ConfigureAwait(false);
                SetModel(CurrentModel);
            }

            SetEditMode(EditMode.ReadOnly);
        }


        private async Task EditAsync()
        {

            if (CurrentModel == null) return;
            SetEditMode(EditMode.Edit);
        }


        private async Task CancelAsync()
        {

            if (CurrentModel == null) return;
            if (CurrentModel.IstAngefügt) {

                await OnUIThreadAsync(() => {

                    listView.Foundlist.Remove(listView.Current);
                    listView.Current = null;
                });
                SetModel(null);

            } else {

                await DetailAsync();
            }

            SetEditMode(EditMode.ReadOnly);
        }


        private async Task AddAsync()
            => AddImplAsync(Spezifikation.Create());


        private async Task CopyAsync()
        {

            if (CurrentModel == null) return;
            await AddImplAsync(CurrentModel.Clone());
        }


        private async Task AddImplAsync(Spezifikation model)
        {

            SetModel(model);
            await OnUIThreadAsync(() => {

                var viewItem = new SpecListItem(model);
                listView.Foundlist.Add(viewItem);
                listView.Current = viewItem;
            });
            SetEditMode(EditMode.Append);
        }


        private async Task DeleteAsync()
        {

            if (CurrentModel == null) return;
            if (!await messages.ConfirmDeleteAsync(new DeleteToken { Header = "Soll die Spezifikation wirklich gelöscht werden?", IsDeleteable = true,
                                                                     Reasons = CurrentModel.LöschKonsequenzen() })) return;
            await store.DeleteAsync(CurrentModel);
            await OnUIThreadAsync(() => {

                listView.Foundlist.Remove(listView.Current);
                listView.Current = null;
            });
            SetModel(null);

            SetEditMode();
        }


        private async Task AddWertAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly) return;
            var wertmodel = new SpecWert();
            await OnUIThreadAsync(() => {

                baseView.Wertschablonen.Add(wertmodel);
                baseView.Wertschablone = wertmodel;
            });
        }


        private async Task DeleteWertAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly || baseView.Wertschablone == null) return;
            await OnUIThreadAsync(() => {

                baseView.Wertschablonen.Remove(baseView.Wertschablone);
                baseView.Wertschablone = null;
            });
        }


        private async Task AddMassnahmeAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly) return;
            var mnmodel = new SpecMassnahme();
            await OnUIThreadAsync(() => {

                mnView.VorgeseheneMassnahmen.Add(mnmodel);
                mnView.Current = mnmodel;
            });
        }


        private async Task DeleteMassnahmeAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly || mnView.Current == null) return;
            await OnUIThreadAsync(() => {

                mnView.VorgeseheneMassnahmen.Remove(mnView.Current);
                mnView.Current = null;
            });
        }


        private async Task AddArtikelAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly) return;
            artikelAddView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = artikelAddView.View, Header = "Artikel anfügen", Position = MahApps.Metro.Controls.Position.Right }))
                return;

            var mtlist = artikelAddView.GetSelected();
            if (mtlist.IsNullOrEmpty()) return;

            var oldIds = artikelView.VorgeseheneArtikelliste.Select(p => p.Artikel_Id).ToHashSet();
            await OnUIThreadAsync(() => {

                artikelView.VorgeseheneArtikelliste.AddRange(mtlist.Where(p => !oldIds.Contains(p.Id)).Select(p => new SpecArtikel(p)));
            });
        }


        private async Task RemoveArtikelAsync()
        {

            if (CurrentModel == null || editMode == EditMode.ReadOnly || artikelView.Current == null) return;

            await OnUIThreadAsync(() => {

                artikelView.VorgeseheneArtikelliste.Remove(artikelView.Current);
                artikelView.Current = null;
            });
        }


        private async Task CallEntityAsync(AssignedEntityItem entity)
        {

            if (entity == null) return;

            switch (entity.EntityType) {

                case "Asset":

                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IIHObjektController), Entity_Id = entity.EntityId, Source = this });
                    break;

                case "Artikel":

                    if (!modules.Info.VerwendetModulStock) return;
                    await bus.JumpAsync(new ControllerMessage { Destination = typeof(IArtikelController), Entity_Id = entity.EntityId, Source = this });
                    break;
            }
        }


        private void SetEditMode()
            => SetEditMode(editMode);


        private void SetEditMode(EditMode newMode)
        {

            editMode = newMode;
            listView.EditEnabled        = newMode == EditMode.ReadOnly;
            filterView.EditEnabled      = listView.EditEnabled;

            detailView.EditEnabled      = newMode != EditMode.ReadOnly;
            baseView.EditEnabled        = detailView.EditEnabled;
            artikelView.EditEnabled     = detailView.EditEnabled && artikelView.Current != null;
            mnView.EditEnabled          = detailView.EditEnabled && mnView.Current != null;
            docs.SetEditMode(newMode);

            view.ShowInEditModeButtons  = newMode != EditMode.ReadOnly;
            view.ShowArtikelButtons     = detailView.Tab == SpecDetailViewModel.Tabs.Artikel &&    newMode != EditMode.ReadOnly;
            view.ShowMassnahmeButtons   = detailView.Tab == SpecDetailViewModel.Tabs.Massnahmen && newMode != EditMode.ReadOnly;
            view.ShowDocButtons         = detailView.Tab == SpecDetailViewModel.Tabs.Dokumente &&  newMode != EditMode.ReadOnly;
            view.ShowBaseButtons        = detailView.Tab == SpecDetailViewModel.Tabs.Basis &&      newMode != EditMode.ReadOnly;

            UpdateCommands();
        }


        private void SetModel(Spezifikation current)
        {

            detailView.Current  = current;
            baseView.Current    = current;
            reportView.Current  = current;

            artikelView.CurrentSpezifikation      = current;
            mnView.CurrentSpezifikation           = current;
            if (current == null) listView.Current = null;

            SetEditMode();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            try {

                if (args.PropertyName == nameof(detailView.CurrentTab) && sender == detailView) SetEditMode();
                if (args.PropertyName == nameof(artikelView.Current) && sender == artikelView) SetEditMode();
                if (args.PropertyName == nameof(mnView.Current) && sender == mnView) SetEditMode();
            }
            catch (Exception x) {

                messages.Show(x);
            }
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(artikelView, ViewPropertyChanged, String.Empty);
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, String.Empty);
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (CurrentModel != null && detailView.Current.IsChanged) {

                detailView.ResetTabsIfPrint();
                if (!await messages.ConfirmExitAsync()) return false;
            }


            view.Unload();
            listView.Unload();
            filterView.Unload();
            detailView.Unload();
            reportView.Unload();
            baseView.Unload();
            artikelView.Unload();
            mnView.Unload();
            artikelAddView.Unload();

            SetModel(null);
            await docs.UnloadAsync();
            return true;
        }


        private void UpdateCommands()
        {

            OnUIThreadFireAndForget(() => {

                editCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                copyCommand.RaiseCanExecuteChanged();
                addWertCommand.RaiseCanExecuteChanged();
                deleteWertCommand.RaiseCanExecuteChanged();
                addMassnahmeCommand.RaiseCanExecuteChanged();
                addMassnahmeCommand.RaiseCanExecuteChanged();
                addArtikelCommand.RaiseCanExecuteChanged();
                removeArtikelCommand.RaiseCanExecuteChanged();
            });
        }
    }
}
