﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Validators
{
   
    public interface IAuftragValidator
    {

        IList<String> ErrorList { get; }
    }
}
