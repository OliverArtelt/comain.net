﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ViewModels.WPlan.Planen;


namespace Comain.Client.Views.WPlan.Planen
{

    partial class TafelView
    {

        private UIElement[,] createdCells;
        private UIElement[]  createdHeads;
        private Plan model;


        public void Build(Plan model)
        {

            this.model = model;
            ResetCache();
        }


        public async Task UnloadAsync()
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Plan.Children.Clear();

                PlanViewPort.ScrollToHorizontalOffset(0);
                PlanViewPort.ScrollToVerticalOffset(0);
                RowHeaderViewPort.ScrollToHorizontalOffset(0);
                RowHeaderViewPort.ScrollToVerticalOffset(0);

                createdCells = null;
                createdHeads = null;
                model = null;
            }), null);
        }


        public void ResetCache()
        {

            Plan.Children.Clear();
            RowHeader.Children.Clear();
            createdCells = null;
            createdHeads = null;
            if (model == null) return;

            createdCells = new UIElement[model.Zeilen.Count, model.Spalten.Count];
            createdHeads = new UIElement[model.Zeilen.Count];
            RenderCells();
        }


        public void SpaltenbreitenAnpassen()
            => ResetCache();


        private double VisibleMinX  => PlanViewPort.ContentHorizontalOffset;
        private double VisibleMinY  => PlanViewPort.ContentVerticalOffset;
        private double VisibleMaxX  => VisibleMinX + PlanViewPort.ActualWidth;
        private double VisibleMaxY  => VisibleMinY + PlanViewPort.ActualHeight;


        /// <remarks>
        /// Lazy creation: nur sichtbare Zellen erstellen damit Anzeige schnell erfolgt, beim Scrollen und Resizen noch nicht erstellte Zellen dazupacken
        /// </remarks>
        private void RenderCells()
        {

            if (createdCells == null || model == null) return;

            var visibleMinCol = model.SpaltenIndex(VisibleMinX);
            var visibleMinRow = model.ZeilenIndex(VisibleMinY);
            var visibleMaxCol = model.SpaltenIndex(VisibleMinX + PlanViewPort.ActualWidth);
            if (visibleMaxCol == -1) visibleMaxCol = model.Spalten.Count;
            var visibleMaxRow = model.ZeilenIndexOrMax(VisibleMinY + PlanViewPort.ActualHeight);


            for (int rowndx = visibleMinRow; rowndx <= visibleMaxRow; rowndx++) {

                if (rowndx >= model.Zeilen.Count || rowndx == -1) continue;
                var row = model.Zeilen[rowndx];


                for (int colndx = visibleMinCol; colndx <= visibleMaxCol; colndx++) {

                    if (colndx >= model.Spalten.Count) continue;
                    if (createdCells[rowndx, colndx] != null) continue; //bereits berechnet

                    var brd = new Border { BorderThickness = new Thickness(0) };


                    brd.SetBinding(Border.HeightProperty, new Binding("Height"));
                    brd.SetBinding(Border.BackgroundProperty, new Binding("TypAsFarbe"));
                    brd.SetBinding(Border.WidthProperty, new Binding("Width"));
                    brd.SetBinding(Canvas.LeftProperty, new Binding("Left"));
                    brd.SetBinding(Canvas.TopProperty, new Binding("Top"));


                    switch (row) {

                    case MNPlanzeile mnrow:

                        var mncol = mnrow.Perioden[colndx];
                        brd.DataContext = mncol;
                        var mnlbx = new ItemsControl { Margin = new Thickness(2), VerticalAlignment = VerticalAlignment.Center };
                        mnlbx.ItemsSource = mncol.Aufgaben;
                        mnlbx.ItemTemplate = (DataTemplate)MainPanel.Resources["PlanCellAufgabe"];
                        brd.Child = mnlbx;

                        if (mnrow.Perioden[colndx].IstAktuell) {
                            brd.Background = mnrow.Zeilenfarbenindex % 2 == 0? (Brush)MainPanel.Resources["EvenCurrentBrush"]: (Brush)MainPanel.Resources["OddCurrentBrush"];
                        } else if (!mnrow.Perioden[colndx].IstBearbeitbar) {
                            brd.Background = mnrow.Zeilenfarbenindex % 2 == 0? (Brush)MainPanel.Resources["EvenDisabledBrush"]: (Brush)MainPanel.Resources["OddDisabledBrush"];
                        } else if (mnrow.Perioden[colndx].IstUnveränderlicheKette) {
                            brd.Background = mnrow.Zeilenfarbenindex % 2 == 0? (Brush)MainPanel.Resources["EvenFixedBrush"]: (Brush)MainPanel.Resources["OddFixedBrush"];
                        }
                        break;

                    case IHPlanzeile ihrow:

                        var ihcol = ihrow.Summen[colndx];
                        brd.DataContext = ihcol;
                        var ihlbx = new TextBlock { Margin = new Thickness(2), VerticalAlignment = VerticalAlignment.Center, DataContext = ihcol };
                        ihlbx.SetBinding(TextBlock.TextProperty, new Binding("Wert"));
                        ihlbx.Style = (Style)MainPanel.Resources["PlanIHSumme"];
                        brd.Child = ihlbx;
                        break;

                    case PZPlanzeile pzrow:

                        var pzcol = pzrow.Summen[colndx];
                        brd.DataContext = pzcol;
                        var pzlbx = new TextBlock { Margin = new Thickness(2), VerticalAlignment = VerticalAlignment.Center, DataContext = pzcol };
                        pzlbx.SetBinding(TextBlock.TextProperty, new Binding("Wert"));
                        pzlbx.Style = (Style)MainPanel.Resources["PlanPZSumme"];
                        brd.Child = pzlbx;
                        break;
                    }

                    Plan.Children.Add(brd);
                    createdCells[rowndx, colndx] = brd;
                }


                if (createdHeads[rowndx] != null) continue; //RowHeader bereits berechnet

                var rowhead = new Border { BorderThickness = new Thickness(0), DataContext = row };
                var lbxhead = new ItemsControl();
                lbxhead.ItemsSource = new List<IPlanzeile> { row };
                lbxhead.ItemTemplate = (DataTemplate)MainPanel.Resources["RowHeaderCell"];
                rowhead.Child = lbxhead;

                RowHeader.Children.Add(rowhead);
                rowhead.SetBinding(Border.HeightProperty, new Binding("Height"));
                rowhead.SetBinding(Canvas.TopProperty, new Binding("YStartposition"));
                createdHeads[rowndx] = rowhead;
            }
        }


        private TafelViewModel ViewModel => DataContext as TafelViewModel;


        /// <remarks>
        /// Scrollen mit dem Mausrad auf dem Plan synchronisieren
        /// </remarks>
        private void PlanScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            RowHeaderViewPort.ScrollToVerticalOffset(e.VerticalOffset);
            ColHeaderViewPort.ScrollToHorizontalOffset(e.HorizontalOffset);

            RenderCells();
            SyncVerticalScrollbar();
        }


        /// <remarks>
        /// Scrollen mit dem Mausrad auf den Zeilenköpfen synchronisieren
        /// </remarks>
        private void RowHeaderScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            PlanViewPort.ScrollToVerticalOffset(e.VerticalOffset);
            RenderCells();
            SyncVerticalScrollbar();
        }


        /// <remarks>
        /// Thumb der vertikalen Scrollbar einstellen
        /// </remarks>
        private void SyncVerticalScrollbar()
        {

            if (model == null || model.TotalHeight == 0) return;
            var pos = VisibleMinY / model.TotalHeight;
            if (ScrollY.Value != pos) ScrollY.Value = pos;
        }


        public void ScrollToLeftTop()
        {

            if (model == null) return;
            PlanViewPort.ScrollToVerticalOffset(0);
            PlanViewPort.ScrollToHorizontalOffset(0);
            RowHeaderViewPort.ScrollToVerticalOffset(0);
            ColHeaderViewPort.ScrollToHorizontalOffset(0);

            RenderCells();
            SyncVerticalScrollbar();
        }


        public void ScrollToYPosition(double value)
        {

            if (model == null) return;
            PlanViewPort.ScrollToVerticalOffset(value);
            RowHeaderViewPort.ScrollToVerticalOffset(value);

            RenderCells();
            SyncVerticalScrollbar();
        }


        private DateTime lastScrolled = DateTime.Now;
        private double lastScrollValue = 0;
        private const int scrollMillisecondsOffset = 300;
        private SemaphoreSlim scrollLocker = new SemaphoreSlim(1, 1);


        private async void ScrollY_Scroll(object sender, ScrollEventArgs e)
        {

            lastScrollValue = e.NewValue;
            if (scrollLocker.CurrentCount < 1) return;
            await scrollLocker.WaitAsync();

            try {

                var nextStamp = lastScrolled + TimeSpan.FromMilliseconds(scrollMillisecondsOffset);

                if (nextStamp > DateTime.Now) {

                    int millisecondsToWait = (int)((nextStamp - DateTime.Now).TotalMilliseconds);
                    await Task.Delay(millisecondsToWait);
                }

                var minY = model.TotalHeight * lastScrollValue;
                PlanViewPort.ScrollToVerticalOffset(minY);
                RowHeaderViewPort.ScrollToVerticalOffset(minY);
                RenderCells();

                lastScrolled = DateTime.Now;
            }
            finally { scrollLocker.Release(); }
        }
    }
}
