﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Trackers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class DeleteCommand : IUndoRedoCommand, IResultCommand
    {

        private readonly IMapper mapper;
        private List<Tuple<int, Planaufgabe>> models;


        public DeleteCommand(IEnumerable<Planaufgabe> models, IMapper mapper)
        {
            this.models = models.Select(p => new Tuple<int, Planaufgabe>(p.Parent.Aufgaben.IndexOf(p), p)).ToList();
            this.mapper = mapper;
        }


        public void Redo() => models.ForEach(p => p.Item2.Parent.Aufgaben.Remove(p.Item2));
        public void Undo() => models.ForEach(p => p.Item2.Parent.Aufgaben.Insert(p.Item1, p.Item2));


        public bool ModelHasChanged => true;


        public IEnumerable<PositionSubmitDto> Modifications() 
        {

            var dtos = mapper.Map<List<PositionSubmitDto>>(models.Select(p => p.Item2));
            dtos.ForEach(p => p.ChangeStatus = ChangeStatus.Deleted);
            return dtos;
        }
    }
}
