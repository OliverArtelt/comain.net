﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Dtos.Aufträge;


namespace Comain.Client.Services.IHObjekte
{

    public class PfadService : IPfadService
    {

        public String Baugruppenpfad(IDictionary<String, AuIHKSDto> ihdic, AuIHKSDto objekt)
        {
            return Baugruppenpfad(ihdic, objekt?.IHObjekt);
        }


        public String Baugruppenpfad(IDictionary<String, AuIHKSDto> ihdic, String nummer)
        {

            Stack<String> ReadNode(String part, Stack<String> path)
            {

                String Parent(String num)
                {
                    var parts = num.Split('-');
                    if (parts.Length < 2) return null;
                    return String.Join("-", parts.Take(parts.Length - 1));
                }

                ihdic.TryGetValue(part, out AuIHKSDto current);
                if (current == null) return path;
                path.Push(current.Maschine);
                String newnum = Parent(current.IHObjekt);
                if (newnum == null) return path;
                return ReadNode(newnum, path);
            }

            if (ihdic.IsNullOrEmpty() || String.IsNullOrEmpty(nummer)) {

                return null;

            } else {

                var path = ReadNode(nummer, new Stack<String>());
                return path.IsNullOrEmpty()? null: String.Join(" • ", path.ToArray());
            }
        }
    }
}
