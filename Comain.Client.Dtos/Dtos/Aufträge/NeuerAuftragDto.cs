﻿using System;


namespace Comain.Client.Dtos.Aufträge
{

    public class NeuerAuftragDto
    {
        
        public int       AuIHKS              { get; set; }
        public int?      Schadensbild        { get; set; }
        public int?      SubSchadensbild     { get; set; }
        public int       Dringlichkeit       { get; set; }
        public String    Kurzbeschreibung    { get; set; }
        public String    ExterneNummer       { get; set; }
        public DateTime? Termin              { get; set; }
        public int?      Abnahmeberechtigter { get; set; }
    }
}
