﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Comain.Client.Models.WPlan;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.Models;
using Comain.Client.Views.WPlan.Vorgaben;
using Comain.Client.ViewData.WPlan;
using System.Windows.Input;
using System.Threading.Tasks;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailBaseViewModel : ValidatableViewModel<IVorgabeDetailBaseView, WiMassnahme>
    {

        private readonly SynchronizationContext context;


        public VorgabeDetailBaseViewModel(IVorgabeDetailBaseView view)
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public Task SetzeListenAsync(List<IHObjektFilterModel> ihobjekte, List<NummerFilterModel> gewerke, List<LeistungsartFilterModel> leistungsarten)
        {

            return context.SendAsync(new SendOrPostCallback((o) => {

                Gewerke = gewerke;
                Leistungsarten = leistungsarten.Where(p => p.IstWI).ToList();
                IHObjekte = ihobjekte;

            }), null);
        }


        public override void CurrentChanged(WiMassnahme oldSelected)
        {

            RaisePropertyChanged(nameof(IHObjekt));
            RaisePropertyChanged(nameof(Gewerk));
            RaisePropertyChanged(nameof(Leistungsart));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(IstAktiv));
            RaisePropertyChanged(nameof(Start));
            RaisePropertyChanged(nameof(Normzeit));
            RaisePropertyChanged(nameof(Zyklus));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Wert));
            RaisePropertyChanged(nameof(ExterneAuftragsnummer));
            RaiseTermintypChanged();
        }


        public void RaiseTermintypChanged()
        {

            RaisePropertyChanged(nameof(Termin1));
            RaisePropertyChanged(nameof(Termin2));
            RaisePropertyChanged(nameof(Termin3));
            RaisePropertyChanged(nameof(Termin4));
            RaisePropertyChanged(nameof(TageSichtbar));
            RaisePropertyChanged(nameof(PeriodeSichtbar));
            RaisePropertyChanged(nameof(ZyklusSichtbar));

            RaiseWochenmusterChanged();
        }


        public void RaiseWochenmusterChanged()
        {

            RaisePropertyChanged(nameof(Montags));
            RaisePropertyChanged(nameof(Dienstags));
            RaisePropertyChanged(nameof(Mittwochs));
            RaisePropertyChanged(nameof(Donnerstags));
            RaisePropertyChanged(nameof(Freitags));
            RaisePropertyChanged(nameof(Samstags));
            RaisePropertyChanged(nameof(Sonntags));
        }


        public void Unload()
        {

            IHObjekte = null;
            Gewerke = null;
            Leistungsarten = null;
        }


#region P R O P E R T I E S


        public bool PeriodeSichtbar => TageSichtbar || ZyklusSichtbar;
        public bool TageSichtbar    => Termin3;
        public bool ZyklusSichtbar  => Termin1 || Termin2;


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }

        
        public bool Montags
        {
            get { return current != null && current.Montags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Montags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Dienstags
        {
            get { return current != null && current.Dienstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Dienstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Mittwochs
        {
            get { return current != null && current.Mittwochs; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Mittwochs = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Donnerstags
        {
            get { return current != null && current.Donnerstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Donnerstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Freitags
        {
            get { return current != null && current.Freitags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Freitags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Samstags
        {
            get { return current != null && current.Samstags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Samstags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Sonntags
        {
            get { return current != null && current.Sonntags; }
            set {

                if (Current != null && Current.Termintyp == Termintyp.Tagesplanung) {

                    Current.Sonntags = value;
                    RaiseWochenmusterChanged();
                }
            }
        }


        public bool Termin1
        {
            get { return current != null && current.Termintyp == Termintyp.UnveränderlicheKette; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.UnveränderlicheKette) {

                    Current.Termintyp = Termintyp.UnveränderlicheKette;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin2
        {
            get { return current != null && current.Termintyp == Termintyp.VeränderlicheKette; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.VeränderlicheKette) {

                    Current.Termintyp = Termintyp.VeränderlicheKette;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin3
        {
            get { return current != null && current.Termintyp == Termintyp.Tagesplanung; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.Tagesplanung) {

                    Current.Termintyp = Termintyp.Tagesplanung;
                    RaiseTermintypChanged();
                }
            }
        }


        public bool Termin4
        {
            get { return current != null && current.Termintyp == Termintyp.Bedarfsposition; }
            set {

                if (Current != null && value && Current.Termintyp != Termintyp.Bedarfsposition) {

                    Current.Termintyp = Termintyp.Bedarfsposition;
                    RaiseTermintypChanged();
                }
            }
        }


        public int? Normzeit
        {
            get { return current?.Normzeit; }
            set {

                if (Current != null && Current.Normzeit != value) {

                    Current.Normzeit = value;
                    RaisePropertyChanged(nameof(Normzeit));
                }
            }
        }


        public int? Menge
        {
            get { return current?.Menge; }
            set {

                if (Current != null && Current.Menge != value) {

                    Current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public decimal? Wert
        {
            get { return current?.Wert; }
            set {

                if (Current != null && Current.Wert != value) {

                    Current.Wert = value;
                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public int Zyklus
        {
            get { return current == null? 0: Current.Zyklus; }
            set {

                if (Current != null && Current.Zyklus != value) {

                    Current.Zyklus = value;
                    RaisePropertyChanged(nameof(Zyklus));
                }
            }
        }


        public DateTime? Start
        {
            get { return current?.Start; }
            set {

                if (Current != null && Current.Start != value) {

                    Current.Start = value;
                    RaisePropertyChanged(nameof(Start));
                }
            }
        }


        public bool? IstAktiv
        {
            get { return current?.IstAktiv; }
            set {

                if (Current != null && Current.IstAktiv != value.GetValueOrDefault()) {

                    Current.IstAktiv = value.GetValueOrDefault();
                    RaisePropertyChanged(nameof(IstAktiv));
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return current?.Kurzbeschreibung; }
            set {

                if (Current != null && Current.Kurzbeschreibung != value) {

                    Current.Kurzbeschreibung = value;
                    RaisePropertyChanged(nameof(Kurzbeschreibung));
                }
            }
        }


        public String Nummer
        {
            get { return current?.Nummer; }
            set {

                if (Current != null && Current.Nummer != value) {

                    Current.Nummer = value;
                    RaisePropertyChanged(nameof(Nummer));
                }
            }
        }


        public String Name
        {
            get { return current?.Name; }
            set {

                if (Current != null && Current.Name != value) {

                    Current.Name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }


        public String ExterneAuftragsnummer
        {
            get { return current?.ExterneAuftragsnummer; }
            set {

                if (Current != null && Current.ExterneAuftragsnummer != value) {

                    Current.ExterneAuftragsnummer = value;
                    RaisePropertyChanged(nameof(ExterneAuftragsnummer));
                }
            }
        }


        public IHObjektFilterModel IHObjekt
        {
            get { return current?.IHObjekt; }
            set {

                if (Current != null && Current.IHObjekt != value) {

                    Current.IHObjekt = value;
                    RaisePropertyChanged(nameof(IHObjekt));
                }
            }
        }


        public NummerFilterModel Gewerk
        {
            get { return current?.Gewerk; }
            set {

                if (Current != null && Current.Gewerk != value) {

                    Current.Gewerk = value;
                    RaisePropertyChanged(nameof(Gewerk));
                }
            }
        }


        public LeistungsartFilterModel Leistungsart
        {
            get { return current?.Leistungsart; }
            set {

                if (Current != null && Current.Leistungsart != value) {

                    Current.Leistungsart = value;
                    RaisePropertyChanged(nameof(Leistungsart));
                }
            }
        }


        private List<IHObjektFilterModel> iHObjekte;
        public List<IHObjektFilterModel> IHObjekte
        {
            get { return iHObjekte; }
            set { SetProperty(ref iHObjekte, value); }
        }


        private List<NummerFilterModel> gewerke;
        public List<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


        private List<LeistungsartFilterModel> leistungsarten;
        public List<LeistungsartFilterModel> Leistungsarten
        {
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }


#endregion

    }
}
