﻿using System;
using System.Threading;
using System.Threading.Tasks;


namespace Comain.Client.Controllers
{

    public interface IControllerBus
    {

        CancellationToken   Token           { get; }
        void                CancelToken();

        T                   GetController<T>() where T: IController;
        IController         GetController(Type type);

        /// <summary>
        /// Controller ruft anderen Controller auf, Aufrufer auf Stapel für den Rücksprung packen
        /// </summary>
        Task JumpAsync(ControllerMessage message);

        /// <summary>
        /// Controller vom Hauptmenü aus anspringen
        /// </summary>
        Task JumpAsync(Type type, object parameter = null);


        Task<ControllerMessage> CallAsync(ControllerMessage message);

        /// <summary>
        /// obersten auf dem Stapel liegenden Controller rückrufen
        /// </summary>
        Task BackAsync(object result = null);
    }
}
