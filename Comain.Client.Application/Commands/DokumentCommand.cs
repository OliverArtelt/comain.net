﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class DokumentCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IDokumentController);
        public String       Name        => "Dokumente";
        public String       Parameter   => "Dokumente";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 500;
        public String       Color       => "Chocolate";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.book.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
            => Task.FromResult(true);
    }
}
