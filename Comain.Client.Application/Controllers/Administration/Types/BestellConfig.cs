﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.ViewModels.Stock.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class BestellConfig : IAdminTyp
    {

        public String       Title           => "Bestellungen";
        public int          Sortierung      => 20000;
        public Type         Controller      => typeof(IConfigController<BestellViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulStock;
    }
}
