﻿using CO.OpenXML.XLSX.ExcelStyles;

namespace Comain.Client.ViewModels.WPlan.Reports
{
    public interface IStyleFactory    
    {
        Style AuCode                    { get; }
        Style AuKopfC                   { get; }
        Style AuKopfCW                  { get; }
        Style AuKopfCWAktuell           { get; }
        Style AuKopfDelta               { get; }
        Style AuKopfL                   { get; }
        Style AuKopfR                   { get; }
        Style AuKurzB                   { get; }
        Style AuMoney                   { get; }
        Style AuNumeric                 { get; }
        Style AuText                    { get; }
        Style Datum                     { get; }
        Style FootText                  { get; }
        Style FootValue                 { get; }
        Style GreaterKey                { get; }
        Style Head                      { get; }
        Style IHObjekt                  { get; }
        Style Params                    { get; }
        Style Bearbeiter                { get; }
        Style Standort                  { get; }
        Style SumMoney                  { get; }

        Style KeinErgebnis              { get; }
        Style ErgebnisOffen             { get; }
        Style ErgebnisOK                { get; }
        Style ErgebnisNotOK             { get; }
        Style ErgebnisFortgeführt       { get; }
        Style ErgebnisWirdNichtErledigt { get; }
    }
}