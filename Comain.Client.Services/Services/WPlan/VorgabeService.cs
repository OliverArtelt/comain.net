﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.WPlan
{

    public class VorgabeService : IVorgabeService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;

        
        public VorgabeService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<List<MassnahmeListDto>> ListAsync(int ihid, CancellationToken ct = default)
        {

            var uri = $"/Massn/List/{ihid}";
            var dtolist = await store.GetAsync<List<MassnahmeListDto>>(uri, ct).ConfigureAwait(false);
            return dtolist.OrderBy(p => p.Status).ThenBy(p => p.Name).ToList();
        }


        public async Task<WiMassnahme> DetailAsync(int idMassnahme, CancellationToken ct = default)
        {

            var uri = $"/Massn/{idMassnahme}";
            var dto = await store.GetAsync<WiMassnahmeDto>(uri, ct).ConfigureAwait(false);
            var model = mapper.Map<WiMassnahme>(dto);
            if (!dto.Medien.IsNullOrEmpty()) model.Medien = new TrackableCollection<Media>(dto.Medien.Select(p => new Media(p)));

            model.AcceptChanges();
            return model;
        }


        public async Task<WiMassnahme> UpdateAsync(WiMassnahme model, CancellationToken ct = default)
        {

            var dto = mapper.Map<WiMassnahmeDto>(model);
            dto = await store.PostAsync<WiMassnahmeDto, WiMassnahmeDto>("/Massn", dto, ct).ConfigureAwait(false);
            model = mapper.Map<WiMassnahme>(dto);
            if (!dto.Medien.IsNullOrEmpty()) model.Medien = new TrackableCollection<Media>(dto.Medien.Select(p => new Media(p)));

            model.AcceptChanges();
            return model;
        }


        public async Task DeleteAsync(int idMassnahme, CancellationToken ct = default)
        {

            var uri = $"/Massn/{idMassnahme}";
            await store.DeleteAsync(uri, ct).ConfigureAwait(false);
        }


        public Task<List<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default)
        {
 	        return FilterAsync<LeistungsartFilterModel>("/Filter/LA", ct);
        }


        public Task<List<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/GW", ct);   
        }


        public Task<List<IHObjektFilterModel>> IHObjekteAsync(CancellationToken ct = default)
        {
            return FilterAsync<IHObjektFilterModel>("/Filter/IH", ct);   
        }


        public Task<List<NummerFilterModel>> EinheitenAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/EH", ct);   
        }


        public Task<List<MaterialstammFilterModel>> MaterialstammAsync(CancellationToken ct = default)
        {
            return FilterAsync<MaterialstammFilterModel>("/Filter/MT", ct);   
        }


        private Task<List<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<List<TFilterModel>>(url, ct);
        }


        public Task<List<Media>> ReadFilesAsync(IEnumerable<String> paths, CancellationToken ct = default)
        {

            return Task.Run(() => {

                if (paths.IsNullOrEmpty()) return null;
                var models = paths.AsParallel().Select(p => new Media(p)).ToList();
                return models;
            });
        }


        public async Task<IList<EreignisViewItem>> EreignisseAsync(int mnid, CancellationToken ct = default)
        {
          
            var url = $"/Massn/Ereignisse/{mnid}";
            var dtos = await store.GetAsync<IList<EreignisDto>>(url, ct).ConfigureAwait(false);
            if (dtos.IsNullOrEmpty()) return null;
            return dtos.Select(p => new EreignisViewItem(p)).OrderBy(p => p.FälligAm).ToList();
        }
    }
}
