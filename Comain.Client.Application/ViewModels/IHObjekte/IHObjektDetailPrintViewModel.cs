﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{
    
    public class IHObjektDetailPrintViewModel : PrintViewModel<IIHObjektDetailPrintView>
    {

        enum PrintType { Detail, Barcode, QrCode }


        public IHObjektDetailPrintViewModel(IMessageService messages, IIHObjektDetailPrintView view) : base(messages, view)
        {}


#region P R O P E R T I E S


        private PrintType type = PrintType.Detail;
        

        public bool DruckVollständig
        {
            get { return type == PrintType.Detail; }
            set { 
                
                if (value && type != PrintType.Detail) {
                
                    type = PrintType.Detail;
                    RaisePropertyChanged("DruckVollständig");
                    RaisePropertyChanged("DruckBarcode");
                    RaisePropertyChanged("DruckQrCode");
                }
            }
        }


        public bool DruckBarcode
        {
            get { return type == PrintType.Barcode; }
            set { 
                
                if (value && type != PrintType.Barcode) {
                
                    type = PrintType.Barcode;
                    RaisePropertyChanged("DruckVollständig");
                    RaisePropertyChanged("DruckBarcode");
                    RaisePropertyChanged("DruckQrCode");
                }
            }
        }


        public bool DruckQrCode
        {
            get { return type == PrintType.QrCode; }
            set { 
                
                if (value && type != PrintType.QrCode) {
                
                    type = PrintType.QrCode;
                    RaisePropertyChanged("DruckVollständig");
                    RaisePropertyChanged("DruckBarcode");
                    RaisePropertyChanged("DruckQrCode");
                }
            }
        }


#endregion

    }
}
