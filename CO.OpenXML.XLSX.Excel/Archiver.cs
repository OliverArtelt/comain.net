﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace CO.OpenXML
{

    /// <summary>
    /// Wrapper OpenXML C++-System to DotNetZip
    /// </summary>
    /// <remarks>
    /// primitiver Zip-Archiver,
    /// reicht zum Erstellen/Auslesen von Archiven
    /// beim Bearbeiten muß neues temporäres Archiv erstellt und die Dateien umkopiert werden
    /// </remarks>
    public class Archiver
    {

        /// <summary>
        /// Pfadangabe im Archiv
        /// </summary>
        class PathInfo
        {
            /// <summary>
            /// Basisname der Datei (ohne Pfad)
            /// </summary>
            public string Base { get; private set; }
            /// <summary>
            /// Pfad des Verzeichnissen der Datei
            /// </summary>
            public string Path { get; private set; }

            /// <summary>
            /// relativer Pfad im Archiv abtrennen
            /// </summary>
            /// <param name="path"></param>
            /// <returns></returns>
            public PathInfo(string path)
            {

                FileInfo nfo = new (path);
                if (nfo.Name == path) {

                    Base = path;
                    Path = String.Empty;

                    return;
                };

                int dirlen = path.Length - nfo.Name.Length - 1;

                Base = nfo.Name;
                Path = path.Substring(0, dirlen);
            }
        }


        public void OpenArchive(String oldpath)
        {

          //  if (oldArchive != null) oldArchive.Dispose();
          //  oldArchive = ZipFile.Read(oldpath);
        }


        public void CreateArchive(String newpath)
        {

          //  if (newArchive != null) newArchive.Dispose();
          //  File.Delete(newpath);
          //  newArchive = new ZipFile(newpath);
        }


        /// <summary>
        /// Items im Archiv listen
        /// </summary>
        /// <returns></returns>
        public IList<String> ListContent() => ListContent(false);

        /// <summary>
        /// Items im Archiv listen
        /// </summary>
        /// <param name="newroot">Pfadangaben auf den kleinsten gemeinsamen Nenner reduzieren</param>
        /// <returns></returns>
        public IList<String> ListContent(bool newroot)
        {

          //  if (oldArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");

          //  oldArchive.FlattenFoldersOnExtract = newroot;
          //  return oldArchive.EntryFileNames;
            return null;
        }

        /// <summary>
        /// Item auslesen
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string ExtractAsString(String file)
        {

            //UTF8Encoding enc = new UTF8Encoding();
            //return enc.GetString(ExtractAsBytes(file));
            return default;
        }

        /// <summary>
        /// Item auslesen
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public byte[] ExtractAsBytes(String file)
        {

        //   if (oldArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");
        //
        //   MemoryStream s = new MemoryStream();
        //   oldArchive[file].Extract(s);
        //   return s.ToArray();
            return default;
        }

        /// <summary>
        /// Item einspeichern
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="dstdir"></param>
        public void ImportFromMemory(String buffer, String fileName)
        {

         //   if (newArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");

         //   PathInfo p = new PathInfo(fileName);
         //   newArchive.AddEntry(p.Base, p.Path, buffer);
        }

        /// <summary>
        /// Item einspeichern
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="dstdir"></param>
        public void ImportFromMemory(byte[] buffer, String fileName)
        {

          //  if (newArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");

          //  PathInfo p = new PathInfo(fileName);
          //  newArchive.AddEntry(p.Base, p.Path, buffer);
        }

        /// <summary>
        /// Item von Datei einspeichern
        /// </summary>
        /// <param name="path"></param>
        public void ImportFromFile(String path)
        {

         //   if (newArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");

         //   newArchive.AddFile(path);
        }

        /// <summary>
        /// Item von Datei einspeichern
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dstdir"></param>
        public void ImportFromFile(String fileName, String directoryPathInArchive)
        {

         //   if (newArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");
         //   newArchive.AddFile(fileName, directoryPathInArchive);
        }

        /// <summary>
        /// Item umkopieren
        /// </summary>
        /// <param name="path"></param>
        public void CopyFile(String fileName)
        {

         //  if (newArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");
         //  if (oldArchive == null) throw new ApplicationException("Datei kann nicht erstellt oder geöffnet werden.");
         //
         //  MemoryStream s = new MemoryStream();
         //  oldArchive[fileName].Extract(s);
         //
         //  PathInfo p = new PathInfo(fileName);
         //  newArchive.AddEntry(p.Base, p.Path, s.GetBuffer());
        }

        /// <summary>
        /// neues Archiv in das Dateisystem schreiben
        /// </summary>
        public void Save()
        {
          //  newArchive.Save();
        }
    }
}
