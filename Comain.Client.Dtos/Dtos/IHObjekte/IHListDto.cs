﻿using System;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHListDto
    {

        public int          Id                      { get; set; }
        public String       Nummer                  { get; set; }
        public String       Name                    { get; set; }
        public String       Inventarnummer          { get; set; }
        public String       Bauteil                 { get; set; }
        public String       Baugruppe               { get; set; }
        public String       Fabriknummer            { get; set; }
        public String       Scannummer              { get; set; }
        public String       Typ                     { get; set; }
        public String       Anlage                  { get; set; }
        public String       TechnischeDaten         { get; set; }
        public String       Bemerkungen             { get; set; }
        public int          Kostenstelle_Id         { get; set; }
        public int          Standort_Id             { get; set; }
        public int?         Erhaltklasse            { get; set; }
        public int?         Status                  { get; set; }
    }
}
