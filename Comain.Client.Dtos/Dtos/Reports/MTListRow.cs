﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class MTListRow
    {

        public String       Auftragscode        { get; set; }
        public String       IHObjektnummer      { get; set; }
        public DateTime     Datum               { get; set; }
        public String       Materialbezeichnung { get; set; }
        public String       Materialnummer      { get; set; }
        public String       Lieferant           { get; set; }
        public String       Einheit             { get; set; }
        public decimal      Menge               { get; set; }
        public decimal      Einzelpreis         { get; set; }
        public decimal?     Gesamtpreis         { get; set; }
        public decimal?     Mwst                { get; set; }
        public String       Status              { get; set; }
    }
}
