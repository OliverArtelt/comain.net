﻿using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;
using Comain.Client.Models;


namespace Comain.Client.Services
{

    public interface IKonfigService
    {
      
        Task<Konfiguration> GetAsync(CancellationToken ct = default);
        Task<Konfiguration> SetAsync(Konfiguration model, CancellationToken ct = default);
    }
}
