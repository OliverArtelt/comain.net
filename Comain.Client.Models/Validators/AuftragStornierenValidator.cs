﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Validators
{

    public class AuftragStornierenValidator : AuftragSpeichernValidator, IAuftragValidator
    {
 
        public AuftragStornierenValidator(Auftrag model) : base(model)
        {}
       

        public override IList<String> ErrorList
        {
            
            get { 
            
                var result = base.ErrorList;
                if (model.Status == Auftragstatus.Abgeschlossen) result.Add("Der Auftrag ist bereits abgeschlossen.");
                if (model.Status == Auftragstatus.Storno) result.Add("Der Auftrag ist bereits storniert.");
                if (!model.Personalleistungen.IsNullOrEmpty()) result.Add("Der Auftrag besitzt Personalleistungen.");
                if (!model.Materialleistungen.IsNullOrEmpty()) result.Add("Der Auftrag besitzt Materialleistungen.");

                return result;
            }
        }
    }
}
