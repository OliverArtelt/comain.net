﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IFeiertagController : IVwTypController 
    {}
    
    
    public class FeiertagController : SimpleController<FeiertagDto, Feiertag, IFeiertagView>, IFeiertagController
    {
        
        public FeiertagController(IMessageService messages, IControllerBus bus, ISimpleService<FeiertagDto, Feiertag> store, 
                                  VerwaltungViewModel mainView, SimpleViewModel<Feiertag, IFeiertagView> view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/FT"; }
        }
    }
}
