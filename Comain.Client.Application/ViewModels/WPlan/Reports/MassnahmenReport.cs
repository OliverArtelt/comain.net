﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.Services.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class MassnahmenReport : IPlanReport
    {

        private readonly IPlanService store;

        public bool     BenötigtPlan            => false;
        public String   Name                    => "Maßnahmenkatalog";
        public int      Sortierung              => 100;

        public bool     UseFilterAusgabe        => false;
        public bool     UseFilterLeistungstyp   => false;
        public bool     UseFilterErgebnis       => false;


        public MassnahmenReport(IPlanService store)
            => this.store = store;


        public async Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo info, PlanOptions options, CancellationToken ct = default)
        {

            var report = new ReportResult { ReportAddress = "Comain.Client.Reports.MassnahmeReport.rdlc",
                                            AssemblyAddress = "comain.Client.Reports",
                                            Title = "Maßnahmenkatalog" };
            var maindata = await store.MassnahmenReportAsync(info, ct).ConfigureAwait(false);
            report.AddDataSource("MainData", maindata);
            return report;
        }


        public String   Text                => @"
Der Maßnahmenkatalog zeigt alle zyklischen Wartungsmaßnahmen für die gewählten Assets bzw. Standorte. Verwenden Sie diesen Bericht, um zu sehen, welche Maßnahmen für welches Gewerk wie häufig geplant wurden und welches Ergebnis bei der letzten Durchführung vorlag. 
Wählen Sie einfach die relevanten Standorte oder Assets, gehen Sie auf Suchen und betätigen Sie danach die Schaltfläche ""Maßnahmenkatalog"".
Bitte beachten Sie, dass nur die aktiven Maßnahmen angezeigt werden. Historische (inaktive) Maßnahmen sehen Sie in der Anlagenverwaltung.";
    }
}
