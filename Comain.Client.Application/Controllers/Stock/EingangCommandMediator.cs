﻿using System;
using System.Security;
using Comain.Client.Services;
using Comain.Client.ViewModels.Stock.Wareneingänge;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Wareneingänge
{

    public class EingangCommandMediator : IEingangCommandMediator
    {

        private readonly ILoginService login;
        private readonly EingangViewModel view;
        private readonly EingangDetailViewModel detailView;
        private readonly EingangListViewModel listView;
        private readonly EingangFilterViewModel filterView;


        public EingangCommandMediator(ILoginService login, EingangViewModel view, EingangDetailViewModel detailView, 
                                      EingangListViewModel listView, EingangFilterViewModel filterView)
        {

            this.login = login;
            this.view = view;
            this.detailView = detailView;
            this.listView = listView;
            this.filterView = filterView;
        }


        private bool HasCurrent     { get { return detailView.Current != null; } }
        private bool IsAuthorized   { get { return login.AktuellerNutzer.IstAdministrator || login.AktuellerNutzer.IstLagerEinlagerungen; } }
        
        public EditMode CurrentMode { get; private set; }


        public void SetEditMode(EditMode newMode)
        {
        
            if (newMode == EditMode.Edit   && !IsAuthorized) throw new UnauthorizedAccessException("Sie dürfen diese Bestellung nicht bearbeiten.");
            if (newMode == EditMode.Append && !IsAuthorized) throw new UnauthorizedAccessException("Sie dürfen diese Bestellung nicht bearbeiten.");

            CurrentMode = newMode;
            detailView.EditEnabled  = newMode != EditMode.ReadOnly; 
            listView.EditEnabled    = newMode == EditMode.ReadOnly; 
            filterView.EditEnabled  = newMode == EditMode.ReadOnly; 

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            view.EditVisible        = CurrentMode == EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
            view.CancelVisible      = CurrentMode != EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
            view.SaveVisible        = CurrentMode != EditMode.ReadOnly && IsAuthorized;
            view.ReloadVisible      = CurrentMode == EditMode.ReadOnly && IsAuthorized;

            view.AddVisible         = CurrentMode == EditMode.ReadOnly && IsAuthorized;
            view.DeleteVisible      = CurrentMode != EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
       
            view.AddPosVisible      = CurrentMode != EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
            view.DeletePosVisible   = CurrentMode != EditMode.ReadOnly && IsAuthorized && detailView.Current != null;

            view.LagerVisible       = CurrentMode != EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
            view.ReklamVisible      = CurrentMode == EditMode.ReadOnly && IsAuthorized && detailView.Current != null;
        }
    }
}
