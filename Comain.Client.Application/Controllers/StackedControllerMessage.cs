﻿using System.Threading.Tasks;

namespace Comain.Client.Controllers
{

    public class StackedControllerMessage
    {

        public ControllerMessage Message             { get; set; }
        public int               BreadcrumbDepth     { get; set; }
        public object            View                { get; set; }

        public TaskCompletionSource<ControllerMessage> WaitableResult      { get; set; }
    }
}
