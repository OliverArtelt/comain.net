﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Reports;
using Comain.Client.Models.WPlan;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.WPlan
{

    public class ChecklistService : IChecklistService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public ChecklistService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<TrackableCollection<Checklistposition>> ChecklisteAsync(int auid, CancellationToken ct = default)
        {

            var uri = $"/ChkLst/AU/{auid}";
            var dtolist = await store.GetAsync<List<ChecklistPositionDto>>(uri, ct).ConfigureAwait(false);
            if (dtolist == null) return new TrackableCollection<Checklistposition>();
            var models = mapper.Map<List<Checklistposition>>(dtolist);
            models.ForEach(p => p.InVorigerSitzungErledigt = p.Ergebnis != Checklistergebnis.Undefiniert);
            return new TrackableCollection<Checklistposition>(models);
        }


        public async Task<List<Media>> MedienAsync(int massnahmeId, CancellationToken ct = default)
        {

            var uri = $"/ChkLst/Media/{massnahmeId}";
            var dto = await store.GetAsync<WiMassnahmeDto>(uri, ct).ConfigureAwait(false);
            if (dto.Medien.IsNullOrEmpty()) return null;

            var medien = new List<Media>(dto.Medien.Select(p => new Media(p)));
            return medien;
        }


        public Task SaveAsync(Wartungsergebnis model, CancellationToken ct = default)
        {

            var dto = mapper.Map<WartungsergebnisDto>(model);
            dto.Erledigte = model.Erledigte.Select(p => p.AsDto()).ToList();
            return store.PostAsync<WartungsergebnisDto>($"/ChkLst/AU/", dto, ct);
        }


        public Task<AuftragDruckDto> ReportAsync(Suchkriterien criteria, CancellationToken ct = default)
        {
            return store.FindAsync<AuftragDruckDto>($"/ChkLst/Report/", criteria, ct);
        }


        public Task<List<PositionInfo>> LEPositionAsync(int leid, CancellationToken ct = default)
        {
            return store.GetAsync<List<PositionInfo>>($"/ChkLst/LE/{leid}", ct);
        }


        public Task<List<PositionInfo>> MTPositionAsync(int mtid, CancellationToken ct = default)
        {
            return store.GetAsync<List<PositionInfo>>($"/ChkLst/MT/{mtid}", ct);
        }


        public Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);
        }


        public Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct);
        }
    }
}
