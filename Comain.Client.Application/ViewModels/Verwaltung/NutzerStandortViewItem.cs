﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Verwaltung
{

    public class NutzerStandortViewItem
    {

        private readonly PzFilterModel model;


        public HashSet<int> Standorte   { get; private set; }


        public NutzerStandortViewItem(PzFilterModel model, HashSet<int> standorte)
        {

            this.model = model;
            this.Standorte = standorte ?? new HashSet<int>();
        }


        public int      Nummer      => model.Nummer;
        public String   Name        => model.Name;
        public String   Kurzname    => model.Kurzname;


        public bool IsSelected
        {
            get => Standorte.Contains(Nummer);
            set {
            
                if (value && !IsSelected) Standorte.Add(Nummer);
                else Standorte.Remove(Nummer);
            }
        }
    }
}
