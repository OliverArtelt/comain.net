﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHSimpleSelektorViewModel : ViewModel<IIHSimpleSelektorView>, IDelayableViewModel
    {

        private readonly IKonfigService configs;
        private readonly SynchronizationContext context;

        /// <summary>
        /// IH-Objekte im PZ-Baum
        /// </summary>
        private Dictionary<int, TreeIHViewItem> pzTreeItems;
        /// <summary>
        /// alle IH-Objekte
        /// </summary>
        private IDictionary<String, IHFilterItem> currentIHDic;
        private Konfiguration cfg;


        public IHSimpleSelektorViewModel(IKonfigService configs, IIHSimpleSelektorView view) : base(view)
        {

            context = SynchronizationContext.Current;
            this.configs = configs;
        }


        public async Task LoadAsync(List<IHFilterItem> ihlist, IList<PzFilterModel> pzlist, CancellationToken ct = default)
        {

            ct.ThrowIfCancellationRequested();

            using (new VisualDelay(this)) {

                cfg = await configs.GetAsync(ct);
                currentIHDic = ihlist.DistinctBy(p => p.Nummer).ToDictionary(p => p.Nummer);
                await GeneratePzTreeAsync(pzlist, ct).ConfigureAwait(false);
            }
        }


        private async Task GeneratePzTreeAsync(IEnumerable<PzFilterModel> pzs, CancellationToken ct)
        {

            await Task.Run(() => {

                Standorte = pzs.OrderBy(p => p.Nummer).Select(p => new TreePZViewItem(p)).ToList();

                var list = currentIHDic.Values.OrderBy(p => p.Nummer).Select(p => new TreeIHViewItem(p, cfg)).ToList();
                pzTreeItems = list.ToDictionary(p => p.FilterModel.Id);
                var ihdic = list.ToDictionary(p => p.IHNummer);

                list.ForEach(p => {

                    if (!p.IstMaschine && ihdic.ContainsKey(p.NummerParent)) {

                        ihdic[p.NummerParent].Add(p);
                        p.Parent = ihdic[p.NummerParent];
                    }
                });

                ct.ThrowIfCancellationRequested();

                var pzdic = Standorte.ToDictionary(p => p.Model.Id);
                list.Where(p => p.IstMaschine && pzdic.ContainsKey(p.FilterModel.Standort_Id))
                    .ForEach(p => {

                        pzdic[p.FilterModel.Standort_Id].Add(p);
                        p.Parent = pzdic[p.FilterModel.Standort_Id];
                    });

                Standorte = Standorte.Where(p => !p.SubItems.IsNullOrEmpty()).OrderBy(p => p.Nummer).ToList();
                RaisePropertyChanged("Standorte");
            });
        }


        /// <summary>
        /// von View aufgerufen: Benutzer hat auf einen Knoten geklickt -> IH laden
        /// </summary>
        public void SelectItem(object node)
        {

            if (!EditEnabled) return;
            var ihnode = node as TreeIHViewItem;
            if (ihnode != null) {       //IHO gewählt oder Tab-Umschaltung

                CurrentIHObjekt = ihnode.FilterModel;

            } else {

                CurrentIHObjekt = null;
            }

            RaisePropertyChanged("CurrentIHObjekt");
        }


        public void Clear()
        {

            if (CurrentIHObjekt == null) return;
            if (pzTreeItems == null) return;
            var current = pzTreeItems.GetValueOrDefault(CurrentIHObjekt.Id);
            if (current == null) return;
            current.IsSelected = false;
            CurrentIHObjekt = null;
        }


        public void Unload()
        {

            CurrentIHObjekt = null;
            Standorte = null;
            pzTreeItems = null;

            RaisePropertyChanged("Standorte");
            RaisePropertyChanged("Kostenstellen");
        }


        private void ExpandCurrentNode()
        {

            if (CurrentIHObjekt == null) return;
            if (pzTreeItems == null) return;
            var current = pzTreeItems.GetValueOrDefault(CurrentIHObjekt.Id);
            if (current == null) return;

            ExpandCurrentNode(current);
            current.IsSelected = true;
        }


        private void ExpandCurrentNode(TreeViewItem node)
        {

            node.IsExpanded = true;
            if (node.Parent != null) ExpandCurrentNode(node.Parent);
        }


#region P R O P E R T I E S


        private IHFilterItem currentIHObjekt;
        public IHFilterItem CurrentIHObjekt
        {
            get { return currentIHObjekt; }
            set { SetProperty(ref currentIHObjekt, value); }
        }


        private bool editEnabled = true;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public  List<TreePZViewItem> Standorte { get; private set; }


#endregion


    }
}
