﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CO.OpenXML.XLSX.ExcelStyles;
using FontStyle = CO.OpenXML.XLSX.ExcelStyles.FontStyle;


namespace Comain.Client.ViewModels.WPlan.Reports
{

    public class StyleFactory : IStyleFactory
    {

        private static Color colorNone              = Color.White;
        private static Color colorOffen             = Color.WhiteSmoke;
        private static Color colorOK                = Color.FromArgb(0xc2, 0xf0, 0xc2);
        private static Color colorNotOK             = Color.FromArgb(0xff, 0xc8, 0xbf);
        private static Color colorFortgeführt       = Color.FromArgb(0xbc, 0xde, 0xff);
        private static Color colorWirdNichtErledigt = Color.FromArgb(0xe0, 0xc2, 0xf0);


        public Style Head                       { get; } = new Style { Font = new FontStyle(12, Color.Black, true) };
        public Style Params                     { get; } = new Style { Font = new FontStyle(10, Color.Black, true) };
        public Style Bearbeiter                 { get; } = new Style { Font = new FontStyle(10, Color.Black, true),
                                                                       Color = new BackColor(Color.Silver),
                                                                       Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public Style Standort                   { get; } = new Style { Font = new FontStyle(10, Color.Black, true),
                                                                       Color = new BackColor(Color.Gainsboro),
                                                                       Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public Style IHObjekt                   { get; } = new Style { Font = new FontStyle(10, Color.Black, true),
                                                                       Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public Style AuKopfL                    { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Left),
                                                                       Color = new BackColor(Color.LightCyan) };
        public Style AuKopfC                    { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Center),
                                                                       Color = new BackColor(Color.LightCyan) };
        public Style AuKopfR                    { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Right),
                                                                       Color = new BackColor(Color.LightCyan) };
        public Style AuKopfCW                   { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Center),
                                                                       Color = new BackColor(Color.LightCyan) };
        public Style AuKopfCWAktuell            { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Center),
                                                                       Color = new BackColor(Color.Gold) };
        public Style AuKopfDelta                { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Center),
                                                                       Color = new BackColor(Color.Orange) };
        public Style AuCode                     { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        public Style AuKurzB                    { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Left, true) };
        public Style AuText                     { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style AuNumeric                  { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Right, AlignTypes.Center) };
        public Style AuMoney                    { get; } = new Style { Font = new FontStyle(8),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Right, AlignTypes.Center),
                                                                       Format = new NumberFormat(Format.Money) };
        public Style Datum                      { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style SumMoney                   { get; } = new Style { Font = new FontStyle(8) };
        public Style FootText                   { get; } = new Style { Font = new FontStyle(8) };
        public Style FootValue                  { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Align = new Alignment(AlignTypes.Right) };
        public Style GreaterKey                 { get; } = new Style { Font = new FontStyle(8) };

        public Style KeinErgebnis               { get; } = new Style { Border = new Border(BorderType.Thin) };
        public Style ErgebnisOffen              { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Color = new BackColor(colorOffen),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style ErgebnisOK                 { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Color = new BackColor(colorOK),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style ErgebnisNotOK              { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Color = new BackColor(colorNotOK),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style ErgebnisFortgeführt        { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Color = new BackColor(colorFortgeführt),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        public Style ErgebnisWirdNichtErledigt  { get; } = new Style { Font = new FontStyle(8, Color.Black, true),
                                                                       Color = new BackColor(colorWirdNichtErledigt),
                                                                       Border = new Border(BorderType.Thin),
                                                                       Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
    }
}
