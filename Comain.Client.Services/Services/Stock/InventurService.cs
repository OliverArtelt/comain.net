﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Stock;
using Comain.Client.Gateways;


namespace Comain.Client.Services.Stock.Inventuren
{

    public class InventurService : IInventurService
    {

        private readonly IRepository store;


        public InventurService(IRepository store)
        {
            this.store = store;
        }

        
        public Task<IList<InventurDto>> ReadAsync(CancellationToken ct = default)
        {
            return store.GetAsync<IList<InventurDto>>("/Inventur", ct);
        }

        
        public Task SaveAsync(IList<InventurDto> changes, CancellationToken ct = default)
        {
            return store.PostAsync<IList<InventurDto>>("/Inventur", changes, ct);
        }
    }
}
