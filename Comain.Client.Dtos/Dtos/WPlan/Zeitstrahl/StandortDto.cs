﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class StandortDto
    {

        public int                  Id           { get; set; }
        public int                  Nummer       { get; set; }
        public String               Name         { get; set; }

        public List<IHObjektDto>    IHObjekte    { get; set; }
    }
}
