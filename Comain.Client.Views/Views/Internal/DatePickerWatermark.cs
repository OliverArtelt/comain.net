﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;


namespace Comain.Client.Views.Internal
{

    /// <summary>
    /// englisches hardkodiertes 'Select a date' in DatePickern in deutsches hardkodiertes 'Datum wählen' wandeln
    /// </summary>
    /// <remarks>
    /// http://matthamilton.net/datepicker-watermark
    /// </remarks>
    public static class DatePickerWatermark
    {

        
        public static void RegisterDatePicker()
        {
            
            EventManager.RegisterClassHandler(typeof(DatePicker),
                                              DatePicker.LoadedEvent, 
                                              new RoutedEventHandler(DatePicker_Loaded));
        }


        static T GetChildOfType<T>(this DependencyObject depObj) where T : DependencyObject
        {
            
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }

            return null;
        }


        static void DatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var dp = sender as DatePicker;
            if (dp == null) return;

            var tb = dp.GetChildOfType<DatePickerTextBox>();
            if (tb == null) return;

            var wm = tb.Template.FindName("PART_Watermark", tb) as ContentControl;
            if (wm == null) return;

            wm.Content = "Datum wählen";
        }
    }
}
