﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class IHObjektConfig : IAdminTyp
    {
        
        public string       Title           => "Assets verwalten";
        public int          Sortierung      => 12000;
        public Type         Controller      => typeof(IConfigController<IHObjektViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
