﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{

    public class Aufschlagtyp : Entity, IDataErrorInfo, IComparable<Aufschlagtyp>
    {

        private String     nameKurz;
        private String     nameLang;
        private String     bemerkung;
        private bool?      istStandard;
        private bool?      samstags;
        private bool?      sonntags;
        private bool?      feiertags;
        private TimeSpan?  uhrzeitVon;
        private TimeSpan?  uhrzeitBis;
        private decimal?   prozent;


#region P R O P E R T I E S


        public String NameKurz
        {
            get { return nameKurz; }
            set { SetProperty(ref nameKurz, value); }
        }

        public String NameLang
        {
            get { return nameLang; }
            set { SetProperty(ref nameLang, value); }
        }

        public String Bemerkung
        {
            get { return bemerkung; }
            set { SetProperty(ref bemerkung, value); }
        }

        public bool? IstStandard
        {
            get { return istStandard; }
            set { SetProperty(ref istStandard, value); }
        }

        public bool? Samstags
        {
            get { return samstags; }
            set { SetProperty(ref samstags, value); }
        }

        public bool? Sonntags
        {
            get { return sonntags; }
            set { SetProperty(ref sonntags, value); }
        }

        public bool? Feiertags
        {
            get { return feiertags; }
            set { SetProperty(ref feiertags, value); }
        }

        public TimeSpan? UhrzeitVon
        {
            get { return uhrzeitVon; }
            set { SetProperty(ref uhrzeitVon, value); }
        }

        public TimeSpan? UhrzeitBis
        {
            get { return uhrzeitBis; }
            set { SetProperty(ref uhrzeitBis, value); }
        }

        public decimal? Prozent
        {
            get { return prozent; }
            set { SetProperty(ref prozent, value); }
        }


#endregion


        public int CompareTo(Aufschlagtyp other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.NameKurz.CompareNatural(other.NameKurz);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["NameKurz"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["NameLang"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {


                switch (columnName) {

                case "NameKurz":

                    if (String.IsNullOrWhiteSpace(NameKurz)) return "Geben Sie das Kürzel des Aufschlages an.";
                    if (NameKurz.Length > 10) return "Das Kürzel darf aus maximal 10 Zeichen bestehen.";
                    break;

                case "NameLang":

                    if (String.IsNullOrWhiteSpace(NameLang)) return "Geben Sie die Bezeichnung des Aufschlages an.";
                    if (NameLang.Length > 48) return "Die Bezeichnung darf aus maximal 48 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}