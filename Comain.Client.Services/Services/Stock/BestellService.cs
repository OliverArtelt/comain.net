﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Services;
using Comain.Client.Models.Stock;


namespace Comain.Client.Services.Stock.Bestellungen
{

    public class BestellService : IBestellService
    {

        private readonly IRepository store;
        private readonly IKonfigService config;
        private readonly IMapper mapper;


        public BestellService(IRepository store, IMapper mapper, IKonfigService config)
        {

            this.store = store;
            this.config = config;
            this.mapper = mapper;
        }


        public async Task<IList<Lieferant>> LieferantenAsync(CancellationToken ct = default)
        {
            
            var dtos = await store.GetAsync<IList<LieferantDto>>("/Bestell/LF", ct).ConfigureAwait(false);
            var models = mapper.Map<List<Lieferant>>(dtos.OrderBy(p => p.Name1));
            return models;
        }


        public async Task<IList<Artikel>> ArtikelAsync(CancellationToken ct = default)
        {
            
            var dtos = await store.GetAsync<IList<ArtikelDto>>("/Bestell/AT", ct).ConfigureAwait(false);
            var models = mapper.Map<List<Artikel>>(dtos);
            return models;
        }


        public Task<IList<BestellInfoReportDto>> BestellInfoAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IList<BestellInfoReportDto>>("/Bestell/Info", filter, ct);


        public Task<IList<BestellReportDto>> BestellReportAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IList<BestellReportDto>>("/Bestell/Report", filter, ct);


        public Task<int> AnzahlBSArtikelAsync(CancellationToken ct = default)
            => store.GetAsync<int>("/Bestell/ATCount", ct);


        public Task<Konfiguration> KonfigAsync(CancellationToken ct = default)
            => config.GetAsync(ct);


        public Task<IList<PersonalFilterModel>> BearbeiterAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);


        public Task<IList<BestellListDto>> ListeAsync(Suchkriterien filter, CancellationToken ct = default)
            => store.FindAsync<IList<BestellListDto>>("/Bestell/Search", filter, ct);


        public async Task<Bestellung> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/Bestell/Detail/{0}", id);
            return await GetDetailAsync(url, ct).ConfigureAwait(false);
        }


        public async Task DeleteAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format(@"/Bestell/{0}", id);
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }


        public Task<Bestellung> UpdateAsync(Bestellung model, CancellationToken ct = default)
        {
            return SendModifiedAsync("/Bestell/Detail", model, ct);
        }


        public Task<Bestellung> OrderAsync(Bestellung model, CancellationToken ct = default)
        {
            return SendModifiedAsync("/Bestell/Order", model, ct);
        }


        public async Task<Bestellung> StornoAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format(@"/Bestell/Storno/{0}", id);
            return await GetDetailAsync(url, ct).ConfigureAwait(false);
        }


        public async Task<Bestellung> CloseAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format(@"/Bestell/Close/{0}", id);
            return await GetDetailAsync(url, ct).ConfigureAwait(false);
        }


        private async Task<Bestellung> SendModifiedAsync(String url, Bestellung model, CancellationToken ct = default)
        {

            var dto = mapper.Map<Bestellung, BestellDto>(model);
            dto.Positionen = mapper.AsDtoList<Bestellposition, BestellposDto>(model.Positionen);

            var result = await store.PostAsync<BestellDto, BestellDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<BestellDto, Bestellung>(result); 
            
            newmodel.AcceptChanges();
            if (!newmodel.Positionen.IsNullOrEmpty()) newmodel.Positionen.AcceptChanges();
            return newmodel;  
        }


        private async Task<Bestellung> GetDetailAsync(String url, CancellationToken ct = default)
        {

            var dto = await store.GetAsync<BestellDto>(url, ct).ConfigureAwait(false);
            if (dto == null) return null;

            var model = mapper.Map<BestellDto, Bestellung>(dto);
            model.AcceptChanges();
            model.Positionen.AcceptChanges();
            return model; 
        }
    }
}
