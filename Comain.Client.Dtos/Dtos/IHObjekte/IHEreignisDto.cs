﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHEreignisDto
    {

        /// <remarks>
        /// falls Unterobjekte mit ausgegeben werden sollen, diese konkret angeben
        /// </remarks>
        public int IHId                     { get; set; }
        /// <remarks>
        /// falls Unterobjekte mit ausgegeben werden sollen, diese konkret angeben
        /// </remarks>
        public String IHObjekt              { get; set; }
        /// <remarks>
        /// falls Unterobjekte mit ausgegeben werden sollen, diese konkret angeben
        /// </remarks>
        public String Maschine              { get; set; }
        /// <remarks>
        /// Zeitangabe für das Sortieren der Ereignisse
        /// </remarks>
        public DateTime Zeitpunkt           { get; set; }
        /// <remarks>
        /// wie der Zeitpunkt angezeigt werden soll
        /// </remarks>
        public String ZeitpunktAsString     { get; set; }

        public String Ereignis              { get; set; }

        public String Ereignistext          { get; set; }

        public List<KeyValuePair<String, String>>   Details      { get; set; }
    }
}
