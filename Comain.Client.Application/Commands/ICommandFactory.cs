﻿using System;
using System.Collections.Generic;
using Comain.Client.Models;


namespace Comain.Client.Commands
{

    public interface ICommandFactory
    {
        
        IControllerCommand              this[String cmdname]    { get; }
        IEnumerable<IControllerCommand> VerfügbareAktionen(ModulInfo module, Konfiguration cfg);
    }
}
