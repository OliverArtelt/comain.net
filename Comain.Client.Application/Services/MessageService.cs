﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Comain.Client.Gateways;
using Comain.Client.ViewData;
using Comain.Client.ViewModels.Services;
using Comain.Client.Views;
using Microsoft.Extensions.Logging;

namespace Comain.Client.Services
{

    public class MessageService : IMessageService
    {

        private readonly IMainView view;
        private readonly ILogger logger;
        private readonly IFlyoutService flyouts;
        private readonly DeleteViewModel deleteView;
        private readonly MessageViewModel messageView;
        private readonly ConfirmViewModel confirmView;


        public delegate void OverlayRaisedEventHandler(object sender, EventArgs e);
        public event EventHandler OverlayRaisedEvent;


        public MessageService(IMainView view, ILogger<MessageService> logger, IFlyoutService flyouts,
                              DeleteViewModel deleteView, ConfirmViewModel confirmView, MessageViewModel messageView)
        {

            this.view = view;
            this.logger = logger;
            this.flyouts = flyouts;
            this.deleteView = deleteView;
            this.confirmView = confirmView;
            this.messageView = messageView;
            deleteView.ExecuteCommand = flyouts.YesCommand;
            confirmView.ExecuteCommand = flyouts.YesCommand;
        }


        public bool DialogIsOpen
        {
            get { return view.DialogIsOpen; }
        }


        public void Show(Exception x)
        {

            if (IsCanceledException(x)) {

                var place = x.StackTrace.Split().FirstOrDefault(p => p.StartsWith("Comain"));
                logger.LogInformation("Current task cancelled. ({place})", place);
                return;
            }

            logger.LogError(x.ToString());

            var ax = x as AggregateException;
            if (ax != null && !ax.InnerExceptions.IsNullOrEmpty() && ax.InnerExceptions.All(p => p.Message == ax.InnerExceptions[0].Message))
                x = ax.InnerException;

            String msg = null;
            var ex1 = x as ComainValidationException;
            if (ex1 != null) {

                msg = ex1.Message + Environment.NewLine + String.Join(Environment.NewLine, ex1.Explanations);

            } else {

                var ex2 = x as UserException;
                if (ex2 != null) {

#if DEBUG

                    msg = ex2.ToString();

#else

                    msg = ex2.Message;

#endif

                } else {

                    var ex3 = x as ComainServerException;
                    if (ex3 != null) {

                        msg = ex3.ServerMessage;
                        if (!ex3.ServerExplanations.IsNullOrEmpty()) msg += Environment.NewLine + String.Join(Environment.NewLine, ex3.ServerExplanations);
                        msg += Environment.NewLine + ex3.ServerDetails;

                    } else {

#if DEBUG

                        msg = x.ToString();

#else

                        msg = x.UnwrapMessage();

#endif

                    }
                }
            }

            OnOverlayRaiseEvent(new EventArgs());
            view.ShowMessage("Ein Fehler ist aufgetreten.", msg);
        }


        public void Show(String body, String header = null)
        {

            OnOverlayRaiseEvent(new EventArgs());
            view.ShowMessage(header, body);
        }


        public Task ShowMessageAsync(MessageToken token)
        {

            messageView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = messageView.View });
        }


        public Task<bool> ConfirmAsync(MessageToken token)
        {

            confirmView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = confirmView.View });
        }


        public Task<bool> ConfirmDeleteAsync(DeleteToken token)
        {

            deleteView.SetContent(token);
            return flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = deleteView.View });
        }


        public async Task<bool> ConfirmExitAsync(String head = null, String body = null)
        {

            OnOverlayRaiseEvent(new EventArgs());
            return await view.ConfirmExitAsync(head, body);
        }


        private bool IsCanceledException(Exception x)
        {

            if (x is OperationCanceledException) return true;
            if (x is TaskCanceledException) return true;
            if (x.InnerException == null) return false;
            return IsCanceledException(x.InnerException);
        }


        protected virtual void OnOverlayRaiseEvent(EventArgs e)
        {

            EventHandler handler = OverlayRaisedEvent;
            if (handler != null) handler(this, e);
        }
    }
}
