﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class WLInventurDto
    {

        public int          Id              { get; set; }
        public DateTime     Datum           { get; set; }
        public String       Benutzer        { get; set; }
        public String       Bemerkung       { get; set; }
        public int          Lagerort_Id     { get; set; }
        public int          Material_Id     { get; set; }
        public decimal      AlteMenge       { get; set; }
        public decimal      NeueMenge       { get; set; }
    }
}
