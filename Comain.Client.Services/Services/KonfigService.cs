﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos;
using Comain.Client.Gateways;
using Comain.Client.Models;

namespace Comain.Client.Services
{

    public class KonfigService : IKonfigService
    {

        private readonly IRepository store;


        public KonfigService(IRepository store)
        {
            this.store = store;
        }


        public async Task<Konfiguration> GetAsync(CancellationToken ct = default)
        {
            var result = await store.GetAsync<Konfiguration>("/Config", ct).ConfigureAwait(false);
            result.AcceptChanges();
            return result;
        }


        public async Task<Konfiguration> SetAsync(Konfiguration model, CancellationToken ct = default)
        {

            var result = model.Modifikationen();
            if (!result.IsNullOrEmpty()) {

                var dto = result.Select(p => new NameWertDto { Name = p.Key, Wert = p.Value }).ToList();
                await store.PostAsync<List<NameWertDto>>("/Config", dto, ct).ConfigureAwait(false);
            }

            ct.ThrowIfCancellationRequested();

            return await GetAsync(ct);
        }
    }
}
