﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class ProfilViewModel : ViewModel<IProfilView>, IDelayableViewModel
    {

        public ProfilViewModel(IProfilView view) : base(view)
        {
        }


        public void Unload()
        {
        }
       

#region P R O P E R T I E S
        

        private String name;
        public String Name
        { 
            get { return name; }
            set { SetProperty(ref name, value); }
        }
       

        private String passwordOld;
        public String PasswordOld
        { 
            get { return passwordOld; }
            set { SetProperty(ref passwordOld, value); }
        }
       

        private String passwordNew1;
        public String PasswordNew1
        { 
            get { return passwordNew1; }
            set { SetProperty(ref passwordNew1, value); }
        }
   

        private String passwordNew2;
        public String PasswordNew2
        { 
            get { return passwordNew2; }
            set { SetProperty(ref passwordNew2, value); }
        }


        private String passwordFehlerText;
        public String PasswordFehlerText
        { 
            get { return passwordFehlerText; }
            set { SetProperty(ref passwordFehlerText, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool confirmLogout;
        public bool ConfirmLogout
        { 
            get { return confirmLogout; }
            set { SetProperty(ref confirmLogout, value); }
        }
   

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand passwordCommand;
        public ICommand PasswordCommand
        { 
            get { return passwordCommand; }
            set { SetProperty(ref passwordCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }

       
#endregion   
     
    }
}
