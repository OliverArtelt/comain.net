﻿using System;


namespace Comain.Client.Controllers.Stock.Wareneingänge
{
    
    public interface IEingangCommandMediator
    {
        
        Comain.Client.ViewModels.EditMode CurrentMode { get; }
        void NeuSetzen();
        void SetEditMode(Comain.Client.ViewModels.EditMode newMode);
    }
}