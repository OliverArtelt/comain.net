﻿using System;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Stock.Administration
{

    public class AnwesenheitViewModel : ValidatableViewModel<IAnwesenheitView, Konfiguration>, IConfigViewModel
    {

        public AnwesenheitViewModel(IAnwesenheitView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(StandardSchichtUhrzeitVon));
            RaisePropertyChanged(nameof(StandardSchichtUhrzeitBis));
            RaisePropertyChanged(nameof(StandardSchichtPause));
            RaisePropertyChanged(nameof(StandardSchichtUhrzeitFilter));
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        public String StandardSchichtUhrzeitVon
        {
            get => Current == null || !Current.StandardSchichtUhrzeitVon.HasValue ? null: Current.StandardSchichtUhrzeitVon.Value.ToString("hh\\:mm");
            set {

                if (Current != null) {

                    var time = value.ParseTime();
                    if (Current.StandardSchichtUhrzeitVon != time) {

                        Current.StandardSchichtUhrzeitVon = time;
                        RaisePropertyChanged(nameof(StandardSchichtUhrzeitVon));
                    }
                }
            }
        }


        public String StandardSchichtUhrzeitBis
        {
            get => Current == null || !Current.StandardSchichtUhrzeitBis.HasValue ? null: Current.StandardSchichtUhrzeitBis.Value.ToString("hh\\:mm");
            set {

                if (Current != null) {

                    var time = value.ParseTime();
                    if (Current.StandardSchichtUhrzeitBis != time) {

                        Current.StandardSchichtUhrzeitBis = time;
                        RaisePropertyChanged(nameof(StandardSchichtUhrzeitBis));
                    }
                }
            }
        }


        public decimal? StandardSchichtPause
        {
            get => Current?.StandardSchichtPause;
            set {

                if (Current != null && Current.StandardSchichtPause != value) {

                    Current.StandardSchichtPause = value;
                    RaisePropertyChanged(nameof(StandardSchichtPause));
                }
            }
        }


        public bool StandardSchichtUhrzeitFilter
        {
            get => Current == null? false: Current.StandardSchichtUhrzeitFilter;
            set {

                if (Current != null && Current.StandardSchichtUhrzeitFilter != value) {

                    Current.StandardSchichtUhrzeitFilter = value;
                    RaisePropertyChanged(nameof(StandardSchichtUhrzeitFilter));
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
