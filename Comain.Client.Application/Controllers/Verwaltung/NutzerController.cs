﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Auth;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface INutzerController : IController
    {}


    public class NutzerController : ControllerBase, INutzerController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly INutzerService store;
        private readonly NutzerViewModel view;
        private readonly MainViewModel mainView;
        private readonly ILoginService login;
        private SerializableCommand deleteCommand;
        private SerializableCommand editCommand;


        public NutzerController(IBreadcrumbService crumbs, INutzerService store, ILoginService login, IControllerBus bus,
                                MainViewModel mainView, NutzerViewModel view, IMessageService messages)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.store = store;
            this.view = view;
            this.login = login;
            this.mainView = mainView;
        }


        public void Initialize()
        {

            view.EditCommand = editCommand = CreateCommand(_ => EditAsync(), _ => view.Current != null);
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.CancelCommand = view.ReloadCommand;
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => view.Current != null);
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Anwender");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        protected override Task<bool> UnloadImplAsync()
        {

            view.Unload();
            return Task.FromResult(true);
        }


        private Task EditAsync()
        {

            if (view.Current != null) view.EditEnabled = true;
            return Task.FromResult(true);
        }


        private async Task LoadAsync()
        {

            view.EditEnabled = false;

            using (new VisualDelay(view)) {

                var ustask = store.NutzerAsync(bus.Token);
                var pstask = store.PersonalAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);

                await Task.WhenAll(ustask, pstask, pztask).ConfigureAwait(false);
                await view.LoadAsync(ustask.Result, pstask.Result, pztask.Result).ConfigureAwait(false);

                UpdateCommands();
            }
        }


        private async Task SaveAsync()
        {

            if (view.Current == null) return;
            var test = view.Current.Errorlist;
            if (!test.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Anwender kann nicht gespeichert werden", Reasons = test });
                return;
            }

            using (new VisualDelay(view)) {

                await store.UpdateAsync(view.GetCurrent(), bus.Token).ConfigureAwait(false);
                await LoadAsync();
            }
        }


        private async Task AddAsync()
        {

            var model = new Nutzer { Name = "Neuer Nutzer" };

            await OnUIThreadAsync(() => { 

                view.Nutzerliste.Add(model);
                view.Current = model;
                view.EditEnabled = true;
            });

            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            if (view.Current == null) return;

            if (!String.IsNullOrEmpty(view.Current.Id)) {

                if (!await messages.ConfirmDeleteAsync(new DeleteToken { Header = "Soll der Anwender wirklich gelöscht werden?", IsDeleteable = true })) return;
                await store.DeleteAsync(view.Current, bus.Token);
            }

            await OnUIThreadAsync(() => { 

                view.Nutzerliste.Remove(view.Current);
                view.Current = null;
                return;

            });

            UpdateCommands();
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        private void UpdateCommands()
        {

            context.Post(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
                editCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current") UpdateCommands();
        }
    }
}
