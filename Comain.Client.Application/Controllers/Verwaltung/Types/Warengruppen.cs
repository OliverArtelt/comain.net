﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Stock.Verwaltung;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Stock.Verwaltung.Types
{

    public class Warengruppen : IVwTyp
    {
       
        public string       Title           => "Warengruppen";
        public int          Sortierung      => 6000;
        public Type         Controller      => typeof(IWarengruppenController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator;    
        }
    }
}
