﻿using System;
using System.Linq;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock.Reports;

namespace Comain.Client.ViewData.Stock.Reports
{

    public class BestandExportRow
    {
        
        public String        Nummer                 { get; set; }
        public String        Name                   { get; set; }
        public String        Herstellernummer       { get; set; }
        public String        Warengruppe            { get; set; }
        public decimal?      Mindestbestand         { get; set; }
        public decimal?      Mindestbestellmenge    { get; set; }
        public decimal?      Bestellt               { get; set; }

        public String        PosTyp                 { get; set; }
       
        public String        IHObjekt               { get; set; }
        public String        Maschine               { get; set; }

        public String        Lagerort               { get; set; }
        public decimal?      Menge                  { get; set; }
        public decimal?      Mischpreis             { get; set; }
        public decimal?      Gesamtpreis            { get; set; }

        public decimal?      Artikelmenge           { get; set; }
        public decimal?      Artikelmischpreis      { get; set; }
        public decimal?      Artikelgesamtpreis     { get; set; }

        public DateTime?     DeaktiviertAm          { get; set; }


        public BestandExportRow()
        {}


        public BestandExportRow(ArtikelReportDto dto)
        {
        
            PosTyp              = "Artikel";
            Nummer              = dto.Artikelnummer; 
            Name                = dto.Artikelname; 
            Herstellernummer    = dto.Herstellernummer; 
            Mindestbestand      = dto.Mindestbestand; 
            Mindestbestellmenge = dto.Mindestbestellmenge; 
            Bestellt            = dto.Bestellt; 
            Warengruppe         = dto.Warengruppe; 
            DeaktiviertAm       = dto.DeaktiviertAm; 
        }


        public BestandExportRow(ArtikelReportDto dto, bool istTotal)
        {
        
            PosTyp              = "Artikel gesamt";
            Nummer              = dto.Artikelnummer; 
            Artikelmenge        = dto.Lager.Sum(p => p.Menge);
            Artikelgesamtpreis  = dto.Lager.Sum(p => p.Gesamtpreis);
            if (Artikelmenge > 0) Artikelmischpreis = Artikelgesamtpreis / Artikelmenge;
        }


        public BestandExportRow(ArtikelReportDto dto, NummerFilterModel ihdto) 
        {
        
            PosTyp    = "Asset";
            Nummer    = dto.Artikelnummer; 
            IHObjekt  = ihdto.Nummer;
            Maschine  = ihdto.Name;
        }


        public BestandExportRow(ArtikelReportDto dto, LagerortReportDto lodto)
        {
        
            PosTyp      = "Lagerort";
            Nummer      = dto.Artikelnummer; 
            Lagerort    = lodto.Nummer;
            Menge       = lodto.Menge;
            Mischpreis  = lodto.Mischpreis;
            Gesamtpreis = lodto.Gesamtpreis;
        }
    }
}