﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewData;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{
    
    public interface IVerfügbarkeitController : IVwTypController 
    {}
    
    
    public class VerfügbarkeitController : ControllerBase, IVerfügbarkeitController
    {
    
        private readonly IFlyoutService flyouts;
        private readonly IVfService store;
        private readonly VerwaltungViewModel mainView;
        private readonly VFStep1ViewModel step1View;
        private readonly VFStep2ViewModel step2View;
        private readonly VFCopyViewModel copyView;
        private readonly IHSelektorViewModel selektorView;
        private int idstamp;
        private DateTime currentMonth;
        private TrackableCollection<IHObjektBelegung> currentList;
        private IList<DateTime> copyableDates;
        private IDictionary<int, IHSelektorItem> ihDict;

    
        public VerfügbarkeitController(IMessageService messages, IFlyoutService flyouts, IVfService store, IControllerBus bus, 
                                       VerwaltungViewModel mainView, VFStep1ViewModel step1View, VFStep2ViewModel step2View, 
                                       IHSelektorViewModel selektorView, VFCopyViewModel copyView)
          : base(messages, bus)
        {

            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.step1View = step1View;
            this.step2View = step2View;
            this.copyView = copyView;
            this.selektorView = selektorView;
        }


        public void Initialize()
        {
            
            step1View.StepCommand = CreateCommand(_ => Step2Async());
            step2View.AddCommand = CreateCommand(_ => AddAsync());
            step2View.BackCommand = CreateCommand(_ => StepBackAsync());
            step2View.CancelCommand = CreateCommand(_ => LoadAsync());
            step2View.CopyCommand = CreateCommand(_ => CopyAsync());
            step2View.SaveCommand = CreateCommand(_ => SaveAsync());
            step2View.DeleteCommand = CreateCommand(_ => DeleteAsync(false));
            selektorView.TakeCommand = flyouts.YesCommand;
            copyView.TakeCommand = flyouts.YesCommand;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);
            mainView.DetailView = step1View.View;
        }


        public void Shutdown()
        {}


        protected override async Task<bool> UnloadImplAsync()
        {
            
            if (store.HasChanges && !await messages.ConfirmExitAsync()) return false;
            
            store.Reset();
            step1View.Unload();
            step2View.Unload();
            selektorView.Unload();
            copyView.Unload();
            currentList = null;
            ihDict = null;
            copyableDates = null;

            return true;
        }


        private async Task SaveAsync()
        {
            
            using (new VisualDelay(step2View)) {
            
                await store.SaveAsync(bus.Token);
                await LoadAsync();
            }
        }


        private async Task Step2Async()
        {
            
            currentMonth = new DateTime(step1View.Jahr, step1View.Monat + 1, 1);
            step2View.Monat = currentMonth;
            mainView.DetailView = step2View.View;
            await LoadAsync();
        }


        private async Task StepBackAsync()
        {
            
            if (!await UnloadImplAsync()) return;
            mainView.DetailView = step1View.View;
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(step2View)) {
                
                var ihtask = store.GetIHListAsync(bus.Token); 
                var vftask = store.GetBelegungAsync(currentMonth.Year, currentMonth.Month, bus.Token); 
                var cdtask = store.GetVormonateAsync(bus.Token); 

                await Task.WhenAll(ihtask, vftask, cdtask).ConfigureAwait(false);    

                currentList = vftask.Result;
                copyableDates = cdtask.Result;
                ihDict = ihtask.Result.ToDictionary(p => p.Id);

                bus.Token.ThrowIfCancellationRequested();

                await SetBelegListAsync(currentList); 
                await selektorView.SetListAsync(ihtask.Result); 
            }

            idstamp = 0;
        }


        private async Task SetBelegListAsync(IEnumerable<IHObjektBelegung> vflist)
        {

            if (vflist == null) return;
            var currentList = vflist.Where(p => ihDict.ContainsKey(p.IHObjekt_Id)).Select(p => new VfViewItem(p, ihDict[p.IHObjekt_Id])).ToList();

            await OnUIThreadAsync(() => { 
           
                step2View.Liste = new ObservableCollection<VfViewItem>(currentList);
            
            });
        }


        private async Task AddAsync()
        {

            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = selektorView.View, Header = "Assets auswählen", Position = MahApps.Metro.Controls.Position.Right })) return; 
            
            var list = selektorView.GetSelected();
            if (list == null) return;

            var cltask = flyouts.CloseAsync();
            var adtask = AddToListAsync(list);

            await Task.WhenAll(cltask, adtask).ConfigureAwait(false);
        }


        private async Task DeleteAsync(bool clearAll)
        {

            List<VfViewItem> deletelist = null;
            
            if (clearAll) deletelist = step2View.Liste.ToList();
            else          deletelist = step2View.Liste.Where(p => p.IsSelected).ToList();

            await OnUIThreadAsync(() => { 
                
                foreach (var vitem in deletelist) {

                    currentList.Remove(vitem.Model);
                    step2View.Liste.Remove(vitem);
                }
            
            });
        }


        private async Task CopyAsync()
        {

            var datelist = copyableDates.Where(p => p != currentMonth).OrderByDescending(p => p).ToList();
            if (datelist.Count == 0) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Es wurde kein Monat zum Kopieren gefunden." });
                return;
            }

            copyView.Monate = datelist;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = copyView.View, Position = MahApps.Metro.Controls.Position.Bottom })) return; 
            
            var list = await store.GetCopyAsync(copyView.Monat.Year, copyView.Monat.Month, bus.Token);

            var cltask = flyouts.CloseAsync();
            var adtask = AddToListAsync(list);

            await Task.WhenAll(cltask, adtask).ConfigureAwait(false);
        }


        private async Task AddToListAsync(IEnumerable<IHSelektorItem> list)
        {
            
            var modellist = new List<IHObjektBelegung>();
            var vitemlist = new List<VfViewItem>();
                
            foreach (var ih in list) { 
            
                if (currentList.Any(p => p.IHObjekt_Id == ih.Id)) continue;

                var newmodel = new IHObjektBelegung { Id = --idstamp, Jahr = currentMonth.Year, Monat = (short)currentMonth.Month, IHObjekt_Id = ih.Id };
                var viewitem = new VfViewItem(newmodel, ih);
                modellist.Add(newmodel);
                vitemlist.Add(viewitem);
            };

            await OnUIThreadAsync(() => { 

                currentList.AddRange(modellist);
                step2View.Liste.AddRange(vitemlist);
            
            });
        }


        private async Task AddToListAsync(IEnumerable<IHObjektBelegung> list)
        {
         
            if (list == null) return;
            var modellist = new List<IHObjektBelegung>();
            var vitemlist = new List<VfViewItem>();
           
            if (copyView.Ersetzen) await DeleteAsync(true);
                
            foreach (var model in list) { 
            
                if (currentList.Any(p => p.IHObjekt_Id == model.IHObjekt_Id)) continue;
            
                IHSelektorItem ih;
                if (!ihDict.TryGetValue(model.IHObjekt_Id, out ih)) continue;
                
                model.Id = --idstamp;
                model.Jahr = currentMonth.Year;   
                model.Monat = (short)currentMonth.Month; 
                modellist.Add(model);  
                var viewitem = new VfViewItem(model, ih);
                vitemlist.Add(viewitem);
            }

            await OnUIThreadAsync(() => { 

                currentList.AddRange(modellist);
                step2View.Liste.AddRange(vitemlist);
            
            });
        }
    }
}
