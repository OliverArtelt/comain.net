﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewData;
using Comain.Client.Views.Services;


namespace Comain.Client.ViewModels.Services
{
    
    public class ConfirmViewModel : ViewModel<IConfirmView>
    {

        public ConfirmViewModel(IConfirmView view) : base(view)
        {
        }


        public void SetContent(MessageToken data)
        {

            Header = data.Header;
            Arguments = data.Reasons;
        }


#region P R O P E R T I E S
 

        private IEnumerable<String> arguments;
        public IEnumerable<String> Arguments
        { 
            get { return arguments; }
            set { SetProperty(ref arguments, value); }
        }
 

        private String header;
        public String Header
        { 
            get { return header; }
            set { SetProperty(ref header, value); }
        }


        private ICommand executeCommand;
        public ICommand ExecuteCommand
        { 
            get { return executeCommand; }
            set { SetProperty(ref executeCommand, value); }
        }


#endregion

    }
}
