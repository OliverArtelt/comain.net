﻿using System;


namespace Comain.Client.Dtos.IHObjekte
{

    public class IHKstDto
    {

        public int          Id                  { get; set; } 
        public DateTime?    Datum               { get; set; } 
        public bool?        IstAktiv            { get; set; }
        public String       Nummer              { get; set; }
        public String       Name                { get; set; }
    }
}
