﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.ViewModels.IHObjekte
{

    [DebuggerDisplay("{Nummer}")]
    public class TreePZViewItem : TreeViewItem
    {

        public PzFilterModel        Model       { get; private set; }
        public List<TreeIHViewItem> SubItems    { get; private set; }

        public String Name     => Model.Name;
        public int    Nummer   => Model.Nummer;


        public TreePZViewItem(PzFilterModel model)
        {
            Model = model;
        }


        public override bool IsParentOf(TreeIHViewItem dest)
        {
            return dest != null && dest.IstMaschine && dest.FilterModel.Standort_Id == Model.Id;
        }


        public void Add(TreeIHViewItem subItem)
        {

            if (SubItems == null) SubItems = new List<TreeIHViewItem>();
            SubItems.Add(subItem);
        }
    }
}
