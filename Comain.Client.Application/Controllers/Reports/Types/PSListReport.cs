﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public class PSListReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public PSListReport(IReportService store)
        {
            this.store = store;
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.PSListAsync(GebeFilter(), ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Personalliste"; }
        }


        public string Gruppe
        {
            get { return "Personal"; }
        }


        public int Sortierung
        {
            get { return 300; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.PSListReport.rdlc"; }
        }
    }
}
