﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    [DebuggerDisplay("{Nummer}")]
    public class IHFilterItem : NotifyBase, IEquatable<IHFilterItem>
    {

        public int      Id              { get; set; }
        public String   Nummer          { get; set; }
        public String   Name            { get; set; }
        public String   Inventarnummer  { get; set; }
        public String   Fabriknummer    { get; set; }
        public int      Kostenstelle_Id { get; set; }
        public int      Standort_Id     { get; set; }
        public int?     Erhaltklasse    { get; set; }
        public int?     Status          { get; set; }


        private Stack<String> baugruppen;
        public Stack<String> Baugruppen
        {
            get { return baugruppen == null ? (baugruppen = new Stack<String>()) : baugruppen; }
            set { baugruppen = value; }
        }


        public String IHObjekt => $"{Nummer} - {Pfad}";


        public String Pfad
        {
            get {

                if (Baugruppen.IsNullOrEmpty()) return Name;
                return String.Join(" • ", Baugruppen.ToArray());
            }
        }


        private bool isChecked;
        /// <summary>
        /// Checkbox-Support
        /// </summary>
        public bool IsChecked
        {
            get { return isChecked; }
            set { SetProperty(ref isChecked, value); }
        }


        public IHFilterItem()
        {
        }


        public IHFilterItem(IHListDto dto)
        {

            Id = dto.Id;
            Nummer = dto.Nummer;
            Name = dto.Name;
            Inventarnummer = dto.Inventarnummer;
            Fabriknummer = dto.Fabriknummer;
            Kostenstelle_Id = dto.Kostenstelle_Id;
            Standort_Id = dto.Standort_Id;
            Erhaltklasse = dto.Erhaltklasse;
            Status = dto.Status;
        }


        public IHFilterItem(IHObjektFilterModel dto)
        {

            Id = dto.Id;
            Nummer = dto.Nummer;
            Name = dto.Name;
            Inventarnummer = dto.Inventarnummer;
            Fabriknummer = dto.Fabriknummer;
            Kostenstelle_Id = dto.Kostenstelle_Id;
            Standort_Id = dto.Standort_Id;
            Erhaltklasse = dto.Erhaltklasse;
            Status = dto.Status;
        }


        public IHFilterItem(AuIHKSDto dto)
        {

            Id = dto.Id;
            Nummer = dto.IHObjekt;
            Name = dto.Maschine;
            Inventarnummer = dto.Inventarnummer;
            Fabriknummer = dto.Fabriknummer;
            Kostenstelle_Id = dto.Kostenstelle_Id;
            Standort_Id = dto.Standort_Id;
            Erhaltklasse = dto.Erhaltklasse;
            Status = dto.Status;
        }


        public IHFilterItem(IHObjekt model)
        {

            Id = model.Id;
            Nummer = model.Nummer;
            Name = model.Name;
            Inventarnummer = model.Inventarnummer;
            Fabriknummer = model.Fabriknummer;
            Kostenstelle_Id = model.Kostenstelle.Id;
            Standort_Id = model.Standort.Id;
            Erhaltklasse = (int?)model.Erhaltklasse;
            Status = (int?)model.Status;
        }


        public bool Matches(IEnumerable<String> searchterms)
        {

            if (searchterms.IsNullOrEmpty()) return true;
            return searchterms.All(p => Matches(p));
        }


        public bool Matches(String searchText)
        {

            if (Nummer != null && Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Name != null && Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Inventarnummer != null && Inventarnummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Fabriknummer != null && Fabriknummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (!Baugruppen.IsNullOrEmpty() && Baugruppen.Any(q => q.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))) return true;
            return false;
        }


        public String ParentNummer
        {
            get {

                var parts = Nummer.Split('-');
                if (parts.Length < 2) return null;
                return String.Join("-", parts.Take(parts.Length - 1));
            }
        }


        public override bool Equals(object obj)
            => Equals(obj as IHFilterItem);


        public bool Equals(IHFilterItem obj)
            => obj == null? false: this.Id == obj.Id;


        public override int GetHashCode()
            => Id.GetHashCode();
    }
}
