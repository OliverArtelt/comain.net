﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Stock;
using Comain.Client.Dtos.Stock.Reports;
using Comain.Client.Models.Stock;

namespace Comain.Client.Services.Stock.Wareneingänge
{

    public interface IEingangService
    {

        Task<IList<EingangBestellposDto>>   OffenePositionenAsync(CancellationToken ct = default);
        Task<IList<String>>                 AuftragsnummernAsync(CancellationToken ct = default);
        Task<IList<String>>                 BestellnummernAsync(CancellationToken ct = default);
        Task<IList<String>>                 LieferscheinnummernAsync(CancellationToken ct = default);
        Task<Lagerort>                      LetzerLagerortAsync(int artikelId, CancellationToken ct = default);
        Task<IList<Lagerort>>               LagerorteAsync(CancellationToken ct = default);
        
        Task<IList<EingangListDto>>         FindAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<IList<WEReportDto>>            ReportAsync(Suchkriterien filter, CancellationToken ct = default);

        Task<Lieferschein>                  DetailAsync(int id, CancellationToken ct = default);
        Task<Lieferschein>                  UpdateAsync(Lieferschein model, CancellationToken ct = default);
        Task                                DeleteAsync(int id, CancellationToken ct = default);
    }
}
