﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Bestellungen
{

    public class BestellAddData
    {

        public IList<Lieferant>     Lieferanten     { get; set; }
        public IList<Artikel>       Artikelliste    { get; set; }
        public Action<Bestellung>   Callback        { get; set; }
    }
}
