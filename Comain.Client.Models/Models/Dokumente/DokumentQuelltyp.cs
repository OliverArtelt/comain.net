﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comain.Client.Models.Dokumente
{
    public enum DokumentQuelltyp
    {
        Undefiniert = 0, IHObjekt, IHObjektParent, IHObjektChild, Auftrag, Massnahme, Personal, Artikel, Spezifikation
    }


    public static class DokumentQuelltypExtensions
    {

        public static String AsText(this DokumentQuelltyp value)
        {

            if (value == DokumentQuelltyp.IHObjekt)       return "Asset";
            if (value == DokumentQuelltyp.IHObjektChild)  return "Baugruppe";
            if (value == DokumentQuelltyp.IHObjektParent) return "Übergeordnetes Asset";
            if (value == DokumentQuelltyp.Undefiniert)    return null;
            return value.ToString();
        }
    }     
}
