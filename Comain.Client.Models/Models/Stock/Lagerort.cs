﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain;


namespace Comain.Client.Models.Stock
{

    public class Lagerort : Entity, IDataErrorInfo, IComparable<Lagerort>
    {
        
        private String   halle;
        private String   regal;
        private String   fach;
        private String   nummer;
        private String   bemerkungen;
        
        
        public virtual ICollection<Wareneinlagerung>    Einlagerungen    { get; set; }


#region P R O P E R T I E S


        public String Halle 
        {
            get { return halle; }
            set { SetProperty(ref halle, value); }
        }
        

        public String Regal 
        {
            get { return regal; }
            set { SetProperty(ref regal, value); }
        }
        

        public String Fach 
        {
            get { return fach; }
            set { SetProperty(ref fach, value); }
        }
        

        public String Nummer 
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public String Bemerkungen 
        {
            get { return bemerkungen; }
            set { SetProperty(ref bemerkungen, value); }
        }
      

#endregion

        
        public int CompareTo(Lagerort other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Halle"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Regal"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Fach"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Halle":

                    if (String.IsNullOrWhiteSpace(Halle)) return null;
                    if (Halle.Length > 8) return "Die Halle darf aus maximal 8 Zeichen bestehen."; 
                    break;

                case "Regal":

                    if (String.IsNullOrWhiteSpace(Regal)) return null;
                    if (Regal.Length > 8) return "Das Regal darf aus maximal 8 Zeichen bestehen."; 
                    break;

                case "Fach":

                    if (String.IsNullOrWhiteSpace(Fach)) return null;
                    if (Fach.Length > 8) return "Das Fach darf aus maximal 8 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
