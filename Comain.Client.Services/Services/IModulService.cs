﻿using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;
using Comain.Client.Models;


namespace Comain.Client.Services
{

    public interface IModulService
    {

        ModulInfo   Info    { get; }

        Task        LoadAsync(CancellationToken ct = default);
        void        Clear();
    }
}
