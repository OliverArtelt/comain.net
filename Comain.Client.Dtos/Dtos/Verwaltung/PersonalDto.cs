﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class PersonalDto : ChangeTrackingBase
    {

        public String       Nummer                  { get; set; }
        public String       Klammer                 { get; set; }
        public String       Nachname                { get; set; }
        public String       Vorname                 { get; set; }
        public String       Strasse                 { get; set; }
        public String       Plz                     { get; set; }
        public String       Ort                     { get; set; }
        public String       Telefon                 { get; set; }
        public String       Qualifikation           { get; set; }
        public String       EMail                   { get; set; }
        public DateTime?    Eintritt                { get; set; }
        public DateTime?    Austritt                { get; set; }
        public int?         Gewerk_Id               { get; set; }
        public int?         Monatssoll              { get; set; }
        public decimal?     Stundensatz             { get; set; }

        public ICollection<IHObjektPersonalDto> Assets  { get; set; }
    }
}
