﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class NummerDto : ChangeTrackingBase
    {
        
        public short      Nummer                  { get; set; }
        public String     Name                    { get; set; }
    }
}
