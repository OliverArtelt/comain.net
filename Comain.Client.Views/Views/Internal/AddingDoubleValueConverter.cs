﻿using System;
using System.Linq;
using System.Globalization;
using System.Windows;
using System.Windows.Data;


namespace Comain.Client.Views.Internal
{
   
    public class AddingDoubleValueConverter : IValueConverter
    {
 
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (parameter == null || value == null) return value;
            if (!(value is double)) return value;
            double d;
            if (!Double.TryParse(parameter.ToString(), out d)) return value;
            return Math.Max((double)value + d, 0);
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
