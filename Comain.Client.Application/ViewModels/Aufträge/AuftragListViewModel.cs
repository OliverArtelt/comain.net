﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragListViewModel : ViewModel<IAuftragListView>
    {

        private readonly ISettings settings;


        public AuftragListViewModel(IAuftragListView view, ISettings settings) : base(view)
        {
            this.settings = settings;
        }


        public void Unload()
        {
            Foundlist = null;
        }


        public void SetCurrent(int auid)
        {

            if (Foundlist.IsNullOrEmpty()) return;
            current = Foundlist.FirstOrDefault(p => p.Id == auid);
            RaisePropertyChanged(nameof(Current));
        }


#region P R O P E R T I E S


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ObservableCollection<AuftragListViewItem> foundlist;
        public ObservableCollection<AuftragListViewItem> Foundlist
        {
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private AuftragListViewItem current;
        public AuftragListViewItem Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


        public String LayoutStore
        {
            get => settings.LayoutAuftragList;
            set {

                if (settings.LayoutAuftragList != value) {

                    settings.LayoutAuftragList = value;
                    settings.Save();
                }
            }
        }


#endregion

    }
}
