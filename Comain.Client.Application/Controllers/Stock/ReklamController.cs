﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Controllers.Stock.Wareneingänge;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels.Stock;
using Comain.Client.Services.Stock.Reklamationen;
using Comain.Client.ViewModels.Stock.Reklamationen;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Reklamationen
{
    
    public class ReklamController : ControllerBase, IReklamController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IReklamService store;
        private readonly IReklamCommandMediator commands;
        private readonly MainViewModel mainView;
        private readonly ReklamViewModel view;
        private readonly ReklamListViewModel listView;
        private readonly ReklamDetailViewModel detailView;
        private readonly ReklamBSPosViewModel addView;
        private readonly ReklamDetailReportViewModel reportView;
        private readonly ReklamDetailBaseViewModel baseView;
        private IList<RKListDto> currentList;
        private IList<RKEingangDto> weList;
        private Konfiguration config;
        private SerializableCommand detailCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand editCommand;
        private SerializableCommand addPosCommand; 
        private SerializableCommand deletePosCommand;
        private WEReklamData caller;


        public ReklamController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts, IReklamCommandMediator commands,
                                IReklamService store, IControllerBus bus, 
                                ReklamBSPosViewModel addView, ReklamDetailBaseViewModel baseView, ReklamDetailReportViewModel reportView,
                                MainViewModel mainView, ReklamViewModel view, ReklamListViewModel listView, ReklamDetailViewModel detailView)
          : base(messages, bus)        
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.detailView = detailView;
            this.addView = addView;
            this.reportView = reportView;
            this.baseView = baseView;
            this.commands = commands;
        }


        public void Initialize()
        {

            view.ListView = listView.View;
            view.DetailView = detailView.View;
            detailView.BasisView = baseView.View;
            detailView.ReportView = reportView.View;
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            listView.DetailCommand = detailCommand = CreateCommand(_ => DetailAsync(), _ => listView.Current != null);
            view.AddCommand = CreateCommand(_ => AddAsync());
            view.CancelCommand = CreateCommand(_ => CancelAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => listView.Current != null);
            view.EditCommand = editCommand = CreateCommand(_ => EditAsync(), _ => detailView.Current != null);
            view.AddPosCommand = addPosCommand = CreateCommand(_ => AddPosAsync(), _ => detailView.Current != null);
            view.DeletePosCommand = deletePosCommand = CreateCommand(_ => DeletePosAsync(), _ => detailView.Current != null && baseView.CurrentPosition != null);

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(baseView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            //Quereinstieg: Aufruf vom WEController
            caller = parameter as WEReklamData;
                                                   
            if (caller == null) crumbs.Add("Ersatzteile");
            crumbs.Add("Reklamationen");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;         
            
            await ExecuteWithLockAsync(_ => LoadAsync());
            if (caller != null) await AddFromWeAsync(caller.Wareneingang);      
        }
 

        private async Task BackAsync()
        {

            if (caller != null) {

                if (!await UnloadImplAsync()) return;
                crumbs.Remove();
                caller.Callback();
                
            } else {

                await bus.BackAsync();
            }
        }


        public void Shutdown()
        {
            
            PropertyChangedEventManager.RemoveHandler(baseView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
        }
 
 
        protected override async Task<bool> UnloadImplAsync()
        {

            if (detailView.Current != null && commands.CurrentMode != EditMode.ReadOnly &&
                (detailView.Current.IsChanged || detailView.Current.Positionen.IsChanged)) {
                                                   
                detailView.ResetTabsIfPrint(); 
                if (!await messages.ConfirmExitAsync()) return false;
            }

            weList = null;
            currentList = null;
            baseView.Unload();
            reportView.Unload();
            listView.Unload();
            addView.Unload();
            SetModel(null);

            return true;
        }


        private async Task LoadAsync()
        {

            commands.SetEditMode(EditMode.ReadOnly);
            var oldlistmodel = listView.Current;

            listView.Unload();

            using (new VisualDelay(view)) {

                var wetask = store.EingängeAsync(bus.Token);
                var cftask = store.KonfigAsync(bus.Token);
                var rktask = LoadListAsync();
             
                await Task.WhenAll(rktask, wetask, cftask).ConfigureAwait(false);
             
                bus.Token.ThrowIfCancellationRequested();
                config = cftask.Result;
                weList = wetask.Result;             
            }
    
            if (oldlistmodel != null && !currentList.IsNullOrEmpty()) {
            
                listView.Current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                await DetailAsync();
            
            } else {
          
                SetModel(null);
            }
        }


        private async Task LoadListAsync()
        {
   
            using (new VisualDelay(view)) {

                listView.Current = null;
                SetModel(null);    
          
                currentList = await store.ListAsync(bus.Token).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();
          
                await context.SendAsync(new SendOrPostCallback((o) => { 
                
                    listView.Foundlist = new ObservableCollection<RKListDto>(currentList);
                       
                }), null);
            }
        }
  

        private async Task UpdatePrintViewAsync()
        {

            reportView.ViewerVisible = false;
            if (detailView.Current == null) return;

            var address = "Comain.Client.Stock.Reports.Reklamation.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = ("Reklamation " + detailView.Current.Bestellung.Nummer).Trim() };
            var reportmodel = new ReklamReportModel(detailView.Current, config);
           
            report.AddDataSource("MainData", new List<ReklamReportModel> { reportmodel });
            report.AddDataSource("PosData", reportmodel.Positionen);

            await reportView.BindViewerAsync(report);
        } 


        private void SetModel(Reklamation current)
        {

            detailView.Current = current;
            baseView.Current = current;

            if (current != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null && listviewitem != listView.Current) listView.Current = listviewitem;
            }
 
            UpdateCommands(); 
        }    
 

        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
             
            if (args.PropertyName == "CurrentTab") { 
                
                UpdateCommands();
                if (detailView.Tab == ReklamDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync(); 
            }
            
            if (args.PropertyName == "Current") { 
                
                UpdateCommands();
                if (detailView.Tab == ReklamDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();  
            }

            if (sender == baseView && args.PropertyName == "CurrentPosition") UpdateCommands(); 
        }


        private void UpdateCommands()
        {
            
            commands.NeuSetzen();

            context.Post(new SendOrPostCallback((o) => { 
            
                detailCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                editCommand.RaiseCanExecuteChanged();
                addPosCommand.RaiseCanExecuteChanged(); 
                deletePosCommand.RaiseCanExecuteChanged();

            }), null);
        }

 
        private async Task DetailAsync()
        {

            if (listView.Current != null) {
           
                using (new VisualDelay(detailView)) {
               
                    var model = await store.DetailAsync(listView.Current.Id, bus.Token).ConfigureAwait(false);
                    SetModel(model);
                }
            
            } else {
           
                SetModel(null);    
            }
        }
   

        private Task EditAsync()
        {
            
            if (detailView.Current != null) commands.SetEditMode(EditMode.Edit);
            return Task.FromResult(true);
        }


        private async Task CancelAsync()
        {

            if (commands.CurrentMode == EditMode.Append) {

                SetModel(null);

            } else {

                await DetailAsync();
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task SaveAsync()
        {

            var rk = detailView.Current;
            if (rk == null || !rk.IsChanged) return;

            if (!rk.ErrorList.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Reklamation kann nicht gespeichert werden", Reasons = rk.ErrorList });
                return;
            }

            using (new VisualDelay(detailView)) {

                var model = await store.UpdateAsync(rk, bus.Token);
                if (rk.IstAngefügt) {
                
                    var vi = new RKListDto { Bestellung = model.Bestellung.Nummer, Id = model.Id, Lieferdatum = model.Lieferschein.Datum,
                                                          Lieferschein = model.Lieferschein.Nummer, ReklamiertAm = model.Datum };
                    await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist.Add(vi); }), null);
                }
                
                SetModel(model);
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task VerwerfenAsync()
        {

            if (commands.CurrentMode == EditMode.Append) {

                SetModel(null);

            } else {

                await DetailAsync();
            }

            commands.SetEditMode(EditMode.ReadOnly);
        }


        private async Task AddAsync()
        {

            if (weList.Count == 0) {
             
                await messages.ShowMessageAsync(new MessageToken { Header = "Es wurden keine reklamierbaren Wareneingänge gefunden." });
                return;     
            }

            await context.SendAsync(new SendOrPostCallback((o) => { addView.List = weList; }), null);

            addView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = addView.View, Header = "Reklamation erstellen", Position = MahApps.Metro.Controls.Position.Right })) return;

            if (addView.Current == null) return;
            bus.Token.ThrowIfCancellationRequested();

            var bs = await store.BestellungAsync(addView.Current.Bestellung_Id, bus.Token);
            var ls = new Lieferschein { Id = addView.Current.Lieferschein_Id, Nummer = addView.Current.Lieferschein, Datum = addView.Current.GeliefertAm };
            var rk = new Reklamation { Bestellung = bs, Datum = DateTime.Now, Lieferschein = ls };  
            var rkpos = new Reklamposition { Artikelbezeichnung = addView.Current.Artikelbezeichnung, ArtikelId = addView.Current.Material_Id, Artikelnummer = addView.Current.Artikelnummer,
                                             Bestellposition = addView.Current.Bestellnummer, EingangId = addView.Current.Eingang_Id, Preis = addView.Current.Preis, 
                                             LfArtikelnummer = addView.Current.LfArtikelnummer, Position = 1 };
            var mengeVerfügbar = addView.Current.Liefermenge - addView.Current.MengeOK.GetValueOrDefault();
            if (mengeVerfügbar <= 0) mengeVerfügbar = 1;
            rkpos.Menge = mengeVerfügbar; 

            await context.SendAsync(new SendOrPostCallback((o) => { rk.Positionen.Add(rkpos); }), null);
            SetModel(rk);
            commands.SetEditMode(EditMode.Append);
            UpdateCommands();
        }


        private async Task AddFromWeAsync(Wareneingang src)
        {

            var bs = await store.BestellungAsync(src.Bestellposition.Bestellung.Id, bus.Token);
            var ls = new Lieferschein { Id = src.Lieferschein.Id, Nummer = src.Lieferschein.Nummer, Datum = src.Lieferschein.Datum };
            var rk = new Reklamation { Bestellung = bs, Datum = DateTime.Now, Lieferschein = ls };  
            var rkpos = new Reklamposition { Artikelbezeichnung = src.Material.Name, ArtikelId = src.Material.Id, Artikelnummer = src.Material.Nummer,
                                             Bestellposition = src.Bestellposition.PositionUndNummer, EingangId = src.Id, Preis = src.Bestellposition.Preis, 
                                             LfArtikelnummer = src.Bestellposition.LfArtikelnummer, Position = 1 };
            var mengeVerfügbar = src.Liefermenge - src.MengeOK.GetValueOrDefault();
            if (mengeVerfügbar <= 0) mengeVerfügbar = 1;
            rkpos.Menge = mengeVerfügbar; 

            await context.SendAsync(new SendOrPostCallback((o) => { rk.Positionen.Add(rkpos); }), null);
            SetModel(rk);
            commands.SetEditMode(EditMode.Append);
            UpdateCommands();
        }


        private async Task AddPosAsync()
        {

            if (detailView.Current == null) return;
 
            var used = detailView.Current.Positionen.Select(q => q.EingangId).ToList();
            var filterList = weList.Where(p => p.Lieferschein_Id == detailView.Current.Lieferschein.Id &&
                                               used.All(q => q != p.Eingang_Id)).ToList();

            if (filterList.Count == 0) {
             
                await messages.ShowMessageAsync(new MessageToken { Header = "Es wurden keine weiteren reklamierbaren Wareneingänge derselben Bestellung gefunden." });
                return;     
            }


            await context.SendAsync(new SendOrPostCallback((o) => {
             
                addView.List = filterList; 
             
            }), null);

            addView.TakeCommand = flyouts.YesCommand;
            if (!await flyouts.OpenConfirmAsync(new FlyoutConfig { ChildView = addView.View, Header = "Weiteren Wareneingang reklamieren", Position = MahApps.Metro.Controls.Position.Right })) return;

            if (addView.Current == null) return;
            var rkpos = new Reklamposition { Artikelbezeichnung = addView.Current.Artikelbezeichnung, ArtikelId = addView.Current.Material_Id, Artikelnummer = addView.Current.Artikelnummer,
                                             Bestellposition = addView.Current.Bestellnummer, EingangId = addView.Current.Eingang_Id, Preis = addView.Current.Preis, 
                                             LfArtikelnummer = addView.Current.LfArtikelnummer, Position = NextPosition };
            var mengeVerfügbar = addView.Current.Liefermenge - addView.Current.MengeOK.GetValueOrDefault();
            if (mengeVerfügbar <= 0) mengeVerfügbar = 1;
            rkpos.Menge = mengeVerfügbar;

            await context.SendAsync(new SendOrPostCallback((o) => { detailView.Current.Positionen.Add(rkpos); }), null);
            UpdateCommands();
        }


        private int NextPosition => detailView.Current.Positionen.IsNullOrEmpty()? 1: detailView.Current.Positionen.Max(p => p.Position) + 1; 


        private async Task DeleteAsync()
        {
            
            var rk = detailView.Current;
            if (rk == null) return;

            if (!rk.IstAngefügt) {
            
                if (!await messages.ConfirmDeleteAsync(new DeleteToken { IsDeleteable = true, Header = "Soll die Reklamation tatsächlich gelöscht werden?" })) return;
                await store.DeleteAsync(rk.Id, bus.Token).ConfigureAwait(false);
            }
            
            await context.SendAsync(new SendOrPostCallback((o) => { listView.Foundlist.Remove(listView.Current); }), null);
            
            SetModel(null);
            commands.SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task DeletePosAsync()
        {
            
            if (detailView.Current == null || baseView.CurrentPosition == null) return;
            
            await context.SendAsync(new SendOrPostCallback((o) => { detailView.Current.Positionen.Remove(baseView.CurrentPosition); }), null);
            UpdateCommands();
        }
    }
}
