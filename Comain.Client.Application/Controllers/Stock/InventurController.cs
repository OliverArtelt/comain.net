﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Services;
using Comain.Client.Services.Stock.Inventuren;
using Comain.Client.ViewModels.Stock.Inventuren;
using Comain.Client.ViewModels;


namespace Comain.Client.Controllers.Stock.Inventuren
{
    
    public class InventurController : ControllerBase, IInventurController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IInventurService store;
        private readonly MainViewModel mainView;
        private readonly InventurViewModel view;
        private readonly InventurListViewModel listView;
        private readonly InventurFilterViewModel filterView;
        private readonly InventurReportViewModel reportView;

        private IList<InventurViewItem> currentList;


        public InventurController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                  IInventurService store, IControllerBus bus, 
                                  MainViewModel mainView, InventurViewModel view, InventurListViewModel listView, InventurFilterViewModel filterView, 
                                  InventurReportViewModel reportView)
          : base(messages, bus)        
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.reportView = reportView;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());

            view.ReportView = reportView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;

            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Inventuren");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;         

            await ExecuteWithLockAsync(_ => LoadAsync());      
        }
 

        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, String.Empty);
        }
  

        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            
            if (args.PropertyName == "CurrentTab") { 
                
                if (view.Tab == InventurViewModel.Tabs.Druck) await UpdatePrintViewAsync(); 
            }
        }

 
        protected override async Task<bool> UnloadImplAsync()
        {
            
            if (listView.Liste != null && listView.Liste.IsChanged && !await messages.ConfirmExitAsync()) return false;
            
            filterView.Unload();
            listView.Unload();
            reportView.Unload();
            currentList = null;
            return true;
        }


        private async Task LoadAsync()
        {

            var qry = await store.ReadAsync(bus.Token).ConfigureAwait(false);
            currentList = qry.Select(p => new InventurViewItem(p)).ToList();
            await SearchAsync(); 
        }


        private async Task SearchAsync()
        {
            
            filterView.ResultText = null;
            var list = currentList;
            if (!String.IsNullOrWhiteSpace(filterView.SearchText)) list = currentList.Where(p => p.Lagerort.Contains(filterView.SearchText, StringComparison.CurrentCultureIgnoreCase)).ToList();

            await context.SendAsync(new SendOrPostCallback((o) => { listView.Liste = new TrackableCollection<InventurViewItem>(list); }), null);

            switch (list.Count) {

            case 0:

                filterView.ResultText = "Keine Einlagerungen gefunden.";
                break;

            case 1:

                filterView.ResultText = "Eine Einlagerungen gefunden.";
                break;

            default:

                filterView.ResultText = String.Format("{0} Einlagerungen gefunden.", list.Count);
                break;
            }

            if (view.Tab == InventurViewModel.Tabs.Druck) await UpdatePrintViewAsync();
        }


        private async Task UpdatePrintViewAsync()
        {

            reportView.ViewerVisible = false;
            if (listView.Liste == null) return;

            var address = "Comain.Client.Reports.Stock.Inventurliste.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = "comain.Client.Reports", Title = "Inventurliste" };
           
            report.AddDataSource("MainData", listView.Liste);
            await reportView.BindViewerAsync(report);
        } 


        private async Task SaveAsync()
        {
          
            if (listView.Liste == null) return;
            var models = listView.Liste.ChangedItems().Select(p => p.Model).ToList();
            if (models.IsNullOrEmpty()) return;

            await store.SaveAsync(models, bus.Token).ConfigureAwait(false);
            await LoadAsync();
        }
    }
}
