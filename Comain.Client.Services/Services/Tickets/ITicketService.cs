﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Tickets
{
    public interface ITicketService
    {

        Task<IList<TicketListDto>>                  SearchAsync(Suchkriterien filter, CancellationToken ct = default);
        Task<MeineTicketAnzahl>                     MeineTicketsAsync(CancellationToken ct = default);
        Task<Ticket>                                DetailAsync(int id, CancellationToken ct = default);
        Task<List<Media>>                           MediaAsync(int auftragId, CancellationToken ct = default);

        Task<(Ticket model, String sendefehler)>    UpdateAsync(TicketUpdateDto model, CancellationToken ct = default);
        Task                                        DeleteAsync(Ticket model, CancellationToken ct = default);
        Task<(Ticket model, String sendefehler)>    StornoAsync(Ticket model, CancellationToken ct = default);
        Task<(Ticket model, String sendefehler)>    CloseAsync(Ticket model, CancellationToken ct = default);

        Task<IList<NummerFilterModel>>              FilterGewerkeAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>            FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<PzFilterModel>>                  FilterStandorteAsync(CancellationToken ct = default);
        Task<IList<KstFilterModel>>                 FilterKostenstellenAsync(CancellationToken ct = default);
        Task<IList<AuIHKSDto>>                      FilterAuIHKSAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>              FilterSchadensbilderAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>              FilterSchadensursachenAsync(CancellationToken ct = default);
        Task<IDringlichkeitenProvider>              FilterDringlichkeitAsync(CancellationToken ct = default);
    }
}