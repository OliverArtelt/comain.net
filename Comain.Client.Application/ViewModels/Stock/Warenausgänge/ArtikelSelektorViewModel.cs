﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Warenausgänge;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{

    public class ArtikelSelektorViewModel : ViewModel<IArtikelSelektorView>
    {
        
        private readonly SynchronizationContext context;


        public ArtikelSelektorViewModel(IArtikelSelektorView view) : base(view)
        {
            
            context = SynchronizationContext.Current;
            SearchCommand = new DelegateCommand(Refresh);
        }


        public void SetzeArtikelliste(IEnumerable<WLItem> liste)
        {

            FoundList = new CollectionViewSource { Source = liste }.View;
            FoundList.Filter = Predicate;
            Refresh();
        }


        private bool Predicate(object arg)
        {

            if (String.IsNullOrEmpty(filter)) return true;
            
            var at = arg as WLItem;
            if (at == null) return false;
            return at.Artikelnummer.Contains(filter, StringComparison.CurrentCultureIgnoreCase) ||
                   at.Artikelname.Contains(filter, StringComparison.CurrentCultureIgnoreCase);
        }

       
        private void Refresh()
        {

            if (FoundList == null) return;
            context.Post(new SendOrPostCallback((o) => { 
            
                FoundList?.Refresh();        
                RaisePropertyChanged("FoundList");
                Current = null;
                var cnt = ((CollectionView)FoundList).Count;

                switch (cnt) {

                case 0: 

                    Result = "Es wurde kein Artikel gefunden.";
                    break;    

                case 1: 

                    Result = "Es wurde ein Artikel gefunden.";
                    break;    

                default: 

                    Result = String.Format("Es wurden {0} Artikel gefunden.", cnt);
                    break;    
                }

            }), null);
        } 


#region P R O P E R T I E S


        private ICommand takeCommand;
        public ICommand TakeCommand
        { 
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


        private WLItem current;
        public WLItem Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }   
            set { SetProperty(ref searchCommand, value); }
        }


        private String filter;
        public String Filter
        {
            get { return filter; }   
            set { SetProperty(ref filter, value); }
        }


        private String result;
        public String Result
        {
            get { return result; }   
            set { SetProperty(ref result, value); }
        }


        public ICollectionView FoundList { get; private set; }
 

        private AuSelektorDto auftrag;
        public AuSelektorDto Auftrag
        {
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }
 

        private IEnumerable aufträge;
        public IEnumerable Aufträge
        {
            get { return aufträge; }
            set { SetProperty(ref aufträge, value); }
        }
 

#endregion
   
    }
}
