﻿using System;
using Comain.Client.Trackers;


namespace Comain.Client.Dtos.Verwaltung
{

    public class DringlichkeitDto : ChangeTrackingBase
    {
        
        public int          Auftragstyp     { get; set; }
        public int          Position        { get; set; }
        public String       Nummer          { get; set; }
        public String       Bezeichnung     { get; set; }
        public bool         WirdVerwendet   { get; set; }
        public int?         Leistungsart    { get; set; }
        public int?         Tagesfrist      { get; set; }
    }
}
