﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.ViewModels.WPlan.Reports
{
    public interface IPlanReport : IPlanOutput
    {
        /// <remarks>
        /// model.Parameter = Filter mit denen der Plan erstellt wurde.
        /// parameter = aktueller Filter
        /// </remarks>
        Task<ReportResult> GetReportAsync(Plan model, WPlanCreateInfo parameter, PlanOptions options, CancellationToken ct = default);
    }
}
