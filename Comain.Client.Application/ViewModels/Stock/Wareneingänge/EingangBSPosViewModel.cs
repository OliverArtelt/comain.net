﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Views.Stock.Wareneingänge;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{

    public class EingangBSPosViewModel : ViewModel<IEingangBSPosView>
    {

        private IEnumerable<BestellposSelektorItem> items;
        private readonly SynchronizationContext context;

 
        public EingangBSPosViewModel(IEingangBSPosView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {
            Positionen = null;
        }
  

        private bool Predicate(object arg)
        {

            var bpos = arg as BestellposSelektorItem;
            if (bpos == null) return false;
            if (!NurOffene) return true;
            return bpos.MengeOffen > 0m;
        }


        public void SetzePositionen(IEnumerable<BestellposSelektorItem> items)
        {

            this.items = items;
            Positionen = null;
            if (items != null) {
                
                Positionen = new CollectionViewSource { Source = items }.View;
                Positionen.Filter = Predicate;
                Positionen?.Refresh();
            }

            RaisePropertyChanged("Positionen");
        }


        public IEnumerable<BestellposSelektorItem> GetSelected()
        {
            return items.Where(p => p.IsSelected);
        }


        private void Refresh()
        {
            
            context.Post(new SendOrPostCallback((o) => { 
            
                Positionen?.Refresh();        
                RaisePropertyChanged("Positionen");
            }), null);
        }   


#region P R O P E R T I E S
 

        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private bool nurOffene;
        public bool NurOffene
        {
            get { return nurOffene; }
            set { if (SetProperty(ref nurOffene, value)) Refresh(); }
        }
 

        public ICollectionView Positionen  { get; private set; }
 

        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }


#endregion

    }
}