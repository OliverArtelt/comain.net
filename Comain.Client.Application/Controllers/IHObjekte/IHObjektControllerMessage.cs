﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Controllers.IHObjekte
{

    public class IHObjektControllerMessage : ControllerMessage
    {

        public  IHObjekt    IHObjekt        { get; set; }
        public  int?        Massnahme_Id    { get; set; }
    }
}
