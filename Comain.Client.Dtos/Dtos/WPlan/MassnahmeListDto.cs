﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.WPlan
{

    public class MassnahmeListDto : ChangeTrackingBase
    {

        public String       Maschine                { get; set; }
        public String       Baugruppe               { get; set; }
        public String       IHObjekt                { get; set; }
        public String       Nummer                  { get; set; }
        public String       Name                    { get; set; }
        public String       Kurzbeschreibung        { get; set; }
        public String       Leistungsart            { get; set; }
        public String       Gewerk                  { get; set; }
        public String       ExterneAuftragsnummer   { get; set; }
        public int          Termintyp               { get; set; }
        public int          Tagesmuster             { get; set; }
        public int          Zyklus                  { get; set; }
        public String       Status                  { get; set; }
        public DateTime?    Letzter                 { get; set; }
        public DateTime?    Nächster                { get; set; }
        public int          AnzahlPositionen        { get; set; }
        public int?         Leistungsart_Id         { get; set; }
        public int?         Gewerk_Id               { get; set; }
        public int?         Menge                   { get; set; }
        public int?         Normzeit                { get; set; }
    }
}
