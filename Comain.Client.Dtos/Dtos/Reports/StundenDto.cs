﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class StundenDto
    {

        public String   Klammer             { get; set; }
        public String   Nummer              { get; set; }
        public String   Nachname            { get; set; }
        public String   Vorname             { get; set; }
        public String   Auftrag             { get; set; }
        public int?     InterneAst          { get; set; }
        public String   Kurzbeschreibung    { get; set; }
        public DateTime Beginn              { get; set; }
        public DateTime Ende                { get; set; }
        public int      Dauer               { get; set; }
        public int      Menge               { get; set; }
        public String   Aufschläge          { get; set; }
    }
}
