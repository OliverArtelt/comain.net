﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class SbSuViewModel : ViewModel<ISbSuView>, IFilterViewModel
    {

        private readonly IFilterService store;


        public SbSuViewModel(IFilterService store, ISbSuView view) : base(view)
        {
            this.store = store;
        }


        public void Reset()
        {
            
            Schadensbild = null;
            Schadensursache = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            var bld = new StringBuilder();
            if (Schadensbild != null) bld.Append("Schadensbild ").Append(Schadensbild.Name).Append(", ");
            if (Schadensursache != null) bld.Append("Schadensursache ").Append(Schadensursache.Name).Append(", ");
            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString(); 
        }


        public void AddFilter(Suchkriterien filter)
        {
 
            filter.Add("SB", Schadensbild.Id);
            filter.Add("SU", Schadensursache.Id);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            
            RaisePropertyChanged("Schadensbilder");
            RaisePropertyChanged("Schadensursachen");
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        public IList<NummerFilterModel> Schadensbilder
        { 
            get { return store == null? null: store.Schadensbilder; }
        } 
   

        public IList<NummerFilterModel> Schadensursachen
        { 
            get { return store == null? null: store.Schadensursachen; }
        } 
   

        private NummerFilterModel schadensbild;
        public NummerFilterModel Schadensbild
        { 
            get { return schadensbild; }
            set { SetProperty(ref schadensbild, value); }
        } 
   

        private NummerFilterModel schadensursache;
        public NummerFilterModel Schadensursache
        { 
            get { return schadensursache; }
            set { SetProperty(ref schadensursache, value); }
        } 
   

#endregion

    }
}
