﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Stock.Reklamationen;

namespace Comain.Client.Views.Stock.Reklamationen
{

    public partial class ReklamListView:UserControl  ,IReklamListView
    {
        public ReklamListView()
        {
        InitializeComponent();
        }
  

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as ReklamListViewModel;
            if (vm != null && vm.DetailCommand != null) vm.DetailCommand.Execute(sender);
        }
   }
}
