﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Comain.Client.Models;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Reklamationen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Reklamationen
{
    
    public class ReklamDetailBaseViewModel : ValidatableViewModel<IReklamDetailBaseView, Reklamation>
    {
 
        private readonly SynchronizationContext context;


        public ReklamDetailBaseViewModel(IReklamDetailBaseView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(Reklamation oldSelected)
        {
           
            RaisePropertyChanged("Datum");
            RaisePropertyChanged("Notizen");
            RaisePropertyChanged("Bestellung");
            RaisePropertyChanged("Lieferschein");
            RaisePropertyChanged("Lieferant");
            RaisePropertyChanged("Positionen");
            RaisePropertyChanged("CurrentPosition");
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S

         
        public String Lieferant
        { 
            get { return Current == null? null: Current.Bestellung.Lieferant.NameUndOrt; }
        }
                 
         
        public String Lieferschein
        { 
            get { return Current == null? null: Current.Lieferschein.Nummer; }
        }
                  
         
        public String Bestellung
        { 
            get { return Current == null? null: Current.Bestellung.Nummer; }
        }
                  
         
        public ObservableCollection<Reklamposition> Positionen
        { 
            get { return Current == null? null: Current.Positionen; }
        }
                  
         
        public DateTime? Datum
        { 
            get { return Current == null? (DateTime?)null: Current.Datum; }
            set { 
            
                if (current != null && current.Datum != value && value.HasValue) {

                    current.Datum = value.Value;
                    RaisePropertyChanged("Datum");
                }
            }
        }
                 
         
        public String Notizen
        { 
            get { return Current == null? null: Current.Notizen; }
            set { 
            
                if (current != null && current.Notizen != value) {

                    current.Notizen = value;
                    RaisePropertyChanged("Notizen");
                }
            }
        }
   
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
                

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
 

        private Reklamposition currentPosition;
        public Reklamposition CurrentPosition
        { 
            get { return currentPosition; }
            set { SetProperty(ref currentPosition, value); }
        }

#endregion

    }
}
