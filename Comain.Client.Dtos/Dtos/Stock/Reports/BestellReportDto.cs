﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class BestellReportDto
    {

        public int          BestellId         { get; set; }
        public String       Nummer            { get; set; }
        public String       Lieferant         { get; set; }
        public DateTime?    AusgangAm         { get; set; }
        public DateTime?    Liefertermin      { get; set; }
        public String       Status            { get; set; }
        public decimal?     GesamtpreisBS     { get; set; }

        public int?         Position          { get; set; }
        public String       Artikelnummer     { get; set; }
        public String       LfArtikelnummer   { get; set; }  
        public String       Artikelname       { get; set; }
        public String       Einheit           { get; set; }
        public decimal      Menge             { get; set; }
        public decimal?     Einzelpreis       { get; set; }
        public decimal?     GesamtpreisPos    { get; set; }
    }
}
