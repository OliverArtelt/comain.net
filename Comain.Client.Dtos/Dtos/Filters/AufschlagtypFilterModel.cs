﻿using System;
using Comain.Client.Dtos;

namespace Comain.Client.Dtos.Filters
{

    public class AufschlagtypFilterModel : EntityModelBase, IComparable<AufschlagtypFilterModel>
    {

        public String     NameKurz         { get; set; }
        public String     NameLang         { get; set; }
        public String     Bemerkung        { get; set; }

        public bool?      IstStandard      { get; set; }
        public bool?      Samstags         { get; set; }
        public bool?      Sonntags         { get; set; }
        public bool?      Feiertags        { get; set; }

        public TimeSpan?  UhrzeitVon       { get; set; }
        public TimeSpan?  UhrzeitBis       { get; set; }
        public decimal?   Prozent          { get; set; }


        public int CompareTo(AufschlagtypFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            if (this.NameKurz == null && other.NameKurz == null) return 0;
            if (this.NameKurz == null) return -1;

            return this.NameKurz.CompareNatural(other.NameKurz);
        }
    }
}
