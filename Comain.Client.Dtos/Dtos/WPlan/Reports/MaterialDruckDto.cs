﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Reports
{
  
    public class MaterialDruckDto
    {

        public String                   Name            { get; set; }
        public String                   Nummer          { get; set; }
        public String                   Einheit         { get; set; }
        public decimal?                 Menge           { get; set; }
    }
}
