﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Users
{

    public enum LoginStatus
    {
       
        Anfrage = 0,
        Mailbestaetigung = 1,
        Aktiv = 2,
        Gesperrt = 3
    }
}
