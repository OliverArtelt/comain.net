﻿using System.Threading;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Services.Verwaltung
{

    public interface IKostenstellenService
    {
        Comain.TrackableCollection<Kostenstelle> CurrentKstList { get; }
        bool HasChanges { get; }
        System.Threading.Tasks.Task LoadAsync(CancellationToken ct = default);
        void Reset();
        System.Threading.Tasks.Task SaveAsync(CancellationToken ct = default);
        System.Collections.Generic.IEnumerable<Comain.Client.ViewData.Verwaltung.PzSelektorItem> Standorte(Kostenstelle kst);
    }
}
