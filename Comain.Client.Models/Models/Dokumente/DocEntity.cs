﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Dokumente
{

    public abstract class DocEntity : Entity
    {

        private ICollection<Dokument> dokumente;
        public ICollection<Dokument> Dokumente
        {
            get => dokumente ?? (dokumente = new HashSet<Dokument>());
            set => SetProperty(ref dokumente, value);
        }


        public bool DocumentsLoaded => dokumente != null;
        public bool DocumentsChanged => DocumentsLoaded &&
                                        (HasChangedProp(nameof(Dokumente)) || Dokumente.Any(p => p.IsChanged));


        public void AddDocs(IEnumerable<Dokument> models)
        {

            if (models.IsNullOrEmpty()) return;
            Dokumente.AddRange(models);
            RaisePropertyChanged(nameof(Dokumente));
        }


        public void RemoveDocs(IEnumerable<Dokument> models)
        {

            if (models.IsNullOrEmpty()) return;
            models.ForEach(p => Dokumente.Remove(p));
            RaisePropertyChanged(nameof(Dokumente));
        }


        public abstract String EntityType { get; }
    }
}
