﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class KstFilterModel : EntityModelBase, IComparable<KstFilterModel>
    {

        public String   Nummer          { get; set; }
        public String   Name            { get; set; }
        public decimal  Sortierung      { get; set; }

        
        public int CompareTo(KstFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return decimal.Compare(this.Sortierung, other.Sortierung);
        }
    }
}
