﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Warenausgänge;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class WAReport : ReportBase, IReport
    {
       
        private readonly DisplayFilterViewModel displayView;
        private readonly IAusgangService store;


        public WAReport(IAusgangService store, DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, AUFilterViewModel auView, KostenstelleViewModel kstView, 
                        LOFilterViewModel loView, ZeitViewModel zeitView)
        {

            this.displayView = displayView;
            this.store = store;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, auView, kstView, loView, zeitView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }


        public override string Title
        {
            get { return "Warenausgang"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 7000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.WarenausgangExport.rdlc": "Comain.Client.Reports.Stock.WarenausgangReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.ReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
