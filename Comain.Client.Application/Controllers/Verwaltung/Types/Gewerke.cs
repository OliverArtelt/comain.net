﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Gewerke : IVwTyp
    {

        public string       Title           => "Gewerke";
        public int          Sortierung      => 1700;
        public Type         Controller      => typeof(IGewerkController);


        public bool HatZugriff(Nutzer nutzer)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }
    }
}
