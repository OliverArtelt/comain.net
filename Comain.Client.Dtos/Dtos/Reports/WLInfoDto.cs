﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Reports
{
    public class WLInfoDto : ChangeTrackingBase
    {

        public decimal          Menge               { get; set; }
        public decimal          Restmenge           { get; set; }
        public decimal?         Preis               { get; set; }
        public DateTime         Datum               { get; set; }
        public int?             Wareneingang_Id     { get; set; }
        public LagerortDto      Lagerort            { get; set; }
    }
}
