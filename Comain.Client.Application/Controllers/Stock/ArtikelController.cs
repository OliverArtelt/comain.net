﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Reports;
using Comain.Client.Services;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels.Stock;
using Comain.Client.Services.Stock.Artikelstamm;
using Comain.Client.ViewModels.Stock.Artikelstamm;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Dtos.Filters;
using Comain.Client.Controllers.Dokumente;

namespace Comain.Client.Controllers.Stock.Artikelstamm
{

    public class ArtikelController : ControllerBase, IArtikelController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IFlyoutService flyouts;
        private readonly IArtikelService store;
        private readonly IArtikelCommandMediator commands;
        private readonly MainViewModel mainView;
        private readonly ArtikelViewModel view;
        private readonly ArtikelListViewModel listView;
        private readonly ArtikelFilterViewModel filterView;
        private readonly ArtikelDetailViewModel detailView;
        private readonly ArtikelDetailBaseViewModel baseView;
        private readonly ArtikelDetailReportViewModel reportView;
        private readonly ArtikelDetailLfViewModel lfView;
        private readonly ArtikelDetailSpecViewModel specView;
        private readonly LfPickViewModel lfPickView;
        private readonly IhoPickViewModel ihoPickView;
        private bool loadCompleted;
        private IList<ArtikelListViewItem> currentList;
        private SerializableCommand copyCommand;
        private SerializableCommand lfCommand;
        private SerializableCommand ihoCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand editCommand;
        private IList<LieferantFilterModel> lflist;
        private IDokumentAssignController docs;


        public ArtikelController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                 IArtikelService store, IArtikelCommandMediator commands, IControllerBus bus,
                                 MainViewModel mainView, ArtikelViewModel view, ArtikelListViewModel listView, ArtikelFilterViewModel filterView,
                                 ArtikelDetailViewModel detailView, ArtikelDetailBaseViewModel baseView, ArtikelDetailReportViewModel reportView,
                                 ArtikelDetailLfViewModel lfView, LfPickViewModel lfPickView, IhoPickViewModel ihoPickView, ArtikelDetailSpecViewModel specView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.baseView = baseView;
            this.reportView = reportView;
            this.lfView = lfView;
            this.lfPickView = lfPickView;
            this.ihoPickView = ihoPickView;
            this.specView = specView;
            this.commands = commands;
        }


        public void Initialize()
        {

            docs = bus.GetController<IMTDokumentController>();

            view.DetailView = detailView.View;
            view.FilterView = filterView.View;
            view.ListView = listView.View;

            detailView.BasisView = baseView.View;
            detailView.LfView = lfView.View;
            detailView.ReportView = reportView.View;
            detailView.DokumentView = docs?.View;
            detailView.SpecView = specView.View;
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            listView.DetailCommand = CreateCommand(_ => DetailAsync());
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());

            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.ReloadCommand = CreateCommand(_ => LoadAsync());
            view.AddCommand = CreateCommand(_ => AddAsync(CopyMode.Add));
            view.CopyCommand = copyCommand = CreateCommand(_ => AddAsync(CopyMode.Copy), _ => listView.Current != null);
            view.DeleteCommand = deleteCommand = CreateCommand(_ => DeleteAsync(), _ => detailView.Current != null);
            view.EditCommand = editCommand = CreateCommand(_ => EditAsync(), _ => detailView.Current != null);
            view.CancelCommand = CreateCommand(_ => CancelAsync());
            lfView.LieferantCommand = lfCommand = CreateCommand(_ => LieferantAsync(), _ => listView.Current != null);
            view.LieferantCommand = lfCommand;
            baseView.IhObjektCommand = ihoCommand = CreateCommand(_ => IhObjektAsync(), _ => listView.Current != null);
            view.IhObjektCommand = ihoCommand;
            view.DeactivateCommand = CreateCommand(_ => DeactivateAsync());
            view.ReactivateCommand = CreateCommand(_ => ReactivateAsync());

            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, String.Empty);
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Ersatzteile");
            crumbs.Add("Artikel");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
            await docs.RunAsync(parameter);

            if (parameter is ControllerMessage msg) {

                if (msg.Entity_Id > 0) await ExecuteWithLockAsync(_ => DetailAsync(msg.Entity_Id));
            }
        }


        private Task BackAsync() => bus.BackAsync();


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, String.Empty);
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (detailView.Current != null && (detailView.Current.IsChanged ||
                                               detailView.Current.MaterialIHObjektMap.IsChanged ||
                                               detailView.Current.MaterialLieferantMap.IsChanged ||
                                               detailView.Current.DocumentsChanged)) {

                detailView.ResetTabsIfPrint();
                if (!await messages.ConfirmExitAsync()) return false;
            }

            await docs.UnloadAsync();

            baseView.Unload();
            reportView.Unload();
            filterView.Unload();
            listView.Unload();
            ihoPickView.Unload();
            SetModel(null);

            return true;
        }


        private async Task LoadAsync()
        {

            SetModel(null);
            SetEditMode(EditMode.ReadOnly);

            loadCompleted = false;
            var oldlistmodel = listView.Current;

            listView.Unload();
            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var kstask = store.KostenstellenAsync(bus.Token);
                var lftask = store.LieferantenAsync(bus.Token);
                var wgtask = store.WarengruppenAsync(bus.Token);
                var ehtask = store.EinheitenAsync(bus.Token);
                var sptask = store.SpecsAsync(bus.Token);
                var attask = LoadListAsync();

                await Task.WhenAll(attask, ihtask, pztask, kstask, lftask, wgtask, ehtask, sptask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();

                await filterView.LoadAsync(ihtask.Result.Select(p => new IHFilterItem(p)).ToList(), pztask.Result, kstask.Result, lftask.Result, bus.Token);
                await OnUIThreadAsync(() => {

                    filterView.Warengruppen = wgtask.Result;
                    lflist = lftask.Result;
                    baseView.SetzeListen(wgtask.Result, ehtask.Result, sptask.Result);
                    ihoPickView.SetIHObjekte(ihtask.Result);
                });

                await docs.LoadAsync();
            }

            if (oldlistmodel != null && !currentList.IsNullOrEmpty()) {

                listView.Current = currentList.FirstOrDefault(p => p.Id == oldlistmodel.Id);
                await DetailAsync();
            }

            loadCompleted = true;
            UpdateCommands();
        }


        private async Task SearchAsync()
        {

            if (loadCompleted) await LoadListAsync();
            else               await LoadAsync();
        }


        private async Task LoadListAsync()
        {

            detailView.ResetTabs();

            using (new VisualDelay(view)) {

                listView.Current = null;

                var qry = await store.ArtikelAsync(filterView.GebeFilter(), bus.Token).ConfigureAwait(false);
                currentList = qry.Select(p => new ArtikelListViewItem(p)).ToList();
                bus.Token.ThrowIfCancellationRequested();

                await context.SendAsync(new SendOrPostCallback((o) => {

                    listView.Foundlist = new ObservableCollection<ArtikelListViewItem>(currentList);
                    filterView.ResultText = String.Format("Filter: {0} Artikel gefunden", currentList.Count);

                }), null);
            }
        }


        private async Task DetailAsync(int? atid = null)
        {

            if (atid.HasValue || listView.Current != null) {

                using (new VisualDelay(detailView)) {

                    var model = await store.DetailAsync(atid ?? listView.Current.Id, bus.Token).ConfigureAwait(false);
                    SetModel(model);
                    if (detailView.Tab == ArtikelDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
                    await docs.DetailAsync(model);
                }

            } else {

                SetModel(null);
                await docs.DetailAsync(null);
            }
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == "CurrentTab") {

                UpdateCommands();
                if (detailView.Tab == ArtikelDetailViewModel.Tabs.Druck) await UpdatePrintViewAsync();
            }
        }


        private async Task UpdatePrintViewAsync()
        {

            reportView.ViewerVisible = false;
            if (detailView.Current == null) return;
            if (detailView.Current.IstAngefügt) return;

            var address = @"Comain.Client.Reports.Stock.LagerReport.rdlc";
            var report = new ReportResult { ReportAddress = address, AssemblyAddress = @"comain.Client.Reports", Title = detailView.Current.Nummer };
            var reportmodel = new ArtikelRow(detailView.Current);

            report.AddDataSource("MainData", new List<ArtikelRow> { reportmodel });
            report.AddDataSource("IHData", reportmodel.IHObjekte);
            report.AddDataSource("LFData", reportmodel.Lieferanten);
            report.AddDataSource("LOData", reportmodel.Lagerorte);

            await reportView.BindViewerAsync(report);
        }


        private void SetModel(Artikel current)
        {

            detailView.Current = current;
            lfView.Current = current;
            baseView.Current = current;
            specView.Current = current;

            if (current != null && listView.Foundlist != null) {

                var listviewitem = listView.Foundlist.FirstOrDefault(p => p.Id == current.Id);
                if (listviewitem != null) listviewitem.Model = current;
            }

            UpdateCommands();
        }


        private void SetEditMode(EditMode newMode)
        {

            commands.SetEditMode(newMode);
            docs.SetEditMode(newMode);
        }


        private void UpdateCommands()
        {

            commands.NeuSetzen();

            context.Post(new SendOrPostCallback((o) => {

                copyCommand.RaiseCanExecuteChanged();
                lfCommand.RaiseCanExecuteChanged();
                ihoCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                editCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private Task EditAsync()
        {

            if (detailView.Current != null) SetEditMode(EditMode.Edit);
            docs.SetEditMode(EditMode.Edit);
            return Task.FromResult(true);
        }


        private async Task CancelAsync()
        {

            if (commands.CurrentMode == EditMode.Append) {

                SetModel(null);

            } else {

                await DetailAsync();
            }

            SetEditMode(EditMode.ReadOnly);
        }


        private async Task SaveAsync()
        {

            if (detailView.Current == null) return;
            if (commands.CurrentMode == EditMode.ReadOnly) return;

            if (detailView.Current.ErrorList.Count > 0) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Der Artikel kann nicht gespeichert werden.", Reasons = detailView.Current.ErrorList });
                return;
            }

            using (new VisualDelay(detailView)) {

                var model = await store.SaveAsync(detailView.Current, bus.Token);
                SetModel(model);
                docs.UpdateEntityId(model);
                await docs.SaveAsync();
                await docs.DetailAsync(model);

                if (commands.CurrentMode == EditMode.Append) await OnUIThreadAsync(() => { listView.Foundlist.Add(new ArtikelListViewItem(model)); });
                SetEditMode(EditMode.ReadOnly);
            }

            await DetailAsync();
        }


        private async Task AddAsync(CopyMode mode)
        {

            Artikel model = null;

            switch (mode) {

            case CopyMode.Copy :

                if (detailView.Current == null) return;
                model = detailView.Current.Clone();
                break;

            case CopyMode.Add :

                model = new Artikel();
                break;

            default:

                return;
            }

            detailView.ResetTabs();
            SetModel(model);
            await docs.DetailAsync(model);
            SetEditMode(EditMode.Append);
        }


        private async Task DeleteAsync()
        {

            if (commands.CurrentMode == EditMode.ReadOnly) return;
            if (detailView.Current == null) return;

            if (!await messages.ConfirmDeleteAsync(new DeleteToken { Header = "Soll der Artikel wirklich gelöscht werden?", IsDeleteable = true })) return;

            await store.DeleteAsync(detailView.Current, bus.Token).ConfigureAwait(false);
            SetEditMode(EditMode.ReadOnly);

            await context.SendAsync(new SendOrPostCallback((o) => {

                if (listView.Foundlist == null) return;
                var lm = listView.Foundlist.FirstOrDefault(p => p.Id == detailView.Current.Id);
                if (lm != null) listView.Foundlist.Remove(lm);

            }), null);

            SetModel(null);
        }


        private async Task DeactivateAsync()
        {

            if (detailView.Current == null) return;
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Artikel wirklich deaktiviert werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.DeactivateAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
                SetModel(model);
            }
        }


        private async Task ReactivateAsync()
        {

            if (detailView.Current == null) return;
            if (!await messages.ConfirmAsync(new MessageToken { Header = "Soll der Artikel wirklich reaktiviert werden?" })) return;

            using (new VisualDelay(detailView)) {

                var model = await store.ReactivateAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
                SetModel(model);
            }
        }


        private async Task LieferantAsync()
        {

            if (commands.CurrentMode == EditMode.ReadOnly) return;
            if (detailView.Current == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                lfPickView.Liste = new ObservableCollection<LfPickItem>(lflist.Select(lf => new LfPickItem(detailView.Current, lf)));

            }), null);

            await flyouts.OpenAsync(new FlyoutConfig { ChildView = lfPickView.View, Header = "Lieferanten auswählen", Position = MahApps.Metro.Controls.Position.Right });
        }


        private async Task IhObjektAsync()
        {

            if (commands.CurrentMode == EditMode.ReadOnly) return;
            if (detailView.Current == null) return;
            await ihoPickView.SetArtikelAsync(detailView.Current);
            await flyouts.OpenAsync(new FlyoutConfig { ChildView = ihoPickView.View, Header = "Assets auswählen", Position = MahApps.Metro.Controls.Position.Right });
        }
    }
}
