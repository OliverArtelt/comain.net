﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class EingangFilterDto
    {
       
        public int        Id              { get; set; }  
        public String     Nummer          { get; set; }  
        public String     Artikel         { get; set; }
    }
}
