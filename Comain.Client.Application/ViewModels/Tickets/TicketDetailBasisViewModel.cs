﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;


namespace Comain.Client.ViewModels.Tickets
{

    public class TicketDetailBasisViewModel : ValidatableViewModel<ITicketDetailBasisView, Ticket>
    {

        private readonly SynchronizationContext context;
        private readonly IHSelektorViewModel ihFilter;
        private IEnumerable<AuIHKSDto> ihkslist;

 
        public TicketDetailBasisViewModel(IHSelektorViewModel ihFilter, ITicketDetailBasisView view) : base(view)
        {

            context = SynchronizationContext.Current;
            this.ihFilter = ihFilter;
            ihFilter.ClearButtonVisible = false;
            PropertyChangedEventManager.AddHandler(ihFilter, ViewPropertyChanged, "CurrentIHObjekt");
        }


        public async Task SetListsAsync(IEnumerable<AuIHKSDto> ihkslist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist, 
                                        IList<NummerFilterModel> sblist, IList<NummerFilterModel> sulist,
                                        CancellationToken ct = default)
        {

            this.ihkslist = ihkslist;
            await ihFilter.LoadAsync(ihkslist.Select(p => new IHFilterItem(p)).ToList(), pzlist, kstlist, ct);

            await context.SendAsync(new SendOrPostCallback((o) => {
            
                IHObjekte = ihkslist.OrderBy(p => p.IHObjekt).ToList();

                RaisePropertyChanged(nameof(IHObjekte));
                RaisePropertyChanged(nameof(AuIHKS));
                RaisePropertyChanged(nameof(IHObjektFilterModel));

                Schadensbilder = sblist;
                Schadensursachen = sulist;
            
            }), null);
        }


        public override void CurrentChanged(Ticket oldSelected)
        {

            ihFilter.SetModel(Current != null && Current.AuIHKS != null ? Current.AuIHKS.Id: (int?)null);

            RaisePropertyChanged(nameof(Schadensbild));
            RaisePropertyChanged(nameof(Schadensursache));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(Aktivitätenbeschreibung));
            RaisePropertyChanged(nameof(Fertigstellungstermin));
            RaisePropertyChanged(nameof(AuIHKS));   
            RaisePropertyChanged(nameof(Standort));   
            RaisePropertyChanged(nameof(Maschine));   
            RaisePropertyChanged(nameof(Baugruppen));   
            RaisePropertyChanged(nameof(Dringlichkeit));   
        }
        

        public void Unload()
        {
        
            ihFilter.Unload();
            Schadensbilder = null;
            Schadensursachen = null;
            IHObjekte = null;      
            Dringlichkeiten = null;
        }
 

        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (ihFilter.CurrentIHObjekt == null) return;
            if (ihkslist.IsNullOrEmpty()) return;
            AuIHKS = ihkslist.FirstOrDefault(p => p.Id == ihFilter.CurrentIHObjekt.Id);
        }
 

#region P R O P E R T I E S


        public IEnumerable<AuIHKSDto>   IHObjekte       { get; private set; }
        public object                   IHSelektor      { get { return ihFilter.View; } }


        public AuIHKSDto AuIHKS
        {
            get { return current == null? null: current.AuIHKS; }   
            set { 
                
                if (current != null && current.AuIHKS != value) {
                    
                    current.AuIHKS = value;
                    RaisePropertyChanged(nameof(AuIHKS));   
                    RaisePropertyChanged(nameof(Standort));   
                    RaisePropertyChanged(nameof(Maschine));   
                    RaisePropertyChanged(nameof(Baugruppen));   
                }
            }
        }
 

        public String Standort
        {
            get { return current?.AuIHKS?.Standort; }   
        }


        public String Maschine
        {
            get { return current?.AuIHKS?.Maschine; }   
        }
 

        public String Baugruppen
        {
            get { return ihFilter.Baugruppenpfad; }   
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


        private IList<Dringlichkeit> dringlichkeiten;
        public IList<Dringlichkeit> Dringlichkeiten
        { 
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        public Dringlichkeit Dringlichkeit
        { 
            get { return current?.Dringlichkeit; }
            set { 
                
                if (Current != null && Current.Dringlichkeit != value) {

                    Current.Dringlichkeit = value;
                    RaisePropertyChanged(nameof(Dringlichkeit));
                }
            }
        }


        private IEnumerable<NummerFilterModel> schadensbilder;
        public IEnumerable<NummerFilterModel> Schadensbilder
        {
            get { return schadensbilder; }   
            set { SetProperty(ref schadensbilder, value); }
        }


        private IEnumerable<NummerFilterModel> schadensursachen;
        public IEnumerable<NummerFilterModel> Schadensursachen
        {
            get { return schadensursachen; }   
            set { SetProperty(ref schadensursachen, value); }
        }


        public NummerFilterModel Schadensbild
        {
            get { return current?.Schadensbild; }   
            set { 
                
                if (current != null && current.Schadensbild != value) {
                    
                    current.Schadensbild = value;
                    RaisePropertyChanged("Schadensbild");
                }
            }
        }
 

        public NummerFilterModel Schadensursache
        {
            get { return current?.Schadensursache; }   
            set { 
                
                if (current != null && current.Schadensursache != value) {
                    
                    current.Schadensursache = value;
                    RaisePropertyChanged("Schadensursache");
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return current?.Kurzbeschreibung; }   
            set { 
                
                if (current != null && current.Kurzbeschreibung != value) {
                    
                    current.Kurzbeschreibung = value;
                    RaisePropertyChanged("Kurzbeschreibung");
                }
            }
        }


        public String Aktivitätenbeschreibung
        {
            get { return current?.Aktivitätenbeschreibung; }   
            set { 
                
                if (current != null && current.Aktivitätenbeschreibung != value) {
                    
                    current.Aktivitätenbeschreibung = value;
                    RaisePropertyChanged("Aktivitätenbeschreibung");
                }
            }
        }


        public DateTime? Fertigstellungstermin
        {
            get { return current?.Fertigstellungstermin; }   
            set { 
                
                if (current != null && current.Fertigstellungstermin != value) {
                    
                    current.Fertigstellungstermin = value;
                    RaisePropertyChanged("Fertigstellungstermin");
                }
            }
        }


#endregion

    }
}
