﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Eine Tabelle einlesen und Zellwerte zur Verfügung stellen
    /// Stile etc können nicht gelesen werden
    /// </summary>
    public class WorksheetReader
    {


        Dictionary<CellAddress, string> myValues; 
        int maxrow; 
        
        /// <summary>
        /// Formatierobjekte um kulturunabhängige Zellinhalte lesen zu können (Dezimalpunkt statt Komma)
        /// </summary>
        CultureInfo invariantCulture = new CultureInfo("");
        NumberStyles floatNumberStyle = NumberStyles.Number | NumberStyles.AllowCurrencySymbol | NumberStyles.Float;
        DateTimeStyles datetimeStyle = DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeLocal;
        
        
        /// <summary>
        /// eine Tabelle einlesen
        /// </summary>
        /// <param name="sheet">Tabelle als XML-Dokument</param>
        /// <param name="strings">Wörterbuch</param>
        public WorksheetReader(String src, StringPoolDecoder dictionary)
        {
        
            myValues = new Dictionary<CellAddress, string>();
            maxrow = 0;
            
            using (TextReader rdr = new StringReader(src)) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                
                var rows = from it in doc.Descendants(ns + "sheetData").Descendants(ns + "row")
                           select it;
                        
                foreach (var row in rows) {

                    //Zeilennummer auslesen
                    int rownum = Int32.Parse(row.Attribute("r").Value);
                    //maximal beschriebene Zeile anpassen
                    if (rownum > maxrow) maxrow = rownum;
           
                    foreach (var cell in row.Elements(ns + "c")) {

                        //Zelladresse
                        XAttribute r = cell.Attribute("r");
                        if (r == null) continue;

                        //Zellwert
                        XElement v = cell.Element(ns + "v");
                        if (v == null) continue;
                        
                        //Zelltyp
                        XAttribute t = cell.Attribute("t");
                        
                        //wurde Zelltyp spezifiziert und ist dieser Typ "s" (== Wörterbuchverknüpfung)?
                        if (t != null && t.Value == "s") {

                            //Daten als Wörterbuchindex interpretieren und String aus diesem holen
                            myValues.Add(new CellAddress(r.Value), dictionary[Int32.Parse(v.Value)]);

                        } else {

                            //Daten uninterpretiert übernehmen
                            myValues.Add(new CellAddress(r.Value), v.Value);
                        }
                    } 
                }
            }
        }
        
     
#region C E L L   R E A D I N G
       
       
        public bool NullAt(int row, int col) { return NullAt(new CellAddress(row, col)); }

        public String StringAt(int row, int col) { return StringAt(new CellAddress(row, col)); }

        public int IntegerAt(int row, int col) { return IntegerAt(new CellAddress(row, col)); }

        public double FloatAt(int row, int col) { return FloatAt(new CellAddress(row, col)); }

        public DateTime DateTimeAt(int row, int col) { return DateTimeAt(new CellAddress(row, col)); }

        
        
        public String this[String cellname] { get { return StringAt(new CellAddress(cellname)); } }

        public bool NullAt(String cellname) { return NullAt(new CellAddress(cellname)); }

        public String StringAt(String cellname) { return StringAt(new CellAddress(cellname)); }

        public int IntegerAt(String cellname) { return IntegerAt(new CellAddress(cellname)); }

        public double FloatAt(String cellname) { return FloatAt(new CellAddress(cellname)); }

        public DateTime DateTimeAt(String cellname) { return DateTimeAt(new CellAddress(cellname)); }

        
        
        public bool NullAt(CellAddress cell)
        {
        
            return !myValues.ContainsKey(cell);
        } 

        
        public String StringAt(CellAddress cell)
        {
        
            String result;
            if (!myValues.TryGetValue(cell, out result)) return String.Empty;
            return result;
        } 

        
        public int IntegerAt(CellAddress cell)
        {
        
            String resstr;
            int resint;
            if (!myValues.TryGetValue(cell, out resstr)) return 0;
            Int32.TryParse(resstr, out resint);
            return resint;
        } 

        
        public double FloatAt(CellAddress cell)
        {
        
            String resstr;
            double resflt;
            if (!myValues.TryGetValue(cell, out resstr)) return 0.0;
            Double.TryParse(resstr, floatNumberStyle, invariantCulture, out resflt);
            return resflt;
        } 

        
        public DateTime DateTimeAt(CellAddress cell)
        {
        
            String resstr;
            DateTime resdat;
            if (!myValues.TryGetValue(cell, out resstr)) return DateTime.MinValue;
            //1. als String angegeben
            if (DateTime.TryParse(resstr, out resdat)) return resdat;
            //2. als kulturunabhängiger String angegeben
            if (DateTime.TryParse(resstr, invariantCulture, datetimeStyle, out resdat)) return resdat;
            //3. als Excel-kodierter Zahlenwert angegeben
            double resdbl;
            if (!Double.TryParse(resstr, NumberStyles.AllowDecimalPoint, invariantCulture, out resdbl)) return DateTime.MinValue;
            return DateTime.FromOADate(resdbl);
        } 


#endregion
                              
        /// <summary>
        /// höchste beschriebene Zeile übergeben
        /// </summary>
        public int MaxRow { get { return maxrow; } }
    }
}
