﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Comain.Client.Controllers
{
    public interface IUndoRedoCommand
    {

        bool ModelHasChanged  { get; }

        void Redo();
        void Undo();
    }
}
