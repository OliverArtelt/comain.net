﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class NutzerCommand : IControllerCommand 
    {

        public Type         Controller  => typeof(INutzerController);
        public String       Name        => "Anwender";
        public String       Parameter   => "Users";
        public String       Gruppe      => "Verwaltung";
        public int          Sortierung  => 3000;
        public String       Color       => "SlateGray";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.user.png";
        public String       LiveValue   => null;
        public String       LiveText    => null; 
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
