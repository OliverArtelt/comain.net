﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Tickets
{

    public class AddTicketService : IAddTicketService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public AddTicketService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<AuIHKSDto>> IHObjekteAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<AuIHKSDto>>("/Filter/AuIHKS", ct);
            return qry.OrderBy(p => p.IHObjekt).ToList();
        }


        public async Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);
            return qry.OrderBy(p => p.Nummer).ToList();
        }


        public async Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct);
            return qry.OrderBy(p => p.Nummer).ToList();
        }


        public async Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
        {

            var qry = await store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);
            return qry.OrderBy(p => p.Nummer).ToList();
        }


        public async Task<(Auftrag model, String sendefehler)> CreateAsync(NeuesTicketDto dto, CancellationToken ct = default)
        {

            var url = "/Ticket/Create";
            var resultDto = await store.PostAsync<NeuesTicketDto, TicketDetailDto>(url, dto, ct).ConfigureAwait(false);
            var resultModel = mapper.Map<TicketDetailDto, Auftrag>(resultDto);
            return (model:resultModel, sendefehler:resultDto.Sendefehler);
        }


        public async Task<String> IHPathAsync(String ihnummer, CancellationToken ct = default)
        {

            var url = String.Format("/IH/Path/{0}", ihnummer);
            var dtolist = await store.GetAsync<List<IHInfoDto>>(url, ct).ConfigureAwait(false);
            if (dtolist.IsNullOrEmpty()) return null;
            return String.Join(" • ", dtolist.Select(p => p.Name).ToArray());
        }


        public async Task<IDringlichkeitenProvider> DringlichkeitAsync(CancellationToken ct = default)
        {

            var list = await store.GetAsync<IList<Dringlichkeit>>("/VW/DR", ct);
            return new DringlichkeitenProvider(list);
        }
    }
}
