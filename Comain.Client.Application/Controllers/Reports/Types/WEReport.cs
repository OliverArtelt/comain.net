﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Wareneingänge;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class WEReport : ReportBase, IReport
    {

        private readonly IEingangService store;
        private readonly DisplayFilterViewModel displayView;


        public WEReport(IEingangService store, DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, BSFilterViewModel bsView,
                        AUFilterViewModel auView, ZeitViewModel zeitView)
        {

            this.store = store;
            this.displayView = displayView;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, bsView, auView, zeitView };
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }


        public override string Title
        {
            get { return "Wareneingang"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 4000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.WareneingangExport.rdlc": "Comain.Client.Reports.Stock.WareneingangReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.ReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
