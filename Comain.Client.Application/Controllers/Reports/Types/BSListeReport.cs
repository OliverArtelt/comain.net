﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Bestellungen;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;

namespace Comain.Client.Controllers.Reports
{

    public class BSListeReport : ReportBase, IReport
    {

        private readonly IBestellService store;
        private readonly DisplayFilterViewModel displayView;


        public BSListeReport(IBestellService store, DisplayFilterViewModel displayView, ArtikelFilterViewModel artikelView, BSFilterViewModel bsView, 
                             ZeitViewModel zeitView)
        {

            this.store = store;
            this.displayView = displayView;
            filterViews = new List<IFilterViewModel> { displayView, artikelView, bsView, zeitView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }


        public override string Title
        {
            get { return "Bestellliste"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 3000; }
        }


        public override string ReportAddress
        {
            get { return displayView.DisplayExport? "Comain.Client.Reports.Stock.BestelllistExport.rdlc": "Comain.Client.Reports.Stock.BestelllistReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.BestellReportAsync(GebeFilter(), ct));
            return result;
        }
    }
}
