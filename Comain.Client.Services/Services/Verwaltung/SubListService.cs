﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{

    public class SubListService : ISubListService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;
        private TrackableCollection<NummerItem> currentList;
        private TrackableCollection<SubNummerItem> currentSubList;


        public SubListService(IRepository store, IMapper mapper)
        {

            this.store = store;
            this.mapper = mapper;
        }


        public async Task<Tuple<TrackableCollection<NummerItem>, TrackableCollection<SubNummerItem>>> GetAsync(String url, CancellationToken ct = default)
        {

            currentList = null;
            currentSubList = null;

            var result = await store.GetAsync<SubListDto>(url, ct).ConfigureAwait(false);
            var mainlist = mapper.Map<List<NummerItem>>(result.Mainlist);
            mainlist.Sort();
            currentList = new TrackableCollection<NummerItem>(mainlist);

            var sublist = mapper.Map<List<SubNummerItem>>(result.Sublist);
            currentSubList = new TrackableCollection<SubNummerItem>(sublist);

            return new Tuple<TrackableCollection<NummerItem>, TrackableCollection<SubNummerItem>>(currentList, currentSubList);
        }


        public bool HasChanges
        {
            get { return currentList != null && currentSubList != null && (currentList.IsChanged || currentSubList.IsChanged); }
        }


        public async Task SaveAsync(String url, CancellationToken ct = default)
        {

            var mainlist = mapper.AsDtoList<NummerItem, NummerDto>(currentList);
            var sublist = mapper.AsDtoList<SubNummerItem, SubNummerDto>(currentSubList);
            if (mainlist == null && sublist == null) return;

            var result = new SubListDto { Mainlist = mainlist, Sublist = sublist };

            await store.PostAsync(url, result, ct).ConfigureAwait(false);
            currentList.AcceptChanges();
            currentSubList.AcceptChanges();
        }


        public void Reset()
        {

            currentList = null;
            currentSubList = null;
        }
    }
}
