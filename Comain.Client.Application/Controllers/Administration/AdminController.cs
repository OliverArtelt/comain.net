﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;


namespace Comain.Client.Controllers.Administration
{

    public interface IAdminController : IController
    {}


    public class AdminController : ControllerBase, IAdminController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly ILoginService login;
        private readonly IModulService module;
        private readonly MainViewModel mainView;
        private readonly AdminViewModel view;
        private readonly IEnumerable<IAdminTyp> types;
        private IAdminTyp current;
        private IController currentController;


        public AdminController(IBreadcrumbService crumbs, MainViewModel mainView, AdminViewModel view,
                               IMessageService messages, ILoginService login, IEnumerable<IAdminTyp> types, IControllerBus bus,
                               IModulService module)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.mainView = mainView;
            this.view = view;
            this.login = login;
            this.types = types;
            this.module = module;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => bus.BackAsync());
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Administration");
            view.DetailView = null;
            current = null;
            view.Current = null;
            mainView.PageView = view.View;

            view.Mandant = login.AktuelleVerbindung;
            view.Einheiten = types.Where(p => p.HatZugriff(module.Info, login.AktuellerNutzer)).OrderBy(p => p.Sortierung).ToList();
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        private async Task LoadEinheitAsync()
        {

            if (currentController != null) await currentController.UnloadAsync();
            current = view.Current;
            if (current == null) return;
            currentController = bus.GetController(current.Controller);

            if (currentController == null) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Dieses Modul steht leider derzeit nicht zur Verfügung." });
                return;
            }

            await currentController.RunAsync(null);
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (current == null || currentController == null) return true;
            return await currentController.UnloadAsync();
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current") await LoadEinheitAsync();
        }
    }
}
