﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.ViewModels.Stock.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class AnwesenheitConfig : IAdminTyp
    {

        public String       Title           => "Anwesenheiten";
        public int          Sortierung      => 40000;
        public Type         Controller      => typeof(IConfigController<AnwesenheitViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => module.VerwendetModulStock;
    }
}
