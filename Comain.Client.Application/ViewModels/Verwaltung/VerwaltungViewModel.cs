﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class VerwaltungViewModel : ViewModel<IVerwaltungView>
    {

        public VerwaltungViewModel(IVerwaltungView view) : base(view)
        {
        }


#region P R O P E R T I E S


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private IEnumerable<IVwTyp> einheiten;
        public IEnumerable<IVwTyp> Einheiten
        { 
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


        private IVwTyp current;
        public IVwTyp Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }
         

        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }

       
#endregion   

    }
}
