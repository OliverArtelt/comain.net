﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comain.Client.Models
{
    public class ModulInfo
    {

        public bool     VerwendetModulAuftrag       { get; set; }
        public bool     VerwendetModulStock         { get; set; }
        public bool     VerwendetModulEmma          { get; set; }
        public bool     VerwendetModulWPlan         { get; set; }
        public bool     VerwendetModulTicket        { get; set; }
        public bool     VerwendetModulBevor         { get; set; }
        public bool     VerwendetModulSpecs         { get; set; }
        public bool     VerwendetModulAnwesenheit   { get; set; }
    }
}
