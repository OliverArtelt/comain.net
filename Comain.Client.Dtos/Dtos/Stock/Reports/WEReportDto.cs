﻿using System;


namespace Comain.Client.Dtos.Stock.Reports
{

    public class WEReportDto
    {

        public int          LieferscheinId      { get; set; }
        public String       Lieferschein        { get; set; }
        public DateTime     Lieferdatum         { get; set; }
        public String       Auftrag             { get; set; }
        public String       Bestellung          { get; set; }
        public DateTime?    Ausgangsdatum       { get; set; }
        public int?         Position            { get; set; }
        public String       Artikelnummer       { get; set; }
        public String       Artikelname         { get; set; }
        public decimal      Bestellmenge        { get; set; }
        public decimal      Liefermenge         { get; set; }
        public decimal?     MengeOK             { get; set; }
        public bool         Reklamiert          { get; set; }
        public String       LieferantName       { get; set; }
        public String       LieferantOrt        { get; set; }
    }
}
