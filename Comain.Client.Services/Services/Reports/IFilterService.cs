﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.ViewData.WPlan;

namespace Comain.Client.Services.Reports
{

    public interface IFilterService
    {

        PeriodeDto                      Periode                 { get; }
        IList<KstFilterModel>           Kostenstellen           { get; }
        IList<PzFilterModel>            Standorte               { get; }
        IList<IHObjektFilterModel>      IHObjekte               { get; }
        IList<IHObjektFilterModel>      IHObjekteVerfügbarkeit  { get; }
        IList<LeistungsartFilterModel>  Leistungsarten          { get; }
        IList<NummerFilterModel>        Objektgruppen           { get; }
        IList<SubNummerFilterModel>     Objektarten             { get; }
        IList<PersonalFilterModel>      Personalnummerliste     { get; }
        IList<PersonalFilterModel>      Personalklammerliste    { get; }
        IList<NummerFilterModel>        Schadensbilder          { get; }
        IList<NummerFilterModel>        Schadensursachen        { get; }
        IList<NummerFilterModel>        Warengruppen            { get; }
        IList<NummerFilterModel>        Lieferscheine           { get; }
        IList<LagerortDto>              Lagerorte               { get; }
        IList<LieferantFilterModel>     Lieferanten             { get; }
        IList<PZFilterItem>             IHTreeListe             { get; }
        IList<int>                      InterneAuftragsstellen  { get; }

        Task LoadAsync(CancellationToken ct = default);
        void Unload();
    }
}
