﻿using System;
using Comain.Client.Dtos.Stock;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{

    public class WLItem
    {

        private readonly EinlagerungDto model;


        public WLItem(EinlagerungDto model)
        {
            this.model = model;
        }


        public EinlagerungDto   Model               { get { return model; } }

        public decimal          Restmenge           { get { return model.Restmenge; } }
        public DateTime         Datum               { get { return model.Datum; } }
        public String           Lagerort            { get { return model.Lagerort.Nummer; } }
        public String           Artikelnummer       { get { return model.Material.Nummer; } }
        public String           Artikelname         { get { return model.Material.Name; } }
    }
}
