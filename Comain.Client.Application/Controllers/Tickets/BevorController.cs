﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Bevor;
using Comain.Client.ViewModels.Bevor;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Services;
using Comain.Client.Views;
using Microsoft.Win32;

namespace Comain.Client.Controllers.Tickets
{

    public class BevorController : ControllerBase, IBevorController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IBevorService store;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IKonfigService config;
        private readonly ITabletInfo tablets;
        private readonly MainViewModel mainView;
        private readonly BevorViewModel view;
        private readonly BevorResultViewModel resultView;
        private readonly CameraViewModel cameraView;
        private readonly ImageEditViewModel editView;
        private SerializableCommand saveCommand;
        private SerializableCommand cameraCommand;
        private SerializableCommand fileCommand;
        private SerializableCommand deleteCommand;
        private SerializableCommand deleteAllCommand;
        private SerializableCommand openImageCommand;
        private SerializableCommand saveImageCommand;
        private SerializableCommand editImageCommand;


        public BevorController(IBreadcrumbService crumbs, IBevorService store, Comain.Client.Services.IMessageService messages, ILoginService login, IFlyoutService flyouts,
                               IKonfigService config, IControllerBus bus, ITabletInfo tablets,
                               MainViewModel mainView, BevorViewModel view, BevorResultViewModel resultView, CameraViewModel cameraView, ImageEditViewModel editView)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.config = config;
            this.store = store;
            this.tablets = tablets;
            this.login = login;
            this.flyouts = flyouts;
            this.mainView = mainView;
            this.view = view;
            this.resultView = resultView;
            this.cameraView = cameraView;
            this.editView = editView;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = saveCommand = CreateCommand(_ => SaveAsync());
            view.ImageFromCameraCommand = cameraCommand = CreateCommand(_ => ImageFromCameraAsync(), _ => view.Current != null);
            view.ImageFromFileCommand = fileCommand = CreateCommand(_ => ImageFromFileAsync(), _ => view.Current != null);
            view.DeleteImageCommand = deleteCommand = CreateCommand(_ => DeleteImageAsync(), _ => view.Current != null && view.CurrentMedia != null);
            view.DeleteImagesCommand = deleteAllCommand = CreateCommand(_ => DeleteImagesAsync(), _ => view.Current != null && !view.Medien.IsNullOrEmpty());
            view.OpenImageCommand = openImageCommand = CreateCommand(_ => OpenImageAsync(), _ => view.Current != null && view.CurrentMedia != null);
            view.SaveImageCommand = saveImageCommand = CreateCommand(_ => SaveImageAsync(), _ => view.Current != null && view.CurrentMedia != null);
            view.EditImageCommand = editImageCommand = CreateCommand(_ => EditImageAsync(), _ => view.Current != null && view.CurrentMedia != null);
            resultView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            editView.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            editView.SaveCommand = CreateCommand(_ => TakeEditedImageAsync());
            cameraView.CompletedCommand = CreateCommand(p => SnapshotDoneAsync(p));

            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
            tablets.TabletModeChanged += (o, e) => { view.TabletMode = tablets.InTabletMode; };
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Besonderes Vorkommnis");
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            resultView.Mandant = login.AktuelleVerbindung;
            cameraView.Mandant = login.AktuelleVerbindung;
            editView.Mandant = login.AktuelleVerbindung;

            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {
            PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var ihtask = store.IHObjekteAsync(bus.Token);
                var pztask = store.StandorteAsync(bus.Token);
                var pstask = store.PersonalAsync(bus.Token);
                var drtask = store.DringlichkeitAsync(bus.Token);

                await Task.WhenAll(ihtask, pztask, pstask, drtask).ConfigureAwait(false);
                await view.LoadAsync(ihtask.Result, pztask.Result, pstask.Result, drtask.Result.Liste(Auftragstyp.BesonderesVorkommnis), bus.Token).ConfigureAwait(false);
            }
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (mainView.PageView == cameraView.View) {

                cameraView.Unload();
                mainView.PageView = view.View;
            }

            if (view.Current != null && view.Current.IsChanged && !await messages.ConfirmExitAsync()) return false;

            view.Unload();
            cameraView.Unload();
            editView.Unload();
            return true;
        }


        private async Task BackAsync()
        {

            if (mainView.PageView == editView.View) {

                mainView.PageView = view.View;
                return;
            }

            await bus.BackAsync();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(view.Current) || args.PropertyName == nameof(view.CurrentMedia)) UpdateCommands();
        }


        private void UpdateCommands()
        {

            context.Send(new SendOrPostCallback((o) => {

                saveCommand.RaiseCanExecuteChanged();
                cameraCommand.RaiseCanExecuteChanged();
                fileCommand.RaiseCanExecuteChanged();
                deleteCommand.RaiseCanExecuteChanged();
                deleteAllCommand.RaiseCanExecuteChanged();
                openImageCommand.RaiseCanExecuteChanged();
                saveImageCommand.RaiseCanExecuteChanged();
                editImageCommand.RaiseCanExecuteChanged();
            }), null);
        }


        private async Task SaveAsync()
        {

            if (view.Current == null) return;
            if (!view.Current.ErrorList.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Das Vorkommnis kann nicht gespeichert werden.", Reasons = view.Current.ErrorList });
                return;
            }

            using (new VisualDelay(view)) {

                var sendefehler = await store.CreateAsync(view.Current, bus.Token).ConfigureAwait(false);
                resultView.SetzeSendefehler(sendefehler);
                view.Current = null;
            }

            mainView.PageView = resultView.View;
        }


        private async Task ImageFromCameraAsync()
        {

            if (view.Current == null) return;

            try {

                mainView.PageView = cameraView.View;
                await cameraView.StartAsync();
            }
            catch (Exception) {

                mainView.PageView = view.View;
                throw;
            }
        }


        private async Task SnapshotDoneAsync(object image)
        {

            try {

                mainView.PageView = view.View;
                var bitmap = image as Bitmap;
                if (bitmap == null) return;
                var model = new Media(bitmap);

                await context.SendAsync(new SendOrPostCallback((o) => {

                    view.Medien.Add(model);

                }), null);
            }
            catch (Exception x) {

                messages.Show(x);
            }

            UpdateCommands();
        }


        private async Task ImageFromFileAsync()
        {

            if (view.Current == null) return;

            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Bilder wählen";

			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                var medien = await store.ReadFilesAsync(dialog.FileNames, bus.Token).ConfigureAwait(false);

                await context.SendAsync(new SendOrPostCallback((o) => {

                    view.Current.Medien.AddRange(medien);
                }), null);
            }
        }


        private async Task DeleteImageAsync()
        {

            if (view.Current == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                view.Current.Medien.Remove(view.CurrentMedia);
            }), null);
        }


        private async Task DeleteImagesAsync()
        {

            if (view.Current == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                view.Current.Medien.Clear();
            }), null);
        }


        private async Task OpenImageAsync()
        {

            if (view.CurrentMedia == null) return;

            await view.CurrentMedia.StoreTempFileAsync(bus.Token).ConfigureAwait(false);
            System.Diagnostics.Process.Start(view.CurrentMedia.TempPath);
        }


        private async Task EditImageAsync()
        {

            if (view.CurrentMedia == null) return;

            await editView.SetImageAsync(view.CurrentMedia.Image);
            mainView.PageView = editView.View;
        }


        private async Task TakeEditedImageAsync()
        {

            if (view.CurrentMedia == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                using (new VisualDelay(view)) {

                    var editedImage = editView.ExportImage();
                    if (editedImage == null) return;

                    //editedImage.Save(@"C:\tmp\erde.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    view.CurrentMedia.LoadFromBitmap(editedImage);
                    mainView.PageView = view.View;
                }

            }), null);
        }


        private async Task SaveImageAsync()
        {

            if (view.CurrentMedia == null) return;

            var dialog = new SaveFileDialog();
            dialog.Filter = "JPEG file (*.jpg)|*.jpg";
            dialog.DefaultExt = "jpg";
            dialog.FileName = view.CurrentMedia.Dateiname;
            dialog.Title = "Speicherort wählen";
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileName.IsNullOrEmpty()) return;

            await view.CurrentMedia.StoreFileAsync(dialog.FileName, bus.Token).ConfigureAwait(false);
        }
    }
}
