﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Comain.Client.Views.WPlan.Planen
{

    public partial class PlanTreeView : UserControl, IPlanTreeView
    {

        public PlanTreeView()
        {
            InitializeComponent();
        }


        public void ExpandTree(bool expand)
        {

            foreach (object obj in Tree.Items) {

                ItemsControl childControl = Tree.ItemContainerGenerator.ContainerFromItem(obj) as ItemsControl;
                TreeViewItem item = childControl as TreeViewItem;
                item.IsExpanded = expand;
            }
        }
    }
}
