﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;

namespace Comain.Client.Gateways
{

    public class ConnectionEndpoint
    {

        public String Key               { get; set; }
        public String Name              { get; set; }
        public String BaseUrl           { get; set; }
        public String Color             { get; set; }
        public bool   UseCompression    { get; set; }

        public String Username          { get; set; }
        public String Password          { get; set; }


        public String   Credentials     { get => String.Format(@"grant_type=password&username={0}&password={1}", Username, Password); }
        public LoginDto LoginData       { get => new LoginDto { Username = Username, Password = Password }; }
    }
}
