﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Comain.Client.Trackers;

namespace Comain
{

    public static class ChangeTrackerExtensions
    {

        public static List<TDto> AsDtoList<TModel, TDto>(this IMapper mapper, TrackableCollection<TModel> src) 
        
                where TDto : ChangeTrackingBase
                where TModel : NotifyBase
        {

            if (src == null || !src.IsChanged) return new List<TDto>();
            var changed = mapper.Map<List<TDto>>(src.ChangedItems());
            changed.ForEach(p => p.ChangeStatus = ChangeStatus.Changed);
            var added = mapper.Map<List<TDto>>(src.AddedItems());
            added.ForEach(p => p.ChangeStatus = ChangeStatus.Added);
            var deleted = mapper.Map<List<TDto>>(src.DeletedItems());
            deleted.ForEach(p => p.ChangeStatus = ChangeStatus.Deleted);

            return changed.Union(added).Union(deleted).ToList();
        }


        public static List<TDto> AsDtoList<TModel, TDto>(this IMapper mapper, TrackableCollection<TModel> src, Func<TModel, TDto> map)

                where TDto : ChangeTrackingBase
                where TModel : NotifyBase
        {

            if (src == null || !src.IsChanged) return new List<TDto>();
            var changed = src.ChangedItems().Select(p => map(p)).ToList();
            changed.ForEach(p => p.ChangeStatus = ChangeStatus.Changed);
            var added = src.AddedItems().Select(p => map(p)).ToList();
            added.ForEach(p => p.ChangeStatus = ChangeStatus.Added);
            var deleted = src.DeletedItems().Select(p => map(p)).ToList();
            deleted.ForEach(p => p.ChangeStatus = ChangeStatus.Deleted);

            var result = changed.Union(added).Union(deleted).ToList();
            return result;
        }


        public static TrackableCollection<T> AsTrackable<T>(this IEnumerable<T> src) where T: NotifyBase
        {
            return new TrackableCollection<T>(src);
        }
    }
}
