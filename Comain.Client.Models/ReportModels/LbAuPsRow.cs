﻿using System;
using Comain.Client.Dtos.Reports;


namespace Comain.Client.ReportModels
{

    public class LbAuPsRow
    {

        public bool         IstAuZeile            { get; set; }
        public String       ImBudget              { get; set; }
        public String       FBNummer              { get; set; }

        public String       KstNummer             { get; set; }
        public String       KstName               { get; set; }
        public String       PzNummer              { get; set; }
        public String       PzName                { get; set; }
        public String       IhNummer              { get; set; }
        public String       IhName                { get; set; }
        public String       Inventarnummer        { get; set; }
        public String       Fabriknummer          { get; set; }

        /// <summary>
        /// Auftragsnummer
        /// </summary>
        public String       Nummer                { get; set; }
        public String       ExterneNummer         { get; set; }
        public String       Kurzbeschreibung      { get; set; }
        public String       Aktivbeschreibung     { get; set; }
        public DateTime?    Auftragsdatum         { get; set; }
        public DateTime?    Abschlußdatum         { get; set; }
        public String       Auftraggeber          { get; set; }
       
        public int?         InterneAuStelle       { get; set; }
        public decimal?     Eigenkosten           { get; set; }
        public decimal?     Materialkosten        { get; set; }
        public decimal?     Fremdkosten           { get; set; }
        public decimal?     MwSt                  { get; set; }
        public decimal?     Gesamtkosten          { get; set; }
        public decimal?     Personalstunden       { get; set; }
        
        public String       PsNummer              { get; set; }
        public String       PsKlammer             { get; set; }
        public decimal?     Kosten                { get; set; }
        public DateTime?    Beginn                { get; set; }
        public DateTime?    Ende                  { get; set; }
        public int?         Dauer                 { get; set; }
    
    
        public LbAuPsRow()
        {}
    
    
        public LbAuPsRow(LbAuDto auDto)
        {
        
            IstAuZeile        = true;
            ImBudget          = auDto.ImBudget? "Ja": "Nein";          
            FBNummer          = auDto.FBNummer;
                      
            KstNummer         = auDto.KstNummer;          
            KstName           = auDto.KstName;            
            PzNummer          = auDto.PzNummer;           
            PzName            = auDto.PzName;             
            IhNummer          = auDto.IhNummer;           
            IhName            = auDto.IhName;             
            Inventarnummer    = auDto.Inventarnummer;     
            Fabriknummer      = auDto.Fabriknummer;       
                            
            Nummer            = auDto.Nummer;             
            ExterneNummer     = auDto.ExterneNummer;      
            Kurzbeschreibung  = auDto.Kurzbeschreibung;   
            Aktivbeschreibung = auDto.Aktivbeschreibung;  
            Auftragsdatum     = auDto.Auftragsdatum;      
            Abschlußdatum     = auDto.Abschlußdatum;      
            Auftraggeber      = auDto.Auftraggeber;       
                           
            InterneAuStelle   = auDto.InterneAuStelle;    
            Eigenkosten       = auDto.Eigenkosten;        
            Materialkosten    = auDto.Materialkosten;     
            Fremdkosten       = auDto.Fremdkosten;        
            MwSt              = auDto.MwSt;               
            Gesamtkosten      = auDto.Gesamtkosten;       
            Personalstunden   = auDto.Personalstunden;    
        }
    
    
        public LbAuPsRow(LbAuDto auDto, LbAuPsItem psDto) 
        {
            
            IstAuZeile        = false;
            Nummer            = auDto.Nummer;             
            PsNummer          = psDto.PsNummer;           
            PsKlammer         = psDto.PsKlammer;          
            Kosten            = psDto.Kosten;             
            Beginn            = psDto.Beginn;             
            Ende              = psDto.Ende;               
            Dauer             = psDto.Dauer;                     
        }
    }
}
