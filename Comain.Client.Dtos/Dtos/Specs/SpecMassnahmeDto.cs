﻿using System;
using System.Collections.Generic;
using System.Text;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Specs
{

    public class SpecMassnahmeDto : ChangeTrackingBase
    {

        public String                   Nummer                  { get; set; }
        public String                   Name                    { get; set; }
        public String                   Kurzbeschreibung        { get; set; }
        public int                      Zyklus                  { get; set; }
        public int                      Termintyp               { get; set; }
        public int                      Tagesmuster             { get; set; }
        public DateTime?                Start                   { get; set; }
        public int?                     Normzeit                { get; set; }
        public int?                     Menge                   { get; set; }
        public decimal?                 Wert                    { get; set; }
        public String                   ExterneAuftragsnummer   { get; set; }
        public String                   Qualifikation           { get; set; }
        public String                   Werkzeug                { get; set; }
        public String                   Durchführung            { get; set; }
        public String                   Sicherheit              { get; set; }
        public String                   Dokumentation           { get; set; }

        public int                      Spezifikation_Id        { get; set; }

        public NummerFilterModel        Gewerk                  { get; set; }
        public LeistungsartFilterModel  Leistungsart            { get; set; }
    }
}
