﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Trackers;
using Comain.Client.ViewData.Verwaltung;

namespace Comain.Client.Services.Aufträge
{

    public class AuftragService : IAuftragService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public AuftragService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<AuftragListDto>> AufträgeAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var url = "/Auftrag/Search";
            var result = await store.FindAsync<IList<AuftragListDto>>(url, filter, ct).ConfigureAwait(false);
            return result.OrderByDescending(p => p.Projekt)
                         .ThenBy(p => p.Nummer, new NaturalComparer())
                         .ToList();
        }


        public async Task<Auftrag> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/Auftrag/Detail/{0}", id);
            var dto = await store.GetAsync<AuftragDetailDto>(url, ct).ConfigureAwait(false);
            if (dto == null) return null;

            var model = mapper.Map<AuftragDetailDto, Auftrag>(dto);
            model.Projektnummer = dto.Projektnummer;
            model.AcceptChanges();
            return model;
        }


        public async Task<String> IHPathAsync(String ihnummer, CancellationToken ct = default)
        {

            if (String.IsNullOrEmpty(ihnummer)) return null;

            var url = String.Format("/IH/Path/{0}", ihnummer);
            var dtolist = await store.GetAsync<List<IHInfoDto>>(url, ct).ConfigureAwait(false);
            if (dtolist.IsNullOrEmpty()) return null;
            return String.Join(" • ", dtolist.Select(p => p.Name).ToArray());
        }


        public async Task<Auftrag> CreateAsync(NeuerAuftragDto dto, CancellationToken ct = default)
        {

            var url = "/Auftrag/Create";
            var result = await store.PostAsync<NeuerAuftragDto, AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
            return mapper.Map<AuftragDetailDto, Auftrag>(result);
        }


        public async Task<Auftrag> SaveAsync(Auftrag model, CancellationToken ct = default)
        {

            if (!model.IsChanged) return model;
            var url = "/Auftrag/Detail";
            var dto = mapper.Map<Auftrag, AuftragDetailDto>(model);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<AuftragDetailDto, AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<AuftragDetailDto, Auftrag>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public async Task DeleteAsync(Auftrag model, CancellationToken ct = default)
        {

            var url = "/Auftrag/Delete";
            var dto = mapper.Map<Auftrag, AuftragDetailDto>(model);
            dto.ChangeStatus = ChangeStatus.Deleted;

            await store.PostAsync<AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
        }


        public async Task<Auftrag> StornoAsync(Auftrag model, CancellationToken ct = default)
        {

            var url = "/Auftrag/Storno";
            var dto = mapper.Map<Auftrag, AuftragDetailDto>(model);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<AuftragDetailDto, AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<AuftragDetailDto, Auftrag>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public async Task<Auftrag> CloseAsync(Auftrag model, CancellationToken ct = default)
        {

            var url = "/Auftrag/Close";
            var dto = mapper.Map<Auftrag, AuftragDetailDto>(model);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<AuftragDetailDto, AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<AuftragDetailDto, Auftrag>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public async Task<Auftrag> ReopenAsync(int auftrag, String reason, CancellationToken ct = default)
        {

            var url = "/Auftrag/Reopen";
            var dto = new ReopenDto { Id = auftrag, Reason = reason };

            var result = await store.PostAsync<ReopenDto, AuftragDetailDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<AuftragDetailDto, Auftrag>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public async Task<Auftrag> NeuerUnterauftragAsync(int mainId, CancellationToken ct = default)
        {

            var url = String.Format("/Auftrag/Create/{0}", mainId);

            var newdto = await store.GetAsync<AuftragDetailDto>(url, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<AuftragDetailDto, Auftrag>(newdto);
            return newmodel;
        }


        public Task<IList<PzFilterModel>> FilterStandorteAsync(CancellationToken ct = default)
        {
            return FilterAsync<PzFilterModel>("/Filter/PZ", ct);
        }


        public Task<IList<NummerFilterModel>> FilterGewerkeAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/GW", ct);
        }


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
        {
            return FilterAsync<PersonalFilterModel>("/Filter/PS", ct);
        }


        public Task<IList<IHObjektFilterModel>> FilterIHObjekteAsync(CancellationToken ct = default)
        {
            return FilterAsync<IHObjektFilterModel>("/Filter/IH", ct);
        }


        public Task<IList<KstFilterModel>> FilterKostenstellenAsync(CancellationToken ct = default)
        {
            return FilterAsync<KstFilterModel>("/Filter/KST", ct);
        }


        public Task<IList<LeistungsartFilterModel>> FilterLeistungsartenAsync(CancellationToken ct = default)
        {
            return FilterAsync<LeistungsartFilterModel>("/Filter/LA", ct);
        }


        public Task<IList<NummerFilterModel>> FilterSchadensbilderAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/SB", ct);
        }


        public Task<IList<NummerFilterModel>> FilterSchadensursachenAsync(CancellationToken ct = default)
        {
            return FilterAsync<NummerFilterModel>("/Filter/SU", ct);
        }


        public Task<IList<SubNummerFilterModel>> FilterSubSchadensbilderAsync(CancellationToken ct = default)
        {
            return FilterAsync<SubNummerFilterModel>("/Filter/SBSub", ct);
        }


        public Task<IList<SubNummerFilterModel>> FilterSubSchadensursachenAsync(CancellationToken ct = default)
        {
            return FilterAsync<SubNummerFilterModel>("/Filter/SUSub", ct);
        }


        public Task<IList<AuIHKSDto>> FilterAuIHKSAsync(CancellationToken ct = default)
        {
            return FilterAsync<AuIHKSDto>("/Filter/AuIHKS", ct);
        }


        public async Task<IDringlichkeitenProvider> FilterDringlichkeitAsync(CancellationToken ct = default)
        {

            var list = await FilterAsync<Dringlichkeit>("/VW/DR", ct);
            return new DringlichkeitenProvider(list);
        }


        public Task<IList<int>> FilterInterneAuStelleAsync(CancellationToken ct = default)
        {
            return FilterAsync<int>("/Filter/IntAUStelle", ct);
        }


        private Task<IList<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<IList<TFilterModel>>(url, ct);
        }
    }
}
