﻿using System;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Formatierung eines Zellwertes
    /// </summary>
    public enum Format
    {
        /// <summary>
        /// Standardzahl
        /// </summary>
        Standard = 0,
        /// <summary>
        /// Auf ganze $,€ gerundet, negative Werte werden nicht rot gefärbt
        /// </summary>
        MoneyRounded = 6,
        /// <summary>
        /// mit Cents, negative Werte werden nicht rot gefärbt
        /// </summary>
        Money = 8,
        /// <summary>
        /// 12.04.2005
        /// </summary>
        Date = 14, 
        /// <summary>
        /// 10:03
        /// </summary>
        Time = 20, 
        /// <summary>
        /// 10:03:20
        /// </summary>
        TimeFull = 21,
        /// <summary>
        /// 12.04.2005 10:03
        /// </summary>
        DateTime = 22
    }


    /// <summary>
    /// Nummerformatobjekt, es werden z.Z. nur eingebaute Formate unterstützt
    /// </summary>
    public class NumberFormat : ICellStyleType, IEquatable<NumberFormat>
    {
        
        public Format format;
        
        
        public NumberFormat() : this(Format.Standard) {}

        public NumberFormat(Format f)
        {
            format = f;
        }


        public bool IsDefault()
        {
            return format == Format.Standard;
        }

        
        public void WriteXML(XElement parent)
        {
            throw new NotImplementedException();
        }

        
        public bool Equals(NumberFormat other)
        {
            return this.format == other.format;
        }
    }
}
