﻿using System;
using System.Collections.ObjectModel;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragViewModel : ViewModel<IAuftragView>, IDelayableViewModel
    {

        public AuftragViewModel(IAuftragView view) : base(view)
        {
        }


#region P R O P E R T I E S


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        {
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private ICommand editCommand;
        public ICommand EditCommand
        {
            get { return editCommand; }
            set { SetProperty(ref editCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }


        private ICommand stornoCommand;
        public ICommand StornoCommand
        {
            get { return stornoCommand; }
            set { SetProperty(ref stornoCommand, value); }
        }


        private ICommand reopenCommand;
        public ICommand ReopenCommand
        {
            get { return reopenCommand; }
            set { SetProperty(ref reopenCommand, value); }
        }


        private ICommand lENeuCommand;
        public ICommand LENeuCommand
        {
            get { return lENeuCommand; }
            set { SetProperty(ref lENeuCommand, value); }
        }


        private ICommand lEEditCommand;
        public ICommand LEEditCommand
        {
            get { return lEEditCommand; }
            set { SetProperty(ref lEEditCommand, value); }
        }


        private ICommand mTNeuCommand;
        public ICommand MTNeuCommand
        {
            get { return mTNeuCommand; }
            set { SetProperty(ref mTNeuCommand, value); }
        }


        private ICommand mTEditCommand;
        public ICommand MTEditCommand
        {
            get { return mTEditCommand; }
            set { SetProperty(ref mTEditCommand, value); }
        }


        private ICommand fbNummerCommand;
        public ICommand FbNummerCommand
        {
            get { return fbNummerCommand; }
            set { SetProperty(ref fbNummerCommand, value); }
        }


        private ICommand positionDetailCommand;
        public ICommand PositionDetailCommand
        {
            get { return positionDetailCommand; }
            set { SetProperty(ref positionDetailCommand, value); }
        }


        private bool editVisible;
        public bool EditVisible
        {
            get { return editVisible; }
            set { SetProperty(ref editVisible, value); }
        }


        private bool addVisible;
        public bool AddVisible
        {
            get { return addVisible; }
            set { SetProperty(ref addVisible, value); }
        }


        private bool cancelVisible;
        public bool CancelVisible
        {
            get { return cancelVisible; }
            set { SetProperty(ref cancelVisible, value); }
        }


        private bool reloadVisible;
        public bool ReloadVisible
        {
            get { return reloadVisible; }
            set { SetProperty(ref reloadVisible, value); }
        }


        private bool saveVisible;
        public bool SaveVisible
        {
            get { return saveVisible; }
            set { SetProperty(ref saveVisible, value); }
        }


        private bool deleteVisible;
        public bool DeleteVisible
        {
            get { return deleteVisible; }
            set { SetProperty(ref deleteVisible, value); }
        }


        private bool closeVisible;
        public bool CloseVisible
        {
            get { return closeVisible; }
            set { SetProperty(ref closeVisible, value); }
        }


        private bool stornoVisible;
        public bool StornoVisible
        {
            get { return stornoVisible; }
            set { SetProperty(ref stornoVisible, value); }
        }


        private bool reopenVisible;
        public bool ReopenVisible
        {
            get { return reopenVisible; }
            set { SetProperty(ref reopenVisible, value); }
        }


        private bool lENeuVisible;
        public bool LENeuVisible
        {
            get { return lENeuVisible; }
            set { SetProperty(ref lENeuVisible, value); }
        }


        private bool lEEditVisible;
        public bool LEEditVisible
        {
            get { return lEEditVisible; }
            set { SetProperty(ref lEEditVisible, value); }
        }


        private bool mTNeuVisible;
        public bool MTNeuVisible
        {
            get { return mTNeuVisible; }
            set { SetProperty(ref mTNeuVisible, value); }
        }


        private bool mTEditVisible;
        public bool MTEditVisible
        {
            get { return mTEditVisible; }
            set { SetProperty(ref mTEditVisible, value); }
        }


        private bool fbNummerVisible;
        public bool FbNummerVisible
        {
            get { return fbNummerVisible; }
            set { SetProperty(ref fbNummerVisible, value); }
        }


        private bool positionDetailVisible;
        public bool PositionDetailVisible
        {
            get { return positionDetailVisible; }
            set { SetProperty(ref positionDetailVisible, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
