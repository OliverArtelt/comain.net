﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class EntwPkzDto
    {

        public int          Jahr        { get; set; }
        public decimal?     Menge       { get; set; }
        public String       Einheit     { get; set; }
        public decimal?     Kosten      { get; set; }
        

        public decimal? Pkz 
        { 
            get {

                if (Menge.GetValueOrDefault() > 0) return Kosten / Menge;
                return null;    
            }
        }


        public String MengeUndEinheit
        { 
            get {

                if (Menge.HasValue) return String.Format("{0:N0} {1}", Menge, Einheit);
                return null;    
            }
        }


        public String PkzUndEinheit 
        { 
            get {

                if (Pkz.HasValue) return String.Format("{0:C} / {1}", Pkz, Einheit);
                return null;    
            }
        }
    }
}
