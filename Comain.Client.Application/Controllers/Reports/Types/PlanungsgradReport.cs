﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class PlanungsgradReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public PlanungsgradReport(IReportService store, JahrspanViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView)
        {

            this.store = store;            
            filterViews = new List<IFilterViewModel> { zeitView, kstView, pzView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var dto = await store.PGAsync(GebeFilter(), ct);

            var result = Create();
            result.AddDataSource("MainData", dto.Jahre);
            result.AddParameter("parPlanLA", dto.PlanLA);
            result.AddParameter("parUnplanLA", dto.UnplanLA);

            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override String Title
        {
            get { return "Planungsgrad"; }
        }


        public String Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 700; }
        }


        public override String ReportAddress
        {
            get { return "Comain.Client.Reports.PlanungsgradReport.rdlc"; }
        }
    }
}
