﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class VfViewItem : ValidatableViewItem<IHObjektBelegung>
    {

        public VfViewItem(IHObjektBelegung model, IHSelektorItem iho) : base(model)
        {
            
            Nummer = iho.Nummer;
            Name   = iho.Name;
           
            if (iho.Status.GetValueOrDefault() == 1) Status = "Sehr hoch";
            else if (iho.Status.GetValueOrDefault() == 2) Status = "Hoch";
            else if (iho.Status.GetValueOrDefault() == 3) Status = "Gering";
            else if (iho.Status.GetValueOrDefault() == 4) Status = "Keine";
            else Status = String.Empty;
        }
        
        
        public String Nummer    { get; private set; }                  
        public String Name      { get; private set; }                  
        public String Status    { get; private set; }                  
        
        
        private bool isSelected;                  
        public bool IsSelected                  
        {
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); }
        }


        public int? Belegungszeit                  
        {
            get { return Model == null? (int?)null: Model.Belegungszeit; }
            set { 
            
                if (Model != null && Model.Belegungszeit != value) {

                    Model.Belegungszeit = value;
                    RaisePropertyChanged("Belegungszeit");
                }
            }
        }


        public int? Ausfallzeit                  
        {
            get { return Model == null? (int?)null: Model.Ausfallzeit; }
            set { 
            
                if (Model != null && Model.Ausfallzeit != value) {

                    Model.Ausfallzeit = value;
                    RaisePropertyChanged("Ausfallzeit");
                }
            }
        }
    }
}
