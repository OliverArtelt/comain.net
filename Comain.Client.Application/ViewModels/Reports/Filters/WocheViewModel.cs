﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class WocheViewModel : ViewModel<IWocheView>, IFilterViewModel
    {

        public WocheViewModel(IWocheView view) : base(view)
        {
            Reset();
        }


        public void Reset()
        {
            woche = DateTime.Now.MondayOf();
        }


        public String Validiere()
        {

            if (!woche.HasValue) return "Geben Sie ein gültiges Datum an.";
            return null;
        }


        public string FilterAlsString()
        {

            if (woche.HasValue) return String.Format("{0}, {1}", woche.Value.MondayOf().ToShortDateString(), KwString);
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (woche.HasValue) filter.Add("Woche", woche);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {}


        public String KwString
        {

            get {

                if (!woche.HasValue) return null;
                return "KW " + woche.Value.KWString();
            }
        }


        public String Reportname
        {

            get {

                if (!woche.HasValue) return "WI Wartungsplan";
                int weekkey = woche.Value.WeekWithYear();
                return String.Format("WI Wartungsplan KW {0}-{1}", weekkey / 100, weekkey % 100);
            }
        }


#region P R O P E R T I E S


        private DateTime? woche;
        public DateTime? Woche
        {
            get { return woche; }
            set { if (SetProperty(ref woche, value)) RaisePropertyChanged("KwString"); }
        }


#endregion


    }
}
