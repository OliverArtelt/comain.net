﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Specs;
using Comain.Client.ViewModels;
using Comain.Client.Views.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class AssetSpecViewModel : ValidatableViewModel<IAssetSpecView, IHObjekt>, IDelayableViewModel
    {

        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public AssetSpecViewModel(IAssetSpecView view)
          : base(view)
        {
        }


        public override void CurrentChanged(IHObjekt oldSelected)
        {
            RaisePropertyChanged(nameof(TechnischeWerte));
        }


        public void Unload()
        {
            Current = null;
        }


#region P R O P E R T I E S


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool inEditMode;
        public bool InEditMode
        {
            get { return inEditMode; }
            set { SetProperty(ref inEditMode, value); }
        }


        public IEnumerable<TechnischerWert> TechnischeWerte
            => Current?.RemainingSpecs;


#endregion

    }
}
