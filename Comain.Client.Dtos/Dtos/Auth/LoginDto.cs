﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comain.Client.Dtos.Auth
{

    public class LoginDto
    {

        public String   Username    { get; set; }
        public String   Password    { get; set; }
    }
}
