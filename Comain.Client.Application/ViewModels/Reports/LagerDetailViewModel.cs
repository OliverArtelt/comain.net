﻿using Comain.Client.Services;
using Comain.Client.Views.Reports;


namespace Comain.Client.ViewModels.Reports
{
    
    public class LagerDetailViewModel : PrintViewModel<ILagerDetailView>, IDelayableViewModel
    {

        public LagerDetailViewModel(IMessageService messages, ILagerDetailView view) : base(messages, view)
        {}
    }
}
