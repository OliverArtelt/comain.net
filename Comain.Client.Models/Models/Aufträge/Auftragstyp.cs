﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{

    public enum Auftragstyp
    {

        Undefiniert = 0,
        Standardauftrag = 1,
        FremdBudget = 4,
        Generalauftrag = 5,
        Parameterauftrag = 6,
        Ticket = 7,
        BesonderesVorkommnis = 8,
        Wartungsplan = 9
    }


    public static class AuftragstypExtensions
    {

        public static String AsText(this Auftragstyp typ)
        {

            if (typ == Auftragstyp.Standardauftrag     ) return "Standardauftrag";
            if (typ == Auftragstyp.FremdBudget         ) return "Fremdbudget";
            if (typ == Auftragstyp.Generalauftrag      ) return "Generalauftrag";
            if (typ == Auftragstyp.Parameterauftrag    ) return "Parameterauftrag";
            if (typ == Auftragstyp.Ticket              ) return "Ticket";
            if (typ == Auftragstyp.BesonderesVorkommnis) return "Besonderes Vorkommnis";
            if (typ == Auftragstyp.Wartungsplan)         return "Wartungsauftrag";
            return null;
        }
    }
}
