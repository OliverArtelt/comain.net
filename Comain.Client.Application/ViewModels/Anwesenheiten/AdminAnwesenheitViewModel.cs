﻿using System;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Views.Anwesenheiten;

namespace Comain.Client.ViewModels.Stock.Administration
{

    public class AdminAnwesenheitViewModel : ValidatableViewModel<IAdminAnwesenheitView, Konfiguration>, IConfigViewModel
    {

        public AdminAnwesenheitViewModel(IAdminAnwesenheitView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged("Bestellfuß");
        }


        public void Unload()
        {}


#region P R O P E R T I E S

/*
        public TimeSpan? SchichtZeitVon
        {
            get { return Current == null? null: Current.StandardSchichtZeitVon; }
            set {

                if (Current != null && Current.Bestellfuß != value) {

                    Current.Bestellfuß = value;
                    RaisePropertyChanged("Bestellfuß");
                }
            }
        }


        public String Rechnungsanschrift
        {
            get { return Current == null? null: Current.Rechnungsanschrift; }
            set {

                if (Current != null && Current.Rechnungsanschrift != value) {

                    Current.Rechnungsanschrift = value;
                    RaisePropertyChanged("Rechnungsanschrift");
                }
            }
        } */


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
