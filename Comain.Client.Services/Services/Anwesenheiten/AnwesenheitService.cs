﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Gateways;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.Models.WPlan;


namespace Comain.Client.Services.Anwesenheiten
{

    public class AnwesenheitService : IAnwesenheitService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public AnwesenheitService(IRepository store, IMapper mapper)
        {

            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<Anwesenheit>> AnwesenheitenAsync(AssignedPSRequest args, CancellationToken ct = default)
        {

            var result = await store.PutAsync<AssignedPSRequest, AssignedPSResponse>("/RC/Read", args, ct);
            var models = mapper.Map<List<Anwesenheit>>(result.List);
            models.ForEach(p => p.AcceptChanges());
            return models;
        }


        public async Task<IList<MassnahmeListDto>> MassnahmenAsync(int ihid, CancellationToken ct = default)
        {

            var result = await store.GetAsync<List<MassnahmeListDto>>($"/Massn/List/{ihid}", ct);
            return result;
        }


        public async Task<IEnumerable<AnwesenheitReportDto>> ReportAsync(AssignedPSRequest args,  CancellationToken ct = default)
        {

            var filter = new Suchkriterien();
            filter.Add("Asset", args.Asset_Id);
            filter.Add("ZeitVon", args.UhrzeitVon);
            filter.Add("Datum", args.Datum);

            return await store.FindAsync<IEnumerable<AnwesenheitReportDto>>("/RC/Report", filter, ct);
        }


        public async Task<IEnumerable<AnwesenheitReportDto>> ReportAsync(AssignedCopyRequest args,  CancellationToken ct = default)
        {

            var filter = new Suchkriterien();
            filter.Add("Asset", args.Asset_Id);
            filter.Add("ZeitVon", args.Von);
            filter.Add("ZeitBis", args.Bis);
            filter.Add("Datum", args.Datum);

            return await store.FindAsync<IEnumerable<AnwesenheitReportDto>>("/RC/Report", filter, ct);
        }

        /// <remarks>
        /// Datum in args ist das Zieldatum, die Quelle ist der letztbenutzte Tag vor diesem Datum
        /// </remarks>
        public async Task<IList<Anwesenheit>> CopyFromAsync(AssignedPSRequest args, CancellationToken ct = default)
        {

            var result = await store.PutAsync<AssignedPSRequest, AssignedPSResponse>("/RC/CopyFrom", args, ct);
            var models = mapper.Map<List<Anwesenheit>>(result.List);
            models.ForEach(p => p.Datum = args.Datum);
            models.ForEach(p => p.AcceptChanges());
            return models;
        }


        public Task CopyToAsync(AssignedCopyRequest args, CancellationToken ct = default)
            => store.PutAsync<AssignedCopyRequest>("/RC/CopyTo", args, ct);


        public async Task SaveZeitplanAsync(AssignedPSResponse args, CancellationToken ct = default)
        {

            if (args == null || args.List.IsNullOrEmpty()) return;
            await store.PutAsync("/RC/Plan", args, ct);
        }


        public async Task SaveMassnahmenAsync(IEnumerable<Anwesenheit> list, CancellationToken ct = default)
        {

            if (list.IsNullOrEmpty()) return;
            var dtos = list.SelectMany(p => mapper.AsDtoList<AnwesenheitMassnahme, AnwesenheitMassnahmeDto>(p.Massnahmen)).ToList();
            await store.PutAsync("/RC/MN", dtos, ct);
            list.ForEach(p => p.AcceptChanges());
        }


        public Task<IList<PersonalFilterModel>> AssetPersonalAsync(int ihid, CancellationToken ct = default)
            => store.GetAsync<IList<PersonalFilterModel>>($"/Filter/AssetPS/{ihid}", ct);

        public Task<IList<IHListDto>> IHObjekteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<IHListDto>>("/Filter/IH", ct);

        public Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);

        public Task<IList<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct);

        public Task<IList<LeistungsartFilterModel>> LeistungsartenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<LeistungsartFilterModel>>("/Filter/LA", ct);

        public Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct);

        public Task<IList<String>> TätigkeitenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<String>>("/RC/Taet", ct);
    }
}
