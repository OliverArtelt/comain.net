﻿

namespace Comain.Factories
{
    
    /// <summary>
    /// darüber kann DIC transiente Objekte injiziieren, Instanziierung dieser Schnittstelle übernimmt DIC
    /// </summary>
    public interface IFactory<T> where T : class
    {

        T Create();
        void Release(T val);
    }
    

    public interface IContainerFactory<T> where T : class
    {
        Container<T> Create();
    }


    public class ContainerFactory<T> : IContainerFactory<T> where T : class
    {
        
        private readonly IFactory<T> impl;
                                         

        public ContainerFactory(IFactory<T> impl)
        {
            this.impl = impl;
        }    


        public Container<T> Create()
        {
            return new Container<T>(impl);
        }
    }
}
