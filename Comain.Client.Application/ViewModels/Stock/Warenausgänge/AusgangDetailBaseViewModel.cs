﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using Comain.Client.Models;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.Views.Stock.Warenausgänge;
using Comain.Client.ViewModels;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos;
using Comain.Client.ViewModels.Verwaltung;

namespace Comain.Client.ViewModels.Stock.Warenausgänge
{
    
    public class AusgangDetailBaseViewModel : ValidatableViewModel<IAusgangDetailBaseView, Warenausgang>
    {
 
        private readonly SynchronizationContext context;


        public AusgangDetailBaseViewModel(IAusgangDetailBaseView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }

        
        public void Unload()
        {

            Aufträge = null;
            Kostenstellen = null;
            PsSearchProvider.ClearItems(); 
        }  


        public override void CurrentChanged(Warenausgang oldSelected)
        {
           
            RaisePropertyChanged("Artikel");
            RaisePropertyChanged("Lagerort");
            RaisePropertyChanged("Lagerdatum");
            RaisePropertyChanged("Lagermenge");
            RaisePropertyChanged("Lagerrestmenge");
            RaisePropertyChanged("Lagerpreis");
            RaisePropertyChanged("Datum");
            RaisePropertyChanged("Menge");
            RaisePropertyChanged("Auftrag");
            RaisePropertyChanged("Kostenstelle");
            RaisePropertyChanged("Nummer");
            RaisePropertyChanged("Entnahmeschein");
            RaisePropertyChanged("Nutzer");
            RaisePropertyChanged("Lagerist");
            RaisePropertyChanged("Notizen");
        }


        public void SetzeListen(IEnumerable<KstFilterModel> kslist, IEnumerable<PersonalFilterModel> pslist, IEnumerable<AuSelektorDto> aulist)
        {

            PsSearchProvider.SetItems(pslist?.OrderBy(p => p.Name));
            Aufträge = aulist;
            Kostenstellen = kslist;
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        public String Artikel
        { 
            get { return Current == null || Current.Material == null? null: String.Format("[{0}] {1}", Current.Material.Nummer, Current.Material.Name); }
        }
                 

        public String Lagerort
        { 
            get { return Current == null || Current.Lagerort == null? null: Current.Lagerort.Nummer; }
        }
                 

        public DateTime? Lagerdatum
        { 
            get { return Current == null || Current.Wareneinlagerung == null? (DateTime?)null: Current.Wareneinlagerung.Datum; }
        }
                 

        public decimal? Lagermenge
        { 
            get { return Current == null || Current.Wareneinlagerung == null? (decimal?)null: Current.Wareneinlagerung.Menge; }
        }
                 

        public decimal? Lagerrestmenge
        { 
            get { return Current == null || Current.Wareneinlagerung == null? (decimal?)null: Current.Wareneinlagerung.Restmenge; }
        }
                 

        public decimal? Lagerpreis
        { 
            get { return Current == null || Current.Wareneinlagerung == null? (decimal?)null: Current.Wareneinlagerung.Preis; }
        }
                 

        public DateTime? Datum
        { 
            get { return Current == null? (DateTime?)null: Current.Datum; }
            set { 
            
                if (current != null && current.Datum != value && value.HasValue) {

                    current.Datum = value.Value;
                    RaisePropertyChanged("Datum");
                }
            }
        }
                 

        public decimal? Menge
        { 
            get { return Current == null? (decimal?)null: Current.Menge; }
            set { 
            
                if (current != null && current.Menge != value && value.HasValue) {

                    current.Menge = value.Value;
                    RaisePropertyChanged("Menge");
                }
            }
        }
                 

        public AuSelektorDto Auftrag
        { 
            get { return Current == null? null: Current.Auftrag; }
            set { 
            
                if (current != null && value != null && (current.Auftrag == null || current.Auftrag.Id != value.Id)) {

                    current.Auftrag = value;
                    RaisePropertyChanged("Auftrag");
                }
            }
        }
                 

        public KstFilterModel Kostenstelle
        { 
            get { return Current == null? null: Current.Kostenstelle; }
            set { 
            
                if (current != null && value != null && (current.Kostenstelle == null || current.Kostenstelle.Id != value.Id)) {

                    current.Kostenstelle = value;
                    RaisePropertyChanged("Kostenstelle");
                }
            }
        }


        public String Nummer
        { 
            get { return Current == null? null: Current.Nummer; }
        }
                 

        public String Entnahmeschein
        { 
            get { return Current == null? null: Current.Entnahmeschein; }
        }

 
        public PersonalFilterModel Nutzer
        { 
            get { return Current == null? null: Current.Nutzer; }
            set { 
            
                if (current != null && !EntityModelBase.Equals(current.Nutzer, value)) {

                    current.Nutzer = value;
                    RaisePropertyChanged("Nutzer");
                }
            }
        }


        public PersonalFilterModel Lagerist
        { 
            get { return Current == null? null: Current.Lagerist; }
            set { 
            
                if (current != null && !EntityModelBase.Equals(current.Lagerist, value)) {

                    current.Lagerist = value;
                    RaisePropertyChanged("Lagerist");
                }
            }
        }


        public String Notizen
        { 
            get { return Current == null? null: Current.Notizen; }
            set { 
            
                if (current != null && current.Notizen != value) {

                    current.Notizen = value;
                    RaisePropertyChanged("Notizen");
                }
            }
        }
                 

        private IEnumerable aufträge;
        public IEnumerable Aufträge
        {
            get { return aufträge; }
            set { SetProperty(ref aufträge, value); }
        }


        private IEnumerable<KstFilterModel> kostenstellen;
        public IEnumerable<KstFilterModel> Kostenstellen
        {
            get { return kostenstellen; }
            set { SetProperty(ref kostenstellen, value); }
        }


#endregion

    }
}
