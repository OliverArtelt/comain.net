﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ReportModels;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public class LeererAUReport : ReportBase, IReport
    {

        public LeererAUReport()
        {
        }


        public Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", new List<AuftragRow> { new AuftragRow(null, null, false) });
            result.HasLogo = true;
            return Task.FromResult<ReportResult>(result);
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Leerer Auftrag"; }
        }


        public string Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 2000; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.AuftragLeerReport.rdlc"; }
        }
    }
}
