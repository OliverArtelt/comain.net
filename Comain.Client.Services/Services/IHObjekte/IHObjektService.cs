﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Dtos.Specs;
using Comain.Client.Dtos.Stock;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Specs;
using Comain.Client.Models.Stock;
using Comain.Client.ReportModels;
using Comain.Client.Trackers;
using Comain.Client.Dtos;
using Comain.Client.Models.WPlan;

namespace Comain.Client.Services.IHObjekte
{

    public class IHObjektService : IIHObjektService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public IHObjektService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IHObjekt> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/IH/Detail/{0}", id);
            var dto = await store.GetAsync<IHDetailDto>(url, ct).ConfigureAwait(false);
            if (dto == null) return null;

            var model = mapper.Map<IHDetailDto, IHObjekt>(dto);
            model.AcceptChanges();
            return model;
        }


        public async Task<IHObjekt> SaveAsync(IHObjekt model, CancellationToken ct = default)
        {

            var url = "/IH/Detail";
            var dto = mapper.Map<IHObjekt, IHDetailDto>(model);
            dto.TechnischeWerte = mapper.Map<List<TechWertDto>>(model.TechnischeWerte.Where(p => p.IsChanged).ToList());
            dto.MaterialIHObjektMap = mapper.AsDtoList<ArtikelSelectModel, AssetArtikelDto>(model.MaterialIHObjektMap);
            dto.ChangeStatus = ChangeStatus.Changed;

            var result = await store.PostAsync<IHDetailDto, IHDetailDto>(url, dto, ct).ConfigureAwait(false);
            var newmodel = mapper.Map<IHDetailDto, IHObjekt>(result);

            newmodel.AcceptChanges();
            return newmodel;
        }


        public async Task<IHDeleteDto> DeleteKonsequenzenAsync(IHObjekt model, CancellationToken ct = default)
        {

            if (model == null) throw new NullReferenceException("Kein Asset gewählt.");
            var url = String.Format("/IH/Delete/{0}", model.Id);
            var result = await store.GetAsync<IHDeleteDto>(url, ct).ConfigureAwait(false);
            return result;
        }


        public async Task DeleteAsync(IHObjekt model, CancellationToken ct = default)
        {

            if (model == null) throw new NullReferenceException("Kein Asset gewählt.");
            var url = String.Format("/IH/Delete/{0}", model.Id);
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }


        public async Task<IEnumerable<IHEreignisReportModel>> EreignisseAsync(int id, bool withSub, CancellationToken ct = default)
        {

            var url = $"/IH/History/{id}/{withSub}";
            var dtos = await store.GetAsync<List<IHEreignisDto>>(url, ct).ConfigureAwait(false);
            return CreateEreignislist(dtos);
        }


        private IEnumerable<IHEreignisReportModel> CreateEreignislist(List<IHEreignisDto> dtos)
        {

            int cnt = 0;

            foreach (var dto in dtos) {

                cnt++;

                if (dto.Details.IsNullOrEmpty()) yield return new IHEreignisReportModel(dto, cnt);
                else foreach(var kvp in dto.Details) {

                    yield return new IHEreignisReportModel(dto, cnt, kvp.Key, kvp.Value);
                }
            }
        }


        public async Task<IList<IHListDto>> IHObjekteAsync(CancellationToken ct = default)
            => await store.GetAsync<IList<IHListDto>>("/IH/Search", ct).ConfigureAwait(false);


        public Task<IList<PzFilterModel>> StandorteAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PzFilterModel>>("/Filter/PZ", ct);


        public Task<IList<KstFilterModel>> KostenstellenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<KstFilterModel>>("/Filter/KST", ct);


        public Task<IList<NummerFilterModel>> ObjektgruppenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/OGr", ct);


        public Task<IList<SubNummerFilterModel>> ObjektartenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<SubNummerFilterModel>>("/Filter/OArt", ct);


        public Task<IList<LieferantFilterModel>> LieferantenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<LieferantFilterModel>>("/Filter/LF", ct);


        public Task<IList<PersonalFilterModel>> PersonalAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);


        public Task<IHMoveInfo> MoveInfoAsync(IHMoveRequestDto dto, CancellationToken ct = default)
            => store.PutAsync<IHMoveRequestDto, IHMoveInfo>("/IH/MoveInfo", dto, ct);


        public Task MoveAsync(IHMoveInstruction instruction, CancellationToken ct = default)
            => store.PostAsync<IHMoveInstruction>("/IH/Move", instruction, ct);


        public Task<IList<SpecFilterModel>> SpecsAsync(CancellationToken ct = default)
            => store.GetAsync<IList<SpecFilterModel>>("/Spc/Filter/Assets", ct);


        public Task<List<MaterialstammFilterModel>> MaterialstammAsync(CancellationToken ct = default)
            => store.GetAsync<List<MaterialstammFilterModel>>("/Filter/MT", ct);


        public async Task SelfServiceAsync(IEnumerable<int> mnidlist, CancellationToken ct = default)
        {

            if (mnidlist.IsNullOrEmpty()) return;
            await store.PostAsync<List<int>>("/Self", mnidlist.ToList(), ct).ConfigureAwait(false);
        }
    }
}
