﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class PsDto
    {

        public DateTime     Datum           { get; set; }
        public String       Auftrag         { get; set; }
        public String       Zeit            { get; set; }
        public String       Aufschläge      { get; set; }
        public String       Personalnummer  { get; set; }
        public decimal      Stunden         { get; set; }
    }
}
