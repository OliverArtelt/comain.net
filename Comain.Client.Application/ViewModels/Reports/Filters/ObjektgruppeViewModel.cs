﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class ObjektgruppeViewModel : ViewModel<IObjektgruppeView>, IFilterViewModel
    {

        private readonly IFilterService store;
        private SynchronizationContext context;


        public ObjektgruppeViewModel(IFilterService store, IObjektgruppeView view) : base(view)
        {
            
            this.store = store;
            context = SynchronizationContext.Current;
        }


        public void Reset()
        {
            
            Objektgruppe = null;
            Objektart = null;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            var bld = new StringBuilder();
            if (Objektgruppe != null) bld.Append("Objektgruppe ").Append(Objektgruppe.Name).Append(", ");
            if (Objektart != null) bld.Append("Objektart ").Append(Objektart.Name).Append(", ");
            if (bld.Length > 1) bld.Length -= 2;
            return bld.ToString(); 
        }


        public void AddFilter(Suchkriterien filter)
        {
            
            if (Objektgruppe != null) filter.Add("OGr", Objektgruppe.Id);
            if (Objektart != null) filter.Add("OArt", Objektart.Id);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            
            return context.SendAsync(new SendOrPostCallback((o) => {
                
                objektarten = new CollectionViewSource { Source = store.Objektarten }.View;
                objektarten.Filter = FilterPredicate;
                objektarten.SortDescriptions.Clear();
                objektarten.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                Refresh();

                RaisePropertyChanged("Objektgruppen");
                RaisePropertyChanged("Objektarten");
                RaisePropertyChanged("ObjektartVisible");
            
            }), null);
        }


        private bool FilterPredicate(object arg)
        {

            var oart = arg as SubNummerFilterModel;
            if (oart == null) return false;
            if (objektgruppe == null) return false;

            return oart.Haupt_Id == objektgruppe.Id;
        }

       
        private void Refresh()
        {
            
            if (objektarten == null) return;
            context.Post(new SendOrPostCallback((o) => { objektarten?.Refresh(); }), null);
            RaisePropertyChanged("Objektarten");
        }


        public void Unload()
        {
            objektarten = null;
        }


#region P R O P E R T I E S
     

        public IList<NummerFilterModel> Objektgruppen
        { 
            get { return store == null? null: store.Objektgruppen; }
        } 
   
        
        private ICollectionView objektarten;
        public ICollectionView Objektarten
        { 
            get { return objektarten; }
        } 
   

        private NummerFilterModel objektgruppe;
        public NummerFilterModel Objektgruppe
        { 
            get { return objektgruppe; }
            set { 
            
                if (SetProperty(ref objektgruppe, value)) {
                
                    RaisePropertyChanged("ObjektartVisible"); 
                    Refresh();
                }
            } 
        } 
   

        private SubNummerFilterModel objektart;
        public SubNummerFilterModel Objektart
        { 
            get { return objektart; }
            set { SetProperty(ref objektart, value); }
        } 


        public bool ObjektartVisible
        {
            get { return Objektgruppe != null; }
        }


#endregion

    }
}
