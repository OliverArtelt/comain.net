﻿using System;


namespace Comain.Client.Dtos.Filters
{

    public class LieferantFilterModel : EntityModelBase, IComparable<LieferantFilterModel>
    {
        
        public String     Name1                   { get; set; }
        public String     Name2                   { get; set; }
        public String     Ort                     { get; set; }
        public bool       LieferantFürIHObjekte   { get; set; }
        public bool       LieferantFürAufträge    { get; set; }

        
        public int CompareTo(LieferantFilterModel other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            var b1 = (this.Name1 == null && other.Name1 == null);
            if (!b1 && this.Name1 == null) return -1;

            int testName1 = 0;
            if (!b1) testName1 = this.Name1.CompareNatural(other.Name1);
            if (!b1 && testName1 != 0) return testName1;
            
            if (this.Name2 == null && other.Name2 == null) return 0;
            if (this.Name2 == null) return -1;

            return this.Name2.CompareNatural(other.Name2);
        }
    }
}
