﻿using System;
using System.Windows;

namespace Comain.Client
{

    public class Settings : ISettings
    {

        public String       Accent                      { get; set; }
        public String       Theme                       { get; set; }
        public bool         ConfirmLogout               { get; set; }

        public String       ProductName                 => "Comain";
        public String       ApplicationVersion          => "6.0.0";

        public String       LayoutAuftragList           { get; set; }


        public Settings()
        {

         //  Accent      = Properties.Settings.Default.Accent;
         //  Theme       = Properties.Settings.Default.Theme;
         //  ProductName = Properties.Settings.Default.ProductName;
         //  var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
         //  ApplicationVersion = String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
        }


        public void Save()
        {

         //  Properties.Settings.Default.Accent = Accent;
         //  Properties.Settings.Default.Theme = Theme;
         //  Properties.Settings.Default.Save();
        }
    }
}
