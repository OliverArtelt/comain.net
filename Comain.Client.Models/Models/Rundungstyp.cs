﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models
{

    public enum Rundungstyp
    {

        Abschneiden = 0,
        Kaufmännisch = 1,
        Mathematisch = 2,
        Aufrunden = 3
    }


    public static class RoundingExtensions
    {

        public static decimal? RoundTwoDecimals(decimal? val, Rundungstyp rtyp)
        {
            
            if (!val.HasValue) return null;
            return val.Value.RoundTwoDecimals(rtyp);
        }


        public static decimal RoundTwoDecimals(this decimal val, Rundungstyp rtyp)
        {

            switch (rtyp) {
        
                case Rundungstyp.Abschneiden  : return Math.Truncate(val * 100m) / 100m;      
                case Rundungstyp.Kaufmännisch : return Math.Round(val, 2, MidpointRounding.AwayFromZero);    
                case Rundungstyp.Mathematisch : return Math.Round(val, 2, MidpointRounding.ToEven);     
                case Rundungstyp.Aufrunden    : return Math.Round(val + 0.005m, 2);      
            }

            return val;
        }
    }
}
