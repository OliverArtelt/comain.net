﻿using System;


namespace Comain.Client.Views
{

    public class TabletModeChangedEventArgs : EventArgs
    {
        
        public bool InTabletMode { get; }


        public TabletModeChangedEventArgs(bool inTabletMode)
        {
            InTabletMode = inTabletMode;
        }
    }    
}
