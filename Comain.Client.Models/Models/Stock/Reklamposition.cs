﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;


namespace Comain.Client.Models.Stock
{
    
    public class Reklamposition : Entity, IDataErrorInfo
    {
       
        private int          position; 
        private decimal      menge;    
        private decimal?     preis;    
        private String       notizen;      

        private String       artikelnummer;          
        private String       lfartikelnummer;          
        private String       artikelbezeichnung;     
        private String       bestellposition;        
        
        private int          artikelId;              
        private int          eingangId;              


#region P R O P E R T I E S

        
        public int Position     
        {
            get { return position; }
            set { SetProperty(ref position, value); }
        }
        

        public decimal Menge        
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }
        

        public decimal? Preis        
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }
        

        public String Notizen          
        {
            get { return notizen; }
            set { SetProperty(ref notizen, value); }
        }


        public String Artikelnummer          
        {
            get { return artikelnummer; }
            set { SetProperty(ref artikelnummer, value); }
        }

        /// <summary>
        /// Artikelnummer /Bestellnummer des Lieferanten
        /// </summary>
        public String LfArtikelnummer          
        {
            get { return lfartikelnummer; }
            set { SetProperty(ref lfartikelnummer, value); }
        }
        

        public String Artikelbezeichnung     
        {
            get { return artikelbezeichnung; }
            set { SetProperty(ref artikelbezeichnung, value); }
        }
        

        public String Bestellposition        
        {
            get { return bestellposition; }
            set { SetProperty(ref bestellposition, value); }
        }
        

        public int ArtikelId             
        {
            get { return artikelId; }
            set { SetProperty(ref artikelId, value); }
        }
        

        public int EingangId             
        {
            get { return eingangId; }
            set { SetProperty(ref eingangId, value); }
        }
        

#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 

                var test = this["Menge"];
                return test;
            }
        }

       
        public List<String> ErrorList
        {
            
            get { 

                var result = new List<String>();
            
                String test = this["Menge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Menge":

                    if (Menge <= 0) return "Die reklamierte Menge muss angegeben werden."; 
                    break;
                }

                return null;
            }
        }

#endregion    
        
    }
}
