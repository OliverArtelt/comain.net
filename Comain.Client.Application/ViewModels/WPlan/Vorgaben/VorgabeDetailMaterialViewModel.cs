﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan;
using Comain.Client.Views.WPlan.Vorgaben;
using System.Threading.Tasks;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeDetailMaterialViewModel : ValidatableViewModel<IVorgabeDetailMaterialView, WiMaterialVorgabe>
    {

        private readonly SynchronizationContext context;


        public VorgabeDetailMaterialViewModel(IVorgabeDetailMaterialView view)
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public Task SetzeListenAsync(List<NummerFilterModel> einheiten, List<MaterialstammFilterModel> materialliste)
        {

            return context.SendAsync(new SendOrPostCallback((o) => {

                Einheiten = einheiten;
                Materialliste = materialliste;

            }), null);
        }


        public override void CurrentChanged(WiMaterialVorgabe oldSelected)
        {

            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Material));
            RaisePropertyChanged(nameof(Einheit));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Anweisung));
            RaisePropertyChanged(nameof(EPreis));
            RaisePropertyChanged(nameof(Erforderlich));
        }


        public void Unload()
        {

            Einheiten = null;
            Materialliste = null;
        }


#region P R O P E R T I E S


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private TrackableCollection<WiMaterialVorgabe> materialVorgabe;
        public TrackableCollection<WiMaterialVorgabe> MaterialVorgaben
        {
            get { return materialVorgabe; }
            set { SetProperty(ref materialVorgabe, value); }
        }


        public decimal? Menge
        {
            get { return Current?.Menge; }
            set {

                if (Current != null && Current.Menge != value) {

                    Current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public decimal? EPreis
        {
            get { return Current?.EPreis; }
            set {

                if (Current != null && Current.EPreis != value) {

                    Current.EPreis = value;
                    RaisePropertyChanged(nameof(EPreis));
                }
            }
        }


        public bool? Erforderlich
        {
            get { return Current?.Erforderlich; }
            set {

                if (Current != null && value.HasValue && Current.Erforderlich != value.Value) {

                    Current.Erforderlich = value.Value;
                    RaisePropertyChanged(nameof(Erforderlich));
                }
            }
        }


        public String Name
        {
            get { return Current?.Name; }
            set {

                if (Current != null && Current.Name != value) {

                    Current.Name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }


        public String Nummer
        {
            get { return Current?.Nummer; }
            set {

                if (Current != null && Current.Nummer != value) {

                    Current.Nummer = value;
                    RaisePropertyChanged(nameof(Nummer));
                }
            }
        }


        public String Anweisung
        {
            get { return Current?.Anweisung; }
            set {

                if (Current != null && Current.Anweisung != value) {

                    Current.Anweisung = value;
                    RaisePropertyChanged(nameof(Anweisung));
                }
            }
        }


        public NummerFilterModel Einheit
        {
            get { return Current?.Einheit; }
            set {

                if (Current != null && Current.Einheit != value) {

                    Current.Einheit = value;
                    RaisePropertyChanged(nameof(Einheit));
                }
            }
        }


        public MaterialstammFilterModel Material
        {
            get { return Current?.Material; }
            set {

                if (Current != null && Current.Material != value) {

                    Current.Material = value;
                    if (value?.Einheit != null && !Einheiten.IsNullOrEmpty()) Einheit = Einheiten.FirstOrDefault(p => p.Id == value.Einheit.Id);
                    RaisePropertyChanged(nameof(Material));
                }
            }
        }


        private List<MaterialstammFilterModel> materialliste;
        public List<MaterialstammFilterModel> Materialliste
        {
            get { return materialliste; }
            set { SetProperty(ref materialliste, value); }
        }


        private List<NummerFilterModel> einheiten;
        public List<NummerFilterModel> Einheiten
        {
            get { return einheiten; }
            set { SetProperty(ref einheiten, value); }
        }


#endregion

    }
}
