﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain;
using Comain.Client.Models;
using Comain.Client.Models.WPlan;


namespace Comain.Client.Models.WPlan
{

    public class Wartungsergebnis 
    {

        public DateTime?   DatumLeistung            { get; set; }
        public TimeSpan?   UhrzeitVon               { get; set; }
        public TimeSpan?   UhrzeitBis               { get; set; }
        public DateTime?   Folgetermin              { get; set; }
        public int?        Instandhalter            { get; set; }
        public int?        NeuerInstandhalter       { get; set; }
        public bool        MinutenWerdenGeteilt     { get; set; }

        public IList<Checklistposition>    Erledigte   { get; set; }
    }
}
