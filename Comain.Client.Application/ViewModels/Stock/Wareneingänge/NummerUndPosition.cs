﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{

    public class NummerUndPosition : IComparable<NummerUndPosition> , IComparable
    {

        public String Nummer   { get; set; }
        public int?   Position { get; set; }
            

        public override string ToString()
        {
                
            if (!Position.HasValue) return Nummer; 
            return String.Format("{0} - {1}", Nummer, Position); 
        }


        public int CompareTo(NummerUndPosition other)
        {
                
            if (other == null) return -1;
            int cmp = this.Nummer.CompareTo(other.Nummer);
            if (cmp != 0) return cmp;
            if ( this.Position.HasValue && !other.Position.HasValue) return -1;
            if (!this.Position.HasValue &&  other.Position.HasValue) return +1;
            if (!this.Position.HasValue && !other.Position.HasValue) return  0;
            if (this.Position < other.Position) return -1;
            if (this.Position == other.Position) return 0;
            return 1;
        }

            
        public int CompareTo(object obj)
        {
            return this.CompareTo(obj as NummerUndPosition);      
        }
    }
}
