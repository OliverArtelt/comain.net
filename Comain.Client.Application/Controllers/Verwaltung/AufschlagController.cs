﻿using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IAufschlagController : IVwTypController 
    {}
    
    
    public class AufschlagController : SimpleController<AufschlagtypDto, Aufschlagtyp, IAufschlagView>, IAufschlagController
    {
        
        public AufschlagController(IMessageService messages, IControllerBus bus, ISimpleService<AufschlagtypDto, Aufschlagtyp> store, 
                                   VerwaltungViewModel mainView, AufschlagViewModel view)
          : base(messages, store, bus, mainView, view)
        {}

        
        protected override string Url
        {
            get { return "/VW/AS"; }
        }
    }
}
