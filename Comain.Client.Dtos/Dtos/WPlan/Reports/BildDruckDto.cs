﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Reports
{

    public class BildDruckDto
    {

        public int      Id              { get; set; }
        public String   Dateiname       { get; set; }
        public String   Storename       { get; set; }
        public byte[]   Image           { get; set; }
    }
}
