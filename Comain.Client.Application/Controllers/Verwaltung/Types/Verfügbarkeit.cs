﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Verfügbarkeit : IVwTyp
    {
       
        public string       Title         => "Verfügbarkeit";
        public int          Sortierung    => 3000;
        public Type         Controller    => typeof(IVerfügbarkeitController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            return false;
            //if (nutzer == null) return false;
            //return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;    
        }
    }
}
