﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ReportModels.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Summen einer Periode
    /// </summary>
    public class Planspaltensumme : NotifyBase, IAcceptor
    {

        private Planspalte period;


        public Planspaltensumme(Planspalte period)
        {
            this.period = period;
        }


        public List<PZSpaltensumme> Zellen  { get; } = new List<PZSpaltensumme>();
        public int                  ColumnIndex   => period.Index;


        public decimal? Stunden  => Zellen?.Sum(p => p.Stunden.GetValueOrDefault());
        public decimal? Kosten   => Zellen?.Sum(p => p.Kosten.GetValueOrDefault());


        public decimal ExportierteGeplanteStunden(PlanOptions opts)
            => Zellen.Sum(p => p.ExportierteGeplanteStunden(opts));

        public decimal ExportierteGeplanteKosten(PlanOptions opts)
            => Zellen.Sum(p => p.ExportierteGeplanteKosten(opts));


        public String Wert 
        {
            get {

                if (Anzeigemodus == Plananzeigemodus.Zeiten && Stunden.HasValue) return String.Format("{0:N1} h", Stunden.Value);
                if (Anzeigemodus == Plananzeigemodus.Kosten && Kosten.HasValue) return Kosten.Value.ToString("c");
                return null;
            }
        }


        private Plananzeigemodus anzeigemodus;
        public Plananzeigemodus Anzeigemodus
        {
            get { return anzeigemodus; }
            set { 
            
                if (SetProperty(ref anzeigemodus, value)) {

                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public void Accept(IVisitor v)
        {
            v.Visit(this);
        }
    }
}
