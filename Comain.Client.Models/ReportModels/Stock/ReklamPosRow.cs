﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.Stock;


namespace Comain.Client.ReportModels.Stock
{
    
    public class ReklamPosRow
    {
        
        private readonly Reklamposition model;


        public ReklamPosRow(Reklamposition model)
        {
            this.model = model;
        }


        public String   Bestellposition     { get { return model.Bestellposition; } }
        public String   LfArtikelnummer     { get { return model.LfArtikelnummer; } }
        public String   Artikelnummer       { get { return model.Artikelnummer; } }
        public String   Artikelname         { get { return model.Artikelbezeichnung; } }
        public decimal  Menge               { get { return model.Menge; } }
        public decimal? Preis               { get { return model.Preis; } }
        public String   Details             { get { return model.Notizen; } }
    }
}
