﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public abstract class ReportBase
    {

        protected IList<IFilterViewModel> filterViews;

        public abstract String Title            { get; }
        public abstract String ReportAddress    { get; }
        public virtual  String ReportAssembly   { get { return "comain.Client.Reports"; } }


        public virtual IList<IFilterViewModel> FilterViews => filterViews;


        public virtual IEnumerable<String> Validiere()
        {

            if (filterViews == null) return null;
            return filterViews.Select(p => p.Validiere()).Where(p => !String.IsNullOrEmpty(p)).ToList();
        }


        public virtual Task DisplayAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        protected virtual ReportResult Create()
        {

            var result = new ReportResult { Title = Title, ReportAddress = ReportAddress, AssemblyAddress = ReportAssembly };
            result.AddParameter("parTitle", Title);
            if (filterViews != null)
                result.AddParameter("parFilter", String.Join("; ", filterViews.Select(p => p.FilterAlsString()).Where(p => !String.IsNullOrWhiteSpace(p)).ToArray()));

            return result;
        }


        protected virtual Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            if (filterViews != null) filterViews.ForEach(p => p.AddFilter(filter));
            return filter;
        }


        public virtual Task<ReportResult> CreateDetailAsync(String report, List<Tuple<String, String>> parameters, CancellationToken ct = default)
            => null;
    }
}
