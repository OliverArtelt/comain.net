﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewData.Verwaltung
{

    public class DringlichkeitenProvider : IDringlichkeitenProvider
    {

        private readonly Dictionary<int, Dringlichkeit> dict;
        private readonly ReadOnlyCollection<Dringlichkeit> aufträge;
        private readonly ReadOnlyCollection<Dringlichkeit> tickets;
        private readonly ReadOnlyCollection<Dringlichkeit> bevor;


        public DringlichkeitenProvider(IEnumerable<Dringlichkeit> allModels)
        {

            allModels.ForEach(p => p.AcceptChanges());

            aufträge = new ReadOnlyCollection<Dringlichkeit>(allModels.Where(p => p.WirdVerwendet && p.Auftragstyp == Auftragstyp.Standardauftrag).OrderBy(p => p.Position).ToList());
            tickets  = new ReadOnlyCollection<Dringlichkeit>(allModels.Where(p => p.WirdVerwendet && p.Auftragstyp == Auftragstyp.Ticket).OrderBy(p => p.Position).ToList());
            bevor    = new ReadOnlyCollection<Dringlichkeit>(allModels.Where(p => p.WirdVerwendet && p.Auftragstyp == Auftragstyp.BesonderesVorkommnis).OrderBy(p => p.Position).ToList());
            dict     = allModels.ToDictionary(p => p.Id);
        }


        public IList<Dringlichkeit> Liste(Auftragstyp typ)
        {

            if (typ == Auftragstyp.BesonderesVorkommnis) return bevor;
            if (typ == Auftragstyp.Ticket) return tickets;
            return aufträge;
        }


        public Dringlichkeit this[int? id]
        {

            get {

                if (!id.HasValue) return null;
                return dict.GetValueOrDefault(id.Value);
            }
        }
    }
}
