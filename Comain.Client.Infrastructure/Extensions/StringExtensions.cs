﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;

namespace Comain
{

    public static class StringExtensions
    {

        public static bool Contains(this String source, String toCheck, StringComparison comp)
        {

            if (source == null) return false;
            return source.IndexOf(toCheck, comp) >= 0;
        }


        public static String AsFilename(this String docname)
        {

            if (String.IsNullOrEmpty(docname)) return docname;
            var exclist = Path.GetInvalidFileNameChars()
                              .Union(
                          Path.GetInvalidPathChars()).ToList();

            String res = new String(docname.Select(p => exclist.Contains(p)? '_': p).ToArray());
            return res;
        }


        public static String JsonEscaping(this String text)
        {

            if (String.IsNullOrEmpty(text)) return text;
            return text.Replace(@"""", @"\""");
        }


        public static String CutWordAfter(this String text, int maxlen)
        {

            if (text.Length <= maxlen) return text;
            if (maxlen == 0) return String.Empty;
            if (maxlen == 1) return text.Substring(0, 1);
            String rawText = text.Substring(0, maxlen);

            if (Char.IsLetterOrDigit(text[maxlen])) {

                int lastSpace = rawText.LastIndexOf(' ');
                if (lastSpace < 0) return text.CutAfter(maxlen);
                rawText = rawText.Substring(0, lastSpace);
            }

            return rawText + " ...";
        }


        public static String CutAfter(this String text, int maxlen)
        {

            if (text.Length <= maxlen) return text;
            return text.Substring(0, maxlen);
        }


        public static String RemoveNewlines(this String text)
        {
            return text.RemoveNewlines(" ");
        }


        public static String RemoveNewlines(this String text, String replacement)
        {

            var parts = text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            return String.Join(replacement, parts);
        }


        [SuppressUnmanagedCodeSecurity]
        internal static class SafeNativeMethods
        {
            [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
            public static extern int StrCmpLogicalW(string psz1, string psz2);
        }


        public static int CompareNatural(this string a, string b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a, b);
        }


        public class StringNaturalComparer : IComparer<String>
        {

            public int Compare(String x, String y)
            {
                return SafeNativeMethods.StrCmpLogicalW(x, y);
            }
        }
    }
}
