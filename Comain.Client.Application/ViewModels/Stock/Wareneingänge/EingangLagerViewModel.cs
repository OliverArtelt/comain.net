﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneingänge;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{
    
    public class EingangLagerViewModel : ViewModel<IEingangLagerView>
    {
 
        public EingangLagerViewModel(IEingangLagerView view) : base(view)
        {
            Datum = DateTime.Today;             
        }


        public void Unload()
        {
            Lagerorte = null;
        }


#region P R O P E R T I E S
 

        private String letzterLagerort;
        public String LetzterLagerort
        { 
            get { return letzterLagerort; }
            set { SetProperty(ref letzterLagerort, value); }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }
            set { SetProperty(ref takeCommand, value); }
        }
  

        private IEnumerable<Lagerort> lagerorte;
        public IEnumerable<Lagerort> Lagerorte
        {
            get { return lagerorte; }
            set { SetProperty(ref lagerorte, value); }
        }
 

        private Lagerort lagerort;
        public Lagerort Lagerort
        {
            get { return lagerort; }
            set { SetProperty(ref lagerort, value); }
        }


        private DateTime datum;
        public DateTime Datum
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }


        private decimal menge;
        public decimal Menge
        {
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }


        private decimal? preis;
        public decimal? Preis
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }


#endregion

    }
}
