﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;

namespace Comain.Client.Services.Verwaltung
{

    public class MaterialService : IMaterialService
    {

        private const String url = "/VW/MT";
        protected readonly IRepository store;
        private readonly IMapper mapper;

        public TrackableCollection<Materialstamm> CurrentList { get; private set; }


        public MaterialService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<TrackableCollection<Materialstamm>> LoadAsync(CancellationToken ct = default)
        {

            CurrentList = null;
            var result = await store.GetAsync<IList<MaterialDto>>(url, ct).ConfigureAwait(false);
            var list = mapper.Map<List<Materialstamm>>(result);
            list.Sort();
            CurrentList = list.AsTrackable();
            return CurrentList;   
        }


        public async Task SaveAsync(CancellationToken ct = default)
        {

            if (CurrentList == null) return;
            
            var result = new MtUpdateDto { Materialliste = mapper.AsDtoList<Materialstamm, MaterialDto>(CurrentList),
                                           MtLfMap = new List<MtLfMap>() };

            foreach (var mt in CurrentList) {
            
                if (mt.MaterialLieferantMap == null || mt.MaterialLieferantMap.Count == 0) continue;
                result.MtLfMap.AddRange(mapper.AsDtoList<MaterialLieferant, MtLfMap>(mt.MaterialLieferantMap));
            }                               

            if ((result.Materialliste == null || result.Materialliste.Count == 0) && (result.MtLfMap == null || result.MtLfMap.Count == 0)) return;
               
            await store.PostAsync<MtUpdateDto>(url, result, ct).ConfigureAwait(false);
            CurrentList.AcceptChanges();
            CurrentList.ForEach(p => p.MaterialLieferantMap.AcceptChanges());
        }


        public async Task<IEnumerable<KeyNameDto>> EinheitenAsync(CancellationToken ct = default)
        {
            
            var result = await store.GetAsync<IList<KeyNameDto>>("/VW/EH", ct).ConfigureAwait(false);
            return result;
        }


        public async Task<IEnumerable<KeyNameDto>> WarengruppenAsync(CancellationToken ct = default)
        {
            
            var result = await store.GetAsync<IList<KeyNameDto>>("/VW/WG", ct).ConfigureAwait(false);
            return result;
        }


        public async Task<IEnumerable<LieferantDto>> LieferantenAsync(CancellationToken ct = default)
        {
            
            var result = await store.GetAsync<IList<LieferantDto>>("/VW/LF", ct).ConfigureAwait(false);
            return result;
        }


        public bool HasChanges
        {

            get {
            
                if (CurrentList == null) return false;
                if (CurrentList.IsChanged) return true;
                return CurrentList.SelectMany(p => p.MaterialLieferantMap).Any(p => p.IsChanged);
            }
        }


        public void Reset()
        {
            CurrentList = null;
        }
    }
}
