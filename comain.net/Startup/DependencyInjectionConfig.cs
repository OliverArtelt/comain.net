﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications.Services;
using System.Waf.Presentation.Services;
using AutoMapper;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.Controllers.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Services;
using Comain.Client.Services.Verwaltung;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Dokumente;
using Comain.Client.ViewModels.Services;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views;
using Comain.Client.Views.Dokumente;
using Comain.Client.Views.IHObjekte;
using Comain.Client.Views.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Scrutor;

namespace Comain.Client
{

    public static class DependencyInjectionConfig
    {

        public static void RegisterServices(HostBuilderContext context, IServiceCollection services)
        {

            services.AddSingleton<ApplicationController>();
            services.AddSingleton<ISettings, Settings>();
            services.AddSingleton<IRepository, Repository>();
            services.AddSingleton<IFileDialogService, FileDialogService>();
            services.AddSingleton<ITabletInfo, TabletInfo>();

            services.AddTransient<IBackgroundPrinter, BackgroundPrinter>();
            services.AddTransient<ViewModels.IHObjekte.IHSelektorViewModel>();
            services.AddTransient<ViewModels.IHObjekte.IHSimpleSelektorViewModel>();
            services.AddTransient<IIHSelektorView, IHSelektorView>();
            services.AddTransient<ProgressViewModel>();
            services.AddTransient<IProgressView, ProgressView>();
            services.AddTransient<IDokumentAssignView, DokumentAssignView>();
            services.AddTransient<DokumentAssignViewModel>();
            services.AddTransient<IIHSimpleSelektorView, IHSimpleSelektorView>();
            services.AddTransient<IPersonalSearchProvider, PersonalSearchProvider>();
            services.AddTransient<ICameraView, CameraView>();
            services.AddTransient<CameraViewModel>();
            services.AddTransient<IImageEditView, ImageEditView>();
            services.AddTransient<ImageEditViewModel>();

            services.AddSingleton<IControllerBus, ControllerBus>();
            services.AddSingleton<IControllerMediator, ControllerBus>();

            services.AddSingleton(typeof(IConfigController<>), typeof(ConfigController<>));
            services.AddSingleton(typeof(ISimpleService<,>), typeof(SimpleService<,>));
            services.AddSingleton(typeof(SimpleViewModel<,>));


            services.Scan(scan =>
                scan.FromAssemblyDependencies(Assembly.GetEntryAssembly())
                    .AddClasses(classes => classes.AssignableTo<IVwTyp>())
                        .UsingRegistrationStrategy(RegistrationStrategy.Append)
                        .AsImplementedInterfaces()
                        .WithSingletonLifetime()
                    .AddClasses(classes => classes.AssignableTo<IAdminTyp>())
                        .UsingRegistrationStrategy(RegistrationStrategy.Append)
                        .AsImplementedInterfaces()
                        .WithSingletonLifetime()
                    .AddClasses(classes => classes.AssignableTo<IControllerCommand>())
                        .UsingRegistrationStrategy(RegistrationStrategy.Append)
                        .AsSelfWithInterfaces()
                        .WithSingletonLifetime()
                    .AddClasses(classes => classes.AssignableTo<IController>())
                        .UsingRegistrationStrategy(RegistrationStrategy.Append)
                        .AsSelfWithInterfaces()
                        .WithSingletonLifetime()
                    .AddClasses(classes => classes.Where(type => type.Name.EndsWith("Factory")))
                        .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                        .AsMatchingInterface()
                        .WithTransientLifetime()
                    .AddClasses()
                        .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                        .AsMatchingInterface()
                        .WithSingletonLifetime()
                    .AddClasses(classes => classes.Where(type => type.Name.EndsWith("ViewModel")))
                        .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                        .AsSelfWithInterfaces()
                        .WithSingletonLifetime());
        }
    }
}
