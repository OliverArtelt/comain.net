﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Views.IHObjekte;


namespace Comain.Client.ViewModels.IHObjekte
{

    public class IHObjektFilterViewModel : ViewModel<IIHObjektFilterView>
    {

        private readonly SynchronizationContext context;
        private DelegateCommand searchCommand;
        private DelegateCommand nextCommand;
        private DelegateCommand prevCommand;


        public IHObjektFilterViewModel(IIHObjektFilterView view, IIHCompleteSearchProvider iterator, IIHFastSearchProvider autocomplete) : base(view)
        {

            context = SynchronizationContext.Current;
            
            Iterator = iterator;
            Autocomplete = autocomplete;
            SetIHListe(null);

            NextCommand = nextCommand = new DelegateCommand(Next, () => Iterator.Gesamt > 0);
            PrevCommand = prevCommand = new DelegateCommand(Prev, () => Iterator.Gesamt > 0);
            SearchCommand = searchCommand = new DelegateCommand(Search, () => !String.IsNullOrEmpty(suchText));
        }


        public void SetIHListe(IList<IHListDto> list) 
        {

            if (list.IsNullOrEmpty()) {
            
                ResultText = "Assets filtern";
                return;
            }
            
            if (list.Count == 1) ResultText = "Ein Asset gefunden.";
            else ResultText = list.Count.ToString("N0") + " Assets gefunden."; 
            
            if (list == null) {

                Iterator.SetzeIHObjekte(null);
                Autocomplete.ClearItems();
           
            } else {
                
                Iterator.SetzeIHObjekte(list);
                Autocomplete.SetItems(list);
            }

            SuchText = null;
            RaisePropertyChanged("FoundText");
            RaisePropertyChanged("Current");
        }


        public void Search()
        {
            
            Iterator.Search(suchText);
            IHObjekt_Id = Iterator.Current?.Id;
            EnableCommands();
            RaisePropertyChanged("FoundText");
        }


        public void Next()
        {
            IHObjekt_Id = Iterator.Next().Id;
        }


        public void Prev()
        {
            IHObjekt_Id = Iterator.Prev().Id;
        }


        public void Unload()
        {
            SetIHListe(null);
        }


        private void EnableCommands()
        {

            context.Send(new SendOrPostCallback((o) => {

                searchCommand.RaiseCanExecuteChanged();
                nextCommand.RaiseCanExecuteChanged();
                prevCommand.RaiseCanExecuteChanged();     
            
            }), null);
        }


#region P R O P E R T I E S


        public ICommand                  SearchCommand  { get; private set; }
        public ICommand                  NextCommand    { get; private set; }
        public ICommand                  PrevCommand    { get; private set; }
        public IIHFastSearchProvider     Autocomplete   { get; private set; }
        public IIHCompleteSearchProvider Iterator       { get; private set; }


        /// <summary>
        /// Position des Iterators
        /// </summary>
        public String FoundText 
        { 
            get { 
            
                if (String.IsNullOrEmpty(suchText)) return null;
                if (Iterator.Gesamt == 0) return "0 / 0";
                return String.Format("{0} / {1}", Iterator.Aktuell + 1, Iterator.Gesamt); 
            }
        }

        
        private String suchText;
        /// <summary>
        /// Suchtext für die Suche mit Iterator
        /// </summary>
        public String SuchText
        { 
            get { return suchText; }
            set { if (SetProperty(ref suchText, value)) EnableCommands(); }
        }

        
        private String resultText;
        /// <summary>
        /// Anzahl im Baum befindlicher Objekte
        /// </summary>
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }

        
        private bool editEnabled;
        /// <summary>
        /// Bearbeitung wird verhindert wenn Detail im Bearbeitungsmodus ist
        /// </summary>
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }

        
        private int? ihObjekt_Id;
        public int? IHObjekt_Id
        { 
            get { return ihObjekt_Id; }
            set { if (SetProperty(ref ihObjekt_Id, value)) RaisePropertyChanged(nameof(FoundText)); }
        }

        
        private IHFilterItem current;
        public IHFilterItem Current
        { 
            get { return current; }
            set { if (SetProperty(ref current, value)) IHObjekt_Id = Current?.Id; }
        }


#endregion

    }
}
