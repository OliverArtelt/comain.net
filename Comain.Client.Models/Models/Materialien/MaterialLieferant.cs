﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{
   
    public class MaterialLieferant : Entity, IDataErrorInfo
    {
       
        private String   name;
        private String   nummer;
        private String   lieferant;
        private decimal? preis;
        private short?   rank;
        private int      lieferant_Id;
        private int      materialstamm_Id;
        private decimal? mindestbestellmenge;
        private int?     bestellzeit;


#region P R O P E R T I E S


        public String Name 
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        

        public String Nummer 
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public decimal? Preis 
        {
            get { return preis; }
            set { SetProperty(ref preis, value); }
        }


        public decimal? Mindestbestellmenge
        {
            get { return mindestbestellmenge; }
            set { SetProperty(ref mindestbestellmenge, value); }
        }


        public int? Bestellzeit
        {
            get { return bestellzeit; }
            set { SetProperty(ref bestellzeit, value); }
        }
        

        public short? Rank 
        {
            get { return rank; }
            set { SetProperty(ref rank, value); }
        }
        

        public int Lieferant_Id 
        {
            get { return lieferant_Id; }
            set { SetProperty(ref lieferant_Id, value); }
        }
        

        public int Materialstamm_Id 
        {
            get { return materialstamm_Id; }
            set { SetProperty(ref materialstamm_Id, value); }
        }

       
        public String Lieferant
        {
            get { return lieferant; }
            set { SetProperty(ref lieferant, value); }
        }

       
        public bool NoRank
        {
            get { return rank.GetValueOrDefault() == 0; }
            set { 
            
                if (value) {

                    rank = null;
                    RaisePropertyChanged("NoRank");  
                    RaisePropertyChanged("Rank1");  
                    RaisePropertyChanged("Rank2");  
                    RaisePropertyChanged("Rank3");  
                }
            }
        }

       
        public bool Rank1
        {
            get { return rank.GetValueOrDefault() == 1; }
            set { 
            
                if (value) {

                    rank = 1;
                    RaisePropertyChanged("NoRank");  
                    RaisePropertyChanged("Rank1");  
                    RaisePropertyChanged("Rank2");  
                    RaisePropertyChanged("Rank3");  
                }
            }
        }

       
        public bool Rank2
        {
            get { return rank.GetValueOrDefault() == 2; }
            set { 
            
                if (value) {

                    rank = 2;
                    RaisePropertyChanged("NoRank");  
                    RaisePropertyChanged("Rank1");  
                    RaisePropertyChanged("Rank2");  
                    RaisePropertyChanged("Rank3");  
                }
            }
        }

       
        public bool Rank3
        {
            get { return rank.GetValueOrDefault() == 3; }
            set { 
            
                if (value) {

                    rank = 3;
                    RaisePropertyChanged("NoRank");  
                    RaisePropertyChanged("Rank1");  
                    RaisePropertyChanged("Rank2");  
                    RaisePropertyChanged("Rank3");  
                }
            }
        }
      

#endregion


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return null;
                    if (Nummer.Length > 48) return "Die Nummer darf aus maximal 48 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
