﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Models.Users;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class AssetPersonalViewModel : ValidatableViewModel<IAssetPersonalView, IHObjekt>
    {

        public AssetPersonalViewModel(IAssetPersonalView view) : base(view)
        {
        }


        public override void CurrentChanged(IHObjekt oldSelected)
        {

            RaisePropertyChanged(nameof(ListVisible));

            if (Current == null) {

                AssignList = null;
                RaisePropertyChanged(nameof(AssignList));
                return;
            }

            var dic = Current.Personalliste.ToDictionary(p => p.Personal_Id);
            AssignList = Personalliste.Select(p => new IHObjektPersonalAssignViewItem(Current, p, dic.GetValueOrDefault(p.Id))).ToList();

            RaisePropertyChanged(nameof(AssignList));
        }


        public void Select(bool select)
            => AssignList?.ForEach(p => p.Select(select));


        public virtual void Unload()
        {
            AssignList = null;
            Personalliste = null;
        }


#region P R O P E R T I E S


        public bool ListVisible => Current != null && Current.IstMaschine;


        private bool inEditMode;
        public bool InEditMode
        {
            get { return inEditMode; }
            set { SetProperty(ref inEditMode, value); }
        }


        private IList<PersonalFilterModel> personalliste;
        public IList<PersonalFilterModel> Personalliste
        {
            get => personalliste;
            set => SetProperty(ref personalliste, value);
        }


        private IList<IHObjektPersonalAssignViewItem> assignList;
        public IList<IHObjektPersonalAssignViewItem> AssignList
        {
            get => assignList;
            set => SetProperty(ref assignList, value);
        }


#endregion

    }
}
