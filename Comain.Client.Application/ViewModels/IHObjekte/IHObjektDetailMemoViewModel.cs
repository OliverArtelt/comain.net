﻿using System;
using System.Linq;
using System.Collections;
using Comain.Client.Models;
using Comain.Client.Views.IHObjekte;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{
    
    public class IHObjektDetailMemoViewModel : ValidatableViewModel<IIHObjektDetailMemoView, IHObjekt>
    {

        public IHObjektDetailMemoViewModel(IIHObjektDetailMemoView view) : base(view)
        {}


        public override void CurrentChanged(IHObjekt oldSelected)
        {

            IHObjektPzMap = null;
            if (Current != null && Current.IHObjektPzMap != null) IHObjektPzMap = Current.IHObjektPzMap.OrderByDescending(p => p.Datum).ToList();

            IHObjektKstMap = null;
            if (Current != null && Current.IHObjektKstMap != null) IHObjektKstMap = Current.IHObjektKstMap.OrderByDescending(p => p.Datum).ToList();

            RaisePropertyChanged("TechnischeDaten");   
            RaisePropertyChanged("Bemerkungen");   
            RaisePropertyChanged("IHObjektPzMap");   
            RaisePropertyChanged("IHObjektKstMap");   
        }

        
        public void Unload()
        {
        }  


#region P R O P E R T I E S
 
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }
 

        public String TechnischeDaten
        {
            get { return current == null? null: current.TechnischeDaten; }   
            set { 
                
                if (current != null && current.TechnischeDaten != value) {
                    
                    current.TechnischeDaten = value;
                    RaisePropertyChanged("TechnischeDaten");
                }
            }
        }
 

        public String Bemerkungen
        {
            get { return current == null? null: current.Bemerkungen; }   
            set { 
                
                if (current != null && current.Bemerkungen != value) {
                    
                    current.Bemerkungen = value;
                    RaisePropertyChanged("Bemerkungen");
                }
            }
        }
 

        public IEnumerable IHObjektPzMap  { get; private set; }
        
        public IEnumerable IHObjektKstMap { get; private set; }


#endregion

    }
}
