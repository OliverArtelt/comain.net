﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views;
using Comain.Client.Views.Aufträge;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragDetailPersonalViewModel : ValidatableViewModel<IAuftragDetailPersonalView, Auftrag>
    {

        private readonly SynchronizationContext context;
        private readonly IAuftragDetailPersonalView view;


        public AuftragDetailPersonalViewModel(IAuftragDetailPersonalView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
            this.view = view;
        }


        public override void CurrentChanged(Auftrag oldSelected)
        {

            RaisePropertyChanged(nameof(Auftraggeber));
            RaisePropertyChanged(nameof(Auftragnehmer));
            RaisePropertyChanged(nameof(Auftragsleiter));
            RaisePropertyChanged(nameof(Abnahmeberechtigter));
            RaisePropertyChanged(nameof(ExterneNummer));
            RaisePropertyChanged(nameof(Gewerk));
            RaisePropertyChanged(nameof(Beauftragungsdatum));
            RaisePropertyChanged(nameof(Fertigstellungstermin));
            RaisePropertyChanged(nameof(Auftragsleiterdatum));
            RaisePropertyChanged(nameof(Abnahmedatum));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Wert));
            RaisePropertyChanged(nameof(Stunden));
            RaisePropertyChanged(nameof(InterneAuStelle));
            RaisePropertyChanged(nameof(FälligkeitEnabled));
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist, IEnumerable<NummerFilterModel> gwlist)
        {

            context.Send(new SendOrPostCallback((o) => {

                PsSearchProvider.SetItems(pslist);
                Gewerkliste = gwlist;
                Personalliste = pslist;

            }), null);

        }


        public void Unload()
        {

            PsSearchProvider.ClearItems();
            Gewerkliste = null;
            Personalliste = null;
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { if (SetProperty(ref editEnabled, value)) RaisePropertyChanged(nameof(FälligkeitEnabled)); }
        }


        public bool FälligkeitEnabled => EditEnabled && Current != null && !Current.UnveränderlicherTermin;


        private bool addEnabled;
        public bool AddEnabled
        {
            get { return addEnabled; }
            set { SetProperty(ref addEnabled, value); }
        }


        private bool istKunde;
        public bool IstKunde
        {
            get { return istKunde; }
            set { SetProperty(ref istKunde, value); }
        }


        private IEnumerable<PersonalFilterModel> personalliste;
        public IEnumerable<PersonalFilterModel> Personalliste
        {
            get { return personalliste; }
            set { SetProperty(ref personalliste, value); }
        }


        private IEnumerable<NummerFilterModel> gewerkliste;
        public IEnumerable<NummerFilterModel> Gewerkliste
        {
            get { return gewerkliste; }
            set { SetProperty(ref gewerkliste, value); }
        }


        public int? InterneAuStelle
        {
            get { return current == null? (int?)null: current.InterneAuStelle; }
            set {

                if (current != null && current.InterneAuStelle != value) {

                    current.InterneAuStelle = value;
                    RaisePropertyChanged(nameof(InterneAuStelle));
                }
            }
        }


        public decimal? Stunden
        {
            get { return current == null? (decimal?)null: current.Stunden; }
            set {

                if (current != null && current.Stunden != value) {

                    current.Stunden = value;
                    RaisePropertyChanged(nameof(Stunden));
                }
            }
        }


        public decimal? Wert
        {
            get { return current == null? (decimal?)null: current.Wert; }
            set {

                if (current != null && current.Wert != value) {

                    current.Wert = value;
                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public short? Menge
        {
            get { return current == null? (short?)null: current.Menge; }
            set {

                if (current != null && current.Menge != value) {

                    current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public DateTime? Abnahmedatum
        {
            get { return current == null? (DateTime?)null: current.Abnahmedatum; }
            set {

                if (current != null && current.Abnahmedatum != value) {

                    current.Abnahmedatum = value;
                    RaisePropertyChanged(nameof(Abnahmedatum));
                }
            }
        }


        public DateTime? Auftragsleiterdatum
        {
            get { return current == null? (DateTime?)null: current.Auftragsleiterdatum; }
            set {

                if (current != null && current.Auftragsleiterdatum != value) {

                    current.Auftragsleiterdatum = value;
                    RaisePropertyChanged(nameof(Auftragsleiterdatum));
                }
            }
        }


        public DateTime? Fertigstellungstermin
        {
            get { return current == null? (DateTime?)null: current.Fertigstellungstermin; }
            set {

                if (current != null && current.Fertigstellungstermin != value) {

                    current.Fertigstellungstermin = value;
                    RaisePropertyChanged(nameof(Fertigstellungstermin));
                }
            }
        }


        public DateTime? Beauftragungsdatum
        {
            get { return current == null? (DateTime?)null: current.Beauftragungsdatum; }
            set {

                if (current != null && current.Beauftragungsdatum != value) {

                    current.Beauftragungsdatum = value;
                    RaisePropertyChanged(nameof(Beauftragungsdatum));
                }
            }
        }


        public String ExterneNummer
        {
            get { return current == null? null: current.ExterneNummer; }
            set {

                if (current != null && current.ExterneNummer != value) {

                    current.ExterneNummer = value;
                    RaisePropertyChanged(nameof(ExterneNummer));
                }
            }
        }


        public NummerFilterModel Gewerk
        {
            get { return current == null? null: current.Gewerk; }
            set {

                if (current != null && current.Gewerk != value) {

                    current.Gewerk = value;
                    RaisePropertyChanged(nameof(Gewerk));
                }
            }
        }


        public PersonalFilterModel Abnahmeberechtigter
        {
            get { return current == null? null: current.Abnahmeberechtigter; }
            set {

                if (current != null && current.Abnahmeberechtigter != value) {

                    current.Abnahmeberechtigter = value;
                    RaisePropertyChanged(nameof(Abnahmeberechtigter));
                }
            }
        }


        public PersonalFilterModel Auftragsleiter
        {
            get { return current == null? null: current.Auftragsleiter; }
            set {

                if (current != null && current.Auftragsleiter != value) {

                    current.Auftragsleiter = value;
                    RaisePropertyChanged(nameof(Auftragsleiter));
                }
            }
        }


        public PersonalFilterModel Auftragnehmer
        {
            get { return current == null? null: current.Auftragnehmer; }
            set {

                if (current != null && current.Auftragnehmer != value) {

                    current.Auftragnehmer = value;
                    RaisePropertyChanged(nameof(Auftragnehmer));
                }
            }
        }


        public PersonalFilterModel Auftraggeber
        {
            get { return current == null? null: current.Auftraggeber; }
            set {

                if (current != null && current.Auftraggeber != value) {

                    current.Auftraggeber = value;
                    RaisePropertyChanged(nameof(Auftraggeber));
                }
            }
        }


#endregion

    }
}
