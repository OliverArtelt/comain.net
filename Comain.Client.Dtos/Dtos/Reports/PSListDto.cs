﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class PSListDto
    {

        public String   Klammer         { get; set; }
        public String   Nummer          { get; set; }
        public String   Gewerk          { get; set; }
        public String   Nachname        { get; set; }
        public String   Vorname         { get; set; }
        public String   Adresse         { get; set; }
        public String   Telefon         { get; set; }
        public String   Qualifikation   { get; set; }
        public int?     Monatssoll      { get; set; }
        public decimal? Stundensatz     { get; set; }
    }
}
