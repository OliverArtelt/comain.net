﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views;


namespace Comain.Client.ViewModels
{

    public class LoginViewModel : ViewModel<ILoginView>, IDelayableViewModel
    {

        public LoginViewModel(ILoginView view) : base(view)
        {

#if DEBUG

            FillCommand = new DelegateCommand(Fill);

#endif
        }


#if DEBUG

        private void Fill(object p)
        {

            Username = p.ToString();
            Password = "Start123!";
           // Password = "Olonedugu#482";
           // Password = "Ohip8_Uwe38";
        }

        public bool ShowDefaultLogins => true;

#else

        public bool ShowDefaultLogins => false;

#endif


#region P R O P E R T I E S


        public ICommand FillCommand { get; private set; }


        private IEnumerable hosts;
        public IEnumerable Hosts
        {
            get { return hosts; }
            set { SetProperty(ref hosts, value); }
        }


        private String username;
        public String Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }


        private String password;
        public String Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }


        private ICommand actionCommand;
        public ICommand ActionCommand
        {
            get { return actionCommand; }
            set { SetProperty(ref actionCommand, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
