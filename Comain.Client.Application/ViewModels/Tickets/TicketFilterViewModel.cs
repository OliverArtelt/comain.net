﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Tickets
{

    public class TicketFilterViewModel : ViewModel<ITicketFilterView>
    {

        private readonly SynchronizationContext context;
        private readonly ILoginService login;

 
        public TicketFilterViewModel(ITicketFilterView view, ILoginService login) : base(view)
        {

            this.login = login;
            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(ResetSearch);
            ResetSearch();
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();

            filter.Add(nameof(Suchtext),            Suchtext);           
            filter.Add(nameof(StatusErstellt),      StatusErstellt);        
            filter.Add(nameof(StatusBearbeitung),   StatusBearbeitung);  
            filter.Add(nameof(StatusUnterbrochen),  StatusUnterbrochen); 
            filter.Add(nameof(StatusÜbergeben),     StatusÜbergeben);    
            filter.Add(nameof(StatusAbgenommen),    StatusAbgenommen);    
            filter.Add(nameof(StatusAbgelehnt),     StatusAbgelehnt);    
            filter.Add(nameof(StatusAbgeschlossen), StatusAbgeschlossen);
            filter.Add(nameof(StatusStorniert),     StatusStorniert);    
            filter.Add(nameof(UseD1),               UseD1);              
            filter.Add(nameof(UseD2),               UseD2);              
            filter.Add(nameof(UseD3),               UseD3);              
            filter.Add(nameof(UseD4),               UseD4);              
            filter.Add(nameof(UseD5),               UseD5);              

            return filter;
        }


        public void ResetSearch()
        {

            Suchtext            = null;
            UseD1               = true;
            UseD2               = true;
            UseD3               = true;
            UseD4               = true;
            UseD5               = true;

            StatusErstellt      = false;   
            StatusBearbeitung   = false;
            StatusUnterbrochen  = false;
            StatusÜbergeben     = false;
            StatusAbgenommen    = false;
            StatusAbgelehnt     = false;
            StatusAbgeschlossen = false;
            StatusStorniert     = false;

            if (login.AktuellerNutzer == null) return;

            if (login.AktuellerNutzer.IstTicketInstandhalter) {

                StatusErstellt      = true;   
                StatusBearbeitung   = true;
                StatusAbgelehnt     = true;
            }

            if (login.AktuellerNutzer.IstTicketSchichtführer) {

                StatusErstellt      = true;   
                StatusBearbeitung   = true;
                StatusÜbergeben     = true;
            }

            if (login.AktuellerNutzer.IstTicketLeiterInstandhaltung || login.AktuellerNutzer.IstTicketLeiterProduktion || 
                login.AktuellerNutzer.IstAdministrator || login.AktuellerNutzer.IstBevor) {

                StatusErstellt      = true;   
                StatusBearbeitung   = true;
                StatusUnterbrochen  = true;
                StatusÜbergeben     = true;
                StatusAbgenommen    = true;
                StatusAbgelehnt     = true;
            }
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S


        private String meineTicketsText;
        public String MeineTicketsText
        { 
            get { return meineTicketsText; }
            set { if (SetProperty(ref meineTicketsText, value)) RaisePropertyChanged(nameof(MeineTicketsVisible)); }
        }


        public bool MeineTicketsVisible
        { 
            get { return !String.IsNullOrEmpty(MeineTicketsText); }
        }


        private ICommand meineTicketsCommand;
        public ICommand MeineTicketsCommand
        { 
            get { return meineTicketsCommand; }
            set { SetProperty(ref meineTicketsCommand, value); }
        }
 

        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }
 

        public ICommand ResetCommand { get; private set; }


        private bool editEnabled = true;
        public bool EditEnabled
        {
            get { return editEnabled; }   
            set { SetProperty(ref editEnabled, value); }
        }


        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private String suchtext;
        public String Suchtext
        { 
            get { return suchtext; }
            set { SetProperty(ref suchtext, value); }
        }


        private bool statusErstellt;
        public bool StatusErstellt
        {
            get { return statusErstellt; }   
            set { SetProperty(ref statusErstellt, value); }
        }


        private bool statusBearbeitung;
        public bool StatusBearbeitung
        {
            get { return statusBearbeitung; }   
            set { SetProperty(ref statusBearbeitung, value); }
        }


        private bool statusUnterbrochen;
        public bool StatusUnterbrochen
        {
            get { return statusUnterbrochen; }   
            set { SetProperty(ref statusUnterbrochen, value); }
        }


        private bool statusÜbergeben;
        public bool StatusÜbergeben
        {
            get { return statusÜbergeben; }   
            set { SetProperty(ref statusÜbergeben, value); }
        }


        private bool statusAbgenommen;
        public bool StatusAbgenommen
        {
            get { return statusAbgenommen; }   
            set { SetProperty(ref statusAbgenommen, value); }
        }


        private bool statusAbgelehnt;
        public bool StatusAbgelehnt
        {
            get { return statusAbgelehnt; }   
            set { SetProperty(ref statusAbgelehnt, value); }
        }


        private bool statusAbgeschlossen;
        public bool StatusAbgeschlossen
        {
            get { return statusAbgeschlossen; }   
            set { SetProperty(ref statusAbgeschlossen, value); }
        }


        private bool statusStorniert;
        public bool StatusStorniert
        {
            get { return statusStorniert; }   
            set { SetProperty(ref statusStorniert, value); }
        }

     
        private bool useD1;
        public bool UseD1
        { 
            get { return useD1; }
            set { SetProperty(ref useD1, value); }
        }

     
        private bool useD2;
        public bool UseD2
        { 
            get { return useD2; }
            set { SetProperty(ref useD2, value); }
        }

     
        private bool useD3;
        public bool UseD3
        { 
            get { return useD3; }
            set { SetProperty(ref useD3, value); }
        }

     
        private bool useD4;
        public bool UseD4
        { 
            get { return useD4; }
            set { SetProperty(ref useD4, value); }
        }

     
        private bool useD5;
        public bool UseD5
        { 
            get { return useD5; }
            set { SetProperty(ref useD5, value); }
        }

     
#endregion

    }
}
