﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Services.Reports;
using Comain.Client.ViewData.WPlan;
using Comain.Client.Views.Reports.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class MultipleIHListViewModel : ViewModel<IMultipleIHListView>, IFilterViewModel
    {

        private readonly IFilterService store;
        private readonly SynchronizationContext context = SynchronizationContext.Current;
        private bool allSelected;
        private bool allExpanded;


        public MultipleIHListViewModel(IFilterService store, IMultipleIHListView view) : base(view)
        {

            this.store = store;
            OpenCommand = new DelegateCommand(p => { PopupIsOpen = true; });
            ClearCommand = new DelegateCommand(Reset);
            SelectCommand = new DelegateCommand(p => { allSelected = !allSelected; PZList?.ForEach(p => { p.IsChecked = allSelected; }); }); 
            ExpandCommand = new DelegateCommand(p => { allExpanded = !allExpanded; PZList?.ForEach(p => { p.IsExpanded = allExpanded; }); }); 
        }


        public string FilterAlsString() => String.IsNullOrWhiteSpace(Assetliste)? null: $"Assets {Assetliste}";


        public string Validiere() => null;


        public void AddFilter(Suchkriterien filter)
        {
            if (!String.IsNullOrWhiteSpace(Assetliste)) filter.Add("Assets", Assetliste);
        }


        public void Reset()
        {

            Assetliste = null;
            PZList?.ForEach(p => { p.IsChecked = false; p.IsExpanded = false; });
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            PZList = store.IHTreeListe;
            return Task.FromResult(true);
        }


        public void Unload()
        {
            PZList = null;
        }


        public void UpdateSelected()
        {
            Assetliste = String.Join(", ",  GewählteMaschinen()?.Select(p => p.Nummer).OrderBy(p => p).ToArray());
        }


        public IEnumerable<IHFilterItem> GewählteMaschinen()
            => PZList?.SelectMany(p => p.IHObjekte.Where(q => q.IsChecked));


#region P R O P E R T I E S


        private bool popupIsOpen;
        public bool PopupIsOpen
        {
            get => popupIsOpen;
            set {

                SetProperty(ref popupIsOpen, value);
                if (!value) UpdateSelected();
            }
        }


        private String assetliste;
        public String Assetliste
        {
            get => assetliste;
            set => SetProperty(ref assetliste, value);
        }


        public ICommand OpenCommand     { get; }
        public ICommand ClearCommand    { get; }
        public ICommand SelectCommand   { get; }
        public ICommand ExpandCommand   { get; }


        private IList<PZFilterItem> pzlist;
        public IList<PZFilterItem> PZList
        {
            get { return pzlist; }
            set { SetProperty(ref pzlist, value); }
        }

#endregion

    }
}
