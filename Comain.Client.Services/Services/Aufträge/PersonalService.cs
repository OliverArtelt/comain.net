﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;


namespace Comain.Client.Services.Aufträge
{

    public class PersonalService : IPersonalService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public PersonalService(IRepository store, IMapper mapper)
        {

            this.store = store;
            this.mapper = mapper;
        }


        public async Task<Personalleistung> SaveAsync(Personalleistung model, CancellationToken ct = default)
        {

            var dto = mapper.Map<PersonalleistungDto>(model);
            var savedDto = await store.PostAsync<PersonalleistungDto, PersonalleistungDto>("/LE", dto, ct).ConfigureAwait(false);
            var savedModel = mapper.Map<Personalleistung>(savedDto);
            return savedModel;
        }


        public async Task DeleteAsync(Personalleistung model, CancellationToken ct = default)
        {

            var url = String.Format(@"/LE/{0}", model.Id);
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
        {
            return FilterAsync<PersonalFilterModel>("/Filter/PS", ct);
        }


        public Task<IList<AufschlagtypFilterModel>> FilterAufschlagtypenAsync(CancellationToken ct = default)
        {
            return FilterAsync<AufschlagtypFilterModel>("/Filter/AS", ct);
        }


        private Task<IList<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<IList<TFilterModel>>(url, ct);
        }
    }
}
