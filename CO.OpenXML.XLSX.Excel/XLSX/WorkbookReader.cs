﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Excel-Dokument öffnen und Wörterbuch aufsetzen
    /// </summary>
    public class WorkbookReader
    {

        WorksheetReader myOpenSheet;
        StringPoolDecoder myDictionary;
        Archiver myArchive;

        
        /// <summary>
        /// Dokument öffnen und Wörterbuch aufsetzen
        /// </summary>
        /// <param name="path">Dokumentpfad</param>
        public WorkbookReader(string path)
        { 

            myArchive = new Archiver();
            myArchive.OpenArchive(path);

            try {       

                //Dokumente ohne Stringpool (leeres Dokument, nur Zahlen, nur eingebettete Strings) akzeptieren
                string sharedStrings = myArchive.ExtractAsString("xl\\sharedStrings.xml");
                myDictionary = new StringPoolDecoder(sharedStrings);

            } catch (Exception) {}
        }

        
        /// <summary>
        /// Sheet zum Auslesen öffnen
        /// </summary>
        /// <param name="sheetId"> Tab-Id (1,2,3)</param>
        /// <returns>Worksheet</returns>
        public WorksheetReader OpenSheet(int sheetId)
        {

            String path = String.Format("xl\\worksheets\\sheet{0}.xml", sheetId);
            myOpenSheet = new WorksheetReader(myArchive.ExtractAsString(path), myDictionary);
            return myOpenSheet;
        }
    }
}
