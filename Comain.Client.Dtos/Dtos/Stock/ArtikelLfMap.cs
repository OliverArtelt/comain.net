﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class ArtikelLfMap : ChangeTrackingBase
    {
       
        public String               Name                    { get; set; }
        public String               Nummer                  { get; set; }
        public decimal?             Preis                   { get; set; }
        public short?               Rank                    { get; set; }
        public decimal?             Mindestbestellmenge     { get; set; }
        public int?                 Bestellzeit             { get; set; }
        public int                  Materialstamm_Id        { get; set; }

        public LieferantFilterModel Lieferant               { get; set; }
    }
}
