﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan;

namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class ZeitstrahlDto
    {

        public DateTime                 ErstelltAm                  { get; set; }
        public String                   Anlagen                     { get; set; }
        public String                   LeistungsartAsString        { get; set; }
        public String                   GewerkAsString              { get; set; }
        public String                   PersonalAsString            { get; set; }
        public String                   PlanungstypAsString         { get; set; }
        public String                   PlanungsstatusAsString      { get; set; }
        public String                   Zeitraum                    { get; set; }
        public int?                     WochenRückmeldung           { get; set; }

        public List<ZeitDto>            Perioden                    { get; set; }
        public List<StandortDto>        Standorte                   { get; set; }
        public List<MaterialLagerDto>   Material                    { get; set; }
    }
}
