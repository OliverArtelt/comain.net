﻿using System.Windows.Controls;
using Comain.Client.Views.Reports;

namespace Comain.Client.Views.Stock.Warenausgänge
{
    /// <summary>
    /// Interaction logic for AusgangDetailReportView.xaml
    /// </summary>
    public partial class AusgangDetailReportView:UserControl        ,IAusgangDetailReportView
    {
        public AusgangDetailReportView()
        {
        InitializeComponent();
        }
    }
}
