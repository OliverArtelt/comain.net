﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Trackers;
using Comain.Client.ViewData.Verwaltung;


namespace Comain.Client.Services.Verwaltung
{

    public class StandortService : IStandortService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;
        private const String uri = "/VW/KstPz";
        private List<Kostenstelle> currentKstList;

        public TrackableCollection<Standort> CurrentPzList { get; private set; }


        public StandortService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            var current = await store.GetAsync<KstPzDto>(uri, ct).ConfigureAwait(false);
            var pzlist = mapper.Map<List<Standort>>(current.Standorte);
            pzlist.Sort();
            currentKstList = mapper.Map<List<Kostenstelle>>(current.Kostenstellen);
            currentKstList.Sort();
            var kstdic = currentKstList.ToDictionary(p => p.Id);
            var mapdic = current.Map.ToMultiDictionary(p => p.Standort_Id, p => kstdic[p.Kostenstelle_Id]);

            pzlist.ForEach(pz => {

                if (mapdic.ContainsKey(pz.Id)) pz.ZugeordneteKostenstellen = new TrackableCollection<Kostenstelle>(mapdic[pz.Id]);
            });

            CurrentPzList = new TrackableCollection<Standort>(pzlist);
        }


        public IEnumerable<KstSelektorItem> Kostenstellen(Standort pz)
        {

            var kstpzlist = currentKstList.Select(kst => new KstSelektorItem(pz, kst)).ToList();
            return kstpzlist;
        }


        public bool HasChanges
        {
            get { return CurrentPzList != null && (CurrentPzList.IsChanged || CurrentPzList.Any(p => p.ZugeordneteKostenstellen.IsChanged)); }
        }


        public async Task SaveAsync(CancellationToken ct = default)
        {

            var result = new KstPzDto();
            result.Standorte = mapper.AsDtoList<Standort, PzDto>(CurrentPzList);
            result.Map = new List<KstPzMap>();

            foreach (var pz in CurrentPzList) {

                if (pz.ZugeordneteKostenstellen == null || !pz.ZugeordneteKostenstellen.IsChanged) continue;
                result.Map.AddRange(pz.ZugeordneteKostenstellen.AddedItems().Select(kst => new KstPzMap { ChangeStatus = ChangeStatus.Added, Standort_Id = pz.Id, Kostenstelle_Id = kst.Id }));
                result.Map.AddRange(pz.ZugeordneteKostenstellen.DeletedItems().Select(kst => new KstPzMap { ChangeStatus = ChangeStatus.Deleted, Standort_Id = pz.Id, Kostenstelle_Id = kst.Id }));
            }

            await store.PostAsync<KstPzDto>(uri, result, ct);
            CurrentPzList.AcceptChanges();
            CurrentPzList.Where(p => p.ZugeordneteKostenstellen != null).ForEach(p => p.ZugeordneteKostenstellen.AcceptChanges());
        }


        public void Reset()
        {

            CurrentPzList = null;
            currentKstList = null;
        }
    }
}
