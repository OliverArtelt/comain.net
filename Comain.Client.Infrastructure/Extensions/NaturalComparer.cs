﻿using System;
using System.Collections.Generic;


namespace Comain
{

    public class NaturalComparer : IComparer<String>
    {
        
        public int Compare(string x, string y)
        {
            return x.CompareNatural(y);
        }
    }
}
