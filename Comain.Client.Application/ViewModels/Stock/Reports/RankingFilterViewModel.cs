﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Stock.Reports;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.ViewModels.Stock.Reports
{

    public class RankingFilterViewModel : ViewModel<IRankingFilterView>, IFilterViewModel
    {

        public RankingFilterViewModel(IRankingFilterView view) : base(view)
        {}


        public void Reset()
        {
          
            Rank0 = false;
            Rank1 = false;
            Rank2 = false;
            Rank3 = false;
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {
            
            if (Rank0 || Rank1 || Rank2 || Rank3)
                return "Rank " + RankAsStringList;
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {
            if (!String.IsNullOrWhiteSpace(RankAsStringList)) filter.Add("Rank", RankAsStringList);
        }


        private String RankAsStringList
        {
            
            get {

                String s = String.Empty;
                if (Rank0) s += "keiner,";
                if (Rank1) s += "I,";
                if (Rank2) s += "II,";
                if (Rank3) s += "III,";
                return s.TrimEnd(new char[] { ',' }); 
            }
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S
   

        private bool rank0;
        public bool Rank0
        { 
            get { return rank0; }
            set { SetProperty(ref rank0, value); }
        } 
   

        private bool rank1;
        public bool Rank1
        { 
            get { return rank1; }
            set { SetProperty(ref rank1, value); }
        } 
   

        private bool rank2;
        public bool Rank2
        { 
            get { return rank2; }
            set { SetProperty(ref rank2, value); }
        } 
   

        private bool rank3;
        public bool Rank3
        { 
            get { return rank3; }
            set { SetProperty(ref rank3, value); }
        } 


#endregion

    }
}
