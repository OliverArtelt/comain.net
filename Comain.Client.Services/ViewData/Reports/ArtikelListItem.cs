﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.ViewData.Reports
{
   
    public class ArtikelListItem
    {

        public enum SortStatus { Fehlt, Bestellt, Vorhanden, Frei, Deaktiviert }

       
        public int          Id                  { get; set; }
        public String       Nummer              { get; set; }
        public String       Name                { get; set; }
        public String       Warengruppe         { get; set; }
        public String       IHObjekte           { get; set; }
        public String       Lieferanten         { get; set; }
        public decimal?     Mindestbestand      { get; set; }
        public decimal?     Mindestbestellmenge { get; set; }
        public decimal?     Bestellmenge        { get; set; }
        public decimal?     BestandReal         { get; set; }
        public decimal?     Mischpreis          { get; set; }
        public DateTime?    DeaktiviertAm       { get; set; }

        
        public SortStatus Status 
        { 
            get { 

                if (DeaktiviertAm.HasValue) return SortStatus.Deaktiviert;
                if (Mindestbestand.GetValueOrDefault() == 0) return SortStatus.Frei;
                if (BestandReal.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return SortStatus.Vorhanden;
                if (BestandReal.GetValueOrDefault() + Bestellmenge.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return SortStatus.Bestellt;
                return SortStatus.Fehlt;
            } 
        }
       
       
        public String Color 
        { 
            get { 

                if (DeaktiviertAm.HasValue) return "CornflowerBlue";
                if (BestandReal.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return "LimeGreen";
                if (BestandReal.GetValueOrDefault() + Bestellmenge.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return "Gold";
                if (Mindestbestand.GetValueOrDefault() > 0) return "Coral";
                return "LightSlateGray";
            } 
        }
    }
}
