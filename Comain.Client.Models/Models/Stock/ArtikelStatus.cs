﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Stock
{

    public enum ArtikelStatus
    {

        /// <summary>
        /// Artikel ist nicht ausreichend vorhanden bzw. nicht in ausreichender Menge bestellt
        /// </summary>
        Fehlt = 0,
        /// <summary>
        /// fehlende Menge wurde bereits bestellt
        /// </summary>
        Bestellt,
        /// <summary>
        /// Artikel ist ausreichend vorhanden
        /// </summary>
        Vorhanden,
        /// <summary>
        /// Artikel wird nicht geführt (Mindestmenge = 0), d.h. er muss manuell bestellt werden
        /// </summary>
        Frei
    }
}
