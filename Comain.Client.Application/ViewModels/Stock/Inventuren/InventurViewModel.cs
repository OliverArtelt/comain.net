﻿using System;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Inventuren;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Inventuren
{

    public class InventurViewModel : ViewModel<IInventurView>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Druck } 

 
        private readonly SynchronizationContext context;


        public InventurViewModel(IInventurView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public Tabs Tab { get { return (Tabs)currentTab; } }


#region P R O P E R T I E S


        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { SetProperty(ref currentTab, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
                 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
 

        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }
 

        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object reportView;
        public object ReportView
        { 
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


#endregion

    }
}
