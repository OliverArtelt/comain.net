﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class TafelDetailPersonalViewModel : ViewModel<ITafelDetailPersonalView>
    {

        private const int maxShown = 50;
        private readonly SynchronizationContext context;
        private Dictionary<int, PersonalFilterModel> psdic;


        public TafelDetailPersonalViewModel(ITafelDetailPersonalView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }


        public void Unload()
        {

            SelektiertePositionen = null;
            PsSearchProvider.ClearItems();
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist)
        {

            psdic = pslist.ToDictionary(p => p.Id);
            context.Send(new SendOrPostCallback((o) => { 
            
                PsSearchProvider.SetItems(pslist);
            }), null);
        }


        public void SetPositionen(IEnumerable<Planaufgabe> positionen)
        {

            SelektiertePositionen = null;
            SelektierterText = null;


            if (positionen.IsNullOrEmpty()) {
            
                SelektierterText = "Keine Position gewählt.";
                return;
            }

            var amt = positionen.Count(p => p.IstBearbeitbar);
            SelektiertePositionen = positionen.Where(p => p.IstBearbeitbar).Take(maxShown).ToList();
            if (SelektiertePositionen.IsNullOrEmpty()) {
            
                SelektierterText = "Keine offene Position gewählt.";
                return;
            }


            if (SelektiertePositionen.Count == 1) SelektierterText = "Eine offene Position ist ausgewählt.";
            else if (amt <= maxShown) SelektierterText = $"{amt} offene Positionen sind ausgewählt.";
            else SelektierterText = $"{amt} offene Positionen sind ausgewählt, die ersten {maxShown} werden angezeigt.";
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private List<Planaufgabe> selektiertePositionen;
        public List<Planaufgabe> SelektiertePositionen
        {
            get { return selektiertePositionen; }
            set { SetProperty(ref selektiertePositionen, value); }
        }


        private String selectText;
        public String SelektierterText
        {
            get { return selectText; }
            set { SetProperty(ref selectText, value); }
        }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get { return personal; }
            set { SetProperty(ref personal, value); }
        }


        private ICommand personalCommand;
        public ICommand PersonalCommand
        {
            get { return personalCommand; }
            set { SetProperty(ref personalCommand, value); }
        }


#endregion

    }
}
