﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Aufträge
{
    public class AuftragSelektor : NotifyBase
    {

        public int              Id                      { get; set; }
        public Auftragstyp      Typ                     { get; set; }
        public Auftragstatus    Status                  { get; set; }
        public String           Nummer                  { get; set; }
        public String           Kurzbeschreibung        { get; set; }
        public DateTime         Auftragsdatum           { get; set; }
        public DateTime?        Termin                  { get; set; }
        public DateTime?        ÜbergebenAm             { get; set; }


        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value); 
        }


        public String StatusAsText  => Status == Auftragstatus.Offen && ÜbergebenAm.HasValue? "Übergeben": Status.ToString();


        public void Select(bool isSelected) => IsSelected = isSelected;


        public void SelectStandard(bool isSelected) 
        { 
            if (Typ == Auftragstyp.Standardauftrag || Typ == Auftragstyp.FremdBudget) IsSelected = isSelected; 
        }


        public void SelectPlanung(bool isSelected) 
        { 
            if (Typ == Auftragstyp.Wartungsplan) IsSelected = isSelected; 
        }


        public void SelectTicket(bool isSelected) 
        { 
            if (Typ == Auftragstyp.Ticket || Typ == Auftragstyp.BesonderesVorkommnis) IsSelected = isSelected; 
        }
    }
}
