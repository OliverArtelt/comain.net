﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Tickets;

namespace Comain.Client.Views.Tickets
{

    public partial class TicketListView:UserControl ,ITicketListView
    {
    
        public TicketListView()
        {
            InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as TicketListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
