﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{

    public class Leistungsart : Entity, IDataErrorInfo, IComparable<Leistungsart>, IEquatable<Leistungsart>
    {

        private String      nummer;
        private String      name;
        private bool        istProdKennziffer;
        private bool        istWI;
        private bool        istPlankosten;
        private bool        istUnPlankosten;
        private bool        istPräventation;
        private bool        istSachkundigenprüfung;


#region P R O P E R T I E S


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        public bool IstProdKennziffer
        {
            get { return istProdKennziffer; }
            set { SetProperty(ref istProdKennziffer, value); }
        }

        public bool IstWI
        {
            get { return istWI; }
            set { SetProperty(ref istWI, value); }
        }

        public bool IstPlankosten
        {
            get { return istPlankosten; }
            set { SetProperty(ref istPlankosten, value); }
        }

        public bool IstUnPlankosten
        {
            get { return istUnPlankosten; }
            set { SetProperty(ref istUnPlankosten, value); }
        }

        public bool IstPräventation
        {
            get { return istPräventation; }
            set { SetProperty(ref istPräventation, value); }
        }

        public bool IstSachkundigenprüfung
        {
            get { return istSachkundigenprüfung; }
            set { SetProperty(ref istSachkundigenprüfung, value); }
        }


#endregion


        public bool IstPlanmäßigOderUnplanmäßig
        {
            get { return istPlankosten || istUnPlankosten; }
        }


        public int CompareTo(Leistungsart other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


        public bool Equals(Leistungsart other)
        {

            if (other == null) return false;
            return this.Id.Equals(other.Id);
        }


        public override bool Equals(object obj)
        {
            return Equals(obj as Leistungsart);
        }


        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Name"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {


                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer der Leistungsart an.";
                    if (Nummer.Length > 10) return "Die Nummer der Leistungsart darf aus maximal 10 Zeichen bestehen.";
                    break;

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung der Leistungsart an.";
                    if (Name.Length > 150) return "Die Nummer der Leistungsart darf aus maximal 150 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion
    }
}
