﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.Views.Stock.Wareneinlagerungen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{
    
    public class EinlagerViewModel : ViewModel<IEinlagerView>, IDelayableViewModel
    {
 
        public EinlagerViewModel(IEinlagerView view) : base(view)
        {
        }


#region P R O P E R T I E S
 

        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }
 

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
                 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
 

        private ICommand addFromEingangCommand;
        public ICommand AddFromEingangCommand
        { 
            get { return addFromEingangCommand; }
            set { SetProperty(ref addFromEingangCommand, value); }
        }
 

        private ICommand addFromArtikelCommand;
        public ICommand AddFromArtikelCommand
        { 
            get { return addFromArtikelCommand; }
            set { SetProperty(ref addFromArtikelCommand, value); }
        }
 

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }
 

        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }
 

        private ICommand closeCommand;
        public ICommand CloseCommand
        { 
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }


#endregion

    }
}
