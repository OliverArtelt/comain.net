﻿using System;
using System.Diagnostics;


namespace Comain.Client.Dtos.Reports
{

    [DebuggerDisplay("{Auftragsnummer}")]
    public class Top10Dto
    {

        public String      HauptIHNummer            { get; set; }
        public String      HauptIHName              { get; set; }
        public String      IHNummer                 { get; set; }
        public String      IHName                   { get; set; }
        public int         AuftragsId               { get; set; }
        public int         Anzahl                   { get; set; }
        public int?        Minuten                  { get; set; }
        public String      Auftragsnummer           { get; set; }
        public String      Kurzbeschreibung         { get; set; }
        public String      Aktivitätenbeschreibung  { get; set; }
        public String      Kostenstelle             { get; set; }
        public String      SB                       { get; set; }
        public String      SSB                      { get; set; }
        public String      SU                       { get; set; }
        public String      SSU                      { get; set; }
        public decimal?    Personalkosten           { get; set; }
        public decimal?    Materialkosten           { get; set; }
        public decimal?    Fremdkosten              { get; set; }

        public decimal     Gesamtkosten             { get { return Personalkosten.GetValueOrDefault() + Materialkosten.GetValueOrDefault() + Fremdkosten.GetValueOrDefault(); } } 
    }
}
