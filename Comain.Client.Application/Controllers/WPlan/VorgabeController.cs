﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.Dokumente;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Services;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Services;
using Comain.Client.Models.WPlan;
using Comain.Client.Services.WPlan;
using Comain.Client.ViewModels.WPlan.Vorgaben;
using Microsoft.Win32;


namespace Comain.Client.Controllers.WPlan
{

    public interface IVorgabeController : IController 
    {}


    public class VorgabeController : ControllerBase, IVorgabeController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IFlyoutService flyouts;
        private readonly ILoginService login;
        private readonly IVorgabeService store;
        private readonly IModulService modules;
        private readonly MainViewModel mainView;
        private readonly VorgabeViewModel view;
        private readonly VorgabeListViewModel listView;
        private readonly VorgabeFilterViewModel filterView;
        private readonly VorgabeDetailViewModel detailView;
        private readonly VorgabeDetailMemoViewModel memoView;
        private readonly VorgabeDetailMediaViewModel mediaView;
        private readonly VorgabeDetailMaterialViewModel materialView;
        private readonly VorgabeDetailBaseViewModel baseView;
        private readonly VorgabeDetailAuftragViewModel auftragView;
        private readonly CameraViewModel cameraView;
        private readonly ImageEditViewModel editView;
        private EditMode currentMode;
        private IHObjekt ihroot;
        private List<IHObjektFilterModel> ihlist;
        SerializableCommand deleteCommand;
        SerializableCommand deleteMaterialCommand;
        SerializableCommand addMaterialCommand;
        SerializableCommand imageFromCameraCommand;
        SerializableCommand imageFromFileCommand;
        SerializableCommand editImageCommand;
        SerializableCommand saveImageCommand;
        SerializableCommand deleteImageCommand;
        SerializableCommand deleteImagesCommand;
        SerializableCommand openAuftragCommand;
        private IDokumentAssignController docs;


        public VorgabeController(IBreadcrumbService crumbs, IMessageService messages, ILoginService login, IFlyoutService flyouts,
                                 IControllerBus bus, IVorgabeService store, IModulService modules,
                                 MainViewModel mainView, VorgabeViewModel view, VorgabeListViewModel listView, VorgabeFilterViewModel filterView,
                                 VorgabeDetailViewModel detailView, VorgabeDetailMemoViewModel memoView, VorgabeDetailAuftragViewModel auftragView,
                                 VorgabeDetailMediaViewModel mediaView, VorgabeDetailMaterialViewModel materialView, VorgabeDetailBaseViewModel baseView,
                                 CameraViewModel cameraView, ImageEditViewModel editView)

          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.login = login;
            this.flyouts = flyouts;
            this.store = store;
            this.mainView = mainView;
            this.view = view;
            this.listView = listView;
            this.filterView = filterView;
            this.detailView = detailView;
            this.memoView = memoView;
            this.mediaView = mediaView;
            this.materialView = materialView;
            this.baseView = baseView;
            this.cameraView = cameraView;
            this.auftragView = auftragView;
            this.editView = editView;
            this.modules = modules;
        }


        public void Initialize()
        {

            docs = bus.GetController<IMNDokumentController>();

            view.DetailView = detailView.View;
            view.ListView = listView.View;
            view.FilterView = filterView.View;
            detailView.BaseView = baseView.View;
            detailView.MemoView = memoView.View;
            detailView.MaterialView = materialView.View;
            detailView.MediaView = mediaView.View;
            detailView.AuftragView = auftragView.View;
            detailView.DokumentView = docs?.View;

            view.SaveCommand            = CreateCommand(_ => SaveAsync());
            view.EditCommand            = CreateCommand(_ => EditAsync());
            view.CancelCommand          = CreateCommand(_ => VerwerfenAsync());
            view.ReloadCommand          = CreateCommand(_ => LoadAsync());
            view.AddCommand             = CreateCommand(_ => AddAsync());
            view.DeleteCommand          = deleteCommand = CreateCommand(_ => DeleteAsync(), p => detailView.Current != null);
            view.AddMaterialCommand     = addMaterialCommand = CreateCommand(_ => AddMaterialAsync(), p => currentMode != EditMode.ReadOnly);
            view.DeleteMaterialCommand  = deleteMaterialCommand = CreateCommand(_ => DeleteMaterialAsync(), p => currentMode != EditMode.ReadOnly && materialView.Current != null);
            view.ImageFromCameraCommand = imageFromCameraCommand = CreateCommand(_ => ImageFromCameraAsync(), p => currentMode != EditMode.ReadOnly);
            view.ImageFromFileCommand   = imageFromFileCommand = CreateCommand(_ => ImageFromFileAsync(), p => currentMode != EditMode.ReadOnly);
            view.EditImageCommand       = editImageCommand = CreateCommand(_ => EditImageAsync(), p => currentMode != EditMode.ReadOnly && mediaView.CurrentMedia != null);
            view.SaveImageCommand       = saveImageCommand = CreateCommand(_ => SaveImageAsync(), p => mediaView.CurrentMedia != null);
            view.DeleteImageCommand     = deleteImageCommand = CreateCommand(_ => DeleteImageAsync(), p => currentMode != EditMode.ReadOnly && mediaView.CurrentMedia != null);
            view.DeleteImagesCommand    = deleteImagesCommand = CreateCommand(_ => DeleteImagesAsync(), p => currentMode != EditMode.ReadOnly);
            view.BackCommand            = new AsyncDelegateCommand(_ => BackAsync());
            listView.DetailCommand      = CreateCommand(_ => DetailAsync());
            editView.BackCommand        = new AsyncDelegateCommand(_ => BackAsync());
            editView.SaveCommand        = CreateCommand(_ => TakeEditedImageAsync());
            cameraView.CompletedCommand = CreateCommand(p => SnapshotDoneAsync(p));
            filterView.SearchCommand    = CreateCommand(_ => SearchAsync());
            mediaView.EditImageCommand  = editImageCommand;
            view.OpenAuftragCommand     = openAuftragCommand = CreateCommand(_ => OpenAuftragAsync(), _ => auftragView.Ereignis != null && currentMode == EditMode.ReadOnly);
            auftragView.OpenAuftragCommand = openAuftragCommand;

            PropertyChangedEventManager.AddHandler(auftragView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(listView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(mediaView, ViewPropertyChanged, "");
            PropertyChangedEventManager.AddHandler(materialView, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            //nicht direkt als Kachel im Hauptmenü aufrufbar
            if (!(parameter is ControllerMessage)) return;

            //erstmaliger Aufruf von MassnahmenController
            var c = parameter as IHObjektControllerMessage;
            //vom Rückruf aus dem AuftragController unterscheiden
            if (c != null) {

                ihroot = c.IHObjekt;
                detailView.IHObjekt = ihroot;
                crumbs.Add("Maßnahmen");
            }

            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            SetEditMode(EditMode.ReadOnly);
            detailView.Tab = VorgabeDetailViewModel.Tabs.Basis;
            UpdateCommands();
            await ExecuteWithLockAsync(_ => LoadAsync());

            if (c == null) return;

            if (c.Massnahme_Id.HasValue && !listView.CurrentList.IsNullOrEmpty()) {

                listView.Current = listView.CurrentList.FirstOrDefault(p => p.Id == c.Massnahme_Id.Value);
                await ExecuteWithLockAsync(_ => DetailAsync());

            } else {

                await ExecuteWithLockAsync(_ => AddAsync());
            }

            await docs.RunAsync(parameter);
        }


        public void Shutdown()
        {

            PropertyChangedEventManager.RemoveHandler(materialView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(mediaView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(detailView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(listView, ViewPropertyChanged, "");
            PropertyChangedEventManager.RemoveHandler(auftragView, ViewPropertyChanged, "");
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (args.PropertyName == nameof(detailView.CurrentTab) ||
                args.PropertyName == nameof(listView.Current) ||
                args.PropertyName == nameof(mediaView.CurrentMedia)) UpdateCommands();
            if (sender == materialView && args.PropertyName == nameof(materialView.Current)) UpdateCommands();
            if (sender == auftragView && args.PropertyName == nameof(auftragView.Ereignis)) UpdateCommands();
        }


        private async Task BackAsync()
        {

            if (mainView.PageView == editView.View) {

                mainView.PageView = view.View;
                return;
            }

            await bus.BackAsync();
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (mainView.PageView == cameraView.View) {

                cameraView.Unload();
                mainView.PageView = view.View;
            }

            if ((detailView.Current != null && (detailView.Current.IsChanged || detailView.Current.DocumentsChanged)) &&
                !await messages.ConfirmExitAsync()) return false;

            await SetModelAsync(null);
            view.Unload();
            listView.Unload();
            filterView.Unload();
            detailView.Unload();
            memoView.Unload();
            mediaView.Unload();
            materialView.Unload();
            baseView.Unload();
            cameraView.Unload();
            editView.Unload();
            auftragView.Unload();
            await docs.UnloadAsync();

            return true;
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                var latask = store.LeistungsartenAsync(bus.Token);
                var gwtask = store.GewerkeAsync(bus.Token);
                var ihtask = store.IHObjekteAsync(bus.Token);
                var ehtask = store.EinheitenAsync(bus.Token);
                var mttask = store.MaterialstammAsync(bus.Token);

                await Task.WhenAll(latask, ihtask, gwtask, ehtask, mttask).ConfigureAwait(false);

                ihlist = ihtask.Result.Where(p => p.Nummer.StartsWith(ihroot.Nummer)).OrderBy(p => p.Nummer).ToList();
                if (ihlist.Count == 0) throw new ComainOperationException("Das Asset wurde nicht gefunden.");

                bus.Token.ThrowIfCancellationRequested();

                await Task.WhenAll(materialView.SetzeListenAsync(ehtask.Result, mttask.Result),
                                   baseView.SetzeListenAsync(ihlist, gwtask.Result, latask.Result)).ConfigureAwait(false);
                filterView.Clear();
                await SearchAsync();
                await docs.LoadAsync();
            }

            UpdateCommands();
        }


        private async Task SaveAsync()
        {

            var lvi = listView.Current;

            if (detailView.Current == null) return;
            if (!detailView.Current.Errorlist.IsNullOrEmpty()) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Maßnahme kann nicht gespeichert werden.", Reasons = detailView.Current.Errorlist });
                return;
            }

            using (new VisualDelay(view)) {

                var savedModel = await store.UpdateAsync(detailView.Current, bus.Token);
                await SetModelAsync(savedModel);

                if (currentMode == EditMode.Append) {

                    await OnUIThreadAsync(() => {

                        lvi = new MassnahmeListViewItem(savedModel);
                        listView.CurrentList.Add(lvi);
                        listView.Current = lvi;

                    });
                }

                docs.UpdateEntityId(savedModel);
                await docs.SaveAsync();

                if (currentMode == EditMode.Edit) lvi.Model = savedModel;
                SetEditMode(EditMode.ReadOnly);
                UpdateCommands();
            }
        }


        private async Task SearchAsync()
        {

            int? lastId = listView.Current?.Id;

            var dtolist = await store.ListAsync(ihroot.Id, bus.Token).ConfigureAwait(false);
            var modellist = new ObservableCollection<MassnahmeListViewItem>(dtolist.Select(p => new MassnahmeListViewItem(p)).OrderBy(p => p.Name).ToList());
            await listView.LoadAsync(modellist).ConfigureAwait(false);

            if (lastId.HasValue) {

                listView.Current = modellist.FirstOrDefault(p => p.Id == lastId.Value);
                await DetailAsync();
            }
        }


        private async Task DetailAsync()
        {

            if (listView.Current == null) {

                await SetModelAsync(null);
                return;
            }

            var current = listView.Current;
            auftragView.Unload();

            using (new VisualDelay(auftragView)) {
                using (new VisualDelay(detailView)) {

                    current.Model = await store.DetailAsync(current.Id, bus.Token);
                    await SetModelAsync(current.Model).ConfigureAwait(false);
                }

                bus.Token.ThrowIfCancellationRequested();

                var ereignisse = await store.EreignisseAsync(current.Id, bus.Token);
                await auftragView.LoadAsync(ereignisse);
                await docs.DetailAsync(current.Model);
            }
        }


        private Task EditAsync()
        {

            if (detailView.Current != null) {

                SetEditMode(EditMode.Edit);
                UpdateCommands();
            }
            return Task.FromResult(true);
        }


        private async Task AddAsync()
        {

            var model = new WiMassnahme { IHObjekt = ihlist.First(), Start = DateTime.Today };
            model.AcceptChanges();

            await SetModelAsync(model);
            auftragView.Unload();
            SetEditMode(EditMode.Append);
            await docs.DetailAsync(model);
            UpdateCommands();
        }


        private async Task DeleteAsync()
        {

            if (detailView.Current == null || listView.CurrentList == null) return;
            bool hatAufträge = !auftragView.Ereignisse.IsNullOrEmpty();


            int clcnt = hatAufträge? auftragView.Ereignisse.Count(p => !p.IstOffen): 0;
            if (clcnt > 0) {

                var result = new List<String>();
                if (clcnt == 1) result.Add("Es wurde bereits eine Checklistposition rückgemeldet.");
                else            result.Add($"Es wurden bereits {clcnt} Checklistpositionen rückgemeldet.");
                result.Add("Deaktivieren Sie ge­ge­be­nen­falls die Maßnahme.");

                await messages.ShowMessageAsync(new MessageToken { Header = "Die Maßnahme darf nicht gelöscht werden.", Reasons = result });
                return;
            }


            int cpcnt = hatAufträge? auftragView.Ereignisse.Count: 0;
            int aucnt = hatAufträge? auftragView.Ereignisse.Count(p => p.AuftragIstStornierbar): 0;

            var reasons = new List<String>();

            if (cpcnt == 1) reasons.Add("Es wird eine Checklistposition mit entfernt.");
            else if (cpcnt > 1) reasons.Add($"Es werden {cpcnt} Checklistpositionen mit entfernt.");

            if (aucnt == 1) reasons.Add("Ein Auftrag wird storniert.");
            else if (aucnt > 1) reasons.Add($"{aucnt} Aufträge werden storniert.");

            if (cpcnt + aucnt > 0) reasons.Add("Sie müssen die Wartungsaufträge erneut planen damit die Änderungen wirksam werden.");

            if (!await messages.ConfirmDeleteAsync(new DeleteToken { IsDeleteable = true, Header = "Wollen Sie die Maßnahme wirklich löschen?", Reasons = reasons })) return;


            await store.DeleteAsync(detailView.Current.Id, bus.Token).ConfigureAwait(false);
            await SetModelAsync(null);
            await LoadAsync();
        }


        private async Task VerwerfenAsync()
        {

            await DetailAsync();
            SetEditMode(EditMode.ReadOnly);
            UpdateCommands();
        }


        private async Task AddMaterialAsync()
        {

            if (detailView.Current == null) return;

            var model = new WiMaterialVorgabe { Menge = 1 };
            await context.SendAsync(new SendOrPostCallback((o) => { detailView.Current.Materialien.Add(model); }), null);
            materialView.Current = model;

            UpdateCommands();
        }


        private async Task DeleteMaterialAsync()
        {

            if (detailView.Current == null) return;
            if (materialView.Current == null) return;
            if (detailView.Current.Materialien.IsNullOrEmpty()) return;

            await context.SendAsync(new SendOrPostCallback((o) => { detailView.Current.Materialien.Remove(materialView.Current); }), null);

            UpdateCommands();
        }


        private async Task ImageFromCameraAsync()
        {

            if (mediaView.Current == null) return;

            try {

                mainView.PageView = cameraView.View;
                await cameraView.StartAsync();
            }
            catch (Exception) {

                mainView.PageView = view.View;
                throw;
            }

            UpdateCommands();
        }


        private async Task SnapshotDoneAsync(object image)
        {

            try {

                mainView.PageView = view.View;
                var bitmap = image as Bitmap;
                if (bitmap == null) return;
                var model = new Media(bitmap);

                await context.SendAsync(new SendOrPostCallback((o) => {

                    mediaView.Medien.Add(model);

                }), null);
            }
            catch (Exception x) {

                messages.Show(x);
            }

            UpdateCommands();
        }


        private async Task ImageFromFileAsync()
        {

            if (mediaView.Current == null) return;

            var dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Title = "Bilder wählen";

			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileNames.IsNullOrEmpty()) return;

            using (new VisualDelay(view)) {

                var medien = await store.ReadFilesAsync(dialog.FileNames, bus.Token).ConfigureAwait(false);

                await context.SendAsync(new SendOrPostCallback((o) => {

                    mediaView.Current.Medien.AddRange(medien);
                }), null);
            }

            UpdateCommands();
        }


        private async Task EditImageAsync()
        {


            if (mediaView.CurrentMedia == null) return;

            await editView.SetImageAsync(mediaView.CurrentMedia.Image);
            mainView.PageView = editView.View;

            UpdateCommands();
        }


        private async Task TakeEditedImageAsync()
        {

            if (mediaView.CurrentMedia == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                using (new VisualDelay(view)) {

                    var editedImage = editView.ExportImage();
                    if (editedImage == null) return;

                    mediaView.CurrentMedia.LoadFromBitmap(editedImage);
                    mainView.PageView = view.View;
                }

            }), null);
        }


        private async Task SaveImageAsync()
        {

            if (mediaView.CurrentMedia == null) return;
            var filename = mediaView.CurrentMedia.Dateiname ?? "Bild.jpg";
            filename = Path.ChangeExtension(filename, "jpg");

            var dialog = new SaveFileDialog();
            dialog.Filter = "JPEG file (*.jpg)|*.jpg";
            dialog.DefaultExt = "jpg";
            dialog.FileName = filename;
            dialog.Title = "Speicherort wählen";
			if (!dialog.ShowDialog().GetValueOrDefault() || dialog.FileName.IsNullOrEmpty()) return;

            await mediaView.CurrentMedia.StoreFileAsync(dialog.FileName, bus.Token).ConfigureAwait(false);
        }


        private async Task DeleteImageAsync()
        {

            if (mediaView.CurrentMedia == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                mediaView.Current.Medien.Remove(mediaView.CurrentMedia);
            }), null);

            UpdateCommands();
        }


        private async Task DeleteImagesAsync()
        {

            if (mediaView.Current == null) return;

            await context.SendAsync(new SendOrPostCallback((o) => {

                mediaView.Current.Medien.Clear();
            }), null);

            UpdateCommands();
        }


        private Task SetModelAsync(WiMassnahme model)
        {

            return context.SendAsync(new SendOrPostCallback((o) => {

                detailView.Current = model;
                baseView.Current = model;
                materialView.MaterialVorgaben = model?.Materialien;
                materialView.Current = null;  // Material, nicht Vorgabe
                if (!materialView.MaterialVorgaben.IsNullOrEmpty() && materialView.MaterialVorgaben.Count == 1)
                    materialView.Current = materialView.MaterialVorgaben.First();
                mediaView.Current = model;
                memoView.Current = model;
            }), null);
        }


        public void SetEditMode(EditMode newMode)
        {

            currentMode = newMode;
            docs.SetEditMode(newMode);
        }


        private void UpdateCommands()
        {

            filterView.EditEnabled   = currentMode == EditMode.ReadOnly;
            listView.EditEnabled     = filterView.EditEnabled;
            baseView.EditEnabled     = !filterView.EditEnabled;
            memoView.EditEnabled     = !filterView.EditEnabled;
            mediaView.EditEnabled    = !filterView.EditEnabled;
            materialView.EditEnabled = !filterView.EditEnabled && materialView.Current != null;
            detailView.EditEnabled   = !filterView.EditEnabled;

            view.SaveVisible         = currentMode != EditMode.ReadOnly;
            view.EditVisible         = detailView.Current != null && currentMode == EditMode.ReadOnly;
            view.CancelVisible       = currentMode != EditMode.ReadOnly;
            view.ReloadVisible       = currentMode == EditMode.ReadOnly;
            view.AddVisible          = currentMode == EditMode.ReadOnly && detailView.Tab == VorgabeDetailViewModel.Tabs.Basis;
            view.DeleteVisible       = detailView.Current != null && detailView.Tab == VorgabeDetailViewModel.Tabs.Basis && currentMode == EditMode.ReadOnly;
            view.MaterialSelected    = detailView.Current != null && detailView.Tab == VorgabeDetailViewModel.Tabs.Material;
            view.MediaSelected       = detailView.Current != null && detailView.Tab == VorgabeDetailViewModel.Tabs.Media;
            view.OpenAuftragVisible  = detailView.Tab == VorgabeDetailViewModel.Tabs.Auftrag;

            context.Send(new SendOrPostCallback((o) => {

                deleteCommand.RaiseCanExecuteChanged();
                deleteMaterialCommand.RaiseCanExecuteChanged();
                imageFromCameraCommand.RaiseCanExecuteChanged();
                imageFromFileCommand.RaiseCanExecuteChanged();
                editImageCommand.RaiseCanExecuteChanged();
                saveImageCommand.RaiseCanExecuteChanged();
                deleteImageCommand.RaiseCanExecuteChanged();
                deleteImagesCommand.RaiseCanExecuteChanged();
                addMaterialCommand.RaiseCanExecuteChanged();
                openAuftragCommand.RaiseCanExecuteChanged();

            }), null);
        }


        private async Task OpenAuftragAsync()
        {

            if (auftragView.Ereignis == null) return;
            if (currentMode != EditMode.ReadOnly) return;
            if (!modules.Info.VerwendetModulAuftrag) return;

            await bus.JumpAsync(new ControllerMessage { Destination = typeof(IAuftragController), Source = this, Entity_Id = auftragView.Ereignis.AuftragId }).ConfigureAwait(false);
        }
    }
}
