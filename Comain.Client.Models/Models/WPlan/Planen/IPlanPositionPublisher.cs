﻿using System.Drawing;

namespace Comain.Client.WPlan.Models.Planen
{

    public interface IPlanPositionPublisher
    {

        double Height       { get; }
        double Width        { get; }
        double Left         { get; }
        double Top          { get; }
        Brush  TypAsFarbe   { get; }

        void NotifyPositionChanged();
    }
}
