﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Einheiten : IVwTyp
    {

        public string       Title           => "Einheiten";
        public int          Sortierung      => 2300;
        public Type         Controller      => typeof(IEinheitController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung || nutzer.IstLagerAdministrator;    
        }
    }
}
