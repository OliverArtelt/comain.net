﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.Tickets;

namespace Comain.Client.Models.Tickets
{

    public class BevorTicket : Entity, IDataErrorInfo
    {

        private AuIHKSDto                   iHObjekt;              
        private DateTime?                   datum;                 
        private TimeSpan?                   uhrzeit;               
        private String                      kurzbeschreibung;      
        private Dringlichkeit               dringlichkeit;     
        private TrackableCollection<Media>  medien;
        private PersonalFilterModel         auftragnehmer;


        public AuIHKSDto IHObjekt               
        { 
            get { return iHObjekt; } 
            set { SetProperty(ref iHObjekt, value); } 
        }
                      

        public PersonalFilterModel Auftragnehmer               
        { 
            get { return auftragnehmer; } 
            set { SetProperty(ref auftragnehmer, value); } 
        }
                      

        public DateTime? Datum                   
        { 
            get { return datum; } 
            set { SetProperty(ref datum, value); } 
        }
                      

        public TimeSpan? Uhrzeit                
        { 
            get { return uhrzeit; } 
            set { SetProperty(ref uhrzeit, value); } 
        }
                      

        public String Kurzbeschreibung      
        { 
            get { return kurzbeschreibung; } 
            set { SetProperty(ref kurzbeschreibung, value); } 
        }
                      

        public Dringlichkeit Dringlichkeit    
        { 
            get { return dringlichkeit; } 
            set { SetProperty(ref dringlichkeit, value); } 
        }

                                                                        
        public TrackableCollection<Media> Medien  
        { 
            get {
            
                if (medien == null) medien = new TrackableCollection<Media>();
                return medien;
            } 
            set { SetProperty(ref medien, value); } 
        }
                      

        public BevorTicket()
        {

            IHObjekt         = null;
            Kurzbeschreibung = null;
            Datum            = DateTime.Now.Date;
            Uhrzeit          = DateTime.Now.TimeOfDay;
            Medien           = new TrackableCollection<Media>();

            AcceptChanges();
        }
 

        public BevorTicketDto AsDto()
        {

            var dto = new BevorTicketDto();

            dto.IHObjekt = IHObjekt.Id; // wir übertragen AuIHKS
            dto.Datum = Datum + Uhrzeit;
            dto.Dringlichkeit_Id = Dringlichkeit.Id;
            dto.Kurzbeschreibung = Kurzbeschreibung;
            dto.Medien = Medien.Select(p => p.AsDto()).ToList();
            dto.Personal_Id = Auftragnehmer?.Id;

            return dto;
        }


        public override bool IsChanged 
        {
            get {

                if (base.IsChanged) return true;
                if (IstAngefügt && !Medien.IsNullOrEmpty()) return true;
                return Medien.Any(p => p.IsChanged);
            }
        }


#region V A L I D A T I O N 
        

        public virtual IList<String> ErrorList
        {
            
            get { 
            
                var result = new List<String>();
                
                String test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Dringlichkeit"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Uhrzeit"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string Error
        {
            
            get { 
            
                String test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Dringlichkeit"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Datum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Uhrzeit"];
                return test;
            }
        }

        
        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "AuIHKS":

                    if (IHObjekt == null) return "Geben Sie den Ort an.";
                    break;

                case "Dringlichkeit":

                    if (Dringlichkeit == null) return "Wählen Sie eine Dringlichkeit."; 
                    break;

                case "Kurzbeschreibung":

                    if (String.IsNullOrWhiteSpace(Kurzbeschreibung)) return "Beschreiben Sie möglichst präzise das vorliegende Problem, damit sich der Instandhalter auf die Bearbeitung vorbereiten kann."; 
                    break;

                case "Datum":

                    if (!Datum.HasValue) return "Geben Sie das Datum an."; 
                    break;

                case "Uhrzeit":

                    if (!Uhrzeit.HasValue) return "Geben Sie die Uhrzeit an."; 
                    break;
                }

                return null;
            }
        }


#endregion

    }
}

