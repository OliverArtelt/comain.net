﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Views.WPlan.Vorgaben;


namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeListViewModel : ViewModel<IVorgabeListView>
    {
 
        private readonly SynchronizationContext context;


        public VorgabeListViewModel(IVorgabeListView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {

            Foundlist = null;
            CurrentList = null;
            Current = null;
        }


        public async Task LoadAsync(ObservableCollection<MassnahmeListViewItem> list)
        {      

            await context.SendAsync(new SendOrPostCallback((o) => {
                
                if (list == null) return;
                CurrentList = list;
                RaisePropertyChanged(nameof(CurrentList));
            
            }), null);
        }


        private bool FilterPredicate(object arg)
        {

            var mn = arg as MassnahmeListViewItem;
            if (mn == null) return false;
            return !NurAktive || mn.IstAktiv;
        }

       
        private void Refresh()
        {
            
            if (foundlist == null) return;
            context.Post(new SendOrPostCallback((o) => { foundlist?.Refresh(); }), null);
            RaisePropertyChanged(nameof(Foundlist));
        }


#region P R O P E R T I E S


        public ObservableCollection<MassnahmeListViewItem> CurrentList  { get; private set; }


        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool nurAktive;
        public bool NurAktive
        { 
            get { return nurAktive; }
            set { if (SetProperty(ref nurAktive, value)) Refresh(); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        { 
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private ICollectionView foundlist;
        public ICollectionView Foundlist
        { 
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private MassnahmeListViewItem current;
        public MassnahmeListViewItem Current
        { 
            get { return current; }
            set { SetProperty(ref current, value); }
        }
    

#endregion

    }
}
