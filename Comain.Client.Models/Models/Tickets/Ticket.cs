﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Dokumente;
using Comain.Client.Dtos.Tickets;

namespace Comain.Client.Models.Tickets
{

    public class Ticket : DocEntity, IDataErrorInfo
    {

        private Ticketstatus             status;
        private String                   nummer;
        private String                   kurzbeschreibung;
        private String                   aktivitätenbeschreibung;
        private DateTime                 gemeldetAm;
        private AuIHKSDto                auIHKS;
        private LeistungsartFilterModel  leistungsart;
        private Dringlichkeit            dringlichkeit;
        private NummerFilterModel        gewerk;
        private NummerFilterModel        schadensbild;
        private NummerFilterModel        schadensursache;
        private PersonalFilterModel      auftraggeber;
        private PersonalFilterModel      auftragnehmer;
        private PersonalFilterModel      auftragsleiter;
        private PersonalFilterModel      abnahmeberechtigter;
        private DateTime?                fertigstellungstermin;

        private DateTime?                leDatum;
        private TimeSpan?                leUhrzeitVon;
        private TimeSpan?                leUhrzeitBis;
        private String                   leAnmerkung;
        private String                   leMaterial;
        private bool                     leIstFertig;
        private bool                     leIstNichtFertig;

        private bool                     abnahmeAbgenommen;
        private bool                     abnahmeAbgelehnt;
        private String                   abnahmeBegründung;
        private bool                     istSauber;

        private ICollection<Personalleistung>   personalleistungen;
        private ICollection<Ticketprotokoll>    protokoll;


        //Infofelder die nicht im Client bearbeitet werden
        public DateTime?                 Beauftragungsdatum       { get; set; }
        public DateTime?                 ÜbergabeAm               { get; set; }
        public int?                      Störzeit                 { get; set; }
        public DateTime?                 Auftragsleiterdatum      { get; set; }
        public DateTime?                 Abnahmedatum             { get; set; }




        public override String EntityType => "AU";


        public TicketUpdateDto AsDto()
        {

            var dto = new TicketUpdateDto();

            dto.Id                      = Id;

            dto.AuIHKS_Id               = AuIHKS?.Id;
            dto.Dringlichkeit           = Dringlichkeit.Id;
            dto.Fertigstellungstermin   = Fertigstellungstermin;

            dto.Kurzbeschreibung        = Kurzbeschreibung;
            dto.Aktivitätenbeschreibung = Aktivitätenbeschreibung;
            dto.Schadensbild            = Schadensbild?.Id;
            dto.Schadensursache         = Schadensursache?.Id;

            dto.Gewerk                  = Gewerk?.Id;
            dto.Auftragnehmer           = Auftragnehmer?.Id;
            dto.Auftraggeber            = Auftraggeber?.Id;
            dto.Auftragsleiter          = Auftragsleiter?.Id;
            dto.Abnahmeberechtigter     = Abnahmeberechtigter?.Id;

            dto.LeDatum                 = LeDatum;
            dto.LeUhrzeitVon            = LeUhrzeitVon;
            dto.LeUhrzeitBis            = LeUhrzeitBis;
            dto.LeAnmerkung             = LeAnmerkung;
            dto.LeMaterial              = LeMaterial;
            dto.LeIstFertig             = LeIstFertig;
            dto.LeIstNichtFertig        = LeIstNichtFertig;
            dto.LeIstSauber             = IstSauber;

            dto.AbnahmeAbgenommen       = AbnahmeAbgenommen;
            dto.AbnahmeBegründung       = AbnahmeBegründung;
            dto.AbnahmeAbgelehnt        = AbnahmeAbgelehnt;

            return dto;
        }


        /// <remarks>
        /// Grund nur benennen wenn er noch aktuell ist
        /// </remarks>
        public String GrundDerAblehnung
        {
            get {

                if (status == Ticketstatus.Abgeschlossen) return null;
                if (status == Ticketstatus.Storniert) return null;
                if (status == Ticketstatus.Abgenommen) return null;
                if (Protokoll.IsNullOrEmpty()) return null;
                if (!Protokoll.Any(p => p.Status == Ticketstatus.AbnahmeAbgelehnt)) return null;

                var list = Protokoll.Reverse();
                var entry = list.First(p => p.Status == Ticketstatus.Abgeschlossen || p.Status == Ticketstatus.AbnahmeAbgelehnt || p.Status == Ticketstatus.Abgenommen ||
                                            p.Status == Ticketstatus.Storniert || p.Status == Ticketstatus.ÜbergabeZurAbnahme);
                if (entry.Status != Ticketstatus.AbnahmeAbgelehnt) return null;
                return entry.Bemerkungen;
            }
        }


        public bool IstAbgelehnt
        {
            get {

                if (status == Ticketstatus.Abgeschlossen) return false;
                if (status == Ticketstatus.Storniert) return false;
                if (status == Ticketstatus.Abgenommen) return false;
                if (Protokoll.IsNullOrEmpty()) return false;
                if (!Protokoll.Any(p => p.Status == Ticketstatus.AbnahmeAbgelehnt)) return false;

                var list = Protokoll.Reverse();
                var entry = list.First(p => p.Status == Ticketstatus.Abgeschlossen || p.Status == Ticketstatus.AbnahmeAbgelehnt || p.Status == Ticketstatus.Abgenommen ||
                                            p.Status == Ticketstatus.Storniert || p.Status == Ticketstatus.ÜbergabeZurAbnahme);
                if (entry.Status != Ticketstatus.AbnahmeAbgelehnt) return false;
                return true;
            }
        }


        public String TypAsString
        {
            get {

                if (Typ == Auftragstyp.Ticket) return "Ticket";
                if (Typ == Auftragstyp.BesonderesVorkommnis) return "Besonderes Vorkommnis";
                return "Auftrag";
            }
        }


#region P R O P E R T I E S


        public Auftragstatus   Auftragstatus   { get; set; }
        public Auftragstyp     Typ             { get; set; }


        public Dringlichkeit Dringlichkeit
        {
            get { return dringlichkeit; }
            set { SetProperty(ref dringlichkeit, value); }
        }


        public DateTime? LeDatum
        {
            get { return leDatum; }
            set { SetProperty(ref leDatum, value); }
        }


        public TimeSpan? LeUhrzeitVon
        {
            get { return leUhrzeitVon; }
            set { SetProperty(ref leUhrzeitVon, value); }
        }


        public TimeSpan? LeUhrzeitBis
        {
            get { return leUhrzeitBis; }
            set { SetProperty(ref leUhrzeitBis, value); }
        }


        public String LeAnmerkung
        {
            get { return leAnmerkung; }
            set { SetProperty(ref leAnmerkung, value); }
        }


        public String LeMaterial
        {
            get { return leMaterial; }
            set { SetProperty(ref leMaterial, value); }
        }


        public bool IstSauber
        {
            get { return istSauber; }
            set { SetProperty(ref istSauber, value); }
        }


        public bool LeIstFertig
        {
            get { return leIstFertig; }
            set { SetProperty(ref leIstFertig, value); }
        }


        public bool LeIstNichtFertig
        {
            get { return leIstNichtFertig; }
            set { SetProperty(ref leIstNichtFertig, value); }
        }


        public bool AbnahmeAbgenommen
        {
            get { return abnahmeAbgenommen; }
            set { SetProperty(ref abnahmeAbgenommen, value); }
        }


        public bool AbnahmeAbgelehnt
        {
            get { return abnahmeAbgelehnt; }
            set { SetProperty(ref abnahmeAbgelehnt, value); }
        }


        public String AbnahmeBegründung
        {
            get { return abnahmeBegründung; }
            set { SetProperty(ref abnahmeBegründung, value); }
        }


        public String StatusAsString { get { return status.StatusAsText(); } }


        public Ticketstatus Status
        {
            get { return status; }
            set { SetProperty(ref status, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Kurzbeschreibung
        {
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }


        public String Aktivitätenbeschreibung
        {
            get { return aktivitätenbeschreibung; }
            set { SetProperty(ref aktivitätenbeschreibung, value); }
        }


        public DateTime GemeldetAm
        {
            get { return gemeldetAm; }
            set { SetProperty(ref gemeldetAm, value); }
        }


        public AuIHKSDto AuIHKS
        {
            get { return auIHKS; }
            set { SetProperty(ref auIHKS, value); }
        }


        public LeistungsartFilterModel Leistungsart
        {
            get { return leistungsart; }
            set { SetProperty(ref leistungsart, value); }
        }


        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        public NummerFilterModel Schadensbild
        {
            get { return schadensbild; }
            set { SetProperty(ref schadensbild, value); }
        }


        public NummerFilterModel Schadensursache
        {
            get { return schadensursache; }
            set { SetProperty(ref schadensursache, value); }
        }


        public PersonalFilterModel Auftragnehmer
        {
            get { return auftragnehmer; }
            set { SetProperty(ref auftragnehmer, value); }
        }


        public PersonalFilterModel Auftragsleiter
        {
            get { return auftragsleiter; }
            set { SetProperty(ref auftragsleiter, value); }
        }


        public PersonalFilterModel Auftraggeber
        {
            get { return auftraggeber; }
            set { SetProperty(ref auftraggeber, value); }
        }


        public PersonalFilterModel Abnahmeberechtigter
        {
            get { return abnahmeberechtigter; }
            set { SetProperty(ref abnahmeberechtigter, value); }
        }


        public DateTime? Fertigstellungstermin
        {
            get { return fertigstellungstermin; }
            set { SetProperty(ref fertigstellungstermin, value); }
        }


        public ICollection<Personalleistung> Personalleistungen
        {
            get {

                if (personalleistungen == null) personalleistungen = new TrackableCollection<Personalleistung>();
                return personalleistungen;
            }
            set { SetProperty(ref personalleistungen, value); }
        }


        public ICollection<Ticketprotokoll> Protokoll
        {
            get {

                if (protokoll == null) protokoll = new TrackableCollection<Ticketprotokoll>();
                return protokoll;
            }
            set { SetProperty(ref protokoll, value); }
        }


#endregion


        public bool LeistungWurdeEingetragen
        {
            get { return LeDatum.HasValue || LeUhrzeitVon.HasValue || LeUhrzeitBis.HasValue || !String.IsNullOrWhiteSpace(LeAnmerkung); }
        }


        public bool AbnahmeWurdeEingetragen
        {
            get { return AbnahmeAbgelehnt || AbnahmeAbgenommen; }
        }


#region V A L I D A T I O N


        public List<String> Errors
        {

            get {

                var result = new List<String>();


                if (Auftragstatus == Auftragstatus.Abgeschlossen) {

                    result.Add("Der Auftrag ist bereits abgeschlossen.");
                    return result;
                }

                if (Auftragstatus == Auftragstatus.Storno) {

                    result.Add("Der Auftrag ist bereits storniert.");
                    return result;
                }

                String test = this["LeDatum"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["LeUhrzeitVon"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["LeUhrzeitBis"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["AbnahmeBegründung"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["IstSauber"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public String Error
        {

            get {

                String test = this["LeDatum"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["LeUhrzeitVon"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["LeUhrzeitBis"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["AbnahmeBegründung"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["AuIHKS"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Kurzbeschreibung"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["IstSauber"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {


               switch (columnName) {

               case nameof(LeDatum):

                    if (LeistungWurdeEingetragen && (!LeDatum.HasValue || LeDatum.Value.Year < 2010 || LeDatum.Value.Year > 2100)) return "Wenn Sie eine Leistung eintragen wollen müssen Sie ein plausibles Datum angeben.";
                    if (LeistungWurdeEingetragen && LeDatum.HasValue && LeDatum.Value.Date < GemeldetAm.Date) return "Wenn Sie eine Leistung eintragen wollen darf diese nicht vor der Ticketanlage liegen.";
                    break;

                case nameof(LeUhrzeitVon):

                    if (LeistungWurdeEingetragen && !LeUhrzeitVon.HasValue) return "Wenn Sie eine Leistung eintragen wollen müssen Sie eine plausible Startzeit angeben.";
                    break;

                case nameof(LeUhrzeitBis):

                    if (LeistungWurdeEingetragen && !LeUhrzeitBis.HasValue) return "Wenn Sie eine Leistung eintragen wollen müssen Sie eine plausible Endezeit angeben.";
                    break;

                case nameof(AbnahmeBegründung):

                    if (AbnahmeAbgelehnt && String.IsNullOrWhiteSpace(AbnahmeBegründung)) return "Wenn Sie die Ticketabnahme ablehnen müssen Sie dies begründen.";
                    break;

                case nameof(AuIHKS):

                    if (AuIHKS == null) return "Das Ticket muss einer Maschine / Anlage zugeordnet sein.";
                    break;

                case nameof(Kurzbeschreibung):

                    if (String.IsNullOrWhiteSpace(Kurzbeschreibung)) return "Das Ticket muss eine Beschreibung besitzen.";
                    break;

                case nameof(IstSauber):

                    if (!IstSauber && AbnahmeAbgenommen) return "Wenn das Ticket abgeniommen werden soll muss der Arbeitsplatz sauber und frei von Fremdkörpern verlassen worden sein.";
                    break;
                }

                return null;
            }
        }


#endregion


    }
}
