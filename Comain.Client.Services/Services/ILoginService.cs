﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Auth;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.ViewData;

namespace Comain.Client.Services
{

    public interface ILoginService
    {

        Nutzer       AktuellerNutzer     { get; }
        String       AktuelleVerbindung  { get; }
        String       AktuellesProfil     { get; }
        String       AktuelleAdresse     { get; }

        List<ConnectionViewItem> Endpoints  { get; }


        Task<Nutzer> LoginAsync(String endpoint, String username, String password, CancellationToken ct = default);
        Task         LogoutAsync(CancellationToken ct = default);
        Task         SetPasswordAsync(PasswordDto dto, CancellationToken ct = default);
    }
}
