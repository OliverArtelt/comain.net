﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Excel-Dokument schreiben
    /// </summary>
    public class WorkbookWriter
    {

        /// <summary>
        /// Dateinamen und -inhalte des Dokumentes
        /// </summary>
        private FileList files; 
        /// <summary>
        /// erste zu verwendende Ausgabezeile
        /// </summary>
        private readonly int firstOutputRow;
        /// <summary>
        /// Wörterbuch
        /// </summary>
        private StringPoolEncoder stringDict;
        /// <summary>
        /// Stilverzeichnis
        /// </summary>
        private StylePool styles;         
        /// <summary>
        /// Nichtauszugebende Spalten
        /// </summary>
        private List<int> suppressedCols;   
        /// <summary>
        /// zu schreibendes Dokument / zu lesende Schablone
        /// </summary>
        private Archiver archiver;
        
        
        /// <summary>
        /// Nichtauszugebende Spalten (wenn z.B. Summenspalten nicht mit ausgegeben werden sollen)
        /// </summary>
        public List<int> SuppressedColumns 
        { 
            get { return suppressedCols; }
            set { suppressedCols = value; }
        }
        
        /// <summary>
        /// Excel-Dokument schreiben
        /// </summary>
        /// <param name="dst">Pfad Zieldatei</param>
        public WorkbookWriter(string dst) : this(dst, String.Empty, 1) {}
        
        /// <summary>
        /// Excel-Dokument schreiben
        /// </summary>
        /// <param name="dst">Pfad Zieldatei</param>
        /// <param name="tpl">Pfad Schablonendatei oder leer</param>
        public WorkbookWriter(string dst, string tpl) : this(dst, tpl, 1) {}
        
        /// <summary>
        /// Excel-Dokument schreiben
        /// </summary>
        /// <param name="dst">Pfad Zieldatei</param>
        /// <param name="tpl">Pfad Schablonendatei oder leer</param>
        /// <param name="fstRow">erste Ausgabezeile 
        /// (darüber liegende Zeilen werden nicht verändert, ab dieser Zeile erscheint neuer Inhalt)</param>
        public WorkbookWriter(string dst, string tpl, int fstRow)
        {
    
            firstOutputRow = fstRow;
            archiver = new Archiver();
            files = new FileList();
            
            CreateFiles(dst, tpl);
            CreateSharedStrings();
            CreateStyles();

            PrepareWorkbook();
        }

        /// <summary>
        /// Vorlage einlesen / leeres Dokument erstellen
        /// </summary>
        private void CreateFiles(string dst, string tpl)
        {
        
            archiver.CreateArchive(dst);

            //Wurde Vorlage angegeben?
            if (!String.IsNullOrEmpty(tpl)) {

                //Vorlage öffnen und enthaltende Dateien listen
                archiver.OpenArchive(tpl);
                
                //Dateiinhalte in Stringvektor einlesen
                foreach (var item in archiver.ListContent(true)) {
                    
                    files.SetBytes(item, archiver.ExtractAsBytes(item));
                }
                
            } else {

                //from Scratch: neues leeres Dokument erstellen
                files.SetString("[Content_Types].xml",        EmptyDocFactory.ContentTypes());
                files.SetString("_rels/.rels",                EmptyDocFactory.Rels_Rels());
                files.SetString("docProps/app.xml",           EmptyDocFactory.DocProps_App());
                files.SetString("docProps/core.xml",          EmptyDocFactory.DocProps_Core());
                files.SetString("xl/styles.xml",              EmptyDocFactory.XL_Styles());
                files.SetString("xl/workbook.xml",            EmptyDocFactory.XL_Workbook());
                files.SetString("xl/_rels/workbook.xml.rels", EmptyDocFactory.XL_Rels_Workbook());
                files.SetString("xl/theme/theme1.xml",        EmptyDocFactory.XL_Theme_Theme1());
                files.SetString("xl/worksheets/sheet1.xml",   EmptyDocFactory.XL_Worksheets_Sheet1());
            }
        }

        /// <summary>
        /// Wörterbuch erstellen
        /// </summary>
        private void CreateSharedStrings()
        {

            //ist ein Wörterbuch bereits vorhanden?
            if (!files.ContainsKey("xl/sharedStrings.xml")) {

                //Wörterbuchschablone anfügen
                files.SetString("xl/sharedStrings.xml", EmptyDocFactory.XL_sharedStrings());

                TextReader rdr;
                using (rdr = new StringReader(files.GetString("[Content_Types].xml"))) {
                
                    XDocument doc = XDocument.Load(rdr);
                    XNamespace ns = doc.Root.Name.Namespace;
                    
                    //Dokumenttyp "Wörterbuch" registrieren
                    XElement types = doc.Element(ns + "Types");
                    XElement strings = new XElement(ns + "Override");
                    strings.Add(new XAttribute("PartName", "/xl/sharedStrings.xml"));
                    strings.Add(new XAttribute("ContentType", "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"));
                    types.Add(strings);
                    
                    files.SetString("[Content_Types].xml", doc.ToString());
                }

                //Wörterbuch mit Arbeitsmappe verknüpfen
                using (rdr = new StringReader(files.GetString("xl/_rels/workbook.xml.rels"))) {
                
                    XDocument doc = XDocument.Load(rdr);
                    XNamespace ns = doc.Root.Name.Namespace;
                    
                    XElement rels = doc.Element(ns + "Relationships");
                    String id = String.Format("rId{0}", rels.Descendants().Count() + 1);
                    
                    XElement rel = new XElement(ns + "Relationship");
                    rel.Add(new XAttribute("Id", id));
                    rel.Add(new XAttribute("Type", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"));
                    rel.Add(new XAttribute("Target", "sharedStrings.xml"));
                    rels.Add(rel);
                    
                    files.SetString("xl/_rels/workbook.xml.rels", doc.ToString());
                }
            }

            stringDict = new StringPoolEncoder(files.GetString("xl/sharedStrings.xml"));
        }
      
        /// <summary>
        /// Stilverzeichnis einrichten
        /// </summary>
        private void CreateStyles()
        {
        
            styles = new StylePool(files.GetString("xl/styles.xml"));
        }
        
        /// <summary>
        /// Dokumenttyp "Template" in "Sheet" umwandeln, falls Mustervorlage .xltx statt Excel-Dokument .xlsx verwendet wurde, 
        /// letzte Bearbeitung eintragen
        /// </summary>
        private void PrepareWorkbook()
        {
                        
            TextReader rdr;
            
            //Dokumenttyp "Template" in "Sheet" umwandeln
            String workbooktype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml";

            using (rdr = new StringReader(files.GetString("[Content_Types].xml"))) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                
                XElement wb = (from x in doc.Descendants(ns + "Override") 
                               where x.Attribute("PartName").Value.Equals("/xl/workbook.xml")
                               select x).Single();
                wb.Attribute("ContentType").Value = workbooktype;
                files.SetString("[Content_Types].xml", doc.ToString());
            }                              

            //letzte Bearbeitung eintragen
            using (rdr = new StringReader(files.GetString("docProps/core.xml"))) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace dcterms = "http://purl.org/dc/terms/";
                
                XElement created  = (from x in doc.Descendants(dcterms + "created") 
                                    select x).Single();
                created.Value = DateTime.Now.ToOpenXMLString();                     
                                    
                XElement modified = (from x in doc.Descendants(dcterms + "modified") 
                                    select x).Single();
                modified.Value = created.Value;                     
                
                files.SetString("docProps/core.xml", doc.ToString());
            }                              
        }                              
 
        
        /// <summary>
        /// Tabellenschreiber einrichten
        /// </summary>
        /// <returns></returns>
        public WorksheetWriter OpenSheet() { return OpenSheet(1); }

        /// <summary>
        /// Tabellenschreiber einrichten
        /// </summary>
        /// <param name="sheetId"></param>
        /// <returns></returns>
        public WorksheetWriter OpenSheet(int sheetId)
        {

            //Wörterbuch und Stilverzeichnis sind zentral für alle Tabellen und werden deshalb immer mitgegeben
            WorksheetWriter sheet = new WorksheetWriter(sheetId, files, stringDict, styles, firstOutputRow, suppressedCols);
            return sheet;
        }

        /// <summary>
        /// Dokumentteile zusammensetzen und ins Archiv schreiben
        /// </summary>
        public void WriteDocument()
        {
         
            //Wörterbuch und Stilverzeichnis übernehmen
            files.SetString("xl/sharedStrings.xml", stringDict.WriteXML());
            files.SetString("xl/styles.xml", styles.WriteXML());

            //Alle Dateiinhalte archivieren
            foreach (var entry in files) archiver.ImportFromMemory(entry.Value, entry.Key);
            archiver.Save();
        }
    }
}
