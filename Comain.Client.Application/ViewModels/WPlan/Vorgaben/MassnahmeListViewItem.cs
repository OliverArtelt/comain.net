﻿using System;
using System.Linq;
using System.ComponentModel;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class MassnahmeListViewItem : NotifyBase
    {

        private String       baugruppe;
        private String       iHObjekt;
        private String       name;
        private String       nummer;
        private String       leistungsart;
        private String       gewerk;
        private Termintyp    termintyp;
        private int          zyklus;
        private Weekday      tagesmuster;
        private String       status;
        private DateTime?    letzter;
        private DateTime?    nächster;
        private String       externeAuftragsnummer;


        public int Id  { get; set; }


        public String Baugruppe
        {
            get { return baugruppe; }
            set { SetProperty(ref baugruppe, value); }
        }


        public String IHObjekt
        {
            get { return iHObjekt; }
            set { SetProperty(ref iHObjekt, value); }
        }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Leistungsart
        {
            get { return leistungsart; }
            set { SetProperty(ref leistungsart, value); }
        }


        public String Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        public String ExterneAuftragsnummer
        {
            get { return externeAuftragsnummer; }
            set { SetProperty(ref externeAuftragsnummer, value); }
        }


        public Termintyp Termintyp
        {
            get { return termintyp; }
            set { if (SetProperty(ref termintyp, value)) RaisePropertyChanged(nameof(TermintypAsString)); }
        }


        public int Zyklus
        {
            get { return zyklus; }
            set { SetProperty(ref zyklus, value); }
        }


        public Weekday Tagesmuster
        {
            get { return tagesmuster; }
            set { SetProperty(ref tagesmuster, value); }
        }


        public bool IstAktiv => Status == "Aktiv";


        public String TermintypAsString => Termintyp.AsText();


        public String ZyklusAsString => WiMassnahme.ZyklusAsString(Termintyp, Zyklus, Tagesmuster);


        public String Status
        {
            get { return status; }
            set {

                if (SetProperty(ref status, value)) {

                    RaisePropertyChanged(nameof(StatusAsColor));
                    RaisePropertyChanged(nameof(IstAktiv));
                }
            }
        }


        public DateTime? Letzter
        {
            get { return letzter; }
            set { SetProperty(ref letzter, value); }
        }


        public DateTime? Nächster
        {
            get { return nächster; }
            set { SetProperty(ref nächster, value); }
        }


        public String StatusAsColor
        {
            get {

                switch (Status) {

                    case "Aktiv":   return "LimeGreen";
                    case "Inaktiv": return "LightSlateGray";
                    default:        return "White";
                }
            }
        }


        private WiMassnahme model;
        public WiMassnahme Model
        {
            get { return model; }
            set {

                if (value != model) {

                    if (model != null) {

                        PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    }

                    model = value;

                    if (model != null) {

                        PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                        Emboss();
                    }
                }
            }
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case nameof(model.Name):

                Name = model.Name;
                RaisePropertyChanged(nameof(Name));
                break;

            case nameof(model.Nummer):

                Nummer = model.Nummer;
                RaisePropertyChanged(nameof(Nummer));
                break;

            case nameof(model.ExterneAuftragsnummer):

                ExterneAuftragsnummer = model.ExterneAuftragsnummer;
                RaisePropertyChanged(nameof(ExterneAuftragsnummer));
                break;

            case nameof(model.IHObjekt):

                Baugruppe = model.IHObjekt.Name;
                IHObjekt = model.IHObjekt.Nummer;
                RaisePropertyChanged(nameof(Baugruppe));
                RaisePropertyChanged(nameof(IHObjekt));
                break;

            case nameof(model.Leistungsart):

                Leistungsart = null;
                if (model.Leistungsart != null) Leistungsart = model.Leistungsart.Nummer + " - " + model.Leistungsart.Name;
                RaisePropertyChanged(nameof(Leistungsart));
                break;

            case nameof(model.Gewerk):

                Gewerk = null;
                if (model.Gewerk != null) Gewerk = model.Gewerk.Nummer + " - " + model.Gewerk.Name;
                RaisePropertyChanged(nameof(Gewerk));
                break;

            case nameof(model.Termintyp):

                Termintyp = model.Termintyp;
                RaisePropertyChanged(nameof(Termintyp));
                RaisePropertyChanged(nameof(ZyklusAsString));
                RaisePropertyChanged(nameof(TermintypAsString));
                break;

            case nameof(model.Zyklus):

                Zyklus = model.Zyklus;
                RaisePropertyChanged(nameof(Zyklus));
                RaisePropertyChanged(nameof(ZyklusAsString));
                break;

            case nameof(model.Tagesmuster):

                Tagesmuster = model.Tagesmuster;
                RaisePropertyChanged(nameof(Zyklus));
                RaisePropertyChanged(nameof(ZyklusAsString));
                break;

            case nameof(model.IstAktiv):

                Status = model.IstAktiv ? "Aktiv": "Inaktiv";
                RaisePropertyChanged(nameof(Status));
                RaisePropertyChanged(nameof(StatusAsColor));
                break;
            }
        }


        public MassnahmeListViewItem()
        {}


        public MassnahmeListViewItem(WiMassnahme model)
        {

            Model = model;
            Emboss();
        }


        public MassnahmeListViewItem(MassnahmeListDto dto)
        {

            if (!String.IsNullOrEmpty(dto.Baugruppe)) {

                var parts = dto.Baugruppe.Split('•');
                Baugruppe = parts.LastOrDefault()?.Trim();
            }

            Id           = dto.Id;
            IHObjekt     = dto.IHObjekt;
            Name         = dto.Name;
            Nummer       = dto.Nummer;
            Leistungsart = dto.Leistungsart;
            Gewerk       = dto.Gewerk;
            Termintyp    = (Termintyp)dto.Termintyp;
            Zyklus       = dto.Zyklus;
            Status       = dto.Status;
            Nächster     = dto.Nächster;
            Letzter      = dto.Letzter;
            Tagesmuster  = (Weekday)dto.Tagesmuster;
            ExterneAuftragsnummer = dto.ExterneAuftragsnummer;
        }


        public void Emboss()
        {

            Id           = model.Id;
            Baugruppe    = model.IHObjekt == null? null: model.IHObjekt.Name;
            IHObjekt     = model.IHObjekt == null? null: model.IHObjekt.Nummer;
            Name         = model.Name;
            Nummer       = model.Nummer;
            Leistungsart = model.Leistungsart == null? null: model.Leistungsart.Nummer + " - " + model.Leistungsart.Name;
            Gewerk       = model.Gewerk == null? null: model.Gewerk.Nummer + " - " + model.Gewerk.Name;
            Termintyp    = model.Termintyp;
            Zyklus       = model.Zyklus;
            Tagesmuster  = model.Tagesmuster;
            Status       = model.IstAktiv ? "Aktiv": "Inaktiv";
            ExterneAuftragsnummer = model.ExterneAuftragsnummer;
        }
    }
}
