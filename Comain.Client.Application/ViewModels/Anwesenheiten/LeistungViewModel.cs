﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Dtos.Anwesenheiten;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;
using Comain.Client.Models;
using Comain.Client.Models.Anwesenheiten;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Anwesenheiten;
using Comain.Client.Models.IHObjekte;
using Comain.Client.Dtos.WPlan;

namespace Comain.Client.ViewModels.Anwesenheiten
{

    public class LeistungViewModel: ViewModel<ILeistungView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly IHSelektorViewModel ihFilter;
        private readonly LeistungFilterViewModel filter;
        private IEnumerable<MassnahmeListDto> alleMassnahmen;


        public LeistungViewModel(ILeistungView view, IHSelektorViewModel ihFilter, LeistungFilterViewModel filter) : base(view)
        {

            context = SynchronizationContext.Current;
            this.ihFilter = ihFilter;
            this.filter = filter;
            PropertyChangedEventManager.AddHandler(ihFilter, ViewPropertyChanged, String.Empty);
            filter.SearchCommand = new DelegateCommand(_ => { Refresh(); });
            filter.ResetCommand = new DelegateCommand(_ => { filter.ResetFilter(); Refresh(); });
        }


        public async Task SetzeListenAsync(IList<IHListDto> ihlist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                           IList<LeistungsartFilterModel> lalist, IList<NummerFilterModel> gwlist, CancellationToken ct = default)
        {

            await ihFilter.LoadAsync(ihlist.Where(p => Comain.Client.Models.IHObjekte.IHObjekt.IstMaschinennummer(p.Nummer))
                                           .Select(p => new IHFilterItem(p)).ToList(),
                                     pzlist, kstlist, ct);
            filter.Gewerke = gwlist;
            filter.Leistungsarten = lalist;
        }


        public void SetzeSelektion(IEnumerable<Anwesenheit> anwesenheiten, IEnumerable<MassnahmeListDto> massnahmen)
        {

            var current = CurrentAnwesenheit;
            Anwesenheiten = anwesenheiten.ToList();
            alleMassnahmen = massnahmen;
            if (current != null) CurrentAnwesenheit = Anwesenheiten.FirstOrDefault(p => p.Id == current.Id);

            SetzeMassnahmen();
        }


        private void SetzeMassnahmen()
        {

            Massnahmen = null;
            if (CurrentAnwesenheit == null || alleMassnahmen == null) return;

            var mnlist = alleMassnahmen.Select(mn => new AssignMassnahmeViewItem(CurrentAnwesenheit, mn));

            context.Send(new SendOrPostCallback((o) => {

                Massnahmen = new CollectionViewSource { Source = mnlist }.View;
                Massnahmen.Filter = Predicate;
                Refresh();
            }), null);
        }


        private bool Predicate(object o)
        {

            var mn = o as AssignMassnahmeViewItem;
            if (mn == null) return false;
            if (filter.Leistungsart != null && mn.Leistungsart_Id != filter.Leistungsart.Id) return false;
            if (filter.Gewerk != null && mn.Gewerk_Id != filter.Gewerk.Id) return false;
            if (!String.IsNullOrWhiteSpace(filter.SearchText) &&
                !mn.Name.Contains(filter.SearchText, StringComparison.CurrentCultureIgnoreCase) &&
                !mn.Nummer.Contains(filter.SearchText, StringComparison.CurrentCultureIgnoreCase)) return false;
            if (filter.AssignedOnly && !mn.IstSelektiert) return false;
            return true;
        }


        private void Refresh()
        {

            if (Massnahmen == null) return;
            context.Send(new SendOrPostCallback((o) => { Massnahmen.Refresh(); }), null);
        }


        public void Unload()
        {

            Massnahmen = null;
            Anwesenheiten = null;
            ihFilter.Unload();
            filter.Unload();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (sender == ihFilter && args.PropertyName == nameof(ihFilter.CurrentIHObjekt))
                SearchCommand?.Execute(sender);
        }


        public AssignedPSRequest GebeFilter()
            => IHObjekt == null || !Datum.HasValue || !UhrzeitVon.HasValue? null:
                new AssignedPSRequest { Asset_Id = IHObjekt.Id, Datum = Datum.Value, UhrzeitVon = UhrzeitVon.Value, MitMassnahmen = true };


#region P R O P E R T I E S


        private DateTime? datum = DateTime.Today;
        public DateTime? Datum
        {
            get => datum;
            set { if (SetProperty(ref datum, value)) SearchCommand?.Execute(this); }
        }


        public object           IHSelektor   => ihFilter.View;
        public IHFilterItem     IHObjekt     => ihFilter.CurrentIHObjekt;
        public TimeSpan?        UhrzeitVon   { get; private set; }


        public String UhrzeitVonInput
        {
            get => !UhrzeitVon.HasValue ? null: UhrzeitVon.Value.ToString("hh\\:mm");
            set {

                var time = value.ParseTime();
                if (UhrzeitVon != time) {

                    UhrzeitVon = time;
                    RaisePropertyChanged(nameof(UhrzeitVon));
                    RaisePropertyChanged(nameof(UhrzeitVonInput));
                    SearchCommand?.Execute(this);
                }
            }
        }


        private ICollectionView massnahmen;
        public ICollectionView Massnahmen
        {
            get => massnahmen;
            private set => SetProperty(ref massnahmen, value);
        }


        public bool IsChanged => !Anwesenheiten.IsNullOrEmpty() && Anwesenheiten.Any(p => p.IsChanged);


        private IList<Anwesenheit> anwesenheiten;
        public IList<Anwesenheit> Anwesenheiten
        {
            get => anwesenheiten;
            private set => SetProperty(ref anwesenheiten, value);
        }


        private Anwesenheit currentAnwesenheit;
        public Anwesenheit CurrentAnwesenheit
        {
            get => currentAnwesenheit;
            set { if (SetProperty(ref currentAnwesenheit, value)) SetzeMassnahmen(); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get => searchCommand;
            set => SetProperty(ref searchCommand, value);
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get => saveCommand;
            set => SetProperty(ref saveCommand, value);
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get => backCommand;
            set => SetProperty(ref backCommand, value);
        }


        private ICommand auftragCommand;
        public ICommand AuftragCommand
        {
            get => auftragCommand;
            set => SetProperty(ref auftragCommand, value);
        }


        private String mandant;
        public String Mandant
        {
            get => mandant;
            set => SetProperty(ref mandant, value);
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get => showDelay;
            set => SetProperty(ref showDelay, value);
        }


        public object FilterView => filter.View;


#endregion

    }
}
