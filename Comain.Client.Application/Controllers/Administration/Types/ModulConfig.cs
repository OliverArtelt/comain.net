﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Models;
using Comain.Client.Models.Users;


namespace Comain.Client.Controllers.Administration.Types
{

    public class ModulConfig : IAdminTyp
    {
        
        public string       Title           => "Module verwalten";
        public int          Sortierung      => 10000; 
        public Type         Controller      => typeof(IConfigController<ModulViewModel>);


        public bool HatZugriff(ModulInfo module, Nutzer user)
            => true;
    }
}
