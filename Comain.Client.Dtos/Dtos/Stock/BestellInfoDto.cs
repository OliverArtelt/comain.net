﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class BestellInfoDto
    {
       
        public int              BestellungId    { get; set; } 
        public String           LfArtikelnummer { get; set; }
        public String           Nummer          { get; set; }
        public String           Auftrag         { get; set; }
        public String           Kostenstelle    { get; set; }
        public DateTime?        DatumAusgang    { get; set; }
        public DateTime?        Termin          { get; set; }

        public int              PositionId      { get; set; } 
        public int?             Position        { get; set; }  
        public decimal          Menge           { get; set; }  
        public decimal?         Preis           { get; set; }  
   }
}
