﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.Anwesenheiten
{

    public class AssignedPSResponse : AssignedPSRequest
    {

        public IEnumerable<AnwesenheitDto>  List    { get; set; }
    }
}
