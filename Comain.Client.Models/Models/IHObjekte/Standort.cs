﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.IHObjekte
{

    public class Standort : Entity, IDataErrorInfo, IComparable<Standort>
    {

        private String name;
        private String kurzname;
        private int nummer;
        private bool istKundenstandort;
        private TrackableCollection<Kostenstelle> zugeordneteKostenstellen;


#region P R O P E R T I E S


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Kurzname
        {
            get { return kurzname; }
            set { SetProperty(ref kurzname, value); }
        }


        public int Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public bool IstKundenstandort
        {
            get { return istKundenstandort; }
            set { SetProperty(ref istKundenstandort, value); }
        }


        public TrackableCollection<Kostenstelle> ZugeordneteKostenstellen
        {
            get { return zugeordneteKostenstellen ?? (zugeordneteKostenstellen = new TrackableCollection<Kostenstelle>()); }
            set { SetProperty(ref zugeordneteKostenstellen, value); }
        }


#endregion


        public int CompareTo(Standort other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareTo(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Name)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Kurzname)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Name)];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 150) return "Die Bezeichnung darf aus maximal 150 Zeichen bestehen.";
                    break;

                case nameof(Kurzname):

                    if (String.IsNullOrWhiteSpace(Kurzname)) return "Geben Sie den Kurznamen an.";
                    if (Kurzname.Length > 10) return "Der Kurzname darf aus maximal 10 Zeichen bestehen.";
                    break;

                case nameof(Nummer):

                    if (Nummer <= 0) return "Geben Sie die Nummer an. Sie muss größer 0 sein.";
                    if (Nummer > 999) return "Die Nummer darf aus maximal 3 Ziffern bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
