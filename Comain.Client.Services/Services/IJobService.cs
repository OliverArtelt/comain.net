﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.ViewData;

namespace Comain.Client.Services
{

    public interface IJobService
    {

        Task<JobStatus> GetAsync(String sessionId, CancellationToken ct = default);
        Task            CancelAsync(String sessionId, CancellationToken ct = default);
    }
}