﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Services;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ViewData.WPlan;
using Microsoft.Extensions.Logging;

namespace Comain.Client.Controllers.WPlan.Planen
{

    public class PlanTableBuilder : IPlanTableBuilder
    {

        private ZeitstrahlDto    dto;
        private List<Planspalte> spalten;
        private List<IPlanzeile> zeilen;
        private readonly ILogger logger;
        private readonly ILoginService logins;


        public PlanTableBuilder(ILogger<PlanTableBuilder> logger, ILoginService logins)
        {

            this.logger = logger;
            this.logins = logins;
        }


        public Plan BuildPlan(WPlanCreateInfo pars, ZeitstrahlDto dto, Konfiguration cfg)
        {

            this.dto = dto;
            spalten = dto.Perioden.Select((p, ndx) => new Planspalte(p, ndx, dto.WochenRückmeldung, cfg)).ToList();
            zeilen = new List<IPlanzeile>();
            var plan = new Plan(pars, spalten, zeilen, logins.AktuellerNutzer, cfg, logger);
            plan.Benutzer = logins.AktuellerNutzer.Name;

            int zeilenindex = 0;
            PZPlanzeile currentPZ = null;
            IHPlanzeile currentIH = null;

            foreach (var pz in dto.Standorte) {

                zeilen.Add(currentPZ = new PZPlanzeile(plan, pz));

                foreach (var ih in pz.IHObjekte) {

                    zeilen.Add(currentIH = new IHPlanzeile(currentPZ, ih));
                    currentPZ.Children.Add(currentIH);

                    foreach (var mn in ih.Massnahmen) {

                        var mnvi = new MNPlanzeile(currentIH, mn);
                        zeilen.Add(mnvi);
                        currentIH.Children.Add(mnvi);
                        zeilenindex++;
                        mnvi.LeereZeileAnzeigen = !pars.NurRelevante;
                    }

                    currentIH.SetzeAufträge();
                }
            }

            plan.CalculateHeights();
            plan.Accept(new SummenLinkVisitor());


            if (!dto.Material.IsNullOrEmpty()) {

                var mtlist = dto.Material.Where(p => p.MengeVorhanden.GetValueOrDefault() > 0m)
                                         .Select(p => (id: p.Id, menge: p.MengeVorhanden.Value ))
                                         .ToList();
                plan.VerteileMaterial(mtlist);
            }

            return plan;
        }
    }
}
