﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Emma;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels;
using Comain.Client.Models.Emma;

namespace Comain.Client.ViewModels.Emma
{

    public class EmmaFilterViewModel : ViewModel<IEmmaFilterView>
    {

        private readonly SynchronizationContext context;


        public EmmaFilterViewModel(IEmmaFilterView view)
          : base(view)
        {

            context = SynchronizationContext.Current;
            ResetCommand = new DelegateCommand(Reset);
            Reset();
        }


        public Task LoadAsync(EmmaSession session)
            => context.SendAsync(new SendOrPostCallback((o) => {

                    IHObjekte = session.IHObjekte.Select(p => p.Inventarnummer).Where(p => !String.IsNullOrWhiteSpace(p)).Distinct().OrderBy(p => p).ToList();
                    Aufträge = session.Aufträge.Select(p => p.ExterneNummer).Where(p => !String.IsNullOrWhiteSpace(p)).Distinct().OrderBy(p => p).ToList();
                    Personalliste = session.Personalliste.Select(p => p.Nummer).Where(p => !String.IsNullOrWhiteSpace(p)).Distinct().OrderBy(p => p).ToList();

                    RaisePropertyChanged(nameof(IHObjekte));
                    RaisePropertyChanged(nameof(Aufträge));
                    RaisePropertyChanged(nameof(Personalliste));
                }), null);


        public void Unload()
        {

            Aufträge      = null;
            Personalliste = null;
            IHObjekte     = null;
        }


        public void Reset()
        {

            Auftrag             = default;
            Personal            = default;
            IHObjekt            = default;
            DatumVon            = DateTime.Today.MondayOf();
            DatumBis            = DatumVon.Value.AddDays(6);
            StatusUnbearbeitet  = true;
            StatusUnvollständig = true;
            StatusVollständig   = true;
            StatusBestätigt     = true;
            StatusAbgeschlossen = false;
            StatusGelöscht      = false;
            StatusFehlerhaft    = true;
        }


        public Suchkriterien GebeFilter()
        {

            var filter = new Suchkriterien();
            filter.Add(nameof(IHObjekt), IHObjekt);
            filter.Add(nameof(Auftrag), Auftrag);
            filter.Add(nameof(Personal), Personal);
            filter.Add(nameof(DatumVon), DatumVon);
            filter.Add(nameof(DatumBis), DatumBis);
            filter.Add(nameof(StatusUnbearbeitet), StatusUnbearbeitet);
            filter.Add(nameof(StatusUnvollständig), StatusUnvollständig);
            filter.Add(nameof(StatusVollständig), StatusVollständig);
            filter.Add(nameof(StatusBestätigt), StatusBestätigt);
            filter.Add(nameof(StatusAbgeschlossen), StatusAbgeschlossen);
            filter.Add(nameof(StatusGelöscht), StatusGelöscht);
            filter.Add(nameof(StatusFehlerhaft), StatusFehlerhaft);

            return filter;
        }


        public void SetResultStatus(IList<Leistung> list)
        {

            if (list.IsNullOrEmpty()) ResultText = "FILTER: keine Leistung gefunden";
            else if (list.Count == 1) ResultText = "FILTER: eine Leistung gefunden";
            else ResultText = $"FILTER: {list.Count} Leistungen gefunden";
        }


#region P R O P E R T I E S


        private String resultText;
        public String ResultText
        {
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        public ICommand ResetCommand { get; private set; }


        public IEnumerable Aufträge        { get; private set; }
        public IEnumerable Personalliste   { get; private set; }
        public IEnumerable IHObjekte       { get; private set; }


        private String auftrag;
        public String Auftrag
        {
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }


        private String personal;
        public String Personal
        {
            get { return personal; }
            set { SetProperty(ref personal, value); }
        }


        private String ihobjekt;
        public String IHObjekt
        {
            get { return ihobjekt; }
            set { SetProperty(ref ihobjekt, value); }
        }


        private DateTime? datumVon;
        public DateTime? DatumVon
        {
            get { return datumVon; }
            set { SetProperty(ref datumVon, value); }
        }


        private DateTime? datumBis;
        public DateTime? DatumBis
        {
            get { return datumBis; }
            set { SetProperty(ref datumBis, value); }
        }


        private bool statusUnbearbeitet;
        public  bool StatusUnbearbeitet
        {
            get { return statusUnbearbeitet; }
            set { SetProperty(ref statusUnbearbeitet, value); }
        }


        private bool statusUnvollständig;
        public  bool StatusUnvollständig
        {
            get { return statusUnvollständig; }
            set { SetProperty(ref statusUnvollständig, value); }
        }


        private bool statusVollständig;
        public  bool StatusVollständig
        {
            get { return statusVollständig; }
            set { SetProperty(ref statusVollständig, value); }
        }


        private bool statusBestätigt;
        public  bool StatusBestätigt
        {
            get { return statusBestätigt; }
            set { SetProperty(ref statusBestätigt, value); }
        }


        private bool statusAbgeschlossen;
        public  bool StatusAbgeschlossen
        {
            get { return statusAbgeschlossen; }
            set { SetProperty(ref statusAbgeschlossen, value); }
        }


        private bool statusGelöscht;
        public  bool StatusGelöscht
        {
            get { return statusGelöscht; }
            set { SetProperty(ref statusGelöscht, value); }
        }


        private bool statusFehlerhaft;
        public  bool StatusFehlerhaft
        {
            get { return statusFehlerhaft; }
            set { SetProperty(ref statusFehlerhaft, value); }
        }

#endregion

    }
}
