﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Administration;
using Comain.Client.ViewModels.Emma;
using Comain.Client.Services;
using Comain.Client.Services.Administration;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;

namespace Comain.Client.Controllers.Emma
{

    public interface IEmmaConfigController : IController  
    {}


    public class EmmaConfigController : ConfigController<EmmaConfigViewModel>, IEmmaConfigController
    {

        private readonly IFilterService filterStore;
        private readonly EmmaConfigViewModel configView;

  
        public EmmaConfigController(IKonfigService store, IMessageService messages, IControllerBus bus, IFilterService filterStore, 
                                    AdminViewModel view, EmmaConfigViewModel configView)
       
            : base(store, messages, bus, view, configView)
       
        {
            
            this.filterStore = filterStore;     
            this.configView = configView;     
        }


        public override async Task RunAsync(object parameter)
        {
            
            await base.RunAsync(parameter);
            await ExecuteWithLockAsync(_ => LoadAsync());
        }   


        private async Task LoadAsync()
        {

            using (new VisualDelay(configView)) {

                var cftask = store.GetAsync(bus.Token);
                var latask = filterStore.LeistungsartenAsync(bus.Token);
                await Task.WhenAll(cftask, latask).ConfigureAwait(false);

                bus.Token.ThrowIfCancellationRequested();

                current = cftask.Result;
                configView.SetzeListe(latask.Result);
                configView.Current = current;
            }
        }
    }
}
