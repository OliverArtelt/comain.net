﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers;
using Comain.Client.Views.Services;


namespace Comain.Client.ViewModels.Services
{

    public class CameraViewModel : ViewModel<ICameraView>
    {

        private const int initDelay = 200;
        private readonly SynchronizationContext context;


        public CameraViewModel(ICameraView view)
          : base(view)
        {

            context = SynchronizationContext.Current;

            BackCommand  = new DelegateCommand(Cancel);
            StartCommand = new Comain.Client.Controllers.AsyncDelegateCommand(p => StartCaptureAsync(p.ToString()));
            TakeCommand  = new Comain.Client.Controllers.AsyncDelegateCommand(_ => TakeCaptureAsync());
        }


        public void Unload()
        {
        }


        /// <summary>
        /// Aufruf durch übergeordnetes Control
        /// </summary>
        public async Task StartAsync()
        {

       //     try {
       //
       //         await OpenViewerAsync();
       //
       //         await context.SendAsync(new SendOrPostCallback((o) => {
       //
       //             Cameras = null;
       //             Camera = null;
       //             var cdevs = ViewCore.GetVideoCaptureDevices().ToList();
       //             if (cdevs.IsNullOrEmpty()) throw new ComainOperationException("Es wurde keine Kamera gefunden.");
       //
       //             //wenn nur eine Kamera existiert dann diese sofort einschalten, Kamerawahl erübrigt sich
       //             if (cdevs.Count == 1) Camera = cdevs.First();
       //             else Cameras = cdevs;
       //             if (Camera != null) ViewCore.StartCapture(Camera);
       //
       //         }), null);
       //     }
       //     catch (Exception x) {
       //
       //         ViewerVisible = false;
       //         throw x;
       //     }
        }


        /// <summary>
        /// Aufruf durch Kameraauswahl
        /// </summary>
        private async Task StartCaptureAsync(String cameraname)
        {

         //   try {
         //
         //       Camera = Cameras.First(p => p.Name == cameraname);
         //       await OpenViewerAsync();
         //
         //       await context.SendAsync(new SendOrPostCallback((o) => { ViewCore.StartCapture(Camera); }), null);
         //   }
         //   catch (Exception x) {
         //
         //       ViewerVisible = false;
         //       throw x;
         //   }
        }


        /// <summary>
        /// Bild schiessen und übergeben
        /// </summary>
        private async Task TakeCaptureAsync()
        {

        //    try {
        //
        //        Bitmap snapshot = null;
        //
        //        await context.SendAsync(new SendOrPostCallback((o) => {
        //
        //            if (!ViewCore.IsCapturing) return;
        //            snapshot = ViewCore.GetCurrentImage();
        //            ViewCore.StopCapture();
        //
        //        }), null);
        //
        //        if (CompletedCommand != null) CompletedCommand.Execute(snapshot);
        //    }
        //    catch (Exception x) {
        //
        //        ViewerVisible = false;
        //        throw x;
        //    }
        }


        /// <summary>
        /// zurückgehen, Aufnahme abbrechen
        /// </summary>
        public void Cancel()
        {

         //   try {
         //
         //       context.Send(new SendOrPostCallback((o) => { if (ViewCore.IsCapturing) ViewCore.StopCapture(); }), null);
         //       if (CompletedCommand != null) CompletedCommand.Execute(null);
         //   }
         //   catch (Exception x) {
         //
         //       ViewerVisible = false;
         //       throw x;
         //   }
        }


        /// <remarks>
        /// erst muss das Display sichtbar sein weil sonst die DirectX-Pipeline nicht läuft da die Senke dann noch nicht existiert
        /// </remarks>
        private async Task OpenViewerAsync()
        {

            if (ViewerVisible) return;
            ViewerVisible = true;
            await Task.Delay(initDelay);  //warten bis hardware sich gefangen hat
        }


#region P R O P E R T I E S


        public ICommand BackCommand     { get; private set; }
        public ICommand StartCommand    { get; private set; }
        public ICommand TakeCommand     { get; private set; }


        private ICommand completedCommand;
        public ICommand CompletedCommand
        {
            get { return completedCommand; }
            set { SetProperty(ref completedCommand, value); }
        }

/*
        private List<WebCameraId> cameras;
        public List<WebCameraId> Cameras
        {
            get { return cameras; }
            set { SetProperty(ref cameras, value); }
        }


        private WebCameraId camera;
        public WebCameraId Camera
        {
            get { return camera; }
            set { SetProperty(ref camera, value); }
        }
*/

        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private bool viewerVisible;
        public bool ViewerVisible
        {
            get { return viewerVisible; }
            set { SetProperty(ref viewerVisible, value); }
        }


#endregion

    }
}
