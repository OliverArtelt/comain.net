﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.ReportModels;

namespace Comain.Client.Models.Tickets
{

    public class TicketPrintRow
    {

        public String   Auftragskurzbeschreibung     { get; private set; }
        public String   Unteraufträge                { get; private set; }
        public String   Auftragsnummer               { get; private set; }
        public String   Auftragsstatus               { get; private set; }
        public String   IHObjektNummer               { get; private set; }
        public String   Maschine                     { get; private set; }
        public String   Inventarnummer               { get; private set; }
        public String   Fabriknummer                 { get; private set; }
        public String   Standort                     { get; private set; }
        public String   Kostenstelle                 { get; private set; }
        public String   FBNummer                     { get; private set; }
        public String   Priorität1                   { get; private set; }
        public String   Dringlichkeit                { get; private set; }
        public String   Dringlichkeitname            { get; private set; }
        public String   Auftraggeber                 { get; private set; }
        public String   ExterneAuftragsnummer        { get; private set; }
        public String   Beauftragungsdatum           { get; private set; }
        public String   Fertigstellungstermin        { get; private set; }
        public String   Auftragswert                 { get; private set; }
        public String   Stundenvorgabe               { get; private set; }
        public String   Menge                        { get; private set; }
        public String   KWZyklus                     { get; private set; }
        public String   KWStart                      { get; private set; }
        public String   InnerhalbBetriebsferien      { get; private set; }
        public String   Leistungsart                 { get; private set; }
        public String   LeistungsartName             { get; private set; }
        public String   Schadensbild                 { get; private set; }
        public String   SubSchadensbild              { get; private set; }
        public String   Schadensursache              { get; private set; }
        public String   SubSchadensursache           { get; private set; }
        public String   MeldungAm                    { get; private set; }
        public String   MeldungUm                    { get; private set; }
        public String   ÜbergabeAm                   { get; private set; }
        public String   ÜbergabeUm                   { get; private set; }
        public String   Störzeit                     { get; private set; }
        public String   Aktivitätenbeschreibung      { get; private set; }
        public String   Abnahmeberechtigter          { get; private set; }
        public String   Abnahmedatum                 { get; private set; }
        public String   Auftragsleiter               { get; private set; }
        public String   Auftragsleiterdatum          { get; private set; }
        public String   LEGesamtkosten               { get; private set; }
        public String   MTGesamtkosten               { get; private set; }


        public List<AuftragLeMtRow> Leistungen => new List<AuftragLeMtRow>();


        public TicketPrintRow(Ticket model, String ihpath, bool complete)
        {

            if (model == null) return;

            Auftragsnummer           = model.Nummer;
            IHObjektNummer           = model.AuIHKS?.IHObjekt;
            Inventarnummer           = model.AuIHKS?.Inventarnummer;
            Fabriknummer             = model.AuIHKS?.Fabriknummer;
            Standort                 = model.AuIHKS?.Standort;
            Kostenstelle             = model.AuIHKS?.Kostenstelle;
            Dringlichkeit            = model.Dringlichkeit?.Nummer;
            Dringlichkeitname        = model.Dringlichkeit?.Bezeichnung;
            Auftragskurzbeschreibung = model.Kurzbeschreibung;
            Auftragsstatus           = model.StatusAsString;
            MeldungAm                = model.GemeldetAm.ToShortDateString();
            MeldungUm                = model.GemeldetAm.ToShortTimeString();
            ÜbergabeAm               = model.ÜbergabeAm?.ToShortDateString();
            ÜbergabeUm               = model.ÜbergabeAm?.ToShortTimeString();
            Beauftragungsdatum       = model.Beauftragungsdatum?.ToShortDateString();
            Auftragsleiterdatum      = model.Auftragsleiterdatum?.ToShortDateString();
            Abnahmedatum             = model.Abnahmedatum?.ToShortDateString();
            Störzeit                 = model.Störzeit?.ToString();

            Maschine = String.IsNullOrEmpty(ihpath)? model.AuIHKS?.Maschine: ihpath;
            if (String.IsNullOrEmpty(Maschine)) Maschine = "Asset wurde nicht gefunden.";
            if (model.Fertigstellungstermin.HasValue) Fertigstellungstermin = model.Fertigstellungstermin.Value.ToShortDateString();

            Leistungsart     = model.Leistungsart.Nummer;
            LeistungsartName = model.Leistungsart.Name;

            if (model.Schadensbild != null)         Schadensbild         = model.Schadensbild.Name;
            if (model.Schadensursache != null)      Schadensursache      = model.Schadensursache.Name;
            if (model.Abnahmeberechtigter != null)  Abnahmeberechtigter  = model.Abnahmeberechtigter.Nachname + ", " + model.Abnahmeberechtigter.Vorname + " [" + model.Abnahmeberechtigter.Nummer + "]";
            if (model.Auftragsleiter != null)       Auftragsleiter       = model.Auftragsleiter.Nachname + ", " + model.Auftragsleiter.Vorname + " [" + model.Auftragsleiter.Nummer + "]";
            if (model.Auftraggeber != null)         Auftraggeber         = model.Auftraggeber.Nachname + ", " + model.Auftraggeber.Vorname + " [" + model.Auftraggeber.Nummer + "]";

            if (!complete) return;

            Aktivitätenbeschreibung   = model.Aktivitätenbeschreibung;
        }
    }
}
