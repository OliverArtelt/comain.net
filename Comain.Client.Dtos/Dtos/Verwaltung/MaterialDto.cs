﻿using System;
using System.Collections.Generic;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class MaterialDto : ChangeTrackingBase
    {
        
        public  String              Name              { get; set; }
        public  String              Nummer            { get; set; }
        public  String              Beschreibung      { get; set; }
        public  String              Herstellernummer  { get; set; }
        public  decimal?            Mischpreis        { get; set; }
        public  int?                Klasse            { get; set; }
        public  int?                Einheit_Id        { get; set; }
        public  int?                Warengruppe_Id    { get; set; }

        public  ICollection<MtLfMap>  MaterialLieferantMap     { get; set; }
    }
}
