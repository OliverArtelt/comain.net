﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Stock.Wareneinlagerungen;

namespace Comain.Client.Views.Stock.Wareneinlagerungen
{
    /// <summary>
    /// Interaction logic for EinlagerListView.xaml
    /// </summary>
    public partial class EinlagerListView:UserControl ,IEinlagerListView
    {
        public EinlagerListView()
        {
        InitializeComponent();
        }


        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as EinlagerListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
