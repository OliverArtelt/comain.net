﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.ViewModels;
using Comain.Client.Views.WPlan.Planen;


namespace Comain.Client.ViewModels.WPlan.Planen
{

    public class PlanViewModel : ViewModel<IPlanView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public PlanViewModel(IPlanView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void Unload()
        {}


#region P R O P E R T I E S


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private object treeView;
        public object TreeView
        {
            get { return treeView; }
            set { SetProperty(ref treeView, value); }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


#endregion

    }
}
