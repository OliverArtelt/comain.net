﻿using System;


namespace Comain.Client.Dtos
{

    public class NameWertDto
    {

        public String   Name    { get; set; }
        public String   Wert    { get; set; }
    }
}
