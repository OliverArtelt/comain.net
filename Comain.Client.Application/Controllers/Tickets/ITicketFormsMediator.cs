﻿using Comain.Client.ViewModels;

namespace Comain.Client.Controllers.Tickets
{
    public interface ITicketFormsMediator
    {
        EditMode CurrentMode { get; }

        void NeuSetzen();
        void SetEditMode(EditMode newMode);
    }
}