﻿using System;
using System.Collections.Generic;
using Comain.Client.Dtos.Aufträge;

namespace Comain.Client.Services.IHObjekte
{

    public interface IPfadService
    {

        String Baugruppenpfad(IDictionary<String, AuIHKSDto> ihdic, AuIHKSDto objekt);
        String Baugruppenpfad(IDictionary<String, AuIHKSDto> ihdic, String nummer);
    }
}