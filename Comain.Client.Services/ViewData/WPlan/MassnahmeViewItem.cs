﻿using System;
using System.ComponentModel;
using System.Text;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;

namespace Comain.Client.ViewData.WPlan
{

    public class MassnahmeViewItem : NotifyBase, IDataErrorInfo
    {

        private String       name;
        private String       leistungsart;
        private String       gewerk;
        private String       externeAuftragsnummer;
        private int?         leistungsart_Id;
        private int?         gewerk_Id;
        private int          zyklus;

        public  int          Id                { get; set; }
        public  String       Maschine          { get; set; }
        public  String       Baugruppe         { get; set; }
        public  String       IHObjekt          { get; set; }
        public  String       Kurzbeschreibung  { get; set; }
        public  String       Nummer            { get; set; }
        public  Termintyp    Termintyp         { get; set; }
        public  Weekday      Tagesmuster       { get; set; }
        public  int?         Normzeit          { get; set; }
        public  String       Status            { get; set; }
        public  DateTime?    Letzter           { get; set; }
        public  DateTime?    Nächster          { get; set; }


        public String Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }


        public String ExterneAuftragsnummer
        {
            get => externeAuftragsnummer;
            set => SetProperty(ref externeAuftragsnummer, value);
        }


        public String Leistungsart
        {
            get => leistungsart;
            set => SetProperty(ref leistungsart, value);
        }


        public String Gewerk
        {
            get => gewerk;
            set => SetProperty(ref gewerk, value);
        }


        public int? Leistungsart_Id
        {
            get => leistungsart_Id;
            set => SetProperty(ref leistungsart_Id, value);
        }


        public int? Gewerk_Id
        {
            get => gewerk_Id;
            set => SetProperty(ref gewerk_Id, value);
        }


        public int Zyklus
        {
            get => zyklus;
            set => SetProperty(ref zyklus, value);
        }


        public String ZyklusAsString
            => WiMassnahme.ZyklusAsString(Termintyp, Zyklus, Tagesmuster);


        public String StatusAsColor
        {
            get {

                switch (Status) {

                    case "Aktiv":   return "LimeGreen";
                    case "Inaktiv": return "LightSlateGray";
                    default:        return "White";
                }
            }
        }


        public String TermintypAsString => Termintyp.AsText();


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this[nameof(Name)];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case nameof(Name):

                    if (String.IsNullOrEmpty(Name)) return "Geben Sie den Namen der Maßnahme an.";
                    if (Name.Length > 150) return "Der Name darf aus maximal 150 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
