﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Materialien
{

    public class Lieferant : Entity, IDataErrorInfo, IComparable<Lieferant> 
    {

        private String     name1;                 
        private String     name2;                 
        private String     strasse;               
        private String     plz;                   
        private String     ort;                   
        private String     telefon;               
        private String     fax;                   
        private String     eMail;                 
        private String     rechnungsanschrift;    
        private bool       lieferantFürIHObjekte; 
        private bool       lieferantFürAufträge;  


#region P R O P E R T I E S


        public TrackableCollection<LieferantAP> Ansprechpartnerliste    { get; set; }

        
        public String Name1                  
        {
            get { return name1; }
            set { SetProperty(ref name1, value); }
        }
        
        public String Name2               
        {
            get { return name2; }
            set { SetProperty(ref name2, value); }
        }
        
        public String Strasse              
        {
            get { return strasse; }
            set { SetProperty(ref strasse, value); }
        }
        
        public String Plz                  
        {
            get { return plz; }
            set { SetProperty(ref plz, value); }
        }
        
        public String Ort                   
        {
            get { return ort; }
            set { SetProperty(ref ort, value); }
        }
        
        public String Telefon            
        {
            get { return telefon; }
            set { SetProperty(ref telefon, value); }
        }
        
        public String Fax                    
        {
            get { return fax; }
            set { SetProperty(ref fax, value); }
        }
        
        public String EMail               
        {
            get { return eMail; }
            set { SetProperty(ref eMail, value); }
        }
        
        public String Rechnungsanschrift    
        {
            get { return rechnungsanschrift; }
            set { SetProperty(ref rechnungsanschrift, value); }
        }
        
        public bool LieferantFürIHObjekte  
        {
            get { return lieferantFürIHObjekte; }
            set { SetProperty(ref lieferantFürIHObjekte, value); }
        }
        
        public bool LieferantFürAufträge  
        {
            get { return lieferantFürAufträge; }
            set { SetProperty(ref lieferantFürAufträge, value); }
        }
        

#endregion

        
        public String VollerName
        {
            
            get {

                if (String.IsNullOrWhiteSpace(name2)) return name1;
                return String.Format("{0}, {1}", name1, name2);
            }
        }


        public String NameUndOrt
        {
            get {

                var name = String.IsNullOrWhiteSpace(Name2)? Name1 : String.Format("{0} {1}", Name1, Name2);
                return String.IsNullOrWhiteSpace(Ort)? name: String.Format("{0} ({1})", name, Ort);
            }
        }


        public String PlzUndOrt
        {
            get {
                return String.Format("{0} {1}", Plz, Ort).Trim();
            }
        }


#region V A L I D A T I O N 
        

        public string Error
        {
           
            get { 
            
                String test = this["Name1"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Name2"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Strasse"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Plz"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Ort"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Telefon"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Fax"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["EMail"];
                return test;
            }
        }

        
        public string this[string columnName]
        {
            
            get { 
            
            
                switch (columnName) {

                case "Name1":

                    if (String.IsNullOrWhiteSpace(Name1)) return "Geben Sie den Namen 1 des Lieferanten an."; 
                    if (Name1.Length > 100) return "Der Name 1 des Lieferanten darf aus maximal 100 Zeichen bestehen.";
                    break;

                case "Name2":

                    if (String.IsNullOrWhiteSpace(Name2)) return null; 
                    if (Name2.Length > 80) return "Der Name 2 des Lieferanten darf aus maximal 80 Zeichen bestehen.";
                    break;

                case "Strasse":

                    if (String.IsNullOrWhiteSpace(Strasse)) return null; 
                    if (Strasse.Length > 80) return "Die Strasse des Lieferanten darf aus maximal 80 Zeichen bestehen.";
                    break;

                case "Plz":

                    if (String.IsNullOrWhiteSpace(Plz)) return null; 
                    if (Plz.Length > 8) return "Die Postleitzahl des Lieferanten darf aus maximal 8 Zeichen bestehen.";
                    break;

                case "Ort":

                    if (String.IsNullOrWhiteSpace(Ort)) return null; 
                    if (Ort.Length > 48) return "Der Ort des Lieferanten darf aus maximal 48 Zeichen bestehen.";
                    break;

                case "Telefon":

                    if (String.IsNullOrWhiteSpace(Telefon)) return null; 
                    if (Telefon.Length > 254) return "Die Telefonnummer des Lieferanten darf aus maximal 254 Zeichen bestehen.";
                    break;

                case "Fax":

                    if (String.IsNullOrWhiteSpace(Fax)) return null; 
                    if (Fax.Length > 32) return "Die Faxnummer des Lieferanten darf aus maximal 32 Zeichen bestehen.";
                    break;

                case "EMail":

                    if (String.IsNullOrWhiteSpace(EMail)) return null; 
                    if (EMail.Length > 128) return "Die E-Mailadresse des Lieferanten darf aus maximal 128 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion


        public int CompareTo(Lieferant other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Name1.CompareNatural(other.Name1);
        }

      /*
        public override bool Equals(Object obj)
        {
            return Equals(obj as Lieferant);
        }


        public bool Equals(Lieferant other)
        {
            
            if (other == null) return false;
            return this.Id == other.Id;
        }


        public override Int32 GetHashCode()
        {
            return this.Id;
        }    */
    }
}
