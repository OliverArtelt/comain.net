﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Factories;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class IHTreeViewModel : ViewModel<IIHTreeView>, IFilterViewModel
    {

        private readonly IFilterService store;
        private readonly IHSelektorViewModel ihFilter;


        public IHTreeViewModel(IHSelektorViewModel ihFilter, IFilterService store, IIHTreeView view) : base(view)
        {

            this.ihFilter = ihFilter;
            this.store = store;
        }


        public void Reset()
        {
            ihFilter.Reset();
        }


        public String Validiere()
        {
            return null;
        }


        public string FilterAlsString()
        {

            if (IHObjekt != null) return String.Format("Asset {0}, {1}", IHObjekt.Nummer, IHObjekt.Name);
            return null;
        }


        public void AddFilter(Suchkriterien filter)
        {

            if (IHObjekt != null) filter.Add("IHObjekt", IHObjekt.Nummer);
        }


        public async Task LoadAsync(CancellationToken ct = default)
        {

            await ihFilter.LoadAsync(store.IHObjekte.Select(p => new IHFilterItem(p)).ToList(), store.Standorte, store.Kostenstellen, ct).ConfigureAwait(false);
            RaisePropertyChanged("IHObjekte");
        }


        public void Unload()
        {
            ihFilter.Unload();
        }


#region P R O P E R T I E S


        public object       IHSelektor  { get { return ihFilter.View; } }
        public IHFilterItem IHObjekt    { get { return ihFilter.CurrentIHObjekt; } }


#endregion

    }
}
