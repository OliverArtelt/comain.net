﻿using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.Stock.Bestellungen;

namespace Comain.Client.Views.Stock.Bestellungen
{

    public partial class BestellListView:UserControl   ,IBestellListView
    {
        public BestellListView()
        {
        InitializeComponent();
        }
 

        private void DataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {

            var vm = DataContext as BestellListViewModel;
            if (vm != null) vm.DetailCommand.Execute(sender);
        }
    }
}
