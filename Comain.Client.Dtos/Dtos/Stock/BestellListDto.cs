﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class BestellListDto
    {

        public int          Id                { get; set; }
        public String       Nummer            { get; set; }
        public DateTime?    AusgangAm         { get; set; }
        public String       Lieferant         { get; set; }
        public DateTime?    Liefertermin      { get; set; }
        public String       Status            { get; set; }
        public bool         Reklamiert        { get; set; }
        public decimal?     Gesamtpreis       { get; set; }
    }
}
