﻿using System;


namespace Comain.Client.Dtos.Aufträge
{

    public class AuftragListDto
    {

        public int              Id                      { get; set; }
        public int              Typ                     { get; set; }
        public int              Projekt                 { get; set; }
        public String           Nummer                  { get; set; }
        public String           Kurzbeschreibung        { get; set; }
        public DateTime         Auftragsdatum           { get; set; }
        public String           IHObjektNummer          { get; set; }
        public String           Maschine                { get; set; }
        public String           Auftraggeber            { get; set; }
        public String           Dringlichkeit           { get; set; }
        public DateTime?        Termin                  { get; set; }
        public String           ExterneNummer           { get; set; }
        public String           Kostenstelle            { get; set; }
        public String           Gewerk                  { get; set; }
        public String           Status                  { get; set; }
        public decimal?         Stunden                 { get; set; }
        public bool?            HatLeistungen           { get; set; }
    }
}
