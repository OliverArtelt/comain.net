﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Emma;
using Comain.Client.ViewModels;
using Comain.Client.Models.Emma;

namespace Comain.Client.ViewModels.Emma
{

    public class EmmaListViewModel : ViewModel<IEmmaListView>
    {

        private readonly SynchronizationContext context;


        public EmmaListViewModel(IEmmaListView view)
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public async Task LoadAsync(IEnumerable<Leistung> rawlist)
        {

            Foundlist = null;
            if (rawlist != null) {

                await context.SendAsync(new SendOrPostCallback((o) => { 
                
                    Foundlist = new TrackableCollection<Leistung>(rawlist); 
                    Foundlist.AcceptChanges();

                }), null);
            }
        }


        public void Unload()
            => Foundlist = null;


        public bool IstWochenbericht
        {
            get {

                if (Foundlist.IsNullOrEmpty()) return false;
                var min = Foundlist.Min(p => p.EmmaVon);
                var max = Foundlist.Max(p => p.EmmaVon);
                var weekend = min.MondayOf().AddDays(7);
                if (max > weekend) return false;
                return min.Date < max.Date;
            }
        }


#region P R O P E R T I E S


        private TrackableCollection<Leistung> foundlist;
        public TrackableCollection<Leistung> Foundlist
        {
            get { return foundlist; }
            set { SetProperty(ref foundlist, value); }
        }


        private Leistung current;
        public Leistung Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


#endregion

    }
}
