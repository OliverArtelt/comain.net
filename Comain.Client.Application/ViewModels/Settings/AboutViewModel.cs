﻿using System;
using System.Waf.Applications;
using Comain.Client.Views.Settings;


namespace Comain.Client.ViewModels.Settings
{
   
    public class AboutViewModel : ViewModel<IAboutView>
    {

        public AboutViewModel(IAboutView view, ISettings settings) : base(view)
        {

            Produkt = settings.ProductName;
            Version = settings.ApplicationVersion;
        }


#region P R O P E R T I E S
   

        public String Produkt { get; private set; }
        public String Version { get; private set; }


#endregion


    }
}
