﻿using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewModels.Tickets;
using Comain.Client.ViewModels;

namespace Comain.Client.Controllers.Tickets
{

    public class TicketFormsMediator : ITicketFormsMediator
    {

        private readonly ILoginService login;
        private readonly TicketViewModel view;
        private readonly TicketListViewModel listView;
        private readonly TicketFilterViewModel filterView;
        private readonly TicketDetailViewModel detailView;
        private readonly TicketDetailBasisViewModel basisView;
        private readonly TicketDetailHistorieViewModel historieView;
        private readonly TicketDetailLeistungViewModel leistungView;
        private readonly TicketDetailPersonalViewModel personalView;
        private readonly TicketDetailAbnahmeViewModel abnahmeView;


        public TicketFormsMediator(ILoginService login, TicketViewModel view, TicketListViewModel listView, TicketFilterViewModel filterView, 
                                   TicketDetailViewModel detailView, TicketDetailBasisViewModel basisView, TicketDetailHistorieViewModel historieView,
                                   TicketDetailLeistungViewModel leistungView, TicketDetailPersonalViewModel personalView, TicketDetailAbnahmeViewModel abnahmeView)
        {

            this.login = login;
            this.view = view;
            this.listView = listView;
            this.detailView = detailView;
            this.filterView = filterView;
            this.basisView = basisView;
            this.historieView = historieView;
            this.leistungView = leistungView;
            this.personalView = personalView;
            this.abnahmeView = abnahmeView;
        }


        private bool        HasCurrent      { get { return detailView.Current != null; } }
        public  EditMode    CurrentMode     { get; private set; }
        

        /// <summary>
        /// darf Ticket von der logische Abfolge her noch bearbeitet werden? 
        /// </summary>
        private bool TicketIstFertig      
        { 
            get { 
            
                if (detailView.Current == null) return false; 
                if (detailView.Current.Status == Ticketstatus.Abgenommen) return true; 
                if (detailView.Current.Status == Ticketstatus.Abgeschlossen) return true; 
                if (detailView.Current.Status == Ticketstatus.Storniert) return true; 
                return false;
            } 
        }


        public void SetEditMode(EditMode newMode)
        {

            CurrentMode = newMode;
            listView.EditEnabled = CurrentMode == EditMode.ReadOnly;
            filterView.EditEnabled = CurrentMode == EditMode.ReadOnly;

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            Ticket au = detailView.Current;
            Nutzer ps = login.AktuellerNutzer;

            //im Editmodus
            //kein Storno
            //kein Abgeschlossen
            view.SaveVisible      = au != null && (CurrentMode == EditMode.Edit || CurrentMode == EditMode.Append) && au.Status != Ticketstatus.Abgeschlossen && au.Status != Ticketstatus.Storniert &&
                                    (ps.IstAdministrator || ps.IstTicketNutzer);
            //nicht im Editmodus
            //kein Storno
            //kein Abgeschlossen
            view.EditVisible      = au != null && CurrentMode == EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung || ps.IstTicketNutzer && !TicketIstFertig);
            //im Editmodus
            view.CancelVisible    = au != null && (CurrentMode == EditMode.Edit || CurrentMode == EditMode.Append) &&
                                    (ps.IstAdministrator || ps.IstTicketNutzer);
            //nicht im Editmodus
            view.ReloadVisible    = CurrentMode == EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketNutzer);
            //nicht im Editmodus
            view.AddVisible       = CurrentMode == EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketSchichtführer || ps.IstTicketInstandhalter);
            //noch keine Leistungen
            view.DeleteVisible    = au != null && CurrentMode == EditMode.ReadOnly && 
                                    (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung || ps.IstTicketSchichtführer);
            //kein Storno
            //kein Abgeschlossen
            view.CloseVisible     = au != null && CurrentMode == EditMode.ReadOnly && au.Status != Ticketstatus.Abgeschlossen && au.Status != Ticketstatus.Storniert &&
                                    (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung);
            //kein Storno
            //kein Abgeschlossen
            view.StornoVisible    = au != null && CurrentMode == EditMode.ReadOnly && au.Status != Ticketstatus.Abgeschlossen && au.Status != Ticketstatus.Storniert && 
                                    (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung);

            //Exportieren und Öffnen angehängter Bilder darf jeder
            view.SaveImageVisible = au != null && (ps.IstAdministrator || ps.IstTicketNutzer) && au.Typ == Auftragstyp.BesonderesVorkommnis && detailView.Tab == TicketDetailViewModel.Tabs.Media;
            view.OpenImageVisible = au != null && (ps.IstAdministrator || ps.IstTicketNutzer) && au.Typ == Auftragstyp.BesonderesVorkommnis && detailView.Tab == TicketDetailViewModel.Tabs.Media;


            detailView.BasisViewVisible    = ps.IstAdministrator || ps.IstTicketNutzer;
            detailView.PersonalViewVisible = ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung || ps.IstTicketSchichtführer;
            detailView.LeistungViewVisible = ps.IstAdministrator || (ps.IstTicketInstandhalter || ps.IstTicketLeiterInstandhaltung) && !TicketIstFertig;
            detailView.AbnahmeViewVisible  = ps.IstAdministrator || ps.IstTicketSchichtführer && !TicketIstFertig;
            detailView.HistorieViewVisible = ps.IstAdministrator || ps.IstTicketNutzer;
            detailView.MediaViewVisible    = (ps.IstAdministrator || ps.IstTicketNutzer) && au != null && !au.IstAngefügt && au.Typ == Auftragstyp.BesonderesVorkommnis;
            detailView.PrintViewVisible    = (ps.IstAdministrator || ps.IstTicketNutzer) && au != null && !au.IstAngefügt;

            basisView.EditEnabled    = au != null && CurrentMode != EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung || ps.IstTicketNutzer && !TicketIstFertig);
            personalView.EditEnabled = au != null && CurrentMode != EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketLeiterInstandhaltung || 
                                       ((ps.IstTicketLeiterProduktion || ps.IstTicketSchichtführer) && !TicketIstFertig));
            leistungView.EditEnabled = au != null && CurrentMode != EditMode.ReadOnly && (ps.IstAdministrator || (ps.IstTicketInstandhalter || ps.IstTicketLeiterInstandhaltung) && !TicketIstFertig);
            abnahmeView.EditEnabled  = au != null && CurrentMode != EditMode.ReadOnly && (ps.IstAdministrator || ps.IstTicketSchichtführer && !TicketIstFertig);
        }
    } 
}
