﻿using System.Collections;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Artikelstamm;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{
    
    public class ArtikelDetailLfViewModel : ValidatableViewModel<IArtikelDetailLfView, Artikel>
    {
 
        private readonly SynchronizationContext context;


        public ArtikelDetailLfViewModel(IArtikelDetailLfView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public override void CurrentChanged(Artikel oldSelected)
        {
            RaisePropertyChanged("Lieferanten");    
        }

        
        public void Unload()
        {
        }  


#region P R O P E R T I E S
  
        
        private bool editEnabled;
        public new bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }

        
        public IEnumerable Lieferanten
        { 
            get { return current == null? null: current.MaterialLieferantMap; }
        }
 

        private ICommand lieferantCommand;
        public ICommand LieferantCommand
        { 
            get { return lieferantCommand; }
            set { SetProperty(ref lieferantCommand, value); }
        }


#endregion

    }
}
