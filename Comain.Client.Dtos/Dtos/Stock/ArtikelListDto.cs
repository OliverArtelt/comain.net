﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class ArtikelListDto : ChangeTrackingBase
    {

        public String       Nummer              { get; set; }
        public String       Name                { get; set; }
        public String       Warengruppe         { get; set; }
        public String       IHObjekte           { get; set; }
        public String       Lieferanten         { get; set; }
        public decimal?     Mindestbestand      { get; set; }
        public decimal?     Mindestbestellmenge { get; set; }
        public decimal?     Bestellmenge        { get; set; }
        public decimal?     BestandReal         { get; set; }
        public decimal?     Mischpreis          { get; set; }
        public DateTime?    DeaktiviertAm       { get; set; }
    }
}
