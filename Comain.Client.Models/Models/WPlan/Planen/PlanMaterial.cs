﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{

    [DebuggerDisplay("{Nummer} {Name}: {Parent.FälligAm}")]
    public class PlanMaterial
    {

        public PlanMaterial(Planaufgabe parent, MaterialInfo info)
        {
            Info = info;
            Parent = parent;
        }


        public MaterialInfo   Info                  { get; }
        public Planaufgabe    Parent                { get; }
        public decimal        MengeZugewiesen       { get; set; }


        public int            Id                    => Info.Id;
        public int?           MaterialId            => Info.MaterialId;
        public String         Name                  => Info.Name;
        public String         Nummer                => Info.Nummer;
        public bool           Erforderlich          => Info.Erforderlich;
        public decimal        MengeBenötigt         => Erforderlich? Info.MengeNormalisiert: 0m;
        public decimal?       GPreis                => MengeBenötigt * Info.EPreis;
        public String         MengeBenötigtText     => Info.MengeEinheitText;
        public String         MengeZugewiesenText   => String.IsNullOrEmpty(Info.Einheit)? MengeZugewiesen.ToString(): $"{MengeZugewiesen} {Info.Einheit}";
        public bool           IstFehlend            => MengeZugewiesen < MengeBenötigt;
    }
}
