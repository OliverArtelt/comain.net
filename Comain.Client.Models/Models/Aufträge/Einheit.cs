﻿using System;
using System.ComponentModel;


namespace Comain.Client.Models.Aufträge
{

    public class Einheit : Entity, IDataErrorInfo, IComparable<Einheit>
    {

        private String name;
        private String nummer;
        private bool istFremdleistung;


#region P R O P E R T I E S

        
        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
        
        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        
        public bool IstFremdleistung
        {
            get { return istFremdleistung; }
            set { SetProperty(ref istFremdleistung, value); }
        }


#endregion

        
        public int CompareTo(Einheit other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareTo(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie die Bezeichnung an.";
                    if (Name.Length > 150) return "Die Bezeichnung darf aus maximal 150 Zeichen bestehen."; 
                    break;

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Einheit an.";
                    if (Nummer.Length > 10) return "Die Einheit darf aus maximal 10 Zeichen bestehen."; 
                    break;
                }

                return null;
            }
        }

#endregion
 
    }
}
