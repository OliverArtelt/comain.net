﻿using System;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Kombinationsobjekt - hält eine Kombination aus Fliegengewichten jeweils eines der anderen Stilklassen
    /// </summary>
    public class CellXf : ICellStyleType, IEquatable<CellXf>
    {

        int fontindex;
        int formatindex;
        int borderindex;
        int backcolorindex;
        int alignindex;

        /// <summary>
        /// Sonderfall: Ausrichtungsobjektknoten im Dokument dem Kombinationsknoten unterordnen: Ausrichtungsobjektindex auslesen
        /// </summary>
        public int AlignIndex { get { return alignindex; } }


        public CellXf()                                            : this(0, 0, 0, 0, 0) {}

        public CellXf(int font)                                    : this(font, 0, 0, 0, 0) {}

        public CellXf(int font, int format)                        : this(font, format, 0, 0, 0) {}

        public CellXf(int font, int format, int border)            : this(font, format, border, 0, 0) {}

        public CellXf(int font, int format, int border, int color) : this(font, format, border, color, 0) {}

        public CellXf(int font, int format, int border, int color, int align)
        {
        
            fontindex = font;
            formatindex = format;
            borderindex = border;
            backcolorindex = color;
            alignindex = align;
        }


        public bool IsDefault()
        {
            return fontindex == 0 && formatindex == 0 && borderindex == 0 && backcolorindex == 0 && alignindex == 0;
        }

        
        public void WriteXML(XElement parent)
        {
         
            XNamespace ns = parent.Name.Namespace;

            XElement x = new XElement(ns + "xf",
                new XAttribute("numFmtId",  formatindex),
                new XAttribute("fontId",    fontindex),
                new XAttribute("fillId",    backcolorindex),
                new XAttribute("borderId",  borderindex),
                new XAttribute("xfId",      0));
            
            if (fontindex != 0)      x.Add(new XAttribute("applyFont", 1));
            if (formatindex != 0)    x.Add(new XAttribute("applyNumberFormat", 1));
            if (borderindex != 0)    x.Add(new XAttribute("applyBorder", 1));
            if (backcolorindex != 0) x.Add(new XAttribute("applyFill", 1));
            if (alignindex != 0)     x.Add(new XAttribute("applyAlignment", 1));
            
            parent.Add(x);
        }

        
        public bool Equals(CellXf other)
        {
            
            return this.fontindex      == other.fontindex      &&  
                   this.formatindex    == other.formatindex    &&
                   this.borderindex    == other.borderindex    && 
                   this.backcolorindex == other.backcolorindex &&
                   this.alignindex     == other.alignindex;
        }
    }
}
