﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Views.Anwesenheiten;

namespace Comain.Client.ViewModels.Anwesenheiten
{

    public class LeistungFilterViewModel : ViewModel<ILeistungFilterView>
    {

        public LeistungFilterViewModel(ILeistungFilterView view) : base(view)
        { }


        public void Unload()
        {

            Leistungsarten = null;
            Gewerke = null;
        }


        public void ResetFilter()
        {

            SearchText = null;
            AssignedOnly = false;
            Leistungsart = null;
            Gewerk = null;
        }


        #region P R O P E R T I E S


        private bool assignedOnly;
        public bool AssignedOnly
        {
            get => assignedOnly;
            set => SetProperty(ref assignedOnly, value);
        }


        private String searchText;
        public String SearchText
        {
            get => searchText;
            set => SetProperty(ref searchText, value);
        }


        private IEnumerable<LeistungsartFilterModel> leistungsarten;
        public IEnumerable<LeistungsartFilterModel> Leistungsarten
        {
            get => leistungsarten;
            set => SetProperty(ref leistungsarten, value);
        }


        private LeistungsartFilterModel leistungsart;
        public LeistungsartFilterModel Leistungsart
        {
            get => leistungsart;
            set => SetProperty(ref leistungsart, value);
        }


        private IEnumerable<NummerFilterModel> gewerke;
        public IEnumerable<NummerFilterModel> Gewerke
        {
            get => gewerke;
            set => SetProperty(ref gewerke, value);
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get => gewerk;
            set => SetProperty(ref gewerk, value);
        }


        private ICommand resetCommand;
        public ICommand ResetCommand
        {
            get => resetCommand;
            set => SetProperty(ref resetCommand, value);
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        {
            get => searchCommand;
            set => SetProperty(ref searchCommand, value);
        }


        #endregion

    }
}
