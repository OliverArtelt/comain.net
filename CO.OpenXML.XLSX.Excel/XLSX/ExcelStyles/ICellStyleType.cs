﻿using System;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Stilobjekte definieren
    /// </summary>
    public interface ICellStyleType
    {
    
        /// <summary>
        /// Entscheiden ob Stil Standardstil ist
        /// </summary>
        /// <returns></returns>
        bool IsDefault();
        /// <summary>
        /// Stilknoten an das Dokument anhängen
        /// </summary>
        /// <param name="parent">Sammelknoten der die Knoten listet</param>
        void WriteXML(XElement parent);
    }
}
