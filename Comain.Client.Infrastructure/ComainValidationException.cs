﻿using System;
using System.Collections.Generic;


namespace Comain
{

    [Serializable]
    public class ComainValidationException : UserException
    {

        public IList<String> Explanations   { get; private set; }


        public ComainValidationException(String message, IList<String> explanations) : base(message)
        {
            Explanations = explanations;
        }


        public ComainValidationException(String message, IList<String> explanations, Exception source) : base(message, source)
        {
            Explanations = explanations;
        }


        public ComainValidationException(ExceptionDto dto) : base(dto.Message)
        {
            Explanations = dto.Explanations;
        }


        public override String Explain()
        {

            if (Explanations.IsNullOrEmpty()) return Message;
            var list = new List<String> { Message };
            list.AddRange(Explanations);
            return String.Join(System.Environment.NewLine, list);
        }
    }
}
