﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Views.Verwaltung;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class SubListViewModel : ViewModel<ISubListView>, IDelayableViewModel
    {

        protected readonly SynchronizationContext context;


        public SubListViewModel(ISubListView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        private bool Predicate(object arg)
        {

            var sub = arg as SubNummerItem;
            if (sub == null) return false;
            if (Mainitem == null) return false;

            return Mainitem.Id == sub.Haupt_Id;
        }


        private void Refresh()
        {
            
            context.Post(new SendOrPostCallback((o) => { 
            
                if (Sublist != null) {
                
                    Sublist.SortDescriptions.Clear();
                    Sublist.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                    Sublist?.Refresh();        
                }
                RaisePropertyChanged("Sublist");
            }), null);
        }   


        public void Unload()
        {
            
            Mainlist = null;
            RaisePropertyChanged("Mainlist");
            Sublist = null;
            RaisePropertyChanged("Sublist");
        }


        public void SetLists(IEnumerable<NummerItem> mainlist, IEnumerable<SubNummerItem> sublist)
        {

            Mainlist = mainlist;
            RaisePropertyChanged("Mainlist");
            Sublist = new CollectionViewSource { Source = sublist }.View;
            Sublist.Filter = Predicate;
            Refresh();
        }


#region P R O P E R T I E S

        
        public IEnumerable Mainlist { get; private set; }
        
       
        private NummerItem mainitem;   
        public NummerItem Mainitem   
        {
            get { return mainitem; }
            set { if (SetProperty(ref mainitem, value)) Refresh(); }
        }

        
        public ICollectionView Sublist { get; private set; }
        
       
        private SubNummerItem subitem;   
        public SubNummerItem Subitem   
        {
            get { return subitem; }
            set { SetProperty(ref subitem, value); }
        }


        private String itemname;
        public String Itemname
        { 
            get { return itemname; }
            set { 
            
                if (SetProperty(ref itemname, value)) {
                
                    RaisePropertyChanged("ItemAddCommandName");   
                    RaisePropertyChanged("ItemDeleteCommandName");   
                }
            }
        }


        private String subItemname;
        public String SubItemname
        { 
            get { return subItemname; }
            set { 
            
                if (SetProperty(ref subItemname, value)) {
                
                    RaisePropertyChanged("SubItemAddCommandName");   
                    RaisePropertyChanged("SubItemDeleteCommandName");   
                }
            }
        }
        

        public String ItemAddCommandName        { get { return Itemname + " anfügen"; } }
        
        public String ItemDeleteCommandName     { get { return Itemname + " löschen"; } }
        
        public String SubItemAddCommandName     { get { return SubItemname + " anfügen"; } }
        
        public String SubItemDeleteCommandName  { get { return SubItemname + " löschen"; } }


        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }
  

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
   

        private ICommand cancelCommand;
        public ICommand CancelCommand
        { 
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }
   

        private ICommand addMainCommand;
        public ICommand AddMainCommand
        { 
            get { return addMainCommand; }
            set { SetProperty(ref addMainCommand, value); }
        }
   

        private ICommand deleteMainCommand;
        public ICommand DeleteMainCommand
        { 
            get { return deleteMainCommand; }
            set { SetProperty(ref deleteMainCommand, value); }
        }
   

        private ICommand addSubCommand;
        public ICommand AddSubCommand
        { 
            get { return addSubCommand; }
            set { SetProperty(ref addSubCommand, value); }
        }
   

        private ICommand deleteSubCommand;
        public ICommand DeleteSubCommand
        { 
            get { return deleteSubCommand; }
            set { SetProperty(ref deleteSubCommand, value); }
        }

       
#endregion   

    }
}
