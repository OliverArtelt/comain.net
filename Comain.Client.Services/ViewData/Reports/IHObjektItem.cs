﻿using System;
using Comain.Client.Dtos.Reports;

namespace Comain.Client.ViewData.Reports
{

    public class IHObjektItem
    {
        
        public String   Nummer                  { get; private set; }
        public String   Name                    { get; private set; }
        public String   Inventarnummer          { get; private set; }
        public String   Fabriknummer            { get; private set; }
        public int      Position                { get; private set; }
    
    
        public IHObjektItem(ArtikelIhoMap model)
        {

            Nummer         = model.IHObjekt.Nummer;        
            Name           = model.IHObjekt.Name;          
            Inventarnummer = model.IHObjekt.Inventarnummer;
            Fabriknummer   = model.IHObjekt.Fabriknummer;  
            Position       = model.Position;  
        }
    }
}
