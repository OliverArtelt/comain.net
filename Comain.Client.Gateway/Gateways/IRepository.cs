﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Gateways
{

    public interface IRepository
    {

        Task        LoginAsync(ConnectionEndpoint endpoint, CancellationToken ct = default);
        Task        LogoutAsync(CancellationToken ct = default);
        
        Task<T>     FindAsync<T>(String uri, Suchkriterien filter, CancellationToken ct = default);
        Task<T>     GetAsync<T>(String uri, CancellationToken ct = default);

        Task        PostAsync<T>(String uri, T dto, CancellationToken ct = default);
        Task<TDst>  PostAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default);
        Task        PutAsync<T>(String uri, T dto, CancellationToken ct = default);
        Task<TDst>  PutAsync<TSrc, TDst>(String uri, TSrc dto, CancellationToken ct = default);
        Task        DeleteAsync(String uri, CancellationToken ct = default);
    }
}
