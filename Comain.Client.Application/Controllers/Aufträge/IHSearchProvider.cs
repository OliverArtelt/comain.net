﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using FeserWard.Controls;


namespace Comain.Client.Controllers.Aufträge
{

    public interface IIHSearchProvider : IIntelliboxResultsProvider
    {

        KstFilterModel                      Kostenstelle    { get; set; }
        PzFilterModel                       Standort        { get; set; }
        IEnumerable<IHObjektFilterModel>    IHObjekte       { get; set; }
    }


    public class IHSearchProvider : IIHSearchProvider
    {

        public KstFilterModel                      Kostenstelle    { get; set; }
        public PzFilterModel                       Standort        { get; set; }
        public IEnumerable<IHObjektFilterModel>    IHObjekte       { get; set; }

        
        public IEnumerable DoSearch(String searchTerm, int maxResults, object extraInfo)
        {

            if (IHObjekte == null) yield break;
            if (String.IsNullOrWhiteSpace(searchTerm)) yield break;

            int count = 0;
            foreach (var ih in IHObjekte) {

                if (Kostenstelle != null && ih.Kostenstelle_Id != Kostenstelle.Id) continue;
                if (Standort != null && ih.Standort_Id != Standort.Id) continue;

                if (ih.Nummer.StartsWith(searchTerm) ||
                    ih.Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
                    ih.Inventarnummer == searchTerm) {

                    count++;
                    if (count > maxResults) yield break;
                      
                    yield return ih;
                }
            }
        }


        public IEnumerable GetAll(object extraInfo)
        {

            if (IHObjekte.IsNullOrEmpty()) yield break;
            foreach (var ih in IHObjekte) yield return ih;
        }
    }
}
