﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Alle Positionen die in einem Wartungsauftrag zusammengefaßt sind: 'Was hat der Instandhalter jetzt alles an dieser Maschine zu erledigen'
    /// </summary>
    /// <remarks>
    /// wir verwenden einfach die erste Position da sie alle denselben Auftrag zugeordnet sind 
    /// (Auftrag wird in der Planung nicht verwendet und muß deshalb so blöd rückgeschlossen werden)
    /// </remarks>
    public class Planauftrag 
    {

        public List<Planaufgabe>    Positionen          { get; }

        public int                  AuftragId           => Positionen[0].AuftragId;
        public String               Auftrag             => Positionen[0].Nummer;
        public Auftragstatus?       Auftragstatus       => Positionen[0].Auftragstatus;
        public DateTime?            Termin              => Positionen[0].FälligAm;
        public String               Auftragnehmer       => Positionen[0].Auftragnehmer;
        public String               Kurzbeschreibung    => Positionen[0].Kurzbeschreibung;
        public String               IHObjekt            => Positionen[0].IHObjekt;
        public String               Maschine            => Positionen[0].Maschine;
        public String               Gewerk              => Positionen[0].Gewerk;
        public String               Leistungsart        => Positionen[0].Leistungsart;
        public String               Kostenstelle        => Positionen[0].Kostenstelle;

        public bool                 IstSelektiert       => Positionen != null && Positionen.Any(p => p.IstSelektiert);
        public bool                 IstSichtbar         => Positionen != null && Positionen.Any(p => p.IstSichtbar);


        public Planauftrag(IEnumerable<Planaufgabe> poslist)
        {

            if (poslist.IsNullOrEmpty()) throw new ComainOperationException("Positionsliste darf nicht leer sein.");
            Positionen = poslist.OrderBy(p => p.FälligAm).ToList();
        }
    }
}
