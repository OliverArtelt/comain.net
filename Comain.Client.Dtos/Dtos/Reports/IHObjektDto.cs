﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Dtos.Reports
{

    public class IHObjektDto
    {

        public String   IHObjektNummer                  { get; set; }
        public String   Inventarnummer                  { get; set; }
        public String   Maschine                        { get; set; }
        public String   Baugruppe                       { get; set; }
        public String   Bauteil                         { get; set; }
        public String   Hersteller                      { get; set; }
        public String   Typ                             { get; set; }
        public String   Fabriknummer                    { get; set; }
        public String   Anlage                          { get; set; }
        public String   TechnischeDaten                 { get; set; }
        public String   Bemerkungen                     { get; set; }
        public String   Kostenstelle                    { get; set; }
        public short?   Baujahr                         { get; set; }
        public short?   Anschaffungsjahr                { get; set; }
        public String   Status                          { get; set; }
        public String   Erhaltklasse                    { get; set; }
        public String   VerantwIH                       { get; set; }
        public String   VerantwProd                     { get; set; }
        
        public decimal? Anschaffung                     { get; set; }
        public decimal? Wiederbeschaffung               { get; set; }
        public decimal? Störkosten                      { get; set; }
        public decimal? Ausfallkosten                   { get; set; }
        public int      Störzeit                        { get; set; }
        public int      Ausfallzeit                     { get; set; }
        public int      Störanzahl                      { get; set; }

        public IEnumerable<NameWert> Schadensbild       { get; set; }
        public IEnumerable<NameWert> Schadensursache    { get; set; }
    }
}
