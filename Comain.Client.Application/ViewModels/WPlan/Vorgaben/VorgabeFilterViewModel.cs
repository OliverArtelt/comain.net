﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.WPlan.Vorgaben;

namespace Comain.Client.ViewModels.WPlan.Vorgaben
{

    public class VorgabeFilterViewModel : ViewModel<IVorgabeFilterView>
    {
 
        public VorgabeFilterViewModel(IVorgabeFilterView view) 
          : base(view)
        {
        }


        public void Unload()
        {
        }


        public void Clear()
        {
            NurAktive = false;
        }
         

#region P R O P E R T I E S

   
        private String resultText;
        public String ResultText
        { 
            get { return resultText; }
            set { SetProperty(ref resultText, value); }
        }

   
        private bool editEnabled;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }

   
        private bool nurAktive;
        public bool NurAktive
        { 
            get { return nurAktive; }
            set { SetProperty(ref nurAktive, value); }
        }


        private ICommand searchCommand;
        public ICommand SearchCommand
        { 
            get { return searchCommand; }
            set { SetProperty(ref searchCommand, value); }
        }


        private ICommand resetCommand;
        public ICommand ResetCommand
        { 
            get { return resetCommand; }
            set { SetProperty(ref resetCommand, value); }
        }
    

#endregion

    }
}
