﻿using System;


namespace Comain.Client.Models.Users
{

    public class LoginUser
    {

        public String       Id              { get; set; }
        public String       UserName        { get; set; }
        public String       Email           { get; set; }
        public DateTime     Created         { get; set; }
        public DateTime     Updated         { get; set; }
        public DateTime?    LastLogin       { get; set; }
        public String       Name1           { get; set; }
        public String       Name2           { get; set; }
        public String       Beschreibung    { get; set; }


        public String Fullname
        {
            get {

                if (!String.IsNullOrEmpty(Name2) && !String.IsNullOrEmpty(Name1)) return String.Format("{0}, {1}", Name1, Name2);
                if (!String.IsNullOrEmpty(Name1)) return Name1;
                return UserName;
            }
        }
    }
}
