﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Commands;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Controllers.Tickets;

namespace Comain.Client.Commands
{
    public class AddTicketCommand : IControllerCommand 
    {

        public Type         Controller  => typeof(IAddTicketController);
        public String       Name        => "Neues Ticket"; 
        public String       Parameter   => "Ticket.Add"; 
        public String       Gruppe      => "Ticket";
        public int          Sortierung  => 100;
        public String       Color       => "BlueViolet";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.ticket.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;
    
       
        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {
        
            if (nutzer == null || !module.VerwendetModulTicket) return false;
            return nutzer.IstAdministrator || nutzer.IstTicketNutzer;    
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
