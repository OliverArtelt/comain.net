﻿using System;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Verwaltung
{

    public class LieferantAPDto : ChangeTrackingBase
    {
        
        public String       Name            { get; set; }
        public String       Vorname         { get; set; }
        public String       Telefon         { get; set; }
        public String       Fax             { get; set; }
        public String       EMail           { get; set; }
        public int          Lieferant_Id    { get; set; }
    }
}
