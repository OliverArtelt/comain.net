﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class WLFilterDto
    {

        public int              Wareneinlagerung_Id     { get; set; }
        public int              Lagerort_Id             { get; set; }
        public int?             Lieferant_Id            { get; set; }
        public int              Material_Id             { get; set; }
        public int?             Einheit_Id              { get; set; }
        public decimal          Restmenge               { get; set; }
        public DateTime         Datum                   { get; set; }
        public String           Lieferant               { get; set; }
        public String           Lagerort                { get; set; }
        public String           Wareneingang            { get; set; }
    }
}
