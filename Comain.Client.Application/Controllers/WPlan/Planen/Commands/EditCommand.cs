﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Controllers;
using Comain.Client.Trackers;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Controllers.WPlan.Planen.Commands
{

    public class EditCommand : IUndoRedoCommand, IResultCommand
    {

        private readonly IMapper mapper;
        private readonly List<Tuple<EditAction, Planaufgabe>> models;
        private readonly EditAction action;


        public EditCommand(IEnumerable<Planaufgabe> models, EditAction action, IMapper mapper)
        {

            this.models = models.Select(p => new Tuple<EditAction, Planaufgabe>(new EditAction { Ergebnis = p.Ergebnis, Menge = p.Menge, HeuteErledigt = action.HeuteErledigt }, p)).ToList();
            this.action = action;
            this.mapper = mapper;
        }


        public void Redo() => models.ForEach(p => { p.Item2.Edit(action); });
        public void Undo() => models.ForEach(p => { p.Item2.Edit(p.Item1); });


        public bool ModelHasChanged => true;


        public IEnumerable<PositionSubmitDto> Modifications() 
        {

            var dtos = mapper.Map<List<PositionSubmitDto>>(models.Select(p => p.Item2));
            dtos.ForEach(p => p.ChangeStatus = ChangeStatus.Changed);
            return dtos;
        }
    }
}
