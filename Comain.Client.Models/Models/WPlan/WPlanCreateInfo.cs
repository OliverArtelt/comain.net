﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan
{

    /// <summary>
    /// Filterangaben für die Serveranfrage
    /// </summary>
    public class WPlanCreateInfo
    {

        /// <summary>
        /// mit Kommata getrennte Assetnummern
        /// </summary>
        public String           IHList                  { get; set; }
        public DateTime?        StartDate               { get; set; }
        public DateTime?        EndDate                 { get; set; }
        public int?             Leistungsart            { get; set; }
        public int?             Gewerk                  { get; set; }
        public int?             Personal                { get; set; }
        public bool             NurRelevante            { get; set; }

        public Planungstyp      Planungstyp             { get; set; }
        public Planungsstatus   Planungsstatus          { get; set; }

        public String           CallbackConnectionId    { get; set; }
    }
}
