﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Models.WPlan;

namespace Comain.Client.ViewData.WPlan
{

    /// <summary>
    /// Wartungsposition auf dem Zeitstrahl einer Maßnahme
    /// </summary>
    public class EreignisViewItem 
    {

        private readonly EreignisDto dto;


        public EreignisViewItem(EreignisDto dto)
        {
            this.dto = dto;
        }


        public int                PositionId            => dto.PositionId;
        public int                AuftragId             => dto.AuftragId;
        public String             Auftrag               => dto.Auftragsnummer;
        public Checklistergebnis  Ergebnis              => (Checklistergebnis)dto.Ergebnis;
        public String             Status                => ((Checklistergebnis)dto.Ergebnis).AsText();
        public String             FortgeführtAus        => dto.FortgeführtAus;
        public DateTime?          FälligAm              => dto.FälligAm;
        public DateTime?          DurchgeführtAm        => dto.DurchgeführtAm;
        public int?               Minuten               => dto.MinutenGearbeitet;
        public String             Auftragnehmer         => dto.Auftragnehmer;
        public bool               AuftragIstStornierbar => dto.AuftragIstStornierbar;


        public bool IstOffen => Ergebnis == Checklistergebnis.Undefiniert;

        public bool IstÜberfällig => Ergebnis == Checklistergebnis.Undefiniert && FälligAm.HasValue && FälligAm.Value.MondayOf() < DateTime.Today.MondayOf();


        public String StatusAsColor
        { 
            get {

                if (IstÜberfällig) return "GoldenRod";
                if (Ergebnis == Checklistergebnis.OK) return "LimeGreen";
                if (Ergebnis == Checklistergebnis.NotOK) return "Tomato";
                if (Ergebnis == Checklistergebnis.WirdNichtErledigt) return "DarkOrchid";
                if (Ergebnis == Checklistergebnis.Fortgeführt) return "DodgerBlue";
                return FälligAm.HasValue && FälligAm.Value.Date.MondayOf() < DateTime.Today.MondayOf()? "#ffef99": "White";
            }
        }
    }
}
