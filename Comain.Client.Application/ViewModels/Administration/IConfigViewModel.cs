﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Models;


namespace Comain.Client.ViewModels.Administration
{

    public interface IConfigViewModel : IDelayableViewModel
    {

        ICommand        SaveCommand     { get; set; }
        Konfiguration   Current         { get; set; }
        
        void Unload();
    }
}
