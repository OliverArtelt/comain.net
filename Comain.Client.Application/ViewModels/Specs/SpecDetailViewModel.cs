﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Models.Specs;
using Comain.Client.Views.Specs.Verwaltung;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Specs.Verwaltung
{

    public class SpecDetailViewModel : ValidatableViewModel<ISpecDetailView, Spezifikation>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Massnahmen, Artikel, Dokumente, Report }


        private readonly SynchronizationContext context = SynchronizationContext.Current;


        public SpecDetailViewModel(ISpecDetailView view)
          : base(view)
        {
        }


        public override void CurrentChanged(Spezifikation oldSelected)
        {

            RaisePropertyChanged(nameof(Baugruppenname));
            RaisePropertyChanged(nameof(Bauteilname));
            RaisePropertyChanged(nameof(FürAssets));
            RaisePropertyChanged(nameof(FürArtikel));
            ResetTabsForAsset();
        }


        private void ResetTabsForAsset()
        {

            if (FürArtikel.GetValueOrDefault() && (Tab == Tabs.Artikel || Tab == Tabs.Massnahmen)) Tab = Tabs.Basis;
            RaisePropertyChanged(nameof(AssetTabsVisible));
        }


        public void Unload()
        {}


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public void ResetTabsIfPrint()
        {
            if (Tab == Tabs.Report) CurrentTab = 0;
        }


#region P R O P E R T I E S


        public String Baugruppenname
        {
            get => current?.Baugruppenname;
            set {

                if (current != null && Baugruppenname != value) {

                    current.Baugruppenname = value;
                    RaisePropertyChanged(nameof(Baugruppenname));
                }
            }
        }


        public String Bauteilname
        {
            get => current?.Bauteilname;
            set {

                if (current != null && Bauteilname != value) {

                    current.Bauteilname = value;
                    RaisePropertyChanged(nameof(Bauteilname));
                }
            }
        }


        public bool? FürAssets
        {
            get => current?.FürAssets;
            set {

                if (current != null && FürAssets != value && value.HasValue) {

                    current.FürAssets = value.Value;
                    RaisePropertyChanged(nameof(FürAssets));
                    ResetTabsForAsset();
                    if (FürAssets.GetValueOrDefault()) FürArtikel = false;
                }
            }
        }


        public bool? FürArtikel
        {
            get => current?.FürArtikel;
            set {

                if (current != null && FürArtikel != value && value.HasValue) {

                    current.FürArtikel = value.Value;
                    RaisePropertyChanged(nameof(FürArtikel));
                    ResetTabsForAsset();
                    if (FürArtikel.GetValueOrDefault()) FürAssets = false;
                }
            }
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public bool AssetTabsVisible => Current != null && Current.FürAssets;


        public Tabs Tab
        {
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; }
        }


        private object baseView;
        public object BaseView
        {
            get { return baseView; }
            set { SetProperty(ref baseView, value); }
        }


        private object reportView;
        public object ReportView
        {
            get { return reportView; }
            set { SetProperty(ref reportView, value); }
        }


        private object artikelView;
        public object ArtikelView
        {
            get { return artikelView; }
            set { SetProperty(ref artikelView, value); }
        }


        private object docView;
        public object DocView
        {
            get { return docView; }
            set { SetProperty(ref docView, value); }
        }


        private object mnView;
        public object MnView
        {
            get { return mnView; }
            set { SetProperty(ref mnView, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
