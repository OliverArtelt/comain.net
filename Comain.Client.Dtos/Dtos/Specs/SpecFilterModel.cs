﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos;

namespace Comain.Client.Dtos.Specs
{
    public class SpecFilterModel : EntityModelBase
    {

        public String   Baugruppenname          { get; set; }
        public String   Bauteilname             { get; set; }


        public String Name => String.IsNullOrEmpty(Bauteilname)? Baugruppenname: $"{Baugruppenname} • {Bauteilname}";
    }
}
