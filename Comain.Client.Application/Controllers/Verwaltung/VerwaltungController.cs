﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Controllers.Verwaltung.Types;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.Controllers.Verwaltung
{

    public interface IVerwaltungController : IController
    {}


    public class VerwaltungController : ControllerBase, IVerwaltungController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly IVwTypFactory factory;
        private readonly ILoginService login;
        private readonly MainViewModel mainView;
        private readonly VerwaltungViewModel view;
        private IVwTyp current;
        private IController currentController;
        private ControllerMessage caller;


        public VerwaltungController(IBreadcrumbService crumbs, MainViewModel mainView, VerwaltungViewModel view,
                                    IMessageService messages, IVwTypFactory factory, ILoginService login, IControllerBus bus)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.mainView = mainView;
            this.view = view;
            this.factory = factory;
            this.login = login;
        }


        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Verwaltung");
            view.DetailView = null;
            current = null;
            view.Current = null;
            mainView.PageView = view.View;

            view.Mandant = login.AktuelleVerbindung;
            view.Einheiten = factory.VerfügbareEinheiten();

            caller = parameter as ControllerMessage;
            if (caller?.Parameter is Models.Users.Personal || caller?.Parameter is PersonalFilterModel) {

                var ctrl = bus.GetController<IPersonalController>();
                if (ctrl != null) await ctrl.RunAsync(caller);
            }
        }


        public void Shutdown()
            => PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");


        private async Task LoadEinheitAsync()
        {

            if (!await UnloadImplAsync()) return;

            view.DetailView = null;
            current = null;
            currentController = null;
            if (view.Current == null) return;
            current = view.Current;
            currentController = bus.GetController(current.Controller);
            await currentController.RunAsync(null);
        }


        protected override async Task<bool> UnloadImplAsync()
        {

            if (current == null || current.Controller == null) return true;
            return await currentController.UnloadAsync();
        }


        private Task BackAsync()
        {
            return bus.BackAsync(caller);
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current") await LoadEinheitAsync();
        }
    }
}
