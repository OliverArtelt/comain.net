﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Dringlichkeiten : IVwTyp
    {

        public string       Title           => "Dringlichkeiten";
        public int          Sortierung      => 2800;
        public Type         Controller      => typeof(IDringlichkeitController);


        public bool HatZugriff(Nutzer nutzer)
        {
        
            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;    
        }
    }
}
