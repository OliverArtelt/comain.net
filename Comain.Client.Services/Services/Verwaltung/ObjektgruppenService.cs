﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Services.Verwaltung
{

    public class ObjektgruppenService : IObjektgruppenService
    {
        
        private const String url = @"/VW/OGr";
        private readonly IRepository store;
        private readonly IMapper mapper;
        private TrackableCollection<Objektgruppe> currentList;
        private TrackableCollection<Objektart> currentSubList;


        public ObjektgruppenService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<Tuple<TrackableCollection<Objektgruppe>, TrackableCollection<Objektart>>> GetAsync(CancellationToken ct = default)
        {

            currentList = null;
            currentSubList = null;
            
            var result = await store.GetAsync<OGrListDto>(url, ct).ConfigureAwait(false);
            var mainlist = mapper.Map<List<Objektgruppe>>(result.Mainlist);
            mainlist.Sort();
            currentList = new TrackableCollection<Objektgruppe>(mainlist);
            
            var sublist = mapper.Map<List<Objektart>>(result.Sublist);
            currentSubList = new TrackableCollection<Objektart>(sublist);
            
            return new Tuple<TrackableCollection<Objektgruppe>, TrackableCollection<Objektart>>(currentList, currentSubList);  
        }


        public bool HasChanges
        {
            get { return currentList != null && currentSubList != null && (currentList.IsChanged || currentSubList.IsChanged); }
        }


        public async Task SaveAsync(CancellationToken ct = default)
        {
            
            var mainlist = mapper.AsDtoList<Objektgruppe, ObjektgruppeDto>(currentList);
            var sublist = mapper.AsDtoList<Objektart, ObjektartDto>(currentSubList);
            if (mainlist == null && sublist == null) return;
           
            var result = new OGrListDto { Mainlist = mainlist, Sublist = sublist }; 

            await store.PostAsync(url, result, ct).ConfigureAwait(false);
            currentList.AcceptChanges();
            currentSubList.AcceptChanges();
        }


        public void Reset()
        {
           
            currentList = null;
            currentSubList = null;
        }
    }
}
