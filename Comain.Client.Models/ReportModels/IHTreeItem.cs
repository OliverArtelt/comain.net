﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ReportModels
{

    public class IHTreeItem
    {

        public String   Nummer          { get; set; }
        public String   Inventarnummer  { get; set; }
        public String   Name            { get; set; }
        public int      Level           { get; set; }
        public String   Hauptnummer     { get; set; }


        public IHTreeItem()
        {}


        public IHTreeItem(IHObjektFilterModel dto)
        {

            Name = dto.Name;
            Nummer = dto.Nummer;
            Inventarnummer = dto.Inventarnummer;
            Hauptnummer = IHObjekt.GebeMaschinennummer(Nummer);
            Level = IHObjekt.Baugruppenlevel(Nummer);
        }
    }
}
