﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.Aufträge
{

    public enum Auftragstatus
    {

        Offen = 0,
        Überfällig = 7,
        Abgeschlossen = 9,
        Storno = 88, 
        Archiv = 99
    }
}
