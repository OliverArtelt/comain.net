﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Comain.Client.Views.Internal
{

    public class DateTextPicker : DatePicker
    {

        protected override void OnDateValidationError(DatePickerDateValidationErrorEventArgs e)
        {

            if (String.IsNullOrWhiteSpace(e.Text)) {

                SelectedDate = null;
                return;
            }

            DateTime? secondTry = ParseDate(e.Text);
            if (secondTry.HasValue) {

                SelectedDate = secondTry;
                return;
            }

            base.OnDateValidationError(e);
        }


        protected override void OnCalendarOpened(RoutedEventArgs e)
        {

            base.OnCalendarOpened(e);
            if (SelectedDate == null) DisplayDate = DateTime.Today;
        }


        private DateTime? ParseDate(String time)
        {

            if (String.IsNullOrEmpty(time)) return null;
            DateTime zeit;
            if (DateTime.TryParse(time, out zeit)) return zeit;
            if (DateTime.TryParseExact(time, @"ddMMyy", null, System.Globalization.DateTimeStyles.None, out zeit)) return zeit;
            if (DateTime.TryParseExact(time, @"ddMMyyyy", null, System.Globalization.DateTimeStyles.None, out zeit)) return zeit;
            if (DateTime.TryParseExact(time, @"ddMM", null, System.Globalization.DateTimeStyles.None, out zeit)) return zeit;

            return null;
        }
    }
}
