﻿using System;


namespace Comain.Client.Dtos.Stock
{

    public class InventurDto
    {

        public int      ArtikelId           { get; set; }
        public int      LagerortId          { get; set; }
        public String   Lagerort            { get; set; }
        public String   Artikelnummer       { get; set; }
        public String   Artikelname         { get; set; }
        public String   Herstellernummer    { get; set; }
        public decimal  Lagerbestand        { get; set; }
        public decimal? Inventurbestand     { get; set; }
        public String   Bemerkungen         { get; set; }
        public String   Einheit             { get; set; }
    }
}
