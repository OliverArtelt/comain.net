﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Dokumente;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.Models.Users
{

    [DebuggerDisplay("{NummerUndName}")]
    public class Personal : DocEntity, IDataErrorInfo, IComparable<Personal>
    {

        private String      nummer;
        private String      klammer;
        private String      nachname;
        private String      vorname;
        private String      strasse;
        private String      plz;
        private String      ort;
        private String      telefon;
        private String      qualifikation;
        private String      eMail;
        private DateTime?   eintritt;
        private DateTime?   austritt;
        private int?        gewerk_Id;
        private int?        monatssoll;
        private decimal?    stundensatz;


        public String NummerUndName
        {
            get {

                if (String.IsNullOrWhiteSpace(Vorname)) return String.Format("[{0}] {1}", Nummer, Nachname);
                return String.Format("[{0}] {1}, {2}", Nummer, Nachname, Vorname);
            }
        }


        public PersonalFilterModel AsListDto()
            => new PersonalFilterModel { Id         = Id,
                                         Gewerk_Id  = Gewerk_Id,
                                         Nummer     = Nummer,
                                         Nachname   = Nachname,
                                         Vorname    = Vorname };


        public override String EntityType => "PS";


        public override bool IsChanged => base.IsChanged || Assets != null && Assets.IsChanged;


        public override void AcceptChanges()
        {

            base.AcceptChanges();
            Assets?.AcceptChanges();
        }


        private TrackableCollection<IHObjektPersonal> assets;
        public TrackableCollection<IHObjektPersonal> Assets
        {
            get => assets ?? (assets = new TrackableCollection<IHObjektPersonal>());
            set => SetProperty(ref assets, value);
        }


#region P R O P E R T I E S


        public String Klammer
        {
            get { return klammer; }
            set { SetProperty(ref klammer, value); }
        }

        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }

        public String Nachname
        {
            get { return nachname; }
            set { SetProperty(ref nachname, value); }
        }

        public String Vorname
        {
            get { return vorname; }
            set { SetProperty(ref vorname, value); }
        }

        public String Strasse
        {
            get { return strasse; }
            set { SetProperty(ref strasse, value); }
        }

        public String Plz
        {
            get { return plz; }
            set { SetProperty(ref plz, value); }
        }

        public String Ort
        {
            get { return ort; }
            set { SetProperty(ref ort, value); }
        }

        public String Telefon
        {
            get { return telefon; }
            set { SetProperty(ref telefon, value); }
        }

        public String Qualifikation
        {
            get { return qualifikation; }
            set { SetProperty(ref qualifikation, value); }
        }

        public String EMail
        {
            get { return eMail; }
            set { SetProperty(ref eMail, value); }
        }

        public DateTime? Eintritt
        {
            get { return eintritt; }
            set { SetProperty(ref eintritt, value); }
        }

        public DateTime? Austritt
        {
            get { return austritt; }
            set { SetProperty(ref austritt, value); }
        }

        public int? Gewerk_Id
        {
            get { return gewerk_Id; }
            set { SetProperty(ref gewerk_Id, value); }
        }

        public int? Monatssoll
        {
            get { return monatssoll; }
            set { SetProperty(ref monatssoll, value); }
        }

        public decimal? Stundensatz
        {
            get { return stundensatz; }
            set { SetProperty(ref stundensatz, value); }
        }


#endregion


        public bool Matches(String searchText)
        {

            if (Nummer != null && Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Nachname != null && Nachname.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Vorname != null && Vorname.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Klammer != null && Klammer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            return false;
        }


        public int CompareTo(Personal other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Nachname"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Vorname"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Klammer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Strasse"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Plz"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Ort"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Telefon"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["EMail"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Qualifikation"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Stundensatz"];
                return test;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 15) return "Die Nummer darf aus maximal 15 Zeichen bestehen.";
                    break;

                case "Nachname":

                    if (String.IsNullOrWhiteSpace(Nachname)) return "Geben Sie den Nachnamen an.";
                    if (Nachname.Length > 100) return "Der Nachname darf aus maximal 100 Zeichen bestehen.";
                    break;

                case "Vorname":

                    if (String.IsNullOrWhiteSpace(Vorname)) return "Geben Sie den Vornamen an.";
                    if (Vorname.Length > 50) return "Der Vorname darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Klammer":

                    if (String.IsNullOrWhiteSpace(Klammer)) return null;
                    if (Klammer.Length > 15) return "Die Personalklammer darf aus maximal 15 Zeichen bestehen.";
                    break;

                case "Strasse":

                    if (String.IsNullOrWhiteSpace(Strasse)) return null;
                    if (Strasse.Length > 150) return "Die Strasse darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Plz":

                    if (String.IsNullOrWhiteSpace(Plz)) return null;
                    if (Plz.Length > 5) return "Die Postleitzahl darf aus maximal 5 Zeichen bestehen.";
                    break;

                case "Ort":

                    if (String.IsNullOrWhiteSpace(Ort)) return null;
                    if (Ort.Length > 150) return "Der Ort darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Telefon":

                    if (String.IsNullOrWhiteSpace(Telefon)) return null;
                    if (Telefon.Length > 50) return "Die Telefonnummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "EMail":

                    if (String.IsNullOrWhiteSpace(EMail)) return null;
                    if (EMail.Length > 50) return "Die E-Mailadresse darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Qualifikation":

                    if (String.IsNullOrWhiteSpace(Qualifikation)) return null;
                    if (Qualifikation.Length > 254) return "Die Qualifikation darf aus maximal 254 Zeichen bestehen.";
                    break;
                }

                return null;
            }
        }

#endregion

    }
}
