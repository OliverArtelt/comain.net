﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class MassnahmeDto
    {

        public int                      Id                      { get; set; }
        public String                   Name                    { get; set; }
        public String                   Nummer                  { get; set; }
        public String                   Kurzbeschreibung        { get; set; }
        public String                   Durchführung            { get; set; }
        public int                      Zyklus                  { get; set; }
        public int                      Termintyp               { get; set; }
        public int?                     Normzeit                { get; set; }
        public int?                     Menge                   { get; set; }
        public decimal?                 Wert                    { get; set; }
        public int                      Tagesmuster             { get; set; }
        public decimal?                 Budget48                { get; set; }
        public decimal?                 Budget52                { get; set; }
        public String                   Kostenstelle            { get; set; }
        public String                   Leistungsart            { get; set; }
        public String                   Gewerk                  { get; set; }
        public String                   ExterneAuftragsnummer   { get; set; }
        public DateTime?                Start                   { get; set; }
        public bool                     IstInaktiv              { get; set; }

        public List<PositionDto>        Positionen              { get; set; }
        public List<MaterialInfoDto>    MaterialInfo            { get; set; }
    }
}
