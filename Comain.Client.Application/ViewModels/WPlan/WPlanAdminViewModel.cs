﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.ViewModels.Verwaltung;
using Comain.Client.Views.WPlan;


namespace Comain.Client.ViewModels.WPlan
{

    public class WPlanAdminViewModel : ValidatableViewModel<IWPlanAdminView, Konfiguration>, IConfigViewModel
    {

        private readonly SynchronizationContext context;
        private Dictionary<int, PersonalFilterModel> psdic;


        public WPlanAdminViewModel(IWPlanAdminView view, IPersonalSearchProvider searcher) : base(view)
        {

            context = SynchronizationContext.Current;
            PsSearchProvider = searcher;
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(Leistungsart));
            RaisePropertyChanged(nameof(Gewerk));
            RaisePropertyChanged(nameof(Dringlichkeit));
            RaisePropertyChanged(nameof(NokLeistungsart));
            RaisePropertyChanged(nameof(NokGewerk));
            RaisePropertyChanged(nameof(NokDringlichkeit));
            RaisePropertyChanged(nameof(WPlanNormzeit));
            RaisePropertyChanged(nameof(WPlanStundensatz));
            RaisePropertyChanged(nameof(WPlanWochenRückmeldung));
            RaisePropertyChanged(nameof(WPlanProdDarfPlantafelSehen));
            RaisePropertyChanged(nameof(WPlanReloadAfterSave));
            RaisePropertyChanged(nameof(WPlanMaterialNegativ));
            RaisePropertyChanged(nameof(WPlanMaterialNachfragen));
            RaisePropertyChanged(nameof(WPlanEinlagerungAutomatisch));
            RaisePropertyChanged(nameof(WPlanEinlagerungNachfragen));
            RaisePropertyChanged(nameof(WPlanAnzahlWochenBestellen));

            Abnahmeberechtigter = psdic != null && Current != null && Current.WPlanAbnahmeberechtigter.HasValue? psdic.GetValueOrDefault(Current.WPlanAbnahmeberechtigter.Value): null;
            Auftragsleiter      = psdic != null && Current != null && Current.WPlanAuftragsleiter.HasValue?      psdic.GetValueOrDefault(Current.WPlanAuftragsleiter.Value): null;
            Auftraggeber        = psdic != null && Current != null && Current.WPlanAuftraggeber.HasValue?        psdic.GetValueOrDefault(Current.WPlanAuftraggeber.Value): null;
            Auftragnehmer       = psdic != null && Current != null && Current.WPlanAuftragnehmer.HasValue?       psdic.GetValueOrDefault(Current.WPlanAuftragnehmer.Value): null;
        }


        public void Unload()
        {

            Leistungsarten = null;
            psdic = null;
            PsSearchProvider.ClearItems();
        }


        public void SetzeListen(IEnumerable<LeistungsartFilterModel> lalist, IEnumerable<PersonalFilterModel> pslist, IEnumerable<DringlichkeitDto> drlist,
                                IEnumerable<NummerFilterModel> gwlist)
        {

            psdic = pslist.ToDictionary(p => p.Id);
            context.Send(new SendOrPostCallback((o) => {

                Leistungsarten = lalist.OrderBy(p => p.Nummer).ToList();
                PsSearchProvider.SetItems(pslist?.OrderBy(p => p.Name));
                Dringlichkeiten = drlist;
                Gewerke = gwlist.OrderBy(p => p.Nummer).ToList();

            }), null);
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private IEnumerable<LeistungsartFilterModel> leistungsarten;
        public IEnumerable<LeistungsartFilterModel> Leistungsarten
        {
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }


        private IEnumerable<DringlichkeitDto> dringlichkeiten;
        public IEnumerable<DringlichkeitDto> Dringlichkeiten
        {
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        private IEnumerable<NummerFilterModel> gewerke;
        public IEnumerable<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


        public LeistungsartFilterModel Leistungsart
        {
            get {

                if (Current == null) return null;
                if (Leistungsarten.IsNullOrEmpty()) return null;
                if (!Current.WPlanLeistungsart.HasValue) return null;
                return Leistungsarten.FirstOrDefault(p => p.Id == Current.WPlanLeistungsart);
            }
            set {

                if (Current != null) {

                    Current.WPlanLeistungsart = value?.Id;
                    RaisePropertyChanged(nameof(Leistungsart));
                }
            }
        }


        public NummerFilterModel Gewerk
        {
            get {

                if (Current == null) return null;
                if (Gewerke.IsNullOrEmpty()) return null;
                if (!Current.WPlanGewerk.HasValue) return null;
                return Gewerke.FirstOrDefault(p => p.Id == Current.WPlanGewerk);
            }
            set {

                if (Current != null) {

                    Current.WPlanGewerk = value?.Id;
                    RaisePropertyChanged(nameof(Gewerk));
                }
            }
        }


        public DringlichkeitDto Dringlichkeit
        {
            get {

                if (Current == null) return null;
                if (Dringlichkeiten.IsNullOrEmpty()) return null;
                if (!Current.WPlanDringlichkeit.HasValue) return null;
                return Dringlichkeiten.FirstOrDefault(p => p.Id == Current.WPlanDringlichkeit);
            }
            set {

                if (Current != null) {

                    Current.WPlanDringlichkeit = value?.Id;
                    RaisePropertyChanged(nameof(Dringlichkeit));
                }
            }
        }


        public LeistungsartFilterModel NokLeistungsart
        {
            get {

                if (Current == null) return null;
                if (Leistungsarten.IsNullOrEmpty()) return null;
                if (!Current.WPlanNokLeistungsart.HasValue) return null;
                return Leistungsarten.FirstOrDefault(p => p.Id == Current.WPlanNokLeistungsart);
            }
            set {

                if (Current != null) {

                    Current.WPlanNokLeistungsart = value?.Id;
                    RaisePropertyChanged(nameof(NokLeistungsart));
                }
            }
        }


        public NummerFilterModel NokGewerk
        {
            get {

                if (Current == null) return null;
                if (Gewerke.IsNullOrEmpty()) return null;
                if (!Current.WPlanNokGewerk.HasValue) return null;
                return Gewerke.FirstOrDefault(p => p.Id == Current.WPlanNokGewerk);
            }
            set {

                if (Current != null) {

                    Current.WPlanNokGewerk = value?.Id;
                    RaisePropertyChanged(nameof(NokGewerk));
                }
            }
        }


        public DringlichkeitDto NokDringlichkeit
        {
            get {

                if (Current == null) return null;
                if (Dringlichkeiten.IsNullOrEmpty()) return null;
                if (!Current.WPlanNokDringlichkeit.HasValue) return null;
                return Dringlichkeiten.FirstOrDefault(p => p.Id == Current.WPlanNokDringlichkeit);
            }
            set {

                if (Current != null) {

                    Current.WPlanNokDringlichkeit = value?.Id;
                    RaisePropertyChanged(nameof(NokDringlichkeit));
                }
            }
        }


        public int? WPlanNormzeit
        {
            get => Current?.WPlanNormzeit;
            set {

                if (Current != null) {

                    Current.WPlanNormzeit = value;
                    RaisePropertyChanged(nameof(WPlanNormzeit));
                }
            }
        }


        public int? WPlanWochenRückmeldung
        {
            get => Current?.WPlanWochenRückmeldung;
            set {

                if (Current != null) {

                    Current.WPlanWochenRückmeldung = value;
                    RaisePropertyChanged(nameof(WPlanWochenRückmeldung));
                }
            }
        }


        public decimal? WPlanStundensatz
        {
            get => Current?.WPlanStundensatz;
            set {

                if (Current != null) {

                    Current.WPlanStundensatz = value;
                    RaisePropertyChanged(nameof(WPlanStundensatz));
                }
            }
        }


        private PersonalFilterModel auftraggeber;
        public PersonalFilterModel Auftraggeber
        {
            get { return auftraggeber; }
            set {

                auftraggeber = value;
                Current.WPlanAuftraggeber = value?.Id;
                RaisePropertyChanged(nameof(Auftraggeber));
            }
        }


        private PersonalFilterModel auftragsleiter;
        public PersonalFilterModel Auftragsleiter
        {
            get { return auftragsleiter; }
            set {

                auftragsleiter = value;
                Current.WPlanAuftragsleiter = value?.Id;
                RaisePropertyChanged(nameof(Auftragsleiter));
            }
        }


        private PersonalFilterModel auftragnehmer;
        public PersonalFilterModel Auftragnehmer
        {
            get { return auftragnehmer; }
            set {

                auftragnehmer = value;
                Current.WPlanAuftragnehmer = value?.Id;
                RaisePropertyChanged(nameof(Auftragnehmer));
            }
        }


        private PersonalFilterModel abnahmeberechtigter;
        public PersonalFilterModel Abnahmeberechtigter
        {
            get { return abnahmeberechtigter; }
            set {

                abnahmeberechtigter = value;
                Current.WPlanAbnahmeberechtigter = value?.Id;
                RaisePropertyChanged(nameof(Abnahmeberechtigter));
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        public bool WPlanProdDarfPlantafelSehen
        {
            get => Current == null? false: Current.WPlanProdDarfPlantafelSehen;
            set {

                if (Current != null) {

                    Current.WPlanProdDarfPlantafelSehen = value;
                    RaisePropertyChanged(nameof(WPlanProdDarfPlantafelSehen));
                }
            }
        }


        public bool WPlanReloadAfterSave
        {
            get => Current == null? false: Current.WPlanReloadAfterSave;
            set {

                if (Current != null) {

                    Current.WPlanReloadAfterSave = value;
                    RaisePropertyChanged(nameof(WPlanReloadAfterSave));
                }
            }
        }


        public bool WPlanMaterialNegativ
        {
            get => Current == null? false: !Current.WPlanMaterialPrüfung;
            set {

                if (Current != null && value) {

                    Current.WPlanMaterialPrüfung = false;
                    RaisePropertyChanged(nameof(WPlanMaterialNegativ));
                    RaisePropertyChanged(nameof(WPlanMaterialNachfragen));
                }
            }
        }


        public bool WPlanMaterialNachfragen
        {
            get => Current == null? false: Current.WPlanMaterialPrüfung;
            set {

                if (Current != null && value) {

                    Current.WPlanMaterialPrüfung = true;
                    RaisePropertyChanged(nameof(WPlanMaterialNegativ));
                    RaisePropertyChanged(nameof(WPlanMaterialNachfragen));
                }
            }
        }


        public bool WPlanEinlagerungAutomatisch
        {
            get => Current == null? false: !Current.WPlanEinlagerungNachfragen;
            set {

                if (Current != null && value) {

                    Current.WPlanEinlagerungNachfragen = false;
                    RaisePropertyChanged(nameof(WPlanEinlagerungAutomatisch));
                    RaisePropertyChanged(nameof(WPlanEinlagerungNachfragen));
                }
            }
        }


        public bool WPlanEinlagerungNachfragen
        {
            get => Current == null? false: Current.WPlanEinlagerungNachfragen;
            set {

                if (Current != null && value) {

                    Current.WPlanEinlagerungNachfragen = true;
                    RaisePropertyChanged(nameof(WPlanEinlagerungAutomatisch));
                    RaisePropertyChanged(nameof(WPlanEinlagerungNachfragen));
                }
            }
        }


        public int? WPlanAnzahlWochenBestellen
        {
            get => Current?.WPlanAnzahlWochenBestellen;
            set {

                if (Current != null) {

                    Current.WPlanAnzahlWochenBestellen = value;
                    RaisePropertyChanged(nameof(WPlanAnzahlWochenBestellen));
                }
            }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
