﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.IHObjekte
{

    public class IHObjektBelegung : Entity, IDataErrorInfo
    {

        private int     iHObjekt_Id;
        private int     jahr;
        private short   monat;
        private int?    belegungszeit;
        private int?    ausfallzeit;


#region P R O P E R T I E S

        
        public int IHObjekt_Id                  
        {
            get { return iHObjekt_Id; }
            set { SetProperty(ref iHObjekt_Id, value); }
        }

        
        public int Jahr                  
        {
            get { return jahr; }
            set { SetProperty(ref jahr, value); }
        }

        
        public short Monat                  
        {
            get { return monat; }
            set { SetProperty(ref monat, value); }
        }

        
        public int? Belegungszeit                  
        {
            get { return belegungszeit; }
            set { SetProperty(ref belegungszeit, value); }
        }

        
        public int? Ausfallzeit                  
        {
            get { return ausfallzeit; }
            set { SetProperty(ref ausfallzeit, value); }
        }
       

#endregion


#region V A L I D A T I O N 
        

        public string Error
        {
            get { return null; }
        }

        
        public string this[string columnName]
        {
            get { return null; }
        }

#endregion

    }
}
