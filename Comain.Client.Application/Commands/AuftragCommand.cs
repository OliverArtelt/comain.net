﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public class AuftragCommand : IControllerCommand
    {

        public Type         Controller  => typeof(IAuftragController);
        public String       Name        => "Aufträge";
        public String       Parameter   => "AU List";
        public String       Gruppe      => "Aufträge";
        public int          Sortierung  => 1000;
        public String       Color       => "RoyalBlue";
        public String       Image       => @"/Comain.Client.Views;component/Resources/appbar.column.one.png";
        public String       LiveValue   => null;
        public String       LiveText    => null;


        public bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg)
        {

            if (nutzer == null) return false;
            if (!module.VerwendetModulAuftrag) return false;
            return nutzer.IstAdministrator || nutzer.IstProduktionLeitung || nutzer.IstProduktionSchichtmeister || nutzer.IstInstandhaltungInstandhalter ||
                   nutzer.IstInstandhaltungLeitung;
        }


        public Task UpdateAsync(CancellationToken ct = default)
        {
            return Task.FromResult(true);
        }
    }
}
