﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Dtos.Auth;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Verwaltung;


namespace Comain.Client.Controllers.Verwaltung
{

    public interface IProfilController : IController 
    {}


    /// <summary>
    /// Kennwortänderung durch den Benutzer
    /// </summary>
    public class ProfilController : ControllerBase, IProfilController
    {
       
        private readonly IBreadcrumbService crumbs;
        private readonly ProfilViewModel view;
        private readonly MainViewModel mainView;
        private readonly ILoginService login;
        private readonly ISettings settings;


        public ProfilController(IBreadcrumbService crumbs, ILoginService login, ISettings settings, IControllerBus bus,
                                MainViewModel mainView, ProfilViewModel view, IMessageService messages)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.view = view;
            this.login = login;
            this.mainView = mainView;
            this.settings = settings;
        }
       

        public void Initialize()
        {

            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            view.SaveCommand = CreateCommand(_ => SaveAsync());
            view.PasswordCommand = CreateCommand(_ => SetPasswordAsync());
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Profil");
            view.Name = login.AktuellerNutzer.Name;
            mainView.PageView = view.View;
            view.Mandant = login.AktuelleVerbindung;
            view.ConfirmLogout = settings.ConfirmLogout;
        }


        public Task<bool> UnloadAsync()
        {

            view.PasswordOld  = null;
            view.PasswordNew1 = null;
            view.PasswordNew2 = null;
            return Task.FromResult(true);
        }


        private async Task SetPasswordAsync()
        {

            var test = Nutzer.ValidatePassword(view.PasswordOld, view.PasswordNew1, view.PasswordNew2);
            if (!String.IsNullOrEmpty(test)) {

                await messages.ShowMessageAsync(new MessageToken { Header = "Das Kennwort kann nicht gesetzt werden", 
                                                                   Reasons = new List<String> { test }});
                return;                                                     
            }

            var dto = new PasswordDto { UserId = login.AktuellerNutzer.Id, OldPassword = view.PasswordOld, NewPassword = view.PasswordNew1 };
              
            using (new VisualDelay(view)) {

                await login.SetPasswordAsync(dto, bus.Token);
                view.PasswordOld  = null;
                view.PasswordNew1 = null;
                view.PasswordNew2 = null;
            }

            await messages.ShowMessageAsync(new MessageToken { Header = "Das Kennwort wurde erfolgreich geändert.", 
                                                               Reasons = new List<String> { "Benutzen Sie Ihr neues Kennwort bei der nächsten Anmeldung." }});
        }


        private Task SaveAsync()
        {

            settings.ConfirmLogout = view.ConfirmLogout;
            settings.Save();
            return Task.FromResult(true);
        }


        public void Shutdown()
        {
        }


        private Task BackAsync()
        {
            return bus.BackAsync();
        }
    }
}
