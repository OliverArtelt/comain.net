﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.WPlan;

namespace Comain.Client.Models.WPlan.Planen
{

    public class MaterialInfo
    {

        public int          Id              { get; set; }
        public int?         MaterialId      { get; set; }
        public String       Name            { get; set; }
        public String       Nummer          { get; set; }
        public String       Einheit         { get; set; }
        public decimal?     Menge           { get; set; }
        public decimal?     EPreis          { get; set; }
        public bool         Erforderlich    { get; set; }


        public decimal MengeNormalisiert => Menge.GetValueOrDefault() == 0? 1m: Menge.Value;

        public String MengeEinheitText => String.IsNullOrEmpty(Einheit)? MengeNormalisiert.ToString(): $"{MengeNormalisiert} {Einheit}";

        public String ErforderlichText => Erforderlich? "Einbau erforderlich": "Einbau frei";

        public String PreisText => EPreis.Value.ToString("C2");

        public String MengePreisText
        {
            get {

                if (!EPreis.HasValue) return MengeEinheitText;
                return $"{MengeEinheitText} x {PreisText}";
            }
        }


        public MaterialInfo()
        {}


        public MaterialInfo(MaterialInfoDto dto)
        {

            Id              = dto.Id;
            MaterialId      = dto.MaterialId;
            Name            = dto.Name;
            Nummer          = dto.Nummer;
            Einheit         = dto.Einheit;
            Menge           = dto.Menge;
            EPreis          = dto.EPreis;
            Erforderlich    = dto.Erforderlich;
        }
    }
}
