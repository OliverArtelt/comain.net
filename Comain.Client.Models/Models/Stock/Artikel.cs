﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Comain.Client.Models;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Dokumente;
using Comain.Client.Dtos.Specs;
using Comain.Client.Models.Specs;

namespace Comain.Client.Models.Stock
{

    public class Artikel : DocEntity, IDataErrorInfo, IComparable<Artikel>
    {

        private String              name;
        private String              nummer;
        private String              beschreibung;
        private String              herstellernummer;
        private decimal?            mischpreis;
        private Materialklasse?     klasse;

        private decimal?            mindestbestand;
        private decimal?            mindestbestellmenge;
        private decimal?            bestellmenge;
        private decimal?            reservierteMenge;
        private decimal?            bestandReal;
        private decimal?            bestandVirtuell;

        private NummerFilterModel   einheit;
        private NummerFilterModel   warengruppe;
        private SpecFilterModel     spezifikation;

        private TrackableCollection<ArtikelLieferant> materialLieferantMap;
        private TrackableCollection<ArtikelIHObjekt> materialIHObjektMap;


        public override String EntityType => "MT";


        public TrackableCollection<ArtikelLieferant> MaterialLieferantMap
        {

            get {

                if (materialLieferantMap == null) materialLieferantMap = new TrackableCollection<ArtikelLieferant>();
                return materialLieferantMap;
            }
            set { SetProperty(ref materialLieferantMap, value); }
        }


        public TrackableCollection<ArtikelIHObjekt> MaterialIHObjektMap
        {

            get {

                if (materialIHObjektMap == null) materialIHObjektMap = new TrackableCollection<ArtikelIHObjekt>();
                return materialIHObjektMap;
            }
            set { SetProperty(ref materialIHObjektMap, value); }
        }


        private TrackableCollection<TechnischerWert> technischeWerte;
        public TrackableCollection<TechnischerWert> TechnischeWerte
        {
            get => technischeWerte ?? (technischeWerte = new TrackableCollection<TechnischerWert>());
            set => SetProperty(ref technischeWerte, value);
        }


        public ICollection<Wareneinlagerung> Einlagerungen    { get; set; }


        public Lagerort StandardLagerort
        {
            get {

                if (Einlagerungen.IsNullOrEmpty()) return null;
                return Einlagerungen.OrderByDescending(p => p.Datum).Select(p => p.Lagerort).FirstOrDefault();
            }
        }


        public virtual Artikel Clone()
        {

            var clone = new Artikel();
            clone.Name                  = this.name;
            clone.Nummer                = this.nummer + " Kopie";
            clone.Beschreibung          = this.beschreibung;
            clone.Herstellernummer      = this.herstellernummer;
            clone.Mischpreis            = this.mischpreis;
            clone.Klasse                = this.klasse;
            clone.Mindestbestand        = this.mindestbestand;
            clone.Mindestbestellmenge   = this.mindestbestellmenge;
            clone.Einheit               = this.einheit;
            clone.Warengruppe           = this.warengruppe;
            clone.Spezifikation         = this.spezifikation;

            clone.MaterialLieferantMap = new TrackableCollection<ArtikelLieferant>();
            clone.MaterialLieferantMap.AddRange(this.MaterialLieferantMap.Select(p => new ArtikelLieferant { Lieferant = p.Lieferant, Name = p.Name, Nummer = p.Nummer, Preis = p.Preis, Rank = p.Rank }));

            clone.MaterialIHObjektMap = new TrackableCollection<ArtikelIHObjekt>();
            clone.MaterialIHObjektMap.AddRange(this.MaterialIHObjektMap.Select(p => new ArtikelIHObjekt { IHObjekt = p.IHObjekt, Position = p.Position }));

            return clone;
        }


        /// <summary>
        /// Menge, die (nach)bestellt werden müsste
        /// </summary>
        public virtual decimal OffeneMenge
        {

            get {

                if (!mindestbestand.HasValue || !bestandVirtuell.HasValue) return 0m;
                return Math.Max(0, mindestbestand.Value - bestandVirtuell.Value);
            }
        }


        public ArtikelStatus Status
        {
            get {

                if (Mindestbestand.GetValueOrDefault() == 0) return ArtikelStatus.Frei;
                if (BestandReal.GetValueOrDefault() - ReservierteMenge.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return ArtikelStatus.Vorhanden;
                if (BestandVirtuell.GetValueOrDefault() >= Mindestbestand.GetValueOrDefault()) return ArtikelStatus.Bestellt;
                return ArtikelStatus.Fehlt;
            }
        }


        public String StatusAsColor
        {
            get {

                switch (Status) {

                    case ArtikelStatus.Vorhanden: return "LimeGreen";
                    case ArtikelStatus.Bestellt:  return "Gold";
                    case ArtikelStatus.Fehlt:     return "Coral";
                    default:                      return "LightSlateGray";
                }
            }
        }


        public bool Matches(IEnumerable<String> searchterms)
        {

            if (searchterms.IsNullOrEmpty()) return true;
            return searchterms.All(p => Matches(p));
        }


        public bool Matches(String searchText)
        {

            if (Name != null && Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Nummer != null && Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Beschreibung != null && Beschreibung.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;
            if (Herstellernummer != null && Herstellernummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)) return true;

            if (!MaterialLieferantMap.IsNullOrEmpty() &&
                MaterialLieferantMap.Any(p => p.Name != null && p.Name.Contains(searchText, StringComparison.CurrentCultureIgnoreCase) ||
                                              p.Nummer != null && p.Nummer.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))) return true;
            return false;
        }


#region P R O P E R T I E S


        public SpecFilterModel Spezifikation                   
        { 
            get { return spezifikation; } 
            set { SetProperty(ref spezifikation, value); } 
        }


        public DateTime? DeaktiviertAm  { get; set; }


        public String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public String Nummer
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }


        public String Beschreibung
        {
            get { return beschreibung; }
            set { SetProperty(ref beschreibung, value); }
        }


        public String Herstellernummer
        {
            get { return herstellernummer; }
            set { SetProperty(ref herstellernummer, value); }
        }


        public decimal? Mischpreis
        {
            get { return mischpreis; }
            set { SetProperty(ref mischpreis, value); }
        }

        /// <summary>
        /// Lagermindestbestand
        /// </summary>
        public decimal? Mindestbestand
        {
            get { return mindestbestand; }
            set { SetProperty(ref mindestbestand, value); }
        }


        public decimal? Mindestbestellmenge
        {
            get { return mindestbestellmenge; }
            set { SetProperty(ref mindestbestellmenge, value); }
        }

        /// <summary>
        /// Bestellte Menge, die noch nicht geliefert wurde
        /// </summary>
        public decimal? Bestellmenge
        {
            get { return bestellmenge; }
            set { SetProperty(ref bestellmenge, value); }
        }


        public decimal? ReservierteMenge
        {
            get { return reservierteMenge; }
            set { SetProperty(ref reservierteMenge, value); }
        }

        /// <summary>
        /// aktueller Lagerbestand
        /// </summary>
        public decimal? BestandReal
        {
            get { return bestandReal; }
            set { SetProperty(ref bestandReal, value); }
        }

        /// <summary>
        /// Lagerbestand - Reservierte Menge + Bestellte Menge
        /// </summary>
        public decimal? BestandVirtuell
        {
            get { return bestandVirtuell; }
            set { SetProperty(ref bestandVirtuell, value); }
        }


        public Materialklasse? Klasse
        {
            get { return klasse; }
            set { SetProperty(ref klasse, value); }
        }


        public NummerFilterModel Einheit
        {
            get { return einheit; }
            set { SetProperty(ref einheit, value); }
        }


        public NummerFilterModel Warengruppe
        {
            get { return warengruppe; }
            set { SetProperty(ref warengruppe, value); }
        }


#endregion


        public int CompareTo(Artikel other)
        {

            if (other == this) return 0;
            if (other == null) return 1;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Mindestbestellmenge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Bestellmenge"];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this["Herstellernummer"];
                return test;
            }
        }


        public List<String> ErrorList
        {

            get {

                var result = new List<String>();

                String test = this["Name"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Nummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Herstellernummer"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Mindestbestellmenge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);
                test = this["Bestellmenge"];
                if (!String.IsNullOrEmpty(test)) result.Add(test);

                return result;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Nummer":

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 50) return "Die Nummer darf aus maximal 50 Zeichen bestehen.";
                    break;

                case "Name":

                    if (String.IsNullOrWhiteSpace(Name)) return "Geben Sie den Namen an.";
                    if (Name.Length > 150) return "Der Nachname darf aus maximal 150 Zeichen bestehen.";
                    break;

                case "Herstellernummer":

                    if (String.IsNullOrWhiteSpace(Herstellernummer)) return null;
                    if (Herstellernummer.Length > 48) return "Die Herstellernummer darf aus maximal 48 Zeichen bestehen.";
                    break;

                case "Mindestbestellmenge":

                    if (Mindestbestellmenge.GetValueOrDefault() < 0m) return "Die Mindestbestellmenge muss grösser 0 sein.";
                    break;

                case "Bestellmenge":

                    if (Bestellmenge.GetValueOrDefault() < 0m) return "Die Bestellmenge muss grösser 0 sein.";
                    break;
                }

                return null;
            }
        }

#endregion


    }
}
