﻿using System;
using System.Windows.Data;


namespace Comain.Client.Views.Internal
{

    internal class StarImageConverter : IValueConverter
    {
        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null) return null;
            if (!(value is Int32)) return null;
            if (parameter == null) return null;
            var path = String.Format(@"/Comain.Views;component/Resources/Star{0}{1}.png", parameter, value.ToString());
            return path;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
