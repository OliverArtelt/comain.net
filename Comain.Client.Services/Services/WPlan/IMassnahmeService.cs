﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.ViewData.WPlan;


namespace Comain.Client.Services.WPlan
{

    public interface IMassnahmeService
    {

        Task<IList<MassnahmeViewItem>>       MassnahmenReadAsync(int ihid, CancellationToken ct = default);
        Task                                 MassnahmenSaveAsync(TrackableCollection<MassnahmeViewItem> mnlist, CancellationToken ct = default);
        Task<List<MassnahmeRow>>             MassnahmenReportAsync(WPlanCreateInfo filter, CancellationToken ct = default);

        Task<OrphanedCount>                  WartungOrphanedAsync(WPlanCreateInfo args, CancellationToken ct = default);
        Task<IList<AuftragViewItem>>         WartungReadAsync(WPlanCreateInfo args, CancellationToken ct = default);
        Task<String>                         WartungCreateAsync(WPlanCreateInfo args, CancellationToken ct = default);
        Task                                 WartungSaveAsync(IList<AuftragViewItem> dtos, CancellationToken ct = default);
        Task                                 WartungCloseAsync(IList<AuftragViewItem> models, CancellationToken ct = default);
        Task<List<AuftragReportItem>>        WPlanReportAsync(IEnumerable<int> auids, CancellationToken ct = default);

        Task<List<PZFilterItem>>             IHTreeAsync(CancellationToken ct = default);
        Task<IList<NummerFilterModel>>       FilterGewerkeAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>     FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<LeistungsartFilterModel>> FilterLeistungsartenAsync(CancellationToken ct = default);
    }
}