﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain
{
    public class RingCursor<T> 
    {

        private List<T> items;
        private int currentPosition;


        public RingCursor(IEnumerable<T> items)
        {
            
            if (items.IsNullOrEmpty()) this.items = new List<T>();
            else this.items = items.ToList();
        }


        public int Count => items.Count;


        public bool IsEmpty => items.Count == 0;


        public int Position => currentPosition;


        public T Current => IsEmpty? default: items[currentPosition];


        public T Next()
        {

            if (items.IsNullOrEmpty()) return default;

            if (Count > 1) currentPosition++;
            if (currentPosition == Count) currentPosition = 0;
            return Current;
        }


        public T Prev()
        {

            if (items.IsNullOrEmpty()) return default;

            if (Count > 1) currentPosition--;
            if (currentPosition == -1) currentPosition = Count - 1;
            return Current;
        }
    }
}
