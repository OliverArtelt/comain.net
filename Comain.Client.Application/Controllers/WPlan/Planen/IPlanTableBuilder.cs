﻿using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;

namespace Comain.Client.Controllers.WPlan.Planen
{

    public interface IPlanTableBuilder
    {
        Plan BuildPlan(WPlanCreateInfo pars, ZeitstrahlDto dto, Konfiguration cfg);
    }
}