﻿using System;


namespace Comain.Client.Dtos.Reports
{

    public class InaktiveIhoDto
    {

        public String       Nummer          { get; set; }
        public String       Inventarnummer  { get; set; }
        public String       Name            { get; set; }
        public DateTime?    DeaktiviertAm   { get; set; }
    }
}
