﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Comain.Client.Views.Services;


namespace Comain.Client.ViewModels.Services
{

    public class ImageEditViewModel : ViewModel<IImageEditView>
    {

        private readonly SynchronizationContext context;
        private readonly IImageEditView view;
        private System.Windows.Point center;


        public ImageEditViewModel(IImageEditView view)
          : base(view)
        {

            this.view = view;
            context = SynchronizationContext.Current;
            ResetCommand     = new DelegateCommand(ClearCanvas); 
            ColorCommand     = new DelegateCommand(p => SetCanvasColor(p.ToString()));
            ThicknessCommand = new DelegateCommand(p => SetCanvasThickness(p.ToString()));
            TurnCommand      = new DelegateCommand(p => RotateCanvas(p.ToString()));
        }


        public BitmapSource Image   { get; private set; }


        public int ImageWidth  => Image == null? 0: (int)Image.Width;
        public int ImageHeight => Image == null? 0: (int)Image.Height;


        public void Unload()
        {
            
            Image = null;
            RaisePropertyChanged(nameof(Image));
            RaisePropertyChanged(nameof(ImageWidth));
            RaisePropertyChanged(nameof(ImageHeight));
        }


        public async Task SetImageAsync(BitmapSource snapshot)
        {

            if (snapshot == null) {

                Unload();
                return;
            }


            await context.SendAsync(new SendOrPostCallback((o) => {

                ClearCanvas();
                Image = snapshot;
                center = new System.Windows.Point(0, 0);

                RaisePropertyChanged(nameof(Image));
                RaisePropertyChanged(nameof(ImageWidth));
                RaisePropertyChanged(nameof(ImageHeight));

            }), null);
        }


        private void ClearCanvas()
        {
            view.Canvas.Strokes.Clear();
        }


        private void SetCanvasColor(String colorname)
        {

            var color = System.Drawing.Color.FromName(colorname);
            view.Canvas.DefaultDrawingAttributes.Color = System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
        }


        private void SetCanvasThickness(String value)
        {

            if (Int32.TryParse(value, out int thickness)) {

                view.Canvas.DefaultDrawingAttributes.Width = thickness;
                view.Canvas.DefaultDrawingAttributes.Height = thickness;
            }
        }


        private void RotateCanvas(String value)
        {

            if (Int32.TryParse(value, out int angle)) {

                Image = new TransformedBitmap(Image, new RotateTransform(angle, center.X, center.Y));
                RaisePropertyChanged(nameof(Image));
                RaisePropertyChanged(nameof(ImageWidth));
                RaisePropertyChanged(nameof(ImageHeight));

                RotateStrokes(angle);
            }
        }


        private void RotateStrokes(int angle)
        {

            var strokes = view.Canvas.Strokes.Clone();
            var matrix = new Matrix();
            matrix.RotateAt(angle, center.X, center.Y);
            strokes.Transform(matrix, false);
            view.Canvas.Strokes = strokes;
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
 

        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        public ICommand ResetCommand        { get; }
        public ICommand ColorCommand        { get; }
        public ICommand ThicknessCommand    { get; }
        public ICommand TurnCommand         { get; }


        /// <remarks>
        /// https://blogs.msdn.microsoft.com/jaimer/2009/07/03/rendertargetbitmap-tips/
        /// </remarks>
        public Bitmap ExportImage()
        {

            double dpiX = 1.0, dpiY = 1.0;
            var source = PresentationSource.FromVisual(view.Canvas);

            if (source != null) {

                dpiX = source.CompositionTarget.TransformToDevice.M11;
                dpiY = source.CompositionTarget.TransformToDevice.M22;
            }

            var rtb = new RenderTargetBitmap((int)(view.Canvas.Width * dpiX), (int)(view.Canvas.Height * dpiY), 96.0 * dpiX, 96.0 * dpiY, PixelFormats.Pbgra32);
            rtb.Render(view.Canvas);


            using (var stream = new MemoryStream()) {

                var encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));
                encoder.Save(stream);              
                
                stream.Position = 0;
                var bitmap = new Bitmap(stream);
                using (var tmpImage = System.Drawing.Image.FromStream(stream)) { 
                 
                    var image = new Bitmap(tmpImage);
                    return image;
                }
            }
        }
    }
}
