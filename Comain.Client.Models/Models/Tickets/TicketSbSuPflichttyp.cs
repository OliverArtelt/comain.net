﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Tickets
{

    /// <summary>
    /// Wann ist SU / SU Pflichtangabe im Ticket?
    /// </summary>
    public enum TicketSbSuPflichttyp
    {
        
        Nie = 0,
        Abschluss = 1,
        Abnahmebereit = 2,
        Leistung = 3
    }
}
