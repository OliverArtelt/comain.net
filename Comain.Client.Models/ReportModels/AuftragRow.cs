﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ReportModels
{

    public class AuftragRow
    {

        public String   Auftragskurzbeschreibung     { get; private set; }
        public String   Unteraufträge                { get; private set; }
        public String   Auftragsnummer               { get; private set; }
        public String   Auftragsstatus               { get; private set; }
        public String   IHObjektNummer               { get; private set; }
        public String   Maschine                     { get; private set; }
        public String   Inventarnummer               { get; private set; }
        public String   Fabriknummer                 { get; private set; }
        public String   Standort                     { get; private set; }
        public String   Kostenstelle                 { get; private set; }
        public String   FBNummer                     { get; private set; }
        public String   Priorität1                   { get; private set; }
        public String   Dringlichkeit                { get; private set; }
        public String   Dringlichkeitname            { get; private set; }
        public String   Auftraggeber                 { get; private set; }
        public String   ExterneAuftragsnummer        { get; private set; }
        public String   Beauftragungsdatum           { get; private set; }
        public String   Fertigstellungstermin        { get; private set; }
        public String   Auftragswert                 { get; private set; }
        public String   Stundenvorgabe               { get; private set; }
        public String   Menge                        { get; private set; }
        public String   KWZyklus                     { get; private set; }
        public String   KWStart                      { get; private set; }
        public String   InnerhalbBetriebsferien      { get; private set; }
        public String   Leistungsart                 { get; private set; }
        public String   LeistungsartName             { get; private set; }
        public String   Schadensbild                 { get; private set; }
        public String   SubSchadensbild              { get; private set; }
        public String   Schadensursache              { get; private set; }
        public String   SubSchadensursache           { get; private set; }
        public String   MeldungAm                    { get; private set; }
        public String   MeldungUm                    { get; private set; }
        public String   ÜbergabeAm                   { get; private set; }
        public String   ÜbergabeUm                   { get; private set; }
        public String   Störzeit                     { get; private set; }
        public String   Aktivitätenbeschreibung      { get; private set; }
        public String   Abnahmeberechtigter          { get; private set; }
        public String   Abnahmedatum                 { get; private set; }
        public String   Auftragsleiter               { get; private set; }
        public String   Auftragsleiterdatum          { get; private set; }
        public String   LEGesamtkosten               { get; private set; }
        public String   MTGesamtkosten               { get; private set; }


        public List<AuftragLeMtRow> Leistungen       { get; private set; }


        public AuftragRow(Auftrag model, String ihpath, bool complete)
        {
        
            if (model == null) return;

            Auftragsnummer           = model.Nummer;
            IHObjektNummer           = model.AuIHKS?.IHObjekt;
            Inventarnummer           = model.AuIHKS?.Inventarnummer;
            Fabriknummer             = model.AuIHKS?.Fabriknummer;
            Standort                 = model.AuIHKS?.Standort;
            Kostenstelle             = model.AuIHKS?.Kostenstelle;
            FBNummer                 = model.FBNummer;
            Dringlichkeit            = model.Dringlichkeit?.Nummer;
            Dringlichkeitname        = model.Dringlichkeit?.Bezeichnung;
            Auftragskurzbeschreibung = model.Kurzbeschreibung;
            ExterneAuftragsnummer    = model.ExterneNummer;
            Auftragsstatus           = model.StatusAsString;

            Maschine = String.IsNullOrEmpty(ihpath)? model.AuIHKS?.Maschine: ihpath;
            if (String.IsNullOrEmpty(Maschine)) Maschine = "Asset wurde nicht gefunden.";
            if (model.Beauftragungsdatum.HasValue) Beauftragungsdatum = model.Beauftragungsdatum.Value.ToShortDateString();
            if (model.Fertigstellungstermin.HasValue) Fertigstellungstermin = model.Fertigstellungstermin.Value.ToShortDateString();
            if (model.Wert.HasValue) Auftragswert = model.Wert.Value.ToString("C");
            if (model.Stunden.HasValue) Stundenvorgabe = model.Stunden.ToString();
            Menge = model.Menge.GetValueOrDefault() == 0? "1": model.Menge.ToString(); 
            
            Leistungsart     = model.Leistungsart.Nummer;
            LeistungsartName = model.Leistungsart.Name;
            
            if (model.Schadensbild != null)         Schadensbild         = model.Schadensbild.Name;
            if (model.SubSchadensbild != null)      SubSchadensbild      = model.SubSchadensbild.Name;
            if (model.Schadensursache != null)      Schadensursache      = model.Schadensursache.Name;
            if (model.SubSchadensursache != null)   SubSchadensursache   = model.SubSchadensursache.Name;
            if (model.GemeldetDatum.HasValue)       MeldungAm            = model.GemeldetDatum.Value.ToShortDateString();
            if (model.GemeldetZeit.HasValue)        MeldungUm            = model.GemeldetZeit.Value.ToString("hh':'mm");
            if (model.ÜbergabeDatum.HasValue)       ÜbergabeAm           = model.ÜbergabeDatum.Value.ToShortDateString();
            if (model.ÜbergabeZeit.HasValue)        ÜbergabeUm           = model.ÜbergabeZeit.Value.ToString("hh':'mm");
            if (model.Störzeit.HasValue)            Störzeit             = model.Störzeit.Value.ToString();
           
            if (model.Abnahmeberechtigter != null)  Abnahmeberechtigter  = model.Abnahmeberechtigter.Nachname + ", " + model.Abnahmeberechtigter.Vorname + " [" + model.Abnahmeberechtigter.Nummer + "]";
            if (model.Abnahmedatum.HasValue)        Abnahmedatum         = model.Abnahmedatum.Value.ToShortDateString();
            if (model.Auftragsleiter != null)       Auftragsleiter       = model.Auftragsleiter.Nachname + ", " + model.Auftragsleiter.Vorname + " [" + model.Auftragsleiter.Nummer + "]";
            if (model.Auftragsleiterdatum.HasValue) Auftragsleiterdatum  = model.Auftragsleiterdatum.Value.ToShortDateString();
            if (model.Auftraggeber != null)         Auftraggeber         = model.Auftraggeber.Nachname + ", " + model.Auftraggeber.Vorname + " [" + model.Auftraggeber.Nummer + "]";
          
            if (!complete) return;

            Aktivitätenbeschreibung   = model.Aktivitätenbeschreibung;
           
            LEGesamtkosten = model.Personalleistungen.Sum(p => p.Kosten.GetValueOrDefault()).ToString("C");
            MTGesamtkosten = model.Materialleistungen.Sum(p => p.Gesamtpreis.GetValueOrDefault()).ToString("C");

            Leistungen = new List<AuftragLeMtRow>();
            var lelist = model.Personalleistungen.ToList();
            var mtlist = model.Materialleistungen.ToList();
            int len = Math.Max(lelist.Count, mtlist.Count);
            for (int i = 0; i < len; i++) Leistungen.Add(new AuftragLeMtRow(i >= lelist.Count? null: lelist[i], i >= mtlist.Count? null: mtlist[i])); 
        }
    }
}
