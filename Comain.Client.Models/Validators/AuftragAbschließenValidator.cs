﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Validators
{

    public class AuftragAbschließenValidator : AuftragSpeichernValidator, IAuftragValidator
    {
 
        public AuftragAbschließenValidator(Auftrag model) : base(model)
        {}
       

        public override IList<String> ErrorList
        {
            
            get { 
            
                var result = base.ErrorList;
                
                if (model.Status == Auftragstatus.Abgeschlossen) result.Add("Der Auftrag ist bereits abgeschlossen.");
                
                if (model.Status == Auftragstatus.Storno) result.Add("Der Auftrag ist bereits storniert.");
                
                if (model.IstHauptOderFbAuftrag && model.Leistungsart.IstPlanmäßigOderUnplanmäßig && !model.BerechneteStörzeit.HasValue) 
                    result.Add("Der Auftrag besitzt keine plausiblen Melde- und Übergabezeiten.");
                
                if (model.IstHauptOderFbAuftrag && model.Leistungsart.IstPlanmäßigOderUnplanmäßig && model.Schadensbild == null) 
                    result.Add("Der Auftrag besitzt kein Schadensbild.");
                
                if (model.IstHauptOderFbAuftrag && model.Leistungsart.IstPlanmäßigOderUnplanmäßig && model.Schadensursache == null) 
                    result.Add("Der Auftrag besitzt keine Schadensursache.");
                
                if (model.IstHauptOderFbAuftrag && model.Leistungsart.IstUnPlankosten && !model.StörzeitIstPlausibel) 
                    result.Add("Der Auftrag besitzt keine plausible Störzeit. Diese darf nicht größer sein als der Zeitraum zwischen Melde- und Übergabezeit.");
                
                if (model.IstHauptOderFbAuftrag && model.Störzeit.HasValue && (model.Störzeit.Value < 0 || model.Störzeit.Value > model.BerechneteStörzeit)) 
                    result.Add("Wenn eine Störzeit angegeben wurde muss diese plausibel sein.");
                
                if (model.Leistungsart.IstPlanmäßigOderUnplanmäßig && model.LeistungenVorMeldezeit) 
                    result.Add("Der Auftrag besitzt Leistungen die vor der Meldezeit liegen.");
            
                return result;
            }
        }
    }
}
