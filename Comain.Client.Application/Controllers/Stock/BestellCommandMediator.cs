﻿using System;
using Comain.Client.Services;
using Comain.Client.ViewModels.Stock.Artikelstamm;
using Comain.Client.ViewModels.Stock.Bestellungen;
using Comain.Client.ViewModels;
using Comain.Client.Models.Stock;

namespace Comain.Client.Controllers.Stock.Bestellungen
{

    public class BestellCommandMediator : IBestellCommandMediator
    {

        private readonly ILoginService login;
        private readonly BestellViewModel view;
        private readonly BestellDetailViewModel detailView;
        private readonly BestellDetailBaseViewModel basisView;
        private readonly BestellListViewModel listView;
        private readonly BestellFilterViewModel filterView;
        private readonly BestellDetailMemoViewModel memoView;


        public BestellCommandMediator(ILoginService login, BestellViewModel view, BestellDetailViewModel detailView, BestellDetailBaseViewModel basisView,
                                      BestellListViewModel listView, BestellFilterViewModel filterView, BestellDetailMemoViewModel memoView)
        {

            this.login = login;
            this.view = view;
            this.detailView = detailView;
            this.basisView = basisView;
            this.listView = listView;
            this.filterView = filterView;
            this.memoView = memoView;
        }


        private bool HasCurrent   { get { return detailView.Current != null; } }
        
        public EditMode CurrentMode { get; private set; }


        public void SetEditMode(EditMode newMode)
        {
        
            if (newMode == EditMode.Edit && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diese Bestellung nicht bearbeiten.");
            if (newMode == EditMode.Append && login.AktuellerNutzer.IstProduktion) throw new NotSupportedException("Sie dürfen diese Bestellung nicht bearbeiten.");

            CurrentMode = newMode;
            listView.EditEnabled   = newMode == EditMode.ReadOnly; 
            filterView.EditEnabled = newMode == EditMode.ReadOnly; 
            basisView.EditEnabled  = newMode != EditMode.ReadOnly; 
            memoView.EditEnabled   = newMode != EditMode.ReadOnly; 

            NeuSetzen();
        }

        
        public void NeuSetzen()
        {

            view.EditVisible        = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Current != null && detailView.Current.IsEditable;
            view.CancelVisible      = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Current != null;
            view.SaveVisible        = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion;
            view.AddVisible         = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == BestellDetailViewModel.Tabs.Basis;
            view.DeleteVisible      = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == BestellDetailViewModel.Tabs.Basis && 
                                      detailView.Current != null;
            view.ReloadVisible      = CurrentMode == EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion;
       
            view.AddPosVisible      = CurrentMode != EditMode.ReadOnly && !login.AktuellerNutzer.IstProduktion && detailView.Tab == BestellDetailViewModel.Tabs.Basis && 
                                      detailView.Current != null && detailView.Current.IsEditable;
            view.DeletePosVisible   = CurrentMode == EditMode.Edit && !login.AktuellerNutzer.IstProduktion && detailView.Tab == BestellDetailViewModel.Tabs.Basis && 
                                      detailView.Current != null && detailView.Current.IsEditable;
            view.SetOrderedVisible  = !login.AktuellerNutzer.IstProduktion && detailView.Current != null && detailView.Current.Status == Bestellstatus.Angelegt;
            view.SetClosedVisible   = !login.AktuellerNutzer.IstProduktion && detailView.Current != null && detailView.Current.IstOffen;
            view.SetStornoVisible   = !login.AktuellerNutzer.IstProduktion && detailView.Current != null && 
                                      (detailView.Current.IstOffen || detailView.Current.Status == Bestellstatus.Angelegt);
        }
    }
}
