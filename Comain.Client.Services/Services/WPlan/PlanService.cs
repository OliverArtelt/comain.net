﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Reports;
using Comain.Client.Gateways;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.WPlan;
using Comain.Client.Models.WPlan.Planen;
using Comain.Client.ReportModels.WPlan;
using Comain.Client.ViewData.WPlan;


namespace Comain.Client.Services.WPlan
{

    public class PlanService : IPlanService
    {

        private readonly IRepository store;


        public PlanService(IRepository store)
        {
            this.store = store;
        }


        public Task<ZeitstrahlDto> GetPlanAsync(WPlanCreateInfo nfo, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, ZeitstrahlDto>("/WPlan/Zeitstrahl", nfo, ct);


        public Task<Planstatistik> GetStatsAsync(WPlanCreateInfo nfo, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, Planstatistik>("/WPlan/Statistics", nfo, ct);


        public async Task StoreAsync(IEnumerable<PositionSubmitDto> dtos, CancellationToken ct = default)
        {

            if (dtos.IsNullOrEmpty()) return;
            await store.PostAsync<IEnumerable<PositionSubmitDto>>("/WPlan", dtos, ct).ConfigureAwait(false);
        }


        public Task<List<PZFilterItem>> IHTreeAsync(CancellationToken ct = default)
            => store.GetAsync<List<PZFilterItem>>("/WPlan/IHTree", ct);


        public Task<IList<NummerFilterModel>> FilterGewerkeAsync(CancellationToken ct = default)
            => store.GetAsync<IList<NummerFilterModel>>("/Filter/GW", ct);


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
            => store.GetAsync<IList<PersonalFilterModel>>("/Filter/PS", ct);


        public Task<IList<LeistungsartFilterModel>> FilterLeistungsartenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<LeistungsartFilterModel>>("/Filter/LA", ct);


        public async Task<List<AuftragReportItem>> DetailReportAsync(IEnumerable<int> auids, CancellationToken ct = default)
        {

            var dtos = await store.PostAsync<IEnumerable<int>, List<AuftragReportItem>>("/WPlan/ReportOffen", auids, ct);
            dtos = dtos.OrderBy(p => p.Auftragsnummer, new NaturalComparer()).ToList();
            int sortIndex = 0;
            dtos.ForEach(p => p.SortIndex = sortIndex++);
            return dtos;
        }


        public Task<List<MassnahmeRow>> MassnahmenReportAsync(WPlanCreateInfo filter, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, List<MassnahmeRow>>("/WPlan/MNReport", filter, ct);


        public Task<OrphanedCount> CountOrphanedAsync(WPlanCreateInfo args, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, OrphanedCount>("/ChkLst/Orphaned", args, ct);


        public Task<String> CreateAsync(WPlanCreateInfo args, CancellationToken ct = default)
            => store.PostAsync<WPlanCreateInfo, String>("/ChkLst/Create", args, ct);
    }
}
