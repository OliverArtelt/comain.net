﻿using Comain.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Gateways;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Models.Tickets;
using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Dtos;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Services.Tickets
{

    public class TicketService : ITicketService
    {

        private readonly IRepository store;
        private readonly IMapper mapper;


        public TicketService(IRepository store, IMapper mapper)
        {
            this.store = store;
            this.mapper = mapper;
        }


        public async Task<IList<TicketListDto>> SearchAsync(Suchkriterien filter, CancellationToken ct = default)
        {

            var url = "/Ticket/Search";
            var result = await store.FindAsync<IList<TicketListDto>>(url, filter, ct).ConfigureAwait(false);
            return result;
        }


        public async Task<Ticket> DetailAsync(int id, CancellationToken ct = default)
        {

            var url = String.Format("/Ticket/{0}", id);
            var dto = await store.GetAsync<TicketDetailDto>(url, ct).ConfigureAwait(false);
            if (dto == null) return null;

            var model = mapper.Map<TicketDetailDto, Ticket>(dto);
            model.AcceptChanges();
            return model;
        }


        public async Task<List<Media>> MediaAsync(int auftragId, CancellationToken ct = default)
        {

            var url = String.Format("/Bevor/Media/{0}", auftragId);
            var dtos = await store.GetAsync<List<MediaDto>>(url, ct).ConfigureAwait(false);
            if (dtos.IsNullOrEmpty()) return null;

            ct.ThrowIfCancellationRequested();

            var models = dtos.Select(p => new Media(p)).ToList();
            return models;
        }


        public async Task<(Ticket model, String sendefehler)> UpdateAsync(TicketUpdateDto dto, CancellationToken ct = default)
        {

            var url = @"/Ticket";
            var result = await store.PostAsync<TicketUpdateDto, TicketDetailDto>(url, dto, ct).ConfigureAwait(false);
            if (result == null) return (null, null);

            var newmodel = mapper.Map<TicketDetailDto, Ticket>(result);
            newmodel.AcceptChanges();
            return (newmodel, result.Sendefehler);
        }


        public async Task DeleteAsync(Ticket model, CancellationToken ct = default)
        {

            var url = $"/Ticket/{model.Id}";
            await store.DeleteAsync(url, ct).ConfigureAwait(false);
        }


        public async Task<(Ticket model, String sendefehler)> StornoAsync(Ticket model, CancellationToken ct = default)
        {

            var url = String.Format(@"/Ticket/Storno/{0}", model.Id);
            var result = await store.GetAsync<TicketDetailDto>(url, ct).ConfigureAwait(false);

            var newmodel = mapper.Map<TicketDetailDto, Ticket>(result);
            newmodel.AcceptChanges();
            return (newmodel, result.Sendefehler);
        }


        public async Task<(Ticket model, String sendefehler)> CloseAsync(Ticket model, CancellationToken ct = default)
        {

            var url = String.Format(@"/Ticket/Close/{0}", model.Id);
            var result = await store.GetAsync<TicketDetailDto>(url, ct).ConfigureAwait(false);

            var newmodel = mapper.Map<TicketDetailDto, Ticket>(result);
            newmodel.AcceptChanges();
            return (newmodel, result.Sendefehler);
        }


        public async Task<MeineTicketAnzahl> MeineTicketsAsync(CancellationToken ct = default)
        {

            var url = @"/Ticket/Task";
            var result = await store.GetAsync<MeineTicketAnzahl>(url, ct).ConfigureAwait(false);
            return result;
        }


        public Task<IList<PzFilterModel>> FilterStandorteAsync(CancellationToken ct = default)
            => FilterAsync<PzFilterModel>("/Filter/PZ", ct);


        public Task<IList<KstFilterModel>> FilterKostenstellenAsync(CancellationToken ct = default)
            => FilterAsync<KstFilterModel>("/Filter/KST", ct);


        public Task<IList<AuIHKSDto>> FilterAuIHKSAsync(CancellationToken ct = default)
            => FilterAsync<AuIHKSDto>("/Filter/AuIHKS", ct);


        public Task<IList<NummerFilterModel>> FilterGewerkeAsync(CancellationToken ct = default)
            => FilterAsync<NummerFilterModel>("/Filter/GW", ct);


        public Task<IList<PersonalFilterModel>> FilterPersonalAsync(CancellationToken ct = default)
            => FilterAsync<PersonalFilterModel>("/Filter/PS", ct);


        public Task<IList<NummerFilterModel>> FilterSchadensbilderAsync(CancellationToken ct = default)
            => FilterAsync<NummerFilterModel>("/Filter/SB", ct);


        public Task<IList<NummerFilterModel>> FilterSchadensursachenAsync(CancellationToken ct = default)
            => FilterAsync<NummerFilterModel>("/Filter/SU", ct);


        public async Task<IDringlichkeitenProvider> FilterDringlichkeitAsync(CancellationToken ct = default)
        {

            var list = await FilterAsync<Dringlichkeit>("/VW/DR", ct);
            return new DringlichkeitenProvider(list);
        }


        private Task<IList<TFilterModel>> FilterAsync<TFilterModel>(String url, CancellationToken ct)
        {
            return store.GetAsync<IList<TFilterModel>>(url, ct);
        }
    }
}
