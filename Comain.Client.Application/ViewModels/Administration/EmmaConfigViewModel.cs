﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;
using Comain.Client.Views.Emma;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Administration;
using Comain.Client.Views.Administration;

namespace Comain.Client.ViewModels.Administration
{

    public class EmmaConfigViewModel : ValidatableViewModel<IEmmaConfigView, Konfiguration>, IConfigViewModel
    {

        SynchronizationContext context = SynchronizationContext.Current;


        public EmmaConfigViewModel(IEmmaConfigView view) : base(view)
        {
        }


        public override void CurrentChanged(Konfiguration oldSelected)
        {

            RaisePropertyChanged(nameof(EmmaIHObjektNummern));
            RaisePropertyChanged(nameof(EmmaAuftragsNummern));
            RaisePropertyChanged(nameof(EmmaLeistungsart));
        }


        public void SetzeListe(IEnumerable<LeistungsartFilterModel> lalist)
        {

            context.Send(new SendOrPostCallback((o) => {

                Leistungsarten = lalist.OrderBy(p => p.Nummer).ToList();
            }), null);
        }


        public void Unload()
        {
            Leistungsarten = null;
        }


#region P R O P E R T I E S


        private IEnumerable<LeistungsartFilterModel> leistungsarten;
        public IEnumerable<LeistungsartFilterModel> Leistungsarten
        {
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        }


        public LeistungsartFilterModel EmmaLeistungsart
        {
            get {

                if (Current == null) return null;
                if (Leistungsarten.IsNullOrEmpty()) return null;
                if (!Current.EmmaLeistungsart.HasValue) return null;
                return Leistungsarten.FirstOrDefault(p => p.Id == Current.EmmaLeistungsart.Value);
            }
            set {

                if (Current != null) {

                    Current.EmmaLeistungsart = value?.Id;
                    RaisePropertyChanged(nameof(EmmaLeistungsart));
                }
            }
        }


        public bool EmmaIHObjektNummern
        {
            get { return Current == null? false: Current.EmmaIHObjektNummern; }
            set {

                if (Current != null && Current.EmmaIHObjektNummern != value) {

                    Current.EmmaIHObjektNummern = value;
                    RaisePropertyChanged(nameof(EmmaIHObjektNummern));
                }
            }
        }


        public bool EmmaAuftragsNummern
        {
            get { return Current == null? false: Current.EmmaAuftragsNummern; }
            set {

                if (Current != null && Current.EmmaAuftragsNummern != value) {

                    Current.EmmaAuftragsNummern = value;
                    RaisePropertyChanged(nameof(EmmaAuftragsNummern));
                }
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
