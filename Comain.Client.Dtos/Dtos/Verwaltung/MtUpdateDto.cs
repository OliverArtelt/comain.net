﻿using System.Collections.Generic;


namespace Comain.Client.Dtos.Verwaltung
{

    public class MtUpdateDto
    {

        public List<MaterialDto>  Materialliste   { get; set; }
        public List<MtLfMap>      MtLfMap         { get; set; }
    }
}
