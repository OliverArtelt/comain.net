﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class StörReport : ReportBase, IReport
    {
    
        private readonly ILoginService login;
        private readonly IReportService store;
        private IList<IFilterViewModel> kundenFilterViews;


        public StörReport(ILoginService login, IReportService store,  
                          ZeitViewModel zeitView, KostenstelleViewModel kstView, StandortViewModel pzView, IHStatusViewModel statusView, KundenViewModel kundenView,
                          LeistungsartViewModel laView )
        {
            
            this.login = login;
            this.store = store;
            kundenFilterViews = new List<IFilterViewModel> { zeitView, kstView, pzView, statusView, laView };
            filterViews = new List<IFilterViewModel> { zeitView, kstView, pzView, statusView, kundenView, laView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            StörDto dto = await store.StörAsync(GebeFilter(), ct).ConfigureAwait(false);
            
            result.AddDataSource("MainData", new List<StörDto> { dto });
            if (dto.JahrAnteil != 1m) result.AddParameter("parAnteil", "Hinweis: Die Sollwerte wurden anteilig vom Jahr ermittelt.");
            
            return result;
        }


        public override IList<IFilterViewModel> FilterViews
        {
            get {
            
                if (login.AktuellerNutzer.IstProduktion) return kundenFilterViews;
                return filterViews;           
            }
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Störauswertung"; }
        }


        public string Gruppe
        {
            get { return "Aufträge"; }
        }


        public int Sortierung
        {
            get { return 400; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.StörReport.rdlc"; }
        }
    }
}
