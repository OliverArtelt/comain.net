﻿using System.Windows;
using System.Windows.Controls;
using Comain.Client.ViewModels.IHObjekte;

namespace Comain.Client.Views.IHObjekte
{

    public partial class IHSelektorView:UserControl    ,IIHSelektorView
    {

        public IHSelektorView()
        {
            InitializeComponent();
        }


        private void OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            var vm = DataContext as IHSelektorViewModel;
            if (vm != null) vm.SelectItem(e.NewValue);
        }


        public void ResetSelection()
        {

            var tvi = PzTree.ItemContainerGenerator.ContainerFromIndex(0) as System.Windows.Controls.TreeViewItem;
            if (tvi != null) { tvi.IsSelected = true; tvi.IsSelected = false; }
            tvi = KstTree.ItemContainerGenerator.ContainerFromIndex(0) as System.Windows.Controls.TreeViewItem;
            if (tvi != null) { tvi.IsSelected = true; tvi.IsSelected = false; }
        }
    }
}
