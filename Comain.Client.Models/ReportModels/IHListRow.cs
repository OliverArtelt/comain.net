﻿using System;
using Comain.Client.Dtos.Reports;


namespace Comain.Client.ReportModels
{

    public class IHListRow
    {

        public String       IHObjekt                { get; set; }
        public String       Maschine                { get; set; }
        public String       Standort                { get; set; }
        public String       Kostenstelle            { get; set; }
        public String       Inventarnummer          { get; set; }
        public String       Bemerkung               { get; set; }
        public short?       Baujahr                 { get; set; }
        public short?       Anschaffjahr            { get; set; }
        public String       Fabriknummer            { get; set; }
        public String       Hersteller              { get; set; }
        public String       Lieferant               { get; set; }
        public String       Instandhalter           { get; set; }
        public String       Typ                     { get; set; }
        public String       TechnischeDaten         { get; set; }
        public String       IHStatus                { get; set; }
        public String       Erhaltklasse            { get; set; }
        public String       VerantwIH               { get; set; }
        public String       VerantwProd             { get; set; }

        public DateTime?    DetailDatum             { get; set; }
        public String       DetailNummer            { get; set; }
        public String       DetailName              { get; set; }
        public String       DetailTyp               { get; set; }
        
        public bool         HasDetails              { get; set; }
    
    
        public IHListRow()
        {}
    
    
        public IHListRow(IHListDto dto)
        {
        
            IHObjekt        = dto.IHObjekt        ;    
            Maschine        = dto.Maschine        ;    
            Standort        = dto.Standort        ; 
            Kostenstelle    = dto.Kostenstelle    ;
            Inventarnummer  = dto.Inventarnummer  ;
            Bemerkung       = dto.Bemerkung       ;
            Baujahr         = dto.Baujahr         ;
            Anschaffjahr    = dto.Anschaffjahr    ;
            Fabriknummer    = dto.Fabriknummer    ;
            Hersteller      = dto.Hersteller      ;
            Lieferant       = dto.Lieferant       ;
            Instandhalter   = dto.Instandhalter   ;
            Typ             = dto.Typ             ;
            TechnischeDaten = dto.TechnischeDaten ;
            IHStatus        = dto.IHStatus        ;
            Erhaltklasse    = dto.Erhaltklasse    ;
            VerantwIH       = dto.VerantwIH;
            VerantwProd     = dto.VerantwProd;

            HasDetails = false;
        }
    
    
        public IHListRow(IHListDto dto, IHListDetailDto detail) : this(dto)
        {
                        
            DetailDatum     = detail.Datum;  
            DetailNummer    = detail.Nummer; 
            DetailName      = detail.Name;   
            DetailTyp       = detail.Typ; 

            HasDetails = true;
        }
    }
}
    