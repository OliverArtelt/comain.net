﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using Comain.Client.ViewModels.WPlan.Aufträge;


namespace Comain.Client.Views.WPlan.Aufträge
{

    public partial class ChecklistView : UserControl, IChecklistView
    {

        public ChecklistView()
        {
            InitializeComponent();
        }


        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if (e.ButtonState == MouseButtonState.Pressed && e.LeftButton == MouseButtonState.Pressed)
                ((ChecklistViewModel)DataContext).DetailCommand?.Execute(null);
        }
    }
}
