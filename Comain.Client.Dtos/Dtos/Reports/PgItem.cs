﻿namespace Comain.Client.Dtos.Reports
{

    public class PgItem
    {

        public int      Jahr            { get; set; }
        public decimal? PlanKosten      { get; set; }
        public decimal? UnplanKosten    { get; set; }


        public decimal? Planungsgrad
        {
            get {

                var nenner = PlanKosten + UnplanKosten;
                if (nenner.GetValueOrDefault() <= 0) return null;
                return PlanKosten / nenner; 
            }
        }
    }
}
