﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Comain.Client.Models.WPlan.Planen
{

    public interface IPlanzeile : IAcceptor
    {

        String      TermintypText           { get; }
        String      Nummer                  { get; }
        String      AnzeigeName             { get; }
        String      Kurzbeschreibung        { get; }
        String      Durchführung            { get; }
        String      Kostenstelle            { get; }
        String      Leistungsart            { get; }
        String      Gewerk                  { get; }
        DateTime?   Start                   { get; }
        String      ZyklusAsString          { get; }
        String      ExterneAuftragsnummer   { get; }
        bool        AllowDrop               { get; }
        bool        ExtendedTooltip         { get; }
        Brush       TypAsFarbe              { get; }
        double      Height                  { get; }
        double      XStartposition          { get; }
        double      YStartposition          { get; set; }
        String      Icon                    { get; }
        String      Info                    { get; }


        IEnumerable<Planaufgabe> GebePositionen(int? mincol = null, int? maxcol = null);

        bool Matches(String searchTerm);
    }
}
