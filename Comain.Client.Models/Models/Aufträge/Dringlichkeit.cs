﻿using System;
using System.ComponentModel;


namespace Comain.Client.Models.Aufträge
{

    public class Dringlichkeit : Entity, IDataErrorInfo, IComparable<Dringlichkeit>
    {

        private Auftragstyp  auftragstyp   ;
        private int          position      ;
        private String       nummer        ;
        private String       bezeichnung   ;
        private bool         wirdVerwendet ;
        private int?         leistungsart  ;
        private int?         tagesfrist    ;


#region P R O P E R T I E S


        public Auftragstyp Auftragstyp     
        {
            get { return auftragstyp; }
            set { SetProperty(ref auftragstyp, value); }
        }
        

        public int Position        
        {
            get { return position; }
            set { SetProperty(ref position, value); }
        }
        

        public String Nummer   
        {
            get { return nummer; }
            set { SetProperty(ref nummer, value); }
        }
        

        public String Bezeichnung    
        {
            get { return bezeichnung; }
            set { SetProperty(ref bezeichnung, value); }
        }
        

        public bool WirdVerwendet 
        {
            get { return wirdVerwendet; }
            set { SetProperty(ref wirdVerwendet, value); }
        }
        

        public int? Leistungsart  
        {
            get { return leistungsart; }
            set { SetProperty(ref leistungsart, value); }
        }
        

        public int? Tagesfrist 
        {
            get { return tagesfrist; }
            set { SetProperty(ref tagesfrist, value); }
        }
        

#endregion

        public String Fullname  { get { return $"{Nummer} - {Bezeichnung}"; } } 


        public String AuftragstypAsText   
        { 
            get { 
        
                switch (Auftragstyp) {
                    case Auftragstyp.BesonderesVorkommnis: return "Besonderes Vorkommnis";
                    case Auftragstyp.Ticket: return "Ticket"; 
                    default: return "Auftrag";
                }
            } 
        } 


        public String BackgroundColor
        {
            get { return GetBackgroundColor(Position); }
        }


        public String ForegroundColor
        {
            get { return GetForegroundColor(Position); }
        }

        public static String GetBackgroundColor(int position)
        {

            if (position == 0) return "White";
            if (position == 1) return "Crimson";
            if (position == 2) return "SandyBrown";
            if (position == 3) return "Yellow";
            if (position == 4) return "LightGreen";
            if (position == 5) return "LightBlue";
            return "Gray";
        }
        

        public static String GetForegroundColor(int position)
        {
               
            if (position == 1) return "White";
            return "Black";
        }


        public int CompareTo(Dringlichkeit other)
        {
        
            if (other == this) return 0;
            if (other == null) return 1;
            int cv = this.Auftragstyp.CompareTo(other.Auftragstyp);
            if (cv != 0) return cv;
            return this.Nummer.CompareNatural(other.Nummer);
        }


#region V A L I D A T I O N

       
        public string Error
        {
            
            get { 
            
                String test = this[nameof(Position)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Nummer)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Bezeichnung)];
                if (!String.IsNullOrEmpty(test)) return test;
                test = this[nameof(Tagesfrist)];
                return test;
            }
        }


        public string this[string columnName]
        {
            
            get { 
            
                switch (columnName) {

                case nameof(Position):

                    if (Position < 1 || Position > 5) return "Die Position muss zwischen 1 und 5 liegen."; 
                    break;

                case nameof(Nummer):

                    if (String.IsNullOrWhiteSpace(Nummer)) return "Geben Sie die Nummer an.";
                    if (Nummer.Length > 10) return "Die Nummer darf aus maximal 10 Zeichen bestehen."; 
                    break;

                case nameof(Bezeichnung):

                    if (String.IsNullOrWhiteSpace(Bezeichnung)) return "Geben Sie die Bezeichnung an.";
                    if (Bezeichnung.Length > 250) return "Die Bezeichnung darf aus maximal 250 Zeichen bestehen."; 
                    break;

                case nameof(Tagesfrist):

                    if (!Tagesfrist.HasValue) return null;
                    if (Tagesfrist.Value < 1) return "Wenn eine Tagesfrist angegeben wurd muss diese größer 0 sein."; 
                    break;
                }

                return null;
            }
        }


#endregion

    }
}
