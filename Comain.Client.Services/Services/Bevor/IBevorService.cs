﻿using Comain.Client.ViewData.Verwaltung;
using Comain.Client.Dtos.Verwaltung;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models.Tickets;
using Comain.Client.Models;

namespace Comain.Client.Services.Bevor
{

    public interface IBevorService
    {

        Task<IList<AuIHKSDto>>              IHObjekteAsync(CancellationToken ct = default);
        Task<String>                        IHPathAsync(string ihnummer, CancellationToken ct = default);
        Task<IList<PzFilterModel>>          StandorteAsync(CancellationToken ct = default);
        Task<IList<PersonalFilterModel>>    PersonalAsync(CancellationToken ct = default);
        Task<IDringlichkeitenProvider>      DringlichkeitAsync(CancellationToken ct = default);

        Task<String>                        CreateAsync(BevorTicket dto, CancellationToken ct = default);

        Task<List<Media>>                   ReadFilesAsync(IEnumerable<String> paths, CancellationToken ct = default);
    }
}