﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Commands;


namespace Comain.Client.ViewModels
{

    public class CommandGroup
    {
    
        public String                           Name            { get; set; }
        public IEnumerable<IControllerCommand>  Commands        { get; set; }
    }
}
