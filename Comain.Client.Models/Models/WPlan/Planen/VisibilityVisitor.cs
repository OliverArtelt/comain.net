﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.WPlan.Planen
{

    public class VisibilityVisitor : IVisitor
    {

        private bool    offene;         // offene Positionen anzeigen?
        private bool    erledigte;      // erledigte Positionen anzeigen?
        private bool    nurRelevante;   // Maßnahmen ohne Positionen im Zeitraum ausblenden (Sonderleistungen / Bedarfspositionen beachten)
        private bool    nurMaterial;    // Maßnahmen ohne Material im Zeitraum ausblenden
        private int?    personal;       // personal has value: nur eigene (die des Personals) anzeigen
        private int     currentRownum;  // für alternative Zeilenfarbe


        public VisibilityVisitor(bool offene, bool erledigte, bool nurRelevante, bool nurMaterial, int? personal)
        {

            this.offene = offene;
            this.erledigte = erledigte;
            this.nurRelevante = nurRelevante;
            this.personal = personal;
            this.nurMaterial = nurMaterial;
        }


        public void Visit(Plan model)
        {}


        public void Visit(Planspaltensumme model)
        {}


        public void Visit(PZPlanzeile model)
        {}


        public void Visit(PZSpaltensumme model)
        {}


        public void Visit(IHPlanzeile model)
        {}


        public void Visit(IHSpaltensumme model)
        {}


        public void Visit(MNPlanzeile model)
        {

            model.LeereZeileAnzeigen = !nurRelevante;

            model.Positionen.ForEach(pos => {

                if (personal.HasValue && !pos.HatPersonal(personal.Value)) {

                    pos.IstSichtbar = false;
                    return;
                }

                if (offene && erledigte) pos.IstSichtbar = true;
                else if (offene)         pos.IstSichtbar = pos.Ergebnis == Checklistergebnis.Undefiniert;
                else if (erledigte)      pos.IstSichtbar = pos.Ergebnis != Checklistergebnis.Undefiniert;
                else                     pos.IstSichtbar = false;

                if (nurMaterial && pos.Materialien.IsNullOrEmpty()) pos.IstSichtbar = false;
            });

            model.Zeilenfarbenindex = model.IstSichtbar? currentRownum++: -1;
        }


        public void Visit(Planzelle model)
        {}


        public void Visit(Planaufgabe model)
        {}


        public void VisitAfter(Plan model)
        {}


        public void VisitAfter(PZPlanzeile model)
        {}


        public void VisitAfter(IHPlanzeile model)
        {}
    }
}
