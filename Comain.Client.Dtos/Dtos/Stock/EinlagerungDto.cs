﻿using System;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Stock
{

    public class EinlagerungDto : ChangeTrackingBase
    {

        public decimal          Menge               { get; set; }
        public decimal          Restmenge           { get; set; }
        public decimal?         Preis               { get; set; }
        public DateTime         Datum               { get; set; }
        public LagerortDto      Lagerort            { get; set; }
        public EingangDto       Wareneingang        { get; set; }
        public ArtikelDto       Material            { get; set; }
    }
}
