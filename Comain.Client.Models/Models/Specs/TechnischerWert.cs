﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Specs
{

    [DebuggerDisplay("{Name}")]
    public class TechnischerWert : Entity, IDataErrorInfo
    {

        public static readonly char[] enumSplitter = new char[] {'\n', '\r'};


        public String       Name            { get; set; }
        public bool         Pflichtfeld     { get; set; }
        public Wertetyp     Wertetyp        { get; set; }
        public String       Einheit         { get; set; }
        public String       Enumeration     { get; set; }
        public String       AssetTab        { get; set; }
        public String       ArtikelTab      { get; set; }


        public bool TextVisible     => Wertetyp != Wertetyp.Numeric && Wertetyp != Wertetyp.Boolean && Wertetyp != Wertetyp.Enumeration;
        public bool NumericVisible  => Wertetyp == Wertetyp.Numeric;
        public bool BoolVisible     => Wertetyp == Wertetyp.Boolean;
        public bool EnumVisible     => Wertetyp == Wertetyp.Enumeration;


        public decimal? wert;
        public decimal? Wert
        {
            get => wert;
            set => SetProperty(ref wert, value);
        }


        public bool BoolWert
        {
            get => wert.GetValueOrDefault() > 0;
            set {

                if (value != BoolWert) {

                    wert = value? 1: 0;
                    RaisePropertyChanged(nameof(BoolWert));
                    RaisePropertyChanged(nameof(Wert));
                }
            }
        }


        public String text;
        public String Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }


        public IEnumerable<String> EnumValues => Enumeration?.Split(enumSplitter, StringSplitOptions.RemoveEmptyEntries);


#region V A L I D A T I O N


        public string Error => null;
        public string this[string columnName] => null;


#endregion

    }
}
