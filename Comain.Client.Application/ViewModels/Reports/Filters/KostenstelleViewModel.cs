﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class KostenstelleViewModel : ViewModel<IKostenstelleView>, IFilterViewModel
    {

        private readonly IFilterService store;
        private readonly SynchronizationContext context;


        public KostenstelleViewModel(IFilterService store, IKostenstelleView view) : base(view)
        {

            context = SynchronizationContext.Current;
            this.store = store;
        }


        public void Reset()
        {
            
            if (store.Kostenstellen.IsNullOrEmpty()) return;

            context.Send(new SendOrPostCallback((o) => {
                
                KstVon = store.Kostenstellen.First();
                KstBis = store.Kostenstellen.Last();
            }), null);
        }


        public String Validiere()
        {
            
            if (KstVon == null || KstBis == null) return "Geben Sie zwei Kostenstellen an.";
            if (KstVon.Sortierung > KstBis.Sortierung) return "Die erste Kostenstelle darf nicht größer als die zweite sein.";
            return null;
        }


        public string FilterAlsString()
        {
            return String.Format("Kostenstelle {0} - {1}", KstVon.Nummer, KstBis.Nummer);
        }


        public void AddFilter(Suchkriterien filter)
        {

            filter.Add("KstVon", KstVon.Sortierung);
            filter.Add("KstBis", KstBis.Sortierung);
        }


        public Task LoadAsync(CancellationToken ct = default)
        {
            
            RaisePropertyChanged("Kostenstellen");
            Reset();
            return Task.FromResult(true);     
        }


        public void Unload()
        {}


#region P R O P E R T I E S
   

        public IList<KstFilterModel> Kostenstellen
        { 
            get { return store == null? null: store.Kostenstellen; }
        } 
   

        private KstFilterModel kstVon;
        public KstFilterModel KstVon
        { 
            get { return kstVon; }
            set { SetProperty(ref kstVon, value); }
        } 
   

        private KstFilterModel kstBis;
        public KstFilterModel KstBis
        { 
            get { return kstBis; }
            set { SetProperty(ref kstBis, value); }
        } 
   

#endregion

    }
}
