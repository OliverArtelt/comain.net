﻿using System;


namespace Comain.Client.Models.WPlan
{

    public enum Checklistergebnis
    {

        Undefiniert = 0,
        OK = 1,
        NotOK = 2,
        WirdNichtErledigt = 3,
        Fortgeführt = 4
    }


    public static class ChecklistergebnisExtensions
    {

        public static String AsText(this Checklistergebnis value)
        {

            switch (value) {

                case Checklistergebnis.OK: return "OK";
                case Checklistergebnis.NotOK: return "Nicht OK";
                case Checklistergebnis.WirdNichtErledigt: return "Wird nicht Erledigt";
                case Checklistergebnis.Fortgeführt: return "Fortgeführt";
                default: return "Offen";
            }
        }


        public static String AsShortText(this Checklistergebnis value)
        {

            switch (value) {

                case Checklistergebnis.OK: return "OK";
                case Checklistergebnis.NotOK: return "NOK";
                case Checklistergebnis.WirdNichtErledigt: return "WNE";
                case Checklistergebnis.Fortgeführt: return "FG";
                default: return "Offen";
            }
        }
    }
}
