﻿using System.Threading.Tasks;
using Comain.Client.Services;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Settings;

namespace Comain.Client.Controllers
{

    public interface IConfigController : IController
    {}

   
    public class ConfigController : IConfigController
    {
        
        private readonly IMessageService messageSvc;
        private readonly IBreadcrumbService crumbs;
        private readonly SettingsViewModel settingsView;
        private readonly MainViewModel mainView;


        public ConfigController(MainViewModel mainView, SettingsViewModel settingsView, IMessageService messageSvc,
                                IBreadcrumbService crumbs, IControllerBus bus)
        {

            this.settingsView = settingsView;
            this.mainView = mainView;
            this.messageSvc = messageSvc;
            this.crumbs = crumbs;
        }


        public virtual void Initialize()
        {
        }


        public async Task RunAsync(object parameter)
        {

            mainView.PageView = settingsView.View;
            crumbs.Add("Einstellungen");
        }


        public virtual Task<bool> UnloadAsync()
            => Task.FromResult(true);


        public virtual void Shutdown()
        {}
    }
}
