﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Controllers.Aufträge;
using Comain.Client.Controllers.IHObjekte;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services.IHObjekte;
using Comain.Client.Dtos.Tickets;
using Comain.Client.Views.Tickets;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.IHObjekte;


namespace Comain.Client.ViewModels.Tickets
{

    public class AddTicketViewModel : ViewModel<IAddTicketView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly IHSimpleSelektorViewModel selektorView;


        public AddTicketViewModel(IAddTicketView view, IIHFastSearchProvider ihSearcher, IHSimpleSelektorViewModel selektorView)
          : base(view)
        {

            context = SynchronizationContext.Current;
            IHSearchProvider = ihSearcher;
            this.selektorView = selektorView;
            PropertyChangedEventManager.AddHandler(selektorView, ViewPropertyChanged, "");
        }


        public async Task SetFilterAsync(IList<AuIHKSDto> ihlist, IList<NummerFilterModel> gwlist, IList<PersonalFilterModel> pslist,
                                         IList<PzFilterModel> pzlist, IList<Dringlichkeit> drlist, CancellationToken ct = default)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Gewerke = gwlist;
                Personalliste = pslist;
                Dringlichkeiten = drlist.Where(p => p.WirdVerwendet && p.Auftragstyp == Auftragstyp.Ticket).OrderBy(p => p.Position).ToList();

            }), null);

            var ifilst = ihlist.Select(p => new IHFilterItem(p)).ToList();
            IHSearchProvider.SetItems(ifilst);
            await selektorView.LoadAsync(ifilst, pzlist, ct).ConfigureAwait(false);
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(selektorView.CurrentIHObjekt) && selektorView.CurrentIHObjekt != null) IHObjekt = selektorView.CurrentIHObjekt;
        }


        public NeuesTicketDto GebeAuftrag()
        {

            var validatorMsgs = new List<string>();
            if (IHObjekt == null)
                validatorMsgs.Add("• Geben Sie die betreffende Maschine bzw. die betroffene Baugruppe an.");
            if (Dringlichkeit == null)
                validatorMsgs.Add("• Wählen Sie eine Dringlichkeit.");
            if (String.IsNullOrWhiteSpace(Kurzbeschreibung))
                validatorMsgs.Add("• Beschreiben Sie möglichst präzise das vorliegende Problem im Feld Kurzbeschreibung, damit sich der Instandhalter auf die Bearbeitung vorbereiten kann..");
            if (!Datum.HasValue)
                validatorMsgs.Add("• Geben Sie das Meldedatum an.");
            if (!uhrzeit.HasValue)
                validatorMsgs.Add("• Geben Sie die Meldezeit an.");

            if (validatorMsgs.Count > 0) throw new ComainValidationException("Der Auftrag kann nicht angelegt werden", validatorMsgs);

            var au = new NeuesTicketDto { AuIHKS = IHObjekt.Id, Dringlichkeit_Id = Dringlichkeit.Id, Kurzbeschreibung = Kurzbeschreibung,
                                          Anmerkung = Anmerkung, Gewerk_Id = Gewerk?.Id, Personal_Id = Personal?.Id };
            au.GemeldetAm = Datum.Value.Add(Uhrzeit.Value);

            return au;
        }


        public void Clear()
        {

            IHObjekt         = null;
            Personal         = null;
            Gewerk           = null;
            Kurzbeschreibung = null;
            Anmerkung        = null;
            Datum            = DateTime.Now.Date;
            Uhrzeit          = DateTime.Now.TimeOfDay;
            Dringlichkeit    = null;
            selektorView.Clear();
        }


        public void Unload()
        {

            Personalliste = null;
            Gewerke = null;
            selektorView.Unload();
            Dringlichkeiten = null;
        }


#region P R O P E R T I E S


        public object SelektorView => selektorView?.View;


        private IIHFastSearchProvider ihSearchProvider;
        public IIHFastSearchProvider IHSearchProvider
        {
            get { return ihSearchProvider; }
            set { SetProperty(ref ihSearchProvider, value); }
        }


        private IHFilterItem iHObjekt;
        public IHFilterItem IHObjekt
        {
            get { return iHObjekt; }
            set {

                if (SetProperty(ref iHObjekt, value)) {

                    RaisePropertyChanged(nameof(IHNummer));
                    RaisePropertyChanged(nameof(Baugruppenpfad));
                }
            }
        }


        public String IHNummer          => iHObjekt?.Nummer;
        public String Baugruppenpfad    => iHObjekt?.Pfad ?? iHObjekt?.Name;


        private IList<PersonalFilterModel> personalliste;
        public IList<PersonalFilterModel> Personalliste
        {
            get { return personalliste; }
            set { SetProperty(ref personalliste, value); }
        }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get { return personal; }
            set { SetProperty(ref personal, value); }
        }


        private IList<NummerFilterModel> gewerke;
        public IList<NummerFilterModel> Gewerke
        {
            get { return gewerke; }
            set { SetProperty(ref gewerke, value); }
        }


        private NummerFilterModel gewerk;
        public NummerFilterModel Gewerk
        {
            get { return gewerk; }
            set { SetProperty(ref gewerk, value); }
        }


        private String kurzbeschreibung;
        public String Kurzbeschreibung
        {
            get { return kurzbeschreibung; }
            set { SetProperty(ref kurzbeschreibung, value); }
        }


        private String anmerkung;
        public String Anmerkung
        {
            get { return anmerkung; }
            set { SetProperty(ref anmerkung, value); }
        }


        private DateTime? datum;
        public DateTime? Datum
        {
            get { return datum; }
            set { SetProperty(ref datum, value); }
        }


        private TimeSpan? uhrzeit;
        public TimeSpan? Uhrzeit
        {
            get { return uhrzeit; }
            set { if (SetProperty(ref uhrzeit, value)) RaisePropertyChanged(nameof(UhrzeitText)); }
        }


        public String UhrzeitText
        {
            get { return !uhrzeit.HasValue ? null: uhrzeit.Value.ToString("hh\\:mm"); }
            set {

                var time = value.ParseTime();
                if (uhrzeit != time) {

                    Uhrzeit = time;
                    RaisePropertyChanged(nameof(Uhrzeit));
                }
            }
        }


        private IList<Dringlichkeit> dringlichkeiten;
        public IList<Dringlichkeit> Dringlichkeiten
        {
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        private Dringlichkeit dringlichkeit;
        public Dringlichkeit Dringlichkeit
        {
            get { return dringlichkeit; }
            set { SetProperty(ref dringlichkeit, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private bool tabletMode;
        public bool TabletMode
        {
            get { return tabletMode; }
            set { SetProperty(ref tabletMode, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
