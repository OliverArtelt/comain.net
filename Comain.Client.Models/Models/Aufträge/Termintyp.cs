﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.Aufträge
{

    public enum Termintyp
    {

        Undefiniert = 0,
        /// <summary>
        /// Aufträge erhalten eine nicht veränderliche Fälligkeit.
        /// Anwendungsfälle: Sicherheitsrelevante, fest terminierte Vorgänge.
        /// Sachkundigenprüfung, VBG, behördliche Fristen, Zulassungsfristen
        /// </summary>
        UnveränderlicheKette = 1,
        /// <summary>
        /// Terminkette wird bei Fortsetzung der Planung ab dem letzten realisierten Auftrag fortgeführt.
        /// </summary>
        VeränderlicheKette = 2,
        /// <summary>
        /// Die Aufträge werden wöchentlich ausgeführt, Tagesmuster gibt bitkodiert die Wochentage an
        /// </summary>
        Tagesplanung = 3,
        /// <summary>
        /// Die Aufträge werden bei Gelegenheit mit abgearbeitet (wenn der Hausmeister zur Außenstelle rausfährt um den Wasserstand abzulesen kann er auch gleich die Hecke schneiden) 
        /// </summary>
        Bedarfsposition = 4
    }


    public static class TermintypExtensions
    {

        public static String AsText(this Termintyp value)
        {

            if (value == Termintyp.VeränderlicheKette)   return "Veränderliche Wochenplanung";
            if (value == Termintyp.UnveränderlicheKette) return "Unveränderliche Wochenplanung";
            if (value == Termintyp.Tagesplanung)         return "Tagesplanung";
            if (value == Termintyp.Bedarfsposition)      return "Bedarfsposition";
            return String.Empty;
        }
    }
}
