﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Models.IHObjekte
{

    public enum IHObjektStatus
    {

        Undefiniert = 0,
        SehrHoch = 1,
        Hoch = 2,
        Gering = 3,
        Keine = 4,
    }


    public static class IHObjektStatusExtensions
    {
    
        public static String AsString(this IHObjektStatus? value)
        {
               
            switch (value.GetValueOrDefault()) {

                case IHObjektStatus.SehrHoch: return "sehr hoch";
                case IHObjektStatus.Hoch:     return "hoch";
                case IHObjektStatus.Gering:   return "gering";
                case IHObjektStatus.Keine:    return "keine";
            }

            return null;
        }
    } 
}
