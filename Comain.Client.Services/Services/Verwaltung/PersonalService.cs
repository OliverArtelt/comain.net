﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.Verwaltung;
using Comain.Client.Gateways;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Services.Verwaltung
{

    public class PersonalService : SimpleService<PersonalDto, Personal>, IPersonalService
    {

        public PersonalService(IRepository store, IMapper mapper) : base(store, mapper)
        {}


        public async Task<IList<NummerFilterModel>> GewerkeAsync(CancellationToken ct = default)
        {

            var result = (await store.GetAsync<IList<NummerFilterModel>>(@"Filter/GW", ct).ConfigureAwait(false)).ToList();
            result.Sort();
            return result;
        }


        public Task<IList<IHObjektFilterModel>> MaschinenAsync(CancellationToken ct = default)
            => store.GetAsync<IList<IHObjektFilterModel>>(@"Filter/Maschinen", ct);
    }
}
