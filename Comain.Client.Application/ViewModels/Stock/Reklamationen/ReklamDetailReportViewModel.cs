﻿using Comain.Client.Services;
using Comain.Client.Views.Stock.Reklamationen;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Reklamationen
{
    
    public class ReklamDetailReportViewModel : PrintViewModel<IReklamDetailReportView>, IDelayableViewModel
    {

        public ReklamDetailReportViewModel(IMessageService messages, IReklamDetailReportView view) : base(messages, view)
        {
        }
    }
}
