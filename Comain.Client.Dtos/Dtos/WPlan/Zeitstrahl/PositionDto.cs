﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.WPlan.Zeitstrahl
{

    public class PositionDto
    {

        public int              Position_Id             { get; set; }
        public int              Auftrag_Id              { get; set; }
        public int              Massnahme_Id            { get; set; }
        public String           Auftragsnummer          { get; set; }
        public int              Ergebnis                { get; set; }
        public int              Auftragsstatus          { get; set; }
        public DateTime?        FälligAm                { get; set; }
        public DateTime?        DurchgeführtAm          { get; set; }
        public DateTime?        ÜbergabeAm              { get; set; }
        public int?             Normzeit                { get; set; }
        public int?             MinutenGearbeitet       { get; set; }
        public int              Menge                   { get; set; }
        public int?             MengeGearbeitet         { get; set; }
        public decimal?         Wert                    { get; set; }
        public String           Folgeauftrag            { get; set; }
        public String           Bemerkungen             { get; set; }

        public int?             Auftragnehmer_Id        { get; set; }
        public int?             Auftraggeber_Id         { get; set; }
        public int?             Auftragsleiter_Id       { get; set; }
        public int?             Abnahmeberechtigter_Id  { get; set; }
        public List<int>        PersonalIds             { get; set; }
    }
}
