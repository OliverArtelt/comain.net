﻿using System;
using System.Collections.Generic;
using System.Linq;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.WPlan.Zeitstrahl;
using Comain.Client.Models.Aufträge;
using System.Diagnostics;
using Comain.Client.ReportModels.WPlan;
using System.Windows.Media;

namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Ein IH-Objekt mit Summeninformationen
    /// </summary>
    [DebuggerDisplay("{Nummer} {Name}")]
    public class IHPlanzeile : NotifyBase, IPlanzeile, IAcceptor
    {

        private readonly IHObjektDto  ih;


        public IHPlanzeile(PZPlanzeile parent, IHObjektDto ih)
        {

            this.ih = ih;
            Parent = parent;
            Summen = Enumerable.Range(0, Plan.Spalten.Count).Select(ndx => new IHSpaltensumme(this, Plan.Spalten[ndx])).ToList();
        }


        public bool                     IstSichtbar             => Children.Any(p => p.IstSichtbar);
        public bool                     IstDeaktiviert          => ih.IstInaktiv;
        public String                   DeaktiviertText         => IstDeaktiviert? "Asset wurde deaktiviert": null;
        public double                   Height                  => IstSichtbar? 40: 0;
        public double                   YStartposition          { get; set; }
        public PZPlanzeile              Parent                  { get; }

        public Plan                     Plan                    => Parent.Parent;
        public int                      Id                      => ih.Id;
        public String                   Nummer                  => ih.Nummer;
        public String                   Name                    => ih.Pfad;
        public String                   AnzeigeName             => Name;
        public String                   Inventarnummer          => ih.Inventarnummer;
        public String                   Kurzbeschreibung        => default;
        public String                   Durchführung            => default;
        public String                   Kostenstelle            => default;
        public String                   Leistungsart            => default;
        public String                   Gewerk                  => default;
        public DateTime?                Start                   => default;
        public String                   ZyklusAsString          => default;
        public String                   ExterneAuftragsnummer   => default;
        public String                   TermintypText           => default;
        public List<MaterialInfoDto>    MaterialInfo            => default;


        public bool                     AllowDrop               => false;
        public bool                     ExtendedTooltip         => false;
        public Brush                    TypAsFarbe              => Brushes.PapayaWhip;
        public double                   XStartposition          => 20d;
        public String                   Icon                    => "appbar_cog";
        public int                      Zeilenfarbenindex       => -1;
        public int?                     Menge                   => null;


        public decimal?                 Stunden                 => Children.Where(p => p.IstSichtbar).Sum(p => p.Stunden);
        public decimal?                 Kosten                  => Children.Where(p => p.IstSichtbar).Sum(p => p.Kosten);
        public decimal?                 Budget48                => default;
        public decimal?                 Budget52                => default;


        public bool    ImExportSichtbar(PlanOptions opts)               => Children.Any(p => p.ImExportSichtbar(opts));
        public decimal ExportierteGeplanteStunden(PlanOptions opts)     => Children.Sum(p => p.ExportierteGeplanteStunden(opts));
        public decimal ExportierteVerbrauchteStunden(PlanOptions opts)  => Children.Sum(p => p.ExportierteVerbrauchteStunden(opts));
        public decimal ExportierteGeplanteKosten(PlanOptions opts)      => Children.Sum(p => p.ExportierteGeplanteKosten(opts));
        public decimal ExportierteVerbrauchteKosten(PlanOptions opts)   => Children.Sum(p => p.ExportierteVerbrauchteKosten(opts));



        public String Info => $"Asset {Nummer} {Name}";


        public List<IHSpaltensumme> Summen      { get; }
        public List<Planauftrag>    Aufträge    { get; private set; }

        public List<Planauftrag>    OffeneAufträge  => Aufträge.Where(p => p.Auftragstatus == Auftragstatus.Offen &&
                                                                           p.Positionen.Any(q => q.Ergebnis == Checklistergebnis.Undefiniert))
                                                               .ToList();

        private List<MNPlanzeile> children;
        public List<MNPlanzeile> Children
        {
            get => children ?? (children = new List<MNPlanzeile>());
            set => children = value;
        }


        public IEnumerable<Planaufgabe> GebePositionen(int? mincol = null, int? maxcol = null)
            => Children.SelectMany(p => p.GebePositionen(mincol, maxcol));


        public void SetzeAufträge()
        {

            if (Children == null) return;
            Aufträge = Children.SelectMany(p => p.Perioden.SelectMany(q => q.Aufgaben))
                               .GroupBy(p => p.AuftragId)
                               .Select(p => new Planauftrag(p))
                               .OrderBy(p => p.Termin)
                               .ToList();
        }


        public void Accept(IVisitor v)
        {

            v.Visit(this);
            Children.ForEach(p => p.Accept(v));
            Summen.ForEach(p => p.Accept(v));
            v.VisitAfter(this);
        }


        public bool Matches(String searchTerm)
            => Nummer.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase) ||
               Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase);
    }
}
