﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Dtos.IHObjekte;

namespace Comain.Client.Models.Emma
{

    public class EmmaSession
    {

        public IList<PersonalFilterModel>    Personalliste    { get; private set; }
        public IList<AuftragListDto>         Aufträge         { get; private set; }
        public IList<PzFilterModel>          Standorte        { get; private set; }
        public IList<KstFilterModel>         Kostenstellen    { get; private set; }
        public IList<IHListDto>              IHObjekte        { get; private set; }


        public EmmaSession(IEnumerable<PersonalFilterModel> pslist, IEnumerable<AuftragListDto> aulist, IEnumerable<PzFilterModel> pzlist,
                           IEnumerable<KstFilterModel> kslist, IEnumerable<IHListDto> ihlist)
        {

            Personalliste = pslist.ToList();
            Aufträge = aulist.ToList();
            Standorte = pzlist.ToList();
            Kostenstellen = kslist.ToList();
            IHObjekte = ihlist.ToList();
        }
    }
}
