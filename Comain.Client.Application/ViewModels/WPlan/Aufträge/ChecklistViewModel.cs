﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain;
using Comain.Client.Models;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan;
using Comain.Client.Views.WPlan.Aufträge;


namespace Comain.Client.ViewModels.WPlan.Aufträge
{

    public class ChecklistViewModel : ViewModel<IChecklistView>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public ChecklistViewModel(IChecklistView view)
          : base(view)
        {

            context = SynchronizationContext.Current;
        }


        public void Unload()
        {

            Liste = null;
            Items = null;
        }


        public void SetzePositionenliste(IList<Checklistposition> models)
        {

            if (models == null) {

                Unload();
                return;
            }

            Items = models.Select(p => new ChecklistViewItem(p)).ToList();
            RaisePropertyChanged(nameof(Items));
            RefreshListe();
        }


        public void RefreshListe()
        {

            if (Items == null) return;
            if (erledigteAusblenden) Liste = Items.Where(p => !p.Model.InVorigerSitzungErledigt).ToList();
            else Liste = Items;
            RaisePropertyChanged(nameof(Liste));
        }


        public IList<ChecklistViewItem> Items  { get; private set; }


#region P R O P E R T I E S


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { if (SetProperty(ref editEnabled, value) && Items != null) Items.Where(p => p.NichtDurchgeführt).ForEach(p => { p.ChecklisteBearbeitbar = value; }); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool erledigteAusblenden;
        public bool ErledigteAusblenden
        {
            get { return erledigteAusblenden; }
            set { if (SetProperty(ref erledigteAusblenden, value)) RefreshListe(); }
        }


        public IList<ChecklistViewItem> Liste { get; private set; }


        private ChecklistViewItem current;
        public ChecklistViewItem Current
        {
            get { return current; }
            set { SetProperty(ref current, value); }
        }


        private ICommand detailCommand;
        public ICommand DetailCommand
        {
            get { return detailCommand; }
            set { SetProperty(ref detailCommand, value); }
        }


        private ICommand editPersonalCommand;
        public ICommand EditPersonalCommand
        {
            get { return editPersonalCommand; }
            set { SetProperty(ref editPersonalCommand, value); }
        }


#endregion

    }
}
