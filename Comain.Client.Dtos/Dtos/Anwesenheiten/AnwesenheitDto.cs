﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Trackers;

namespace Comain.Client.Dtos.Anwesenheiten
{

    public class AnwesenheitDto : ChangeTrackingBase
    {

        public DateTime             Datum           { get; set; }
        public TimeSpan             UhrzeitVon      { get; set; }
        public TimeSpan             UhrzeitBis      { get; set; }
        public decimal?             Pause           { get; set; }
        public String               Tätigkeit       { get; set; }
        public String               Bemerkung       { get; set; }

        public int                  Asset_Id        { get; set; }
        public PersonalFilterModel  Personal        { get; set; }

        public ICollection<AnwesenheitMassnahmeDto>     Massnahmen           { get; set; }
   }
}
