﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.WPlan;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewData.WPlan
{

    public class AuftragViewItem : NotifyBase
    {
     
        public WartungListDto   Dto     { get; }

        public int              Id               => Dto.Id;
        public Auftragstatus    Status           => (Auftragstatus)Dto.Status;
        public String           Nummer           => Dto.Nummer;
        public String           Kurzbeschreibung => Dto.Kurzbeschreibung;
        public DateTime?        Termin           => Dto.Termin;
        public DateTime         ErstelltAm       => Dto.ErstelltAm;
        public String           IHObjekt         => Dto.IHObjekt;
        public String           Baugruppe        => Dto.Baugruppe;
        public String           Gewerk           => Dto.Gewerk;
        public String           Dringlichkeit    => Dto.Dringlichkeit;
        public String           Kostenstelle     => Dto.Kostenstelle;
        public String           Leistungsart     => Dto.Leistungsart;
        public bool             HatErledigte     => Dto.HatErledigte;
        public bool             HatOffene        => Dto.HatOffene;
        public int?             Auftraggeber     => Dto.Auftraggeber;
        public int?             Auftragsleiter   => Dto.Auftragsleiter;
        public int?             Auftragnehmer_Id => Dto.Auftragnehmer == null? (int?)null: Dto.Auftragnehmer.Id;


        public String StatusAsString
        {
            get {

                if (Status == Auftragstatus.Abgeschlossen) return "Abgeschlossen";
                if (Status == Auftragstatus.Storno) return "Storniert";

                if (!HatOffene) return "Beendet";
                if (!HatErledigte) return "Nicht begonnen";
                return "Begonnen";
            }
        }


        public String ColorAsString
        {
            get {

                if (Status == Auftragstatus.Abgeschlossen) return "LightSlateGray";
                if (Status == Auftragstatus.Storno) return "LightSlateGray";

                if (!HatOffene) return "LimeGreen";
                if (!HatErledigte) return "Coral";
                return "Gold";
            }
        }


        public bool AuswahlSichtbar => Status == Auftragstatus.Offen || Status == Auftragstatus.Überfällig;


        public String PersonalAsString => Auftragnehmer?.Name;


        private bool istGewählt;
        public bool IstGewählt
        {
            get { return istGewählt; }
            set { SetProperty(ref istGewählt, value); }
        }


        public PersonalFilterModel Auftragnehmer
        {
            get => Dto.Auftragnehmer;
            set {

                if (Dto.Auftragnehmer != value) {

                    Dto.Auftragnehmer = value;
                    RaisePropertyChanged(nameof(Auftragnehmer));
                    RaisePropertyChanged(nameof(PersonalAsString));
                }
            }
        }


        public AuftragViewItem(WartungListDto dto)
        {
            Dto = dto;
        }
    }
}
