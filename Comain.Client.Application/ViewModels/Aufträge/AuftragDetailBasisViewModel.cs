﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using Comain.Client.Dtos.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;
using Comain.Client.Services;
using Comain.Client.ViewModels.IHObjekte;
using Comain.Client.Views.Aufträge;


namespace Comain.Client.ViewModels.Aufträge
{

    public class AuftragDetailBasisViewModel : ValidatableViewModel<IAuftragDetailBasisView, Auftrag>
    {

        private readonly SynchronizationContext context;
        private readonly ILoginService login;
        private readonly IHSelektorViewModel ihFilter;
        private IEnumerable<AuIHKSDto> ihkslist;


        public AuftragDetailBasisViewModel(IHSelektorViewModel ihFilter, IAuftragDetailBasisView view, ILoginService login) : base(view)
        {

            this.login = login;
            this.ihFilter = ihFilter;
            ihFilter.Validator = () => Current != null && Current.AuIHKS == null;
            ihFilter.ClearButtonVisible = false;
            context = SynchronizationContext.Current;
            PropertyChangedEventManager.AddHandler(ihFilter, ViewPropertyChanged, "CurrentIHObjekt");
        }


        public async Task SetAuIHKSListAsync(IEnumerable<AuIHKSDto> ihkslist, IList<PzFilterModel> pzlist, IList<KstFilterModel> kstlist,
                                             CancellationToken ct = default)
        {

            this.ihkslist = ihkslist;
            await ihFilter.LoadAsync(ihkslist.Select(p => new IHFilterItem(p)).ToList(), pzlist, kstlist, ct);

            await context.SendAsync(new SendOrPostCallback((o) => {

                IHObjekte = ihkslist.OrderBy(p => p.IHObjekt).ToList();

                RaisePropertyChanged(nameof(IHObjekte));
                RaisePropertyChanged(nameof(AuIHKS));

            }), null);
        }


        public override void CurrentChanged(Auftrag oldSelected)
        {

            RaisePropertyChanged(nameof(ErstelltAm));
            RaisePropertyChanged(nameof(GeändertAm));
            RaisePropertyChanged(nameof(Titel));
            RaisePropertyChanged(nameof(Nummer));
            RaisePropertyChanged(nameof(current));
            RaisePropertyChanged(nameof(Status));
            RaisePropertyChanged(nameof(AuIHKS));
            RaisePropertyChanged(nameof(Maschine));
            RaisePropertyChanged(nameof(Baugruppe));
            RaisePropertyChanged(nameof(Kostenstelle));
            RaisePropertyChanged(nameof(Standort));
            RaisePropertyChanged(nameof(ImFremdbudget));
            RaisePropertyChanged(nameof(FBNummer));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(GemeldetDatum));
            RaisePropertyChanged(nameof(GemeldetZeit));
            RaisePropertyChanged(nameof(ÜbergabeDatum));
            RaisePropertyChanged(nameof(ÜbergabeZeit));
            RaisePropertyChanged(nameof(Störzeit));
            RaisePropertyChanged(nameof(BerechneteStörzeit));
            RaisePropertyChanged(nameof(Leistungsart_Id));
            RaisePropertyChanged(nameof(Leistungsart));
            RaisePropertyChanged(nameof(Schadensbild));
            RaisePropertyChanged(nameof(Schadensursache));
            RaisePropertyChanged(nameof(SubSchadensbild));
            RaisePropertyChanged(nameof(SubSchadensursache));
            RaisePropertyChanged(nameof(IHObjektnummer));
            RaisePropertyChanged(nameof(Inventarnummer));
            RaisePropertyChanged(nameof(AuIHKS));
            RaisePropertyChanged(nameof(MeldungAm));
            RaisePropertyChanged(nameof(Dringlichkeit));
            RaisePropertyChanged(nameof(Fertigstellungstermin));

            ihFilter.SetModel(Current?.AuIHKS?.Id);

            RefreshSubSchadensbilder();
            RefreshSubSchadensursachen();
            RefreshEnabledState();
        }


        public void RefreshEnabledState()
        {

            RaisePropertyChanged(nameof(SbSuVisible));
            RaisePropertyChanged(nameof(ErstelltAmEnabled));
            RaisePropertyChanged(nameof(FbVisible));
            RaisePropertyChanged(nameof(FbImBudgetVisible));
            RaisePropertyChanged(nameof(AuIHKSEnabled));
            RaisePropertyChanged(nameof(LaEnabled));
            RaisePropertyChanged(nameof(ZeitVisible));
            RaisePropertyChanged(nameof(DringlichkeitEnabled));
            RaisePropertyChanged(nameof(FälligkeitEnabled));
        }


        public async Task SetzeListenAsync(IEnumerable<Leistungsart> leistungsarten,
                                           IEnumerable<NummerFilterModel> schadensbilder, IEnumerable<NummerFilterModel> schadensursachen,
                                           IEnumerable<SubNummerFilterModel> subSchadensbilder, IEnumerable<SubNummerFilterModel> subSchadensursachen)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                Leistungsarten = leistungsarten;
                Schadensursachen = schadensursachen;
                Schadensbilder = schadensbilder;
                SubSchadensbilder = new CollectionViewSource { Source = subSchadensbilder }.View;
                SubSchadensbilder.Filter = SubSchadensbildPredicate;
                SubSchadensbilder.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });
                SubSchadensursachen = new CollectionViewSource { Source = subSchadensursachen }.View;
                SubSchadensursachen.Filter = SubSchadensursachePredicate;
                SubSchadensursachen.SortDescriptions.Add(new SortDescription { PropertyName="Nummer" });

                SubSchadensbilder?.Refresh();
                SubSchadensursachen?.Refresh();

                RaisePropertyChanged(nameof(Leistungsarten));
                RaisePropertyChanged(nameof(Schadensursachen));
                RaisePropertyChanged(nameof(Schadensbilder));
                RaisePropertyChanged(nameof(SubSchadensbilder));
                RaisePropertyChanged(nameof(SubSchadensursachen));

            }), null);
        }


        private bool SubSchadensbildPredicate(object arg)
        {

            var ssb = arg as SubNummerFilterModel;
            if (ssb == null) return false;

            if (Schadensbild == null) return false;
            return ssb.Haupt_Id == Schadensbild.Id;
        }


        private bool SubSchadensursachePredicate(object arg)
        {

            var ssu = arg as SubNummerFilterModel;
            if (ssu == null) return false;

            if (Schadensursache == null) return false;
            return ssu.Haupt_Id == Schadensursache.Id;
        }


        private void RefreshSubSchadensbilder()
        {

            if (Schadensbilder == null || SubSchadensbilder == null) return;
            context.Post(new SendOrPostCallback((o) => {

                SubSchadensbilder?.Refresh();
                RaisePropertyChanged("SubSchadensbilder");
            }), null);
        }


        private void RefreshSubSchadensursachen()
        {

            if (Schadensursachen == null || SubSchadensursachen == null) return;
            context.Post(new SendOrPostCallback((o) => {

                SubSchadensursachen?.Refresh();
                RaisePropertyChanged("SubSchadensursachen");
            }), null);
        }


        public void Unload()
        {

           Leistungsarten = null;
           Schadensursachen = null;
           Schadensbilder = null;
           SubSchadensbilder = null;
           SubSchadensursachen = null;
           IHObjekte = null;
           Dringlichkeiten = null;
           ihFilter.Unload();
        }


        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (ihFilter.CurrentIHObjekt == null) return;
            if (ihkslist.IsNullOrEmpty()) return;
            AuIHKS = ihkslist.FirstOrDefault(p => p.Id == ihFilter.CurrentIHObjekt.Id);
        }


        public Konfiguration Konfiguration   { get; set; }


#region P R O P E R T I E S


        public DateTime? Fertigstellungstermin
        {
            get { return current?.Fertigstellungstermin; }
            set {

                if (current != null && current.Fertigstellungstermin != value) {

                    current.Fertigstellungstermin = value;
                    RaisePropertyChanged(nameof(Fertigstellungstermin));
                }
            }
        }


        public AuIHKSDto AuIHKS
        {
            get { return current == null? null: current.AuIHKS; }
            set {

                if (current != null && current.AuIHKS != value) {

                    current.AuIHKS = value;
                    RaisePropertyChanged(nameof(AuIHKS));
                    RaisePropertyChanged(nameof(Standort));
                    RaisePropertyChanged(nameof(Maschine));
                    RaisePropertyChanged(nameof(Baugruppe));
                    RaisePropertyChanged(nameof(Kostenstelle));
                    RaisePropertyChanged(nameof(Standort));
                }
            }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { if (SetProperty(ref editEnabled, value)) RefreshEnabledState(); }
        }


        public bool ErstelltAmEnabled => Current != null && Current.IstAngefügt;


        public bool FälligkeitEnabled => EditEnabled && Current != null && !Current.UnveränderlicherTermin;


        public bool SbSuVisible
        {

            get {

                if (Current == null) return false;
                if (Current.Leistungsart == null) return false;
                if (!Current.Leistungsart.IstPlankosten && !Current.Leistungsart.IstUnPlankosten) return false;
                return true;
            }
        }


        public bool FbVisible
        {

            get {

                if (Current == null) return false;
                return !istKunde;
            }
        }


        public bool FbImBudgetVisible
        {
            get { return FbVisible && !String.IsNullOrWhiteSpace(FBNummer); }
        }


        public bool AuIHKSEnabled
        {

            get {

                if (Current == null) return false;
                if (!login.AktuellerNutzer.IstAdministrator && !login.AktuellerNutzer.IstInstandhaltungLeitung) return false;
                return EditEnabled;
            }
        }


        public bool LaEnabled
        {

            get {

                if (Current == null) return false;
                if (Current.IstAngefügt) return true;
                if (!login.AktuellerNutzer.IstAdministrator && !login.AktuellerNutzer.IstInstandhaltungLeitung) return false;
                return EditEnabled;
            }
        }


        public bool DringlichkeitEnabled
        {

            get {

                if (Current == null) return false;
                if (Current.IstAngefügt) return EditEnabled;
                if (login.AktuellerNutzer.IstAdministrator) return EditEnabled;
                if (login.AktuellerNutzer.IstInstandhaltungLeitung) return EditEnabled;

                return false;
            }
        }


        public bool ZeitVisible  => Current != null;


        private bool istKunde;
        public bool IstKunde
        {
            get { return istKunde; }
            set { SetProperty(ref istKunde, value); }
        }


        private IList<Dringlichkeit> dringlichkeiten;
        public IList<Dringlichkeit> Dringlichkeiten
        {
            get { return dringlichkeiten; }
            set { SetProperty(ref dringlichkeiten, value); }
        }


        public Dringlichkeit Dringlichkeit
        {
            get { return current?.Dringlichkeit; }
            set {

                if (current != null && current.Dringlichkeit != value) {

                    current.Dringlichkeit = value;
                    RaisePropertyChanged(nameof(Dringlichkeit));
                    if (value != null && value.Leistungsart.HasValue && value.Leistungsart != Leistungsart?.Id && Current.IstAngefügt)
                        Leistungsart = Leistungsarten.FirstOrDefault(p => p.Id == value.Leistungsart.Value);
                    if (value != null && value.Tagesfrist.HasValue && value.Tagesfrist.Value >= 0 && Current.IstAngefügt)
                        Fertigstellungstermin = DateTime.Today.AddDays(value.Tagesfrist.Value);
                }
            }
        }


        public IEnumerable<Leistungsart> Leistungsarten         { get; private set; }
        public IEnumerable<NummerFilterModel> Schadensbilder    { get; private set; }
        public IEnumerable<NummerFilterModel> Schadensursachen  { get; private set; }
        public ICollectionView SubSchadensbilder                { get; private set; }
        public ICollectionView SubSchadensursachen              { get; private set; }
        public IEnumerable<AuIHKSDto> IHObjekte                 { get; private set; }


        public NummerFilterModel Schadensbild
        {
            get { return current?.Schadensbild; }
            set {

                if (current != null && current.Schadensbild != value) {

                    current.Schadensbild = value;
                    RaisePropertyChanged("Schadensbild");
                    RefreshSubSchadensbilder();
                    SubSchadensbild = null;
                }
            }
        }


        public NummerFilterModel Schadensursache
        {
            get { return current?.Schadensursache; }
            set {

                if (current != null && current.Schadensursache != value) {

                    current.Schadensursache = value;
                    RaisePropertyChanged("Schadensursache");
                    RefreshSubSchadensursachen();
                    SubSchadensursache = null;
                }
            }
        }


        public SubNummerFilterModel SubSchadensbild
        {
            get { return current?.SubSchadensbild; }
            set {

                if (current != null && current.SubSchadensbild != value) {

                    current.SubSchadensbild = value;
                    RaisePropertyChanged("SubSchadensbild");
                }
            }
        }


        public SubNummerFilterModel SubSchadensursache
        {
            get { return current?.SubSchadensursache; }
            set {

                if (current != null && current.SubSchadensursache != value) {

                    current.SubSchadensursache = value;
                    RaisePropertyChanged("SubSchadensursache");
                }
            }
        }


        public Leistungsart Leistungsart
        {
            get { return current?.Leistungsart; }
            set {

                if (current != null && current.Leistungsart != value) {

                    current.Leistungsart = value;
                    if (value != null) current.Leistungsart_Id = value.Id;
                    RaisePropertyChanged("Leistungsart_Id");
                    RaisePropertyChanged("Leistungsart");
                    RaisePropertyChanged("SbSuVisible");
                }
            }
        }


        public int? Leistungsart_Id
        {
            get { return current?.Leistungsart_Id; }
            set {

                if (current != null && value.HasValue && current.Leistungsart_Id != value) {

                    current.Leistungsart_Id = value.Value;
                    RaisePropertyChanged("Leistungsart_Id");
                    RaisePropertyChanged("Leistungsart");
                    RaisePropertyChanged("SbSuVisible");
                }
            }
        }


        public int? BerechneteStörzeit
        {
            get { return current == null? (int?)null: current.BerechneteStörzeit; }
        }


        public int? Störzeit
        {
            get { return current == null? (int?)null: current.Störzeit; }
            set {

                if (current != null && current.Störzeit != value) {

                    current.Störzeit = value;
                    RaisePropertyChanged("Störzeit");
                }
            }
        }


        public String ÜbergabeZeit
        {
            get { return current == null || !current.ÜbergabeZeit.HasValue ? null: current.ÜbergabeZeit.Value.ToString("hh\\:mm"); }
            set {

                if (current != null) {

                    var time = value.ParseTime();
                    if (current.ÜbergabeZeit != time) {

                        current.ÜbergabeZeit = time;
                        RaisePropertyChanged("ÜbergabeZeit");
                        RaisePropertyChanged("BerechneteStörzeit");
                    }
                }
            }
        }


        public DateTime? ÜbergabeDatum
        {
            get { return current == null? (DateTime?)null: current.ÜbergabeDatum; }
            set {

                if (current != null && current.ÜbergabeDatum != value) {

                    current.ÜbergabeDatum = value;
                    RaisePropertyChanged("ÜbergabeDatum");
                    RaisePropertyChanged("BerechneteStörzeit");
                }
            }
        }


        public String GemeldetZeit
        {
            get { return current == null || !current.GemeldetZeit.HasValue ? null: current.GemeldetZeit.Value.ToString("hh\\:mm"); }
            set {

                if (current != null) {

                    var time = value.ParseTime();
                    if (current.GemeldetZeit != time) {

                        current.GemeldetZeit = time;
                        RaisePropertyChanged("GemeldetZeit");
                        RaisePropertyChanged("BerechneteStörzeit");
                    }
                }
            }
        }


        public DateTime? GemeldetDatum
        {
            get { return current == null? (DateTime?)null: current.GemeldetDatum; }
            set {

                if (current != null && current.GemeldetDatum != value) {

                    current.GemeldetDatum = value;
                    RaisePropertyChanged("GemeldetDatum");
                    RaisePropertyChanged("BerechneteStörzeit");
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return current == null? null: current.Kurzbeschreibung; }
            set {

                if (current != null && current.Kurzbeschreibung != value) {

                    current.Kurzbeschreibung = value;
                    RaisePropertyChanged("Kurzbeschreibung");
                }
            }
        }


        public bool? ImFremdbudget
        {
            get { return current == null? false: current.ImFremdbudget; }
            set {

                if (current != null && current.ImFremdbudget != value) {

                    current.ImFremdbudget = value;
                    RaisePropertyChanged("FBImBudget");
                    RaisePropertyChanged("FbImBudgetVisible");
                }
            }
        }


        public String FBNummer
        {
            get { return current == null? null: current.FBNummer; }
            set {

                if (current != null && current.FBNummer != value) {

                    current.FBNummer = value;
                    RaisePropertyChanged("FBNummer");
                    RaisePropertyChanged("FbImBudgetVisible");
                }
            }
        }


        public String IHObjektnummer
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.IHObjekt; }
        }


        public String Inventarnummer
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.Inventarnummer; }
        }


        public String Kostenstelle
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.Kostenstelle; }
        }


        public String Baugruppe
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.Baugruppe; }
        }


        public String Maschine
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.Maschine; }
        }


        public String Standort
        {
            get { return current == null || current.AuIHKS == null? null: current.AuIHKS.Standort; }
        }


        public String Status
        {
            get { return current == null? null: current.Status.ToString(); }
        }


        public String Nummer
        {
            get { return current == null? null: current.Nummer; }
        }


        public String MeldungAm
        {
            get { return "Meldung am"; }
        }


        public DateTime? ErstelltAm
        {
            get { return current == null? (DateTime?)null: current.ErstelltAm; }
            set {

                if (current != null && current.ErstelltAm != value) {

                    current.ErstelltAm = value.Value;
                    RaisePropertyChanged("ErstelltAm");
                }
            }
        }


        public DateTime? GeändertAm
        {
            get { return current == null? (DateTime?)null: current.GeändertAm; }
            set {

                if (current != null && current.GeändertAm != value) {

                    current.GeändertAm = value;
                    RaisePropertyChanged("GeändertAm");
                }
            }
        }


        public String Titel
        {
            get { return current == null? null: current.Titel; }
            set {

                if (current != null && current.Titel != value) {

                    current.Titel = value;
                    RaisePropertyChanged("Titel");
                }
            }
        }


        public object IHSelektor => ihFilter.View;


#endregion

    }
}
