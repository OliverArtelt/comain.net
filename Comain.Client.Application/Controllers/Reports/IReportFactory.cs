﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Services.Reports;


namespace Comain.Client.Controllers.Reports
{
    
    public interface IReportFactory
    {
        
        Task LoadAsync(CancellationToken ct = default);
        void Unload();

        IList<IReport> AlleReports();
    }
}
