﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Comain.Client.Dtos.Anwesenheiten
{

    public class AnwesenheitReportDto
    {

        public int          Id                  { get; set; }
        public String       Personalname        { get; set; }
        public String       Personalnummer      { get; set; }
        public String       Personalklammer     { get; set; }

        public String       Standortname        { get; set; }
        public int          Standortnummer      { get; set; }
        public String       Assetname           { get; set; }
        public String       Assetnummer         { get; set; }

        public DateTime     Datum               { get; set; }
        public TimeSpan     UhrzeitVon          { get; set; }
        public TimeSpan     UhrzeitBis          { get; set; }
        public decimal?     Pause               { get; set; }
        public String       Tätigkeit           { get; set; }
    }
}
