﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comain.Client.Models.IHObjekte
{
    public enum IHMoveAuftragMode
    {
        Stays, Offene, NichtÜbergebene, Frei
    }
}
