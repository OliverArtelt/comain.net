﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.Services.Aufträge
{

    public interface IPersonalService
    {
        
        Task<IList<PersonalFilterModel>>            FilterPersonalAsync(CancellationToken ct = default);
        Task<IList<AufschlagtypFilterModel>>        FilterAufschlagtypenAsync(CancellationToken ct = default);
    
        Task<Personalleistung>                      SaveAsync(Personalleistung model, CancellationToken ct = default);
        Task                                        DeleteAsync(Personalleistung model, CancellationToken ct = default);
    }
}
