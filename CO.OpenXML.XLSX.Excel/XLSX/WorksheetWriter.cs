﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using CO.OpenXML.XLSX.ExcelStyles;


namespace CO.OpenXML.XLSX
{

    /// <summary>
    /// Eine Tabelle beschreiben
    /// </summary>
    public class WorksheetWriter
    {

        /// <summary>
        /// Sheet-Index im Workbook (ab 1 beginnend)
        /// </summary>
        private int sheetIndex;
        /// <summary>
        /// im Dokument vorhandene Dateien
        /// </summary>
        private FileList files;
        /// <summary>
        /// Liste der anzupassenden Zeilenhöhen
        /// </summary>
        private Dictionary<int, float> rowHeights;
        /// <summary>
        /// Liste aller Spaltenbreiten wenn angegeben
        /// </summary>
        private List<ColumnStyle> columnWidths;
        /// <summary>
        /// beschriebene Tabellenzellen
        /// </summary>
        private Dictionary<CellAddress, CellValue> cells;
        /// <summary>
        /// erste zu verwendende Ausgabezeile
        /// </summary>
        private int firstOutputRow;
        /// <summary>
        /// globales Wörterbuch
        /// </summary>
        private StringPoolEncoder stringDict;
        /// <summary>
        /// globales Stilverzeichnis
        /// </summary>
        private StylePool styles;
        /// <summary>
        /// von der Ausgabe ausgeschlossene Spalten
        /// </summary>
        private List<int> suppressedCols;
        /// <summary>
        /// größte verwendete (auszugebende) Zeile
        /// </summary>
        private int maxrow = 0;
        /// <summary>
        /// größte verwendete Spalte (Zeilen müssen einen verwendeten Zellenbereich angeben)
        /// </summary>
        private int maxcol = 0;


        /// <summary>
        /// Formatierobjekte um kulturunabhängige Zellinhalte lesen zu können (Dezimalpunkt statt Komma)
        /// </summary>
        CultureInfo invariantCulture = new CultureInfo("");


        /// <summary>
        /// größte verwendete (auszugebende) Zeile
        /// </summary>
        public int MaxRow { get { return maxrow + firstOutputRow - 1; } }
        /// <summary>
        /// größte verwendete Spalte (Zeilen müssen einen verwendeten Zellenbereich angeben)
        /// </summary>
        public int MaxColumn { get { return maxcol; } }


        /// <summary>
        /// einzelne Tabelle schreiben und in das Archiv einfügen
        /// </summary>
        /// <param name="sheet">Ausgangs-/Rohsheet als XML-Dokument</param>
        /// <param name="sp">zu aktualisierender Stringpool</param>
        /// <param name="styles">zu aktualisierender Stylepool</param>
        /// <param name="fstRow">erste zu verwendende Ausgabezeile</param>
        internal WorksheetWriter(int id, FileList files, StringPoolEncoder sp, StylePool styles, int fstRow)
                   : this(id, files, sp, styles, fstRow, null) {}

        /// <summary>
        /// einzelne Tabelle schreiben und in das Archiv einfügen
        /// </summary>
        /// <param name="sheet">Ausgangs-/Rohsheet als XML-Dokument</param>
        /// <param name="sp">zu aktualisierender Stringpool</param>
        /// <param name="styles">zu aktualisierender Stylepool</param>
        /// <param name="fstRow">erste zu verwendende Ausgabezeile</param>
        /// <param name="supCols">von der Ausgabe ausgeschlossene Spalten</param>
        internal WorksheetWriter(int id, FileList files, StringPoolEncoder sp, StylePool styles, int fstRow, List<int> supCols)
        {

            if (fstRow < 1 || fstRow > 1048576) throw new IndexOutOfRangeException("Unzulässige Excel-Zeilennummer");

            firstOutputRow = fstRow;
            stringDict = sp;
            this.styles = styles;
            suppressedCols = supCols;
            cells = new Dictionary<CellAddress,CellValue>();
            rowHeights = new Dictionary<int,float>();
            this.files = files;
            sheetIndex = id;
        }

        /// <summary>
        /// Individuelle Zeilenhöhe eintragen
        /// </summary>
        /// <param name="row"></param>
        /// <param name="height"></param>
        public void SetRowHeight(int row, float height)
        {
            rowHeights[row] = height;
        }

        /// <summary>
        /// Spaltenbreiten ersetzen, es werden alle Spalten durch diese Liste ersetzt
        /// </summary>
        /// <param name="list">neue Liste</param>
        public void SetColumnWidth(IEnumerable<ColumnStyle> list)
        {

            columnWidths = list.ToList();
            columnWidths.Sort();
        }

        /// <summary>
        /// Tabellenbezeichnung im Tabellenregister unten eintragen
        /// </summary>
        /// <param name="name">Neuer Name</param>
        public void SetName(String name)
        {

            using (TextReader rdr = new StringReader(files.GetString("xl/workbook.xml"))) {

                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                XElement sheet = doc.Descendants(ns + "sheets")
                                    .Descendants(ns + "sheet")
                                    .Where(p => p.Attribute("sheetId").Value == sheetIndex.ToString())
                                    .First();
                sheet.Attribute("name").Value = name.Length <= 31? name: name.Substring(0, 31);

                String content = String.Format("{0}{1}{2}", doc.Declaration, Environment.NewLine,
                                               doc.ToString(SaveOptions.None));
                files.SetString("xl/workbook.xml", content);
            }
        }

        /// <summary>
        /// Tabelle in das Verzeichnis zurückschreiben
        /// </summary>
        public void Save(CancellationToken ct = default)
        {
            files.SetString(Filename, WriteSheet(ct));
        }


#region C E L L   W R I T I N G


        public readonly Style Default = new Style();


        public void SetValue(String val, int row, int col,    Style style = null) => SetValue(val, new CellAddress(row, col), style);
        public void SetValue(String val, String address,      Style style = null) => SetValue(val, new CellAddress(address), style);
        public void SetValue(Decimal val, int row, int col,   Style style = null) => SetValue(val, new CellAddress(row, col), style);
        public void SetValue(Decimal val, String address,     Style style = null) => SetValue(val, new CellAddress(address), style);
        public void SetValue(DateTime val, int row, int col,  Style style = null) => SetValue(val, new CellAddress(row, col), style);
        public void SetValue(DateTime val, String address,    Style style = null) => SetValue(val, new CellAddress(address), style);
        public void SetValue(Decimal? val, int row, int col,  Style style = null) => SetValue(val, new CellAddress(row, col), style);
        public void SetValue(Decimal? val, String address,    Style style = null) => SetValue(val, new CellAddress(address), style);
        public void SetValue(DateTime? val, int row, int col, Style style = null) => SetValue(val, new CellAddress(row, col), style);
        public void SetValue(DateTime? val, String address,   Style style = null) => SetValue(val, new CellAddress(address), style);

        public void SetEmpty(int row, int col,      Style style) => SetValue(String.Empty, new CellAddress(row, col), style);
        public void SetEmpty(String address,        Style style) => SetValue(String.Empty, new CellAddress(address), style);
        public void SetEmpty(CellAddress address,   Style style) => SetValue(String.Empty, address, style);


        public void SetValue(String val, CellAddress cell, Style style = null)
        {

            SetMaximum(cell);
            String strval = String.IsNullOrEmpty(val)? null: stringDict[val].ToString();
            cells[cell] = new CellValue { Value = strval, IsPooled = true, StyleIndex = styles[style ?? Style.Default] };
        }


        public void SetValue(Decimal val, CellAddress cell, Style style = null)
        {

            SetMaximum(cell);
            cells[cell] = new CellValue { Value = val.ToString(invariantCulture), IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
        }


        public void SetValue(DateTime val, CellAddress cell, Style style = null)
        {

            SetMaximum(cell);
            double v = val.ToOADate();
            cells[cell] = new CellValue { Value = v.ToString(), IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
        }


        public void SetValue(Decimal? val, CellAddress cell, Style style = null)
        {

            SetMaximum(cell);
            if (val.HasValue) cells[cell] = new CellValue { Value = val.Value.ToString(invariantCulture), IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
            else              cells[cell] = new CellValue { Value = null, IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
        }


        public void SetValue(DateTime? val, CellAddress cell, Style style = null)
        {

            SetMaximum(cell);
            if (val.HasValue) cells[cell] = new CellValue { Value = val.Value.ToOADate().ToString(), IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
            else              cells[cell] = new CellValue { Value = null, IsPooled = false, StyleIndex = styles[style ?? Style.Default] };
        }


        private void SetMaximum(CellAddress addr)
        {

            if (addr.Column > maxcol) maxcol = addr.Column;
            if (addr.Row > maxrow) maxrow = addr.Row;
        }


#endregion


        /// <summary>
        /// Pfad dieser Tabelle im Dateiverzeichnis geben
        /// </summary>
        private String Filename { get { return String.Format("xl/worksheets/sheet{0}.xml", sheetIndex); } }

        /// <summary>
        /// Exceltabelle als XML-Dokument geben
        /// </summary>
        /// <returns></returns>
        private string WriteSheet(CancellationToken ct)
        {

            using (TextReader rdr = new StringReader(files.GetString(Filename))) {

                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                XElement data = doc.Descendants(ns + "sheetData").First();

                //Dokumenttyp "Wörterbuch" registrieren
                XElement types = doc.Element(ns + "Types");

                //ungenutzte Spalten entfernen
                ReplaceColumns(doc);

                //alter Inhalt neu zu beschreibender Zeilen entfernen um Mißinterpretationen vorzubeugen
                var rows = (from r in data.Descendants(ns + "row")
                            where Int32.Parse(r.Attribute("r").Value) >= firstOutputRow
                            select r).ToList();
                foreach (var node in rows) node.Remove();

                //Zeilen haben eine MaxSpan-Angabe
                String spans = String.Format("1:{0}", MaxColumn);


                int currentRow = 0;
                XElement xrow = null;

                foreach (var cell in cells.Keys.OrderBy(p => p)) {

                    if (currentRow != cell.Row) {

                        ct.ThrowIfCancellationRequested();

                        xrow = new XElement(ns + "row");
                        data.Add(xrow);
                        xrow.Add(new XAttribute("r", (cell.Row + firstOutputRow - 1).ToString()));
                        xrow.Add(new XAttribute("spans", spans));
                        if (rowHeights.ContainsKey(cell.Row)) {

                            xrow.Add(new XAttribute("customHeight", "1"));
                            xrow.Add(new XAttribute("ht", rowHeights[cell.Row].ToString(invariantCulture)));
                        }

                        currentRow = cell.Row;
                    }

                    XElement xcell = new XElement(ns + "c");
                    xrow.Add(xcell);

                    //Zelladressen anpassen/verschieben wenn Ausgabezeile > 1
                    CellAddress outCell = (firstOutputRow == 1)? cell: new CellAddress(cell.Row + firstOutputRow - 1, cell.Column);

                    xcell.Add(new XAttribute("r", outCell));
                    CellValue val = cells[cell];
                    if (val.IsPooled) xcell.Add(new XAttribute("t", "s"));
                    if (val.StyleIndex > 0) xcell.Add(new XAttribute("s", val.StyleIndex));
                    if (!String.IsNullOrEmpty(val.Value)) xcell.Add(new XElement(ns + "v", val.Value));
                }

                String content = String.Format("{0}{1}{2}", doc.Declaration, Environment.NewLine,
                                               doc.ToString(SaveOptions.DisableFormatting));
                return content;
            }
        }


        private void ReplaceColumns(XDocument doc)
        {

            if (columnWidths == null) return;

            XNamespace ns = doc.Root.Name.Namespace;
            XElement parent = doc.Descendants(ns + "cols").FirstOrDefault();
            if (parent != null) {

                parent.RemoveAll();

            } else {

                parent = new XElement(ns + "cols");
                XElement sheetFormatPr = doc.Descendants(ns + "sheetFormatPr").First();
                sheetFormatPr.AddAfterSelf(parent);
            }

            foreach (var entry in columnWidths) {

                parent.Add(
                    new XElement(ns + "col",
                        new XAttribute("min", entry.Min),
                        new XAttribute("max", entry.Max),
                        new XAttribute("width", entry.Width),
                        new XAttribute("bestFit", "1"),
                        new XAttribute("customWidth", "1")
                    )
                );
            }
        }


        private void RemoveColumns(XDocument doc)
        {

            if (suppressedCols == null) return;

            XNamespace ns = doc.Root.Name.Namespace;
            List<XElement> colGroups = (from c in doc.Descendants(ns + "cols").Descendants(ns + "col")
                                        orderby c.Attribute("min").Value descending
                                        select c).ToList();

            foreach (var removedCol in suppressedCols.OrderByDescending(p => p)) {

                RemoveColumn(removedCol, colGroups);
            }
        }


        private void RemoveColumn(int column, List<XElement> groups)
        {

            foreach (var gr in groups) {

                int max = Int32.Parse(gr.Attribute("max").Value);
                int min = Int32.Parse(gr.Attribute("min").Value);

                //dahinter => don't care
                if (max < column) return;

                //genau eine Spalte
                if (max == column && min == column) {

                    groups.Remove(gr);
                    gr.Remove();
                    return;
                }

                if (max >= column) gr.Attribute("max").Value = (max - 1).ToString();
                if (min > column)  gr.Attribute("min").Value = (min - 1).ToString();
            }
        }
    }
}
