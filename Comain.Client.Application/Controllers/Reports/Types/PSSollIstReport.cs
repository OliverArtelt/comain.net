﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class PSSollIstReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public PSSollIstReport(IReportService store, ZeitViewModel zeitView)
        {
            
            this.store = store;
            filterViews = new List<IFilterViewModel> { zeitView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.PsSollIstAsync(GebeFilter(), ct));
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Personal Soll/Ist"; }
        }


        public string Gruppe
        {
            get { return "Personal"; }
        }


        public int Sortierung
        {
            get { return 200; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.PSSollIstReport.rdlc"; }
        }
    }
}
