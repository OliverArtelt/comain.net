﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Controllers.Verwaltung.Types
{

    public class Schadensbilder : IVwTyp
    {

        public string   Title           => "Schadensbilder";
        public int      Sortierung      => 1300;
        public Type     Controller      => typeof(ISBController);


        public bool HatZugriff(Nutzer nutzer)
        {

            if (nutzer == null) return false;
            return nutzer.IstAdministrator || nutzer.IstInstandhaltungLeitung;
        }
    }
}
