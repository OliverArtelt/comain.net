﻿using System;
using System.Diagnostics;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewData.Verwaltung
{

    [DebuggerDisplay("{Nummer}")]
    public class IHSelektorItem : NotifyBase
    {

        private readonly IHObjektFilterModel dto;


        public IHSelektorItem(IHObjektFilterModel dto)
        {
            this.dto = dto;
        }


        private bool isSelected;
        public bool IsSelected 
        { 
            get { return isSelected; }
            set { SetProperty(ref isSelected, value); } 
        }


        public int          Id                  { get { return dto.Id; } }
        public String       Nummer              { get { return dto.Nummer; } }
        public String       Name                { get { return dto.Name; } }
        public int?         Status              { get { return dto.Status; } }
        public String       Inventarnummer      { get { return dto.Inventarnummer; } }
        public String       Fabriknummer        { get { return dto.Fabriknummer; } }

        public IHObjektFilterModel Model { get { return dto; } }
    }
}
