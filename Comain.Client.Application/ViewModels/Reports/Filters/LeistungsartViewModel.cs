﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Services.Reports;
using Comain.Client.Views.Reports.Filters;
using Comain.Client.Dtos.Filters;

namespace Comain.Client.ViewModels.Reports.Filters
{

    public class LeistungsartViewModel : ViewModel<ILeistungsartView>, IFilterViewModel
    {
       
        private readonly IFilterService store;


        public LeistungsartViewModel(IFilterService store, ILeistungsartView view) : base(view)
        {
        
            this.store = store;

            SetzeZyklische    = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.SelektZyklische()); });    
            SetzePlanmäßige   = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.SelektPlanmäßige()); });    
            SetzeUnplanmäßige = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.SelektUnplanmäßige()); });    
            SetzePkz          = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.SelektPkz()); });    
            SetzePräventive   = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.SelektPräventive()); }); 
            SetzeAlle         = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.IsSelected = true); }); 
            SetzeKeine        = new DelegateCommand(p => { if (Leistungsarten != null) Leistungsarten.ForEach(q => q.IsSelected = false); }); 
        }


        public void Reset()
        {
            
            if (Leistungsarten == null) return;
            Leistungsarten.ForEach(p => { p.IsSelected = false; });
        }


        public String Validiere()
        {

            if (!Leistungsarten.Any(p => p.IsSelected)) return "Wählen Sie mindestens eine Leistungsart";
            return null;
        }


        public string FilterAlsString()
        {
           
            if (!Leistungsarten.Any(p => p.IsSelected)) return null;
            return "Leistungsarten " + String.Join(", ", Leistungsarten.Where(p => p.IsSelected).Select(p => p.Nummer).ToArray());
        }


        public void AddFilter(Suchkriterien filter)
        {
           
            if (!Leistungsarten.Any(p => p.IsSelected)) return;
            var s = String.Join(",", Leistungsarten.Where(p => p.IsSelected).Select(p => p.Id.ToString()).ToArray());
            filter.Add("LA", s); 
        }


        public Task LoadAsync(CancellationToken ct = default)
        {

            Leistungsarten = store == null || store.Leistungsarten == null? null: store.Leistungsarten.OrderBy(p => p.Nummer).Select(p => new LAListViewItem(p)).ToList(); 
            return Task.FromResult(true);     
        }


        public void Unload()
        {
            Leistungsarten = null;
        }


#region P R O P E R T I E S
   

        private IList<LAListViewItem> leistungsarten;
        public IList<LAListViewItem> Leistungsarten
        { 
            get { return leistungsarten; }
            set { SetProperty(ref leistungsarten, value); }
        } 
   

        public ICommand SetzeZyklische      { get; private set; } 
        public ICommand SetzePlanmäßige     { get; private set; } 
        public ICommand SetzeUnplanmäßige   { get; private set; } 
        public ICommand SetzePkz            { get; private set; } 
        public ICommand SetzePräventive     { get; private set; } 
        public ICommand SetzeAlle           { get; private set; } 
        public ICommand SetzeKeine          { get; private set; } 
   

#endregion

    }
}
