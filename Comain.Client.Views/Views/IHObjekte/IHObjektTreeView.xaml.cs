﻿using System.Windows;
using System.Windows.Controls;
using Comain.Client.ViewModels.IHObjekte;

namespace Comain.Client.Views.IHObjekte
{

    public partial class IHObjektTreeView : UserControl, IIHObjektTreeView
    {

        public IHObjektTreeView()
        {
            InitializeComponent();
        }


        private void OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            var vm = DataContext as IHObjektTreeViewModel;
            if (vm != null) vm.SelectItem(e.NewValue);
        }
    }
}
