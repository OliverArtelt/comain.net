﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Stock.Bestellungen;
using Comain.Client.ViewModels.Stock.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{

    public class BSInfoReport : ReportBase, IReport
    {

        private readonly IBestellService store;
        private readonly BSInfoFilterViewModel infoView;


        public BSInfoReport(IBestellService store, ArtikelFilterViewModel artikelView, BSInfoFilterViewModel infoView)
        {

            this.store = store;
            this.infoView = infoView;
            filterViews = new List<IFilterViewModel> { artikelView, infoView };
        }
 

        public bool HatZugriff(ModulInfo module, Nutzer user)
        {

            if (!module.VerwendetModulStock) return false;
            return user.IstAdministrator || user.IstLagerAdministrator || user.IstLagerReporting;
        }


        public override string Title
        {
            get { return "Bestellinfo / Vorauswahl"; }
        }


        public string Gruppe
        {
            get { return "Ersatzteile"; }
        }


        public int Sortierung
        {
            get { return 2000; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.Stock.BestellInfoReport.rdlc"; }
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            result.AddDataSource("MainData", await store.BestellInfoAsync(GebeFilter(), ct));
            result.AddParameter("parSort", infoView.SortArtikelname? "Artikelname": String.Empty);
            return result;
        }
    }
}
