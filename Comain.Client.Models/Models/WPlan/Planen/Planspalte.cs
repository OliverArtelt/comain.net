﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Dtos.WPlan;


namespace Comain.Client.Models.WPlan.Planen
{

    /// <summary>
    /// Eine Periode im Plan (Hinterlegen ob Ferien, Aktuell ...)
    /// </summary>
    public class Planspalte
    {

        public Planspalte(ZeitDto dto, int index, int? wochenRückmeldung, Konfiguration cfg)
        {

            Periode         = dto.Name;
            Begin           = dto.Begin;
            BeginNext       = dto.End;
            IstFeiertag     = dto.IstFeiertag;
            IstBearbeitbar  = (!wochenRückmeldung.HasValue || Begin.MondayOf() >= DateTime.Today.MondayOf().AddDays(-7 * wochenRückmeldung.Value)) &&
                              (!cfg.Abrechnungsschluß.HasValue || cfg.Abrechnungsschluß.Value < Begin);
            Index = index;
        }


        public String     Periode           { get; }
        public String     TagShort          => Begin.ToString("dd.MM.");
        public DateTime   Begin             { get; }
        public DateTime   BeginNext         { get; }
        public bool       IstBearbeitbar    { get; }
        public bool       IstZukunft        => Begin > DateTime.Today;
        public bool       IstVergangenheit  => BeginNext  <= DateTime.Today;
        public bool       IstAktuell        => DateTime.Today >= Begin && DateTime.Today < BeginNext;
        public bool       IstFeiertag       { get; }
        public int        Index             { get; }


        public String Info => $"Periode {Periode} {Begin}";
        public override string ToString() => Periode;
        public String BackColor => !IstBearbeitbar? "Silver": IstAktuell? "Gold": IstFeiertag? "PaleTurquoise": "Transparent";
        public String ForeColor => !IstBearbeitbar? "Gray":  IstAktuell? "Black": IstFeiertag? "Black": "White";
    }
}
