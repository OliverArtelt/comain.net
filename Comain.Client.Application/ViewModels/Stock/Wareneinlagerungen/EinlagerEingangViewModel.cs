﻿using System.Collections.Generic;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;
using Comain.Client.Models.Stock;
using Comain.Client.Views.Stock.Wareneinlagerungen;


namespace Comain.Client.ViewModels.Stock.Wareneinlagerungen
{

    public class EinlagerEingangViewModel : ViewModel<IEinlagerEingangView>
    {

        private IEnumerable<Wareneingang> foundlist;


        public EinlagerEingangViewModel(IEinlagerEingangView view) : base(view)
        {
        }


        public void Unload()
        {
            FoundList = null;
        }


        /// <summary>
        /// Setze einlagerbare WE nach Laden
        /// </summary>
        /// <param name="found">WE die noch nicht eingelagert wurden</param>
        public void SetFoundList(IEnumerable<Wareneingang> found)
        {
            foundlist = found;
        }


        /// <summary>
        /// Setze noch verfügbare WE nach bereits erfolgter Einlagerung
        /// </summary>
        /// <param name="done"></param>
        public void SetAvailableList(IEnumerable<Wareneinlagerung> done)
        {

            var dic = done.Where(p => p.Wareneingang != null).ToMultiDictionary(p => p.Wareneingang.Id);
            FoundList = new ListCollectionView(foundlist.Select(we => new WEItem(we, dic.GetValueOrDefault(we.Id))).Where(p => p.Eingelagert < p.MengeOK).ToList());
            RaisePropertyChanged("FoundList");
        }


#region P R O P E R T I E S
 
        
        private bool editEnabled = true;
        public bool EditEnabled
        { 
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private ICommand takeCommand;
        public ICommand TakeCommand
        {
            get { return takeCommand; }   
            set { SetProperty(ref takeCommand, value); }
        }


        private WEItem current;
        public WEItem Current
        {
            get { return current; }   
            set { SetProperty(ref current, value); }
        }


        public ListCollectionView FoundList { get; private set; }


#endregion

    }
}
