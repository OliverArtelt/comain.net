﻿using System;
using System.ComponentModel;
using Comain.Client.Models;
using Comain.Client.Models.IHObjekte;

namespace Comain.Client.ViewModels.IHObjekte
{

    public class UpdateableTreeIHViewItem : TreeIHViewItem
    {
             
        public UpdateableTreeIHViewItem(IHFilterItem model, Konfiguration cfg) : base(model, cfg) 
        {}
          
             
        private IHObjekt model; 
        public IHObjekt Model 
        {
            get { return model; }
            set {
            
                if (value != model) {

                    if (model != null) PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    model = value;
                    if (model != null) PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                    Emboss();
                }
            }
        }      
               
        
        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case "Nummer":
                
                IHNummer = model.Nummer;
                RaisePropertyChanged("Nummer");
                break;

            case "Name":
                
                Name = model.Name;
                RaisePropertyChanged("Name");
                break;

            case "Erhaltklasse":
                
                RaisePropertyChanged("Color");
                break;

            case "Status":
                
                RaisePropertyChanged("Color");
                break;
            }
        }


        private void Emboss()
        {
            
            if (Model != null) {

                IHNummer = Model.Nummer;
                Name = Model.Name;
            
            } else {

                IHNummer = FilterModel.Nummer;
                Name = FilterModel.Name;
            }

            RaisePropertyChanged("Color");
        }


        protected override String ColorFromEk()
        {

            if (model != null) return ColorFromEk((int?)Model.Erhaltklasse);
            else return ColorFromEk(FilterModel.Erhaltklasse);
        }


        protected override String ColorFromSt()
        {

            if (Model != null) return ColorFromSt((int?)Model.Status);
            else return ColorFromSt(FilterModel.Status);
        }
    }
}
