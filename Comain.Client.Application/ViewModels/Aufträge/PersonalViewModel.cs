﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Models;
using Comain.Client.Views.Aufträge;
using Comain.Client.Dtos.Filters;
using Comain.Client.ViewModels.Verwaltung;
using System.Collections;
using Comain.Client.Gateways;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{

    public class PersonalViewModel : ValidatableViewModel<IPersonalView, Personalleistung>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;
        private readonly IPersonalView view;
        private Auftrag currentAuftrag;
        private List<int> letzteAufschläge;
        private Personalleistung letzteLeistung;
        private Dictionary<int, PersonalFilterModel> psdic;


        public PersonalViewModel(IPersonalView view, IPersonalSearchProvider searcher) : base(view)
        {

            this.view = view;
            context = SynchronizationContext.Current;
            CopyASCommand = new DelegateCommand(KopiereAufschläge);
            CopyLECommand = new DelegateCommand(KopiereLeistung);
            PsSearchProvider = searcher;
        }


        public void SetzeModel(Auftrag au, Personalleistung le)
        {

            currentAuftrag = au;
            Current = le;

            ErstelleAufschläge();
            if (!le.IstAngefügt) ErstelleKopie();
            Personal = psdic.GetValueOrDefault(le.Personal_Id);
            RaisePropertyChanged();
        }


        public void SetzeListen(IEnumerable<PersonalFilterModel> pslist, IEnumerable<Aufschlagtyp> atlist)
        {

            psdic = pslist.ToDictionary(p => p.Id);
            context.Send(new SendOrPostCallback((o) => {

                PsSearchProvider.SetItems(pslist);
                Aufschlagtypen = atlist;
            }), null);
        }


        public void SetzeStartFocus()
        {
            context.Send(new SendOrPostCallback((o) => { view.SetStartFocus(); }), null);
        }


        private void RaisePropertyChanged()
        {

            RaisePropertyChanged(nameof(Personal));
            RaisePropertyChanged(nameof(BeginnDatum));
            RaisePropertyChanged(nameof(BeginnZeit));
            RaisePropertyChanged(nameof(EndeDatum));
            RaisePropertyChanged(nameof(EndeZeit));
            RaisePropertyChanged(nameof(Minuten));
            RaisePropertyChanged(nameof(Menge));
            RaisePropertyChanged(nameof(Kurzbeschreibung));
            RaisePropertyChanged(nameof(Aufschlagsliste));
            RaisePropertyChanged(nameof(GleichungOhneAufschlag));
            RaisePropertyChanged(nameof(KostenOhneAufschlag));
            RaisePropertyChanged(nameof(Aufschlagkosten));
            RaisePropertyChanged(nameof(ProzentAufschlag));
            RaisePropertyChanged(nameof(Gesamtkosten));
            RaisePropertyChanged(nameof(HinweisAbrechnungsschluss));
            RaisePropertyChanged(nameof(EditEnabled));
        }


        private void ErstelleAufschläge()
        {

            if (Current == null || aufschlagtypen == null) {

                context.Post(new SendOrPostCallback((o) => { Aufschläge = null; }), null);
                RaisePropertyChanged("Aufschläge");
                return;
            }

            var aslist = aufschlagtypen.Select(typ => new AufschlagViewItem(typ, Current, Current.Aufschläge.FirstOrDefault(p => p.Aufschlagtyp_Id == typ.Id))).ToList();
            context.Post(new SendOrPostCallback((o) => {

                Aufschläge = new ObservableCollection<AufschlagViewItem>(aslist);
                RaisePropertyChanged("Aufschläge");
            }), null);
        }


        public void Unload()
        {

            Aufschläge      = null;
            Aufschlagtypen  = null;
            psdic = null;
            PsSearchProvider.ClearItems();
            ChecklistPositionen = null;
        }


        public void ErstelleKopie()
        {

            if (!Aufschläge.IsNullOrEmpty())
                letzteAufschläge = Aufschläge.Where(p => !p.IstStandard && p.IsSelected).Select(p => p.Typ.Id).ToList();
            if (Current != null)
                letzteLeistung = new Personalleistung { BeginnDatum = Current.BeginnDatum, BeginnZeit = Current.BeginnZeit,
                                                        EndeDatum = Current.EndeDatum, EndeZeit = Current.EndeZeit,
                                                        Personal_Id = Current.Personal_Id, Kurzbeschreibung = Current.Kurzbeschreibung };
        }


        private void KopiereAufschläge()
        {

            if (letzteAufschläge == null) return;
            if (Aufschläge == null) return;
            Aufschläge.Where(p => !p.IstStandard).ForEach(p => p.IsSelected = letzteAufschläge.Contains(p.Typ.Id));
        }


        private void KopiereLeistung()
        {

            if (letzteLeistung == null || Current == null) return;
            Current.BeginnDatum = letzteLeistung.BeginnDatum;
            Current.BeginnZeit  = letzteLeistung.BeginnZeit;
            Current.EndeDatum   = letzteLeistung.EndeDatum;
            Current.EndeZeit    = letzteLeistung.EndeZeit;
            Current.Personal_Id = letzteLeistung.Personal_Id;
            Current.Minuten     = letzteLeistung.BerechneteMinuten;
            Personal            = psdic.GetValueOrDefault(letzteLeistung.Personal_Id);
            RaisePropertyChanged();
        }


        public void ClearErrors()
        {

            ShowValidator = false;
            Errors = null;
        }


        public void SetErrors(IList<String> errors)
        {

            Errors = errors.Select(p => "• " + p).ToList();
            ShowValidator = true;
        }


        public virtual void SetErrors(Exception x)
        {

            Errors = new List<String> { "• " + x.Message };

            var vx = x as ComainServerException;
            if (vx?.ServerExplanations != null) Errors.AddRange(vx.ServerExplanations.Select(p => "• " + p));

            ShowValidator = true;
        }


#region P R O P E R T I E S


        public IPersonalSearchProvider PsSearchProvider  { get; }


        private PersonalFilterModel personal;
        public PersonalFilterModel Personal
        {
            get { return personal; }
            set {

                personal = value;
                if (value != null && Current != null && value.Id != Current.Personal_Id) Current.Personal_Id = value.Id;
                RaisePropertyChanged(nameof(Personal));
            }
        }


        private String header;
        public String Header
        {
            get { return header; }
            set { SetProperty(ref header, value); }
        }


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private bool showValidator;
        public bool ShowValidator
        {
            get { return showValidator; }
            set { SetProperty(ref showValidator, value); }
        }


        private List<String> errors;
        public List<String> Errors
        {
            get { return errors; }
            set { SetProperty(ref errors, value); }
        }


        public String Aufschlagsliste
        {
            get {

                if (Current == null) return null;
                String asliste = Current.AufschlägeAsString;
                if (String.IsNullOrWhiteSpace(asliste)) return null;
                return "aktive Aufschläge: " + asliste;
            }
        }


        public String GleichungOhneAufschlag
        {
            get {

                if (Current == null || !Current.Stunden.HasValue || !Current.Stundensatz.HasValue) return null;
                return String.Format("{0} h * {1:c} =", Math.Round(Current.Stunden.Value, 3, MidpointRounding.ToEven), Current.Stundensatz);
            }
        }


        public String KostenOhneAufschlag
        {
            get {

                if (Current == null || !Current.KostenOhneAufschlag.HasValue) return null;
                if (currentAuftrag == null || currentAuftrag.Konfiguration == null) return null;
                return Current.KostenOhneAufschlag.Value.RoundTwoDecimals((Rundungstyp)currentAuftrag.Konfiguration.Rundungstyp).ToString("c");
            }
        }


        private decimal? Aufschlag
        {
            get {

                if (Current == null || !Current.Kosten.HasValue || !Current.KostenOhneAufschlag.HasValue) return null;
                var ak = Current.Kosten.Value - Current.KostenOhneAufschlag.Value.RoundTwoDecimals((Rundungstyp)currentAuftrag.Konfiguration.Rundungstyp);
                return (Math.Abs(ak) > 0.01m)? ak: 0m;
            }
        }


        public String Aufschlagkosten  => Aufschlag?.ToString("c");


        public String ProzentAufschlag
        {
            get {

                if (Current == null || !Aufschlag.HasValue || Current.KostenOhneAufschlag.GetValueOrDefault() == 0m) return null;
                var ap = Aufschlag * 100m / Current.KostenOhneAufschlag.Value;
                return String.Format("+ {0} % =", Math.Round(ap.Value, 1));
            }
        }


        public String Gesamtkosten
        {
            get {

                if (Current == null || !Current.Kosten.HasValue) return null;
                return Current.Kosten.Value.ToString("c");
            }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        public ICommand CopyLECommand   { get; private set; }

        public ICommand CopyASCommand   { get; private set; }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


        public bool ChecklistVisible => !ChecklistPositionen.IsNullOrEmpty();


        private IEnumerable checklistPositionen;
        public IEnumerable ChecklistPositionen
        {
            get { return checklistPositionen; }
            set { if (SetProperty(ref checklistPositionen, value)) RaisePropertyChanged(nameof(ChecklistVisible)); }
        }


        public ObservableCollection<AufschlagViewItem> Aufschläge { get; private set; }


        private IEnumerable<Aufschlagtyp> aufschlagtypen;
        public IEnumerable<Aufschlagtyp> Aufschlagtypen
        {
            get { return aufschlagtypen; }
            set { SetProperty(ref aufschlagtypen, value); }
        }


        public DateTime? BeginnDatum
        {
            get { return Current == null? null: Current.BeginnDatum; }
            set {

                if (Current != null && Current.BeginnDatum != value) {

                    Current.BeginnDatum = value;
                    RaisePropertyChanged("BeginnDatum");

                    if (BeginnDatum.HasValue && EndeDatum.GetValueOrDefault() < BeginnDatum) EndeDatum = BeginnDatum;
                    SetzeMinuten();
               }
            }
        }


        public String BeginnZeit
        {
            get { return Current == null || !Current.BeginnZeit.HasValue ? null: Current.BeginnZeit.Value.ToString("hh\\:mm"); }
            set {

                if (Current != null) {

                    var time = value.ParseTime();
                    if (Current.BeginnZeit != time) {

                        Current.BeginnZeit = time;
                        RaisePropertyChanged("BeginnZeit");
                        SetzeMinuten();
                    }
                }
            }
        }


        public DateTime? EndeDatum
        {
            get { return Current == null? null: Current.EndeDatum; }
            set {

                if (Current != null && Current.EndeDatum != value) {

                    Current.EndeDatum = value;
                    RaisePropertyChanged("EndeDatum");
                    SetzeMinuten();
                }
            }
        }


        public String EndeZeit
        {
            get { return Current == null || !Current.EndeZeit.HasValue ? null: Current.EndeZeit.Value.ToString("hh\\:mm"); }
            set {

                if (Current != null) {

                    var time = value.ParseTime();
                    if (Current.EndeZeit != time) {

                        Current.EndeZeit = time;
                        RaisePropertyChanged("EndeZeit");
                        SetzeMinuten();
                    }
                }
            }
        }


        ///<summary>
        ///wenn Zeiten vollständig sind die Dauer eintragen.
        ///</summary>
        ///<remarks>
        ///Es ist auch möglich Start & Dauer einzugeben damit das System das Ende berechnet. Auf Zirkelbezüge achten.
        ///</remarks>
        private void SetzeMinuten()
        {

            if (Current == null) return;
            var bmin = Current.BerechneteMinuten;
            if (bmin.HasValue && bmin != Current.Minuten) Current.Minuten = bmin;
            RaisePropertyChanged("Minuten");
        }


        public int? Minuten
        {
            get { return Current == null? null: Current.Minuten; }
            set {

                if (Current != null && Current.Minuten != value) {

                    Current.Minuten = value;
                    RaisePropertyChanged("Minuten");

                    if (Minuten.HasValue && Current.Beginn.HasValue) {

                        DateTime ende = Current.Beginn.Value.AddMinutes(Minuten.Value);
                        if (Current.Ende != ende) {

                            EndeDatum = ende.Date;
                            EndeZeit = ende.TimeOfDay.ToString("hh':'mm");
                        }
                    }
                }
            }
        }


        public int Menge
        {
            get { return Current == null? 1: Current.Menge; }
            set {

                if (Current != null && Current.Menge != value) {

                    Current.Menge = value;
                    RaisePropertyChanged(nameof(Menge));
                }
            }
        }


        public String Kurzbeschreibung
        {
            get { return Current == null? null: Current.Kurzbeschreibung; }
            set {

                if (Current != null && Current.Kurzbeschreibung != value) {

                    Current.Kurzbeschreibung = value;
                    RaisePropertyChanged("Kurzbeschreibung");
                }
            }
        }


        public String HinweisAbrechnungsschluss
        {

            get {

                if (currentAuftrag == null || currentAuftrag.Konfiguration == null) return null;
                if (!currentAuftrag.Konfiguration.Abrechnungsschluß.HasValue) return null;
                if (!currentAuftrag.IstOffen) return null;
                return String.Format("Abrechnungsschluß: {0}", currentAuftrag.Konfiguration.Abrechnungsschluß.Value.ToShortDateString());
            }
        }


#endregion


    }
}
