﻿using System;
using System.ComponentModel;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;


namespace Comain.Client.ViewModels.Stock.Wareneingänge
{

    public class EingangListViewItem : NotifyBase
    {
       
        private String       bestellnummer     ;
        private String       lieferschein      ;
        private String       artikelnummer     ;
        private String       artikelbezeichnung;
        private decimal      menge             ;
        private String       auftrag           ;
        private String       kostenstelle      ;
        private DateTime?    bestelltAm        ;
        private DateTime?    geliefertAm       ;
        
                 
        public int LieferscheinId   { get; set; }        
        public int EingangId        { get; set; }        

        
        public String Bestellnummer        
        {           
            get { return bestellnummer; }
            set { SetProperty(ref bestellnummer, value); }
        }
        
                 
        public String Lieferschein        
        {           
            get { return lieferschein; }
            set { SetProperty(ref lieferschein, value); }
        }
        
                 
        public String Artikelnummer        
        {           
            get { return artikelnummer; }
            set { SetProperty(ref artikelnummer, value); }
        }
        
                 
        public String Artikelbezeichnung        
        {           
            get { return artikelbezeichnung; }
            set { SetProperty(ref artikelbezeichnung, value); }
        }
        
                 
        public String Auftrag        
        {           
            get { return auftrag; }
            set { SetProperty(ref auftrag, value); }
        }
        
                 
        public String Kostenstelle        
        {           
            get { return kostenstelle; }
            set { SetProperty(ref kostenstelle, value); }
        }
        
                 
        public decimal Menge        
        {           
            get { return menge; }
            set { SetProperty(ref menge, value); }
        }
        
                 
        public DateTime? BestelltAm        
        {           
            get { return bestelltAm; }
            set { SetProperty(ref bestelltAm, value); }
        }
        
                 
        public DateTime? GeliefertAm        
        {           
            get { return geliefertAm; }
            set { SetProperty(ref geliefertAm, value); }
        }
             
             
        private Wareneingang model; 
        public Wareneingang Model 
        {
            get { return model; }
            set {
            
                if (value != model) {

                    if (model != null) {
                    
                        PropertyChangedEventManager.RemoveHandler(model.Lieferschein, ViewPropertyChanged, "");
                        PropertyChangedEventManager.RemoveHandler(model, ViewPropertyChanged, "");
                    }

                    model = value;

                    if (model != null) {
                    
                        PropertyChangedEventManager.AddHandler(model, ViewPropertyChanged, "");
                        PropertyChangedEventManager.AddHandler(model.Lieferschein, ViewPropertyChanged, "");
                        Emboss();                
                    }            
                }
            }
        }      
               
        
        private void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {

            if (model == null) return;

            switch (args.PropertyName) {

            case "Menge":
            case "Liefermenge":

                Menge = model.Liefermenge;
                RaisePropertyChanged("Menge");
                break;
            }
        }


        public EingangListViewItem()
        {}


        public EingangListViewItem(Wareneingang model)
        {
         
            Model = model;
            Emboss();
        }


        public EingangListViewItem(EingangListDto dto)
        {

            LieferscheinId     = dto.LieferscheinId;    
            EingangId          = dto.EingangId;         
            Bestellnummer      = dto.Bestellnummer;     
            Lieferschein       = dto.Lieferschein;      
            Artikelnummer      = dto.Artikelnummer;     
            Artikelbezeichnung = dto.Artikelbezeichnung;
            Auftrag            = dto.Auftrag;           
            Kostenstelle       = dto.Kostenstelle;      
            Menge              = dto.Menge;             
            BestelltAm         = dto.BestelltAm;        
            GeliefertAm        = dto.GeliefertAm;      
        }

      
        public void Emboss()
        {

            LieferscheinId     = model.Lieferschein.Id;  
            EingangId          = model.Id;   
            Bestellnummer      = model.Bestellposition.Bestellung.Nummer;  
            Lieferschein       = model.Lieferschein.Nummer; 
            Artikelnummer      = model.Material.Nummer;  
            Artikelbezeichnung = model.Material.Name;       
            Auftrag            = model.Bestellposition.Bestellung.Auftrag;           
            Kostenstelle       = model.Bestellposition.Bestellung.Kostenstelle;      
            Menge              = model.Liefermenge;
            BestelltAm         = model.Bestellposition.Bestellung.DatumAusgang;
            GeliefertAm        = model.Lieferschein.Datum;
        }       
    }
}
