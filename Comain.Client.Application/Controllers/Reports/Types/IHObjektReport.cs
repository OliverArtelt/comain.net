﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Dtos.Reports;
using Comain.Client.Models;
using Comain.Client.Models.Users;
using Comain.Client.Services.Reports;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;
using Comain.Client.ViewModels.Reports.Filters;


namespace Comain.Client.Controllers.Reports
{

    public class IHObjektReport : ReportBase, IReport
    {

        private readonly IReportService store;


        public IHObjektReport(IReportService store, IHJahrViewModel ihView)
        {
            
            this.store = store;
            filterViews = new List<IFilterViewModel> { ihView };
        }


        public async Task<ReportResult> CreateAsync(CancellationToken ct = default)
        {

            var result = Create();
            IHObjektDto dto = await store.IHObjektAsync(GebeFilter(), ct).ConfigureAwait(false);
            
            result.AddDataSource("MainData", new List<IHObjektDto> { dto });
            result.AddDataSource("SbData", dto.Schadensbild);
            result.AddDataSource("SuData", dto.Schadensursache);
           
            return result;
        }


        public bool HatZugriff(ModulInfo module, Nutzer user)
        {
            return user.IstAdministrator || user.IstProduktionLeitung || user.IstInstandhaltungLeitung || user.IstInstandhaltungInstandhalter;
        }


        public override string Title
        {
            get { return "Assets"; }
        }


        public string Gruppe
        {
            get { return "Assets"; }
        }


        public int Sortierung
        {
            get { return 400; }
        }


        public override string ReportAddress
        {
            get { return "Comain.Client.Reports.IHObjektReport.rdlc"; }
        }
    }
}
