﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models.WPlan.Planen;


namespace Comain.Client.ReportModels.WPlan
{

    public class WartungsplanRow
    {

        private readonly Planaufgabe model;


        public WartungsplanRow(Planaufgabe model)
            => this.model = model;


        public String       Auftrag             => model.Nummer;
        public String       Kurzbeschreibung    => model.Kurzbeschreibung;
        public String       Name                => String.IsNullOrEmpty(model.Massnahmenummer)? model.Name: $"{model.Massnahmenummer}: {model.Name}";
        public String       Auftragnehmer       => model.Auftragnehmer;
        public DateTime?    Termin              => model.FälligAm;
        public String       IHObjekt            => model.IHObjekt;
        public String       Maschine            => model.Maschine;
        public String       Gewerk              => model.Gewerk;
        public String       Leistungsart        => model.Leistungsart;
        public String       Kostenstelle        => model.Kostenstelle;
    }
}
