﻿using System;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models.Stock;

namespace Comain.Client.ReportModels.Stock
{

    public class LieferantRow
    {
        
        public  String      Name1                  { get; private set; }
        public  String      Name2                  { get; private set; }
        public  String      Rank                   { get; private set; }
        public  decimal?    Preis                  { get; private set; }
        public  decimal?    Mindestbestellmenge    { get; private set; }
        public  int?        Bestellzeit            { get; private set; }


        public String Name { get { return String.Format("{0} {1}", Name1, Name2).Trim(); } }


        public LieferantRow(ArtikelLfMap model)
        {

            Name1               = model.Lieferant.Name1;
            Name2               = model.Lieferant.Name2;
            Rank                = model.Rank.ToString();
            Preis               = model.Preis;
            Mindestbestellmenge = model.Mindestbestellmenge;
            Bestellzeit         = model.Bestellzeit;
        }


        public LieferantRow(ArtikelLieferant model)
        {

            Name1               = model.Lieferant.Name1;
            Name2               = model.Lieferant.Name2;
            Rank                = model.Rank.ToString();
            Preis               = model.Preis;
            Mindestbestellmenge = model.Mindestbestellmenge;
            Bestellzeit         = model.Bestellzeit;
        }
    }
}
