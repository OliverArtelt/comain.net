﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Dtos.Filters;


namespace Comain.Client.ViewModels.IHObjekte
{
    
    [DebuggerDisplay("{Nummer}")]
    public class TreeKSTViewItem : TreeViewItem
    {

        public KstFilterModel       Model       { get; private set; }
        public List<TreeIHViewItem> SubItems    { get; private set; }
    
        public String Name      { get { return Model.Name; } }
        public String Nummer    { get { return Model.Nummer; } }
    
    
        public TreeKSTViewItem(KstFilterModel model)
        {
            Model = model;
        }


        public override bool IsParentOf(TreeIHViewItem dest)
        {
            return dest != null && dest.IstMaschine && dest.FilterModel.Kostenstelle_Id == Model.Id;
        }
    
    
        public void Add(TreeIHViewItem subItem)
        {
            
            if (SubItems == null) SubItems = new List<TreeIHViewItem>();
            SubItems.Add(subItem);
        }
    }
}
