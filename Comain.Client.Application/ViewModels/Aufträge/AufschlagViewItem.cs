﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewModels.Aufträge
{
    
    public class AufschlagViewItem : NotifyBase 
    {

        public AufschlagViewItem(Aufschlagtyp typ, Personalleistung leistung, Aufschlag model)
        {
            
            this.Typ = typ;
            this.Model = model;
            this.Leistung = leistung;
            if (leistung.Aufschläge == null) leistung.Aufschläge = new HashSet<Aufschlag>();
        }


        public Aufschlagtyp         Typ             { get; private set; }
        public Aufschlag            Model           { get; private set; }
        public Personalleistung     Leistung        { get; private set; }
        public String               Kürzel          { get { return Typ.NameKurz; } }
        public String               Bezeichnung     { get { return Typ.NameLang; } }
        public decimal?             Prozent         { get { return Model == null? (decimal?)null: Model.Prozent; } }
        public bool                 IstStandard     { get { return Typ.IstStandard.GetValueOrDefault(); } }
    

        public bool IsSelected
        { 
            get { return Model != null; } 
            set {
            
                if (!value) {

                    Leistung.Aufschläge.Remove(Model);
                    Model = null;
              
                } else {
    
                    Model = new Aufschlag { Aufschlagtyp_Id = Typ.Id };
                    Leistung.Aufschläge.Add(Model);        
                } 

                RaisePropertyChanged("IsSelected");
                RaisePropertyChanged("Prozent");
            } 
        }
    }
}
