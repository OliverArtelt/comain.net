﻿using AutoMapper;
using Comain.Client.Dtos.Stock;
using Comain.Client.Models;
using Comain.Client.Models.Materialien;
using Comain.Client.Models.Stock;


namespace Comain.Client.Converters.Stock
{

    public class EingangDtoConverter : ITypeConverter<EingangDto, Wareneingang>
    {

        public Wareneingang Convert(EingangDto src, Wareneingang dst, ResolutionContext context)
        {

            if (dst == null) dst = new Wareneingang();
            if (src == null) return dst;
            if (src.Id <= 0) dst.Id = src.Id;  

            dst.Id              = src.Id;            
            dst.Position        = src.Position;            
            dst.Liefermenge     = src.Liefermenge;         
            dst.MengeOK         = src.MengeOK;            
            dst.Notizen         = src.Notizen;             
            dst.MengeGeprüft    = src.MengeGeprüft;        
            dst.QualitätGeprüft = src.QualitätGeprüft;     
            
            if (src.Material != null) {
            
                dst.Material = new Artikel();
                  
                dst.Material.Id                 = src.Material.Id;            
                dst.Material.Name               = src.Material.Name;            
                dst.Material.Nummer             = src.Material.Nummer;          
                dst.Material.Beschreibung       = src.Material.Beschreibung;    
                dst.Material.Herstellernummer   = src.Material.Herstellernummer;
                dst.Material.Mischpreis         = src.Material.Mischpreis;      
                dst.Material.Klasse             = (Materialklasse?)src.Material.Klasse;          
                dst.Material.Mindestbestand     = src.Material.Mindestbestand;  
                dst.Material.Bestellmenge       = src.Material.Bestellmenge;    
                dst.Material.ReservierteMenge   = src.Material.ReservierteMenge;
                dst.Material.BestandReal        = src.Material.BestandReal;     
                dst.Material.BestandVirtuell    = src.Material.BestandVirtuell; 
            }
            
            if (src.Bestellposition != null) {
            
                dst.Bestellposition = context.Mapper.Map<Bestellposition>(src.Bestellposition);
                dst.Bestellposition.AcceptChanges();
            }
            
            if (src.Lieferschein != null) {
            
                dst.Lieferschein = new Lieferschein { Id = src.Lieferschein.Id, Datum = src.Lieferschein.Datum, Notizen = src.Lieferschein.Notizen,
                                                      Nummer = src.Lieferschein.Nummer };
                dst.Lieferschein.AcceptChanges();
            }

            dst.Einlagerungen = new TrackableCollection<Wareneinlagerung>();
            if (!src.Einlagerungen.IsNullOrEmpty()) {
            
                foreach (var wlsrc in src.Einlagerungen) {

                    var wldst = new Wareneinlagerung { Datum = wlsrc.Datum, Id = wlsrc.Id, Material = dst.Material, Menge = wlsrc.Menge, 
                                                       Preis = wlsrc.Preis, Restmenge = wlsrc.Restmenge, Wareneingang = dst };
                    if (wlsrc.Lagerort != null) wldst.Lagerort = new Lagerort { Id = wlsrc.Lagerort.Id, Fach = wlsrc.Lagerort.Fach, Halle = wlsrc.Lagerort.Halle,
                                                                                Bemerkungen = wlsrc.Lagerort.Bemerkungen, Nummer = wlsrc.Lagerort.Nummer, Regal = wlsrc.Lagerort.Regal };
                    dst.Einlagerungen.Add(wldst); 
                    wldst.AcceptChanges();  
                }
                ((TrackableCollection<Wareneinlagerung>)dst.Einlagerungen).AcceptChanges();    
            }    

            dst.Reklampositionen = new TrackableCollection<Reklamposition>();
            if (!src.Reklampositionen.IsNullOrEmpty()) {
            
                foreach (var rpsrc in src.Reklampositionen) {

                    var rpdst = new Reklamposition { Id = rpsrc.Id, Artikelbezeichnung = rpsrc.Artikelbezeichnung, ArtikelId = rpsrc.ArtikelId, Artikelnummer = rpsrc.Artikelnummer,
                                                     Bestellposition = rpsrc.Bestellposition, EingangId = rpsrc.EingangId, LfArtikelnummer = rpsrc.LfArtikelnummer, 
                                                     Menge = rpsrc.Menge, Notizen = rpsrc.Notizen, Position = rpsrc.Position, Preis = rpsrc.Preis };
                    dst.Reklampositionen.Add(rpdst);   
                    rpdst.AcceptChanges();  
                }    
                ((TrackableCollection<Reklamposition>)dst.Reklampositionen).AcceptChanges();    
            } 
            
            dst.AcceptChanges();   
            return dst;   
        }
    }
}
