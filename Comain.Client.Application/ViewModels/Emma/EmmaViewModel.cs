﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Emma;
using Comain.Client.ViewModels;

namespace Comain.Client.ViewModels.Emma
{

    public class EmmaViewModel : ViewModel<IEmmaView>, IDelayableViewModel
    {

        public enum Tabs { Basis = 0, Protokoll } 


        private readonly SynchronizationContext context;


        public EmmaViewModel(IEmmaView view) 
          : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public void ResetTabs()
        {
            CurrentTab = 0;
        }


        public void Unload()
        {
        }


#region P R O P E R T I E S


        private int currentTab;
        public int CurrentTab
        { 
            get { return currentTab; }
            set { if (SetProperty(ref currentTab, value)) RaisePropertyChanged(nameof(Tab)); }
        }


        public Tabs Tab 
        { 
            get => (Tabs)CurrentTab;
            set { if (CurrentTab != (int)value) CurrentTab = (int)value; } 
        }


        private bool editEnabled;
        public bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private object listView;
        public object ListView
        {
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object protokollView;
        public object ProtokollView
        { 
            get { return protokollView; }
            set { SetProperty(ref protokollView, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand importCommand;
        public ICommand ImportCommand
        {
            get { return importCommand; }
            set { SetProperty(ref importCommand, value); }
        }


        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get { return closeCommand; }
            set { SetProperty(ref closeCommand, value); }
        }


        private ICommand ihObjektCommand;
        public ICommand IHObjektCommand
        {
            get { return ihObjektCommand; }
            set { SetProperty(ref ihObjektCommand, value); }
        }


        private ICommand auftragCommand;
        public ICommand AuftragCommand
        {
            get { return auftragCommand; }
            set { SetProperty(ref auftragCommand, value); }
        }


        private ICommand personalCommand;
        public ICommand PersonalCommand
        {
            get { return personalCommand; }
            set { SetProperty(ref personalCommand, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private ICommand autoSaveCommand;
        public ICommand AutoSaveCommand
        {
            get { return autoSaveCommand; }
            set { SetProperty(ref autoSaveCommand, value); }
        }


        private ICommand selectAllCommand;
        public ICommand SelectAllCommand
        {
            get { return selectAllCommand; }
            set { SetProperty(ref selectAllCommand, value); }
        }


        private ICommand selectEorCommand;
        public ICommand SelectEorCommand
        {
            get { return selectEorCommand; }
            set { SetProperty(ref selectEorCommand, value); }
        }


#endregion

    }
}
