﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comain.Client.Controllers;
using Comain.Client.Models;
using Comain.Client.Models.Users;

namespace Comain.Client.Commands
{

    public interface IControllerCommand
    {

        String      Gruppe         { get; }
        int         Sortierung     { get; }
        String      Name           { get; }
        String      Parameter      { get; }
        Type        Controller     { get; }
        String      Color          { get; }
        String      Image          { get; }
        String      LiveValue      { get; }
        String      LiveText       { get; }


        bool CommandIsAvailable(Nutzer nutzer, ModulInfo module, Konfiguration cfg);
        /// <summary>
        /// Livetiles
        /// </summary>
        Task UpdateAsync(CancellationToken ct = default);
    }
}
