﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Reports;


namespace Comain.Client.ViewModels.Reports
{

    public class LagerViewModel : ViewModel<ILagerView>, IDelayableViewModel
    {

        public LagerViewModel(ILagerView view) : base(view)
        {
        }


#region P R O P E R T I E S


        private object filterView;
        public object FilterView
        {
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        {
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        {
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        {
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }


        private ICommand backCommand;
        public ICommand BackCommand
        {
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }


        private ICommand reloadCommand;
        public ICommand ReloadCommand
        {
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


#endregion

    }
}
