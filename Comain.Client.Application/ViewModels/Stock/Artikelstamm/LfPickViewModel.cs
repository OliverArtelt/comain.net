﻿using System.Collections.ObjectModel;
using System.Threading;
using System.Waf.Applications;
using Comain.Client.Views.Stock.Artikelstamm;


namespace Comain.Client.ViewModels.Stock.Artikelstamm
{

    public class LfPickViewModel : ViewModel<ILfPickView>
    {
        
        private readonly SynchronizationContext context;


        public LfPickViewModel(ILfPickView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }
 

        public void Unload()
        {
            Liste = null;
        }


#region P R O P E R T I E S


        private ObservableCollection<LfPickItem> liste;
        public ObservableCollection<LfPickItem> Liste
        { 
            get { return liste; }
            set { SetProperty(ref liste, value); }
        }

       
#endregion   

    }
}
