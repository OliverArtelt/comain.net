﻿using System;
using Comain.Client.Dtos.Filters;
using Comain.Client.Models;

namespace Comain.Client.Models.Tickets
{

    public class Ticketprotokoll : Entity 
    {


#region P R O P E R T I E S
 
 
        public DateTime         ErstelltAm      { get; set; }
        public Ticketstatus     Status          { get; set; }
        public String           Bemerkungen     { get; set; }
        public bool             IstSauber       { get; set; }

        /// <summary>
        /// Person welche den Eintrag verursacht hat (z.B: alter Instandhalter bei Schichtwechsel oder IH-Leiter der den Auftrag einem Instandhalter zuweist)
        /// </summary>
        public virtual PersonalFilterModel     Ersteller       { get; set; }
        /// <summary>
        /// Person welche mit den neuen Positionen betraut wurde (z.B: Schichtwechsel: (neuer) Instandhalter)
        /// </summary>
        public virtual PersonalFilterModel     Zielperson      { get; set; }


#endregion

    }
}
