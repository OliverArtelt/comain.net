﻿using System;
using System.Waf.Applications;
using System.Windows.Input;
using Comain.Client.Views.Stock.Bestellungen;
using Comain.Client.Views.Stock.Warenausgänge;
using Comain.Client.ViewModels;


namespace Comain.Client.ViewModels.Stock.Warenausgänge
{
    
    public class AusgangViewModel : ViewModel<IAusgangView>, IDelayableViewModel
    {
 
        public AusgangViewModel(IAusgangView view) : base(view)
        {
        }


#region P R O P E R T I E S
 

        private object filterView;
        public object FilterView
        { 
            get { return filterView; }
            set { SetProperty(ref filterView, value); }
        }


        private object listView;
        public object ListView
        { 
            get { return listView; }
            set { SetProperty(ref listView, value); }
        }


        private object detailView;
        public object DetailView
        { 
            get { return detailView; }
            set { SetProperty(ref detailView, value); }
        }


        private String mandant;
        public String Mandant
        { 
            get { return mandant; }
            set { SetProperty(ref mandant, value); }
        }
 

        private ICommand backCommand;
        public ICommand BackCommand
        { 
            get { return backCommand; }
            set { SetProperty(ref backCommand, value); }
        }
                 

        private bool showDelay;
        public bool ShowDelay
        { 
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        { 
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }
 

        private ICommand reloadCommand;
        public ICommand ReloadCommand
        { 
            get { return reloadCommand; }
            set { SetProperty(ref reloadCommand, value); }
        }
 

        private ICommand addCommand;
        public ICommand AddCommand
        { 
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }
 

        private ICommand deleteCommand;
        public ICommand DeleteCommand
        { 
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


#endregion

    }
}
