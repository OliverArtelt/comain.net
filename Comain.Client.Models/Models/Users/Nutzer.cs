﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;


namespace Comain.Client.Models.Users
{

    public class Nutzer : NotifyBase, IDataErrorInfo
    {

        private String         name;
        private String         email;
        private DateTime?      letzteAnmeldung;
        private LoginStatus    status;
        private List<String>   rollen;
        private String         name1;
        private String         name2;
        private String         password1;
        private String         password2;
        private String         beschreibung;
        private bool           useStandortFilter;
        private String         standortFilter;


#region P R O P E R T I E S


        public virtual String   Id                { get; set; }
        public virtual int?     Personal_Id       { get; set; }


        public void SetStandortFilter(IEnumerable<int> values)
            => StandortFilter = String.Join(",", values ?? Enumerable.Empty<int>());


        public HashSet<int> ParseStandortFilter()
            => standortFilter?.Split(',', ';')
                              .Where(p => Int32.TryParse(p, out _))
                              .Select(p => Int32.Parse(p))
                              .Distinct()
                              .ToHashSet() ?? new HashSet<int>();


        public virtual bool UseStandortFilter
        {
            get { return useStandortFilter; }
            set { SetProperty(ref useStandortFilter, value); }
        }


        public virtual String StandortFilter
        {
            get { return standortFilter; }
            set { SetProperty(ref standortFilter, value); }
        }


        public virtual String Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public virtual String Name1
        {
            get { return name1; }
            set { SetProperty(ref name1, value); }
        }


        public virtual String Name2
        {
            get { return name2; }
            set { SetProperty(ref name2, value); }
        }


        public virtual String Password1
        {
            get { return password1; }
            set { SetProperty(ref password1, value); }
        }


        public virtual String Password2
        {
            get { return password2; }
            set { SetProperty(ref password2, value); }
        }


        public virtual String Beschreibung
        {
            get { return beschreibung; }
            set { SetProperty(ref beschreibung, value); }
        }


        public virtual String Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }


        public virtual DateTime? LetzteAnmeldung
        {
            get { return letzteAnmeldung; }
            set { SetProperty(ref letzteAnmeldung, value); }
        }


        public virtual LoginStatus Status
        {
            get { return status; }
            set { if (SetProperty(ref status, value)) RaisePropertyChanged("StatusAsString"); }
        }


        public virtual List<String> Rollen
        {
            get {

                if (rollen == null) rollen = new List<String>();
                return rollen;
            }
            set { if (SetProperty(ref rollen, value)) RaisePropertyChanged("RolleAsString"); }
        }


#endregion


        public bool IstAngefügt => String.IsNullOrEmpty(Id);


        public static String RolleAsString(String key)
        {

            switch (key) {

                case "ADMIN"        : return "Administrator";
                case "PIE1"         : return "Instandhaltung Leitung";
                case "PIE2"         : return "Instandhaltung Instandhalter";
                case "PIE3"         : return "Instandhaltung Werker";
                case "CUST1"        : return "Produktion Leitung";
                case "CUST2"        : return "Produktion Schichtmeister";
                case "CUST3"        : return "Produktion Lagermitarbeiter";
                case "L1"           : return "Lager Administrator";
                case "L2"           : return "Lager Bestellungen";
                case "L3"           : return "Lager Einlagerungen";
                case "L4"           : return "Lager Reservierungen";
                case "L5"           : return "Lager Reporting";
                case "L6"           : return "Lager Verwaltung";
                case "Ticket1"      : return "Ticketsystem Leiter Instandhaltung";
                case "Ticket2"      : return "Ticketsystem Leiter Produktion";
                case "Ticket3"      : return "Ticketsystem Schichtführer";
                case "Ticket4"      : return "Ticketsystem Instandhalter";
                case "Bevor"        : return "Leiter Besonderes Vorkommnis";
                case "AssetDelete"  : return "Assets löschen";
                default             : return key;
            }
        }


        public bool IstProduktion                   { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p.StartsWith("CUST")); } }
        public bool IstAdministrator                { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "ADMIN"); } }
        public bool IstInstandhaltung               { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p.StartsWith("PIE")); } }
        public bool IstInstandhaltungLeitung        { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "PIE1"); } }
        public bool IstInstandhaltungInstandhalter  { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "PIE2"); } }
        public bool IstInstandhaltungWerker         { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "PIE3"); } }
        public bool IstProduktionLeitung            { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "CUST1"); } }
        public bool IstProduktionSchichtmeister     { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "CUST2"); } }
        public bool IstProduktionLagermitarbeiter   { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "CUST3"); } }
        public bool IstLager                        { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L1" || p == "L2" || p == "L3" || p == "L4" || p == "L5" || p == "L6"); } }
        public bool IstLagerAdministrator           { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L1"); } }
        public bool IstLagerBestellungen            { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L2" || p == "L1"); } }
        public bool IstLagerEinlagerungen           { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L3" || p == "L1"); } }
        public bool IstLagerReservierungen          { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L4" || p == "L1"); } }
        public bool IstLagerReporting               { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L5" || p == "L1"); } }
        public bool IstLagerVerwaltung              { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "L6" || p == "L1"); } }
        public bool IstTicketNutzer                 { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Ticket1" || p == "Ticket2" || p == "Ticket3" || p == "Ticket4" || p == "Bevor"); } }
        public bool IstTicketLeiterInstandhaltung   { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Ticket1"); } }
        public bool IstTicketLeiterProduktion       { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Ticket2"); } }
        public bool IstTicketSchichtführer          { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Ticket3"); } }
        public bool IstTicketInstandhalter          { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Ticket4"); } }
        public bool IstBevor                        { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "Bevor"); } }
        public bool IstDevAdmin                     { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "DevAdmin"); } }
        public bool IstAssetLöscher                 { get { return !rollen.IsNullOrEmpty() && rollen.Any(p => p == "AssetDelete"); } }


        public String RollenAsString
        {

            get {

                if (Rollen == null) return null;
                return String.Join(", ", Rollen.Select(p => RolleAsString(p)).ToArray());
            }
        }


        public String StatusAsString
        {
            get {

                switch (status) {

                    case LoginStatus.Aktiv    : return "Aktiv";
                    case LoginStatus.Gesperrt : return "Gesperrt";
                    default : return null;
                }
            }
        }


        public String Fullname
        {
            get {

                if (!String.IsNullOrEmpty(Name2) && !String.IsNullOrEmpty(Name1)) return String.Format("{0}, {1}", Name1, Name2);
                if (!String.IsNullOrEmpty(Name1)) return Name1;
                return Name;
            }
        }


#region V A L I D A T I O N


        public string Error
        {

            get {

                String test = this["Name"];
                if (!(String.IsNullOrEmpty(test))) return test;
                test = this["Name1"];
                if (!(String.IsNullOrEmpty(test))) return test;
                test = this["Name2"];
                if (!(String.IsNullOrEmpty(test))) return test;
                test = this["Status"];
                if (!(String.IsNullOrEmpty(test))) return test;
                test = this["Rollen"];
                return test;
            }
        }


        public List<String> Errorlist
        {

            get {

                var result = new List<String>();

                String test = this["Name"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);
                test = this["Name1"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);
                test = this["Name2"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);
                test = this["Email"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);
                test = this["Status"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);
                test = this["Rollen"];
                if (!(String.IsNullOrEmpty(test))) result.Add(test);

                if (HasPassword) {

                    var presult = ValidatePasswordAsList(Password1, Password2);
                    if (!presult.IsNullOrEmpty()) result.AddRange(presult);
                }

                return result;
            }
        }


        public string this[string columnName]
        {

            get {

                switch (columnName) {

                case "Name":

                    if (String.IsNullOrEmpty(Name)) return "Der Nutzer muss einen Anmeldenamen besitzen.";
                    if (Name.Length > 256) return "Der Anmeldename ist zu lang.";
                    break;

                case "Name1":

                    if (String.IsNullOrEmpty(Name)) return null;
                    if (Name.Length > 50) return "Der Nachname ist zu lang.";
                    break;

                case "Name2":

                    if (String.IsNullOrEmpty(Name)) return null;
                    if (Name.Length > 50) return "Der Vorname ist zu lang.";
                    break;

                case "Email":

                    if (String.IsNullOrEmpty(Email)) return null;
                    if (Email.Length > 256) return "Der E-Mailadresse ist zu lang.";
                    break;

                case "Status":

                    if (Status != LoginStatus.Aktiv && Status != LoginStatus.Gesperrt) return "Der Nutzer muss einen gültigen Status besitzen.";
                    break;

                case "Rollen":

                    if (Rollen.IsNullOrEmpty()) return "Der Nutzer muss einer Rolle zugeordnet sein.";
                    break;
                }

                return null;
            }
        }


        public bool HasPassword { get { return !String.IsNullOrEmpty(Password1) || !String.IsNullOrEmpty(Password2); } }


        private const int minPasswordLength = 8;


        public static String ValidatePassword(String passwort1, String passwort2)
        {

            if (String.IsNullOrEmpty(passwort1) || String.IsNullOrEmpty(passwort2)) return "Geben Sie beide Kennwörter an.";
            if (passwort1 != passwort2) return "Die beiden Kennwörter stimmen nicht überein.";
            if (passwort1.Length < minPasswordLength) return $"Das Kennwort muss aus mindestens {minPasswordLength} Zeichen bestehen.";
            if (passwort1.All(p => !Char.IsUpper(p))) return "Das Kennwort muss mindestens einen Großbuchstaben besitzen.";
            if (passwort1.All(p => !Char.IsLower(p))) return "Das Kennwort muss mindestens einen Kleinbuchstaben besitzen.";
            if (passwort1.All(p => !Char.IsDigit(p))) return "Das Kennwort muss mindestens eine Ziffer besitzen.";
            if (passwort1.All(p => Char.IsLetterOrDigit(p))) return "Das Kennwort muss mindestens ein Sonderzeichen besitzen.";

            return String.Empty;
        }


        public static IEnumerable<String> ValidatePasswordAsList(String passwort1, String passwort2)
        {

            var result = new List<String>();

            if (String.IsNullOrEmpty(passwort1) || String.IsNullOrEmpty(passwort2)) result.Add("Geben Sie beide Kennwörter an.");
            if (passwort1 != passwort2) result.Add("Die beiden Kennwörter stimmen nicht überein.");
            if (passwort1.Length < minPasswordLength) result.Add($"Das Kennwort muss aus mindestens {minPasswordLength} Zeichen bestehen.");
            if (passwort1.All(p => !Char.IsLetter(p) && !Char.IsUpper(p))) result.Add("Das Kennwort muss mindestens einen Großbuchstaben besitzen.");
            if (passwort1.All(p => !Char.IsLetter(p) && !Char.IsLower(p))) result.Add("Das Kennwort muss mindestens einen Kleinbuchstaben besitzen.");
            if (passwort1.All(p => !Char.IsDigit(p))) result.Add("Das Kennwort muss mindestens eine Ziffer besitzen.");
            if (passwort1.All(p => Char.IsLetterOrDigit(p))) result.Add("Das Kennwort muss mindestens ein Sonderzeichen besitzen.");

            return result;
        }


        public static String ValidatePassword(String passwortAlt, String passwort1, String passwort2)
        {

            if (String.IsNullOrEmpty(passwortAlt)) return "Geben Sie Ihr altes Kennwort an.";
            return ValidatePassword(passwort1, passwort2);
        }


#endregion

    }
}
