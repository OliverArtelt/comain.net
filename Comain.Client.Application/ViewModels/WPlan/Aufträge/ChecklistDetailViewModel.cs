﻿using System;
using System.Threading;
using System.Linq;
using Comain.Client.Views.WPlan.Aufträge;
using Comain.Client.ViewModels;
using Comain.Client.Models.WPlan;
using System.Collections.Generic;
using Comain.Client.Models;
using System.Text;
using Comain.Client.Dtos.WPlan;
using System.Windows.Input;
using System.Threading.Tasks;
using System.ComponentModel;
using Comain.Client.Dtos.Filters;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Waf.Applications;

namespace Comain.Client.ViewModels.WPlan.Aufträge
{

    public class ChecklistDetailViewModel : ValidatableViewModel<IChecklistDetailView, ChecklistViewItem>, IDelayableViewModel
    {

        private readonly SynchronizationContext context;


        public ChecklistDetailViewModel(IChecklistDetailView view)
          : base(view)
        {

            context = SynchronizationContext.Current;

            PersonAddCommand = new DelegateCommand(PersonAdd);
            PersonRemoveCommand = new DelegateCommand(PersonRemove);
            PersonClearCommand = new DelegateCommand(PersonClear);
            ResetFilterCommand = new DelegateCommand(ResetFilter);
        }


        public override void CurrentChanged(ChecklistViewItem oldSelected)
        {

            RaisePropertyChanged(nameof(Status));
            RaisePropertyChanged(nameof(Name));
            RaisePropertyChanged(nameof(Beschreibung));
            RaisePropertyChanged(nameof(Qualifikation));
            RaisePropertyChanged(nameof(Werkzeug));
            RaisePropertyChanged(nameof(Durchführung));
            RaisePropertyChanged(nameof(Sicherheit));
            RaisePropertyChanged(nameof(Dokumentation));
            RaisePropertyChanged(nameof(DurchgeführtAm));
            RaisePropertyChanged(nameof(MinutenGearbeitet));
            RaisePropertyChanged(nameof(Bemerkungen));
            RaisePropertyChanged(nameof(Baugruppe));
            RaisePropertyChanged(nameof(Normzeit));
            RaisePropertyChanged(nameof(MengeErledigt));
            RaisePropertyChanged(nameof(Wert));
            RaisePropertyChanged(nameof(ExterneAuftragsnummer));
            RaisePropertyChanged(nameof(Massnahmenummer));
            RaisePropertyChanged(nameof(MengeGeplant));
            RaisePropertyChanged(nameof(QualifikationVisible));
            RaisePropertyChanged(nameof(WerkzeugVisible));
            RaisePropertyChanged(nameof(DurchführungVisible));
            RaisePropertyChanged(nameof(SicherheitVisible));
            RaisePropertyChanged(nameof(DokumentationVisible));
            RaisePropertyChanged(nameof(InfoVisible));
            RaisePropertyChanged(nameof(BilderVisible));
            RaisePropertyChanged(nameof(MaterialVisible));
            RaisePropertyChanged(nameof(Medien));
            RaisePropertyChanged(nameof(CurrentMedia));
            RaisePropertyChanged(nameof(Baugruppe));
            RaisePropertyChanged(nameof(ErledigtAmText));
            RaisePropertyChanged(nameof(ErledigVonText));
            RaisePropertyChanged(nameof(EditEnabled));
            RaisePropertyChanged(nameof(Materialliste));
            RaisePropertyChanged(nameof(ZyklusAsString));

            RaisepositionErgebnis();


            if (AllePersonen == null || Current == null) return;

            context.Send(new SendOrPostCallback((o) => {

                GefiltertePersonen = new CollectionViewSource { Source = AllePersonen }.View;
                GefiltertePersonen.Filter = PersonalPredicate;
                GefiltertePersonen.SortDescriptions.Clear();
                GefiltertePersonen.SortDescriptions.Add(new SortDescription { PropertyName="Name" });
                PersonRefresh();
                RaisePropertyChanged(nameof(GewähltePersonen));

            }), null);
        }


        private void RaisepositionErgebnis()
        {

            RaisePropertyChanged(nameof(NichtDurchgeführt));
            RaisePropertyChanged(nameof(ErgebnisOk));
            RaisePropertyChanged(nameof(ErgebnisNotOK));
            RaisePropertyChanged(nameof(ErgebnisWirdNichtErledigt));
            RaisePropertyChanged(nameof(ErgebnisFortgeführt));
            RaisePropertyChanged(nameof(LeistungBuchbar));
        }


        public void Unload()
        {

            Medien = null;
            Current = null;
            Gewerke = null;
            AllePersonen = null;
        }


        public async Task SetListAsync(IList<PersonalFilterModel> pslist, IList<NummerFilterModel> gwlist)
        {

            await context.SendAsync(new SendOrPostCallback((o) => {

                AllePersonen = pslist;
                Gewerke = gwlist;

            }), null);

            RaisePropertyChanged(nameof(AllePersonen));
            RaisePropertyChanged(nameof(Gewerke));
        }


#region P R O P E R T I E S


        public String    Name                   => Current?.Model.Name;
        public String    Beschreibung           => Current?.Model.Kurzbeschreibung;
        public String    Qualifikation          => Current?.Model.Qualifikation;
        public String    Werkzeug               => Current?.Model.Werkzeug;
        public String    Durchführung           => Current?.Model.Durchführung;
        public String    Sicherheit             => Current?.Model.Sicherheit;
        public String    Dokumentation          => Current?.Model.Dokumentation;
        public String    Baugruppe              => Current?.Model.Baugruppe;
        public String    Status                 => Current?.Status;
        public String    ExterneAuftragsnummer  => Current?.ExterneAuftragsnummer;
        public String    Massnahmenummer        => Current?.Massnahmenummer;

        public bool      InfoVisible            => QualifikationVisible || WerkzeugVisible || DurchführungVisible || SicherheitVisible || DokumentationVisible;
        public bool      QualifikationVisible   => !String.IsNullOrEmpty(Current?.Model.Qualifikation);
        public bool      WerkzeugVisible        => !String.IsNullOrEmpty(Current?.Model.Werkzeug);
        public bool      DurchführungVisible    => !String.IsNullOrEmpty(Current?.Model.Durchführung);
        public bool      SicherheitVisible      => !String.IsNullOrEmpty(Current?.Model.Sicherheit);
        public bool      DokumentationVisible   => !String.IsNullOrEmpty(Current?.Model.Dokumentation);
        public bool      BilderVisible          => true;
        public bool      MaterialVisible        => Current != null && !Current.Model.Materialliste.IsNullOrEmpty();
        public bool      LeistungBuchbar        => Current != null && Current.LeistungBuchbar;

        public String    ErledigtAmText         => Current?.ErledigtAm;
        public String    ErledigVonText         => Current?.Personal;
        public String    EmpfohlenText          => Current?.Model.EmpfohlenesPersonal?.Name;

        public int?      Normzeit               => Current?.Normzeit;
        public int?      MengeGeplant           => Current?.MengeGeplant;
        public decimal?  Wert                   => Current?.Wert;


        public List<MaterialInfoDto> Materialliste => Current?.Model?.Materialliste;


        private bool editEnabled;
        public new bool EditEnabled
        {
            get { return editEnabled; }
            set { SetProperty(ref editEnabled, value); }
        }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        public String ZyklusAsString
        {
            get {

                if (Current == null) return null;

                var bld = new StringBuilder();
                bld.Append("Zyklus ").Append(WiMassnahme.ZyklusAsString(Current.Model.Termintyp, Current.Model.Zyklus.GetValueOrDefault(), Current.Model.Tagesmuster));
                if (Current.Model.ZuletztAm.HasValue) bld.Append(", zuletzt am ").Append(Current.Model.ZuletztAm.Value.ToShortDateString());
                if (Current.Model.NächsterAm.HasValue) bld.Append(", nächster am ").Append(Current.Model.NächsterAm.Value.ToShortDateString());
                return bld.ToString();
            }
        }


        public DateTime? DurchgeführtAm => Current?.DurchgeführtAm;


        public bool NichtDurchgeführt
        {
            get => Current != null && Current.NichtDurchgeführt;
            set {

                if (Current != null && value && !Current.NichtDurchgeführt) {

                    Current.NichtDurchgeführt = true;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisOk
        {
            get => Current != null && Current.ErgebnisOk;
            set {

                if (Current != null && value && !Current.ErgebnisOk) {

                    Current.ErgebnisOk = true;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisNotOK
        {
            get => Current != null && Current.ErgebnisNotOK;
            set {

                if (Current != null && value && !Current.ErgebnisNotOK) {

                    Current.ErgebnisNotOK = true;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisWirdNichtErledigt
        {
            get => Current != null && Current.ErgebnisWirdNichtErledigt;
            set {

                if (Current != null && value && !Current.ErgebnisWirdNichtErledigt) {

                    Current.ErgebnisWirdNichtErledigt = true;
                    RaisepositionErgebnis();
                }
            }
        }


        public bool ErgebnisFortgeführt
        {
            get => Current != null && Current.ErgebnisFortgeführt;
            set {

                if (Current != null && value && !Current.ErgebnisFortgeführt) {

                    Current.ErgebnisFortgeführt = true;
                    RaisepositionErgebnis();
                }
            }
        }


        public int? MinutenGearbeitet
        {
            get => Current?.MinutenGearbeitet;
            set {

                if (Current != null && value != Current.MinutenGearbeitet) {

                    Current.MinutenGearbeitet = value;
                    RaisePropertyChanged(nameof(MinutenGearbeitet));
                }
            }
        }


        public int? MengeErledigt
        {
            get => Current?.MengeErledigt;
            set {

                if (Current != null && value != Current.MengeErledigt) {

                    Current.MengeErledigt = value;
                    RaisePropertyChanged(nameof(MengeErledigt));
                }
            }
        }


        public String Bemerkungen
        {
            get => Current?.Model.Bemerkungen;
            set {

                if (Current != null && value != Current.Model.Bemerkungen) {

                    Current.Model.Bemerkungen = value;
                    RaisePropertyChanged(nameof(Bemerkungen));
                }
            }
        }


        private List<Media> medien;
        public List<Media> Medien
        {
            get { return medien; }
            set { if (SetProperty(ref medien, value)) RaisePropertyChanged(nameof(BilderVisible)); }
        }


        private Media currentMedia;
        public Media CurrentMedia
        {
            get { return currentMedia; }
            set { SetProperty(ref currentMedia, value); }
        }


#endregion


#region P E R S O N


        private void PersonAdd()
        {

            if (PersonForAdding == null) return;
            GewähltePersonen.Add(PersonForAdding);
            PersonForAdding = null;
            PersonRefresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        private void PersonRemove()
        {

            if (PersonForRemoving == null) return;
            GewähltePersonen.Remove(PersonForRemoving);
            PersonForRemoving = null;
            PersonRefresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        private void PersonClear()
        {

            GewähltePersonen.Clear();
            PersonRefresh();
            Current.RefreshPersonal();
            RaisePropertyChanged(nameof(GewähltePersonen));
        }


        private void ResetFilter()
        {

            Suchtext = null;
            AktuellesGewerk = null;
        }


        public void PersonRefresh()
        {

            if (AllePersonen == null || GefiltertePersonen == null) return;
            context.Post(new SendOrPostCallback((o) => {

                GefiltertePersonen?.Refresh();
                RaisePropertyChanged(nameof(GefiltertePersonen));
            }), null);

            RaisePropertyChanged(nameof(GefiltertePersonen));
        }


        private bool PersonalPredicate(object arg)
        {

            var ps = arg as PersonalFilterModel;
            if (ps == null) return false;

            if (AktuellesGewerk != null && (!ps.Gewerk_Id.HasValue || ps.Gewerk_Id.Value != AktuellesGewerk.Id)) return false;
            if (!String.IsNullOrWhiteSpace(suchtext)) {

                if (!ps.Nummer.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase) &&
                !ps.Nachname.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase) &&
                !ps.Vorname.Contains(suchtext, StringComparison.CurrentCultureIgnoreCase)) return false;
            }
            if (GewähltePersonen.IsNullOrEmpty()) return true;

            return GewähltePersonen.All(p => p.Id != ps.Id);
        }


        public ICollectionView GefiltertePersonen       { get; private set; }
        public IList<PersonalFilterModel> AllePersonen  { get; private set; }


        public ObservableCollection<PersonalFilterModel> GewähltePersonen  => Current?.Model?.Personalliste;


        private String suchtext;
        public String Suchtext
        {
            get { return suchtext; }
            set { if (SetProperty(ref suchtext, value)) PersonRefresh(); }
        }


        public IList<NummerFilterModel> Gewerke  { get; private set; }


        private NummerFilterModel aktuellesGewerk;
        public NummerFilterModel AktuellesGewerk
        {
            get { return aktuellesGewerk; }
            set { if (SetProperty(ref aktuellesGewerk, value)) PersonRefresh(); }
        }


        private PersonalFilterModel personForRemoving;
        public PersonalFilterModel PersonForRemoving
        {
            get { return personForRemoving; }
            set { SetProperty(ref personForRemoving, value); }
        }


        private PersonalFilterModel personForAdding;
        public PersonalFilterModel PersonForAdding
        {
            get { return personForAdding; }
            set { SetProperty(ref personForAdding, value); }
        }


        public ICommand PersonClearCommand  { get; }
        public ICommand ResetFilterCommand  { get; }
        public ICommand PersonAddCommand    { get; }
        public ICommand PersonRemoveCommand { get; }


#endregion


    }
}
