﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Waf.Applications;
using System.Windows.Input;


namespace Comain.Client.ViewModels.Verwaltung
{

    public class SimpleViewModel<TModel, TView> : ValidatableViewModel<TView, TModel>, IDelayableViewModel where TView : IView
                                                                                                           where TModel : INotifyPropertyChanged, IDataErrorInfo
    {

        protected readonly SynchronizationContext context;


        public SimpleViewModel(TView view) : base(view)
        {
            context = SynchronizationContext.Current;
        }


        public virtual void Unload()
        {

            Liste = null;
            Current = default(TModel);
        }


        public virtual void SetList(ObservableCollection<TModel> liste)
        {

            Liste = liste;
            RaisePropertyChanged("Liste");
        }


#region P R O P E R T I E S


        public ObservableCollection<TModel> Liste { get; private set; }


        private bool showDelay;
        public bool ShowDelay
        {
            get { return showDelay; }
            set { SetProperty(ref showDelay, value); }
        }


        private ICommand saveCommand;
        public ICommand SaveCommand
        {
            get { return saveCommand; }
            set { SetProperty(ref saveCommand, value); }
        }


        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get { return cancelCommand; }
            set { SetProperty(ref cancelCommand, value); }
        }


        private ICommand addCommand;
        public ICommand AddCommand
        {
            get { return addCommand; }
            set { SetProperty(ref addCommand, value); }
        }


        private ICommand deleteCommand;
        public ICommand DeleteCommand
        {
            get { return deleteCommand; }
            set { SetProperty(ref deleteCommand, value); }
        }


#endregion


    }
}
