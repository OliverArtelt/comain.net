﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows.Controls;
using Comain.Client.Services;
using Comain.Client.Services.Reports;
using Comain.Client.ViewData;
using Comain.Client.ViewModels;
using Comain.Client.ViewModels.Reports;


namespace Comain.Client.Controllers.Reports
{


    public class ReportController : ControllerBase, IReportController
    {

        private readonly IBreadcrumbService crumbs;
        private readonly MainViewModel mainView;
        private readonly ReportViewModel view;
        private readonly FilterListViewModel filterView;
        private readonly IEnumerable<IFilterService> stores;
        private readonly ILoginService login;
        private readonly IReportFactory factory;
        private bool detailMode;


        public ReportController(IBreadcrumbService crumbs, IEnumerable<IFilterService> stores, ILoginService login, IControllerBus bus,
                                MainViewModel mainView, ReportViewModel view, FilterListViewModel filterView,
                                IMessageService messages, IReportFactory factory)
          : base(messages, bus)
        {

            this.crumbs = crumbs;
            this.mainView = mainView;
            this.view = view;
            this.login = login;
            this.stores = stores;
            this.factory = factory;
            this.filterView = filterView;
        }


        public void Initialize()
        {

            view.FilterView = filterView.View;
            view.BackCommand = new AsyncDelegateCommand(_ => BackAsync());
            filterView.ResetCommand = new DelegateCommand(FilterReset);
            filterView.SearchCommand = CreateCommand(_ => SearchAsync());

          //  PropertyChangedEventManager.AddHandler(view, ViewPropertyChanged, "");
          //  view.DetailReportRequest += DrillthroughEventHandler;
        }


        public override async Task RunAsync(object parameter)
        {

            await base.RunAsync(parameter);

            crumbs.Add("Berichte");
            view.ViewerVisible = false;
            detailMode = false;
            mainView.PageView = view.View;
            view.SetzeReports(factory.AlleReports());
            view.Current = null;
            view.Mandant = login.AktuelleVerbindung;
            await ExecuteWithLockAsync(_ => LoadAsync());
        }


        public void Shutdown()
        {

         //   PropertyChangedEventManager.RemoveHandler(view, ViewPropertyChanged, "");
         //   view.DetailReportRequest -= DrillthroughEventHandler;
        }


        protected override Task<bool> UnloadImplAsync()
        {

            stores.ForEach(p => p.Unload());
            factory.Unload();
            return Task.FromResult(true);
        }


        private async Task BackAsync()
        {

            if (detailMode) {

                await SearchAsync();

            } else if (!view.FilterIsExpanded && view.ViewerVisible) {

                view.FilterIsExpanded = true;

            } else {

                await bus.BackAsync();
            }
        }


        private async Task LoadAsync()
        {

            using (new VisualDelay(view)) {

                await Task.WhenAll(stores.Select(p => p.LoadAsync(bus.Token))).ConfigureAwait(false);
                bus.Token.ThrowIfCancellationRequested();
                await factory.LoadAsync(bus.Token).ConfigureAwait(false);
            }
        }


        private async void ViewPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Current") await DisplayReportAsync();
        }


        private void FilterReset()
        {
            if (view.Current != null) view.Current.FilterViews.ForEach(p => p.Reset());
        }


        /// <summary>
        /// Berichtstyp gewählt: Filterkriterien darstellen
        /// </summary>
        private async Task DisplayReportAsync()
        {

            try {

                if (view.Current == null) {

                    filterView.Filters = null;
                    return;
                }

                await OnUIThreadAsync(() => {

                    view.ViewerVisible = false;
                    filterView.Filters = null;
                    if (view.Current.FilterViews != null)
                        filterView.Filters = view.Current.FilterViews.Select(p => new UserControlContainer { Control = p.View as UserControl}).ToList();
                    view.FilterIsExpanded = true;

                });

                await view.Current.DisplayAsync(bus.Token);
            }
            catch (Exception x) {

                view.ViewerVisible = false;
                messages.Show(x);
            }
        }


        /// <summary>
        /// Bericht rendern und anzeigen
        /// </summary>
        private async Task SearchAsync()
        {

            view.ViewerVisible = false;
            if (view.Current == null) return;
            var validation = view.Current.Validiere();
            if (!validation.IsNullOrEmpty()) {

                var token = new MessageToken { Header = "Der Bericht kann nicht erstellt werden", Reasons = validation.ToList() };
                await messages.ShowMessageAsync(token);
                return;
            }

            view.FilterIsExpanded = false;

            using (new VisualDelay(view)) {

                var result = await view.Current.CreateAsync(bus.Token);
                await view.BindViewerAsync(result);
                detailMode = false;
            }
        }

        /*
        private async Task SearchDetailAsync(DrillthroughEventArgs e)
        {

            var o = e.Report as LocalReport;
            if (o.OriginalParametersToDrillthrough.Count == 0) return;
           
            var plist = o.OriginalParametersToDrillthrough.Where(p => p.Values.Count > 0)
                                                            .Select(p => new Tuple<String, String>(p.Name, p.Values[0]))
                                                            .ToList();
            using (new VisualDelay(view)) {
           
                var result = await view.Current.CreateDetailAsync(e.ReportPath, plist, bus.Token);
                await view.BindViewerAsync(result);
                detailMode = true;
            }
        }


        private async void DrillthroughEventHandler(object sender, DrillthroughEventArgs e)
        {
    
            if (view.Current == null) return;
            await SearchDetailAsync(e);
        }*/
    }
}