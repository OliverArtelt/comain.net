﻿using System;
using Comain.Client.Services;
using Comain.Client.Services.Dokumente;
using Comain.Client.ViewModels.Dokumente;


namespace Comain.Client.Controllers.Dokumente
{

    public class MNDokumentController : DokumentAssignController, IMNDokumentController
    {


        public MNDokumentController(IDokumentService store, IMessageService messages, IControllerBus bus,
                                    DokumentAssignViewModel view)
          : base(store, messages, bus, view)
        {}


        protected override String EntityType => "MN";

        protected override bool   ShowDisplayAsset              => true;
        protected override bool   ShowDisplayAssetWithParents   => true;
        protected override bool   ShowDisplayAssetWithChildren  => false;
        protected override bool   ShowDisplayAuftrag            => false;
        protected override bool   ShowDisplayMassnahme          => false;

        protected override bool   ShowSearchAllDocs             => true;
        protected override bool   ShowSearchAssets              => true;
        protected override bool   ShowSearchMassnahmen          => false;
    }
}
