﻿using System.Collections.Generic;
using Comain.Client.Models;
using Comain.Client.Models.Aufträge;

namespace Comain.Client.ViewData.Verwaltung
{
    public interface IDringlichkeitenProvider
    {

        Dringlichkeit this[int? id] { get; }
        IList<Dringlichkeit> Liste(Auftragstyp typ);
    }
}