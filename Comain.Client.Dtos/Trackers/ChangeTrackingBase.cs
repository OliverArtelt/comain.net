﻿using System;
using System.Collections.Generic;


namespace Comain.Client.Trackers
{

    public abstract class ChangeTrackingBase 
    {
    
        public int              Id              { get; set; }
        public bool             IstAngefügt     { get => Id <= 0; }
        public ChangeStatus     ChangeStatus    { get; set; }
        
        private HashSet<String> changedProps;
        public HashSet<String>  ChangedProps    
        { 
            get {

                if (changedProps == null) changedProps = new HashSet<string>();
                return changedProps;
            }
            set { changedProps = value; } 
        }
    }
}
